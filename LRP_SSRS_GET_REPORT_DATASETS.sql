USE [impresario]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[LRP_SSRS_GET_REPORT_DATASETS]
AS

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
SET NOCOUNT ON

--Copied from Comments http://jacobsebastian.blogspot.com/2008/01/how-to-find-all-stored-procedures-used.html

;
WITH XMLNAMESPACES (
DEFAULT
'http://schemas.microsoft.com/sqlserver/reporting/2008/01/reportdefinition',
'http://schemas.microsoft.com/SQLServer/reporting/reportdesigner' AS rd
),
RPTDATA AS 
(
	SELECT
	SUBSTRING(a.ReportPath,1,CHARINDEX('/',a.ReportPath)-1) AS ParentFolder,
	SUBSTRING(a.ReportPath,CHARINDEX('/',a.ReportPath)+1,50) AS SubFolder,
	ReportPath,
	Name AS reportname,
	x.value('DataSourceName[1]', 'VARCHAR(max)') AS datasourcename,
	q.value('@Name[1]', 'VARCHAR(max)') AS datasetname,
	x.value('CommandType[1]', 'VARCHAR(max)') AS CommandType,
	x.value('CommandText[1]','VARCHAR(max)') AS CommandText
	FROM
	(
		SELECT Name,
		REPLACE(REVERSE(SUBSTRING(REVERSE(Path), CHARINDEX('/', REVERSE(Path)), LEN(REVERSE(Path)))),'/Tessitura/','') AS ReportPath,
		CAST(CAST(Content AS VARBINARY(MAX)) AS XML) AS reportxml
		FROM ReportServer.dbo.Catalog
		WHERE [Type] = 2
	) a
	CROSS APPLY reportxml.nodes('/Report/DataSets/DataSet') d(q)
	CROSS APPLY q.nodes('Query') r(x)
	WHERE NOT PATINDEX('%.%', Name) > 0
)
SELECT 
	r.ReportPath,
	r.reportname,
	r.datasourcename,
	r.datasetname,
	CASE r.CommandType
		WHEN 'StoredProcedure' THEN 'SP'
		ELSE r.CommandType
	END AS CommandType,
	r.CommandText,
	X.SetSecurityContext
FROM RPTDATA r
LEFT JOIN
(
	SELECT r1.ParentFolder,
		r1.SubFolder,
		r1.reportname,
		1 AS SetSecurityContext
	FROM RPTDATA r1
	WHERE r1.datasetname = 'SetSecurityContext'
) X
	ON X.ParentFolder = r.ParentFolder
	AND X.SubFolder = r.SubFolder
	AND X.reportname = r.reportname
WHERE r.datasetname <> 'SetSecurityContext'	

GO
GRANT EXECUTE ON [dbo].[LRP_SSRS_GET_REPORT_DATASETS] TO ImpUsers
GO