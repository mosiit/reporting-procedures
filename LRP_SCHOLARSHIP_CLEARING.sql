USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_SCHOLARSHIP_CLEARING]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LRP_SCHOLARSHIP_CLEARING]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LRP_SCHOLARSHIP_CLEARING]
        @report_start_dt datetime = Null, 
        @report_end_dt datetime = NULL,
        @specific_scholarship_no INT = 0,
        @specific_customer_no INT = 0,
        @specific_order_no INT = 0,
        @exclude_zero_sums CHAR(1) = 'Y',
        @funding_allocation_str VARCHAR(4000)= ''
AS BEGIN

    /*  Procedure Variables  */

        DECLARE @prev_ord_no INT = 0, @prev_sch_no INT = 0, @cust_no INT = 0
        DECLARE @ord_no INT = 0, @sch_no INT = 0, @prf_no INT = 0
        DECLARE @prf_date char(10) = '', @pay_method VARCHAR(30) = ''

        DECLARE @allocations TABLE ([allocation_id] INT NULL)
        DECLARE @order_table TABLE ([order_no] INT, [perf_no] INT, [zone_no] INT, perf_dt DATETIME)
            

    /*  Check Parameters  --  If nulls passed to date parameters, run for yesterday.  */

        SELECT @report_start_dt = IsNull(@report_start_dt, dateadd(day, -1, getdate())),
               @report_end_dt = IsNull(@report_end_dt, dateadd(day, -1, getdate()))

        SELECT @specific_scholarship_no = ISNULL(@specific_scholarship_no, 0)
        SELECT @specific_customer_no = ISNULL(@specific_customer_no, 0)
        SELECT @exclude_zero_sums = ISNULL(@exclude_zero_sums, 'Y')

        SELECT @funding_allocation_str = ISNULL(@funding_allocation_str,'')


    /*  Make sure the date parameters include the full dates, starting at 00:00 and ending at 23:59  */

        SELECT @report_start_dt = convert(char(10),@report_start_dt,111) + ' 00:00:00',
               @report_end_dt = convert(char(10), @report_end_dt,111) + ' 23:59:59'


    /*  Parse out multi-selection parameters  */

        IF @funding_allocation_str <> ''
        	INSERT INTO @allocations ([allocation_id])
	        SELECT [Element] FROM [dbo].[FT_SPLIT_LIST](@funding_allocation_str,',')

    /*  Temporary Table  */

        IF OBJECT_ID('tempdb..#scr_final_table') IS NOT NULL DROP TABLE [#scr_final_table]

        CREATE TABLE [#scr_final_table] ([report_message] varchar(100), [payment_type] varchar(30), [payment_no] int, [sequence_no] int, [order_no] int, [performance_no] int, 
                                         [performance_date] char(10), [customer_no] int, [customer_name] varchar(165), [customer_type] varchar(30), [scholarship_no] VARCHAR(30), 
                                         [scholarship_type] varchar(30), [scholarship_name] varchar(165), [transaction_no] int, [payment_date] char(10), [payment_operator] varchar(8), 
                                         [payment_method] varchar(30), [payment_amount] decimal(18,2), [notes] varchar(255), [allocation_no] INT, [allocation_name] VARCHAR(30)
                     CONSTRAINT [PK_Scholarship_Clearing_#scr_final_table] PRIMARY KEY NONCLUSTERED ([payment_no] ASC, [sequence_no] ASC, [performance_no] ASC))

        CREATE CLUSTERED INDEX [ix_scr_final_table_scholarship_no] ON [#scr_final_table] ([payment_type] ASC, [scholarship_name] ASC, [scholarship_no] ASC) ON [PRIMARY]
    
       
    /*  Get all orders with performances within the date range passed to the report  */
    
        INSERT INTO @order_table ([order_no], [perf_no], [zone_no], [perf_dt])
        SELECT [order_no], 
               MAX([performance_no]), 
               0,
               MAX([performance_dt])
        FROM [dbo].[LV_SCHOLARSHIP_PAYMENTS] (NOLOCK)
        WHERE [performance_dt] BETWEEN @report_start_dt AND @report_end_dt
        --WHERE [payment_dt] BETWEEN @report_start_dt AND @report_end_dt  --11/12/2021 - Changed to performance date at Stan's request
        GROUP BY [order_no]

                --THIS IS THE OLD WAY ORDERS WERE SELECTED...PROBLEMATIC BECAUSE THERE WERE ORDERS BEING SKIPPED BECAUSE THEY HAD NO PERFS ON THEM
                --SELECT sli.[order_no], MAX(sli.[perf_no]), 0, MAX(prf.[performance_dt])
                --FROM [dbo].[T_SUB_LINEITEM] AS sli (NOLOCK)
                --     LEFT OUTER JOIN [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] AS prf (NOLOCK) ON prf.[performance_no] = sli.[perf_no] AND prf.[performance_zone] = sli.[zone_no]
                --WHERE prf.[performance_dt] BETWEEN @report_start_dt AND @report_end_dt AND sli.[sli_status] IN (3, 8, 12)
                --GROUP BY sli.[order_no]


    /*  Using the order table, get all scholarship payments for those orders  */

        INSERT INTO [#scr_final_table] ([report_message], [payment_type], [payment_no], [sequence_no], [order_no], [performance_no], [performance_date], [customer_no], [customer_name], 
                                        [customer_type], [scholarship_no], [scholarship_type], [scholarship_name], [transaction_no], [payment_date], [payment_operator],
                                        [payment_method], [payment_amount], [notes], [allocation_no], [allocation_name])
        SELECT ''
             , ''
             , pay.[payment_no]
             , pay.[transaction_Sequence_no]
             , ord.[order_no]
             , ord.[perf_no]
             , CONVERT(CHAR(10),ord.[perf_dt],111)
             , pay.[customer_no]
             , pay.[customer_name]
             , pay.[customer_type]
             , pay.[scholarship_no]
             , pay.[scholarship_type]
             , pay.[scholarship_name]
             , pay.[transaction_no]
             , pay.[payment_date]
             , pay.[payment_created_by]
             , pay.[payment_method]
             , pay.[payment_amount]
             , pay.[payment_notes]
             , ISNULL(alo.[allocation_no],0)
             , ISNULL(alo.[allocation_name],'')
        FROM @order_table AS ord 
             INNER JOIN [dbo].[LV_SCHOLARSHIP_PAYMENTS] AS pay (NOLOCK) ON pay.[order_no] = ord.[order_no]
             LEFT OUTER JOIN [dbo].[LV_SCHOLARSHIP_ALLOCATIONS] AS alo (NOLOCK) ON alo.[scholarship_no] = pay.[scholarship_no] AND alo.[allocation_pmt_method] = pay.[payment_method_no]
        
    /*  Flag the records where the constituent type on the scholarship is not Internal Fund and it's not an interdepartmental transfer.  */
        
        UPDATE [#scr_final_table] 
        SET [scholarship_name] = 'Interdeparmental Transfer', [scholarship_type] = 'Unknown', [scholarship_no] = 0 , [payment_type] = 'Transfer'
        WHERE [payment_method] = 'Interdepartmental Transfer'

        UPDATE [#scr_final_table] 
        SET [payment_type] = 'Scholarship'
        WHERE [payment_method] <> 'Interdepartmental Transfer'

        UPDATE [#scr_final_table] 
        SET [scholarship_name] = 'Unknown Scholarship Fund (' + CONVERT(VARCHAR(25),[scholarship_no]) + ')', [scholarship_type] = 'Unknown'
        WHERE [payment_method] <> 'Interdepartmental Transfer' and [scholarship_type] <> 'Internal Fund'
      
        UPDATE [#scr_final_table] 
        SET [performance_date] = (SELECT ISNULL(MAX([performance_date]),'') FROM [dbo].[LV_SCHOLARSHIP_PAYMENTS] AS sch WHERE sch.[order_no] = [#scr_final_table].[order_no])
        WHERE [performance_no] = 0

    /*  Delete records with bad dates  */
                        
        DELETE FROM [#scr_final_table] WHERE performance_date = ''

        DELETE FROM [#scr_final_table] WHERE CONVERT(DATETIME,[performance_date]) < @report_start_dt OR CONVERT(DATETIME,[performance_date]) > @report_end_dt

    /*  Narrow down to specific scholarship or customer number  */
        
        IF @specific_scholarship_no > 0 DELETE FROM [#scr_final_table] WHERE [scholarship_no] <> @specific_scholarship_no

        IF @specific_customer_no > 0 DELETE FROM [#scr_final_table] WHERE [customer_no] <> @specific_customer_no

        IF @specific_order_no > 0 DELETE FROM [#scr_final_table] WHERE [order_no] <> @specific_order_no

    /*  Remove zero sums if so desired  */

        IF @exclude_zero_sums = 'Y' BEGIN

            DECLARE zero_method_cursor INSENSITIVE CURSOR FOR
            SELECT customer_no, order_no, scholarship_no, payment_method
            FROM #scr_final_table
            GROUP BY customer_no, order_no, scholarship_no, payment_method
            HAVING SUM(payment_amount) = 0.00
            OPEN zero_method_cursor
            BEGIN_ZERO_METHOD_LOOP:

                FETCH NEXT FROM zero_method_cursor INTO @cust_no, @ord_no, @sch_no, @pay_method
                IF @@FETCH_STATUS = -1 GOTO END_ZERO_METHOD_LOOP

                DELETE FROM [#scr_final_table]
                WHERE customer_no = @cust_no AND order_no = @ord_no AND scholarship_no = @sch_no AND payment_method = @pay_method
    
                GOTO BEGIN_ZERO_METHOD_LOOP

            END_ZERO_METHOD_LOOP:
            CLOSE zero_method_cursor
            DEALLOCATE zero_method_cursor

        END

    /*  Add order number to the notes field.  */

        UPDATE [#scr_final_table] 
        SET [notes] = 'ord# ' + CONVERT(VARCHAR(25),order_no) + ' - ' + [notes] 
        WHERE [notes] <> ''
        
        UPDATE [#scr_final_table] 
        SET [notes] = 'ord# ' + CONVERT(VARCHAR(25),order_no) 
        WHERE [notes] = ''

    /*  If limited to certain allocations, delete the rest of the records  */

        IF EXISTS (SELECT * FROM @allocations)
            DELETE FROM [#scr_final_table]
            WHERE [allocation_no] NOT IN (SELECT [allocation_id] FROM @allocations)

    FINISHED:

        /*  Select the final record set from #final_table  */
        /*  If there is nothing in the #scr_final_table, add a single record with a message saying no records were found/  */
    
            IF not exists (SELECT * FROM [#scr_final_table])
           
                SELECT 'No records found that meet that criteria', '', 0, 0, 0, 0, '', 0, '', '', 0, '', '', 0, '', '', '', 0.00, '', 0, ''

            ELSE

                SELECT [report_message], 
                       [payment_type],
                       [scholarship_name],
                       [scholarship_no],
                       [payment_method],
                       [payment_date],
                       [payment_operator],
                       [order_no],
                       [performance_no],
                       [performance_date], 
                       [customer_no],
                       [customer_name],
                       [customer_type],
                       SUM([payment_amount]) as 'payment_amount',
                       [notes],
                       [allocation_no],
                       [allocation_name]
                FROM [#scr_final_table]   
                GROUP BY [report_message], [payment_type], [scholarship_name], [scholarship_no], [payment_method], [payment_date], [payment_operator], [order_no], [performance_no], [performance_date], 
                         [customer_no], [customer_name], [customer_type], [notes], [allocation_no], [allocation_name]
                HAVING sum([payment_amount]) <> 0.00
                ORDER BY [payment_type], [scholarship_name], [scholarship_no], [payment_method], [payment_date], [payment_operator], [order_no]



    ALL_DONE:

        /*  Clean up  */

            IF OBJECT_ID('tempdb..#scr_final_table') IS NOT NULL DROP TABLE [#scr_final_table]

END
GO

GRANT EXECUTE ON [dbo].[LRP_SCHOLARSHIP_CLEARING] TO impusers
GO

EXECUTE [dbo].[LRP_SCHOLARSHIP_CLEARING] '10-1-2021', '10-31-2021', 0, 0, 0, 'N'

