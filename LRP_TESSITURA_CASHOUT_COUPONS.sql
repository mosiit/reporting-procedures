USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_TESSITURA_CASHOUT_COUPONS]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LRP_TESSITURA_CASHOUT_COUPONS]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE  [dbo].[LRP_TESSITURA_CASHOUT_COUPONS]
       @rpt_dt DATETIME = Null,
       @rpt_operator VARCHAR(8) = Null,
       @machine_locations varchar(4000) = '',
	   @type CHAR(1) = 'C' -- L for Library, C for Coupon, V for Voucher
AS BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

    /*  Procedure Variables  */

        DECLARE @rpt_date CHAR(10), @rpt_message VARCHAR(100)
        DECLARE @coupon_raw_data_table TABLE ([coupon_date] char(10), 
                                              [price_type_name] varchar(30), 
                                              [comp_code_name_short] varchar(20), 
                                              [comp_code_name] varchar(30), 
                                              [total_coupons] int, 
                                              [order_no] int, 
                                              [coupon_operator] varchar(8),
                                              [operator_first] varchar(50),
                                              [operator_last] varchar(50),
                                              [operator_location] varchar(50), 
                                              [elapsed_time] int,
                                              [coupon_machine] VARCHAR(50),
                                              [rpt_message] varchar(100))

        DECLARE @coupon_table table ([coupon_date] char(10),
                                     [price_type_name] varchar(30),
                                     [comp_code_name_short] varchar(20),
                                     [comp_code_name] varchar(30),
                                     [total_coupons] int, 
                                     [total_transactions] int,
                                     [coupon_operator] varchar(8),
                                     [operator_first] varchar(50),
                                     [operator_last] varchar(50),
                                     [operator_location] varchar(50), 
                                     [elapsed_time] int,
                                     [rpt_message] varchar(100))

        DECLARE @location_list TABLE ([location_no_str] VARCHAR(20),
                                      [location_no] INT)

        DECLARE @machine_list TABLE ([machine_name] VARCHAR(50))

        DECLARE @sci_central_machine_list TABLE ([machine_name] VARCHAR(50))

    /*  Check Parameters  - Null Date = Today, Null Operator = All  */

        SELECT @rpt_dt = ISNULL(@rpt_dt, GETDATE())
        SELECT @rpt_operator = ISNULL(@rpt_operator,'All')
    
        SELECT @rpt_date = CONVERT(CHAR(10),@rpt_dt,111), @rpt_message = ''

        SELECT @machine_locations = ISNULL(@machine_locations,'')
    
    /*  Parse machine_locations parameter  */

        INSERT INTO @location_list ([location_no_str]) 
        SELECT [item] FROM [dbo].[LFT_SPLIT_DILEMETED_STRING] (@machine_locations, ',')

        DELETE FROM @location_list WHERE [dbo].[FS_isReallyNumeric] ([location_no_str]) = 0

        UPDATE @location_list SET [location_no] = CONVERT(INT,[location_no_str])

        INSERT INTO @machine_list ([machine_name])
            SELECT [machine_name] 
            FROM [dbo].[TX_MACHINE_LOCATION] 
            WHERE [location] IN (SELECT [location_no]
                                 FROM @location_list)

        INSERT INTO @sci_central_machine_list
            SELECT [machine_name] 
            FROM [dbo].[TX_MACHINE_LOCATION] 
            WHERE [location] IN (SELECT [id] 
                                 FROM [dbo].[TR_LOCATION] 
                                 WHERE [description] IN ('Membership','Science Central'))

    /*  Get Raw Data */

        INSERT INTO @coupon_raw_data_table ([coupon_date],[price_type_name],[comp_code_name_short],[comp_code_name],[total_coupons],[order_no],[coupon_operator],
                                            [operator_first],[operator_last],[operator_location],[elapsed_time],[coupon_machine],[rpt_message])
        SELECT [coupon_date], 
               CASE WHEN [price_type_no] = 268 THEN 'Show and Go' ELSE 'Collected' END,
               [comp_code_name_short],
               [comp_code_name],
               1,
               [order_no],
               [coupon_operator], 
               [coupon_operator_first_name],
               [coupon_operator_last_name],
               [coupon_operator_location],
               0,
               [coupon_machine_original],
               @rpt_message
        FROM [LV_RPT_CASHOUT_COUPONS] 
        WHERE [coupon_date] = @rpt_date 

    /*  IF specific machine locations passed to procedure, delete all records from other machines  */
        IF EXISTS (SELECT * FROM @machine_list)
            DELETE FROM @coupon_raw_data_table 
            WHERE [coupon_machine] NOT IN (SELECT [machine_name] FROM @machine_list)
              AND [coupon_machine] NOT IN (SELECT [machine_name] FROM @sci_central_machine_list)

    /*  Delete anything from the table that is in the LTR_CASHOUT_REPORT_EXCLUDE local table  */
    
        DELETE FROM @coupon_raw_data_table 
        WHERE comp_code_name_short IN (SELECT [code_value_short] 
                                       FROM [dbo].[LTR_CASHOUT_REPORT_EXCLUDE] 
                                       WHERE [code_type] = 'Price Type Reason' AND [inactive] = 'N')
       

    /*  Insert Library Passes, Coupons, or Vouchers into the @coupon_table variable  */
        
    IF @type = 'L' BEGIN
        
        IF @rpt_operator = 'All'
            INSERT INTO @coupon_table ([coupon_date],[price_type_name],[comp_code_name_short],[comp_code_name],[total_coupons],[total_transactions],
                                       [coupon_operator],[operator_first],[operator_last],[operator_location],[elapsed_time],[rpt_message])
            SELECT [coupon_date], 
                   [price_type_name],
                   [comp_code_name_short],
                   [comp_code_name],
                   COUNT(*),
                   COUNT(distinct Order_no),
                   [coupon_operator], 
                   [operator_first],
                   [operator_last],
                   [operator_location],
                   0,
                   @rpt_message
            FROM @coupon_raw_data_table 
            WHERE [coupon_date] = @rpt_date 
              AND [comp_code_name_short] LIKE 'LP%'
            GROUP BY [coupon_date], [price_type_name], [comp_code_name_short], [comp_code_name], [coupon_operator], [operator_first], [operator_last], [operator_location]

        ELSE

            INSERT INTO @coupon_table ([coupon_date],[price_type_name],[comp_code_name_short],[comp_code_name],[total_coupons],[total_transactions],
                                       [coupon_operator],[operator_first],[operator_last],[operator_location],[elapsed_time],[rpt_message])
            SELECT [coupon_date],
                   [price_type_name],
                   [comp_code_name_short],
                   [comp_code_name],
                   COUNT(*),
                   COUNT(DISTINCT [order_no]),
                   [coupon_operator], 
                   [operator_first],
                   [operator_last],
                   [operator_location],
                   0,
                   @rpt_message
            FROM @coupon_raw_data_table 
            WHERE [coupon_date] = @rpt_date 
              AND [comp_code_name_short] LIKE 'LP%' 
              AND [coupon_operator] = @rpt_operator
            GROUP BY [coupon_date], [price_type_name], [comp_code_name_short], [comp_code_name], [coupon_operator], [operator_first], [operator_last], [operator_location]

	END ELSE IF @type = 'C' BEGIN
        
        IF @rpt_operator = 'All'
            INSERT INTO @coupon_table ([coupon_date],[price_type_name],[comp_code_name_short],[comp_code_name],[total_coupons],[total_transactions],
                                       [coupon_operator],[operator_first],[operator_last],[operator_location],[elapsed_time],[rpt_message])
            SELECT [coupon_date],
                   [price_type_name],
                   [comp_code_name_short],
                   [comp_code_name],
                   COUNT(*),
                   COUNT(DISTINCT [order_no]), 
                   [coupon_operator], 
                   [operator_first], 
                   [operator_last],
                   [operator_location],
                   0,
                   @rpt_message
            FROM @coupon_raw_data_table 
            WHERE [coupon_date] = @rpt_date 
              AND [comp_code_name_short] NOT LIKE 'LP%' 
              AND [comp_code_name] NOT LIKE '%voucher%' 
            GROUP BY [coupon_date], [price_type_name], [comp_code_name_short], [comp_code_name], [coupon_operator], [operator_first], [operator_last], [operator_location]

        ELSE

            INSERT INTO @coupon_table ([coupon_date],[price_type_name],[comp_code_name_short],[comp_code_name],[total_coupons],[total_transactions],
                                       [coupon_operator],[operator_first],[operator_last],[operator_location],[elapsed_time],[rpt_message])
            SELECT [coupon_date],
                   [price_type_name],
                   [comp_code_name_short],
                   [comp_code_name],
                   COUNT(*),
                   COUNT(DISTINCT [order_no]), 
                   [coupon_operator], 
                   [operator_first],
                   [operator_last],
                   [operator_location],
                   0,
                   @rpt_message
            FROM @coupon_raw_data_table 
            WHERE [coupon_date] = @rpt_date 
            AND [comp_code_name_short] NOT LIKE 'LP%' 
            AND [comp_code_name] NOT LIKE '%voucher%' 
            AND [coupon_operator] = @rpt_operator
            GROUP BY [coupon_date], [price_type_name], [comp_code_name_short], [comp_code_name], [coupon_operator], [operator_first], [operator_last], [operator_location]

    END ELSE IF @type = 'V' BEGIN

        IF @rpt_operator = 'All'
            INSERT INTO @coupon_table ([coupon_date],[price_type_name],[comp_code_name_short],[comp_code_name],[total_coupons],[total_transactions],
                                       [coupon_operator],[operator_first],[operator_last],[operator_location],[elapsed_time],[rpt_message])
            SELECT [coupon_date],
                   [price_type_name],
                   [comp_code_name_short],
                   [comp_code_name],
                   COUNT(*),
                   COUNT(DISTINCT [order_no]),
                   [coupon_operator], 
                   [operator_first],
                   [operator_last],
                   [operator_location],
                   0,
                   @rpt_message
            FROM @coupon_raw_data_table 
            WHERE [coupon_date] = @rpt_date
              AND [comp_code_name] LIKE '%voucher%'
            GROUP BY [coupon_date], [price_type_name], [comp_code_name_short], [comp_code_name], [coupon_operator], [operator_first], [operator_last], [operator_location]

        ELSE

            INSERT INTO @coupon_table ([coupon_date],[price_type_name],[comp_code_name_short],[comp_code_name],[total_coupons],[total_transactions],
                                       [coupon_operator],[operator_first],[operator_last],[operator_location],[elapsed_time],[rpt_message])
            SELECT [coupon_date],
                   [price_type_name],
                   [comp_code_name_short],
                   [comp_code_name],
                   COUNT(*),
                   COUNT(DISTINCT [order_no]),
                   [coupon_operator], 
                   [operator_first],
                   [operator_last],
                   [operator_location],
                   0,
                   @rpt_message
            FROM @coupon_raw_data_table 
            WHERE [coupon_date] = @rpt_date
              AND [comp_code_name] LIKE '%voucher%' 
              AND [coupon_operator] = @rpt_operator
            GROUP BY [coupon_date], [price_type_name], [comp_code_name_short], [comp_code_name], [coupon_operator], [operator_first], [operator_last], [operator_location]

    END
          
    FINISHED:
        
		SELECT  [coupon_date]
               ,[coupon_operator]
               ,ISNULL([operator_first], '') AS 'operator_first'
               ,ISNULL([operator_last], '') AS 'operator_last'
               ,ISNULL([operator_location], '') AS 'operator_location'
               ,ISNULL([price_type_name], '') AS 'price_type_name'
               ,ISNULL([comp_code_name_short], '') AS 'comp_code_name_short'
               ,ISNULL(CASE WHEN PATINDEX('% - %', [comp_code_name]) > 0 THEN RTRIM(LTRIM(SUBSTRING([comp_code_name], PATINDEX('% - %', [comp_code_name]) + 2, LEN([comp_code_name])))) 
                            ELSE [comp_code_name] END, '') AS 'comp_code_name'
               ,ISNULL([total_coupons], 0) AS 'total_coupons'
               ,ISNULL([total_transactions], 0) AS 'nbr_of_passes'
        FROM @coupon_table 
        ORDER BY ISNULL([operator_location], ''), [coupon_operator], [comp_code_name_short]
        
END
GO

GRANT EXECUTE ON [dbo].[LRP_TESSITURA_CASHOUT_COUPONS] TO impusers
GO


EXECUTE [dbo].[LRP_TESSITURA_CASHOUT_COUPONS] '4-17-2021', 'jfarre00', '', 'C'
--EXECUTE [dbo].[LRP_TESSITURA_CASHOUT_COUPONS] @rpt_dt = '1-1-2020', @rpt_operator = 'jsamso00', @machine_locations = '15,16', @type = 'L'
--EXECUTE [dbo].[LRP_TESSITURA_CASHOUT_COUPONS] @rpt_dt = '1-2-2020', @rpt_operator = 'apetro00', @machine_locations = '', @type = 'L'


--EXECUTE [dbo].[LRP_TESSITURA_CASHOUT_COUPONS] @rpt_dt = '4-4-2017', @rpt_operator = 'candre00', @machine_locations = '15,16,17', @type = 'V'
--SELECT DISTINCT(created_by) FROM dbo.T_SUB_LINEITEM WHERE create_dt BETWEEN '11-25-2016' AND '11-26-2016' AND created_by IN (SELECT userid FROM dbo.T_METUSER WHERE location = 'Box Office')
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--THIS IS THE OLD WAY THE PROCEDURE WAS WRITTEN.  HAD TO ADJUST IT SO THAT I COULD USE THE NEW LTR_CASHOUT_REPORT_EXCLUDE table
--USE [impresario]
--GO

--IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_TESSITURA_CASHOUT_COUPONS]') AND type in (N'P', N'PC'))
--    DROP PROCEDURE [dbo].[LRP_TESSITURA_CASHOUT_COUPONS]
--GO

--SET ANSI_NULLS ON
--SET QUOTED_IDENTIFIER ON
--GO

--CREATE PROCEDURE  [dbo].[LRP_TESSITURA_CASHOUT_COUPONS]
--       @rpt_dt DATETIME,
--       @rpt_operator VARCHAR(8),
--	   @type CHAR(1) -- L for Library, C for Coupon, V for Voucher
--AS 	BEGIN

--	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

--	--DECLARE	@cnt_passes	INT

--    DECLARE @coupon_table TABLE ([coupon_date] CHAR(10),
--                                 [coupon_operator] VARCHAR(8),
--                                 [operator_first] VARCHAR(50),
--                                 [operator_last] VARCHAR(50),
--                                 [operator_location] VARCHAR(50),
--                                 [price_type_name] VARCHAR(30),
--                                 [comp_code_name_short] VARCHAR(20),
--                                 [comp_code_name] VARCHAR(30),
--                                 [total_coupons] INT,
--                                 [total_transactions] INT,
--                                 [elapsed_time] INT,
--                                 [rpt_message] VARCHAR(100))

--	INSERT INTO @coupon_table
--		EXEC [dbo].[LP_RPT_CASHOUT_COUPONS] @rpt_dt, @rpt_operator

--    /*  NOTE: Do not put a blank row into the table if it's empty.  This will cause this portion of the report to be suppress if nothing is found.  */

--    IF @type = 'L'
--		SELECT [coupon_date], [coupon_operator], [operator_first], [operator_last], [operator_location], [price_type_name], [comp_code_name_short], 
--               CASE WHEN PATINDEX('% - %', [comp_code_name]) > 0 THEN RTRIM(LTRIM(SUBSTRING([comp_code_name], PATINDEX('% - %', [comp_code_name]) + 2, LEN([comp_code_name])))) ELSE [comp_code_name] END AS [comp_code_name], 
--               [total_coupons], [total_transactions] AS [nbr_of_passes]
--        FROM @coupon_table 
--        WHERE [comp_code_name_short] LIKE 'LP%'
--	ELSE IF @type = 'C'
--        SELECT [coupon_date], [coupon_operator], [operator_first], [operator_last], [operator_location], [price_type_name], [comp_code_name_short], 
--               CASE WHEN PATINDEX('% - %', [comp_code_name]) > 0 THEN RTRIM(LTRIM(SUBSTRING([comp_code_name], PATINDEX('% - %', [comp_code_name]) + 2, LEN([comp_code_name])))) ELSE [comp_code_name] END AS [comp_code_name], 
--             [total_coupons], [total_transactions] AS [nbr_of_passes] 
--        FROM @coupon_table 
--        WHERE [comp_code_name_short] NOT LIKE 'LP%' AND [comp_code_name] NOT LIKE '%voucher%' AND [total_coupons] <> 0
--    ELSE IF @type = 'V'
--        SELECT [coupon_date], [coupon_operator], [operator_first], [operator_last], [operator_location], [price_type_name], [comp_code_name_short], 
--               CASE WHEN PATINDEX('% - %', [comp_code_name]) > 0 THEN RTRIM(LTRIM(SUBSTRING([comp_code_name], PATINDEX('% - %', [comp_code_name]) + 2, LEN([comp_code_name])))) ELSE [comp_code_name] END AS [comp_code_name], 
--               [total_coupons], [total_transactions] AS [nbr_of_passes]
--        FROM @coupon_table 
--        WHERE [comp_code_name_short] LIKE 'voucher%'
		
--    --SELECT	@cnt_passes = COUNT([coupon_date]) FROM @coupon_table
--    --IF @type = 'L'
--	--	BEGIN
--	--		IF (SELECT COUNT([total_coupons]) FROM @coupon_table WHERE [comp_code_name_short] LIKE 'LP%') = 0
--	--			SELECT '' AS [coupon_date], '' AS [coupon_operator], '' AS [operator_first], '' AS [operator_last], '' AS [price_type_name], '' AS [comp_code_name_short], 'No library passes found' AS [comp_code_name], 0 AS [total_coupons], 0 AS [nbr_of_passes]
--	--		ELSE
--	--			SELECT [coupon_date], [coupon_operator], [operator_first], [operator_last], [operator_location], [price_type_name], [comp_code_name_short], 
--	--				CASE
--	--					WHEN PATINDEX('% - %', [comp_code_name]) > 0 THEN RTRIM(LTRIM(SUBSTRING([comp_code_name], PATINDEX('% - %', [comp_code_name]) + 2, LEN([comp_code_name]))))
--	--					ELSE [comp_code_name]
--	--				END AS [comp_code_name], [total_coupons], [total_transactions] AS [nbr_of_passes] FROM @coupon_table WHERE [comp_code_name_short] LIKE 'LP%'
--	--	END

--	--IF @type = 'C'
--	--	BEGIN
--	--		IF (SELECT COUNT([total_coupons]) FROM @coupon_table WHERE [comp_code_name_short] NOT LIKE 'LP%' AND [comp_code_name] NOT LIKE '%voucher%') = 0
--	--			SELECT '' AS [coupon_date], '' AS [coupon_operator], '' AS [operator_first], '' AS [operator_last], '' AS [price_type_name], '' AS [comp_code_name_short], 'No coupons or pass codes found' AS [comp_code_name], 0 AS [total_coupons], 0 AS [nbr_of_passes]
--	--		ELSE
--	--			SELECT [coupon_date], [coupon_operator], [operator_first], [operator_last], [operator_location], [price_type_name], [comp_code_name_short], CASE
--	--					WHEN PATINDEX('% - %', [comp_code_name]) > 0 THEN RTRIM(LTRIM(SUBSTRING([comp_code_name], PATINDEX('% - %', [comp_code_name]) + 2, LEN([comp_code_name]))))
--	--					ELSE [comp_code_name]
--	--				END AS [comp_code_name], [total_coupons], [total_transactions] AS [nbr_of_passes] FROM @coupon_table WHERE [comp_code_name_short] NOT LIKE 'LP%' AND [comp_code_name] NOT LIKE '%voucher%'
--	--	END

--	--IF @type = 'V'
--	--	BEGIN
--	--		IF (SELECT COUNT([total_coupons]) FROM @coupon_table WHERE [comp_code_name_short] NOT LIKE 'LP%' and [comp_code_name_short] LIKE 'voucher%') = 0 
--	--			SELECT '' AS [coupon_date], '' AS [coupon_operator], '' AS [operator_first], '' AS [operator_last], '' AS [price_type_name], '' AS [comp_code_name_short], 'No vouchers found' AS [comp_code_name], 0 AS [total_coupons], 0 AS [nbr_of_passes]
--	--		ELSE
--	--			SELECT [coupon_date], [coupon_operator], [operator_first], [operator_last], [operator_location], [price_type_name], [comp_code_name_short], CASE
--	--					WHEN PATINDEX('% - %', [comp_code_name]) > 0 THEN RTRIM(LTRIM(SUBSTRING([comp_code_name], PATINDEX('% - %', [comp_code_name]) + 2, LEN([comp_code_name]))))
--	--					ELSE [comp_code_name]
--	--				END AS [comp_code_name], [total_coupons], [total_transactions] AS [nbr_of_passes] FROM @coupon_table WHERE [comp_code_name_short] LIKE 'voucher%'
--	--	END


--END
--GO

--GRANT EXECUTE ON [dbo].[LRP_TESSITURA_CASHOUT_COUPONS] TO impusers
--GO

--EXECUTE [dbo].[LRP_TESSITURA_CASHOUT_COUPONS] @rpt_dt = '10-3-2016', @rpt_operator = 'candre01', @type = 'C'