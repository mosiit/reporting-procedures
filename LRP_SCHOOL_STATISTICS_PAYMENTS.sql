USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_SCHOOL_STATISTICS_PAYMENTS]') AND type in (N'P', N'PC')) BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_SCHOOL_STATISTICS_PAYMENTS] AS' 
END
GO

ALTER PROCEDURE [dbo].[LRP_SCHOOL_STATISTICS_PAYMENTS]
        @report_start_dt DATETIME = Null,
        @report_end_dt DATETIME = Null,
        @report_start_dt_2 DATETIME = Null,
        @report_end_dt_2 DATETIME = Null
AS BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

        /*  Procedure Variables  */

        DECLARE @rpt_message VARCHAR(100) = ''
        DECLARE @date_range VARCHAR(20) = 'First Date Range'

        DECLARE @go_live_date CHAR(10) = '2016/05/18'
        DECLARE @special_exhibit_name VARCHAR(50) = ''

        DECLARE @rpt_start_date CHAR(10), @rpt_end_date CHAR(10),
                @his_start_date CHAR(10), @his_end_date CHAR(10),
                @arc_start_date CHAR(10), @arc_end_date CHAR(10)

        DECLARE @school_orders_history TABLE ([order_no] VARCHAR(50))
        DECLARE @school_orders_archive TABLE ([order_no] VARCHAR(50))

        DECLARE @payment_table TABLE ([id_no] INT IDENTITY (1,1), [data_pass] CHAR(1), [date_range] VARCHAR(50), [order_no] INT, [primary_order_record] CHAR(1), 
                                      [trx_seq_no] INT, [trx_no] INT, [payment_no] INT, [payment_dt] DATETIME, [payment_type] VARCHAR(50), 
                                      [payment_method] VARCHAR(100), [payment_amount] DECIMAL(18,2), [payment_check_no] VARCHAR(50), [payment_notes] VARCHAR(1024), 
                                      [visit_count] INT, [report_message] VARCHAR(100))

        DECLARE @min_id_table TABLE ([date_range] VARCHAR(50), [order_no] INT, [id_no] INT)

    /*  If two date ranges are entered, they are processed one at a time.
        After the first range is processed, the code is sent back to this point to start the second date range  */

        BEGIN_PROCESS:  

    /*  Check Parameters  */

        --IF NULLS PASSED TO PRIMARY DATE RANGE, INSERT ONE BLANK RECORD AND JUMP TO THE END
        IF @report_start_dt IS NULL AND @report_end_dt IS NULL BEGIN
            INSERT INTO @payment_table VALUES ('', '', 0, '', 0, 0, 0, GETDATE(), '', '', 0.0, 0, '', 0, '')
            GOTO FINISHED
        END

        IF @report_end_dt < @report_start_dt BEGIN
            SELECT @rpt_message = 'Invalid dates - End Date Before Start date.'
            GOTO FINISHED
        END

        IF @report_start_dt_2 IS NOT NULL AND @report_end_dt_2 IS NOT NULL and @report_end_dt_2 < @report_start_dt_2 BEGIN
            SELECT @rpt_message = 'Invalid dates - End Date Before Start date.'
            GOTO FINISHED
        END

    /*  Get the yyyy/mm/dd version of the start and end date*/
        
        SELECT @rpt_start_date = CONVERT(CHAR(10),@report_start_dt,111),
               @rpt_end_date = CONVERT(CHAR(10),@report_end_dt,111)

    /*  If both start and end date are *BEFORE* Tessitura Go Live, nothing to select
        If both start and end date are on or *AFTER* Tessitura Go Live, leave dates as they are
        If start date is *BEFORE* and end date is on or *AFTER* Tessitura Go Live, change start date to Go Live date  */

        IF @rpt_start_date < @go_live_date AND @rpt_end_date < @go_live_date
            SELECT @his_start_date = '',
                   @his_end_date = ''
        ELSE IF @rpt_start_date > @go_live_date AND @rpt_end_date > @go_live_date
            SELECT @his_start_date = CONVERT(CHAR(10), @report_start_dt,111),
                   @his_end_date = CONVERT(CHAR(10),@report_end_dt,111)
        ELSE IF @rpt_start_date < @go_live_date AND @rpt_end_date >= @go_live_date
           SELECT @his_start_date = @go_live_date,
                  @his_end_date = CONVERT(CHAR(10), @report_end_dt,111)

       IF ISDATE(@his_start_date) = 1 AND ISDATE(@his_end_date) = 1 BEGIN

            INSERT INTO @payment_table ([data_pass], [date_range], [order_no], [primary_order_record], [trx_seq_no], [trx_no], [payment_no], [payment_dt], [payment_type], [payment_method], 
                                        [payment_amount], [payment_check_no], [payment_notes], [visit_count], [report_message])
            SELECT 'A', @date_range, pay.[order_no], 'N', pay.[transaction_Sequence_no], pay.[transaction_no], pay.[payment_no], pay.[payment_dt], pay.[payment_method], 
                   pay.[scholarship_name], pay.[payment_amount], 0, pay.[payment_notes], 0, ''
            FROM [dbo].[LV_SCHOLARSHIP_PAYMENTS] AS pay
            WHERE order_no IN (SELECT [order_no] FROM [dbo].[LT_HISTORY_TICKET] 
                               WHERE [performance_date] BETWEEN @his_start_date AND @his_end_date AND ([performance_type] = 'School Only' OR [price_type_name] LIKE '%school%'))
              AND payment_amount <> 0.00 AND payment_method <> 'Interdepartmental Transfer'
            ORDER BY pay.order_no, pay.payment_no
          
            INSERT INTO @payment_table ([data_pass], [date_range], [order_no], [primary_order_record], [trx_seq_no], [trx_no], [payment_no], [payment_dt], [payment_type], [payment_method], 
                                        [payment_amount], [payment_check_no], [payment_notes], [visit_count], [report_message])
            SELECT 'A', @date_range, pay.order_no, 'N', pay.transaction_sequence_no, pay.transaction_no, pay.[payment_no], pay.payment_dt, pay.payment_type_name, pay.payment_method_name, 
                   pay.payment_amount, pay.payment_check_no, pay.payment_notes, 0, ''
            FROM dbo.LV_ORDER_PAYMENTS AS pay (NOLOCK)
            WHERE order_no IN (SELECT [order_no] FROM [dbo].[LT_HISTORY_TICKET] 
                               WHERE [performance_date] BETWEEN @his_start_date AND @his_end_date AND ([performance_type] = 'School Only' OR [price_type_name] LIKE '%school%'))
              AND payment_amount <> 0.00 AND payment_method_name NOT LIKE '%Scholarship%' AND pay.payment_method_name <> 'Interdepartmental Transfer'
            ORDER BY pay.order_no, pay.payment_no

        END
        
        /*  If a second date range was passed to the report, change the date information, truncate the table and send it back to the beginning to do it all again  */

            IF @date_range = 'First Date Range' and @report_start_dt_2 IS NOT NULL AND @report_end_dt_2 IS NOT NULL BEGIN

                SELECT @report_start_dt = @report_start_dt_2,
                       @report_end_dt = @report_end_dt_2,
                       @date_range = 'Second Date Range'
                              
                GOTO BEGIN_PROCESS

            END

          /*  Fix the data  */

            DELETE FROM @payment_table WHERE [payment_method] LIKE '%Overnight%' OR [payment_type] LIKE '%Overnight%'
            
            UPDATE @payment_table SET [payment_method] = 'Unknown Scholarship' WHERE [payment_type] LIKE '%Scholarship%' AND payment_method = ''

            UPDATE @payment_table SET [payment_method] = [payment_type] WHERE [payment_method] = ''

            UPDATE @payment_table SET [Payment_method] = 'Invalid Scholarship ID' WHERE [payment_method] like 'Invalid Scholarship ID%'

        /*  Set a primary record for each order by identifying the smallest id number for that order  */

            INSERT INTO @min_id_table ([date_range], [order_no], [id_no])
            SELECT [date_range], [order_no], MIN([id_no]) FROM @payment_table GROUP BY [date_range], [order_no]

            UPDATE @payment_table
            SET primary_order_record = 'Y'
            WHERE id_no IN (SELECT id_no FROM @min_id_table)

        /*  Update the visit count on the primary record for each order only so that visitors do not get double-counted  */

            UPDATE @payment_table
            SET [visit_count] = [dbo].[LFS_GET_ORDER_VISIT_COUNT_BY_EXH] ([order_no])
            WHERE primary_order_record = 'Y'

    FINISHED:

        /*  If no data found, add a single record with a message */

            IF NOT EXISTS (SELECT * FROM @payment_table) BEGIN
                IF ISNULL(@rpt_message,'') = '' SELECT @rpt_message = 'No Data Found For Criteria Entered'
                INSERT INTO @payment_table ([report_message]) VALUES (@rpt_message)
            END 

           
        /*  Return final data set for the report to display  */
        
            SELECT [date_range], [order_no], [primary_order_record], [trx_seq_no], [trx_no], [payment_no], [payment_dt], [payment_type], [payment_method], 
                   [payment_amount], [payment_check_no], [payment_notes], [visit_count], [report_message]
            FROM @payment_table
            ORDER BY order_no, primary_order_record DESC
          
    DONE:

END
GO

GRANT EXECUTE ON [dbo].[LRP_SCHOOL_STATISTICS_PAYMENTS] TO [ImpUsers] AS [dbo]
GO

--EXECUTE [dbo].[LRP_SCHOOL_STATISTICS_PAYMENTS] @report_start_dt = '11-1-2016', @report_end_dt = '11-30-2016 23:59:59', @report_start_dt_2 = Null, @report_end_dt_2 = Null
--EXECUTE [dbo].[LRP_SCHOOL_STATISTICS_PAYMENTS] @report_start_dt = NULL, @report_end_dt = NULL, @report_start_dt_2 = Null, @report_end_dt_2 = Null
--EXECUTE [dbo].[LRP_SCHOOL_STATISTICS_PAYMENTS] @report_start_dt = '3-1-2018', @report_end_dt = '3-31-2018', @report_start_dt_2 = '3-1-2017', @report_end_dt_2 = '3-31-2017'
--EXECUTE [dbo].[LRP_SCHOOL_STATISTICS_PAYMENTS] @report_start_dt = '5-1-2016', @report_end_dt = '5-31-2016', @report_start_dt_2 = '5-1-2013', @report_end_dt_2 = '5-31-2013'

