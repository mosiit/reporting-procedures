USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LRP_ATTEND_WEATHER_CONDITIONS]    Script Date: 7/11/2019 10:31:15 AM ******/
DROP PROCEDURE [dbo].[LRP_ATTEND_WEATHER_CONDITIONS]
GO

/****** Object:  StoredProcedure [dbo].[LRP_ATTEND_WEATHER_CONDITIONS]    Script Date: 7/11/2019 10:31:15 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LRP_ATTEND_WEATHER_CONDITIONS]
	@WeekEnding	DATETIME,
	@WhichYear	VARCHAR(4) = 'This' -- 'This' or 'Last'
AS

SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

DECLARE @tbl_weather TABLE
					(
					DateNative		DATETIME,
					DOW			VARCHAR(10),
					Budget			INT,
					TimeString		VARCHAR(7),
					Conditions		VARCHAR(500),
					TempFormatted	VARCHAR(10),
					TempRaw		VARCHAR(10)
					)

DECLARE	@tbl_grossAttend TABLE (which_year VARCHAR(4), history_dt DATETIME, gross_attend INT, day_nbr INT)

DECLARE	@tbl_hilo TABLE (modified_dt VARCHAR(10), low_temp VARCHAR(4), high_temp VARCHAR(4))

DECLARE	@StartDate	DATETIME,
		@EndDate	DATETIME,
		@StartTime	INT = 9,
		@EndTime	INT = 17

IF @WhichYear = 'This'
	BEGIN

		SELECT	@StartDate = cur_week_start_dt,
				@EndDate = cur_week_end_dt
		FROM	[dbo].[LFT_ATTENDANCE_WEEK_DATES] (@WeekEnding, 0, 'Y')

		--SELECT 'debug',@StartDate,@EndDate,@WeekEnding

		INSERT INTO @tbl_grossAttend
			SELECT 'This', history_dt, SUM(visit_count), ROW_NUMBER () OVER(ORDER BY history_dt) FROM LT_HISTORY_VISIT_COUNT WHERE history_dt BETWEEN @StartDate AND @EndDate GROUP BY history_dt

	END

IF @WhichYear = 'Last'
	BEGIN

		SELECT	@StartDate = cur_week_start_dt,
				@EndDate = cur_week_end_dt
		FROM	[dbo].[LFT_ATTENDANCE_WEEK_DATES] (@WeekEnding, 0, 'Y')

		INSERT INTO @tbl_grossAttend
			SELECT 'This', history_dt, SUM(visit_count), ROW_NUMBER () OVER(ORDER BY history_dt) FROM LT_HISTORY_VISIT_COUNT WHERE history_dt BETWEEN @StartDate AND @EndDate GROUP BY history_dt

		SELECT	@StartDate = prv_week_start_dt,
				@EndDate = prv_week_end_dt
		FROM	[dbo].[LFT_ATTENDANCE_WEEK_DATES] (@WeekEnding, 0, 'Y')

		INSERT INTO @tbl_grossAttend
			SELECT 'Last', history_dt, SUM(visit_count), ROW_NUMBER () OVER(ORDER BY history_dt) FROM LT_HISTORY_VISIT_COUNT WHERE history_dt BETWEEN @StartDate AND @EndDate GROUP BY history_dt

	END

INSERT INTO @tbl_weather (DateNative, DOW, Budget, TimeString, Conditions, TempFormatted, TempRaw)
	SELECT	ModifiedDate,
			DATENAME(WEEKDAY, ModifiedDate) AS 'Day of Week',
			[dbo].[LFS_ATTEND_BUDGET_DATA] (DATEADD(dd, DATEDIFF(dd, 0, ModifiedDate), 0)) AS 'Budget Num',	
			LTRIM(RTRIM(RIGHT(CONVERT (VARCHAR, ModifiedDate, 0), 7)))  AS 'Time',
			REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
			REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
			REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
			REPLACE(REPLACE(SUBSTRING (title, 1, PATINDEX ('%and [-0-9]%', title) - 2) COLLATE latin1_general_100_bin2,
			CHAR(1), ''),CHAR(2), ''),CHAR(3), ''),CHAR(4), ''),CHAR(5), ''),CHAR(6), ''),CHAR(7), ''),CHAR(8), ''),CHAR(9), ''),CHAR(10), ''),
			CHAR(11), ''),CHAR(12), ''),CHAR(13), ''),CHAR(14), ''),CHAR(15), ''),CHAR(16), ''),CHAR(17), ''),CHAR(18), ''),CHAR(19), ''),CHAR(20), ''),
			CHAR(21), ''),CHAR(22), ''),CHAR(23), ''),CHAR(24), ''),CHAR(25), ''),CHAR(26), ''),CHAR(27), ''),CHAR(28), ''),CHAR(29), ''),CHAR(30), ''),
			CHAR(31), ''), CHAR(0) , '') AS 'Weather',
			SUBSTRING (SUBSTRING (title, PATINDEX ('%[-0-9]%', title), LEN (title)), 1, PATINDEX ('% F %', SUBSTRING (title, PATINDEX ('%[-0-9]%', title) - 2, LEN (title)))) AS 'Temp',
			SUBSTRING (SUBSTRING (title, PATINDEX ('%[-0-9]%', title), LEN (title)), 1, PATINDEX ('% F %', SUBSTRING (title, PATINDEX ('%[-0-9]%', title), LEN (title)))) AS 'Temp Number'
	FROM	[dbo].[LT_WeatherBostonItemRSS]
	WHERE	ModifiedDate BETWEEN @StartDate AND @EndDate
	AND		DATEPART(hh, ModifiedDate) BETWEEN @StartTime AND @EndTime

--SELECT 'debug', * FROM @tbl_weather

--INSERT INTO @tbl_grossAttend
--	SELECT history_dt, SUM(visit_count) FROM LT_HISTORY_VISIT_COUNT WHERE history_dt BETWEEN @StartDate AND @EndDate GROUP BY history_dt

--SELECT 'debug', * FROM @tbl_grossAttend

INSERT INTO @tbl_hilo
	SELECT	CONVERT(VARCHAR(10), ModifiedDate, 101),
			MIN(SUBSTRING (SUBSTRING (title, PATINDEX ('%[-0-9]%', title), LEN (title)), 1, PATINDEX ('% F %', SUBSTRING (title, PATINDEX ('%[-0-9]%', title), LEN (title))))),
			MAX(SUBSTRING (SUBSTRING (title, PATINDEX ('%[-0-9]%', title), LEN (title)), 1, PATINDEX ('% F %', SUBSTRING (title, PATINDEX ('%[-0-9]%', title), LEN (title)))))
	FROM	[dbo].[LT_WeatherBostonItemRSS]
	WHERE	ModifiedDate BETWEEN @StartDate AND @EndDate
	GROUP BY CONVERT(VARCHAR(10), ModifiedDate, 101)

--SELECT 'debug', * FROM @tbl_hilo

IF @WhichYear = 'This'
	SELECT	CAST(w.DateNative AS DATE) AS DateNative, 
			DATEPART(YEAR, w.DateNative) AS 'Year',
			w.DOW AS [DayofWeek],
			w.Budget,
			ga.gross_attend AS GrossAttendance,
			ga.gross_attend AS GrossAttendanceThisYear,
			CASE 
				WHEN DATEPART(hh, w.DateNative) < 12 THEN CONVERT(VARCHAR(2), DATEPART(hh, w.DateNative)) + ' AM'
				WHEN DATEPART(hh, w.DateNative) = 12 THEN CONVERT(VARCHAR(2), DATEPART(hh, w.DateNative)) + ' PM'
				WHEN DATEPART(hh, w.DateNative) > 12 THEN CONVERT(VARCHAR(2), DATEPART(hh, w.DateNative) - 12) + ' PM'
			END AS TimeNative,
			--w.TimeString,
			ROW_NUMBER() OVER(PARTITION BY FORMAT (w.DateNative, 'd', 'en-US') ORDER BY DATEPART(hh, w.TimeString)) AS TimeOrder,
			wm.[Conditions] AS Conditions,
			wm.[Icon] AS Icon,
			LTRIM(RTRIM(hl.low_temp)) + '/' + LTRIM(RTRIM(hl.high_temp)) AS 'LowHighTemp'
	FROM	@tbl_weather w
	JOIN	[dbo].[LT_WEATHER_MAPPING] wm
	ON		w.Conditions = wm.[Conditions]
	LEFT JOIN	@tbl_grossAttend ga
	ON		CAST(ga.history_dt AS DATE) = CAST(w.DateNative AS DATE) AND @WhichYear = 'This'
	LEFT JOIN @tbl_hilo hl
	ON		hl.modified_dt = CONVERT(VARCHAR(10), w.DateNative, 101)
	WHERE	wm.[Conditions] IS NOT NULL
	ORDER BY w.DateNative, TimeOrder

--SELECT	'debug', CAST(gal.history_dt AS DATE), gal.gross_attend, gal.which_year
--FROM	@tbl_grossAttend gal
--WHERE	gal.which_year = 'This'

IF @WhichYear = 'Last'
	SELECT	CAST(w.DateNative AS DATE) AS DateNative, 
			DATEPART(YEAR, w.DateNative) AS 'Year',
			w.DOW AS [DayofWeek],
			w.Budget,
			ga.gross_attend AS GrossAttendance,
			gal.gross_attend AS GrossAttendanceThisYear,
			CAST(w.DateNative AS DATE),
			CASE 
				WHEN DATEPART(hh, w.DateNative) < 12 THEN CONVERT(VARCHAR(2), DATEPART(hh, w.DateNative)) + ' AM'
				WHEN DATEPART(hh, w.DateNative) = 12 THEN CONVERT(VARCHAR(2), DATEPART(hh, w.DateNative)) + ' PM'
				WHEN DATEPART(hh, w.DateNative) > 12 THEN CONVERT(VARCHAR(2), DATEPART(hh, w.DateNative) - 12) + ' PM'
			END AS TimeNative,
			--w.TimeString,
			ROW_NUMBER() OVER(PARTITION BY FORMAT (w.DateNative, 'd', 'en-US') ORDER BY DATEPART(hh, w.TimeString)) AS TimeOrder,
			wm.[Conditions] AS Conditions,
			wm.[Icon] AS Icon,
			LTRIM(RTRIM(hl.low_temp)) + '/' + LTRIM(RTRIM(hl.high_temp)) AS 'LowHighTemp'
	FROM	@tbl_weather w
	JOIN	[dbo].[LT_WEATHER_MAPPING] wm
	ON		w.Conditions = wm.[Conditions]
	LEFT JOIN	@tbl_grossAttend ga
	ON		CAST(ga.history_dt AS DATE) = CAST(w.DateNative AS DATE) AND ga.which_year = 'Last'
	LEFT JOIN	@tbl_grossAttend gal
	ON		(gal.day_nbr = ga.day_nbr) AND gal.which_year = 'This'
	LEFT JOIN @tbl_hilo hl
	ON		hl.modified_dt = CONVERT(VARCHAR(10), w.DateNative, 101)
	WHERE	wm.[Conditions] IS NOT NULL
	ORDER BY w.DateNative, TimeOrder

-- Code to clean all non-printable characters courtesy of URL:  https://stackoverflow.com/questions/43148767/sql-server-remove-all-non-printable-ascii-characters
GO

GRANT EXECUTE ON [dbo].[LRP_ATTEND_WEATHER_CONDITIONS] TO ImpUsers
GO