USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- CHART OF ACCOUNTS Exception reporting on T_FUND
-- DSJ 6/15/2016
-- MES 01/20/2021 Updated per SCTASK0001778
--                Changed code to update each exception individually rather than in one big case statement
--                This will make it much easier to add and/or remove exceptions from the report when needed.
ALTER PROCEDURE [dbo].[LRP_EXCEPTION_COA_T_FUND]
AS BEGIN

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
    SET NOCOUNT ON

    /*  Procedure Variables  */

        DECLARE @results TABLE ([fund_no] INT, 
                                [description] VARCHAR (30), 
                                [nonrestricted_income_gl_no] VARCHAR (50),
                                [restricted_income_gl_no] VARCHAR (50),
                                [current_rec_gl_no] VARCHAR (50),
                                [future_rec_gl_no] VARCHAR (50),
                                [written_off_gl_no] VARCHAR (50),
                                [inactive] CHAR (1), 
                                [created_by] VARCHAR (10), 
                                [create_dt] DATETIME, 
                                [last_updated_by] VARCHAR (10), 
                                [last_update_dt] DATETIME,
                                [reason] VARCHAR (350))

    /*  Get Fund Records  */   

        INSERT INTO @results ([fund_no],[description],[nonrestricted_income_gl_no],[restricted_income_gl_no], [current_rec_gl_no],[future_rec_gl_no],[written_off_gl_no],
                              [inactive],[created_by],[create_dt],[last_updated_by],[last_update_dt],[reason])   
        SELECT  [fund_no], 
                [description], 
                [nonrestricted_income_gl_no],
                [restricted_income_gl_no],
                [current_rec_gl_no],
                [future_rec_gl_no],
                [written_off_gl_no],
                [inactive], 
                [created_by], 
                [create_dt], 
                [last_updated_by], 
                [last_update_dt],
                ''
        FROM [dbo].[T_FUND]
        WHERE LEN([nonrestricted_income_gl_no]) = 20 
          AND SUBSTRING([nonrestricted_income_gl_no], 20, 1) <> '9'
          AND CAST([last_update_dt] AS DATE) >= '2020-07-24'

    /*  Identify Exceptions One at a Time  */        

        UPDATE @results
        SET [reason] = '(SUBSTRING(nonrestricted_income_gl_no, 15, 4) = ''0000'' AND SUBSTRING(nonrestricted_income_gl_no, 5,4) IN (''4010'', ''4020'', ''4030'') AND substring(restricted_income_gl_no, 15, 6) NOT IN (''6999-4'' , ''5343-4'') AND DATEDIFF(d,''2016-05-17'',create_dt) > 0)'
        WHERE SUBSTRING([nonrestricted_income_gl_no],15,4) = '0000' 
          AND SUBSTRING([nonrestricted_income_gl_no],5,4) IN ('4010', '4020', '4030')
	      AND SUBSTRING([restricted_income_gl_no],15, 6) NOT IN ('6999-4','5343-4') 
          AND DATEDIFF(d,'2016-05-17',create_dt) > 0


        UPDATE @results
        SET [reason] = '(SUBSTRING(nonrestricted_income_gl_no, 15, 4) = ''0000'' AND SUBSTRING(nonrestricted_income_gl_no, 5,4) IN (''4010'', ''4020'', ''4030'') AND substring(current_rec_gl_no, 15 , 6) NOT IN (''6999-4'' , ''5343-4'') AND DATEDIFF(d,''2016-05-17'',create_dt) > 0)'
        WHERE SUBSTRING([nonrestricted_income_gl_no],15,4) = '0000' 
          --AND SUBSTRING([nonrestricted_income_gl_no],5,4) IN ('4010', '4020', '4030') 
          AND SUBSTRING([current_rec_gl_no],15,6) NOT IN ('6999-4','5343-4') 
          AND DATEDIFF(d,'2016-05-17',[create_dt]) > 0


        UPDATE @results
        SET [reason] = '(substring(nonrestricted_income_gl_no, 15, 4) = ''0000'' AND SUBSTRING(nonrestricted_income_gl_no, 5,4) IN (''4010'', ''4020'', ''4030'') AND substing(future_rec_gl_no, 15, 6) NOT IN (''6999-4'' , ''5343-4'') AND DATEDIFF(d,''2016-05-17'',create_dt) > 0)'
        WHERE (SUBSTRING([nonrestricted_income_gl_no],15, 4) = '0000' 
          --AND SUBSTRING([nonrestricted_income_gl_no],5,4) IN ('4010', '4020', '4030') 
          AND SUBSTRING([future_rec_gl_no],15,6) NOT IN ('6999-4','5343-4') 
          AND DATEDIFF(d,'2016-05-17',create_dt) > 0)


        UPDATE @results
        SET [reason] = '(substring(nonrestricted_income_gl_no, 15, 4) = ''0000'' AND SUBSTRING(nonrestricted_income_gl_no, 5,4) IN (''4010'', ''4020'', ''4030'') AND substring(written_off_gl_no, 15, 6) NOT IN (''6999-4'' , ''5343-4'') AND DATEDIFF(d,''2016-05-17'',create_dt) > 0)'
        WHERE SUBSTRING([nonrestricted_income_gl_no],15,4) = '0000' 
          AND SUBSTRING([nonrestricted_income_gl_no],5,4) IN ('4010', '4020', '4030') 
          AND SUBSTRING([written_off_gl_no],15,6) NOT IN ('6999-4','5343-4') 
          AND DATEDIFF(d,'2016-05-17',[create_dt]) > 0


        UPDATE @results
        SET [reason] = '(SUBSTRING(nonrestricted_income_gl_no,15,4) in (''0008'', ''0009'') AND (nonrestricted_income_gl_no <> restricted_income_gl_no OR nonrestricted_income_gl_no <> current_rec_gl_no OR nonrestricted_income_gl_no <> future_rec_gl_no OR nonrestricted_income_gl_no <> written_off_gl_no))'
        WHERE SUBSTRING([nonrestricted_income_gl_no],15,4) in ('0008', '0009') 
          AND (   [nonrestricted_income_gl_no] <> [restricted_income_gl_no]
               OR [nonrestricted_income_gl_no] <> [current_rec_gl_no]
               OR [nonrestricted_income_gl_no] <> [future_rec_gl_no] 
               OR [nonrestricted_income_gl_no] <> [written_off_gl_no])


        UPDATE @results
        SET [reason] = '(SUBSTRING(nonrestricted_income_gl_no, 15, 3) in (''012'', ''013'', ''014'', ''015'', ''016'', ''017'', ''018'', ''019'') and substring(nonrestricted_income_gl_no, 20, 1) <> ''2'')'
        WHERE SUBSTRING([nonrestricted_income_gl_no],15,3) IN ('012', '013', '014', '015', '016', '017', '018', '019') 
          AND SUBSTRING([nonrestricted_income_gl_no],20,1) <> '2'

	    
        UPDATE @results
        SET [reason] = '(SUBSTRING(nonrestricted_income_gl_no, 15, 3) in (''012'', ''013'', ''014'', ''015'', ''016'', ''017'', ''018'', ''019'') AND substring(restricted_income_gl_no, 15,6) <> ''6997-4'')'
        WHERE SUBSTRING([nonrestricted_income_gl_no],15,3) IN ('012', '013', '014', '015', '016', '017', '018', '019') 
          AND SUBSTRING([restricted_income_gl_no],15,6) <> '6997-4'


        UPDATE @results
        SET [reason] = '(SUBSTRING(nonrestricted_income_gl_no, 15, 3) in (''012'', ''013'', ''014'', ''015'', ''016'', ''017'', ''018'', ''019'') AND current_rec_gl_no <> ''001-1298-0000-6997-4'')'
        WHERE SUBSTRING([nonrestricted_income_gl_no],15,3) IN ('012', '013', '014', '015', '016', '017', '018', '019') 
          AND [current_rec_gl_no] <> '001-1298-0000-6997-4'


	    UPDATE @results
        SET [reason] = '(SUBSTRING(nonrestricted_income_gl_no, 15, 3) in (''012'', ''013'', ''014'', ''015'', ''016'', ''017'', ''018'', ''019'') AND future_rec_gl_no <> ''001-1510-0000-6997-4'')'
        WHERE SUBSTRING([nonrestricted_income_gl_no],15,3) IN ('012', '013', '014', '015', '016', '017', '018', '019') 
          AND [future_rec_gl_no] <> '001-1510-0000-6997-4'
        

        UPDATE @results
        SET [reason] = '(SUBSTRING(nonrestricted_income_gl_no, 15, 3) in (''012'', ''013'', ''014'', ''015'', ''016'', ''017'', ''018'', ''019'') AND substring(written_off_gl_no, 15,6) <> ''6997-4'')'
        WHERE SUBSTRING([nonrestricted_income_gl_no],15,3) IN ('012', '013', '014', '015', '016', '017', '018', '019') 
          AND SUBSTRING([written_off_gl_no],15,6) <> '6997-4'

        --UPDATE @results
        --SET [reason] = '(substring(nonrestricted_income_gl_no, 15, 1) = ''1'' AND fund_no <> 2302)'
        --WHERE SUBSTRING([nonrestricted_income_gl_no],15,1) = '1' 
        --  AND [fund_no] <> 2302
        
        UPDATE @results
        SET [reason] = '(substring(nonrestricted_income_gl_no, 15, 1) = ''2'' AND substring(nonrestricted_income_gl_no, 20, 1) <> 1)'
        WHERE SUBSTRING([nonrestricted_income_gl_no],15,1) = '2' 
          AND SUBSTRING([nonrestricted_income_gl_no],20,1) <> 1


        UPDATE @results
        SET [reason] = '(substring(nonrestricted_income_gl_no, 15, 1) = ''2'' AND substring(restricted_income_gl_no, 10, 11) <> ''0000-6999-4'')'
        WHERE SUBSTRING([nonrestricted_income_gl_no],15,1) = '2' 
          AND SUBSTRING([restricted_income_gl_no],10,11) <> '0000-6999-4'


        UPDATE @results
        SET [reason] = '(substring(nonrestricted_income_gl_no, 15, 1) = ''2'' AND current_rec_gl_no <> ''001-1298-0000-6999-4'')'
        WHERE SUBSTRING([nonrestricted_income_gl_no],15,1) = '2' 
          AND [current_rec_gl_no] <> '001-1298-0000-6999-4'


        UPDATE @results
        SET [reason] = '(substring(nonrestricted_income_gl_no, 15, 1) = ''2'' AND future_rec_gl_no <> ''001-1510-0000-6999-4'')'
        WHERE SUBSTRING([nonrestricted_income_gl_no],15,1) = '2' 
          AND [future_rec_gl_no] <> '001-1510-0000-6999-4'


        UPDATE @results
        SET [reason] = '(substring(nonrestricted_income_gl_no, 15, 1) = ''2'' AND substring(written_off_gl_no, 10, 11) <> ''0000-6999-4'')'
        WHERE SUBSTRING([nonrestricted_income_gl_no],15,1) = '2' 
          AND SUBSTRING([written_off_gl_no],10,11) <> '0000-6999-4'


        UPDATE @results
        SET [reason] = '(substring(nonrestricted_income_gl_no, 15, 1) in (''3'', ''4'') AND (nonrestricted_income_gl_no <> restricted_income_gl_no OR nonrestricted_income_gl_no  <> future_rec_gl_no OR nonrestricted_income_gl_no  <> current_rec_gl_no OR nonrestricted_income_gl_no <> written_off_gl_no))'
        WHERE SUBSTRING( [nonrestricted_income_gl_no],15,1) in ('3', '4') 
          AND (   [nonrestricted_income_gl_no] <> [restricted_income_gl_no] 
               OR [nonrestricted_income_gl_no]  <> [future_rec_gl_no] 
               OR [nonrestricted_income_gl_no]  <> [current_rec_gl_no] 
               OR [nonrestricted_income_gl_no] <> [written_off_gl_no])


        UPDATE @results
        SET [reason] = '(SUBSTRING(nonrestricted_income_gl_no, 15, 1) in (''5'', ''6'', ''7'') AND DATEDIFF(d,''2016-05-17'',create_dt) > 0 AND (substring(nonrestricted_income_gl_no, 10, 4) <> ''0000'' OR substring(nonrestricted_income_gl_no, 20, 1) <> ''4''))'
        WHERE SUBSTRING([nonrestricted_income_gl_no],15,1) in ('5', '6', '7') 
          AND DATEDIFF(d,'2016-05-17',[create_dt]) > 0
          AND (   SUBSTRING([nonrestricted_income_gl_no],10,4) <> '0000' 
               OR SUBSTRING([nonrestricted_income_gl_no],20,1) <> '4')


        UPDATE @results
        SET [reason] = '(substring(nonrestricted_income_gl_no, 15, 1) in (''5'', ''6'', ''7'', ''8'', ''9'') AND nonrestricted_income_gl_no <>restricted_income_gl_no)'
        WHERE SUBSTRING([nonrestricted_income_gl_no],15,1) IN ('5', '6', '7', '8', '9') 
          AND [nonrestricted_income_gl_no] <> [restricted_income_gl_no]


        UPDATE @results
        SET [reason] = '(substring(nonrestricted_income_gl_no, 15, 1) in (''5'',''6'',''7'') AND fund_no NOT IN (1359,1383,127) AND (substring(current_rec_gl_no, 1, 13) <> ''001-1298-0000'' OR substring(current_rec_gl_no, 15, 4) <> substring(nonrestricted_income_gl_no, 15, 4) OR SUBSTRING(current_rec_gl_no, 20, 1) <> ''4''))'
        WHERE SUBSTRING([nonrestricted_income_gl_no],15,1) IN ('5','6','7') 
          AND (   SUBSTRING([current_rec_gl_no],1,13) <> '001-1298-0000' 
               OR SUBSTRING([current_rec_gl_no],15,4) <> SUBSTRING([nonrestricted_income_gl_no],15,4) 
               OR SUBSTRING([current_rec_gl_no],20,1) <> '4')


        UPDATE @results
        SET [reason] = '(substring(nonrestricted_income_gl_no, 15, 1) in (''5'', ''6'',''7'') AND DATEDIFF(d,''2016-05-17'',create_dt) > 0 AND (substring(future_rec_gl_no, 1, 13) <> ''001-1510-0000'' OR substring(future_rec_gl_no, 15, 4) <> substring(nonrestricted_income_gl_no, 15, 4) OR substring(future_rec_gl_no, 20, 1) <> ''4''))'
        WHERE SUBSTRING([nonrestricted_income_gl_no],15,1) IN ('5', '6','7') 
          AND DATEDIFF(d,'2016-05-17',[create_dt]) > 0
          AND (   SUBSTRING([future_rec_gl_no],1,13) <> '001-1510-0000' 
               OR SUBSTRING([future_rec_gl_no],15,4) <> SUBSTRING([nonrestricted_income_gl_no],15,4) 
               OR SUBSTRING([future_rec_gl_no],20,1) <> '4')


        UPDATE @results
        SET [reason] = '(substring(nonrestricted_income_gl_no, 15, 1) in (''5'', ''6'', ''7'', ''8'', ''9'') AND written_off_gl_no <> nonrestricted_income_gl_no)'
        WHERE SUBSTRING([nonrestricted_income_gl_no],15,1) IN ('5', '6', '7', '8', '9') 
          AND [written_off_gl_no] <> [nonrestricted_income_gl_no]


        UPDATE @results
        SET [reason] = '(SUBSTRING(nonrestricted_income_gl_no, 15, 1) in (''8'', ''9'') AND DATEDIFF(d,''2016-05-17'',create_dt) > 0 AND (substring(nonrestricted_income_gl_no, 10, 4) <> ''0000'' OR substring(nonrestricted_income_gl_no, 20, 1) <> ''6''))'
        WHERE SUBSTRING([nonrestricted_income_gl_no],15,1) IN ('8', '9') 
          AND DATEDIFF(d,'2016-05-17',[create_dt]) > 0 
          AND (   SUBSTRING([nonrestricted_income_gl_no],10,4) <> '0000' 
               OR SUBSTRING([nonrestricted_income_gl_no],20,1) <> '6')


        UPDATE @results
        SET [reason] = '(SUBSTRING(nonrestricted_income_gl_no, 15, 1) in (''8'',''9'') AND DATEDIFF(d,''2016-05-17'',create_dt) > 0 AND (substring(current_rec_gl_no, 1, 13) <> ''001-1298-0000'' OR substring(current_rec_gl_no, 15, 4) <> substring(nonrestricted_income_gl_no, 15, 4) OR substring(current_rec_gl_no, 20, 1) <> ''6''))'
        WHERE SUBSTRING([nonrestricted_income_gl_no],15,1) in ('8','9') 
          AND DATEDIFF(d,'2016-05-17',[create_dt]) > 0
          AND (   SUBSTRING([current_rec_gl_no],1,13) <> '001-1298-0000' 
               OR SUBSTRING([current_rec_gl_no],15,4) <> SUBSTRING([nonrestricted_income_gl_no],15,4) 
               OR SUBSTRING([current_rec_gl_no],20,1) <> '6')


        UPDATE @results
        SET [reason] = '(substring(nonrestricted_income_gl_no, 15, 1) in (''8'', ''9'') AND DATEDIFF(d,''2016-05-17'',create_dt) > 0 AND (substring(future_rec_gl_no, 1, 13) <> ''001-1510-0000'' OR substring(future_rec_gl_no, 15, 4) <> substring(nonrestricted_income_gl_no, 15, 4) OR SUBSTRING(future_rec_gl_no, 20, 1) <> ''6''))'
        WHERE SUBSTRING([nonrestricted_income_gl_no],15,1) IN ('8', '9') 
          AND DATEDIFF(d,'2016-05-17',[create_dt]) > 0 
          AND (   SUBSTRING([future_rec_gl_no],1,13) <> '001-1510-0000' 
               OR SUBSTRING([future_rec_gl_no],15,4) <> SUBSTRING([nonrestricted_income_gl_no],15,4) 
               OR SUBSTRING([future_rec_gl_no],20,1) <> '6')


        UPDATE @results
        SET [reason] = '(fund_no not in (select fund_no from LTR_FUND_DETAIL))'
        WHERE [fund_no] NOT IN (SELECT [fund_no] 
                                FROM [dbo].[LTR_FUND_DETAIL])


        UPDATE @results
        SET [reason] = '(fund_no not in (select fund_no from TX_CAMP_FUND))'
        WHERE [fund_no] NOT IN (SELECT [fund_no] 
                                FROM [dbo].[TX_CAMP_FUND]) 


    /*  Delete records with no exception  */

        DELETE FROM @results 
        WHERE [reason] = ''

    FINISHED:

        /*  Final Data Set  */

            SELECT [fund_no],
                   [description],
                   [inactive],
                   [created_by],
                   [create_dt],
                   [last_updated_by],
                   [last_update_dt],
                   [reason]
            FROM @results

END
GO

 --EXECUTE [dbo].[LRP_EXCEPTION_COA_LTR_FUND_DETAIL]





