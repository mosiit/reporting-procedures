USE impresario;
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_ATTEND_WEEKLY_CAPACITY]') AND type in (N'P', N'PC')) BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_ATTEND_WEEKLY_CAPACITY] AS';
    EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LRP_ATTEND_WEEKLY_CAPACITY] TO impusers';
END;
GO

ALTER PROCEDURE [dbo].[LRP_ATTEND_WEEKLY_CAPACITY]
        @week_ending DATETIME = NULL
WITH RECOMPILE AS BEGIN

    SET NOCOUNT ON;
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

    /* Procedure Variables  */

        DECLARE @fiscal_year INT = 0
        DECLARE @week_start_dt DATETIME, @week_end_dt DATETIME;
        DECLARE @month_start_dt DATETIME, @month_end_dt DATETIME;

        DECLARE @prev_fiscal_year INT = 0
        DECLARE @prev_week_start_dt DATETIME, @prev_week_end_dt DATETIME; 
        DECLARE @prev_month_start_dt DATETIME, @prev_month_end_dt DATETIME;

        --These are the title numbers for the various venues
        DECLARE @gross_attend_no INT = 0,           @exhibit_halls_no INT = 27,         @special_exhibitions_no INT = 37,   
                @butterfly_garden_no INT = 157,     @mugar_omni_theater_no INT = 161,   @4d_theater_no INT = 173,
                @hayden_planetarium_no INT = 1132,  @thrill_ride_no INT = 1343,         @entertainment_no INT = 25,
                @adult_offering_no INT = 5542;
        
        DECLARE @entertainment_title_no INT = 25;       --Assigned an invenyory number not being used as a place holder
                                                        --this number is not used anywhere else but the Attendance Reports

        --Set this to Y to separate out school shows from public shows in Omni and Planetarium
        --Set this to N to count school shows in same sections as the public shows in Omni and Planetarium
        DECLARE @separate_School_Sections CHAR(1) = 'N';

    /*  Temporary Tables  */
    
        IF OBJECT_ID('tempdb..#capacity_raw_data') is not null DROP TABLE [#capacity_raw_data];

        CREATE TABLE [#capacity_raw_data] ([week_ending] DATETIME NULL,
                                           [perf_no] INT NOT NULL DEFAULT(0),
                                           [zone_no] INT NOT NULL DEFAULT(0),
                                           [perf_dt] DATETIME NULL,
                                           [perf_type_no] INT NOT NULL DEFAULT(0),
                                           [perf_type_name] VARCHAR(30) NOT NULL DEFAULT(''),
                                           [title_no] INT NOT NULL DEFAULT(0),
                                           [title_name] VARCHAR(30) NOT NULL DEFAULT(''),
                                           [prod_no] INT NOT NULL DEFAULT(0),
                                           [prod_name] VARCHAR(30) NOT NULL DEFAULT(''),
                                           [prod_name_long] VARCHAR(150) NOT NULL DEFAULT(''),
                                           [capacity_total] INT NOT NULL DEFAULT(0),
                                           [capacity_held] INT NOT NULL DEFAULT(0),
                                           [capacity_sellable] INT NOT NULL DEFAULT(0),
                                           [capacity_sold] INT NOT NULL DEFAULT(0),
                                           [capacity_available] INT NOT NULL DEFAULT(0),
                                           [capacity_percent_sold] DECIMAL(18,4) NOT NULL DEFAULT(0.0));

        IF OBJECT_ID('tempdb..#final_table') is not null DROP TABLE [#final_table];

        CREATE TABLE [#final_table] ([week_ending] DATETIME NOT NULL,
                                     [title_order] CHAR(1) NOT NULL DEFAULT ('Z'),
                                     [title_no] INT NOT NULL DEFAULT(0),
                                     [title_name] VARCHAR(30) NOT NULL DEFAULT(''),
                                     [title_color] VARCHAR(30) NOT NULL DEFAULT('LightSteelBlue'),
                                     [perf_type_name] VARCHAR(30) NOT NULL DEFAULT(''),
                                     [prod_no] INT NOT NULL DEFAULT(0),
                                     [prod_name] VARCHAR(30) NOT NULL DEFAULT(''),
                                     [total] DECIMAL(18,4) NOT NULL DEFAULT(0.0),
                                     [sold] DECIMAL(18,4) NOT NULL DEFAULT(0.0),
                                     [sold_seats] DECIMAL(18,4) NOT NULL DEFAULT(0.0),
                                     [sold_history] DECIMAL(18,4) NOT NULL DEFAULT(0.0),
                                     [percent_of capacity] DECIMAL(18,4) NOT NULL DEFAULT(0.0));

    /*  Check Parameters  */

        SELECT @week_ending = CAST(ISNULL(@week_ending,GETDATE()) AS DATE)

    /*  Get date values from LFT_ATTENDANCE_WEEK_DATES function  */
        
        SELECT @fiscal_year = [cur_fiscal_year], 
               @week_start_dt = [cur_week_start_dt], 
               @week_end_dt = [cur_week_end_dt], 
               @month_start_dt = [cur_month_start_dt], 
               @month_end_dt = [cur_month_end_dt], 
               @prev_fiscal_year = [prv_fiscal], 
               @prev_week_start_dt = [prv_week_start_dt], 
               @prev_week_end_dt = [prv_week_end_dt], 
               @prev_month_start_dt = [prv_month_start_dt], 
               @prev_month_end_dt = [prv_month_end_dt]
        FROM [dbo].[LFT_ATTENDANCE_WEEK_DATES] (@week_ending, 0, 'Y');
       

    /*  Get Raw Data  */       

        INSERT INTO [#capacity_raw_data] ([week_ending], [perf_no], [zone_no], [perf_dt], [perf_type_no], [perf_type_name], [title_no], 
                                          [title_name], [prod_no], [prod_name], [prod_name_long], [capacity_total], [capacity_held], 
                                          [capacity_sellable], [capacity_sold], [capacity_available])
        SELECT CASE WHEN prf.[performance_dt] BETWEEN @prev_week_start_dt AND @prev_week_end_dt THEN @prev_week_end_dt
                    ELSE @week_end_dt END,
               prf.[performance_no], 
               prf.[performance_zone],
               prf.[performance_dt],
               prf.[performance_type],
               prf.[performance_type_name],
               prf.[title_no],
               prf.[title_name],
               prf.[production_no],
               prf.[production_name],
               LEFT(prf.[production_name_long],150),
               ISNULL([dbo].[LF_GetCapacity] (prf.performance_no, prf.[performance_zone], 'total'), 0), 
               ISNULL([dbo].[LF_GetCapacity] (prf.performance_no, prf.[performance_zone], 'held'), 0),
               ISNULL([dbo].[LF_GetCapacity] (prf.performance_no, prf.[performance_zone], 'capacity'), 0),
               ISNULL([dbo].[LF_GetCapacity] (prf.performance_no, prf.[performance_zone], 'sold'), 0),
               ISNULL([dbo].[LF_GetCapacity] (prf.performance_no, prf.[performance_zone], 'available'), 0)
        FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] AS prf
        WHERE (prf.[performance_dt] BETWEEN @week_start_dt AND @week_end_dt
               OR prf.[performance_dt] BETWEEN @prev_week_start_dt AND @prev_week_end_dt)
          AND (prf.[title_no] IN (@special_exhibitions_no, @butterfly_garden_no, @mugar_omni_theater_no,
                                 @4d_theater_no, @hayden_planetarium_no, @thrill_ride_no)
              OR prf.[title_no] = @adult_offering_no AND prf.[visit_count_production] = 'Y');



    /*  Delete any performances that are not school or public (like buyouts)  */

        DELETE FROM [#capacity_raw_data]
        WHERE perf_type_name NOT IN ('Public', 'School Only');

    /*  Shorten the Omni Theater and Planetarium Title Names  */

        UPDATE [#capacity_raw_data]
        SET [title_name] = CASE WHEN [title_no] = @mugar_omni_theater_no THEN 'Omni Theater'
                                WHEN [title_no] = @hayden_planetarium_no THEN 'Planetarium'
                                ELSE [title_name] END;


    /*  Combine all butterfly Garden productions into one  */

        UPDATE [#capacity_raw_data]
        SET [prod_name] = 'Butterfly Garden'
        WHERE [title_name] = 'Butterfly Garden';

    /*  Remove Performance Types from venues where it doesn't matter  */

        UPDATE [#capacity_raw_data]
        SET [perf_type_name] = ''
        WHERE @separate_School_Sections = 'N'
           OR ISNULL([title_name],'') NOT IN ('Omni Theater', 'Planetarium');

    /*  Remove the 4D Prefix before any 4D Production  */

        UPDATE [#capacity_raw_data]
        SET [prod_name] = REPLACE([prod_name],'4D: ','')
        WHERE [title_no] = @4d_theater_no;

        
    /*  Insert aggegated data into final table  */

        INSERT INTO [#final_table] ([week_ending], [title_order], [title_no], [title_name], [title_color], [perf_type_name],
                                    [prod_no], [prod_name], [total], [sold_seats])
        SELECT  CAST(cap.[week_ending] AS DATE) AS [week_ending],
                CASE WHEN cap.[title_no] = @special_exhibitions_no THEN 'A'
                     WHEN cap.[title_no] = @butterfly_garden_no THEN 'B'
                     WHEN cap.[title_no] = @mugar_omni_theater_no THEN 'C'
                     WHEN cap.[title_no] = @4d_theater_no THEN 'D'
                     WHEN cap.[title_no] = @thrill_ride_no THEN 'E'
                     WHEN cap.[title_no] = @hayden_planetarium_no THEN 'F'
                     WHEN cap.[title_no] = @entertainment_no THEN 'G'
                     ELSE 'Z' END AS [title_order],
                cap.[title_no],
                cap.[title_name],
                'LightSteelBlue',
                cap.[perf_type_name],
                cap.[prod_no],
                cap.[prod_name],
                SUM(cap.[capacity_sellable]) AS [total],
                SUM(cap.[capacity_sold]) AS [sold]
        FROM [#capacity_raw_data] AS cap
        GROUP BY CAST(cap.[week_ending] AS DATE), cap.[title_no], cap.[title_name], cap.[perf_type_name], cap.[prod_no], cap.[prod_name];


    /* Update final table with attendance history - one week at a time  */

        UPDATE [#final_table]
        SET [sold_history] = (SELECT ISNULL(SUM(sale_total),0)
                              FROM [dbo].[LT_HISTORY_TICKET] AS his
                              WHERE CAST(his.performance_date AS DATE) BETWEEN @week_start_dt AND @week_end_dt
                                AND his.[title_no] = CASE WHEN [#final_table].[title_no] = @entertainment_no THEN @hayden_planetarium_no ELSE [#final_table].[title_no] END
                                AND his.[production_no] = [#final_table].[prod_no])
        WHERE [week_ending] = CAST(@week_end_dt AS DATE);

        UPDATE [#final_table]
        SET [sold_history] = (SELECT ISNULL(SUM(sale_total),0)
                              FROM [dbo].[LT_HISTORY_TICKET] AS his
                              WHERE CAST(his.performance_date AS DATE) BETWEEN @prev_week_start_dt AND @prev_week_end_dt
                                AND his.[title_no] = CASE WHEN [#final_table].[title_no] = @entertainment_no THEN @hayden_planetarium_no ELSE [#final_table].[title_no] END
                                AND his.[production_no] = [#final_table].[prod_no])
        WHERE [week_ending] = CAST(@prev_week_end_dt AS DATE);

    /*  Use history data unless for some reason it returns null or zero, then use seat data  */
    
        UPDATE [#final_table]
        SET [sold] = CASE WHEN ISNULL([sold_history],0) = 0 THEN [sold_seats]
                          ELSE [sold_history] END;

    /*  Calculate the percentages  */

        UPDATE [#final_table]
        SET [percent_of capacity] = CASE WHEN [total] = 0 THEN 0
                                         ELSE ([sold] / [total]) END;

    
    /*  Identify Entertainment productions in the Planetarium venue  */

        WITH CTE_ENTERTAINMENT_SHOWS (prod_no, prod_name, cont_value)
        AS (
            SELECT pro.[prod_no], inv.[description], con.[value]
            FROM [dbo].[T_PRODUCTION] AS pro
                 INNER JOIN [dbo].[T_INVENTORY] AS inv ON inv.[inv_no] = pro.[prod_no]
                 INNER JOIN [dbo].[TX_INV_CONTENT] AS con ON con.[inv_no] = pro.[prod_no] 
                                                         AND con.[content_type] = 37
            WHERE pro.[title_no] = 1132 
              AND con.[value] = 'Entertainment'
           )
        UPDATE [#final_table] 
        SET [title_no] = @entertainment_no,
            [title_name] = 'Entertainment',
            [title_order] = 'G'
        WHERE [title_no] = @hayden_planetarium_no
          AND [prod_no] IN (SELECT [prod_no] 
                            FROM [CTE_ENTERTAINMENT_SHOWS]);

    /*  Identify Entertainment productions in the Adult Offerings venue  */

        UPDATE [#final_table] 
        SET [title_no] = @entertainment_no,
            [title_name] = 'Entertainment',
            [title_order] = 'G'
        WHERE [title_no] = @adult_offering_no;


    /*  Separate out School From Public Omni Theater  */

        UPDATE [#final_table]
        SET [title_name] = 'Omni Theater School',
            [prod_name] = REPLACE([prod_name],'SCH: ','')
        WHERE title_no = @mugar_omni_theater_no
          AND prod_name LIKE 'SCH:%'

        UPDATE [#final_table]
        SET [title_name] = 'Omni Theater Public'
        WHERE title_no = @mugar_omni_theater_no
          AND title_name = 'Omni Theater'

    /*  Separate out School From Public Planetarium  */

        UPDATE [#final_table]
        SET [title_name] = 'Planetarium School',
            [prod_name] = REPLACE([prod_name],'SCH: ','')
        WHERE title_no = @hayden_planetarium_no
          AND prod_name LIKE 'SCH:%'

        UPDATE [#final_table]
        SET [title_name] = 'Planetarium Public'
        WHERE title_no = @hayden_planetarium_no
          AND title_name = 'Planetarium'
        
    FINISHED:

        /*  Select final data set for the report  */
        
            SELECT [week_ending],
                   [title_order],
                   [title_no],
                   [title_name],
                   [title_color],
                   [perf_type_name],
                   [prod_no],
                   [prod_name],
                   CAST([total] AS INT) AS [total],
                   CAST([sold] AS INT) AS [sold],
                   [percent_of capacity]
            FROM [#final_table];

    DONE:

        IF OBJECT_ID('tempdb..#final_table') is not null DROP TABLE [#final_table];
        IF OBJECT_ID('tempdb..#capacity_raw_data') is not null DROP TABLE [#capacity_raw_data];

END
GO

--EXECUTE [dbo].[LRP_ATTEND_WEEKLY_CAPACITY] @week_ending = '10-27-2019'


        --WITH CTE_TITLE_COLORS ([title_no], [title_name], [title_color])
        --AS (SELECT DISTINCT [title_no],
        --                    [title_name],
        --                    CASE WHEN [title_no] = @gross_attend_no THEN 'LightSteelBlue'
        --                         WHEN [title_no] = @exhibit_halls_no THEN 'OliveDrab'
        --                         WHEN [title_no] = @butterfly_garden_no THEN 'MediumSeaGreen'
        --                         WHEN [title_no] = @special_exhibitions_no THEN 'PaleGreen'
        --                         WHEN [title_no] = @mugar_omni_theater_no THEN 'LightSteelBlue'
        --                         WHEN [title_no] = @4d_theater_no THEN 'LightSkyBlue'
        --                         WHEN [title_no] = @thrill_ride_no THEN 'Gainsboro'
        --                         WHEN [title_no] = @hayden_planetarium_no THEN 'LightCoral'
        --                         WHEN [title_no] = @entertainment_no THEN 'Salmon'
        --                         ELSE 'White' END
        --     FROM [#capacity_raw_data]
        --    )


              