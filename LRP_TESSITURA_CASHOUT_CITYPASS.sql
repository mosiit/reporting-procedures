USE [impresario]    
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_TESSITURA_CASHOUT_CITYPASS]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LRP_TESSITURA_CASHOUT_CITYPASS]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LRP_TESSITURA_CASHOUT_CITYPASS]
        @report_dt DATETIME = NULL,
        @report_operator VARCHAR(10) = '',
        @machine_locations VARCHAR(4000) = ''
AS BEGIN

    /*  Procedure Variables  */

        DECLARE @report_date CHAR(10) = '', @citypass_voucher_method INT = 0

        DECLARE @citypass_orders TABLE ([order_no] INT, 
                                        [operator] VARCHAR(10), 
                                        [create_location] VARCHAR(50))
                                        
        DECLARE @citypass_payments TABLE ([order_no] INT, 
                                          [operator] VARCHAR(10), 
                                          [payment_method] INT, 
                                          [payment_method_name] VARCHAR(30), 
                                          [payment_amount] MONEY)

        DECLARE @citypass_info TABLE ([citypass_order_no] INT,
                                      [citypass_sli_no] INT, 
                                      [citypass_batch_no] INT, 
                                      [citypass_dt] DATETIME, 
                                      [citypass_operator] VARCHAR(10),
                                      [citypass_perf_no] INT,
                                      [citypass_zone_no] INT,
                                      [citypass_price_type_no] INT, 
                                      [citypass_price_type] VARCHAR(30),
                                      [citypass_sli_status] INT,
                                      [citypass_count] INT)

        DECLARE @location_list TABLE ([location_no_str] VARCHAR(20),
                                      [location_no] INT)

        DECLARE @machine_list TABLE ([machine_name] VARCHAR(50))

    /* The payment method id for CityPass vouchers is 152.  If that ever channges, this will need to be changed as well */
    
        SELECT @citypass_voucher_method = 152

    /*  Check Parameters - Null report_dt = today  /  null report_operator = blank (blank = All operators)  */

        SELECT @report_dt = ISNULL(@report_dt,GETDATE())
        SELECT @report_operator = ISNULL(@report_operator,'')
   
        SELECT @report_date = CONVERT(CHAR(10),@report_dt,111)

        SELECT @machine_locations = ISNULL(@machine_locations,'')        

    /*  Parse machine_locations parameter  */

        INSERT INTO @location_list ([location_no_str]) 
            SELECT [item] 
            FROM [dbo].[LFT_SPLIT_DILEMETED_STRING] (@machine_locations, ',')

        DELETE FROM @location_list 
        WHERE [dbo].[FS_isReallyNumeric] ([location_no_str]) = 0

        UPDATE @location_list 
        SET [location_no] = CONVERT(INT,[location_no_str])

        INSERT INTO @machine_list ([machine_name])
            SELECT [machine_name] 
            FROM [dbo].[TX_MACHINE_LOCATION] 
            WHERE [location] IN (SELECT [location_no] FROM @location_list);

    /*  Determine which orders for the designated date are city pass transactions */
    
        WITH CTE_CITY_PASS_PERF ([perf_no], [zone_no])
        AS (SELECT [performance_no], 
                   [performance_zone]
            FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] 
            WHERE [performance_date] = @report_date
              AND [title_name] = 'CityPASS')
        INSERT INTO @citypass_orders
        SELECT DISTINCT sli.[order_no], 
                        sli.[created_by], 
                        sli.[create_loc]
        FROM [dbo].[T_SUB_LINEITEM] AS sli (NOLOCK)
             INNER JOIN [CTE_CITY_PASS_PERF] AS cte (NOLOCK) ON cte.[perf_no] = sli.[perf_no] AND cte.[zone_no] = sli.[zone_no]
        WHERE CONVERT(DATE,sli.[create_dt]) = CONVERT(DATE,@report_date) 
          AND sli.[sli_status] IN (3,12)
   
    /*  Limit orders to a specific operator if needed */

        IF @report_operator <> '' DELETE FROM @citypass_orders WHERE [operator] <> @report_operator


    /*  Delete orders that are not from the specific machines requested  */

        DELETE FROM @citypass_orders WHERE [create_location] NOT IN (SELECT [machine_name] FROM @machine_list)

    /*  Determine which of the orders contain the CityPASS Pre-Paid Voucher payment method on them  */

        INSERT INTO @citypass_payments
        SELECT [order_no], [payment_created_by], [payment_method_no], [payment_method_name], [payment_amount]
        FROM [dbo].[LV_ORDER_PAYMENTS] 
        WHERE [order_no] IN (SELECT [order_no] FROM @citypass_orders) AND [payment_method_no] = @citypass_voucher_method   

    /*  Pull CityPass transctional information  */
    
        INSERT INTO @citypass_info
        SELECT sli.[order_no], sli.[sli_no], sli.[batch_no], CONVERT(DATE,sli.[create_dt]), sli.[created_by], sli.[perf_no], sli.[zone_no], 
               sli.[price_type], ptp.[description], sli.[sli_status], 1
        FROM [dbo].[T_SUB_LINEITEM] AS sli (NOLOCK)
             LEFT OUTER JOIN [dbo].[TR_PRICE_TYPE] AS ptp (NOLOCK) ON ptp.[id] = sli.[price_type]
        WHERE order_no IN (SELECT order_no FROM @citypass_payments)

    /*  Do a double check to make sure only the designated operator's sales are in the table */

        IF @report_operator <> '' DELETE FROM @citypass_info WHERE [citypass_operator] <> @report_operator

    FINISHED:

        /*  Do not add blank line if no records are found.  */
  
            SELECT citypass_dt, [citypass_operator], [citypass_price_type], SUM([citypass_count]) AS 'citypass_count'
            FROM @citypass_info
            GROUP BY citypass_dt, [citypass_operator], [citypass_price_type]

    DONE:

END 
GO

GRANT EXECUTE ON [dbo].[LRP_TESSITURA_CASHOUT_CITYPASS] TO impusers
GO

EXECUTE [dbo].[LRP_TESSITURA_CASHOUT_CITYPASS] @report_dt = '4-17-2021', @report_operator = 'jfarre00', @machine_locations = '6,15'
--EXECUTE [dbo].[LRP_TESSITURA_CASHOUT_CITYPASS] @report_dt = '1-8-2017', @report_operator = 'mdalto00'
--EXECUTE [dbo].[LRP_TESSITURA_CASHOUT_CITYPASS] @report_dt = '1-8-2017', @report_operator = 'dbarbe00'
--SELECT * FROM dbo.T_SUB_LINEITEM WHERE order_no = 434122
--SELECT * FROM T_ZONE WHERE zone_no = 431
--SELECT DISTINCT created_by FROM dbo.T_SUB_LINEITEM WHERE convert(date,create_dt ) = '12-3-2016'
