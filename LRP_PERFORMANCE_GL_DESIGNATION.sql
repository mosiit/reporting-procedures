USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_PERFORMANCE_GL_DESIGNATION]') AND type IN (N'P', N'PC'))
BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_PERFORMANCE_GL_DESIGNATION] AS';
    EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LRP_PERFORMANCE_GL_DESIGNATION] TO impusers';
END;
GO

ALTER PROCEDURE [dbo].[LRP_PERFORMANCE_GL_DESIGNATION]
            @report_start_dt DATETIME = NULL,
            @report_end_dt DATETIME = NULL,
            @title_str VARCHAR(4000) = '',
            @exclude_zero_dollars CHAR(1) = 'Y'
WITH RECOMPILE AS BEGIN

    /*  Procedure Variables  */

        DECLARE @title_list AS TABLE ([title_no] INT NOT NULL DEFAULT (0));

        IF OBJECT_ID('tempdb..#gl_data') IS NOT NULL DROP TABLE [#gl_data]

        CREATE TABLE [#gl_data] ([performance_no] INT,
                                 [performance_zone] INT, 
                                 [performance_dt] DATE, 
                                 [performance_time] VARCHAR(10),
                                 [performance_display] VARCHAR(50), 
                                 [title_no] INT,
                                 [title_name] VARCHAR(30),
                                 [production_no] INT,
                                 [production_name] VARCHAR(30),
                                 [price_type] INT, 
                                 [price_type_name] VARCHAR(30), 
                                 [start_price] DECIMAL(18,2),
                                 [default_gl_no] INT, 
                                 [default_gl_account] VARCHAR(30), 
                                 [gl_no] INT,  
                                 [gl_account] VARCHAR(30),
                                 [title_has_non_default] CHAR(1),
                                 [non_default_counter] INT);

    /*  Check Parameters  */

        IF ISNULL(@title_str, '') = ''
            INSERT INTO @title_list ([title_no])
            SELECT [title_no] FROM [dbo].[T_TITLE];
        ELSE
            INSERT INTO @title_list ([title_no])
            SELECT CAST([Element] AS INT) FROM [dbo].[FT_SPLIT_LIST](REPLACE(@title_str,'"',''),',');
            
        SELECT @report_start_dt = ISNULL(@report_start_dt,GETDATE());

        IF @report_end_dt IS NULL
            SELECT @report_end_dt = MAX([performance_dt]) 
            FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] AS prf
                 INNER JOIN @title_list AS ttl ON ttl.[title_no] = prf.[title_no];

        IF ISNULL(@exclude_zero_dollars,'Y') <> 'N'
            SELECT @exclude_zero_dollars = 'Y'

    /*  Pull GL Data  */                 

        INSERT INTO [#gl_data] ([performance_no],[performance_zone],[performance_dt],[performance_time],[performance_display],[title_no],[title_name],
                                [production_no],[production_name],[price_type],[price_type_name],[start_price],[default_gl_no],[default_gl_account],
                                [gl_no],[gl_account],[title_has_non_default], [non_default_counter])
        SELECT gln.[performance_no], 
               gln.[performance_zone], 
               gln.[performance_dt], 
               gln.[performance_time],
               gln.[performance_display], 
               gln.[title_no],
               gln.[title_name],
               gln.[production_no],
               gln.[production_name],
               gln.[price_type], 
               gln.[price_type_name], 
               gln.[start_price], 
               gln.[default_gl_no], 
               gln.[default_gl_account], 
               gln.[gl_no],  
               gln.[gl_account],
               'N',
               0
        FROM [dbo].[LV_PERFORMANCE_PRICE_GL_NUMBERS] AS gln
        WHERE gln.[performance_dt] BETWEEN @report_start_dt AND @report_end_dt
            
            

    /*  Flag any records that have a gl number that is not the default gl number  */

        UPDATE [#gl_data]
        SET [non_default_counter] = 1
        WHERE [gl_account] <> [default_gl_account];

        WITH [cte_non_default] ([title_no], [production_no])
        AS (SELECT [title_no], 
                   [production_no]
            FROM [#gl_data]
            WHERE [gl_account] <> [default_gl_account])
        UPDATE gl
        SET gl.[title_has_non_default] = 'Y'
        FROM [#gl_data] AS gl
             INNER JOIN [cte_non_default] AS df ON df.[title_no] = gl.[title_no] AND df.[production_no] = gl.[production_no];
             
    FINISHED:

        /*  Pull Final Record Set For the Report  */

            SELECT gln.[performance_no],
                   gln.[performance_zone],
                   gln.[performance_dt],
                   gln.[performance_time],
                   gln.[performance_display],
                   gln.[title_no],
                   gln.[title_name],
                   gln.[production_no],
                   gln.[production_name],
                   gln.[price_type],
                   gln.[price_type_name],
                   gln.[start_price],
                   gln.[default_gl_no],
                   gln.[default_gl_account],
                   gln.[gl_no],
                   gln.[gl_account],
                   gln.[title_has_non_default],
                   gln.[non_default_counter]
            FROM [#gl_data] AS gln
                 INNER JOIN @title_list AS ttl ON ttl.[title_no] = gln.[title_no]
            WHERE (@exclude_zero_dollars = 'N' OR [start_price] > 0.0);

    DONE:

        IF OBJECT_ID('tempdb..#gl_data') IS NOT NULL DROP TABLE [#gl_data]

END
GO

--EXECUTE [dbo].[LRP_PERFORMANCE_GL_DESIGNATION] '5-1-2019', '5-31-2019', '"161","1132"','N'





