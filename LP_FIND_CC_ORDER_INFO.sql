USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LP_FIND_CC_ORDER_INFO]    Script Date: 10/18/2017 3:57:03 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[LP_FIND_CC_ORDER_INFO] @payment_cc_reference_no INT
AS
BEGIN

		--CREATE DATE: 8/15/2017
		--CREATED BY: Aileen Duffy-Brown
		--COMMENT: Used in credit card charge disbutes, double charges and research to track down orders based on VANTIV ref #
    

    SELECT order_no,
           payment_cc_reference_no,
           transaction_create_dt,
           transaction_created_by,
           transaction_type_name,
           transaction_amount,
           payment_method_name,
           payment_customer_name,
           payment_customer_no
    FROM LV_ORDER_PAYMENTS (NOLOCK)
    WHERE payment_cc_reference_no = @payment_cc_reference_no;


END;


GO


