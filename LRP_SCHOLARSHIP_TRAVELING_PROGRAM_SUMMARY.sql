SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[LRP_SCHOLARSHIP_TRAVELING_PROGRAM_SUMMARY]
        @report_start_dt DATETIME = NULL, 
        @report_end_dt DATETIME = NULL,
        @customer_type_id INT = 0,
        @scholarship_id INT = 0
AS BEGIN
 

-----------------

--DECLARE 
--	@report_start_dt DATETIME = '2019-10-04', --'2019-12-01', 
--    @report_end_dt DATETIME = '2019-10-04', --'2019-12-31',
--    @customer_type_id INT = 13,
--    @scholarship_id INT = 0

----------------   


    /*  Procedure Variables  */

        DECLARE @ord_no INT, @perf_date CHAR(10), @prod_name VARCHAR(50)
        DECLARE @start_date CHAR(10), @end_date CHAR(10)
        DECLARE @cust_type_id_school INT, @cust_type_id_school_official INT 
            
    /*  Check Parameters  */

        IF @report_start_dt IS NULL
            SELECT @report_start_dt = CONVERT(DATETIME,'7-1-' 
                                    + CASE WHEN DATEPART(MONTH,GETDATE()) < 7 THEN CONVERT(VARCHAR(30),DATEPART(YEAR,DATEADD(YEAR,-1,GETDATE()))) 
                                                                              ELSE CONVERT(VARCHAR(30),DATEPART(YEAR,GETDATE())) END)
        IF @report_end_dt IS NULL
            SELECT @report_end_dt = CONVERT(DATETIME,'6-30-' 
                                    + CASE WHEN DATEPART(MONTH,GETDATE()) >= 7 THEN CONVERT(VARCHAR(30),DATEPART(YEAR,DATEADD(YEAR,1,GETDATE()))) 
                                                                               ELSE CONVERT(VARCHAR(30),DATEPART(YEAR,GETDATE())) END) + '23:59:59.967'

        SELECT @customer_type_id = ISNULL(@customer_type_id,0)
        SELECT @scholarship_id = ISNULL(@scholarship_id,0)

    /*  Make sure the date parameters include the full dates, starting at 00:00 and ending at 23:59  */

        SELECT @start_date = CONVERT(CHAR(10),@report_start_dt,111),
               @end_date = CONVERT(CHAR(10), @report_end_dt,111)


     /*  For the purposes of this report, the School and School Official Record customer types are the same thing.
         The id numbers are needed to combine the two  */

        SELECT @cust_type_id_school = [id] FROM [dbo].[TR_CUST_TYPE] WHERE [description] = 'School'
        SELECT @cust_type_id_school = ISNULL(@cust_type_id_school,0)

        SELECT @cust_type_id_school_official = [id] FROM [dbo].[TR_CUST_TYPE] WHERE [description] = 'School Official Record'
        SELECT @cust_type_id_school_official = ISNULL(@cust_type_id_school_official,0)


    /*  If customer type id selected was school official record, change to school  */

        IF @customer_type_id = @cust_type_id_school_official SELECT @customer_type_id = @cust_type_id_school
    
    /*  Create temporary tables  */
            
            IF OBJECT_ID('tempdb..#trav_prog_orders') IS NOT NULL DROP TABLE [#trav_prog_orders]

            CREATE TABLE [#trav_prog_orders] ([order_no] INT)
            
            CREATE UNIQUE CLUSTERED INDEX [ix_trav_prog_orders] ON [#trav_prog_orders] ([order_no] ASC) ON [PRIMARY]
          
            --,CONSTRAINT [PK_trav_prog_orders_summary_order_no] PRIMARY KEY CLUSTERED ([order_no] ASC) ON [PRIMARY])

            IF OBJECT_ID('tempdb..#trav_prog_data') IS NOT NULL DROP TABLE [#trav_prog_data]

            CREATE TABLE [#trav_prog_data] ([data_type] VARCHAR(10), [order_no] INT, [sli_no] INT, [customer_no] INT, [customer_type_no] INT, [performance_date] CHAR(10), [title_name] VARCHAR(30), 
                                            [scholarship_no] INT, [production_name] VARCHAR(50), [zone_name] VARCHAR(30), [price_type_name] VARCHAR(30), [create_dt] DATETIME, [due_amount] MONEY, 
											[total_paid] MONEY, attendance INT)

            CREATE CLUSTERED INDEX [ix_trav_prog_data_data_type] ON [#trav_prog_data] ([data_type] ASC) ON [PRIMARY]
            CREATE NONCLUSTERED INDEX [ix_trav_prog_data_production_name] ON [#trav_prog_data] ([production_name] ASC) ON [PRIMARY]
            CREATE NONCLUSTERED INDEX [ix_trav_prog_data_price_type_name] ON [#trav_prog_data] ([price_type_name] ASC) ON [PRIMARY]
            CREATE NONCLUSTERED INDEX [ix_trav_prog_data_performance_date] ON [#trav_prog_data] ([performance_date] ASC) ON [PRIMARY]
            CREATE NONCLUSTERED INDEX [ix_trav_prog_data_due_amount] ON [#trav_prog_data] ([due_amount] ASC) ON [PRIMARY]
            CREATE NONCLUSTERED INDEX [ix_trav_prog_data_total_paid] ON [#trav_prog_data] ([total_paid] ASC) ON [PRIMARY]

            IF OBJECT_ID('tempdb..#trav_prog_final') IS NOT NULL DROP TABLE [#trav_prog_final]

            CREATE TABLE [#trav_prog_final] ([data_type] VARCHAR(10), [order_no] INT, [production_name] VARCHAR(50), [customer_type] VARCHAR(30), [due_amount] MONEY, [total_paid] MONEY, [program_counter] INT, attendance INT)

            CREATE CLUSTERED INDEX [ix_trav_prog_final_data_type] ON [#trav_prog_final] ([data_type] ASC) ON [PRIMARY]
            CREATE NONCLUSTERED INDEX [ix_trav_prog_final_production_name] ON [#trav_prog_final] ([production_name] ASC) ON [PRIMARY]
            CREATE NONCLUSTERED INDEX [ix_trav_prog_final_order_no] ON [#trav_prog_final] ([order_no] ASC) ON [PRIMARY]

    /*  Generate a list of all traveling programs sales for designated date range  */

        INSERT INTO [#trav_prog_orders]
        SELECT DISTINCT [order_no] FROM [dbo].[LV_ORDER_DETAIL]
        WHERE [performance_date] BETWEEN @start_date AND @end_date AND title_name = 'Traveling Programs'
          
    /*  Remove any orders where no scholarship payment was made  */

        DELETE FROM [#trav_prog_orders]
        WHERE [order_no] NOT IN (SELECT [order_no] FROM [#trav_prog_orders] WHERE [order_no] IN (SELECT [order_no] FROM [dbo].[LV_SCHOLARSHIP_PAYMENTS]))

    /* Get Traveling Programs Order and Payment Data */
    
        INSERT INTO [#trav_prog_data]
        SELECT 'ord_info', ord.[order_no], ord.[sli_no], ISNULL(ord.[customer_no],0), cus.[cust_type], ord.[performance_date], ord.[title_name], 0, ord.[production_name], 
                ord.[zone_name], ord.[price_type_name], ord.[create_dt], ord.[due_amount], 0.00, COALESCE(TRY_CONVERT(INT,o.custom_1),0)
        FROM [dbo].[LV_ORDER_DETAIL] AS ord (NOLOCK)
             LEFT OUTER JOIN [dbo].[T_CUSTOMER] AS cus ON cus.[customer_no] = ord.[customer_no]
			 LEFT OUTER JOIN [dbo].[T_ORDER] o (NOLOCK) ON (o.order_no = ord.order_no AND o.customer_no = ord.customer_no)
        WHERE ord.[order_no] IN (SELECT [order_no] FROM [#trav_prog_orders])

        INSERT INTO [#trav_prog_data]
        SELECT 'pay_info', pay.[order_no], 0, pay.[customer_no], ISNULL(cus.[customer_no],0) , '', 'payment', pay.[scholarship_no], pay.[scholarship_name], 
                '', pay.[payment_method], pay.[payment_dt], 0.00, SUM(pay.[payment_amount]), COALESCE(TRY_CONVERT(INT,o.custom_1),0)
        FROM [dbo].[LV_SCHOLARSHIP_PAYMENTS] AS pay (NOLOCK)
             LEFT OUTER JOIN [dbo].[T_CUSTOMER] AS cus (NOLOCK) ON cus.[customer_no] = pay.[customer_no]
			 LEFT OUTER JOIN [dbo].[T_ORDER] o (NOLOCK) ON (o.customer_no = cus.customer_no AND o.order_no = pay.order_no)
        WHERE pay.[order_no] IN (SELECT [order_no] FROM [#trav_prog_orders])
        GROUP BY pay.[order_no], pay.[customer_no], ISNULL(cus.[customer_no],0), pay.[scholarship_no], pay.[scholarship_name], pay.[payment_method], pay.[payment_dt],o.[custom_1]
        HAVING SUM([payment_amount]) <> 0.00

        DELETE FROM [#trav_prog_data] WHERE due_amount = 0.00 AND total_paid = 0.00
        DELETE FROM [#trav_prog_data] WHERE [order_no] IN (SELECT [order_no] FROM [#trav_prog_data] WHERE price_type_name = 'Interdepartmental Transfer')

        
        UPDATE [#trav_prog_data] SET [production_name] = 'Unknown Scholarship' WHERE [data_type] = 'pay_info' and  [production_name] = ''

        /*  Combine School and School Official Record into a single customer type  */

        UPDATE [#trav_prog_data] SET [customer_type_no] = @cust_type_id_school WHERE [customer_type_no] = @cust_type_id_school_official

        /*  Remove unwanted constiruent types and schlarships based on parameters  */

            IF @customer_type_id > 0
                DELETE FROM [#trav_prog_data] WHERE [order_no] NOT IN (SELECT [order_no] FROM [#trav_prog_data] WHERE [customer_type_no] = @customer_type_id)

            IF @scholarship_id > 0
                DELETE FROM [#trav_prog_data] WHERE [scholarship_no] <> @scholarship_id
                --DELETE FROM [#trav_prog_data] WHERE [order_no] NOT IN (SELECT [order_no] FROM [#trav_prog_data] WHERE [scholarship_no] = @scholarship_id)

    /*  Create final data table  */

        INSERT INTO [#trav_prog_final]
        SELECT dat.[data_type], dat.[order_no], dat.[production_name], ISNULL(ctp.[description],''), SUM(dat.[due_amount]), SUM(dat.[total_paid]), 0, SUM(COALESCE(CAST(dat.attendance AS INT),0))
        FROM [#trav_prog_data] dat (NOLOCK)
             LEFT OUTER JOIN [dbo].[T_CUSTOMER] AS cus (NOLOCK) ON cus.[customer_no] = dat.[customer_no]
             LEFT OUTER JOIN [dbo].[TR_CUST_TYPE] AS ctp (NOLOCK) ON ctp.[id] = cus.[cust_type]
        WHERE dat.[data_type] = 'pay_info'
        GROUP BY dat.[data_type], dat.[order_no], dat.[production_name], ISNULL(ctp.[description],'')
        HAVING SUM(dat.[total_paid]) <> 0.00

        UPDATE [#trav_prog_final] SET [customer_type] = 'School' WHERE [customer_type] = 'School Official Record'

        --UPDATE [#trav_prog_final] SET [program_counter] = (SELECT COUNT(DISTINCT [sli_no]) FROM [dbo].[LV_ORDER_DETAIL] AS det
        --                                                   WHERE det.[order_no] = [#trav_prog_final].order_no AND det.[zone_no] = 92)     --zone # 92 = 'Program Fee'
     
        UPDATE [#trav_prog_final] SET [program_counter] = (SELECT COUNT(DISTINCT [sli_no]) FROM [dbo].[T_SUB_LINEITEM] 
                                                           WHERE [order_no] = [#trav_prog_final].[order_no] AND [zone_no] = 92 AND sli_status IN (3,12))     --zone # 92 = 'Program Fee'
     
        --(SELECT COUNT(*) FROM [#trav_prog_data] WHERE data_type = 'ord_info' AND [zone_name] = 'Program Fee' AND [#trav_prog_data].order_no = [#trav_prog_final].order_no)

    FINISHED:
       
        /*  Select the final record set from #final_table  */

            SELECT [data_type], [order_no], [production_name], [program_counter], [due_amount], [total_paid], [customer_type], [attendance]
            FROM [#trav_prog_final]


        /*  Clean up and Destroy Temporary Tables  */

            IF OBJECT_ID('tempdb..#trav_prog_final') IS NOT NULL DROP TABLE [#trav_prog_final]

            IF OBJECT_ID('tempdb..#trav_prog_data') IS NOT NULL DROP TABLE [#trav_prog_data]
            
            IF OBJECT_ID('tempdb..#trav_prog_orders') IS NOT NULL DROP TABLE [#trav_prog_orders]

    DONE:

END
GO

EXECUTE [dbo].[LRP_SCHOLARSHIP_TRAVELING_PROGRAM_SUMMARY] @report_start_dt = '2019-10-04', @report_end_dt = '2019-10-04', @customer_type_id = 13, @scholarship_id = 0


