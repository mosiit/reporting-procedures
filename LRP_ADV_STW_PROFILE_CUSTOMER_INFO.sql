USE impresario;
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_ADV_STW_PROFILE_CUSTOMER_INFO]') AND type in (N'P', N'PC'))
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_ADV_STW_PROFILE_CUSTOMER_INFO] AS';
GO

EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LRP_ADV_STW_PROFILE_CUSTOMER_INFO] TO [impusers], [tessitura_app]';
GO

ALTER PROCEDURE [dbo].[LRP_ADV_STW_PROFILE_CUSTOMER_INFO]
        @customer_no INT = NULL,
        @list_no INT = NULL
AS BEGIN

    SET NOCOUNT ON;
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    
    /*  Procedute Variables and Temp Tables  */

        DECLARE @customer_table TABLE ([customer_no] INT NOT NULL DEFAULT (0),
                                       [customer_type] INT NOT NULL DEFAULT(0),
                                       [customer_type_name] VARCHAR (30) NOT NULL DEFAULT (''))

        CREATE TABLE [#cust_info] ([customer_no] INT NOT NULL DEFAULT (0),
                                   [customer_name] VARCHAR(75) NOT NULL DEFAULT (''),
                                   [customer_type_no] INT NOT NULL DEFAULT (0),
                                   [customer_type_name] VARCHAR(30) NOT NULL DEFAULT (''),
                                   [prospect_mgr] VARCHAR(75) NOT NULL DEFAULT (''),
                                   [contact_pm_a1] VARCHAR(75) NOT NULL DEFAULT (''),
                                   [contact_pm_a2] VARCHAR(75) NOT NULL DEFAULT (''),
                                   [affiliate_customer_no] INT NOT NULL DEFAULT (0),
                                   [affiliate_type_no] INT NOT NULL DEFAULT (0),
                                   [affiliate_type_name] VARCHAR(75) NOT NULL DEFAULT (''),
                                   [affiliate_name] VARCHAR(75) NOT NULL DEFAULT (''),
                                   [name_ind] INT NOT NULL DEFAULT (0),
                                   [name_ind_sort] VARCHAR(10) NOT NULL DEFAULT (''))

    /*  Check Parameters  */

        SELECT @customer_no = ISNULL(@customer_no, 0)
        SELECT @list_no = ISNULL(@list_no, 0)

        IF @customer_no > 0
            INSERT INTO @customer_table ([customer_no], [customer_type], [customer_type_name])
            SELECT c.[customer_no], c.[cust_type], t.[description]
            FROM [dbo].[T_CUSTOMER] AS c 
                 INNER JOIN [dbo].[TR_CUST_TYPE] AS t ON t.[id] = c.[cust_type]
            WHERE c.[customer_no] = @customer_no
        ELSE IF @list_no > 0
            INSERT INTO @customer_table ([customer_no], [customer_type], [customer_type_name])
            SELECT l.[customer_no], c.[cust_type], t.[description]
            FROM [dbo].[T_LIST_CONTENTS] AS l
                 INNER JOIN [dbo].[T_CUSTOMER] AS c ON c.[customer_no] = l.[customer_no]
                 INNER JOIN [dbo].[TR_CUST_TYPE] AS t ON t.[id] = c.[cust_type]
            WHERE l.[list_no] = @list_no
        ELSE
            GOTO FINISHED;


            /*********************************
                FOR TESTING
             **********************************/

            --DELETE FROM @customer_table

            --INSERT INTO @customer_table ([customer_no], [customer_type], [customer_type_name])
            --SELECT c.[customer_no], c.[cust_type], t.[description]
            --FROM [dbo].[T_CUSTOMER] AS c 
            --     INNER JOIN [dbo].[TR_CUST_TYPE] AS t ON t.[id] = c.[cust_type]
            --WHERE c.[customer_no]  IN (67226, 115406, 664196, 718691, 3942954);



    /*  Get the Basic Customer Information  */

        
        WITH CTE_PROSPECT_MGR ([customer_no], [prospect_mgr])
             AS (SELECT [customer_no], 
                        [key_value]
                 FROM [dbo].[TX_CUST_KEYWORD]
                 WHERE [keyword_no] = 555), ---555 = Prospect Manager

             CTE_CONTACT_PM_A1 ([customer_no], [contact_pm_a1])
             AS (SELECT a.[customer_no], 
                        c.[key_value] 
                 FROM [dbo].[V_CUSTOMER_WITH_PRIMARY_AFFILIATES] AS a (NOLOCK)
                      INNER JOIN [dbo].[VXS_CUST_KEYWORD] AS c (NOLOCK) ON a.[expanded_customer_no] = c.[customer_no]
                      INNER JOIN @customer_table AS cu ON cu.[customer_no] = a.[customer_no]
                WHERE a.[name_ind] IN (-1, 0) 
                  AND a.[cust_type] + a.[name_ind] <> 7 
                  AND c.[keyword_no] = 630),
        
            CTE_CONTACT_PM_A2 ([customer_no], [contact_pm_a2])
            AS (SELECT a.[customer_no], 
                       c.[key_value] 
                FROM [dbo].[V_CUSTOMER_WITH_PRIMARY_AFFILIATES] AS a (NOLOCK)
                     INNER JOIN [dbo].[VXS_CUST_KEYWORD] AS c (NOLOCK) ON a.[expanded_customer_no] = c.[customer_no]
                     INNER JOIN @customer_table AS cu ON cu.[customer_no] = a.[customer_no]
                WHERE a.[name_ind] = -2 
                  AND a.[cust_type] + a.[name_ind] <> 7 
                  AND c.[keyword_no] = 630)

        INSERT INTO [#cust_info] ([customer_no], [customer_name], [customer_type_no], [customer_type_name], [prospect_mgr], [contact_pm_a1], [contact_pm_a2],
                                   [affiliate_customer_no], [affiliate_type_no], [affiliate_type_name], [affiliate_name], [name_ind], [name_ind_sort])

        --Primary Records (A1 and A2 If Household)
        SELECT nam.[customer_no],
               nam.[best_name],
               cus.[customer_type],
               ISNULL(cus.[customer_type_name], '') AS [customer_type_name],
               ISNULL(mgr.[prospect_mgr], '') AS [prospect_mgr],
               ISNULL(ca1.[contact_pm_a1], '') AS [contact_pm_a1],
               ISNULL(ca2.[contact_pm_a2], '') AS [contact_pm_a2],
               ISNULL(aff.[individual_customer_no], 0) AS [individual_customer_no],
               ISNULL(aff.[affiliation_type_id], 0) AS [affiliation_type_id],
               ISNULL(atp.[description], '') AS [affiliation_type],
               ISNULL(nm2.[best_name], '') AS [affiliate_name],
               ISNULL(aff.[name_ind],0) AS [name_ind],
               CASE ISNULL(aff.[name_ind], 0) WHEN -1 THEN 'A'
                                              WHEN -2 THEN 'B'
                                              WHEN 0 THEN 'C'
                                              ELSE 'D' END AS [name_ind_sort]
        FROM [dbo].[LV_ADV_BEST_NAME] AS nam
             INNER JOIN @customer_table AS cus ON cus.[customer_no] = nam.[customer_no]
             LEFT OUTER JOIN [CTE_PROSPECT_MGR] AS mgr ON mgr.[customer_no] = nam.[customer_no]
             LEFT OUTER JOIN [CTE_CONTACT_PM_A1] AS ca1 ON ca1.[customer_no] = nam.[customer_no]
             LEFT OUTER JOIN [CTE_CONTACT_PM_A2] AS ca2 ON ca2.[customer_no] = nam.[customer_no]
             LEFT OUTER JOIN [dbo].[T_AFFILIATION] AS aff ON aff.[group_customer_no] = nam.[customer_no] AND aff.[affiliation_type_id] = 10002 AND aff.[end_dt] IS NULL
             LEFT OUTER JOIN [dbo].[TR_AFFILIATION_TYPE] AS atp ON atp.[id] = aff.[affiliation_type_id]
             LEFT OUTER JOIN [dbo].[LV_ADV_BEST_NAME] AS nm2 ON nm2.[customer_no] = aff.[individual_customer_no]
        
        UNION 

        --Other Family Records (Affiliations)
        SELECT nam.[customer_no],
                     nam.[best_name],
                     cus.[customer_type],
                     ISNULL(cus.[customer_type_name], '') AS [customer_type_name],
                     ISNULL(mgr.[prospect_mgr], '') AS [prospect_mgr],
                     ISNULL(ca1.[contact_pm_a1], '') AS [contact_pm_a1],
                     ISNULL(ca2.[contact_pm_a2], '') AS [contact_pm_a2],
                     ISNULL(aff.[individual_customer_no], 0) AS [individual_customer_no],
                     ISNULL(aff.[affiliation_type_id], 0) AS [affiliation_type_id],
                     ISNULL(atp.[description], '') AS [affiliation_type],
                     ISNULL(nm2.[best_name], '') AS [affiliate_name],
                     ISNULL(aff.[name_ind], 0) AS [name_ind],
                     CASE ISNULL(aff.[name_ind], 0) WHEN -1 THEN 'A'
                                                    WHEN -2 THEN 'B'
                                                    WHEN 0 THEN 'C'
                                                    ELSE 'D' END AS [name_ind_sort]
        FROM [dbo].[LV_ADV_BEST_NAME] AS nam
             INNER JOIN @customer_table AS cus ON cus.[customer_no] = nam.[customer_no]
             LEFT OUTER JOIN [CTE_PROSPECT_MGR] AS mgr ON mgr.[customer_no] = nam.[customer_no]
             LEFT OUTER JOIN [CTE_CONTACT_PM_A1] AS ca1 ON ca1.[customer_no] = nam.[customer_no]
             LEFT OUTER JOIN [CTE_CONTACT_PM_A2] AS ca2 ON ca2.[customer_no] = nam.[customer_no]
             INNER JOIN [dbo].[T_AFFILIATION] AS aff ON aff.[group_customer_no] = nam.[customer_no] AND aff.[end_dt] IS NULL AND aff.[affiliation_type_id] IN (SELECT [id] 
                                                                                                                                                               FROM [dbo].[TR_AFFILIATION_TYPE] 
                                                                                                                                                               WHERE [relationship_category_id] = 2)
             INNER JOIN [dbo].[TR_AFFILIATION_TYPE] AS atp ON atp.[id] = aff.[affiliation_type_id]
             INNER JOIN [dbo].[LV_ADV_BEST_NAME] AS nm2 ON nm2.[customer_no] = aff.[individual_customer_no]


        INSERT INTO [#cust_info] ([customer_no], [customer_name], [customer_type_no], [customer_type_name], [prospect_mgr], [contact_pm_a1], [contact_pm_a2],
                                   [affiliate_customer_no], [affiliate_type_no], [affiliate_type_name], [affiliate_name], [name_ind], [name_ind_sort])

        --Other Family Records (Associations on A1 and A2)
        SELECT cus.[customer_no],
               cus.[customer_name],
               cus.[customer_type_no],
               cus.[customer_type_name],
               cus.[prospect_mgr],
               cus.[contact_pm_a1],
               cus.[contact_pm_a2],
               aso.[associated_customer_no],
               aso.[association_type_id],
               stp.[description],
               nm1.[best_name],
               cus.[name_ind],
               'D'            
        FROM [#cust_info] AS cus
             INNER JOIN [dbo].[T_ASSOCIATION] AS aso ON aso.[customer_no] = cus.[affiliate_customer_no]
             INNER JOIN [dbo].[TR_ASSOCIATION_TYPE] AS stp ON aso.[association_type_id] = stp.[id]
             INNER JOIN [dbo].[LV_ADV_BEST_NAME] AS nm1 ON nm1.[customer_no] = aso.[associated_customer_no]
         WHERE cus.[name_ind] IN (-1, -2)
           AND stp.[relationship_category_id] = 2

         UNION 

        --Other Family Records (Associations on Household)         
         SELECT DISTINCT cus.[customer_no],
                         cus.[customer_name],
                         cus.[customer_type_no],
                         cus.[customer_type_name],
                         cus.[prospect_mgr],
                         cus.[contact_pm_a1],
                         cus.[contact_pm_a2],
                         aso.[associated_customer_no],
                         aso.[association_type_id],
                         stp.[description],
                         nm1.[best_name],
                         0,
                         'D'
        FROM [#cust_info] AS cus
             INNER JOIN [dbo].[T_ASSOCIATION] AS aso ON aso.[customer_no] = cus.[customer_no]
             INNER JOIN [dbo].[TR_ASSOCIATION_TYPE] AS stp ON aso.[association_type_id] = stp.[id]
             INNER JOIN [dbo].[LV_ADV_BEST_NAME] AS nm1 ON nm1.[customer_no] = aso.[associated_customer_no]
        WHERE cus.[name_ind] NOT IN (-1, -2)
          AND stp.[relationship_category_id] = 2


        INSERT INTO [#cust_info] ([customer_no], [customer_name], [customer_type_no], [customer_type_name], [prospect_mgr], [contact_pm_a1], [contact_pm_a2],
                                   [affiliate_customer_no], [affiliate_type_no], [affiliate_type_name], [affiliate_name], [name_ind], [name_ind_sort])
        
        --Other Contact Records (CFG Contact on A1 and A2)
        SELECT  cus.[customer_no],
                cus.[customer_name],
                cus.[customer_type_no],
                cus.[customer_type_name],
                cus.[prospect_mgr],
                cus.[contact_pm_a1],
                cus.[contact_pm_a2],
                aff.[group_customer_no],
                aff.[affiliation_type_id],
               atp.[description] "CFG_Contact", 
               nam.[best_name],
               cus.[name_ind],
               'E'
        FROM [#cust_info] AS cus
             INNER JOIN [dbo].[T_AFFILIATION] AS aff ON aff.[individual_customer_no] = cus.[affiliate_customer_no]
             INNER JOIN [dbo].[TR_AFFILIATION_TYPE] AS atp ON atp.[id] = aff.[affiliation_type_id]
             INNER JOIN [dbo].[LV_ADV_BEST_NAME] AS nam ON nam.[customer_no] = aff.[group_customer_no]
        WHERE cus.name_ind IN (-2, -1) 
          AND aff.affiliation_type_id IN (10026, 10029) 

        UNION

        --Other Contact Records (CFG Contact on Household)
        SELECT  cus.[customer_no],
                cus.[customer_name],
                cus.[customer_type_no],
                cus.[customer_type_name],
                cus.[prospect_mgr],
                cus.[contact_pm_a1],
                cus.[contact_pm_a2],
                aff.[group_customer_no],
                aff.[affiliation_type_id],
               atp.[description] "CFG_Contact", 
               nam.[best_name],
               cus.[name_ind],
               'E'
        FROM [#cust_info] AS cus
             INNER JOIN [dbo].[T_AFFILIATION] AS aff ON aff.[individual_customer_no] = cus.[affiliate_customer_no]
             INNER JOIN [dbo].[TR_AFFILIATION_TYPE] AS atp ON atp.[id] = aff.[affiliation_type_id]
             INNER JOIN [dbo].[LV_ADV_BEST_NAME] AS nam ON nam.[customer_no] = aff.[group_customer_no]
        WHERE cus.name_ind NOT IN (-2, -1) 
          AND aff.affiliation_type_id IN (10026, 10029) 

    /*  If not Household or Individual, Remove */

        --UPDATE [#cust_info]
        --SET [contact_pm_a1] = '',
        --    [contact_pm_a2] = ''
        --WHERE [customer_type_no] NOT IN (1, 7)
      
    FINISHED:

        /*  Pull Final Data Set  */

            SELECT [customer_no],
                   [customer_name],
                   [customer_type_no],
                   [customer_type_name],
                   [prospect_mgr],
                   [contact_pm_a1],
                   [contact_pm_a2],
                   [affiliate_customer_no],
                   [affiliate_type_no],
                   [affiliate_type_name] + CASE WHEN [name_ind] = -1 THEN ' (A1)'
                                                WHEN [name_ind] = -2 THEN ' (A2)'
                                                ELSE '' END AS [affiliate_type_name],
                   [affiliate_name],
                   [name_ind],
                   [name_ind_sort]
            FROM [#cust_info]
            ORDER BY [customer_no], [name_ind_sort]
    
END
GO

