USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_ADV_CAMPAIGN_ACTIVITY_5YEAR]') AND type IN (N'P', N'PC'))
BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_ADV_CAMPAIGN_ACTIVITY_5YEAR] AS' 
    EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LRP_ADV_CAMPAIGN_ACTIVITY_5YEAR] TO impusers'
END
GO

/*      LRP_ADV_CAMPAIGN_ACTIVITY_5YEAR
        Retrieves donor counts and contribution dollar amounts for the current and the previous four fiscal years for comparison.
        
        NOTE: Uses LV_CAMPAIGN_ACTIVITY_CONTRIBUTIONS view which is a view that was created using the
              same logic in the MOS Advancement Contributions report (LRP_MOS_ADVANCEMENT_CONTRIBUTION_DETAIL).
*/
ALTER PROCEDURE [dbo].[LRP_ADV_CAMPAIGN_ACTIVITY_5YEAR]
        @fiscal_year INT = NULL
WITH RECOMPILE AS BEGIN

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

    /*  Check Fiscal Year Parameter - If none passed, set to current  */
        IF @fiscal_year IS NULL SELECT @fiscal_year = dbo.LF_GetFiscalYear(GETDATE())

    /*  Procedure Variables  */

        --Determine current fiscal year date information
        DECLARE @fiscal_start_dt DATETIME = [dbo].[LF_GetFiscalYearStartDt](@fiscal_year)
        DECLARE @fiscal_end_dt DATETIME = [dbo].[LF_GetFiscalYearEndDt](@fiscal_year)
        DECLARE @current_dt DATETIME = GETDATE()
        IF @current_dt > @fiscal_end_dt SELECT @current_dt = @fiscal_end_dt

        --Determine fiscal year date information for four years back
        DECLARE @first_fiscal_year INT = (@fiscal_year - 4)         
        DECLARE @first_fiscal_start_dt DATETIME = [dbo].[LF_GetFiscalYearStartDt](@first_fiscal_year)
        DECLARE @first_fiscal_end_dt DATETIME = [dbo].[LF_GetFiscalYearEndDt](@first_fiscal_year)
        
        DECLARE @current_fiscal_goal DECIMAL(18,2) = 0.0, @current_fiscal_total DECIMAL(18,2) = 0.0

        DECLARE @givng_levels TABLE ([giv_level] INT NOT NULL DEFAULT (0), 
                                     [giv_minumum] DECIMAL(18,2) NOT NULL DEFAULT (0.0), 
                                     [giv_maximum] DECIMAL(18,2) NOT NULL DEFAULT (0.0), 
                                     [giv_description] VARCHAR(30) NOT NULL DEFAULT (''))

    /*  Temporary Tables  */

        IF OBJECT_ID('tempdb..#contribution_detail') IS NOT NULL DROP TABLE [#contribution_detail]

        CREATE TABLE [#contribution_detail] ([reference_no] INT NOT NULL DEFAULT (0), 
                                             [customer_no] INT NOT NULL DEFAULT (0),
                                             [date] DATE NULL,
                                             [fiscal_year] INT NOT NULL DEFAULT (0),
                                             [fiscal_quarter] INT NOT NULL DEFAULT (0), 
                                             [fiscal_period] INT NOT NULL DEFAULT (0), 
                                             [contribution_amount] DECIMAL(18,2) NOT NULL DEFAULT (0.0), 
                                             [received_amount] DECIMAL(18,2) NOT NULL DEFAULT (0.0), 
                                             [fund_description] VARCHAR(30) NOT NULL DEFAULT (''), 
                                             [printable_fund] VARCHAR(80) NOT NULL DEFAULT (''), 
                                             [designation] VARCHAR(30) NOT NULL DEFAULT (''), 
                                             [overall_campaign] VARCHAR(30) NOT NULL DEFAULT (''), 
                                             [campaign_no] INT NOT NULL DEFAULT (0), 
                                             [campaign] VARCHAR(30) NOT NULL DEFAULT (''), 
                                             [campaign_category] VARCHAR(30) NOT NULL DEFAULT (''), 
                                             [batch] INT NOT NULL DEFAULT (0), 
                                             [solicitor_number] INT NULL DEFAULT (0), 
                                             [solicitor_name] VARCHAR(55) NOT NULL DEFAULT (''), 
                                             [full_fund_name] VARCHAR(80) NOT NULL DEFAULT (''), 
                                             [cm_category1] VARCHAR(80) NOT NULL DEFAULT (''), 
                                             [business_unit] VARCHAR(100) NOT NULL DEFAULT (''))

        IF OBJECT_ID('tempdb..#contribution_totals') IS NOT NULL DROP TABLE [#contribution_totals]

        CREATE TABLE [#contribution_totals] ([report_section_no] INT NOT NULL DEFAULT (0),
                                             [report_section] VARCHAR(30) NOT NULL DEFAULT (''),
                                             [fiscal_year] INT NOT NULL DEFAULT (0),
                                             [is_current_year] INT NOT NULL DEFAULT (0),  
                                             [customer_no] INT NOT NULL DEFAULT (0), 
                                             [contribution_total] DECIMAL(18,2) NOT NULL DEFAULT (0.0), 
                                             [received_total] DECIMAL(18,2) NOT NULL DEFAULT (0.0), 
                                             [contribution_level] VARCHAR(30) NOT NULL DEFAULT (''),
                                             [contribution_level_order] INT NOT NULL DEFAULT (0),
                                             [current_fiscal_year_total] DECIMAL (18,2) NOT NULL DEFAULT (0.0),
                                             [current_fiscal_year_goal] DECIMAL (18,2) NOT NULL DEFAULT (0.0))

        /*  These are the giving levels that were given to me
            If they ever change, it is very important that no two levels overlap in this table.
            If one value could fall into multiple levels, it could cause the report to fail or produce incorrect data.  */

            INSERT INTO @givng_levels ([giv_level], [giv_minumum], [giv_maximum], [giv_description])
            VALUES (1, 1000000.00, 10000000000.00, '$1,000,000 +'),        (2, 500000.00, 999999.99, '$500,000 to $999,999'),
                   (3, 250000.00, 499999.99, '$250,000 to $499,999'),      (4, 100000.00, 249999.99, '$100,000 to $249,999'),
                   (5, 50000.00, 99999.99, '$50,000 to $99,999'),          (6, 25000.00, 49999.99, '$25,000 to $49,999'),
                   (7, 10000.00, 24999.99, '$10,000 to $24,999'),          (8, 5000.00, 9999.99, '$5,000 to $9,999'),
                   (9, 1000.00, 4999.99, '$1,000 to $4,999'),              (10, 500, 999.99, '$500 to $999'),
                   (11, 250.00, 499.99, '$250 to $499'),                   (12, 100, 249.99, '$100 to $249'),
                   (13, 50.00, 99.99, '$50 to $99'),                       (14, 25, 49.99, '$25 to $49'),
                   (15, 0.00, 24.99, 'Under $25')

    /*  Pull contributions for current fiscal plus previous four fiscal years  */

            INSERT INTO [#contribution_detail] ([reference_no], [customer_no], [date], [fiscal_year], [fiscal_quarter], [fiscal_period], [contribution_amount], [received_amount], 
                                                [fund_description], [printable_fund], [designation], [overall_campaign], [campaign_no], [campaign], [campaign_category],
                                                [batch], [solicitor_number], [solicitor_name], [full_fund_name], [cm_category1], [business_unit])
            SELECT DISTINCT [reference_no],
                            [customer_no],
			                [contribution_date],
			                [fiscal_year],
			                [fiscal_quarter],
			                [fiscal_period], 
			                [contribution_amount],
			                [received_amount],
			                [fund_description],
			                [printable_fund],
			                [designation],
			                [overall_campaign],
                            [campaign_no],
			                [campaign],
			                [campaign_category],
			                [batch],
			                ISNULL([solicitor_number],0),
			                ISNULL([solicitor_name],''),
			                [full_fund_name],
			                [cm_category1],
                            [Business_Unit]
            FROM [dbo].[LV_CAMPAIGN_ACTIVITY_CONTRIBUTIONS]
	        WHERE [contribution_date] BETWEEN @first_fiscal_start_dt AND @current_dt
              AND [customer_no] > 0
              AND [campaign_category] <> 'Skip'
	        
    /*  When adding the various data elements to the totals table, each is flagged as report section 1 or 2  
        This allows them to be broken up more easily on the report without having to run multiple procedures  */

    /*  Aggregate contributions (report section 1) */

        INSERT INTO [#contribution_totals] ([report_section_no], [report_section], [fiscal_year], [is_current_year], [customer_no], 
                                            [contribution_total], [received_total]) 
        SELECT 1,
               'donors',
               [fiscal_year], 
               CASE WHEN [fiscal_year] = @fiscal_year THEN 1 ELSE 0 END,
               [customer_no], 
               SUM([contribution_amount]) AS [contribution_total], 
               SUM([received_amount]) AS [received_total]
        FROM [#contribution_detail]
        GROUP BY [fiscal_year], [customer_no]

    /*  Done as an update statement rather than within the query to avoid a huge case statement  */

        UPDATE tot
        SET tot.[contribution_level] = lev.[giv_description],
            tot.[contribution_level_order] = lev.[giv_level]
        FROM [#contribution_totals] AS tot
             INNER JOIN @givng_levels AS lev ON tot.[contribution_total] BETWEEN lev.[giv_minumum] AND lev.[giv_maximum]
        
    /*  Get current fiscal year total and goal  */
       
        SELECT @current_fiscal_total = SUM([contribution_amount]) FROM [#contribution_detail] WHERE [fiscal_year] = @fiscal_year
        
        SELECT @current_fiscal_goal = ISNULL(SUM(ISNULL([Unrestricted_Goal],0) + ISNULL([Restricted_Goal],0)),0)
                                      FROM [dbo].[LTR_CAMPAIGN_BUSINESS_UNIT] 
                                      WHERE [campaign_no] IN (SELECT [campaign_no] FROM [dbo].[T_CAMPAIGN] WHERE fyear = @fiscal_year)

    /*  Add goals to the totals table (report section 2)  */

        INSERT INTO [#contribution_totals] ([report_section_no], [report_section], [fiscal_year], [is_current_year], 
                                            [current_fiscal_year_total], [current_fiscal_year_goal])
        SELECT 2,
              'goals', 
              @fiscal_year, 
              1,
              @current_fiscal_total, 
              @current_fiscal_goal

    FINISHED:
    
        /*  Select final data set to return to the report  */

            SELECT [report_section_no],
                   [report_section],
                   [fiscal_year],
                   [is_current_year],
                   [customer_no],
                   [contribution_total],
                   [received_total],
                   [contribution_level],
                   [contribution_level_order],
                   [current_fiscal_year_total],
                   [current_fiscal_year_goal]
            FROM [#contribution_totals]
            ORDER BY report_section_no, fiscal_year, contribution_level_order DESC

    DONE:

        IF OBJECT_ID('tempdb..#contribution_totals') IS NOT NULL DROP TABLE [#contribution_totals]
        IF OBJECT_ID('tempdb..#contribution_detail') IS NOT NULL DROP TABLE [#contribution_detail]

END
GO

EXECUTE [dbo].[LRP_ADV_CAMPAIGN_ACTIVITY_5YEAR] @fiscal_year = 2020


