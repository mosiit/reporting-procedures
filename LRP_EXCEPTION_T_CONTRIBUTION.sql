USE [impresario]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[LRP_EXCEPTION_T_CONTRIBUTION]
AS
-- T_CONTRIBUTION Exception reporting
-- DSJ 5/24/2016

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

-----------------------------
-- T_CONTRIBUTION discrepancies 
-----------------------------
SELECT ref_no, customer_no, batch_no, cont_type, CONVERT(VARCHAR(10), cont_dt, 101) AS cont_dt, cont_amt, created_by, last_updated_by,
	'' AS creditee_no, '' AS [description], 
	'customer_no should not = 0 when cont_amt > 0' AS reason
FROM T_CONTRIBUTION 
WHERE customer_no = 0 AND cont_amt > 0

UNION ALL 

SELECT ref_no, customer_no, batch_no, cont_type, CONVERT(VARCHAR(10), cont_dt, 101) AS cont_dt, cont_amt, created_by, last_updated_by,
	'' AS creditee_no, '' AS [description], 
	'initiator_no should be NULL' AS reason
FROM T_CONTRIBUTION 
WHERE initiator_no IS NOT NULL

UNION ALL 

SELECT ref_no, customer_no, batch_no, cont_type, CONVERT(VARCHAR(10), cont_dt, 101) AS cont_dt, cont_amt, created_by, last_updated_by,
	'' AS creditee_no, '' AS [description], 
	'custom_1 should not be NULL' AS reason
FROM T_CONTRIBUTION 
WHERE custom_1 IS NULL

UNION ALL 

SELECT ref_no, customer_no, batch_no, cont_type, CONVERT(VARCHAR(10), cont_dt, 101) AS cont_dt, cont_amt, created_by, last_updated_by,
	'' AS creditee_no, '' AS [description], 
	'custom_5 should not be NULL. It should be "N".' AS reason
FROM T_CONTRIBUTION 
WHERE custom_5 is NULL

UNION ALL 

SELECT ref_no, customer_no, batch_no, cont_type, CONVERT(VARCHAR(10), cont_dt, 101) AS cont_dt, cont_amt, created_by, last_updated_by,
	'' AS creditee_no, '' AS [description], 
	'custom_1 = "Stewardship Soft Credit" AND customer_no <> 2653093' AS reason
FROM T_CONTRIBUTION 
WHERE custom_1 = 'Stewardship Soft Credit' AND customer_no <> 2653093

UNION ALL 

SELECT cont.ref_no, cont.customer_no, cont.batch_no, cont.cont_type, CONVERT(VARCHAR(10), cont.cont_dt, 101) AS cont_dt, cont.cont_amt, cont.created_by, cont.last_updated_by,
	'' AS creditee_no, '' AS [description], 
	'Contribution designation (' + LTRIM(RTRIM(STR(cont_designation))) + ') <> Fund Detail designation (' + LTRIM(RTRIM(STR(det.designation))) + ')' AS reason
FROM T_CONTRIBUTION cont
	INNER JOIN dbo.LTR_FUND_DETAIL det
	ON cont.fund_no = det.fund_no
WHERE cont.cont_designation <> det.designation

UNION ALL 

SELECT cont.ref_no, cont.customer_no, cont.batch_no, cont.cont_type, CONVERT(VARCHAR(10), cont.cont_dt, 101) AS cont_dt, cont.cont_amt, cont.created_by, cont.last_updated_by,
	'' AS creditee_no, '' AS [description], 
	'custom_8 should not be NULL for Planned Gifts' AS reason
FROM T_CONTRIBUTION cont
	INNER JOIN dbo.T_CAMPAIGN cam 
	ON cont.campaign_no = cam.campaign_no
WHERE cam.category = 29 AND cont.custom_8 IS NULL 

UNION ALL

SELECT cont.ref_no, cont.customer_no, cont.batch_no, cont.cont_type, CONVERT(VARCHAR(10), cont.cont_dt, 101) AS cont_dt, cont.cont_amt, cont.created_by, cont.last_updated_by,
	'' AS creditee_no, '' AS [description], 
	'contribution has more than one of the following creditee_types (5,12,15)' AS reason
FROM dbo.T_CONTRIBUTION cont
INNER JOIN 
(
	SELECT c.ref_no, COUNT(*) cnt
	FROM dbo.T_CONTRIBUTION c
		INNER JOIN dbo.T_CREDITEE ee
			ON c.ref_no = ee.ref_no
	WHERE creditee_type IN (5,12,15)
	GROUP BY c.ref_no
	HAVING COUNT(*) > 1
) X
ON cont.ref_no = x.ref_no

UNION ALL

SELECT cont.ref_no, cont.customer_no, cont.batch_no, cont.cont_type, CONVERT(VARCHAR(10), cont.cont_dt, 101) AS cont_dt, cont.cont_amt, cont.created_by, cont.last_updated_by,
	'' AS creditee_no, '' AS [description], 
	'Contribution should be on Household' AS reason
FROM T_CONTRIBUTION AS cont 
INNER JOIN T_CUSTOMER AS cu 
	ON cont.customer_no = cu.customer_no 
INNER JOIN T_AFFILIATION AS a 
	ON cont.customer_no = a.individual_customer_no
WHERE cu.cust_type = 1 AND a.name_ind in ('-1', '-2')

UNION ALL 
-----------------------------
-- T_PLANS discrepancies 
-----------------------------

SELECT cont.ref_no, cont.customer_no, cont.batch_no, cont.cont_type, CONVERT(VARCHAR(10), cont.cont_dt, 101) AS cont_dt, cont.cont_amt, cont.created_by, cont.last_updated_by,
	'' AS creditee_no, '' AS [description], 
	'There is only 1 role and it is solicitor.' AS reason
FROM T_CONTRIBUTION cont
	INNER JOIN dbo.T_PLAN p
		ON cont.plan_no = p.plan_no
	INNER JOIN 
	(
	SELECT cp.plan_no, COUNT(*) cnt
	FROM [dbo].[TX_CUST_PLAN] cp 
	GROUP BY cp.plan_no
	) AS X
	ON p.plan_no = X.plan_no
	INNER JOIN tx_cust_plan cplan
		ON cplan.plan_no = X.plan_no
WHERE cont.worker_customer_no = 2653100 AND X.cnt = 1 AND cplan.role_no = 1

UNION ALL 

-----------------------------
-- CREDITEE DISCREPANCIES
-- fields: If T_CREDITEE is involved, : creditee_no, creditee type description
-----------------------------

SELECT cont.ref_no, cont.customer_no, cont.batch_no, cont.cont_type, CONVERT(VARCHAR(10), cont.cont_dt, 101) AS cont_dt, cont.cont_amt, cont.created_by, cont.last_updated_by,
	ee.creditee_no, typ.[descriptiuon], 
	CASE 
		WHEN DATEDIFF(d, cont.cont_dt, ee.credit_dt) <> 0 THEN 
			'Contribution date (' + CONVERT(VARCHAR(10), cont.cont_dt, 101) + ') does not equal Creditee date (' + CONVERT(VARCHAR(10), ee.credit_dt, 101) + ')'
		WHEN cont.cont_amt <> ee.credit_amt THEN 
			'Contribution amount (' + CONVERT(VARCHAR(30), cont.cont_amt, 0) + ') does not equal Creditee amount (' + CONVERT(VARCHAR(30), ee.credit_amt, 0) + ')'
		ELSE ''
		END AS reason 
FROM T_CONTRIBUTION cont
	INNER JOIN dbo.T_CREDITEE ee
		ON cont.ref_no = ee.ref_no
	INNER JOIN dbo.TR_CREDITEE_TYPE typ
		ON ee.creditee_type = typ.id
WHERE cont.cont_dt <> ee.credit_dt OR cont.cont_amt <> ee.credit_amt

UNION ALL

SELECT cont.ref_no, cont.customer_no, cont.batch_no, cont.cont_type, CONVERT(VARCHAR(10), cont.cont_dt, 101) AS cont_dt, cont.cont_amt, cont.created_by, cont.last_updated_by,
	ee.creditee_no, typ.[descriptiuon], 
	CASE ee.creditee_type
		WHEN 12 THEN 'custom_1 should be "Primary Soft Credit" because creditee_type = 12'
		WHEN 15 THEN 'custom_1 should be "Primary Soft Credit" because creditee_type = 15'
		WHEN 5 THEN 'custom_1 should be "Primary Soft Credit" because creditee_type = 5'
		WHEN 1 THEN 'custom_1 should be "Matching Gift Credit" because creditee_type = 1'
		WHEN 14 THEN 'custom_1 should be "Matching Gift Credit" because creditee_type = 14'
		WHEN 10 THEN 'custom_1 should be "Stewardship Soft Credit" because creditee_type = 10'
		WHEN 16 THEN 'custom_1 should be "Stewardship Soft Credit" because creditee_type = 16'
		ELSE ''
		END AS reason 
FROM T_CONTRIBUTION cont
	INNER JOIN dbo.T_CREDITEE ee
		ON cont.ref_no = ee.ref_no
	INNER JOIN dbo.TR_CREDITEE_TYPE typ
		ON ee.creditee_type = typ.id
WHERE cont.custom_1 = '(none)' AND ee.creditee_type IN (1, 5, 10, 12, 14, 15, 16)

UNION ALL 

SELECT cont.ref_no, cont.customer_no, cont.batch_no, cont.cont_type, CONVERT(VARCHAR(10), cont.cont_dt, 101) AS cont_dt, cont.cont_amt, cont.created_by, cont.last_updated_by, 
	'' AS creditee_no, '' AS [description], 
	'cont.custom_1 = "Primary Soft Credit" AND ee.creditee_type NOT IN (5,12,15)' AS reason 
FROM dbo.T_CONTRIBUTION cont
LEFT JOIN 
(
	SELECT DISTINCT c.ref_no
	FROM T_CONTRIBUTION c
		INNER JOIN dbo.T_CREDITEE ee
			ON c.ref_no = ee.ref_no
	WHERE c.custom_1 = 'Primary Soft Credit' AND ee.creditee_type IN (5,12,15)
) AS Y
ON cont.ref_no = Y.ref_no 
WHERE Y.ref_no IS NULL AND cont.custom_1 = 'Primary Soft Credit' 

UNION ALL

SELECT cont.ref_no, cont.customer_no, cont.batch_no, cont.cont_type, CONVERT(VARCHAR(10), cont.cont_dt, 101) AS cont_dt, cont.cont_amt, cont.created_by, cont.last_updated_by, 
	'' AS creditee_no, '' AS [description], 
	'cont.custom_1 = "Matching Gift Credit" AND ee.creditee_type NOT IN (1,14)' AS reason 
FROM dbo.T_CONTRIBUTION cont
LEFT JOIN 
(
	SELECT DISTINCT c.ref_no
	FROM T_CONTRIBUTION c
		INNER JOIN dbo.T_CREDITEE ee
			ON c.ref_no = ee.ref_no
	WHERE c.custom_1 = 'Matching Gift Credit' AND ee.creditee_type IN (1,14)
) AS Y
ON cont.ref_no = Y.ref_no 
WHERE Y.ref_no IS NULL AND cont.custom_1 = 'Matching Gift Credit' 

UNION ALL

SELECT cont.ref_no, cont.customer_no, cont.batch_no, cont.cont_type, CONVERT(VARCHAR(10), cont.cont_dt, 101) AS cont_dt, cont.cont_amt, cont.created_by, cont.last_updated_by, 
	'' AS creditee_no, '' AS [description], 
	'cont.custom_1 = "Stewardship Soft Credit" AND ee.creditee_type NOT IN (10,16)' AS reason 
FROM dbo.T_CONTRIBUTION cont
LEFT JOIN 
(
	SELECT DISTINCT c.ref_no
	FROM T_CONTRIBUTION c
		INNER JOIN dbo.T_CREDITEE ee
			ON c.ref_no = ee.ref_no
	WHERE c.custom_1 = 'Stewardship Soft Credit' AND ee.creditee_type IN (10,16)
) AS Y
ON cont.ref_no = Y.ref_no 
WHERE Y.ref_no IS NULL AND cont.custom_1 = 'Stewardship Soft Credit' 

UNION ALL

SELECT cont.ref_no, cont.customer_no, cont.batch_no, cont.cont_type, CONVERT(VARCHAR(10), cont.cont_dt, 101) AS cont_dt, cont.cont_amt, cont.created_by, cont.last_updated_by, 
	'' AS creditee_no, '' AS [description], 
	'cont.custom_1 = "Matching Gift Credit" AND ee.creditee_type IN (1,14) AND IN (5,12,15)' AS reason 
FROM dbo.T_CONTRIBUTION cont
INNER JOIN 
(
	SELECT DISTINCT c.ref_no
	FROM T_CONTRIBUTION c
		INNER JOIN dbo.T_CREDITEE ee
			ON c.ref_no = ee.ref_no
	WHERE c.custom_1 = 'Matching Gift Credit' AND ee.creditee_type IN (1,14)
) AS Y
ON cont.ref_no = Y.ref_no 
INNER JOIN 
(
	SELECT DISTINCT c.ref_no
	FROM T_CONTRIBUTION c
		INNER JOIN dbo.T_CREDITEE ee
			ON c.ref_no = ee.ref_no
	WHERE c.custom_1 = 'Matching Gift Credit' AND ee.creditee_type IN (5,12,15)
) AS X
ON cont.ref_no = X.ref_no 
WHERE cont.custom_1 = 'Matching Gift Credit' 

UNION ALL

SELECT cont.ref_no, cont.customer_no, cont.batch_no, cont.cont_type, CONVERT(VARCHAR(10), cont.cont_dt, 101) AS cont_dt, cont.cont_amt, cont.created_by, cont.last_updated_by, 
	'' AS creditee_no, '' AS [description], 
	'cont.custom_1 = "Stewardship Soft Credit" AND ee.creditee_type IN (10,16) AND IN (1,5,12,14,15)' AS reason 
FROM dbo.T_CONTRIBUTION cont
INNER JOIN 
(
	SELECT DISTINCT c.ref_no
	FROM T_CONTRIBUTION c
		INNER JOIN dbo.T_CREDITEE ee
			ON c.ref_no = ee.ref_no
	WHERE c.custom_1 = 'Stewardship Soft Credit' AND ee.creditee_type IN (10,16)
) AS Y
ON cont.ref_no = Y.ref_no 
INNER JOIN 
(
	SELECT DISTINCT c.ref_no
	FROM T_CONTRIBUTION c
		INNER JOIN dbo.T_CREDITEE ee
			ON c.ref_no = ee.ref_no
	WHERE c.custom_1 = 'Stewardship Soft Credit' AND ee.creditee_type IN (1,5,12,14,15)
) AS X
ON cont.ref_no = X.ref_no 
WHERE cont.custom_1 = 'Stewardship Soft Credit' 

UNION ALL 
-----------------------------
-- CAMPAIGN DISCREPANCIES
-----------------------------
-- This looks for Leadership gifts that are under 25K.
SELECT c.ref_no, c.customer_no, c.batch_no, c.cont_type, CONVERT(VARCHAR(10), c.cont_dt, 101) AS cont_dt, c.cont_amt, c.created_by, c.last_updated_by,
	'' AS creditee_no, '' AS [description], 
	'Check Campaign_Amt' AS reason 
FROM T_CONTRIBUTION AS c
INNER JOIN T_CAMPAIGN AS g
	ON c.campaign_no = g.campaign_no
WHERE CAST(c.cont_dt AS DATE) >= '2018-07-01' 
	AND c.cont_amt < 25000 
	AND c.cont_amt > 0 
	AND g.category = 40 

UNION ALL 

--This looks for Annual Giving gifts that are over 25K.
SELECT c.ref_no, c.customer_no, c.batch_no, c.cont_type, CONVERT(VARCHAR(10), c.cont_dt, 101) AS cont_dt, c.cont_amt, c.created_by, c.last_updated_by,
	'' AS creditee_no, '' AS [description], 
	'Check Campaign_Amt' AS reason 
FROM T_CONTRIBUTION AS c
INNER JOIN T_CAMPAIGN AS g
	ON c.campaign_no = g.campaign_no
WHERE CAST(c.cont_dt AS DATE) >= '2018-07-01' 
	AND c.cont_amt >= 25000 
	AND c.cont_amt > 0 
	AND g.category = 28 
	AND c.ref_no <> 183957




ORDER BY reason, cont_dt