USE [impresario]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Debbie Jacob
-- Create date: August 17, 2016
-- Description:	On Account Scholarship Tracking Report 
--
-- Sample Exec:	Execute LRP_ON_ACCT_SCHOLARSHIP_TRACKING @mos_str = '11,12,21'
-- =============================================
ALTER PROCEDURE [dbo].[LRP_ON_ACCT_SCHOLARSHIP_TRACKING]
	@mos_str VARCHAR(MAX)  

AS
BEGIN

SET NOCOUNT ON;

--Create and populate a table variable to hold the desired Modes of Sale
DECLARE @mos TABLE
(
	id INT NOT NULL
)

-- IF @mos_str is NULL, search ALL MOS
IF ISNULL(@mos_str, '') <> ''
BEGIN
	INSERT INTO @mos
	SELECT CONVERT(int,Element) FROM dbo.FT_SPLIT_LIST (@mos_str,',')
END
ELSE 
BEGIN
	INSERT @mos
	SELECT id FROM TR_MOS

END 

SELECT  pay.[order_no],
        pay.[transaction_sequence_no],
        pay.[transaction_no],
        pay.[transaction_create_dt],
        pay.[transaction_created_by],
        pay.[transaction_create_loc],
        pay.[transaction_create_loc_description],
        pay.[transaction_reference_no],
        pay.[transaction_dt],
        pay.[transaction_type_no],
        pay.[transaction_type_name],
        pay.[transaction_amount],
        pay.[transaction_posted],
        pay.[transaction_batch_no],
        pay.[tckt_tran_flag],
        pay.[payment_no],
        pay.[payment_sequence_no],
        pay.[payment_created_by],
        pay.[payment_create_dt],
        pay.[payment_dt],
        pay.[payment_amount],
        pay.[payment_method_no],
        pay.[payment_method_name],
        pay.[payment_account_no],
        pay.[payment_check_no],
        pay.[payment_check_name],
        pay.[payment_cc_reference_no],
        pay.[payment_tendered_amount],
        pay.[payment_act_no],
        pay.[payment_customer_no],
        pay.[payment_customer_name],
        pay.[payment_notes],
		mos.[description] AS 'mode_of_sale',
		cust.lname AS Fund,
		CASE cust.customer_no 
			WHEN 0 THEN NULL
			ELSE cust.customer_no
		END AS fund_customer_no
FROM    [dbo].[LV_ORDER_PAYMENTS] pay
INNER JOIN	[dbo].[T_ORDER] ord
	ON		pay.[order_no] = ord.[order_no]
INNER JOIN	[dbo].[TR_MOS] mos
	ON		ord.[MOS] = mos.[id]
INNER JOIN @mos M
	ON mos.id = M.id
INNER JOIN 
(-- Find all orders who has a payment type of On Acct - Scholarship AND has a net balance (of all On Acct - Scholarship records) <> 0 
	SELECT order_no, SUM(payment_amount) AS tot
	FROM LV_ORDER_PAYMENTS 
	WHERE payment_method_no = 32
	GROUP BY order_no
	HAVING SUM(payment_amount) <> 0 
) X
ON X.order_no = ord.order_no
LEFT JOIN [dbo].[T_CUSTOMER] cust
	ON cust.customer_no = pay.payment_check_no
WHERE pay.payment_no <> 0
ORDER BY order_no, pay.transaction_dt

END
GO
