USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LRP_NIGHTLY_SUMM_GIK_LARGEST]    Script Date: 2/8/2021 2:40:11 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




ALTER PROCEDURE [dbo].[LRP_NIGHTLY_SUMM_GIK_LARGEST]
	@section VARCHAR(300),
	@sortOrder INT,
	@runBy	VARCHAR(300)
AS
-- ===============================================================
-- H. Sheridan, 4/4/2016 - GIK_Largest
-- ===============================================================

BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	--SET @section = 'GIK_Largest'
	--SET	@sortOrder = 9

	DECLARE @tbl_sumGIK TABLE
	(
		[customer_no] [INT] NULL,
		[cont_dt] [DATE] NULL,
		[sumamt] [MONEY] NULL
	);

	DECLARE @tbl_maxGIK TABLE
	(
		[customer_no] [INT] NULL,
		[cont_dt] [DATE] NULL,
		[maxamt] [MONEY] NULL
	);

	DECLARE @tbl_maxGIKFinal TABLE
	(
		[customer_no] [INT] NULL,
		[cont_dt] [DATE] NULL,
		[maxamt] [MONEY] NULL
	);



	INSERT INTO @tbl_sumGIK
		SELECT c.customer_no,
			   CAST(c.cont_dt AS DATE) AS cont_dt,
			   SUM(c.cont_amt) AS sumamt
		FROM VS_CONTRIBUTION_WITH_INITIATOR (NOLOCK) AS c
		INNER JOIN T_CAMPAIGN (NOLOCK) AS g ON c.campaign_no = g.campaign_no
		WHERE (
				  c.cont_type = 'G'
				  AND c.customer_no > 0
				  AND g.category NOT IN ( 8, 9 )
				  AND c.custom_1 IN ( '(none)', 'Matching Gift Credit' )
				  AND c.KG_xfer_dt > '01-01-1900'
				  AND c.cont_amt > 0
				  AND c.role = 3
			  )
			  OR
			  (
				  c.cont_type = 'G'
				  AND c.customer_no > 0
				  AND g.category NOT IN ( 8, 9 )
				  AND c.custom_1 IN ( 'Primary Soft Credit', 'Stewardship Soft Credit', 'Matching Gift Credit' )
				  AND c.KG_xfer_dt > '01-01-1900'
				  AND cont_amt > 0
				  AND c.role = 4
				  AND c.creditee_type IN ( '12', '15', '5', '10', '16', '14', '1' )
			  )
		GROUP BY c.customer_no,
				 CAST(c.cont_dt AS DATE);

	INSERT INTO @tbl_maxGIK
		SELECT customer_no,
			   cont_dt,
			   sumamt AS "maxamt"
		FROM @tbl_sumGIK g1
		WHERE g1.sumamt IN
			  (
				  SELECT MAX(g2.sumamt) "sumamt"
				  FROM @tbl_sumGIK g2
				  WHERE g1.customer_no = g2.customer_no
				  GROUP BY g2.customer_no
			  );

	INSERT INTO @tbl_maxGIKFinal
		SELECT customer_no,
			   cont_dt,
			   maxamt
		FROM @tbl_maxGIK g3
		WHERE g3.cont_dt IN
			  (
				  SELECT MAX(g4.cont_dt) "cont_dt"
				  FROM @tbl_maxGIK g4
				  WHERE g3.customer_no = g4.customer_no
				  GROUP BY g4.customer_no
			  );

	INSERT INTO LT_NIGHTLY_SUMMARY
		SELECT  DISTINCT [customer_no],
				@section,
				ISNULL([maxamt], 0),
				[cont_dt],
				@sortOrder,
				GETDATE(),
				@runBy 
		FROM    @tbl_maxGIKFinal

	--select 'debug', * from LT_NIGHTLY_SUMMARY where section = @section

END


GO


