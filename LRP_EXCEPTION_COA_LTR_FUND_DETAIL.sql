USE [impresario]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- CHART OF ACCOUNTS Exception reporting on LTR_FUND_DETAIL
-- DSJ 06/15/2016
-- MES 01/20/2021 Updated per SCTASK0001778
--                Changed code to update each exception individually rather than in one big case statement
--                This will make it much easier to add and/or remove exceptions from the report when needed.
ALTER PROCEDURE [dbo].[LRP_EXCEPTION_COA_LTR_FUND_DETAIL]
AS BEGIN

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    SET NOCOUNT ON;

    /*  Procedure Variables  */

        DECLARE @results TABLE ([fund_no] INT,                  [description] VARCHAR(50),          [acct_goal_no] INT,
                                [acct_grp_no] INT,              [cpf_flag] CHAR(1),                 [exh_prg_cat_no] INT,
                                [full_fund] VARCHAR(80),        [printable_fund] VARCHAR(80),       [overall_cm_no] INT,
                                [cm_category1] INT,             [cm_category2] INT,                 [cm_category3] INT,
                                [designation] INT,              [fund_create_dt] DATETIME,          [reason] VARCHAR(350))

    /*  GET ALL FUND INFORMATION  */

        INSERT INTO @results ([fund_no], [description], [acct_goal_no], [acct_grp_no], [cpf_flag], [exh_prg_cat_no], [full_fund], [printable_fund], 
                              [overall_cm_no], [cm_category1], [cm_category2], [cm_category3], [designation], [fund_create_dt], [reason])
        SELECT fd.[fund_no], 
               f.[description],
               fd.[acct_goal_no],
               fd.[acct_grp_no],
               fd.[cpf_flag],
               fd.[exh_prg_cat_no],
               fd.[full_fund],
               fd.[printable_fund],
               fd.[overall_cm_no],
               fd.[cm_category1],
               fd.[cm_category2],
               fd.[cm_category3],
               fd.[designation],
               fd.[create_dt],
               ''
        FROM LTR_FUND_DETAIL as fd
	         INNER JOIN T_FUND as f ON fd.fund_no = f.fund_no


    /*  Identify Exceptions  */

        UPDATE @results
        SET [reason] = 'acct_goal_no = 0'
        WHERE ISNULL([acct_goal_no], 0) = 0

        UPDATE @results
        SET [reason] = 'acct_grp_no = 0'
        WHERE ISNULL([acct_grp_no], 0) = 0

        UPDATE @results
        SET [reason] = 'cpf_flag NOT IN (''N'', ''Y'')'
        WHERE ISNULL([cpf_flag], '') NOT IN ('N', 'Y')

        UPDATE @results
        SET [reason] = 'exh_prg_cat_no = 0'
        WHERE ISNULL([exh_prg_cat_no], 0) = 0

        UPDATE @results
        SET [reason] = 'full_fund is null OR full_fund = '' '''
        WHERE ISNULL([full_fund], ' ') = ' '

        UPDATE @results
        SET [reason] = 'printable_fund is null OR printable_fund = '' '''
        WHERE ISNULL([printable_fund], ' ') = ' '

        UPDATE @results
        SET [reason] = 'overall_cm_no = 0'
        WHERE ISNULL([overall_cm_no], 0) = 0

        UPDATE @results
        SET [reason] = 'cm_category1 = 0'
        WHERE ISNULL([cm_category1], 0) = 0

        UPDATE @results
        SET [reason] = 'cm_category2 is NULL'
        WHERE [cm_category2] is NULL

        UPDATE @results
        SET [reason] = 'cm_category3 = 0'
        WHERE ISNULL([cm_category3], 0) = 0

        UPDATE @results
        SET [reason] = 'designation = 0'
        WHERE ISNULL([designation], 0) = 0

    /*  Delete records that are not exceptions  */

        DELETE FROM @results 
        WHERE [reason] = ''

    FINISHED:

        /*  Final Data Set For Report  */

            SELECT [fund_no],
                   [description],
                   [reason]
            FROM @results

END
GO

--EXECUTE [dbo].[LRP_EXCEPTION_COA_LTR_FUND_DETAIL]

