USE [impresario]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[LRP_NSCAN_ATTENDANCE_REPORT](
	@scan_start_dt DATETIME = NULL,
	@scan_end_dt DATETIME = NULL,
	@season_str VARCHAR(4000) = NULL,
	@perf_start_dt DATETIME = NULL,
	@perf_end_dt DATETIME = NULL,
	@memb_org_str VARCHAR(4000) = NULL, -- New parameter (DSJ, 9/6/17)
	@access_areas_str VARCHAR(4000) = NULL, -- New parameter (DSJ, 9/6/17)
	@access_area_only CHAR(1) = 'N',
	@memb_scans_ind CHAR(1),
	@detail_or_summary_ind CHAR(1) = 'S'
)
AS
/*	
	*** DSJ 9/1/2017 -- This is copied from NP_ATTENDANCE_REPORT *** 

	NScan Attendance scan report. Report attendance for performances and access areas, with or without membership scans.
	Detail mode will show constituent names for each scan. (This is done in SSRS)

	Created 5/12/2014 - DWW - #7817
	Modified 5/15/2014- DWW - Minor change to require either seasons or access areas to get results.

Exec NP_ATTENDANCE_REPORT @memb_scans_ind = 'Y', @detail_or_summary_ind = 'D', @season_str = '1,2,3,4,5,6', @access_areas_str = '1'

*/

-- Our Results table:
CREATE TABLE #results(
	perf_or_area CHAR(1),
	perf_or_area_no INT,
	description VARCHAR(30),
	scan_dt DATETIME,
	admission_adult INT,
	admission_child INT,
	admission_other INT,
	memb_level_no INT,
	memb_org_desc VARCHAR(30),
	memb_level_desc VARCHAR(30),
	customer_no INT,
	const_display_name VARCHAR(255),
	sort_name VARCHAR(55)
)


-- Get the list of performances to include
DECLARE @perfs TABLE(
	perf_no INT,
	perf_code VARCHAR(10),
	perf_desc VARCHAR(30)
)

INSERT INTO @perfs
	SELECT p.perf_no, p.perf_code, i.description
		FROM [dbo].T_PERF p
		JOIN [dbo].T_INVENTORY i ON p.perf_no = i.inv_no
	WHERE (p.season IN (SELECT CONVERT(INT,Element) FROM dbo.FT_SPLIT_LIST(@season_str,',')))
	  AND p.perf_dt BETWEEN ISNULL(@perf_start_dt,'1/1/1900') AND ISNULL(@perf_end_dt,'12/31/2999')


-- Get results, from performances first:

INSERT INTO #results(   perf_or_area, perf_or_area_no, description, scan_dt, admission_adult, admission_child, admission_other, 
						memb_level_no, memb_org_desc, memb_level_desc, customer_no, const_display_name, sort_name )
	SELECT  'P' AS perf_or_area,
			p.perf_no,
			p.perf_desc,
			a.attend_dt, 
			a.admission_adult, 
			a.admission_child,
			a.admission_other,
			a.memb_level_no,
			mo.description, 
			ml.description,
			a.customer_no,
			dn.display_name,
			dn.sort_name
		FROM [dbo].T_ATTENDANCE a
		JOIN @perfs p ON a.perf_no = p.perf_no	-- Selected perfs only
		LEFT JOIN [dbo].T_MEMB_LEVEL ml ON a.memb_level_no = ml.memb_level_no
		LEFT JOIN [dbo].T_MEMB_ORG mo ON ml.memb_org_no = mo.memb_org_no
		JOIN [dbo].FT_CONSTITUENT_DISPLAY_NAME() dn ON a.customer_no = dn.customer_no
	WHERE a.attend_dt BETWEEN ISNULL(@scan_start_dt,'1/1/1900') AND ISNULL(@scan_end_dt,'12/31/2999')
	  AND (@memb_scans_ind = 'Y' OR ISNULL(a.memb_level_no,0) = 0)	-- either we are including membership scans, or this is not one
	  AND (ISNULL(@memb_org_str,'') = '' OR CHARINDEX(','+CONVERT(VARCHAR,ml.memb_org_no)+',',','+@memb_org_str+',') > 0) -- DSJ 9/6/17
	  AND @access_area_only = 'N' -- DSJ 9/6/17

UNION

-- Now results from selected access areas, if any
	SELECT  'A' AS perf_or_area,
			na.id,
			na.description,
			a.attend_dt, 
			a.admission_adult, 
			a.admission_child,
			a.admission_other,
			a.memb_level_no,

			mo.description, 
			ml.description,
			a.customer_no,
			dn.display_name,
			dn.sort_name
		FROM [dbo].T_ATTENDANCE a
		JOIN [dbo].TR_NSCAN_ACCESS_AREAS na ON a.area_no = na.id
		LEFT JOIN [dbo].T_MEMB_LEVEL ml ON a.memb_level_no = ml.memb_level_no
		LEFT JOIN [dbo].T_MEMB_ORG mo ON ml.memb_org_no = mo.memb_org_no
		JOIN [dbo].FT_CONSTITUENT_DISPLAY_NAME() dn ON a.customer_no = dn.customer_no
	WHERE a.attend_dt BETWEEN ISNULL(@scan_start_dt,'1/1/1900') AND ISNULL(@scan_end_dt,'12/31/2999')
	  AND na.id IN (SELECT CONVERT(INT,Element) FROM dbo.FT_SPLIT_LIST(@access_areas_str,','))
	  AND (@memb_scans_ind = 'Y' OR ISNULL(a.memb_level_no,0) = 0)	-- either we are including membership scans, or this is not one
	  AND (ISNULL(@memb_org_str,'') = '' OR CHARINDEX(','+CONVERT(VARCHAR,ml.memb_org_no)+',',','+@memb_org_str+',') > 0) -- DSJ 9/6/17


SELECT * FROM #results

RETURN


GO


