USE impresario
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_BATCH_STATUS_REPORT]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LRP_BATCH_STATUS_REPORT]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[LRP_BATCH_STATUS_REPORT]
        @report_start_dt DATETIME = NULL,
        @report_end_dt DATETIME = NULL,
        @operator_location varchar(4000) = NULL,
        @batch_type varchar(2000) = NULL,
        @batch_status VARCHAR(1000) = NULL
AS BEGIN

    /*  Procedure Variables  */   

        DECLARE @rpt_message VARCHAR(100)

        DECLARE @operator_location_list TABLE (operator_location VARCHAR(30))
        DECLARE @batch_type_list TABLE (btype_id_str VARCHAR(10), btype_id INT)
        DECLARE @batch_status_list TABLE (status_code VARCHAR(10))
        DECLARE @data_table TABLE ([batch_no] INT, [batch_type] INT, [batch_type_name] VARCHAR(30), [batch_type_group] INT, [batch_Type_group_name] VARCHAR(30), [cntl_ind] CHAR(1), [create_dt] DATETIME, 
                                   [created_by] VARCHAR(10), [owner] VARCHAR(10), [owner_name] VARCHAR(100), [location] VARCHAR(30), [status] CHAR(1), [status_name] varchar(30), [rpt_message] VARCHAR(100))

        SELECT @rpt_message = ''

    /*  Check Parameters  - Null Dates = Today  */

        SELECT @report_start_dt = ISNULL(@report_start_dt,GETDATE())
        SELECT @report_end_dt = ISNULL(@report_end_dt,GETDATE())

        SELECT @report_start_dt = convert(char(10),@report_start_dt,111) + ' 00:00:00',
               @report_end_dt = convert(char(10),@report_end_dt,111) + ' 23:59:59.957'

        SELECT @operator_location = ISNULL(@operator_location,'')
        SELECT @batch_type = ISNULL(@batch_type,'')
        SELECT @batch_status = ISNULL(@batch_status,'')

    /*  Parse Bach type group parameter  */
    
        IF @operator_location <> '' BEGIN

            /*  Parse Out the individual value as strings */
                INSERT INTO @operator_location_list (operator_location)
                SELECT REPLACE(LTRIM(item),'"','') FROM [dbo].[LFT_SPLIT_DILEMETED_STRING] (@operator_location, ',')

        END
       
       --DELETE FROM [TemporaryData].[dbo].[batch_location_list]
       --INSERT INTO [TemporaryData].[dbo].[batch_location_list] ([location]) SELECT * FROM @operator_location_list
       ----SELECT * FROM [TemporaryData].[dbo].[batch_location_list]


    /*  Parse Bach type parameter  */

        IF @batch_type <> '' BEGIN

            /*  Parse Out the individual value as strings */
                INSERT INTO @batch_type_list (btype_id_str)
                SELECT LTRIM(item) FROM [dbo].[LFT_SPLIT_DILEMETED_STRING] (@batch_type, ',')

            /*  If any nonnumeric value got passed, delete it  */
                DELETE FROM @batch_type_list WHERE [dbo].[FS_isReallyNumeric]([btype_id_str]) = 0

            /*  Convert strings to numeric values  */
                UPDATE @batch_type_list SET [btype_id] = CONVERT(INT,[btype_id_str])

        END

        /*  If nothing in table variable - add all batch type ids to table variable  */
            IF NOT EXISTS (SELECT btype_id FROM @batch_type_list)
                INSERT INTO @batch_type_list (btype_id )
                SELECT [id] FROM [dbo].[TR_BATCH_TYPE]
    
    /*  Parse Bach type parameter  */

        /*  If there are status codes passed, parse them into indidual codes - otherwise, add all four status codes (C, H, O, P)  */
            IF @batch_status <> ''
                INSERT INTO @batch_status_list ([status_code])
                SELECT LTRIM(item) FROM [dbo].[LFT_SPLIT_DILEMETED_STRING] (@batch_status, ',')
            ELSE
                INSERT INTO @batch_status_list ([status_code]) 
                VALUES  ('O'), ('H'), ('C'), ('P')

        /*  Remove Quotes from Values  */

            UPDATE @batch_status_list SET [status_code] = REPLACE([status_code],'"','')
        

    /*  Pull data set for the date range entered  */

        INSERT INTO @data_table
        SELECT bat.[batch_no], bat.[batch_type], bty.[description], bty.[batch_type_group], bgp.[description], bat.[cntl_ind], bat.[create_dt], bat.[created_by], bat.[owner], 
               (own.[fname] + ' ' + own.[lname]), own.[location], bat.[status], 
               CASE WHEN bat.[status] = 'C' THEN 'Closed' WHEN bat.[status] = 'H' THEN 'Held'
                    WHEN bat.[status] = 'O' THEN 'Open'   WHEN bat.[status] = 'P' THEN 'Posted' END, ''
        FROM dbo.T_BATCH AS bat (NOLOCK)
            LEFT OUTER JOIN [dbo].[TR_BATCH_TYPE] AS bty (NOLOCK) ON bty.[id] = bat.[batch_type]
             LEFT OUTER JOIN [dbo].[TR_BATCH_TYPE_GROUP] AS bgp (NOLOCK) ON bgp.[id] = bty.[batch_type_group]
             LEFT OUTER JOIN [dbo].[T_METUSER] AS own (NOLOCK) ON own.[userid] = bat.[owner]
        WHERE bat.[create_dt] BETWEEN @report_start_dt AND @report_end_dt

    /*  If limited to specific operator locations, delete the others */      

        IF EXISTS (SELECT [operator_location] FROM @operator_location_list)
            DELETE FROM @data_table WHERE [location] NOT IN (SELECT [operator_location] FROM @operator_location_list)
      
    /*  If limited to specific batch types, delete the others */      
      
      IF EXISTS (SELECT btype_id FROM @batch_type_list)
            DELETE FROM @data_table WHERE batch_type NOT IN (SELECT [btype_id] FROM @batch_type_list)
 
    /*  If limited to specific status codes, delete the others  */

        IF EXISTS (SELECT [status_code] FROM @batch_status_list)
            DELETE FROM @data_table WHERE [status] NOT IN (SELECT LTRIM(RTRIM([status_code])) FROM @batch_status_list)

    FINISHED:

        /*  If no records in the data table, add a single record with a no records found message  */

        IF NOT EXISTS (SELECT * FROM @data_table) BEGIN
            IF @rpt_message = '' SELECT @rpt_message = 'No Records Found For The Criteria You Entered.'
            INSERT INTO @data_table VALUES  (0, 0, '', 0, '', '', null, '', '', '', '', '', '', @rpt_message)
        END

        /*  Select final data set to be passed to the report  */

        SELECT [batch_no], [batch_type], [batch_type_name], [batch_type_group], [batch_Type_group_name], [cntl_ind], [create_dt], 
               [created_by], [owner], [owner_name], [location], [status], [status_name], [rpt_message]
        FROM @data_table

END
GO

GRANT EXECUTE ON [dbo].[LRP_BATCH_STATUS_REPORT] TO ImpUsers
GO  


--EXECUTE [dbo].[LRP_BATCH_STATUS_REPORT] @report_start_dt = '1-1-2017', @report_end_dt = '1-31-2017', @operator_location = '"Box Office","Science Central', @batch_type = Null, @batch_status = 'H,O,C'

--SELECT * FROM TR_BATCH_TYPE WHERE description = 'Web Transaction'
--SELECT * FROM TR_BATCH_TYPE_GROUP WHERE description IN ('Advancement', 'VXO')
