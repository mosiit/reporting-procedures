USE impresario
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_ADV_PIPELINE_CONTRIBUTION_DETAIL]') AND type IN (N'P', N'PC'))
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_ADV_PIPELINE_CONTRIBUTION_DETAIL] AS' 
GO

EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LRP_ADV_PIPELINE_CONTRIBUTION_DETAIL] TO [impusers], [tessitura_app]'
GO


ALTER PROCEDURE [dbo].[LRP_ADV_PIPELINE_CONTRIBUTION_DETAIL]
        @fiscal_year INT = NULL,
        @workers_str VARCHAR(4000) = NULL
AS BEGIN
	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;

    /*  Procedure Variables  */

        DECLARE @fiscal_start_dt DATETIME = NULL
        DECLARE @fiscal_end_dt DATETIME = NULL
        DECLARE @current_dt DATETIME = GETDATE()
        DECLARE @worker_no INT = 0
	    --DECLARE @camp_category_str VARCHAR(4000) = NULL
	    --DECLARE @designation_str VARCHAR(4000) = NULL

    /*  Check Parameters  */

        IF @fiscal_year IS NULL SELECT @fiscal_year = dbo.LF_GetFiscalYear(GETDATE())
        
        SELECT @fiscal_start_dt = [dbo].[LF_GetFiscalYearStartDt](@fiscal_year)

        SELECT @fiscal_end_dt = [dbo].[LF_GetFiscalYearEndDt](@fiscal_year)
        
        IF @current_dt > @fiscal_end_dt SELECT @current_dt = @fiscal_end_dt

        SELECT @workers_str = ISNULL(@workers_str, 0)

        SELECT @worker_no = TRY_CONVERT(INT, @workers_str)
        SELECT @worker_no = ISNULL(@worker_no, 0)

    FINISHED:

        /*  Pull Data  */

    	SELECT DISTINCT c.[ref_no] AS [reference_no],
                        c.[customer_no] AS [customer_number],
                        CASE WHEN cust.[cust_type] = 7 THEN cust.[lname] ELSE cs.[esal1_desc] END AS [name],
                        c.cont_type AS [type],
                        '' AS [creditee_type],
                        CAST(c.[cont_dt] AS DATE) AS [date],
                        bp.[fyear] AS [fiscal_year],
                        bp.[quarter] AS [fiscal_quarter],
                        bp.[period] AS [fiscal_period],
                        c.[cont_amt] AS [contribution_amount],
                        c.[recd_amt] AS [received_amount],
                        kv.[key_value] AS [anonymous],
                        c.[custom_1] AS [soft_credit_type],
                        coa.[fund_description] AS [fund_description],
                        coa.[printable_fund] AS [printable_fund],
                        coa.[designation] AS [designation],
                        coa.[overall_cm] AS [overall_campaign],
                        cm.[description] AS [campaign],
                        coa.[cm_category] AS [campaign_category],
                        coa.[acct_goal] AS [account_goal],
                        coa.[acct_grp] AS [account_group],
                        a.[description] AS [appeal],
                        mt.[description] AS [media],
                        amt.[source_name] AS [source],
                        REPLACE(REPLACE(c.[notes],CHAR(13),'/'),CHAR(10),'/') AS [notes],
                        c.[cancel] AS [cancel], 
                        ps.[description] AS [pledge_status_desc],
                        bt.[description] AS [billing_type],
                        c.[batch_no] AS [batch],
                        c.[KG_xfer_dt] AS [KG_xfer_dt],
                        c.[KGift_desc] AS [KGift_desc],
                        c.[custom_2] AS [stock_ticker],
                        kv3.[key_value] AS [contribution_detail1],
                        kv4.[key_value] AS [contribution_detail2],
                        c.[custom_6] AS [agreement_date],
                        c.[custom_7] AS [challenge_earned],
                        kv5.[key_value] AS [pg_instrument],
                        c.[custom_0] AS [solicitation],
                        c.[worker_customer_no] AS [solicitor_number],
                        WCS.[esal1_desc] AS [solicitor_name],
                        coa.[cpf_flag] AS [cpf_flag],
                        coa.[exh_prog_cat] AS [exh_prog_cat],
                        coa.[full_fund] AS [full_fund_name],
                        coa.[cm_category1] AS [cm_category1],
                        coa.[cm1_intermediatelevel] AS [cm_intermediate1],
                        coa.[cm1_main_level] AS [cm_pillar1],
                        coa.[cm_category2] AS [cm_category2],
                        coa.[cm2_intermediate_level] AS [cm_intermediate2],
                        coa.[cm_category3] AS [cm_category3],
                        coa.[cm3_intermediate_level] AS [cm_intermediate3],
                        c.[custom_2] AS [custom_2],
                        c.[custom_9] AS [custom_9],
                        ct.[description] AS [main_customer_type],
                        ct.[description] AS [original_customer_type],
                        cust.[sort_name],
                        chan.[description] AS Channel
	    FROM [dbo].[T_CONTRIBUTION] AS c
             INNER JOIN [dbo].[T_CUSTOMER] AS cust ON c.[customer_no] = cust.[customer_no]
             INNER JOIN [dbo].[TR_CUST_TYPE] AS ct ON cust.[cust_type] = ct.[id]
             INNER JOIN [dbo].[TX_CUST_SAL] AS cs ON c.[customer_no] = cs.[customer_no] AND cs.[default_ind] = 'Y'
             INNER JOIN [dbo].[TR_SALES_CHANNEL] AS chan ON chan.[id] = c.[channel]
             LEFT OUTER JOIN [dbo].[TX_CUST_SAL] AS WCS ON C.[worker_customer_no] = WCS.[customer_no] AND WCS.[default_ind] = 'Y' 
             LEFT OUTER JOIN [dbo].[T_KWCODED_VALUES] AS kv ON c.[custom_5] = kv.[key_value] AND kv.[keyword_no] = 460
             LEFT OUTER JOIN [dbo].[T_KWCODED_VALUES] AS kv2 ON c.[custom_1] = kv2.[key_value] AND kv2.[keyword_no] = 456
             INNER JOIN [dbo].[TR_Batch_Period] AS bp ON c.[cont_dt] BETWEEN bp.[start_dt] AND bp.[end_dt]
             INNER JOIN [dbo].[T_CAMPAIGN] AS cm ON c.[campaign_no] = cm.[campaign_no]
             INNER JOIN [dbo].[LV_MOS_CHART_OF_ACCOUNTS] AS coa ON c.[fund_no] = coa.[fund_no] AND cm.[campaign_no] = coa.[campaign_no]
             INNER JOIN [dbo].[T_APPEAL] AS a ON c.[appeal_no] = a.[appeal_no]
             INNER JOIN [dbo].[TR_MEDIA_TYPE] AS mt ON c.[media_type] = mt.[id]
             INNER JOIN [dbo].[TX_APPEAL_MEDIA_TYPE] AS amt ON c.[source_no] = amt.[source_no]
             LEFT OUTER JOIN [dbo].[TR_BILLING_TYPE] AS bt ON c.[billing_type] = bt.[id]
             LEFT OUTER JOIN [dbo].[T_KWCODED_VALUES] AS kv3 ON c.[custom_3] = kv3.[key_value] AND kv3.[keyword_no] = 458
             LEFT OUTER JOIN [dbo].[T_KWCODED_VALUES] AS kv4 ON c.[custom_4] = kv4.[key_value] AND kv4.[keyword_no] = 459
             LEFT OUTER JOIN [dbo].[T_KWCODED_VALUES] AS kv5 ON c.[custom_8] = kv5.[key_value] AND kv5.[keyword_no] = 463
             LEFT OUTER JOIN [dbo].[TX_CUST_SAL] AS cs2 ON c.[worker_customer_no] = cs2.[customer_no] AND cs2.[default_ind] = 'Y'
             LEFT OUTER JOIN [dbo].[TR_PLEDGE_STATUS] PS ON C.[pledge_status] = PS.[id] 
        WHERE ISNULL(c.[custom_1], '(none)') IN ('(none)','Matching Gift Credit')
          AND c.[cont_dt] BETWEEN @fiscal_start_dt AND @current_dt
          AND c.[worker_customer_no] = @worker_no
          AND c.[cont_amt] > 0 

	UNION ALL

	SELECT	DISTINCT c.[ref_no] AS [reference_no],
                     cr.[creditee_no] AS [customer_number],
                     CASE WHEN cust2.[cust_type] = 7 THEN cust2.[lname] ELSE cs.[esal1_desc] END AS [name],
                     c.[cont_type] AS [type],
                     crt.[descriptiuon] AS [creditee_type],
                     CAST(c.[cont_dt] AS DATE) AS [date],
                     bp.[fyear] AS [fiscal_year],
                     bp.[quarter] AS [fiscal_quarter],
                     bp.[period] AS [fiscal_period],
                     c.[cont_amt] AS [contribution_amount],
                     c.[recd_amt] AS [received_amount],
                     kv.[key_value] AS [anonymous],
                     c.[custom_1] AS [soft_credit_type],
                     coa.[fund_description] AS [fund_description],
                     coa.[printable_fund] AS [printable_fund],
                     coa.[designation] AS [designation],
                     coa.[overall_cm] AS [overall_campaign],
                     cm.[description] AS [campaign],
                     coa.[cm_category] AS [campaign_category],
                     coa.[acct_goal] AS [account_goal],
                     coa.[acct_grp] AS [account_group],
                     a.[description] AS [appeal],
                     mt.[description] AS [media],
                     amt.[source_name] AS [source],
                     REPLACE(REPLACE(c.[notes], CHAR(13), '/'), CHAR(10), '/') AS [notes],
                     c.[cancel] AS [cancel],
                     ps.[description] AS [pledge_status_desc],
                     bt.[description] AS [billing_type],
                     c.[batch_no] AS [batch],
                     c.[KG_xfer_dt] AS [KG_xfer_dt],
                     c.[KGift_desc] AS [KGift_desc],
                     c.[custom_2] AS [stock_ticker],
                     kv3.[key_value] AS [contribution_detail1],
                     kv4.[key_value] AS [contribution_detail2],
                     c.[custom_6] AS [agreement_date],
                     c.[custom_7] AS [challenge_earned],
                     kv5.[key_value] AS [pg_instrument],
                     c.[custom_0] AS [solicitation],
                     c.[worker_customer_no] AS [solicitor_number],
                     WCS.[esal1_desc] AS [solicitor_name],
                     coa.[cpf_flag] AS [cpf_flag],
                     coa.[exh_prog_cat] AS [exh_prog_cat],
                     coa.[full_fund] AS [full_fund_name],
                     coa.[cm_category1] AS [cm_category1],
                     coa.[cm1_intermediatelevel] AS [cm_intermediate1],
                     coa.[cm1_main_level] AS [cm_pillar1],
                     coa.[cm_category2] AS [cm_category2],
                     coa.[cm2_intermediate_level] AS [cm_intermediate2],
                     coa.[cm_category3] AS [cm_category3],
                     coa.[cm3_intermediate_level] AS [cm_intermediate3],
                     c.[custom_2] AS [custom_2],
                     c.[custom_9] AS [custom_8],
                     ct2.[description] AS [main_customer_type],
                     ct.[description] AS [original_customer_type],
                     cust2.[sort_name],
                     chan.[description] AS [Channel] 
        FROM [dbo].[T_CONTRIBUTION] AS c
             INNER JOIN [dbo].[T_CUSTOMER] AS cust ON c.[customer_no] = cust.[customer_no]
             INNER JOIN [dbo].[TR_CUST_TYPE] AS ct ON cust.[cust_type] = ct.[id]
             INNER JOIN [dbo].[TR_SALES_CHANNEL] AS chan ON chan.[id] = c.[channel]
             LEFT OUTER JOIN [dbo].[TX_CUST_SAL] AS WCS ON C.[worker_customer_no] = WCS.[customer_no] AND WCS.[default_ind] = 'Y' 
             INNER JOIN [dbo].[T_CREDITEE] AS cr ON c.[ref_no] = cr.[ref_no]
             INNER JOIN [dbo].[TR_CREDITEE_TYPE] AS crt ON cr.[creditee_type] = crt.[id]
             INNER JOIN [dbo].[T_CUSTOMER] AS cust2 ON cr.[creditee_no] = cust2.[customer_no]
             INNER JOIN [dbo].[TR_CUST_TYPE] AS ct2 ON cust2.[cust_type] = ct2.[id]
             INNER JOIN [dbo].[TX_CUST_SAL] AS cs ON cr.[creditee_no] = cs.[customer_no] AND cs.[default_ind] = 'Y'
             LEFT OUTER JOIN [dbo].[T_KWCODED_VALUES] AS kv ON c.[custom_5] = kv.[key_value] AND kv.[keyword_no] = 460
             LEFT OUTER JOIN [dbo].[T_KWCODED_VALUES] AS kv2 ON c.[custom_1] = kv2.[key_value] AND kv2.[keyword_no] = 456
             INNER JOIN [dbo].[TR_Batch_Period] AS bp ON c.[cont_dt] BETWEEN bp.[start_dt] AND bp.[end_dt]
             INNER JOIN [dbo].[T_CAMPAIGN] AS cm ON c.[campaign_no] = cm.[campaign_no]
             INNER JOIN [dbo].[LV_MOS_CHART_OF_ACCOUNTS] AS coa ON c.[fund_no] = coa.[fund_no] AND cm.[campaign_no] = coa.[campaign_no] 
             INNER JOIN [dbo].[T_APPEAL] AS a ON c.[appeal_no] = a.[appeal_no]
             INNER JOIN [dbo].[TR_MEDIA_TYPE] AS mt ON c.[media_type] = mt.[id]
             INNER JOIN [dbo].[TX_APPEAL_MEDIA_TYPE] AS amt ON c.[source_no] = amt.[source_no]
             LEFT OUTER JOIN [dbo].[TR_BILLING_TYPE] AS bt ON c.[billing_type] = bt.[id]
             LEFT OUTER JOIN [dbo].[T_KWCODED_VALUES] AS kv3 ON c.[custom_3] = kv3.[key_value] AND kv3.[keyword_no] = 458
             LEFT OUTER JOIN [dbo].[T_KWCODED_VALUES] AS kv4 ON c.[custom_4] = kv4.[key_value] AND kv4.[keyword_no] = 459
             LEFT OUTER JOIN [dbo].[T_KWCODED_VALUES] AS kv5 ON c.[custom_8] = kv5.[key_value] AND kv5.[keyword_no] = 463
             LEFT OUTER JOIN [dbo].[TX_CUST_SAL] AS cs2 ON c.[worker_customer_no] = cs2.[customer_no] AND cs2.[default_ind] = 'Y'
             LEFT OUTER JOIN [dbo].[TR_PLEDGE_STATUS] AS PS ON C.[pledge_status] = PS.[id] 
        WHERE c.[custom_1] IN ('Primary Soft Credit')
          AND c.[cont_dt] BETWEEN @fiscal_start_dt AND @current_dt
          AND cr.[creditee_type] IN (5,12,15)
          AND c.[worker_customer_no] = @worker_no
          AND c.[cont_amt] > 0 
END
GO


EXECUTE [dbo].[LRP_ADV_PIPELINE_CONTRIBUTION_DETAIL] @fiscal_year = 2021, @workers_str = 3504561
GO


--SELECT * FROM [dbo].[LV_CAMPAIGN_ACTIVITY_WORKERS] WHERE [inactive] = 'N' ORDER BY worker_sort