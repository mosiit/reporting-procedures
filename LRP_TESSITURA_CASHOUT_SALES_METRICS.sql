USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_TESSITURA_CASHOUT_SALES_METRICS]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LRP_TESSITURA_CASHOUT_SALES_METRICS]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LRP_TESSITURA_CASHOUT_SALES_METRICS]
        @report_dt DATETIME = NULL,
        @report_operator varchar(8) = NULL,
        @machine_locations VARCHAR(4000) = NULL --Only passed to the LP_UPDATE_HISTORY_SALES_METRICS procedure, not used in this procedure
WITH RECOMPILE AS BEGIN
    
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

    /*  Report Variables  */
            
        DECLARE @data_dt DATETIME = GETDATE()

        DECLARE @temp_table TABLE ([data_dt] DATETIME, [history_dt] DATETIME, [history_date] CHAR(10), [operator] VARCHAR(25), [operator_name] VARCHAR(125), [operator_name_sort] VARCHAR(125),
                                   [operator_location] VARCHAR(50), [orders_created] DECIMAL(18,4), [orders_touched] DECIMAL(18,4), [memb_conversion_eligible] DECIMAL(18,4), 
                                   [membership_sales] DECIMAL(18,4), [basic_2_sales] DECIMAL(18,4), [basic_5_sales] DECIMAL(18,4), [basic_8_sales]  DECIMAL(18,4), [basic_total_sales] DECIMAL(18,4),
                                   [Premier_2_sales] DECIMAL(18,4), [Premier_5_sales] DECIMAL(18,4), [Premier_8_sales] DECIMAL(18,4), [premier_total_sales] DECIMAL(18,4), [one_step_eligible] DECIMAL(18,4),
                                   [one_step_eligible_basic] DECIMAL(18,4), [one_step_basic] DECIMAL(18,4), [one_step_eligible_premier] DECIMAL(18,4), [one_step_premier] DECIMAL(18,4), 
                                   [one_step_total] DECIMAL(18,4), [basic_one_step_recovery] DECIMAL(18,4), [premier_one_step_recovery] DECIMAL(18,4), [total_one_step_recovery] DECIMAL(18,4), 
                                   [basic_gift_memberships] DECIMAL(18,4), [premier_gift_memberships] DECIMAL(18,4), [total_gift_memberships] DECIMAL(18,4), [upsell_eligible] DECIMAL(18,4), 
                                   [upsell_orders] DECIMAL(18,4), [total_perfs_sold] DECIMAL(18,4))

        DECLARE @trx_stat_table table ([rpt_date] datetime, [rpt_operator] varchar(10), [trx_started] decimal(18,4), [trx_touched] decimal(18,4), 
                                       [memb_conv_eligible] DECIMAL(18,4),  [trx_memb_basic] decimal(18,4), [trx_memb_premier] decimal(18,4), 
                                       [trx_memb_total] decimal(18,4), [trx_memb_percent] DECIMAL(18,4), [trx_premier_percent] DECIMAL(18,4),
                                       [one_step_eligible] DECIMAL(18,4), [trx_one_step] DECIMAL(18,4), [trx_one_step_percent] DECIMAL(18,4),
                                       [upsell_eligible] DECIMAL(18,4), [trx_upsell] decimal(18,4), [trx_upsell_percent] decimal(18,4))
 
    /*  Check Parameters  */

        SELECT @report_dt = ISNULL(@report_dt,GETDATE())
        SELECT @report_dt = convert(date, @report_dt)

        SELECT @report_operator = ISNULL(@report_operator,'')

        SELECT @machine_locations = ISNULL(@machine_locations,'')

        IF @report_operator = '' BEGIN
            RAISERROR ('Something went wrong. No operator indicated.', 15, 1 ); 
            GOTO DONE
        END



    /*  Get data from the history table  */

        IF @report_dt < CAST(GETDATE() AS DATE)
            INSERT INTO @temp_table ([data_dt], [history_dt], [history_date], [operator], [operator_name], [operator_name_sort], [operator_location], [orders_created],
                                     [orders_touched], [memb_conversion_eligible], [membership_sales], [basic_2_sales], [basic_5_sales], [basic_8_sales], [basic_total_sales],
                                     [Premier_2_sales], [Premier_5_sales], [Premier_8_sales], [premier_total_sales], [one_step_eligible], [one_step_eligible_basic],
                                     [one_step_basic], [one_step_eligible_premier], [one_step_premier], [one_step_total], [basic_one_step_recovery], [premier_one_step_recovery],
                                     [total_one_step_recovery], [basic_gift_memberships], [premier_gift_memberships], [total_gift_memberships], [upsell_eligible],
                                     [upsell_orders], [total_perfs_sold])
            SELECT [data_dt],
                   [history_dt],
                   [history_date],
                   [operator],
                   [operator_name],
                   [operator_name_sort],
                   [operator_location],
                   [orders_created],
                   [orders_touched],
                   [memb_conversion_eligible],
                   [membership_sales],
                   [basic_2_sales],
                   [basic_5_sales],
                   [basic_8_sales],
                   [basic_total_sales],
                   [Premier_2_sales],
                   [Premier_5_sales],
                   [Premier_8_sales],
                   [premier_total_sales],
                   [one_step_eligible],
                   [one_step_eligible_basic],
                   [one_step_basic],
                   [one_step_eligible_premier],
                   [one_step_premier],
                   [one_step_total],
                   [basic_one_step_recovery],
                   [premier_one_step_recovery],
                   [total_one_step_recovery],
                   [basic_gift_memberships],
                   [premier_gift_memberships],
                   [total_gift_memberships],
                   [upsell_eligible],
                   [upsell_orders],
                   [total_perfs_sold]
           FROM [dbo].[LT_HISTORY_SALES_METRICS]
           WHERE CAST([history_dt] AS DATE) = @report_dt 
             AND [operator] = @report_operator
        ELSE
            INSERT INTO @temp_table ([data_dt], [history_dt], [history_date], [operator], [operator_name], [operator_name_sort], [operator_location], [orders_created],
                                     [orders_touched], [memb_conversion_eligible], [membership_sales], [basic_2_sales], [basic_5_sales], [basic_8_sales], [basic_total_sales],
                                     [Premier_2_sales], [Premier_5_sales], [Premier_8_sales], [premier_total_sales], [one_step_eligible], [one_step_eligible_basic],
                                     [one_step_basic], [one_step_eligible_premier], [one_step_premier], [one_step_total], [basic_one_step_recovery], [premier_one_step_recovery],
                                     [total_one_step_recovery], [basic_gift_memberships], [premier_gift_memberships], [total_gift_memberships], [upsell_eligible],
                                     [upsell_orders], [total_perfs_sold])
            EXECUTE [dbo].[LP_UPDATE_HISTORY_SALES_METRICS] @history_dt = @report_dt, 
                                                            @run_dt = @data_dt, 
                                                            @operator = @report_operator, 
                                                            @machine_locations = @machine_locations, 
                                                            @return_or_write = 'R'
        
    /*  Should only ever pull a single record from this procedure - If more than one record exists, something went wrong  */

        IF (SELECT COUNT(*) FROM @temp_table) > 1 BEGIN
            RAISERROR ('Something went wrong. Multiple records returned.', 15, 1 ); 
            GOTO DONE
        END           

    /*  Pull out the specific fields needed  */
            
        INSERT INTO @trx_stat_table ([rpt_date], [rpt_operator], [trx_started], [trx_touched], [memb_conv_eligible], [one_step_eligible], [upsell_eligible],
                                     [trx_memb_basic], [trx_memb_premier], [trx_memb_total], [trx_one_step], [trx_upsell])
        SELECT @report_dt, [operator], [orders_created], [orders_touched], [memb_conversion_eligible], [one_step_eligible], [upsell_eligible],
               [basic_total_sales], [premier_total_sales], [membership_sales], [one_step_total], [upsell_orders]
        FROM @temp_table
        
    /*  Calculate percentages  */

        UPDATE @trx_stat_table SET trx_memb_percent = CASE WHEN [memb_conv_eligible] <= 0 THEN 0 ELSE ([trx_memb_total] / [memb_conv_eligible]) END

        UPDATE @trx_stat_table SET trx_premier_percent = CASE WHEN [trx_memb_total] <= 0 THEN 0 ELSE ([trx_memb_premier] / [trx_memb_total]) END
        
        UPDATE @trx_stat_table SET trx_one_step_percent = CASE WHEN [one_step_eligible] <= 0 THEN 0 ELSE ([trx_one_step] / [one_step_eligible]) END
        
        UPDATE @trx_stat_table SET trx_upsell_percent = CASE WHEN [upsell_eligible] <= 0 THEN 0 ELSE ([trx_upsell] / [upsell_eligible]) END

    FINISHED:

    /*  Select the final data set from the temp table  */

        SELECT [rpt_date], 
               [rpt_operator], 
               [trx_started], 
               [trx_touched], 
               [memb_conv_eligible], 
               [trx_memb_basic], 
               [trx_memb_premier], 
               [trx_memb_total], 
               [trx_memb_percent],
               [trx_premier_percent],
               [one_step_eligible], 
               [trx_one_step], 
               [trx_one_step_percent],
               [upsell_eligible],
               [trx_upsell],
               [trx_upsell_percent]
        FROM @trx_stat_table

    DONE:


END
GO

GRANT EXECUTE ON [dbo].[LRP_TESSITURA_CASHOUT_SALES_METRICS] TO impusers
GO

EXECUTE [dbo].[LRP_TESSITURA_CASHOUT_SALES_METRICS] @report_dt = '4-17-2021', @report_operator = 'jfarre00', @machine_locations = '6,"15"'


--SELECT * FROM [dbo].[LV_MuseumUserInfo] WHERE [fname] = 'Natalia'
