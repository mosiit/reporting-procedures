USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_EVENT_COUNT]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LRP_EVENT_COUNT]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LRP_EVENT_COUNT] 
        @report_start_dt datetime = Null, 
        @report_end_dt datetime = Null,
        @title_name varchar(30) = 'All'
AS BEGIN

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

    SELECT @title_name = IsNull(@title_name, 'All')
    IF @title_name = '' SELECT @title_name = 'All'

    SELECT @report_start_dt = IsNull(@report_start_dt, dateadd(day,-1,getdate()))
    SELECT @report_end_dt = IsNull(@report_end_dt, dateadd(day,-1,getdate()))

    SELECT @report_start_dt = convert(char(10),@report_start_dt,111) + ' 00:00:00',
           @report_end_dt = convert(char(10),@report_end_dt,111) + ' 23:59:59'
    
    DECLARE @event_table table ([report_message] varchar(25), [title_name] varchar(30), [production_name] varchar(30), [production_name_long] varchar(100), [event_count] int, 
                                [p_report_start_dt] datetime, [p_report_end_date] datetime, [P_title_name] varchar(30))

    INSERT INTO @event_table
    SELECT '', [title_name], [production_name], [production_name_long], count(*), @report_start_dt, @report_end_dt, @title_name
    FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] (NOLOCK)
    WHERE [performance_dt] between @report_start_dt and @report_end_dt
    GROUP BY [title_name], [production_name], [production_name_long]

    IF @title_name <> 'All' DELETE FROM @event_table WHERE [title_name] <> @title_name
    ELSE DELETE FROM @event_table WHERE title_name in ('City Pass', 'Drop-In Activities', 'Live Presentations', 'Traveling Progams', 'Vouchers')

    DONE:

        IF not exists (SELECT * FROM @event_table)
            INSERT INTO @event_table VALUES ('No events found.', '', '', '', 0, @report_start_dt, @report_end_dt, @title_name)

        SELECT [report_message], [title_name], [production_name], [production_name_long], [event_count], [p_report_start_dt], [p_report_end_date], [P_title_name]
        FROM @event_table
        ORDER BY [title_name], [production_name], [production_name_long]

END
GO

GRANT EXECUTE ON [dbo].[LRP_EVENT_COUNT] TO impusers
GO


