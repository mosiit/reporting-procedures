USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_GIFT_ENTITLEMENT_STATS]') AND type in (N'P', N'PC')) BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_GIFT_ENTITLEMENT_STATS] AS' 
    EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LRP_GIFT_ENTITLEMENT_STATS] TO impusers' 
END
GO

ALTER PROCEDURE [dbo].[LRP_GIFT_ENTITLEMENT_STATS]
        @report_start_dt DATETIME = NULL,
        @report_end_dt DATETIME = NULL,
        @date_type VARCHAR(30) = 0,            --Date Type = Entitlement Initialize Date or Gift Create Date or Redeemed Performance Date
        @price_type_group INT = 0,
        @entitlement_str VARCHAR(4000) = '',            
        @used_only CHAR(1) = 'N',
        @title_str VARCHAR(4000) = '',         --title search changed to keyword search but parameter name left as title_no
        @prod_str VARCHAR(4000) = '',
        @giver_no INT = 0,
        @list_no INT = 0,
        @display_grid_str VARCHAR(4000) = ''    --By Entitlement, By Price Type, Redemption by Venue, Redemption By Production, By Giver
AS BEGIN

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    SET NOCOUNT ON;
    SET ANSI_WARNINGS OFF;

    /*  Procedure Variables and Tables  */
        
        DECLARE @keyword_list TABLE ([tkw_no] INT)
        DECLARE @prod_list TABLE ([prod_no] INT)
        DECLARE @display_entitlement TINYINT = 0, @display_price_type TINYINT = 0, @display_Redeem_title TINYINT = 0
        DECLARE @display_redeem_production TINYINT = 0, @display_giver TINYINT = 0

        DECLARE @gift_id INT = 0, @gift_giver_no INT = 0, @gift_recipient_no INT = 0, @num_left INT = 0, @tkw_id INT = 0, @price_group INT = 0, @create_dt DATETIME = NULL 
        DECLARE @gift_giver_name VARCHAR(150) = '', @gift_giver_name_sort VARCHAR(150) = '', @gift_recipient_name VARCHAR(150) = '', @gift_recipient_name_sort VARCHAR(150) = ''
        DECLARE @gift_code VARCHAR(30) = '', @group_gift_code VARCHAR(30) = '', @entitlement_code VARCHAR(30) = '', @entitlement_desc VARCHAR(50) = '', @keyword VARCHAR(30) = '' 
        DECLARE @price_group_name VARCHAR(30) = ''


        IF OBJECT_ID('tempdb..#entitlement_list') IS NOT NULL DROP TABLE [#entitlement_list]

        DECLARE @entitlement_list TABLE ([entitlement_no] INT)


        IF OBJECT_ID('tempdb..#gift_entitlements') IS NOT NULL DROP TABLE [#gift_entitlements]

        CREATE TABLE [#gift_entitlements] (        [gift_id] INT NOT NULL DEFAULT (0),
                                                   [giver_no] INT NOT NULL DEFAULT (0),
                                                   [giver_name] VARCHAR(100) NOT NULL DEFAULT (''),
                                                   [giver_name_sort] VARCHAR(100) NOT NULL DEFAULT (''),
                                                   [recipient_no] INT NOT NULL DEFAULT (0),
                                                   [recipient_name] VARCHAR(100) NOT NULL DEFAULT (''),
                                                   [recipient_name_sort] VARCHAR(100) NOT NULL DEFAULT (''),
                                                   [recipient_email] VARCHAR(100) NOT NULL DEFAULT (''),
                                                   [gift_code] VARCHAR(30) NOT NULL DEFAULT (''),
                                                   [group_gift_code] VARCHAR(30) NOT NULL DEFAULT (''),
                                                   [create_dt] DATETIME NULL,
                                                   [init_dt] DATETIME NULL,
                                                   [expr_dt] DATETIME NULL,
                                                   [ltx_cust_entitlement_id] INT NOT NULL DEFAULT (0),
                                                   [entitlement_no] INT NOT NULL DEFAULT (0),
                                                   [entitlement_code] VARCHAR(30) NOT NULL DEFAULT (''),
                                                   [entitlement_desc] VARCHAR(100) NOT NULL DEFAULT (''),
                                                   [ent_price_type_group] INT NOT NULL DEFAULT (0),
                                                   [price_type_group_name] VARCHAR(30) NOT NULL DEFAULT (''),
                                                   [ent_tkw_id] INT NOT NULL DEFAULT (0),
                                                   [keyword] VARCHAR(50) NOT NULL DEFAULT (''),
                                                   [num_items] INT NOT NULL DEFAULT (0),
                                                   [gift_status] VARCHAR(30) NOT NULL DEFAULT (''),
                                                   [gift_status_name] VARCHAR(30) NOT NULL DEFAULT (''),
                                                   [counter_all] INT NOT NULL DEFAULT (0),
                                                   [counter_created] INT NOT NULL DEFAULT (0),
                                                   [counter_accepted] INT NOT NULL DEFAULT (0),
                                                   [counter_other] INT NOT NULL DEFAULT (0),
                                                   [counter_return_to_giver] INT NOT NULL DEFAULT (0),
                                                   [set_no] INT NOT NULL DEFAULT (0),
                                                   [set_name] VARCHAR(30) NOT NULL DEFAULT (''),
                                                   [order_no] INT NOT NULL DEFAULT (0),
                                                   [order_source_no] INT NOT NULL DEFAULT (0),
                                                   [order_source] VARCHAR(50) NOT NULL   DEFAULT(''),
                                                   [sli_no] INT NOT NULL DEFAULT (0),
                                                   [perf_no] INT NOT NULL DEFAULT (0),
                                                   [performance_no] INT NOT NULL DEFAULT (0),
                                                   [performance_zone] INT NOT NULL DEFAULT (0),
                                                   [performance_dt] DATETIME NULL,
                                                   [performance_date] VARCHAR(10) NOT NULL DEFAULT (''),
                                                   [performance_time] VARCHAR(10) NOT NULL DEFAULT (''),
                                                   [performance_time_display] VARCHAR(30) NOT NULL DEFAULT (''),
                                                   [title_no] INT NOT NULL DEFAULT (0),
                                                   [title_name] VARCHAR(30) NOT NULL DEFAULT (''),
                                                   [production_no] INT NOT NULL DEFAULT (0),
                                                   [production_name] VARCHAR(30) NOT NULL DEFAULT (''),
                                                   [num_used] INT NOT NULL DEFAULT (0),
                                                   [pass_status] VARCHAR(30) NOT NULL DEFAULT (''),
                                                   [price_type_desc] VARCHAR(30) NOT NULL DEFAULT (''),
                                                   [shord_desc] VARCHAR(30) NOT NULL DEFAULT (''),
                                                   [report_start_dt] DATETIME NULL,
                                                   [report_end_dt] DATETIME NULL                        )

    /*  Check Parameters  */

        --Set to N (No) if anything other than Y is passed to parameter
        SELECT @used_only = ISNULL(@used_only,'N')
        IF @used_only <> 'Y' SELECT @used_only = 'N'

        --Set to Gift Initialize Date it anything other than Redeemed Performance Date is passed to parameter
        SELECT @date_type = ISNULL(@date_type,'Gift Initialize Date');
        IF @date_type <> 'Redeemed Performance Date' SELECT @date_type = 'Gift Initialize Date';

        --IF @report_start_dt is null, set to earliest date found
        --      init date or perf date depending on what is passed to @date_type parameter
        IF @report_start_dt IS NULL BEGIN

            IF @date_type = 'Redeemed Performance Date'
                SELECT @report_start_dt = MIN([performance_dt]) FROM [dbo].[LV_GIFT_ENTITLEMENTS]
            ELSE IF @date_type = 'Entitlement Initialize Date'
                SELECT @report_start_dt = MIN([init_dt]) FROM [dbo].[LV_GIFT_ENTITLEMENTS]
            ELSE
                SELECT @report_start_dt = MIN([create_dt]) FROM [dbo].[LV_GIFT_ENTITLEMENTS];
    
        END;

        --IF @report_end_dt is null, set to latest date found
        --      init date or perf date depending on what is passed to @date_type parameter
        IF @report_end_dt IS NULL BEGIN

            IF @date_type = 'Redeemed Performance Date'
                SELECT @report_end_dt = MAX([performance_dt]) FROM [dbo].[LV_GIFT_ENTITLEMENTS]
            ELSE IF @date_type = 'Entitlement Initialize Date'
                SELECT @report_end_dt = MAX([init_dt]) FROM [dbo].[LV_GIFT_ENTITLEMENTS]
            ELSE
                SELECT @report_start_dt = MAX([create_dt]) FROM [dbo].[LV_GIFT_ENTITLEMENTS];

        END;

        --Make sure start date is at beginning of day and end date is at end of day
        SELECT @report_start_dt = CAST(@report_start_dt AS DATE)
        SELECT @report_end_dt = CONVERT(CHAR(10),@report_end_dt,111) + ' 23:59:59.957'

        --Parse keyword list if one is passed
        IF ISNULL(@title_str,'') <> ''
            INSERT INTO @keyword_list ([tkw_no])
            SELECT CAST([element] AS INT) 
            FROM [dbo].[FT_SPLIT_LIST](REPLACE(@title_str,'"',''),',')

        --Parse Production list if one is passed
        IF ISNULL(@prod_str,'') <> ''
            INSERT INTO @prod_list ([prod_no])
            SELECT CAST([element] AS INT) 
            FROM dbo.FT_SPLIT_LIST(REPLACE(@prod_str,'"',''),',')

        --Parse the Entitlement list if one is passed, otherwise create a list of all entitlements
        IF ISNULL(@entitlement_str,'') = ''
            INSERT INTO @entitlement_list ([entitlement_no])
            SELECT [entitlement_no] 
            FROM [dbo].[LTR_ENTITLEMENT]
            WHERE ISNULL(@price_type_group,0) <= 0 or [ent_price_type_group] = @price_type_group
        ELSE
            INSERT INTO @entitlement_list ([entitlement_no])
            SELECT CAST([element] AS INT) 
            FROM dbo.FT_SPLIT_LIST(REPLACE(@entitlement_str,'"',''),',');

        --Specific giver number overrides list.  If giver number is passed, set list number to zero.
        IF ISNULL(@giver_no,0) > 0
            SELECT @list_no = 0

        --Grids to display
        IF CHARINDEX('By Entitlement',@display_grid_str) > 0 SELECT @display_entitlement = 1
        IF CHARINDEX('By Price Type',@display_grid_str) > 0 SELECT @display_price_type = 1
        IF CHARINDEX('Redemption by Venue',@display_grid_str) > 0 SELECT @display_Redeem_title = 1
        IF CHARINDEX('Redemption By Production',@display_grid_str) > 0 SELECT @display_redeem_production = 1
        IF CHARINDEX('By Giver',@display_grid_str) > 0 SELECT @display_giver = 1


    /* Pull appropriate records from the LV_GIFT_ENTITLEMENTS VIEW
       Done as three separate insert statememnts. When it was one statement with a conditional WHERE clause for date type, a report for 3 days took up to 2:00 to run
       When changed to one of three different statements (determined by @date_type parameter) the same report ran in about 0:25.
       All three statments are exactly the same except for the date field being selected on.  */
           
        IF @date_type = 'Redeemed Performance Date' 
   
            INSERT INTO [#gift_entitlements] ([gift_id],[giver_no],[giver_name],[giver_name_sort],[recipient_no],[recipient_name],[recipient_name_sort],[recipient_email],
                                              [gift_code],[group_gift_code],[create_dt],[init_dt],[expr_dt],[ltx_cust_entitlement_id],[entitlement_no],[entitlement_code],
                                              [entitlement_desc],[ent_price_type_group],[price_type_group_name],[ent_tkw_id],[keyword],[num_items],[gift_status],
                                              [gift_status_name],[counter_all],[counter_created],[counter_accepted],[counter_other],[counter_return_to_giver],[set_no],
                                              [set_name],[order_no],[order_source_no],[order_source],[sli_no],[perf_no],[performance_no],[performance_zone],
                                              [performance_dt],[performance_date],[performance_time],[performance_time_display],[title_no],[title_name],[production_no],
                                              [production_name],[num_used],[pass_status],[price_type_desc],[shord_desc],[report_start_dt],[report_end_dt])
            SELECT gft.[gift_id],
                   gft.[giver_no],
                   gft.[giver_name],
                   gft.[giver_name_sort],
                   ISNULL(gft.[recipient_no],0),
                   gft.[recipient_name],
                   gft.[recipient_name_sort],
                   gft.[recipient_email],
                   gft.[gift_code],
                   gft.[group_gift_code],
                   gft.[create_dt],
                   gft.[init_dt],
                   gft.[expr_dt],
                   gft.[ltx_cust_entitlement_id],
                   gft.[entitlement_no],
                   gft.[entitlement_code],
                   gft.[entitlement_desc],
                   gft.[ent_price_type_group],
                   gft.[price_type_group_name],
                   gft.[ent_tkw_id],
                   gft.[keyword],
                   gft.[num_items],
                   gft.[gift_status],
                   gft.[gift_status_name],
                   gft.[counter_all],
                   gft.[counter_created],
                   gft.[counter_accepted],
                   gft.[counter_other],
                   gft.[counter_return_to_giver],
                   gft.[set_no],
                   gft.[set_name],
                   gft.[order_no],
                   gft.[order_source_no],
                   CASE WHEN gft.[counter_return_to_giver] > 0 THEN 'Redeemed at Gate'
                        WHEN gft.[order_source] = '' THEN 'Not Redeemed'
                        ELSE 'Redeemed Through ' + gft.[order_source] END,
                   gft.[sli_no],
                   gft.[perf_no],
                   gft.[performance_no],
                   gft.[performance_zone],
                   gft.[performance_dt],
                   gft.[performance_date],
                   gft.[performance_time],
                   gft.[performance_time_display],
                   gft.[title_no],
                   gft.[title_name],
                   gft.[production_no],
                   gft.[production_name],
                   gft.[num_used],
                   gft.[pass_status],
                   gft.[price_type_desc],
                   gft.[shord_desc],
                   @report_start_dt AS [report_start_dt],
                   @report_end_dt AS [report_end_dt]
            FROM [dbo].[LV_GIFT_ENTITLEMENTS] AS gft
                 INNER JOIN @entitlement_list AS ent ON ent.[entitlement_no] = gft.[entitlement_no]
            WHERE gft.[performance_dt] BETWEEN @report_start_dt AND @report_end_dt
              AND (@used_only = 'N' OR gft.[pass_status] = 'Used')
              AND (ISNULL(@giver_no,0) <= 0 OR gft.[giver_no] = @giver_no)

        ELSE IF @date_type = 'Entitlement Initialize Date'
        
            INSERT INTO [#gift_entitlements] ([gift_id],[giver_no],[giver_name],[giver_name_sort],[recipient_no],[recipient_name],[recipient_name_sort],[recipient_email],
                                              [gift_code],[group_gift_code],[create_dt],[init_dt],[expr_dt],[ltx_cust_entitlement_id],[entitlement_no],[entitlement_code],
                                              [entitlement_desc],[ent_price_type_group],[price_type_group_name],[ent_tkw_id],[keyword],[num_items],[gift_status],
                                              [gift_status_name],[counter_all],[counter_created],[counter_accepted],[counter_other],[counter_return_to_giver],[set_no],
                                              [set_name],[order_no],[order_source_no],[order_source],[sli_no],[perf_no],[performance_no],[performance_zone],
                                              [performance_dt],[performance_date],[performance_time],[performance_time_display],[title_no],[title_name],[production_no],
                                              [production_name],[num_used],[pass_status],[price_type_desc],[shord_desc],[report_start_dt],[report_end_dt])
            SELECT gft.[gift_id],
                   gft.[giver_no],
                   gft.[giver_name],
                   gft.[giver_name_sort],
                   ISNULL(gft.[recipient_no],0),
                   gft.[recipient_name],
                   gft.[recipient_name_sort],
                   gft.[recipient_email],
                   gft.[gift_code],
                   gft.[group_gift_code],
                   gft.[create_dt],
                   gft.[init_dt],
                   gft.[expr_dt],
                   gft.[ltx_cust_entitlement_id],
                   gft.[entitlement_no],
                   gft.[entitlement_code],
                   gft.[entitlement_desc],
                   gft.[ent_price_type_group],
                   gft.[price_type_group_name],
                   gft.[ent_tkw_id],
                   gft.[keyword],
                   gft.[num_items],
                   gft.[gift_status],
                   gft.[gift_status_name],
                   gft.[counter_all],
                   gft.[counter_created],
                   gft.[counter_accepted],
                   gft.[counter_other],
                   gft.[counter_return_to_giver],
                   gft.[set_no],
                   gft.[set_name],
                   gft.[order_no],
                   gft.[order_source_no],
                   CASE WHEN gft.[counter_return_to_giver] > 0 THEN 'Redeemed at Gate'
                        WHEN gft.[order_source] = '' THEN 'Not Redeemed'
                        ELSE 'Redeemed Through ' + gft.[order_source] END,
                   gft.[sli_no],
                   gft.[perf_no],
                   gft.[performance_no],
                   gft.[performance_zone],
                   gft.[performance_dt],
                   gft.[performance_date],
                   gft.[performance_time],
                   gft.[performance_time_display],
                   gft.[title_no],
                   gft.[title_name],
                   gft.[production_no],
                   gft.[production_name],
                   gft.[num_used],
                   gft.[pass_status],
                   gft.[price_type_desc],
                   gft.[shord_desc],
                   @report_start_dt AS [report_start_dt],
                   @report_end_dt AS [report_end_dt]
            FROM [dbo].[LV_GIFT_ENTITLEMENTS] AS gft
                 INNER JOIN @entitlement_list AS ent ON ent.[entitlement_no] = gft.[entitlement_no]
            WHERE gft.[init_dt] BETWEEN @report_start_dt AND @report_end_dt
              AND (@used_only = 'N' OR gft.[pass_status] = 'Used')
              AND (ISNULL(@giver_no,0) <= 0 OR gft.[giver_no] = @giver_no)

        ELSE

             INSERT INTO [#gift_entitlements] ([gift_id],[giver_no],[giver_name],[giver_name_sort],[recipient_no],[recipient_name],[recipient_name_sort],[recipient_email],
                                              [gift_code],[group_gift_code],[create_dt],[init_dt],[expr_dt],[ltx_cust_entitlement_id],[entitlement_no],[entitlement_code],
                                              [entitlement_desc],[ent_price_type_group],[price_type_group_name],[ent_tkw_id],[keyword],[num_items],[gift_status],
                                              [gift_status_name],[counter_all],[counter_created],[counter_accepted],[counter_other],[counter_return_to_giver],[set_no],
                                              [set_name],[order_no],[order_source_no],[order_source],[sli_no],[perf_no],[performance_no],[performance_zone],
                                              [performance_dt],[performance_date],[performance_time],[performance_time_display],[title_no],[title_name],[production_no],
                                              [production_name],[num_used],[pass_status],[price_type_desc],[shord_desc],[report_start_dt],[report_end_dt])
            SELECT gft.[gift_id],
                   gft.[giver_no],
                   gft.[giver_name],
                   gft.[giver_name_sort],
                   ISNULL(gft.[recipient_no],0),
                   gft.[recipient_name],
                   gft.[recipient_name_sort],
                   gft.[recipient_email],
                   gft.[gift_code],
                   gft.[group_gift_code],
                   gft.[create_dt],
                   gft.[init_dt],
                   gft.[expr_dt],
                   gft.[ltx_cust_entitlement_id],
                   gft.[entitlement_no],
                   gft.[entitlement_code],
                   gft.[entitlement_desc],
                   gft.[ent_price_type_group],
                   gft.[price_type_group_name],
                   gft.[ent_tkw_id],
                   gft.[keyword],
                   gft.[num_items],
                   gft.[gift_status],
                   gft.[gift_status_name],
                   gft.[counter_all],
                   gft.[counter_created],
                   gft.[counter_accepted],
                   gft.[counter_other],
                   gft.[counter_return_to_giver],
                   gft.[set_no],
                   gft.[set_name],
                   gft.[order_no],
                   gft.[order_source_no],
                   CASE WHEN gft.[counter_return_to_giver] > 0 THEN 'Redeemed at Gate'
                        WHEN gft.[order_source] = '' THEN 'Not Redeemed'
                        ELSE 'Redeemed Through ' + gft.[order_source] END,
                   gft.[sli_no],
                   gft.[perf_no],
                   gft.[performance_no],
                   gft.[performance_zone],
                   gft.[performance_dt],
                   gft.[performance_date],
                   gft.[performance_time],
                   gft.[performance_time_display],
                   gft.[title_no],
                   gft.[title_name],
                   gft.[production_no],
                   gft.[production_name],
                   gft.[num_used],
                   gft.[pass_status],
                   gft.[price_type_desc],
                   gft.[shord_desc],
                   @report_start_dt AS [report_start_dt],
                   @report_end_dt AS [report_end_dt]
            FROM [dbo].[LV_GIFT_ENTITLEMENTS] AS gft
                 INNER JOIN @entitlement_list AS ent ON ent.[entitlement_no] = gft.[entitlement_no]
            WHERE gft.[create_dt] BETWEEN @report_start_dt AND @report_end_dt
              AND (@used_only = 'N' OR gft.[pass_status] = 'Used')
              AND (ISNULL(@giver_no,0) <= 0 OR gft.[giver_no] = @giver_no)
          
    /*  Remove Customers not on a selected list (if any)*/
          
        IF ISNULL(@list_no,0) > 0
            DELETE FROM [#gift_entitlements]
            WHERE [giver_no] NOT IN (SELECT [customer_no] 
                                     FROM [dbo].[T_LIST_CONTENTS] 
                                     WHERE [list_no] = @list_no)



        DECLARE unused_gifts_cursor INSENSITIVE CURSOR FOR
            SELECT [gift_id],
                   [giver_no],
                   [giver_name],
                   [giver_name_sort],
                   ISNULL([recipient_no],0),
                   ISNULL([recipient_name],''),
                   isnull([recipient_name_sort],''),
                   [gift_code],
                   [group_gift_code],
                   [create_dt],
                   [entitlement_code],
                   [entitlement_desc],
                   [ent_price_type_group],
                   [price_type_group_name],
                   [ent_tkw_id],
                   [keyword],
                   [num_left]
            FROM [dbo].[LV_GIFT_ENTITLEMENTS_PARTIALLY_USED]
            WHERE [gift_id] IN (SELECT [gift_id] 
                                FROM [#gift_entitlements])
        OPEN unused_gifts_cursor
        BEGIN_UNUSED_GIFTS_LOOP:

            FETCH NEXT FROM unused_gifts_cursor INTO @gift_id, @gift_giver_no, @gift_giver_name, @gift_giver_name_sort, @gift_recipient_no, 
                                                     @gift_recipient_name, @gift_recipient_name_sort, @gift_code, @group_gift_code, @create_dt, 
                                                     @entitlement_code, @entitlement_desc, @price_group, @price_group_name, @tkw_id, @keyword, @num_left
            IF @@FETCH_STATUS = -1 GOTO END_UNUSED_GIFTS_LOOP

            WHILE @num_left > 0 BEGIN

                INSERT INTO [#gift_entitlements] ([gift_id],[giver_no],[giver_name],[giver_name_sort],[recipient_no],[recipient_name],[recipient_name_sort],[recipient_email],
                                                  [gift_code],[group_gift_code],[create_dt],[init_dt],[expr_dt],[ltx_cust_entitlement_id],[entitlement_no],[entitlement_code],
                                                  [entitlement_desc],[ent_price_type_group],[price_type_group_name],[ent_tkw_id],[keyword],[num_items],[gift_status],
                                                  [gift_status_name],[counter_all],[counter_created],[counter_accepted],[counter_other],[counter_return_to_giver],[set_no],
                                                  [set_name],[order_no],[order_source_no],[order_source],[sli_no],[perf_no],[performance_no],[performance_zone],
                                                  [performance_dt],[performance_date],[performance_time],[performance_time_display],[title_no],[title_name],[production_no],
                                                  [production_name],[num_used],[pass_status],[price_type_desc],[shord_desc],[report_start_dt],[report_end_dt])
                VALUES (@gift_id, 
                        @gift_giver_no, 
                        @gift_giver_name, 
                        @gift_giver_name_sort, 
                        @gift_recipient_no, 
                        @gift_recipient_name, 
                        @gift_recipient_name_sort, 
                        '', 
                        @gift_code, 
                        @group_gift_code, 
                        @create_dt,
                        NULL,
                        NULL, 
                        0, 
                        0, 
                        @entitlement_code, 
                        @entitlement_desc, 
                        @price_group, 
                        @price_group_name, 
                        @tkw_id, 
                        @keyword, 
                        1, 
                        'A', 
                        'Accepted', 
                        1, 
                        0, 
                        1, 
                        0, 
                        0, 
                        0, 
                        '', 
                        0, 
                        '', 
                        'Not Redeemed', 
                        0, 
                        0,
                        0, 
                        0, 
                        NULL, 
                        '', 
                        '', 
                        '', 
                        0, 
                        '', 
                        0, 
                        '', 
                        0, 
                        'Not Used', 
                        '', 
                        '', 
                        @report_start_dt, 
                        @report_end_dt)

                SELECT @num_left -= 1

            END

            GOTO BEGIN_UNUSED_GIFTS_LOOP

        END_UNUSED_GIFTS_LOOP:
        CLOSE unused_gifts_cursor
        DEALLOCATE unused_gifts_cursor

              
    /*  Remove Titles that are not asked for  */

        IF EXISTS (SELECT * FROM @keyword_list)
            DELETE FROM [#gift_entitlements]
            WHERE ISNULL([ent_tkw_id],0) NOT IN (SELECT [tkw_no] FROM @keyword_list);

        IF EXISTS (SELECT * FROM @prod_list)
            DELETE FROM [#gift_entitlements]
            WHERE ISNULL([production_no],0) > 0 AND ISNULL([production_no],0) NOT IN (SELECT [prod_no] FROM @prod_list)
              
    FINISHED:
    
        /*  Pull final list of gift entitlements for report  */
    
            SELECT [gift_id],
                   [giver_no],
                   [giver_name],
                   [giver_name_sort],
                   [recipient_no],
                   [recipient_name],
                   [recipient_name_sort],
                   [recipient_email],
                   [gift_code],
                   [group_gift_code],
                   [create_dt],
                   [init_dt],
                   [expr_dt],
                   [ltx_cust_entitlement_id],
                   [entitlement_no],
                   [entitlement_code],
                   [entitlement_desc],
                   [ent_price_type_group],
                   [price_type_group_name],
                   [ent_tkw_id],
                   [keyword],
                   [num_items],
                   [gift_status],
                   [gift_status_name],
                   [counter_all],
                   [counter_created],
                   [counter_accepted],
                   [counter_other],
                   [counter_return_to_giver],
                   [set_no],
                   [set_name],
                   [order_no],
                   [order_source_no],
                   [order_source],
                   [sli_no],
                   [perf_no],
                   [performance_no],
                   [performance_zone],
                   [performance_dt],
                   [performance_date],
                   [performance_time],
                   [performance_time_display],
                   [title_no],
                   [title_name],
                   [production_no],
                   [production_name],
                   [num_used],
                   CASE WHEN order_source = 'Redeemed at Gate' THEN [num_used] ELSE 0 END AS [num_used_gate],
                   CASE WHEN order_source = 'Redeemed Through Box Office' THEN [num_used] ELSE 0 END AS [num_used_box],
                   CASE WHEN order_source = 'Redeemed Through Science Central' THEN [num_used] ELSE 0 END AS [num_used_sci],
                   CASE WHEN order_source = 'Redeemed Through Web' THEN [num_used] ELSE 0 END AS [num_used_web],
                   CASE WHEN [num_used] > 0
                         AND [order_source] NOT IN ('Redeemed at Gate','Redeemed Through Box Office','Redeemed Through Science Central','Redeemed Through Web')
                         THEN [num_used] ELSE 0 END AS [num_used_other],
                   [pass_status],
                   [price_type_desc],
                   [shord_desc],
                   [report_start_dt],
                   [report_end_dt],
                   @display_entitlement AS [display_entitlement], 
                   @display_price_type AS [display_price_type], 
                   @display_Redeem_title AS [display_Redeem_title], 
                   @display_redeem_production AS [display_redeem_production], 
                   @display_giver AS [display_giver]
                FROM [#gift_entitlements]
        
    DONE:

END
GO

        --EXECUTE  [dbo].[LRP_GIFT_ENTITLEMENT_STATS]
        --    @report_start_dt = '6-24-2019', 
        --    @report_end_dt = '6-26-2019', 
        --    @date_type = 'Gift Created Date', 
        --    @price_type_group = 0, 
        --    @entitlement_str = Null,
        --    @title_str = Null, 
        --    @prod_str = Null, 
        --    @giver_no = 0, 
        --    @used_only = 'N', 
        --    @list_no = 0,
        --    @display_grid_str = '"By Entitlement","By Price Type","Redemption by Venue","Redemption By Production","By Giver"'

            


        
        --EXECUTE  [dbo].[LRP_GIFT_ENTITLEMENT_STATS]
        --    @report_start_dt = '6-26-2019', @report_end_dt = '6-26-2019', @date_type = 'Gift Create Date', @used_only = 'N',
        --    @entitlement_str = '', @title_str = '', @prod_str = '', @giver_no = 0, @list_no = 0


        --SELECT DISTINCT entitlement_no FROM dbo.LV_GIFT_ENTITLEMENTS WHERE create_dt BETWEEN '1-1-2019' AND '1-31-2019'


      

--SELECT * FROM dbo.T_LIST_CONTENTS WHERE customer_no = 3743448
--SELECT * FROM [dbo].[LV_GIFT_ENTITLEMENTS] WHERE pass_status = 'Used'
--SELECT * FROM LT_ENTITLEMENT_GIFT AS gft
--SELECT  @report_start_dt = '1-1-2019', @report_end_dt = '1-31-2019', @date_type = 'Redeemed Performance Date', @used_only = 'Y'
--SELECT @title_str = '"161"', @prod_str = '"41497","43765"', @giver_no = 0, @list_no = 8574
--SELECT * FROM dbo.LTR_ENTITLEMENT
--SELECT * FROM dbo.TR_GOOESOFT_DROPDOWN WHERE code = 1
--SELECT * FROM dbo.T_INVENTORY WHERE type = 'T'
--SELECT * FROM dbo.gooesoft_report_parameter WHERE report_id IN (SELECT id FROM dbo.gooesoft_report WHERE name LIKE 'MOS:%') and description LIKE '%list%'


/*

gift_id     giver_no    giver_name                                                                                           giver_name_sort                                                                                      recipient_no recipient_name                                                                                       recipient_name_sort                                                                                  recipient_email                                                                                      gift_code                      group_gift_code                create_dt               init_dt                 expr_dt                 ltx_cust_entitlement_id entitlement_no entitlement_code               entitlement_desc                                                                                     ent_price_type_group price_type_group_name          ent_tkw_id  keyword                                            num_items   gift_status                    gift_status_name               counter_all counter_created counter_accepted counter_other counter_return_to_giver set_no      set_name                       order_no    order_source_no order_source                                       sli_no      perf_no     performance_no performance_zone performance_dt          performance_date performance_time performance_time_display       title_no    title_name                     production_no production_name                num_used    num_used_gate num_used_box num_used_sci num_used_web num_used_other pass_status                    price_type_desc                shord_desc                     report_start_dt         report_end_dt           display_entitlement display_price_type display_Redeem_title display_redeem_production display_giver
----------- ----------- ---------------------------------------------------------------------------------------------------- ---------------------------------------------------------------------------------------------------- ------------ ---------------------------------------------------------------------------------------------------- ---------------------------------------------------------------------------------------------------- ---------------------------------------------------------------------------------------------------- ------------------------------ ------------------------------ ----------------------- ----------------------- ----------------------- ----------------------- -------------- ------------------------------ ---------------------------------------------------------------------------------------------------- -------------------- ------------------------------ ----------- -------------------------------------------------- ----------- ------------------------------ ------------------------------ ----------- --------------- ---------------- ------------- ----------------------- ----------- ------------------------------ ----------- --------------- -------------------------------------------------- ----------- ----------- -------------- ---------------- ----------------------- ---------------- ---------------- ------------------------------ ----------- ------------------------------ ------------- ------------------------------ ----------- ------------- ------------ ------------ ------------ -------------- ------------------------------ ------------------------------ ------------------------------ ----------------------- ----------------------- ------------------- ------------------ -------------------- ------------------------- -------------
16282       3850751     Test Again                                                                                           Again/Test                                                                                           3850752      Test AgainTwo                                                                                        AgainTwo/Test                                                                                                                                                                                             19CE-6282-981D                 19CE-6282-981D                 2019-06-26 07:48:08.710 2019-06-26 00:00:00.000 2020-06-30 23:59:59.000 699854                  5              P5 EH PASS                     Premier 5 Membership Exhibit Hall Pass                                                               2                    Membership Bonus Passes        18          Exhibit Halls                                      1           A                              Accepted                       1           0               1                0             0                       0           No Set                         1799598     1               Redeemed Through Web                               8111172     41396       41396          236              2019-06-26 23:00:00.000 2019/06/26       09:00            EX GA                          27          Exhibit Halls                  32            Exhibit Halls                  1           0             0            0            1            0              Used                           Member Pass                    MEM PASS                       2019-06-26 00:00:00.000 2019-06-26 23:59:59.957 1                   1                  1                    1                         1
16283       3850751     Test Again                                                                                           Again/Test                                                                                           3850752      Test AgainTwo                                                                                        AgainTwo/Test                                                                                                                                                                                             4443-6283-8915                 4443-6283-8915                 2019-06-26 07:48:08.797 2019-06-26 00:00:00.000 2020-06-30 23:59:59.000 699853                  31             P5 OM PASS                     Premier 5 Membership Omni Pass                                                                       2                    Membership Bonus Passes        14          Omni                                               1           A                              Accepted                       1           0               1                0             0                       0           No Set                         1799598     1               Redeemed Through Web                               8111173     55847       55847          208              2019-06-26 23:30:00.000 2019/06/26       12:00            12:00 PM                       161         Mugar Omni Theater             48713         Volcanoes                      1           0             0            0            1            0              Used                           Member Pass                    MEM PASS                       2019-06-26 00:00:00.000 2019-06-26 23:59:59.957 1                   1                  1                    1                         1
16284       3850751     Test Again                                                                                           Again/Test                                                                                           3850752      Test AgainTwo                                                                                        AgainTwo/Test                                                                                                                                                                                             6BA4-6284-AA0F                 6BA4-6284-AA0F                 2019-06-26 07:48:08.883 2019-06-26 00:00:00.000 2020-06-30 23:59:59.000 699852                  32             P5 PL PASS                     Premier 5 Membership Planetarium Pass                                                                2                    Membership Bonus Passes        15          Planetarium                                        1           A                              Accepted                       1           0               1                0             0                       0           No Set                         1799598     1               Redeemed Through Web                               8111174     49567       49567          221              2019-06-26 23:30:00.000 2019/06/26       12:30            12:30 PM                       1132        Hayden Planetarium             45692         Destination Mars               1           0             0            0            1            0              Used                           Member Pass                    MEM PASS                       2019-06-26 00:00:00.000 2019-06-26 23:59:59.957 1                   1                  1                    1                         1
16291       3850754     Dino Giver                                                                                           Giver/Dino                                                                                           3850755      Dino Recipient                                                                                       Recipient/Dino                                                                                                                                                                                            D529-6291-BEEE                 D529-6291-BEEE                 2019-06-26 13:44:03.810 2019-06-26 00:00:00.000 2020-04-30 23:59:59.997 699866                  5              P5 EH PASS                     Premier 5 Membership Exhibit Hall Pass                                                               2                    Membership Bonus Passes        18          Exhibit Halls                                      1           A                              Accepted                       1           0               1                0             0                       0           No Set                         1799610     20              Redeemed Through Science Central                   8111197     41396       41396          236              2019-06-26 23:00:00.000 2019/06/26       09:00            EX GA                          27          Exhibit Halls                  32            Exhibit Halls                  1           0             0            1            0            0              Used                           Member Pass                    MEM PASS                       2019-06-26 00:00:00.000 2019-06-26 23:59:59.957 1                   1                  1                    1                         1
16291       3850754     Dino Giver                                                                                           Giver/Dino                                                                                           3850755      Dino Recipient                                                                                       Recipient/Dino                                                                                                                                                                                            D529-6291-BEEE                 D529-6291-BEEE                 2019-06-26 13:44:03.810 2019-06-26 00:00:00.000 2020-04-30 23:59:59.997 699866                  5              P5 EH PASS                     Premier 5 Membership Exhibit Hall Pass                                                               2                    Membership Bonus Passes        18          Exhibit Halls                                      1           A                              Accepted                       1           0               1                0             0                       0           No Set                         1799610     20              Redeemed Through Science Central                   8111198     41396       41396          236              2019-06-26 23:00:00.000 2019/06/26       09:00            EX GA                          27          Exhibit Halls                  32            Exhibit Halls                  1           0             0            1            0            0              Used                           Member Pass                    MEM PASS                       2019-06-26 00:00:00.000 2019-06-26 23:59:59.957 1                   1                  1                    1                         1
16294       3850751     Test Again                                                                                           Again/Test                                                                                           3850757      sdflsjdfl lsdjflsjdf                                                                                 lsdjflsjdf/sdflsjdfl                                                                                 mfinn@Mos.org                                                                                        7816-6292-8CE2                 210D-6292-9801                 2019-06-26 13:51:10.243 2019-06-26 11:19:18.270 2020-06-25 11:19:18.270 699864                  1083           EH PASS                        EH Pass                                                                                              8                    Electronic entitlements        18          Exhibit Halls                                      1           A                              Accepted                       1           0               1                0             0                       0           No Set                         1799613     1               Redeemed Through Web                               8111203     52639       52639          359              2019-07-06 23:30:00.000 2019/07/06       23:30            11:30 PM                       5541        Vouchers                       6081          Exhibit Halls Voucher          1           0             0            0            1            0              Used                           Electronic Pass                E-Pass                         2019-06-26 00:00:00.000 2019-06-26 23:59:59.957 1                   1                  1                    1                         1
16293       3850751     Test Again                                                                                           Again/Test                                                                                           3850757      sdflsjdfl lsdjflsjdf                                                                                 lsdjflsjdf/sdflsjdfl                                                                                 mfinn@Mos.org                                                                                        210D-6292-9801                 210D-6292-9801                 2019-06-26 13:51:10.243 2019-03-28 00:00:00.000 2020-06-25 11:19:18.270 699864                  1083           EH PASS                        EH Pass                                                                                              8                    Electronic entitlements        18          Exhibit Halls                                      1           A                              Accepted                       1           0               1                0             0                       0           No Set                         1799613     1               Redeemed Through Web                               8111204     52639       52639          359              2019-07-06 23:30:00.000 2019/07/06       23:30            11:30 PM                       5541        Vouchers                       6081          Exhibit Halls Voucher          1           0             0            0            1            0              Used                           Electronic Pass                E-Pass                         2019-06-26 00:00:00.000 2019-06-26 23:59:59.957 1                   1                  1                    1                         1
16297       3850751     Test Again                                                                                           Again/Test                                                                                           3850757      sdflsjdfl lsdjflsjdf                                                                                 lsdjflsjdf/sdflsjdfl                                                                                 mfinn@Mos.org                                                                                        C1AD-6295-BF04                 210D-6292-9801                 2019-06-26 13:51:10.603 2019-03-28 00:00:00.000 2020-12-23 23:30:00.000 699873                  1140           BW PASS                        Body Worlds Pass                                                                                     8                    Electronic entitlements        93          Special Exhibit                                    1           A                              Accepted                       1           0               1                0             0                       0           No Set                         1799613     1               Redeemed Through Web                               8111205     50600       50600          601              2019-07-06 23:30:00.000 2019/07/06       10:40            10:40 AM                       37          Special Exhibitions            50577         BODY WORLDS: The Cycle of Life 1           0             0            0            1            0              Used                           Electronic Pass                E-Pass                         2019-06-26 00:00:00.000 2019-06-26 23:59:59.957 1                   1                  1                    1                         1
16296       3850751     Test Again                                                                                           Again/Test                                                                                           3850757      sdflsjdfl lsdjflsjdf                                                                                 lsdjflsjdf/sdflsjdfl                                                                                 mfinn@Mos.org                                                                                        06BD-6295-A55C                 210D-6292-9801                 2019-06-26 13:51:10.603 2019-03-28 00:00:00.000 2020-12-23 23:30:00.000 699873                  1140           BW PASS                        Body Worlds Pass                                                                                     8                    Electronic entitlements        93          Special Exhibit                                    1           A                              Accepted                       1           0               1                0             0                       0           No Set                         1799613     1               Redeemed Through Web                               8111206     50600       50600          601              2019-07-06 23:30:00.000 2019/07/06       10:40            10:40 AM                       37          Special Exhibitions            50577         BODY WORLDS: The Cycle of Life 1           0             0            0            1            0              Used                           Electronic Pass                E-Pass                         2019-06-26 00:00:00.000 2019-06-26 23:59:59.957 1                   1                  1                    1                         1


gift_id     giver_no    giver_name                                                                                           giver_name_sort                                                                                      recipient_no recipient_name                                                                                       recipient_name_sort                                                                                  recipient_email                                                                                      gift_code                      group_gift_code                create_dt               init_dt                 expr_dt                 ltx_cust_entitlement_id entitlement_no entitlement_code               entitlement_desc                                                                                     ent_price_type_group price_type_group_name          ent_tkw_id  keyword                                            num_items   gift_status                    gift_status_name               counter_all counter_created counter_accepted counter_other counter_return_to_giver set_no      set_name                       order_no    order_source_no order_source                                       sli_no      perf_no     performance_no performance_zone performance_dt          performance_date performance_time performance_time_display       title_no    title_name                     production_no production_name                num_used    num_used_gate num_used_box num_used_sci num_used_web num_used_other pass_status                    price_type_desc                shord_desc                     report_start_dt         report_end_dt           display_entitlement display_price_type display_Redeem_title display_redeem_production display_giver
----------- ----------- ---------------------------------------------------------------------------------------------------- ---------------------------------------------------------------------------------------------------- ------------ ---------------------------------------------------------------------------------------------------- ---------------------------------------------------------------------------------------------------- ---------------------------------------------------------------------------------------------------- ------------------------------ ------------------------------ ----------------------- ----------------------- ----------------------- ----------------------- -------------- ------------------------------ ---------------------------------------------------------------------------------------------------- -------------------- ------------------------------ ----------- -------------------------------------------------- ----------- ------------------------------ ------------------------------ ----------- --------------- ---------------- ------------- ----------------------- ----------- ------------------------------ ----------- --------------- -------------------------------------------------- ----------- ----------- -------------- ---------------- ----------------------- ---------------- ---------------- ------------------------------ ----------- ------------------------------ ------------- ------------------------------ ----------- ------------- ------------ ------------ ------------ -------------- ------------------------------ ------------------------------ ------------------------------ ----------------------- ----------------------- ------------------- ------------------ -------------------- ------------------------- -------------
16282       3850751     Test Again                                                                                           Again/Test                                                                                           3850752      Test AgainTwo                                                                                        AgainTwo/Test                                                                                                                                                                                             19CE-6282-981D                 19CE-6282-981D                 2019-06-26 07:48:08.710 2019-06-26 00:00:00.000 2020-06-30 23:59:59.000 699854                  5              P5 EH PASS                     Premier 5 Membership Exhibit Hall Pass                                                               2                    Membership Bonus Passes        18          Exhibit Halls                                      1           A                              Accepted                       1           0               1                0             0                       0           No Set                         1799598     1               Redeemed Through Web                               8111172     41396       41396          236              2019-06-26 23:00:00.000 2019/06/26       09:00            EX GA                          27          Exhibit Halls                  32            Exhibit Halls                  1           0             0            0            1            0              Used                           Member Pass                    MEM PASS                       2019-06-26 00:00:00.000 2019-06-26 23:59:59.957 1                   1                  1                    1                         1
16283       3850751     Test Again                                                                                           Again/Test                                                                                           3850752      Test AgainTwo                                                                                        AgainTwo/Test                                                                                                                                                                                             4443-6283-8915                 4443-6283-8915                 2019-06-26 07:48:08.797 2019-06-26 00:00:00.000 2020-06-30 23:59:59.000 699853                  31             P5 OM PASS                     Premier 5 Membership Omni Pass                                                                       2                    Membership Bonus Passes        14          Omni                                               1           A                              Accepted                       1           0               1                0             0                       0           No Set                         1799598     1               Redeemed Through Web                               8111173     55847       55847          208              2019-06-26 23:30:00.000 2019/06/26       12:00            12:00 PM                       161         Mugar Omni Theater             48713         Volcanoes                      1           0             0            0            1            0              Used                           Member Pass                    MEM PASS                       2019-06-26 00:00:00.000 2019-06-26 23:59:59.957 1                   1                  1                    1                         1
16284       3850751     Test Again                                                                                           Again/Test                                                                                           3850752      Test AgainTwo                                                                                        AgainTwo/Test                                                                                                                                                                                             6BA4-6284-AA0F                 6BA4-6284-AA0F                 2019-06-26 07:48:08.883 2019-06-26 00:00:00.000 2020-06-30 23:59:59.000 699852                  32             P5 PL PASS                     Premier 5 Membership Planetarium Pass                                                                2                    Membership Bonus Passes        15          Planetarium                                        1           A                              Accepted                       1           0               1                0             0                       0           No Set                         1799598     1               Redeemed Through Web                               8111174     49567       49567          221              2019-06-26 23:30:00.000 2019/06/26       12:30            12:30 PM                       1132        Hayden Planetarium             45692         Destination Mars               1           0             0            0            1            0              Used                           Member Pass                    MEM PASS                       2019-06-26 00:00:00.000 2019-06-26 23:59:59.957 1                   1                  1                    1                         1
16291       3850754     Dino Giver                                                                                           Giver/Dino                                                                                           3850755      Dino Recipient                                                                                       Recipient/Dino                                                                                                                                                                                            D529-6291-BEEE                 D529-6291-BEEE                 2019-06-26 13:44:03.810 2019-06-26 00:00:00.000 2020-04-30 23:59:59.997 699866                  5              P5 EH PASS                     Premier 5 Membership Exhibit Hall Pass                                                               2                    Membership Bonus Passes        18          Exhibit Halls                                      1           A                              Accepted                       1           0               1                0             0                       0           No Set                         1799610     20              Redeemed Through Science Central                   8111197     41396       41396          236              2019-06-26 23:00:00.000 2019/06/26       09:00            EX GA                          27          Exhibit Halls                  32            Exhibit Halls                  1           0             0            1            0            0              Used                           Member Pass                    MEM PASS                       2019-06-26 00:00:00.000 2019-06-26 23:59:59.957 1                   1                  1                    1                         1
16291       3850754     Dino Giver                                                                                           Giver/Dino                                                                                           3850755      Dino Recipient                                                                                       Recipient/Dino                                                                                                                                                                                            D529-6291-BEEE                 D529-6291-BEEE                 2019-06-26 13:44:03.810 2019-06-26 00:00:00.000 2020-04-30 23:59:59.997 699866                  5              P5 EH PASS                     Premier 5 Membership Exhibit Hall Pass                                                               2                    Membership Bonus Passes        18          Exhibit Halls                                      1           A                              Accepted                       1           0               1                0             0                       0           No Set                         1799610     20              Redeemed Through Science Central                   8111198     41396       41396          236              2019-06-26 23:00:00.000 2019/06/26       09:00            EX GA                          27          Exhibit Halls                  32            Exhibit Halls                  1           0             0            1            0            0              Used                           Member Pass                    MEM PASS                       2019-06-26 00:00:00.000 2019-06-26 23:59:59.957 1                   1                  1                    1                         1
16294       3850751     Test Again                                                                                           Again/Test                                                                                           3850757      sdflsjdfl lsdjflsjdf                                                                                 lsdjflsjdf/sdflsjdfl                                                                                 mfinn@Mos.org                                                                                        7816-6292-8CE2                 210D-6292-9801                 2019-06-26 13:51:10.243 2019-06-26 11:19:18.270 2020-06-25 11:19:18.270 699864                  1083           EH PASS                        EH Pass                                                                                              8                    Electronic entitlements        18          Exhibit Halls                                      1           A                              Accepted                       1           0               1                0             0                       0           No Set                         1799613     1               Redeemed Through Web                               8111203     52639       52639          359              2019-07-06 23:30:00.000 2019/07/06       23:30            11:30 PM                       5541        Vouchers                       6081          Exhibit Halls Voucher          1           0             0            0            1            0              Used                           Electronic Pass                E-Pass                         2019-06-26 00:00:00.000 2019-06-26 23:59:59.957 1                   1                  1                    1                         1
16293       3850751     Test Again                                                                                           Again/Test                                                                                           3850757      sdflsjdfl lsdjflsjdf                                                                                 lsdjflsjdf/sdflsjdfl                                                                                 mfinn@Mos.org                                                                                        210D-6292-9801                 210D-6292-9801                 2019-06-26 13:51:10.243 2019-03-28 00:00:00.000 2020-06-25 11:19:18.270 699864                  1083           EH PASS                        EH Pass                                                                                              8                    Electronic entitlements        18          Exhibit Halls                                      1           A                              Accepted                       1           0               1                0             0                       0           No Set                         1799613     1               Redeemed Through Web                               8111204     52639       52639          359              2019-07-06 23:30:00.000 2019/07/06       23:30            11:30 PM                       5541        Vouchers                       6081          Exhibit Halls Voucher          1           0             0            0            1            0              Used                           Electronic Pass                E-Pass                         2019-06-26 00:00:00.000 2019-06-26 23:59:59.957 1                   1                  1                    1                         1
16297       3850751     Test Again                                                                                           Again/Test                                                                                           3850757      sdflsjdfl lsdjflsjdf                                                                                 lsdjflsjdf/sdflsjdfl                                                                                 mfinn@Mos.org                                                                                        C1AD-6295-BF04                 210D-6292-9801                 2019-06-26 13:51:10.603 2019-03-28 00:00:00.000 2020-12-23 23:30:00.000 699873                  1140           BW PASS                        Body Worlds Pass                                                                                     8                    Electronic entitlements        93          Special Exhibit                                    1           A                              Accepted                       1           0               1                0             0                       0           No Set                         1799613     1               Redeemed Through Web                               8111205     50600       50600          601              2019-07-06 23:30:00.000 2019/07/06       10:40            10:40 AM                       37          Special Exhibitions            50577         BODY WORLDS: The Cycle of Life 1           0             0            0            1            0              Used                           Electronic Pass                E-Pass                         2019-06-26 00:00:00.000 2019-06-26 23:59:59.957 1                   1                  1                    1                         1
16296       3850751     Test Again                                                                                           Again/Test                                                                                           3850757      sdflsjdfl lsdjflsjdf                                                                                 lsdjflsjdf/sdflsjdfl                                                                                 mfinn@Mos.org                                                                                        06BD-6295-A55C                 210D-6292-9801                 2019-06-26 13:51:10.603 2019-03-28 00:00:00.000 2020-12-23 23:30:00.000 699873                  1140           BW PASS                        Body Worlds Pass                                                                                     8                    Electronic entitlements        93          Special Exhibit                                    1           A                              Accepted                       1           0               1                0             0                       0           No Set                         1799613     1               Redeemed Through Web                               8111206     50600       50600          601              2019-07-06 23:30:00.000 2019/07/06       10:40            10:40 AM                       37          Special Exhibitions            50577         BODY WORLDS: The Cycle of Life 1           0             0            0            1            0              Used                           Electronic Pass                E-Pass                         2019-06-26 00:00:00.000 2019-06-26 23:59:59.957 1                   1                  1                    1                         1
16282       3850751     Test Again                                                                                           Again/Test                                                                                           3850752      Test AgainTwo                                                                                        AgainTwo/Test                                                                                                                                                                                             19CE-6282-981D                                                2019-06-26 07:48:08.710 NULL                    NULL                    0                       0              P5 EH PASS                     Premier 5 Membership Exhibit Hall Pass                                                               2                    Membership Bonus Passes        18          Exhibit Halls                                      1           A                              Accepted                       1           0               1                0             0                       0                                          0           0               Not Redeemed                                       0           0           0              0                NULL                                                                                     0                                          0                                            0           0             0            0            0            0              Not Used                                                                                     2019-06-26 00:00:00.000 2019-06-26 23:59:59.957 1                   1                  1                    1                         1
16284       3850751     Test Again                                                                                           Again/Test                                                                                           3850752      Test AgainTwo                                                                                        AgainTwo/Test                                                                                                                                                                                             6BA4-6284-AA0F                                                2019-06-26 07:48:08.883 NULL                    NULL                    0                       0              P5 PL PASS                     Premier 5 Membership Planetarium Pass                                                                2                    Membership Bonus Passes        15          Planetarium                                        1           A                              Accepted                       1           0               1                0             0                       0                                          0           0               Not Redeemed                                       0           0           0              0                NULL                                                                                     0                                          0                                            0           0             0            0            0            0              Not Used                                                                                     2019-06-26 00:00:00.000 2019-06-26 23:59:59.957 1                   1                  1                    1                         1
16283       3850751     Test Again                                                                                           Again/Test                                                                                           3850752      Test AgainTwo                                                                                        AgainTwo/Test                                                                                                                                                                                             4443-6283-8915                                                2019-06-26 07:48:08.797 NULL                    NULL                    0                       0              P5 OM PASS                     Premier 5 Membership Omni Pass                                                                       2                    Membership Bonus Passes        14          Omni                                               1           A                              Accepted                       1           0               1                0             0                       0                                          0           0               Not Redeemed                                       0           0           0              0                NULL                                                                                     0                                          0                                            0           0             0            0            0            0              Not Used                                                                                     2019-06-26 00:00:00.000 2019-06-26 23:59:59.957 1                   1                  1                    1                         1

*/
