USE impresario;
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_ATTEND_WEEKLY_SCHOOL_VISITS]') AND type in (N'P', N'PC')) BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_ATTEND_WEEKLY_SCHOOL_VISITS] AS';
    EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LRP_ATTEND_WEEKLY_SCHOOL_VISITS] TO impusers';
END;
GO

ALTER PROCEDURE [dbo].[LRP_ATTEND_WEEKLY_SCHOOL_VISITS]
        @week_ending DATETIME = NULL
AS BEGIN

    SET NOCOUNT ON;
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    
    /*  Procedure Variables  */

        DECLARE @fiscal_year INT = 0
        DECLARE @week_start_dt DATETIME, @week_end_dt DATETIME;
        --DECLARE @month_start_dt DATETIME, @month_end_dt DATETIME;

        DECLARE @prev_fiscal_year INT = 0
        DECLARE @prev_week_start_dt DATETIME, @prev_week_end_dt DATETIME; 
        --DECLARE @prev_month_start_dt DATETIME, @prev_month_end_dt DATETIME;
        
        DECLARE @exhibit_halls_no INT = 27
        DECLARE @exhibit_halls_school_no INT = 1233
        
    /*  Temporary Table  */

        IF OBJECT_ID('tempdb..#exhibit_hall_attendance') is not null DROP TABLE [#exhibit_hall_attendance]

        CREATE TABLE [#exhibit_hall_attendance] ([fiscal_year] VARCHAR(25) NOT NULL DEFAULT(''),
                                                 [performance_dt] DATETIME NULL,
                                                 [performance_day] VARCHAR(10) NOT NULL DEFAULT(''),
                                                 [member_scans] INT NOT NULL DEFAULT(0),
                                                 [exhibit_hall_attendance] INT NOT NULL DEFAULT(0),
                                                 [exhibit_hall_show_and_go] INT NOT NULL DEFAULT(0),
                                                 [exhibit_hall_total] INT NOT NULL DEFAULT(0),
                                                 [exhibit_hall_attendance_with_members] INT NOT NULL DEFAULT(0),
                                                 [percent_of_exhibit_hall_attendance] INT NOT NULL DEFAULT(0),
                                                 [closed_msg] VARCHAR(10) NOT NULL DEFAULT(''))

        IF OBJECT_ID('tempdb..#final_table') is not null DROP TABLE [#final_table]

        CREATE TABLE [#final_table] ([fiscal_year] VARCHAR(25) NOT NULL DEFAULT(''),
                                     [performance_dt] DATETIME NULL,
                                     [performance_day] VARCHAR(10) NOT NULL DEFAULT(''),
                                     [exhibit_halls_school] INT NOT NULL DEFAULT(0),
                                     [exhibit_hall_attendance] INT NOT NULL DEFAULT(0),
                                     [percent_of_exhibit_hall_attendance] DECIMAL(18,4) NOT NULL DEFAULT(0.0),
                                     [closed_msg] VARCHAR(10) NOT NULL DEFAULT(''))

    /*  Check Parameters  */

        SELECT @week_ending = ISNULL(@week_ending,GETDATE())
        
    /*  Get date values from LFT_ATTENDANCE_WEEK_DATES function  */
        
        SELECT @fiscal_year = [cur_fiscal_year], 
               @week_start_dt = [cur_week_start_dt], 
               @week_end_dt = [cur_week_end_dt],  
               @prev_fiscal_year = [prv_fiscal], 
               @prev_week_start_dt = [prv_week_start_dt], 
               @prev_week_end_dt = [prv_week_end_dt]
        FROM [dbo].[LFT_ATTENDANCE_WEEK_DATES] (@week_ending, 0, 'Y')


    /*  Use the Weekly Member Visit procedure to get full exhibit Hall Attendance  */

        INSERT INTO [#exhibit_hall_attendance] ([fiscal_year], [performance_dt], [performance_day], [member_scans], [exhibit_hall_attendance], [exhibit_hall_show_and_go], 
                                                [exhibit_hall_total], [exhibit_hall_attendance_with_members], [percent_of_exhibit_hall_attendance], [closed_msg])
        EXECUTE [dbo].[LRP_ATTEND_WEEKLY_MEMBER_VISITS] @week_ending = @week_ending


/*  Get School Visitataion Info  */    

        INSERT INTO [#final_table] ([fiscal_year], [performance_dt], [performance_day], [exhibit_halls_school], [exhibit_hall_attendance], [percent_of_exhibit_hall_attendance], [closed_msg])
            SELECT CASE WHEN TRY_CONVERT(DATE,tik.[performance_date]) BETWEEN @week_start_dt AND @week_end_dt THEN 'FY ' + CAST(@fiscal_year AS VARCHAR(4))
                        WHEN TRY_CONVERT(DATE,tik.[performance_date]) BETWEEN @prev_week_start_dt AND @prev_week_end_dt THEN 'FY ' + CAST(@prev_fiscal_year AS VARCHAR(4))
                        ELSE '' END AS [fiscal_year],
                   TRY_CONVERT(DATE,tik.[performance_date]) AS [performance_dt],
                   LEFT(DATENAME(WEEKDAY,TRY_CONVERT(DATE,tik.[performance_date])),3) AS [performance_day],
                   SUM(tik.[sale_total]) AS [exhibit_halls_school],
                   exh.[exhibit_hall_attendance_with_members] AS [exhibit_hall_attendance],
                   CAST(CASE WHEN exh.[exhibit_hall_attendance_with_members] = 0 THEN 0.0
                             ELSE (CAST(SUM(tik.[sale_total]) AS DECIMAL(18,4)) / CAST(exh.[exhibit_hall_attendance_with_members] AS DECIMAL(18,4)))
                             END AS DECIMAL(18,4)) AS [percent_of_exhibit_hall_attendance],
                   exh.[closed_msg]
            FROM [dbo].[LT_HISTORY_TICKET] AS tik
                    LEFT OUTER JOIN [#exhibit_hall_attendance] AS exh ON exh.[performance_dt] = TRY_CONVERT(DATE,tik.[performance_date])
                    LEFT OUTER JOIN [dbo].[T_ORDER] AS ord ON ord.[order_no] = tik.[order_no]
            WHERE (TRY_CONVERT(DATE,tik.[performance_date]) BETWEEN @week_start_dt and @week_end_dt
                   OR TRY_CONVERT(DATE,tik.[performance_date]) BETWEEN @prev_week_start_dt AND @prev_week_end_dt)
                AND tik.[title_no] = @exhibit_halls_no
                AND ord.[MOS] IN (12,13)                
            GROUP BY CASE WHEN TRY_CONVERT(DATE,tik.[performance_date]) BETWEEN @week_start_dt AND @week_end_dt THEN 'FY ' + CAST(@fiscal_year AS VARCHAR(4))
                            WHEN TRY_CONVERT(DATE,tik.[performance_date]) BETWEEN @prev_week_start_dt AND @prev_week_end_dt THEN 'FY ' + CAST(@prev_fiscal_year AS VARCHAR(4))
                            ELSE '' END,
                        TRY_CONVERT(DATE,tik.[performance_date]), 
                        DATENAME(WEEKDAY,TRY_CONVERT(DATE,tik.[performance_date])),
                        exh.[exhibit_hall_attendance_with_members],exh.[closed_msg]
            ORDER BY [fiscal_year] DESC,
                        TRY_CONVERT(DATE,tik.[performance_date])

    /*  Add in missing days so that the grid is complete  */

        INSERT INTO [#final_table] ([fiscal_year], [performance_dt], [performance_day], [exhibit_halls_school], [exhibit_hall_attendance], [percent_of_exhibit_hall_attendance], [closed_msg])                   
            SELECT DISTINCT fiscal_year,
                            performance_dt,
                            performance_day,
                            0,
                            [exhibit_hall_attendance_with_members],
                            0.0,
                            [closed_msg]
            FROM [#exhibit_hall_attendance]                    
            WHERE [performance_dt] NOT IN (SELECT [performance_dt] 
                                           FROM [#final_table])

    FINISHED:
       
            SELECT [fiscal_year],
                   [performance_dt],
                   [performance_day],
                   [exhibit_halls_school],
                   [exhibit_hall_attendance],
                   [percent_of_exhibit_hall_attendance],
                   [closed_msg]
            FROM [#final_table]
            ORDER BY fiscal_year DESC, performance_dt


    DONE:

        IF OBJECT_ID('tempdb..#exhibit_hall_attendance') is not null DROP TABLE [#exhibit_hall_attendance]

END
GO


EXECUTE [dbo].[LRP_ATTEND_WEEKLY_SCHOOL_VISITS] '1-26-2020'



/*
fiscal_year               performance_dt          performance_day exhibit_halls_school exhibit_hall_attendance percent_of_exhibit_hall_attendance      closed_msg
------------------------- ----------------------- --------------- -------------------- ----------------------- --------------------------------------- ----------
FY 2020                   2020-01-20 00:00:00.000 Mon             0                    5515                    0.0000                                  
FY 2020                   2020-01-21 00:00:00.000 Tue             437                  1101                    0.3969                                  
FY 2020                   2020-01-22 00:00:00.000 Wed             464                  1059                    0.4381                                  
FY 2020                   2020-01-23 00:00:00.000 Thu             758                  1333                    0.5686                                  
FY 2020                   2020-01-24 00:00:00.000 Fri             705                  1755                    0.4017                                  
FY 2020                   2020-01-25 00:00:00.000 Sat             12                   4004                    0.0030                                  
FY 2020                   2020-01-26 00:00:00.000 Sun             0                    3036                    0.0000                                  
FY 2019                   2019-01-21 00:00:00.000 Mon             0                    3805                    0.0000                                  
FY 2019                   2019-01-22 00:00:00.000 Tue             271                  960                     0.2823                                  
FY 2019                   2019-01-23 00:00:00.000 Wed             481                  1035                    0.4647                                  
FY 2019                   2019-01-24 00:00:00.000 Thu             1372                 2088                    0.6571                                  
FY 2019                   2019-01-25 00:00:00.000 Fri             1901                 3116                    0.6101                                  
FY 2019                   2019-01-26 00:00:00.000 Sat             101                  4681                    0.0216                                  
FY 2019                   2019-01-27 00:00:00.000 Sun             0                    3485                    0.0000                                  
*/