USE impresario
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_ADV_CAMPAIGN_ACTIVITY_SOL_PIPE_QUAR_SUMMARY]') AND type IN (N'P', N'PC'))
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_ADV_CAMPAIGN_ACTIVITY_SOL_PIPE_QUAR_SUMMARY] AS' 
GO

EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LRP_ADV_CAMPAIGN_ACTIVITY_SOL_PIPE_QUAR_SUMMARY] TO [impusers], [tessitura_app]'
GO

/*      LRP_ADV_CAMPAIGN_ACTIVITY_SOL_PIPE_QUAR_SUMMARY
        Pulls information for goals, numbers, and YTD percentages for all step activity, vists, ask/yield amounts, and actual contributions taken in.

        NOTE: Contribution amounts are based on what's in The Plan record under cont_amount

*/
ALTER PROCEDURE [dbo].[LRP_ADV_CAMPAIGN_ACTIVITY_SOL_PIPE_QUAR_SUMMARY]
        @fiscal_year INT = NULL,
        @workers_str VARCHAR(4000) = NULL
WITH RECOMPILE AS BEGIN

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

    /*  Procedure Variables  */

        DECLARE @fiscal_start_dt DATETIME = NULL, @fiscal_end_dt DATETIME = NULL
        DECLARE @current_dt DATETIME = GETDATE()

        DECLARE @worker_list TABLE ([worker_no] INT NOT NULL DEFAULT (0))

        DECLARE @quarter_list TABLE ([quarter_no] INT, [quarter_display] VARCHAR(10))
        DECLARE @probability_list TABLE ([probability_no] INT, [probability_sort] CHAR(3), [probability_display] VARCHAR(10))


    /*  Templorary Tables  */
        
        IF OBJECT_ID('tempdb..#plan_data') IS NOT NULL DROP TABLE #plan_data

        CREATE TABLE #plan_data ([plan_no] INT NOT NULL DEFAULT (0),
                                 [worker_customer_no] INT NOT NULL DEFAULT (0),
                                 [worker_name] VARCHAR(75) NOT NULL DEFAULT (0),
                                 [worker_initials] VARCHAR(75) NOT NULL DEFAULT (0),
                                 [worker_inactive] CHAR(1) NOT NULL DEFAULT ('N'),
                                 [complete_by_dt] DATETIME NULL,
                                 [complete_by_quarter] VARCHAR(2) NOT NULL DEFAULT (''),
                                 [ask_amount] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                 [goal_amount] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                 [plan_amount] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                 [contribution_amount] DECIMAL (18,2) NOT NULL DEFAULT (0.0),
                                 [plan_status_id] INT NOT NULL DEFAULT (0),
                                 [plan_status] VARCHAR(50) NOT NULL DEFAULT (''),
                                 [plan_probability] DECIMAL (18,2) NOT NULL DEFAULT (0.0),
                                 [Plan_probability_display] VARCHAR(10) NOT NULL DEFAULT (''),
                                 [plan_probability_sort] CHAR(3) NOT NULL DEFAULT ('EEE'))

        IF OBJECT_ID('tempdb..#final_table') IS NOT NULL DROP TABLE #final_table

        CREATE TABLE #final_table ([plan_probability_sort] CHAR(3) NOT NULL DEFAULT ('EEE'), 
                     [plan_probability_display] VARCHAR(10) NOT NULL DEFAULT (''), 
                     [plan_status] VARCHAR(30) NOT NULL DEFAULT (''), 
                     [complete_by_quarter] VARCHAR(10) NOT NULL DEFAULT (''), 
                     [plan_count] INT NOT NULL DEFAULT (0), 
                     [ask_amount] DECIMAL(18,2) NOT NULL DEFAULT (0.0), 
                     [goal_amount] DECIMAL(18,2) NOT NULL DEFAULT (0.0))

    /*  Check Parameters  */

        IF @fiscal_year IS NULL SELECT @fiscal_year = dbo.LF_GetFiscalYear(GETDATE())
        
        SELECT @fiscal_start_dt = [dbo].[LF_GetFiscalYearStartDt](@fiscal_year)
        SELECT @fiscal_end_dt = [dbo].[LF_GetFiscalYearEndDt](@fiscal_year)

        IF @current_dt > @fiscal_end_dt SELECT @current_dt = @fiscal_end_dt

         IF ISNULL(@workers_str,'') = ''
            INSERT INTO @worker_list ([worker_no])
            SELECT ISNULL([worker_customer_no], 0)
            FROM [dbo].[LV_CAMPAIGN_ACTIVITY_WORKERS] 
            WHERE [inactive] = 'N'
              AND ISNULL([worker_customer_no], 0) > 0
        ELSE
            INSERT INTO @worker_list ([worker_no])
            SELECT ISNULL(TRY_CAST([Element] AS INTEGER),0) 
            FROM [dbo].[FT_SPLIT_LIST](REPLACE(@workers_str,'"',''),',')
            WHERE ISNULL(TRY_CAST([Element] AS INTEGER),0) > 0       

        INSERT INTO @quarter_list ([quarter_no], [quarter_display])
        VALUES (1, 'Q1'), (2, 'Q2'), (3, 'Q3'), (4, 'Q4')

        INSERT INTO @probability_list ([probability_no], [probability_sort], [probability_display])
        VALUES (0, 'EEE', '0%'), (25, 'DDD', '25%'), (50, 'CCC', '50%'), (75, 'BBB', '75%'), (100, 'AAA', '100%')
        

    /*  Get Plan Raw Data  */

        INSERT INTO [#plan_data] ([plan_no], [worker_customer_no], [worker_name], [worker_initials], [worker_inactive], 
                                  [complete_by_dt], [complete_by_quarter], [ask_amount], [goal_amount], [contribution_amount], 
                                  [plan_status_id], [plan_status], [plan_probability])
        SELECT pln.[plan_no],
               ISNULL(xpp.[customer_no],0),
               ISNULL(wrk.[worker_name],''),
               ISNULL(wrk.[worker_initials],''),
               ISNULL(wrk.[inactive],'Y'),
               pln.[complete_by_dt],
               'Q' + CAST([dbo].[LF_GetFiscalQuarter](pln.[complete_by_dt]) AS CHAR(1)),
               ISNULL(pln.[ask_amt],0.0),
               ISNULL(pln.[goal_amt],0.0),
               ISNULL(pln.[cont_amt],0.0),
               pln.[status],
               sta.[description],
               pln.[probability]
        FROM [dbo].[T_PLAN] AS pln
             LEFT OUTER JOIN [dbo].[TX_CUST_PLAN] AS xpp ON xpp.[plan_no] = pln.[plan_no] AND xpp.[role_no] = 1
             INNER JOIN @worker_list AS lis ON lis.[worker_no] = ISNULL(xpp.[customer_no],0)
             LEFT OUTER JOIN [dbo].[LV_CAMPAIGN_ACTIVITY_WORKERS] AS wrk ON wrk.[worker_customer_no] = ISNULL(xpp.[customer_no],0)
             LEFT OUTER JOIN [dbo].[TR_PLAN_STATUS] AS sta ON sta.[id] = pln.[status]
        WHERE pln.[complete_by_dt] BETWEEN @fiscal_start_dt AND @fiscal_end_dt
          AND pln.[type] = 7  --Pipeline
          AND pln.[status] IN (4, 22, 23)
          AND pln.[ask_amt] > 0;

    /*  Update Probability  */

        UPDATE [#plan_data]
        SET [plan_probability_display] = CASE WHEN [plan_probability] BETWEEN 0.76 AND 1.00 THEN '100%'
                                              WHEN [plan_probability] BETWEEN 0.51 AND 0.75 THEN '75%'
                                              WHEN [plan_probability] BETWEEN 0.26 AND 0.50 THEN '50%'
                                              WHEN [plan_probability] BETWEEN 0.00 AND 0.25 THEN '25%' END,
            [plan_probability_sort] = CASE WHEN [plan_probability] BETWEEN 0.76 AND 1.00 THEN 'AAA'
                                           WHEN [plan_probability] BETWEEN 0.51 AND 0.75 THEN 'BBB'
                                           WHEN [plan_probability] BETWEEN 0.26 AND 0.50 THEN 'CCC'
                                           WHEN [plan_probability] BETWEEN 0.00 AND 0.25 THEN 'DDD' END


--SELECT DISTINCT [worker_customer_no] FROM [#plan_data]
--WHERE [worker_customer_no] NOT IN (SELECT worker_no FROM @worker_list)
--SELECT * FROM [#plan_data] WHERE [worker_customer_no] = 0
--SELECT * FROM [T_CUSTOMER] WHERE [customer_no]   IN (701702,7126,747848)
--SELECT * FROM [dbo].[LV_CAMPAIGN_ACTIVITY_WORKERS] WHERE [worker_customer_no]  IN (701702,7126,747848)
--GOTO DONE

--SELECT [plan_status], SUM([ask_amount]) FROM [#plan_data] WHERE [Plan_probability_display] = '75%' GROUP BY [plan_status]
--SELECT SUM([ask_amount]) FROM [#plan_data] WHERE [Plan_probability_display] = '75%'
--GOTO DONE

    /*  Seed the final table with one zeroed record for each combination
        This way if a particular combination does not exist, it displays a zero rather than a blank  */

        INSERT INTO [#final_table] ([plan_probability_sort], [plan_probability_display], [plan_status], 
                                    [complete_by_quarter], [plan_count], [ask_amount], [goal_amount])
        SELECT pro.[probability_sort], 
               pro.[probability_display], 
               sta.[description], 
               qua.[quarter_display], 
               0, 
               0.0, 
               0.0 
        FROM [dbo].[TR_PLAN_STATUS] AS sta
             CROSS JOIN @probability_list AS pro
             CROSS JOIN @quarter_list AS qua
        WHERE sta.[id]  IN (4, 22, 23)
                                                        
    /*  If there is no zero probability data, remove the zero probability seed records  */

    IF NOT EXISTS (SELECT * FROM [#plan_data] WHERE [plan_probability_display] = '0%')
        DELETE FROM [#final_table]
        WHERE [plan_probability_display] = '0%'

    /*  Insert aggregated information into the final table  */

        INSERT INTO [#final_table] ([plan_probability_sort], [plan_probability_display], [plan_status], 
                                    [complete_by_quarter], [plan_count], [ask_amount], [goal_amount])
        SELECT [plan_probability_sort],
               [plan_probability_display],
               [plan_status],
               [complete_by_quarter],
               COUNT(DISTINCT [plan_no]) AS [plan_count],
               SUM([ask_amount]) AS [ask_amount],
               SUM([goal_amount]) AS [goal_amount]
        FROM [#plan_data] 
        GROUP BY [plan_probability_sort], [plan_probability_display], [plan_status], [complete_by_quarter]

    FINISHED:
    
        SELECT [plan_probability_sort],
               [plan_probability_display],
               [plan_status],
               [complete_by_quarter],
               sum([plan_count]) AS [plan_count],
               SUM([ask_amount]) AS [ask_amount],
               SUM([goal_amount]) AS [goal_amount]
        FROM [#final_table] 
        GROUP BY [plan_probability_sort], [plan_probability_display], [plan_status], [complete_by_quarter]
        ORDER BY [plan_probability_sort], [plan_status], [complete_by_quarter]

    DONE:

        IF OBJECT_ID('tempdb..#plan_data') IS NOT NULL DROP TABLE #plan_data

END
GO

EXECUTE [dbo].[LRP_ADV_CAMPAIGN_ACTIVITY_SOL_PIPE_QUAR_SUMMARY] @fiscal_year = NULL, @workers_str = NULL


/*
plan_status                                        
-------------------------------------------------- ---------------------------------------
03-Cultivation                                     71,108,550.00  71,299,404.00    190,854.00
04-Pre-Ask                                         2,722,500.00
05-Solicitation                                    7,941,970.00    7,954,350


---------------------------------------
81773020.00
*/


--SELECT pln.[plan_no], [pln].[customer_no], wrk.[customer_no]
--FROM T_PLAN AS pln
--     LEFT OUTER JOIN [dbo].[TX_CUST_PLAN] AS wrk ON wrk.[plan_no] = pln.[plan_no]
--WHERE CAST([complete_by_dt] AS DATE) BETWEEN '7-1-2020' AND '6-30-2021'
--  AND wrk.[customer_no] NOT IN (SELECT [worker_customer_no] FROM [dbo].[LV_CAMPAIGN_ACTIVITY_WORKERS] WHERE [inactive] = 'N')
--  AND pln.[type] = 7

