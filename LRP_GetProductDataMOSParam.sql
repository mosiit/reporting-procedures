USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LRP_GetProductDataMOSParam]    Script Date: 10/9/2020 2:13:21 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



ALTER PROCEDURE [dbo].[LRP_GetProductDataMOSParam]
(@NumOfDays INT = 0, -- 0 for today's schedule
 @MOS VARCHAR(30) -- expect a string
)
AS
SET NOCOUNT ON;

CREATE TABLE #mos (
	mos_id INT
)

IF ISNULL(@mos, '') = '' OR @mos = '0'
	INSERT INTO #mos
		SELECT id FROM TR_MOS
ELSE
	INSERT INTO #mos
		SELECT CONVERT(INT,Element) FROM dbo.FT_SPLIT_LIST (@MOS,',')

--SELECT * FROM #mos

DECLARE @prods TABLE
(
    EventDate CHAR(10),
    EventTime VARCHAR(8),
    EventEnd VARCHAR(8),
    Venue VARCHAR(100),
    Location VARCHAR(100),
    SoftCapacity INT,
    Instock INT,
    EventNumber INT,
	ProdSeasonNo INT,
    MemberOnly INT,
    SchoolOnly INT,
    EventTitleLong VARCHAR(100),
    ItemLength INT,
	MOS INT,
	PerformanceNo INT,
	EventLink VARCHAR(300),
	TicketedEvent VARCHAR(100)
        UNIQUE CLUSTERED (
                             EventDate,
                             Venue,
                             MemberOnly, 
							 EventNumber,
							 EventTime,
							 MOS,
							 PerformanceNo,
							 ProdSeasonNo
                         )
);

INSERT INTO @prods
	SELECT DISTINCT
		   CONVERT(VARCHAR(10), perf.performance_dt, 111) AS EventDate,
		   perf.performance_time AS EventTime,
		   perf.performance_end_time AS EventEnd,
		   CASE perf.title_name
			   WHEN 'Hayden Planetarium' THEN 'Charles Hayden Planetarium'
			   ELSE perf.title_name
		   END AS Venue,
		   CASE perf.title_name
			   WHEN 'Drop-In Activities' THEN perf.performance_location
			   WHEN 'Live Presentations' THEN perf.performance_location
			   WHEN 'Hayden Planetarium' THEN 'Planetarium'
			   ELSE perf.title_name
		   END AS location,
		   [dbo].[LF_GetCapacity](perf.[performance_no], perf.[performance_zone], 'Capacity') AS SoftCapacity,
		   [dbo].[LF_GetCapacity](perf.[performance_no], perf.[performance_zone], 'Available') AS Instock,
		   perf.production_no AS EventNumber,
		   perf.production_season_no AS PSID,
		   CASE perf.performance_type_name WHEN 'Member Event' THEN 1 ELSE 0 END AS MemberOnly,
		   CASE perf.performance_type_name WHEN 'School Only' THEN 1 ELSE 0 END AS SchoolOnly,
		   --perf.production_name_long AS EventTitleLong,
		   --perf.performance_name_long, 
		   --perf.production_season_long_title, 
		   --perf.production_name_long,
		   --COALESCE(perf.performance_name_long, perf.production_season_long_title, perf.production_name_long) AS EventTitleLong,
		   COALESCE(ttl_perf_long.value, ttl_seas_long.value, ttl_prod_long.value) AS EventTitleLong,
		   DATEDIFF(mi, CAST(perf.performance_time AS DATETIME), CAST(perf.performance_end_time AS DATETIME)) AS ItemLength,
		   g.MOS,
		   perf.performance_no,
		   ISNULL(ttl_link.value, ''),
		   -- 2020-10-09, H. Sheridan - Add 'Ticketed' to the feed
		   ISNULL(COALESCE(ttl_perf_tick.value, ttl_seas_tick.value, ttl_prod_tick.value), 'N') AS TicketedEvent
	FROM dbo.LV_PRODUCTION_ELEMENTS_PERFORMANCE perf
	-- Reference WP_PERF_DETAIL
	JOIN [dbo].tx_perf_pkg_mos g WITH (NOLOCK) 
	ON		perf.performance_no = g.perf_no 
	AND		g.MOS IN (SELECT mos_id FROM #mos)
	AND		perf.performance_dt >= g.start_dt 
	AND		perf.performance_dt <= g.end_dt
	--AND		r.avail_sale_ind = 'Y'
	--LEFT OUTER JOIN dbo.TX_INV_CONTENT cont_eventlink
	--ON	cont_eventlink.inv_no = g.perf_no
	--AND cont_eventlink.id = 41
	LEFT OUTER JOIN [dbo].[TX_INV_CONTENT] AS ttl_perf_long ON ttl_perf_long.[inv_no] = perf.performance_no AND ttl_perf_long.[content_type] = (SELECT [id] FROM TR_INV_CONTENT WHERE [description] = 'Long Title')
	LEFT OUTER JOIN [dbo].[TX_INV_CONTENT] AS ttl_prod_long ON ttl_prod_long.[inv_no] = perf.production_no AND ttl_prod_long.[content_type] = (SELECT [id] FROM TR_INV_CONTENT WHERE [description] = 'Long Title')
	LEFT OUTER JOIN [dbo].[TX_INV_CONTENT] AS ttl_seas_long ON ttl_seas_long.[inv_no] = perf.production_season_no AND ttl_seas_long.[content_type] = (SELECT [id] FROM TR_INV_CONTENT WHERE [description] = 'Long Title')
	LEFT OUTER JOIN [dbo].[TX_INV_CONTENT] AS ttl_link ON ttl_link.[inv_no] = perf.performance_no AND ttl_link.[content_type] = (SELECT [id] FROM TR_INV_CONTENT WHERE [description] = 'Event Link')
	-- 2020-10-09, H. Sheridan - Add 'Ticketed' to the feed
	LEFT OUTER JOIN [dbo].[TX_INV_CONTENT] AS ttl_perf_tick ON ttl_perf_tick.[inv_no] = perf.performance_no AND ttl_perf_tick.[content_type] = (SELECT [id] FROM TR_INV_CONTENT WHERE [description] = 'Ticketed')
	LEFT OUTER JOIN [dbo].[TX_INV_CONTENT] AS ttl_prod_tick ON ttl_prod_tick.[inv_no] = perf.production_no AND ttl_prod_tick.[content_type] = (SELECT [id] FROM TR_INV_CONTENT WHERE [description] = 'Ticketed')
	LEFT OUTER JOIN [dbo].[TX_INV_CONTENT] AS ttl_seas_tick ON ttl_seas_tick.[inv_no] = perf.production_season_no AND ttl_seas_tick.[content_type] = (SELECT [id] FROM TR_INV_CONTENT WHERE [description] = 'Ticketed')
	WHERE DATEDIFF(d, GETDATE(), performance_dt) >= 0
		  AND DATEDIFF(d, GETDATE(), performance_dt) <= @NumOfDays
		  AND perf.performance_name_long <> ''
		  AND
		  (
			  perf.performance_type_name <> 'Buyout'
			  AND perf.production_name NOT LIKE '%Buyout%'
		  )
		  AND perf.is_generic_title = 'N';

--SELECT 'debug', 
--		EventDate,
--        EventTime,
--        EventEnd,
--        Venue,
--        Location,
--        SoftCapacity,
--        Instock,
--        EventNumber,
--        ProdSeasonNo,
--        MemberOnly,
--        SchoolOnly,
--        EventTitleLong,
--        ItemLength,
--        MOS,
--        PerformanceNo,
--		EventLink 
--FROM	@prods

--SELECT DISTINCT 'dates' AS [results], Venue, EventDate
SELECT DISTINCT 'dates' AS [results], EventDate
FROM	@prods
ORDER BY EventDate;

SELECT DISTINCT 'venues' AS [results], Venue, EventDate, MemberOnly
FROM	@prods
ORDER BY EventDate, Venue, MemberOnly;

SELECT DISTINCT 'shows' AS [results], x.Venue, x.EventDate, x.MemberOnly, x.EventNumber, x.ItemLength, x.Location, x.EventTitleLong, x.ProdSeasonNo, x.PerformanceNo, x.EventLink, x.TicketedEvent --, apd.EventTitle, apd.EventURL
FROM	@prods x
--LEFT JOIN dbo.LT_ACQUIA_PRODUCTION_DATA apd
--ON		x.EventNumber = apd.EventNumber
ORDER BY x.EventDate, x.Venue, x.MemberOnly, x.EventNumber;

SELECT DISTINCT 'times' AS [results], Venue, EventDate, MemberOnly, EventNumber, EventEnd, Instock, SchoolOnly, EventTime
FROM	@prods
ORDER BY EventDate, Venue, MemberOnly, EventNumber;









GO


