USE [impresario]
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_FULL_MEMBERSHIP_PROGRESS_REPORT]') AND type in (N'P', N'PC'))
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_FULL_MEMBERSHIP_PROGRESS_REPORT] AS' 
GO

EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LRP_FULL_MEMBERSHIP_PROGRESS_REPORT] TO [impusers], [tessitura_app]'
GO

ALTER PROCEDURE [dbo].[LRP_FULL_MEMBERSHIP_PROGRESS_REPORT]
        @month_FY_YTD_Calc VARCHAR(15) = NULL,
	    @requestDt DATETIME = NULL,
        @get_previous_year CHAR(1) = NULL,
        @memb_type_str VARCHAR(4000) = NULL
AS BEGIN

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    SET ANSI_WARNINGS OFF;
    SET NOCOUNT ON;

    /*  Procedure Variables  */

        DECLARE @last_year_request_dt DATETIME
        DECLARE @fiscal_year VARCHAR(4) = ''
        DECLARE @last_fiscal_year VARCHAR(4) = ''
        DECLARE @lastCampaignAbrev VARCHAR(30) = ''

        DECLARE @memb_types TABLE ([memb_type] VARCHAR(30))

        DECLARE @final_table TABLE   ([campaign_no] INT, 
                                      [campaignName] VARCHAR(30),
                                      [campaignAbbrev] VARCHAR(30), 
                                      [RevenueGoal] DECIMAL(18,2), 
                                      [CountGoal] INT, 
                                      [start_dt] DATETIME, 
                                      [end_dt] DATETIME, 
                                      [totalAmt] DECIMAL(18,2), 
                                      [totMembs] INT,
                                      [LYcampaign_no] INT, 
                                      [LYcampaignName] VARCHAR(30),
                                      [LYcampaignAbbrev] VARCHAR(30),
                                      [LYRevenueGoal] DECIMAL(18,2), 
                                      [LYCountGoal] INT, 
                                      [LYstart_dt] DATETIME, 
                                      [LYend_dt] DATETIME, 
                                      [LYtotalAmt] DECIMAL(18,2), 
                                      [LYtotMembs] INT,
                                      [campaign_sort] VARCHAR(3));

        DECLARE @final_table_LY TABLE   ([campaign_no] INT, 
                                         [campaignName] VARCHAR(30),
                                         [campaignAbbrev] VARCHAR(30), 
                                         [RevenueGoal] DECIMAL(18,2), 
                                         [CountGoal] INT, 
                                         [start_dt] DATETIME, 
                                         [end_dt] DATETIME, 
                                         [totalAmt] DECIMAL(18,2), 
                                         [totMembs] INT,
                                         [LYcampaign_no] INT, 
                                         [LYcampaignName] VARCHAR(30),
                                         [LYcampaignAbbrev] VARCHAR(30),
                                         [LYRevenueGoal] DECIMAL(18,2), 
                                         [LYCountGoal] INT, 
                                         [LYstart_dt] DATETIME, 
                                         [LYend_dt] DATETIME, 
                                         [LYtotalAmt] DECIMAL(18,2), 
                                         [LYtotMembs] INT,
                                         [campaign_sort] VARCHAR(3));
                                          
    /*  Check Parameters  */

        IF ISNULL(@month_FY_YTD_Calc, '') NOT IN ('Month', 'YTD') SELECT @month_FY_YTD_Calc = 'FY';
	    SELECT @requestDt = ISNULL(@requestDt, GETDATE());
        IF ISNULL(@get_previous_year, '') <> 'N' SELECT @get_previous_year = 'Y';
        
        IF ISNULL(@memb_type_str,'') = ''
            INSERT INTO @memb_types ([memb_type])
            VALUES ('Household'), ('Library'), ('Corporate')
        ELSE
            INSERT INTO @memb_types ([memb_type])
            SELECT [element] FROM [dbo].[FT_SPLIT_LIST](REPLACE(@memb_type_str,'"',''), ',')

        --Determine Fiscal Year based on Request Date
        SELECT @fiscal_year = CAST(MAX([fyear]) AS VARCHAR(4)) 
                              FROM [dbo].[TR_Batch_Period] 
                              WHERE @requestDt BETWEEN [start_dt] AND [end_dt];
    
    /*  If the previous year is requested, take care of that first. 
        That way the values can be moved to the Last Year (LY) columns 
        with a single update statement and no WHERE clause  */     
    IF @get_previous_year = 'Y' BEGIN
        
        --Previous year's request date is RequestDt - 1 Year
        SELECT @last_year_request_dt = DATEADD(YEAR,-1,@requestDt);

        --Determine Previous Fiscal Year based on Previous Request Date
        SELECT @last_fiscal_year = CAST(MAX([fyear]) AS VARCHAR(4)) 
                                   FROM [dbo].[TR_Batch_Period] 
                                   WHERE @last_year_request_dt BETWEEN [start_dt] AND [end_dt];

        --Execute [dbo].[LRP_MEMBERSHIP_PROGRESS_REPRORT] to get previous year's Household and Library Information (if requested)
        --Always ansert 'N' to @get_previous_year
        IF EXISTS (SELECT * FROM @memb_types WHERE [memb_type] = 'Household' OR [memb_type] = 'Library')
            INSERT INTO @final_table ([campaign_no],[campaignName],[campaignAbbrev],[RevenueGoal],[CountGoal],[start_dt],[end_dt],[totalAmt],[totMembs],
                                      [LYcampaign_no],[LYcampaignName],[LYcampaignAbbrev],[LYRevenueGoal],[LYCountGoal],[LYstart_dt],[LYend_dt],[LYtotalAmt],[LYtotMembs])
            EXECUTE [dbo].[LRP_MEMBERSHIP_PROGRESS_REPRORT] @month_FY_YTD_Calc = @month_FY_YTD_Calc, @requestDt = @last_year_request_dt, @get_previous_year = 'N';

        --Execute [dbo].[LRP_CORPORATE_MEMBERSHIP_PROGRESS_REPRORT] to get previous year's corporate information (if requested)
        --Always ansert 'N' to @get_previous_year
        IF EXISTS (SELECT * FROM @memb_types WHERE [memb_type] = 'Corporate')
            INSERT INTO @final_table ([campaign_no],[campaignName],[campaignAbbrev],[RevenueGoal],[CountGoal],[start_dt],[end_dt],[totalAmt],[totMembs],
                                      [LYcampaign_no],[LYcampaignName],[LYcampaignAbbrev],[LYRevenueGoal],[LYCountGoal],[LYstart_dt],[LYend_dt],[LYtotalAmt],[LYtotMembs])
            EXECUTE [dbo].[LRP_CORPORATE_MEMBERSHIP_PROGRESS_REPRORT] @month_FY_YTD_Calc = @month_FY_YTD_Calc, @requestDt = @last_year_request_dt, @get_previous_year = 'N' 

        --Move all values from the current data columns to the Last Year Data Columns
        UPDATE @final_table
        SET [LYcampaign_no] = [campaign_no],
            [LYcampaignName] = [campaignName],
            [LYcampaignAbbrev] = [campaignAbbrev],
            [LYRevenueGoal] = [RevenueGoal],
            [LYstart_dt] = [start_dt],
            [LYend_dt] = [end_dt],
            [LYtotalAmt] = [totalAmt],
            [LYtotMembs] = [totMembs]

    END

    --Execute [dbo].[LRP_MEMBERSHIP_PROGRESS_REPRORT] to get current year's Household and Library Information (if requested)
    --Always ansert 'N' to @get_previous_year
    IF EXISTS (SELECT * FROM @memb_types WHERE [memb_type] = 'Household' OR [memb_type] = 'Library')
        INSERT INTO @final_table ([campaign_no],[campaignName],[campaignAbbrev],[RevenueGoal],[CountGoal],[start_dt],[end_dt],[totalAmt],[totMembs],
                                  [LYcampaign_no],[LYcampaignName],[LYcampaignAbbrev],[LYRevenueGoal],[LYCountGoal],[LYstart_dt],[LYend_dt],[LYtotalAmt],[LYtotMembs])
        EXECUTE [dbo].[LRP_MEMBERSHIP_PROGRESS_REPRORT] @month_FY_YTD_Calc = @month_FY_YTD_Calc, @requestDt = @requestDt, @get_previous_year = 'N' 

    --Execute [dbo].[LRP_CORPORATE_MEMBERSHIP_PROGRESS_REPRORT] to get Current year's corporate information (if requested)
    --Always ansert 'N' to @get_previous_year
    IF EXISTS (SELECT * FROM @memb_types WHERE [memb_type] = 'Corporate')
        INSERT INTO @final_table ([campaign_no],[campaignName],[campaignAbbrev],[RevenueGoal],[CountGoal],[start_dt],[end_dt],[totalAmt],[totMembs],
                                  [LYcampaign_no],[LYcampaignName],[LYcampaignAbbrev],[LYRevenueGoal],[LYCountGoal],[LYstart_dt],[LYend_dt],[LYtotalAmt],[LYtotMembs])
        EXECUTE [dbo].[LRP_CORPORATE_MEMBERSHIP_PROGRESS_REPRORT] @month_FY_YTD_Calc = @month_FY_YTD_Calc, @requestDt = @requestDt, @get_previous_year = 'N' 
    
    --Since Household and Library are tied together, a second check needs to be done to eliminate one if only the other is requested
    IF NOT EXISTS (SELECT * FROM @memb_types WHERE [memb_type] = 'Household')
        DELETE FROM @final_table WHERE [campaignAbbrev] LIKE 'Household%'

    IF NOT EXISTS (SELECT * FROM @memb_types WHERE [memb_type] = 'Library')
        DELETE FROM @final_table WHERE [campaignAbbrev] LIKE 'Library%'


    UPDATE @final_table
    SET [campaign_sort] = CASE WHEN [campaignAbbrev] LIKE 'Household%' THEN 'BBB'
                               WHEN [campaignAbbrev] LIKE 'Library%' THEN 'AAA'
                               ELSE 'CCC' END;

    UPDATE @final_table
    SET [campaignAbbrev] = [campaignAbbrev] + RIGHT(@fiscal_year,2),
        [LYcampaignAbbrev] = [LYcampaignAbbrev] + RIGHT([LYcampaignName],2);

    FINISHED:

        /*  Pull final dataset, marring current and previous fiscal year's information to the a single row per campaign  */

            SELECT TY.[campaign_no],
                   TY.[campaignName],
                   TY.[campaignAbbrev],
                   TY.[RevenueGoal],
                   TY.[CountGoal],
                   TY.[start_dt],
                   TY.[end_dt],
                   TY.[totalAmt],
                   TY.[totMembs],
                   LY.[campaign_no] AS [LYcampaign_no],
                   LY.[campaignName] AS [LYcampaignName],
                   LY.[campaignAbbrev] AS [LYcampaignAbbrev],
                   LY.[RevenueGoal] AS [LYRevenueGoal],
                   LY.[CountGoal] AS [LYCountGoal],
                   LY.[start_dt] AS [LYstart_dt],
                   LY.[end_dt] AS [LYend_dt],
                   LY.[totalAmt] AS [LYtotalAmt],
                   LY.[totMembs] AS [LYtotMembs],
                   TY.[campaign_sort]
            FROM @final_table AS TY
                 LEFT OUTER JOIN @final_table AS LY ON ISNULL(LY.[campaignAbbrev],'') = TY.[campaignAbbrev] and ISNULL(LY.[LYcampaign_no],0) <> 0
            WHERE ISNULL(TY.[LYcampaign_no],0) = 0
            ORDER BY TY.[campaign_sort]
          
    DONE:

END
GO

    EXECUTE [dbo].[LRP_FULL_MEMBERSHIP_PROGRESS_REPORT]
            @month_FY_YTD_Calc = 'YTD',
	        @requestDt = '6-30-2020',
            @get_previous_year = 'Y',
            @memb_type_str = 'Household,"Library",Corporate'



    --EXECUTE [dbo].[LRP_MEMBERSHIP_PROGRESS_REPRORT]
    --        @month_FY_YTD_Calc = 'YTD',
	   --     @requestDt = '6-30-2020',
    --        @get_previous_year = 'N'