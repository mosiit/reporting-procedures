USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LRP_ALL_STEPS_BY_DATE]    Script Date: 5/17/2021 1:11:23 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[LRP_ALL_STEPS_BY_DATE]
(
	@start_dt DATETIME = NULL,
	@end_dt DATETIME = NULL,
	@completedSteps VARCHAR(3) = 'All', --Yes,No,All
	@step_type VARCHAR(MAX) = NULL, 
	@worker_str VARCHAR(MAX) = NULL, 
	@list_no INT = NULL
)
AS

--EXEC [dbo].[LRP_ALL_STEPS_BY_DATE]
--	@start_dt = '11/1/2018',
--	@end_dt = '3/26/2019',
--	@completedSteps = 'Yes', --Yes,No,All
--	@step_type = NULL, 
--	@worker_str = NULL, 
--	@list_no = NULL

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

--start_dt and/or end_dt should not be null 
IF @start_dt IS NULL OR @end_dt IS NULL 
RETURN 

DECLARE @stepType TABLE (id INT)

IF @step_type IS NOT NULL
	INSERT INTO @stepType
	SELECT CONVERT(INT,Element) 
	FROM dbo.FT_SPLIT_LIST (@step_type,',')
ELSE 
	INSERT INTO @stepType
	SELECT id
	FROM dbo.TR_STEP_TYPE 
	WHERE id NOT IN (-1, -2) --Status Change, Campaign Change
	-- Angela P wants to include the inactive step types! 

-- TABLE TO HOLD WORKER PARAMETERS
DECLARE	@workerTbl TABLE (workerID INT NOT NULL)

IF ISNULL(@worker_str,'') <> ''
	INSERT INTO @workerTbl (workerID)
	SELECT Element FROM dbo.FT_SPLIT_LIST(@worker_str,',')

SELECT 
	p.customer_no, 
	dn.display_name_short AS customer_name,
	dn.sort_name, 
	p.plan_no, 
	pdn.display_name plan_display_name, 
	s.step_no, 
	s.step_dt, 
	s.completed_on_dt, 
	stype.description AS step_type,
	-- H. Sheridan, 20200517:  SCTASK0002132 - Add step description to MOS: All Steps By Date
	s.description AS step_description,
	s.notes, 
	adn.display_name_short assoc_display_name_short, 
	adn.sort_name assoc_sort_name, 
	wdn.customer_no worker_customer_no, 
	wdn.display_name_short worker_display_name_short, 
	wdn.sort_name worker_sort_name
FROM [dbo].T_STEP s
	INNER JOIN T_PLAN p ON p.plan_no = s.plan_no
	INNER JOIN [dbo].FT_PLAN_DISPLAY_NAME() pdn 
		ON p.plan_no = pdn.plan_no
	INNER JOIN @stepType ST
		ON ST.id = s.step_type
	INNER JOIN dbo.TR_STEP_TYPE stype
		ON stype.id = ST.id
	INNER JOIN [dbo].FT_CONSTITUENT_DISPLAY_NAME() dn 
		ON p.customer_no = dn.customer_no
	LEFT OUTER JOIN [dbo].FT_CONSTITUENT_DISPLAY_NAME() adn 
		ON s.associate_no = adn.customer_no
	LEFT OUTER JOIN [dbo].FT_CONSTITUENT_DISPLAY_NAME() wdn 
		ON s.worker_customer_no = wdn.customer_no
WHERE CAST(s.step_dt AS DATE) BETWEEN @start_dt AND @end_dt
	AND (@list_no IS NULL OR p.customer_no IN (SELECT customer_no FROM [dbo].T_LIST_CONTENTS WHERE list_no = @list_no))
	AND (
		(@completedSteps = 'Yes' AND s.completed_on_dt IS NOT NULL)
		OR (@completedSteps = 'No' AND s.completed_on_dt IS NULL)
		OR (@completedSteps ='All') 
	)
	AND (@worker_str IS NULL OR s.worker_customer_no IN (SELECT workerID FROM @workerTbl) ) -- Did not make this an inner join as not all workers would be pulled into the worker table


GO


