USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_PRICE_TYPE_BREAKDOWN]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LRP_PRICE_TYPE_BREAKDOWN]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LRP_PRICE_TYPE_BREAKDOWN]
        @report_start_dt DATETIME,
        @report_end_dt DATETIME,
        @detail_level VARCHAR(30),
        @include_secondary_venues CHAR(1),
        @title_str VARCHAR(4000)
AS BEGIN

    /*  Procedure Variables  */

        DECLARE @start_date CHAR(10) = '', 
                @end_date CHAR(10) = '',
                @specific_titles CHAR(1) = 'N'

        DECLARE @title_list TABLE ([title_no] INT)

        DECLARE @attend_table TABLE ([attendance_type] VARCHAR(30), 
                                     [title_name] VARCHAR(30), 
                                     [production_name] VARCHAR(30), 
                                     [production_name_long] VARCHAR(130), 
                                     [comp_code_name] VARCHAR(30), 
                                     [Price_type_name] VARCHAR(30), 
                                     [sort_group] CHAR(1), 
                                     [sale_total] INT, 
                                     [due_amt] DECIMAL(18,2), 
                                     [Report_msg] VARCHAR(100))

    /*  Check Parameters  */

        --Null dates default to yesterday.  A null detail level defaults to Production
        SELECT @report_start_dt = ISNULL(@report_start_dt,DATEDIFF(DAY,1,GETDATE()))
        SELECT @report_end_dt = ISNULL(@report_end_dt,DATEDIFF(DAY,1,GETDATE()))
        SELECT @detail_level = ISNULL(@detail_level,'Production')

        --Start Date becomes the string version of the earlier date (regardless of which variable it is)
        --End Date becomes the string version of the later date (regardless of which variable it is)
        IF CONVERT(CHAR(10),@report_start_dt,111) > CONVERT(CHAR(10),@report_end_dt,111)
            SELECT @start_date = CONVERT(CHAR(10),@report_end_dt,111), @end_date = CONVERT(CHAR(10),@report_start_dt,111)
        ELSE
            SELECT @start_date = CONVERT(CHAR(10),@report_start_dt,111), @end_date = CONVERT(CHAR(10),@report_end_dt,111)

        IF ISNULL(@title_str, '') <> '' BEGIN

            INSERT INTO @title_list ([title_no])
            SELECT CAST([element] AS INT) FROM [dbo].[FT_SPLIT_LIST](REPLACE(@title_str,'"',''),',')

            SELECT @specific_titles = 'Y'

        END ELSE BEGIN

            INSERT INTO @title_list ([title_no])
            SELECT [title_no] FROM [dbo].[T_TITLE]

            SELECT @specific_titles = 'N'

        END

    /*  Insert Attendance records into the table variable 
        Records come from the History table, meaning this report only works for past dates  */

        INSERT INTO @attend_table ([attendance_type],[title_name],[production_name],[production_name_long],
                                   [comp_code_name],[Price_type_name],[sort_group],[sale_total],[due_amt],[Report_msg])
        SELECT [attendance_type], 
               [title_name], 
               [production_name], 
               [production_name_long], 
               CASE WHEN [comp_code_name] = '' THEN 'Full Price' 
                    ELSE 'Discounted' END, 
               [Price_type_name], 
               '', 
               SUM([sale_total]), 
               SUM([due_amt]), 
               ''
        FROM [dbo].[LT_HISTORY_TICKET] AS his
             INNER JOIN @title_list AS ttl ON ttl.[title_no] = his.[title_no]
        WHERE [performance_date] BETWEEN @start_date AND @end_date AND [attendance_type] <> 'Gate B (Non-Ticketed)'
        GROUP BY [attendance_type], [title_name], [production_name], [production_name_long], 
                 CASE WHEN [comp_code_name] = '' THEN 'Full Price' ELSE 'Discounted' END,  [Price_type_name]

    /*  If primary venues only, delete all others */

        IF @specific_titles <> 'Y' AND @include_secondary_venues <> 'Y'
            DELETE FROM @attend_table 
            WHERE [title_name] NOT IN ('4-D Theater','Butterfly Garden','Exhibit Halls', 'Hayden Planetarium', 'Mugar Omni Theater', 'Thrill Ride 360')

    /*  If the detail level is 'Title' blank out the production fields so that they are all the same blank value when the date is aggregated  */

        IF @detail_level = 'Title' UPDATE @attend_table SET [production_name] = '', [production_name_long] = ''

    /*  Adult/Senior/Child and Nonmember Adult/Senior/Child are the same.  Combine them by adjusting the Price Type Name field  */

        UPDATE @attend_table SET [Price_type_name] = 'Adult' WHERE [Price_type_name] = 'Nonmember Adult'
        UPDATE @attend_table SET [Price_type_name] = 'Senior' WHERE [Price_type_name] = 'Nonmember Senior'
        UPDATE @attend_table SET [Price_type_name] = 'Child' WHERE [Price_type_name] = 'Nonmember Child'
    
    /*  Set the sort_group values so that the price types appear in the proper order on the report  */

        UPDATE @attend_table SET [sort_group] = 'A' WHERE [Price_type_name] LIKE '%Adult%' AND [price_type_name] NOT LIKE '%Voucher%' AND [comp_code_name] <> 'Discounted'
        UPDATE @attend_table SET [sort_group] = 'B' WHERE [Price_type_name] LIKE '%Senior%' AND [price_type_name] NOT LIKE '%Voucher%' AND [comp_code_name] <> 'Discounted'
        UPDATE @attend_table SET [sort_group] = 'C' WHERE [Price_type_name] LIKE '%Child%' AND [price_type_name] NOT LIKE '%Voucher%' AND [comp_code_name] <> 'Discounted'
        UPDATE @attend_table SET [sort_group] = 'D' WHERE [Price_type_name] = 'General Admission'
        UPDATE @attend_table SET [sort_group] = 'E' WHERE [sort_group] = '' and [Price_type_name] LIKE '%Member%' AND [price_type_name] NOT LIKE '%Membership%'
        UPDATE @attend_table SET [sort_group] = 'F' WHERE [sort_group] = '' and [Price_type_name] LIKE '%School%'
        UPDATE @attend_table SET [sort_group] = 'G' WHERE [sort_group] = '' and [Price_type_name] LIKE '%Tour Op%'
        UPDATE @attend_table SET [sort_group] = 'H' WHERE [sort_group] = '' and [comp_code_name] = 'Discounted'
        UPDATE @attend_table SET [sort_group] = 'I' WHERE [sort_group] = '' and [Price_type_name] = 'Course Participant'
        UPDATE @attend_table SET [sort_group] = 'J' WHERE [sort_group] = '' and ([Price_type_name] LIKE '%Internal%' OR [Price_type_name] LIKE '%External%')
        UPDATE @attend_table SET [sort_group] = 'K' WHERE [sort_group] = '' and [Price_type_name] LIKE '%Voucher%'
    
    /*  Sort group on anything else (that doesn't meet any of the above criteria) is set to Z  */

        UPDATE @attend_table SET [sort_group] = 'Z' WHERE [sort_group] = ''
        
    FINISHED:

        /*  Insert one blank record into the table if nothing is there  */

            IF not exists (SELECT * FROM @attend_table)
                INSERT INTO @attend_table VALUES ('', '', '', '', 'A', '', '', 0, 0.00, 'No data found for that date range')

        /*  Select final aggregated data set from the table */

            SELECT [attendance_type], [title_name], [production_name], [production_name_long], [sort_group], [comp_code_name], [Price_type_name], SUM([sale_total]) AS 'sale_total', SUM([due_amt]) AS 'due_amt'
            FROM @attend_table
            GROUP BY [attendance_type], [title_name], [production_name], [production_name_long], [sort_group], [comp_code_name], [Price_type_name]
            ORDER BY [attendance_type], [title_name], [production_name], [production_name_long], [sort_group], [comp_code_name], [Price_type_name]

END
GO

GRANT EXECUTE ON [dbo].[LRP_PRICE_TYPE_BREAKDOWN] TO impusers
GO

EXECUTE [dbo].[LRP_PRICE_TYPE_BREAKDOWN] @report_start_dt = '11-1-2020', @report_end_dt = '11-30-2020', @detail_level = 'Title', @include_secondary_venues = 'N', @title_str = '"161"'

--SELECT inv_no, description FROM [dbo].[T_INVENTORY] WHERE [type] = 'T' and [inv_no] not in (1148, 14375, 40674, 14376, 40679, 1395, 1398, 17825, 53191, 7179, 1126, 5541) ORDER BY [description]
--SELECT * FROM [dbo].[LV_SS_PRODUCTION_ELEMENTS_TITLE] WHERE [capacity_reporting_title] = 'N'


--SELECT * FROM [dbo].[gooesoft_request] WHERE [queue_status] = 's' AND [report_id] LIKE '%break%'





