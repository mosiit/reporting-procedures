USE impresario;
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_ATTEND_BUDGET_VS_ACTUAL_YTD]') AND type in (N'P', N'PC')) BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_ATTEND_BUDGET_VS_ACTUAL_YTD] AS';
    EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LRP_ATTEND_BUDGET_VS_ACTUAL_YTD] TO impusers';
END;
GO

ALTER PROCEDURE [dbo].[LRP_ATTEND_BUDGET_VS_ACTUAL_YTD]
        @week_ending DATETIME = NULL
AS BEGIN

    SET NOCOUNT ON;
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

    /*  Procedure Variables  */

        DECLARE @fiscal_year INT = 0;
        DECLARE @week_start_dt DATETIME; 
        DECLARE @week_end_dt DATETIME;
        DECLARE @year_start_dt DATETIME;
        DECLARE @year_end_dt DATETIME;

        DECLARE @prev_fiscal_year INT = 0;
        DECLARE @prev_week_start_dt DATETIME;
        DECLARE @prev_week_end_dt DATETIME; 
        DECLARE @prev_year_start_dt DATETIME;
        DECLARE @prev_year_end_dt DATETIME;
   
        DECLARE @gross_attend_no INT = 0, 
                @incremental_attend_no INT = -1;

        DECLARE @ytd_budget INT = 0, 
                @ytd_budget_base INT = 0, 
                @ytd_budget_incr INT = 0;

        DECLARE @ytd_actual INT = 0, 
                @ytd_actual_base INT = 0, 
                @ytd_actual_incr INT = 0;

        DECLARE @prev_ytd_actual INT = 0, 
                @prev_ytd_actual_base INT = 0, 
                @prev_ytd_actual_incr INT = 0;
        
        DECLARE @num_of_days INT = 0,
                @total_days INT = 365;

        DECLARE @ytd_data TABLE ([ytd_month] CHAR(7) NOT NULL DEFAULT (''), 
                                 [ytd_budget] INT NOT NULL DEFAULT (0), 
                                 [inc_budget] INT NOT NULL DEFAULT (0),
                                 [ytd_actual] INT NOT NULL DEFAULT (0))

        DECLARE @ytd_final TABLE ([ytd_month] CHAR(7) NOT NULL DEFAULT (''), 
                                  [ytd_budget] INT NOT NULL DEFAULT (0), 
                                  [inc_budget] INT NOT NULL DEFAULT (0),
                                  [ytd_actual] INT NOT NULL DEFAULT (0),
                                  [inc_actual] INT NOT NULL DEFAULT (0),
                                  [ytd_total] INT NOT NULL DEFAULT (0))
        
    /*  Check Parameters  */

        SELECT @week_ending = CAST(ISNULL(@week_ending,GETDATE()) AS DATE);

    /*  Get date values from LFT_ATTENDANCE_WEEK_DATES function  */
        
        SELECT @fiscal_year = [cur_fiscal_year], 
               @week_start_dt = [cur_week_start_dt], 
               @week_end_dt = [cur_week_end_dt],
               @year_start_dt = [cur_fiscal_start_dt], 
               @year_end_dt = [cur_fiscal_end_dt], 
               @prev_fiscal_year = [prv_fiscal], 
               @prev_week_start_dt = [prv_week_start_dt], 
               @prev_week_end_dt = [prv_week_end_dt], 
               @prev_year_start_dt = [prv_fiscal_start_dt],
               @prev_year_end_dt = [prv_fiscal_end_dt]
        FROM [dbo].[LFT_ATTENDANCE_WEEK_DATES] (@week_ending, 0, 'N');
        
    /*  Get number of days between start of fiscal year and run date of the report  */

        SELECT @num_of_days = (DATEDIFF(DAY,@year_start_dt, @week_end_dt) + 1)

        IF [dbo].[LFS_IsLeapYear] (@fiscal_year) = 'Y'
            SELECT @total_days += 1     --366 instead of 365

    /*  Current Fiscal Year  */

        --GET BASE BUDGET DATA FOR THIS YEAR
        INSERT INTO @ytd_data ([ytd_month], [ytd_budget])
        SELECT LEFT(CONVERT(CHAR(10),[perf_dt],111),7), SUM(ISNULL([budget],0))
        FROM [dbo].[LTR_ATTENDANCE_BUDGET_DATA]
        WHERE [perf_dt] BETWEEN @year_start_dt AND @week_end_dt
          AND [title_no] = @gross_attend_no
        GROUP BY LEFT(CONVERT(CHAR(10),[perf_dt],111),7)

        --GET INCREMENTAL BUDGET DATA FOR THIS YEAR
        INSERT INTO @ytd_data ([ytd_month], [inc_budget])
        SELECT LEFT(CONVERT(CHAR(10),[perf_dt],111),7), SUM(ISNULL([budget],0))
        FROM [dbo].[LTR_ATTENDANCE_BUDGET_DATA]
        WHERE [perf_dt] BETWEEN @year_start_dt AND @week_end_dt
          AND [title_no] = @incremental_attend_no
        GROUP BY LEFT(CONVERT(CHAR(10),[perf_dt],111),7)

        --GET ACTUAL ATTENDANCE DATA FOR THIS YEAR
        INSERT INTO @ytd_data ([ytd_month], [ytd_actual])
        SELECT LEFT([history_date],7),
               SUM(ISNULL([visit_count],0))
        FROM [dbo].[LT_HISTORY_VISIT_COUNT] 
        WHERE [history_dt] BETWEEN @year_start_dt AND @week_end_dt
        GROUP BY LEFT([history_date],7)

        --CREATE FINAL TABLE WITH AGGREGATED DATA FOR THIS YEAR
        INSERT INTO @ytd_final ([ytd_month], [ytd_budget], [inc_budget], [ytd_actual], [ytd_total])
        SELECT [ytd_month],
               SUM([ytd_budget]),
               SUM([inc_budget]),
               SUM([ytd_actual]),
               SUM([ytd_actual])
        FROM @ytd_data
        GROUP BY [ytd_month]

         --ADJUST ACTUAL ATTENDANCE NUMBERS ON MONTHS THIS YEAR WITH INCREMENTAL THAT MEET BASE BUDGET
         UPDATE @ytd_final
         SET [ytd_actual] = [ytd_budget],
             [inc_actual] = [ytd_actual] - [ytd_budget]
         WHERE [ytd_actual] > [ytd_budget]

        --SELECT BUDGET AND ACTUAL DATA FROM THIS YEAR INTO RETURN VARIABLES
        SELECT @ytd_budget_base = SUM([ytd_budget]),
               @ytd_budget_incr = SUM([inc_budget]),
               @ytd_budget = (SUM([ytd_budget]) + SUM([inc_budget])),
               @ytd_actual_base = SUM([ytd_actual]),
               @ytd_actual_incr = SUM([inc_actual]),
               @ytd_actual = SUM([ytd_total])
        FROM @ytd_final

    /*  Previous Fiscal Year  */

        --CLEAR TABLE VARIABLES
        DELETE FROM @ytd_final
        DELETE FROM @ytd_data

        --GET BASE BUDGET DATA FOR PREVIOUS YEAR
        INSERT INTO @ytd_data ([ytd_month], [ytd_budget])
        SELECT LEFT(CONVERT(CHAR(10),[perf_dt],111),7), SUM(ISNULL([budget],0))
        FROM [dbo].[LTR_ATTENDANCE_BUDGET_DATA]
        WHERE [perf_dt] BETWEEN @prev_year_start_dt AND @prev_week_end_dt
          AND [title_no] = @gross_attend_no
        GROUP BY LEFT(CONVERT(CHAR(10),[perf_dt],111),7)

        --GET INCREMENTAL BUDGET DATA FOR PREVIOUS YEAR
        INSERT INTO @ytd_data ([ytd_month], [inc_budget])
        SELECT LEFT(CONVERT(CHAR(10),[perf_dt],111),7), SUM(ISNULL([budget],0))
        FROM [dbo].[LTR_ATTENDANCE_BUDGET_DATA]
        WHERE [perf_dt] BETWEEN @prev_year_start_dt AND @prev_week_end_dt
          AND [title_no] = @incremental_attend_no
        GROUP BY LEFT(CONVERT(CHAR(10),[perf_dt],111),7)

        --GET ACTUAL ATTENDANCE DATA FOR PREVIOUS YEAR
        INSERT INTO @ytd_data ([ytd_month], [ytd_actual])
        SELECT LEFT([history_date],7),
               SUM(ISNULL([visit_count],0))
        FROM [dbo].[LT_HISTORY_VISIT_COUNT] 
        WHERE [history_dt] BETWEEN @prev_year_start_dt AND @prev_week_end_dt
        GROUP BY LEFT([history_date],7)

        --CREATE FINAL TABLE WITH AGGREGATED DATA FOR PREVIOUS YEAR
        INSERT INTO @ytd_final ([ytd_month], [ytd_budget], [inc_budget], [ytd_actual], [ytd_total])
         SELECT [ytd_month],
                SUM([ytd_budget]),
                SUM([inc_budget]),
                SUM([ytd_actual]),
                SUM([ytd_actual])
         FROM @ytd_data
         GROUP BY [ytd_month]
     
        --ADJUST ACTUAL ATTENDANCE NUMBERS ON MONTHS IN PREVIOUS YEAR WITH INCREMENTAL THAT MEET BASE BUDGET
        UPDATE @ytd_final
        SET [ytd_actual] = [ytd_budget],
             [inc_actual] = [ytd_actual] - [ytd_budget]
         WHERE [ytd_actual] > [ytd_budget]
           AND [inc_budget] > 0

        --SELECT BUDGET AND ACTUAL DATA FROM PREVIOUS YEAR INTO RETURN VARIABLES
        SELECT @prev_ytd_actual_base = SUM([ytd_actual]),
               @prev_ytd_actual_incr = SUM([inc_actual]),
               @prev_ytd_actual = SUM([ytd_total])
        FROM @ytd_final


    FINISHED:

        /*  Final data set is a single row with the values generated above  */

            SELECT @fiscal_year AS [fiscal_year],
                   CAST('Gross Attendance' AS VARCHAR(30)) AS [title_name],
                   @ytd_budget AS [year_to_date_budget],
                   @ytd_budget_base AS [year_to_date_budget_base],
                   @ytd_budget_incr AS [year_to_date_budget_incremental],
                   @ytd_actual AS [year_to_date_actual],
                   @ytd_actual_base AS [year_to_date_actual_base],
                   @ytd_actual_incr AS [year_to_date_actual_incremental],
                   @prev_ytd_actual AS [prev_year_to_date_actual],
                   @prev_ytd_actual_base AS [prev_year_to_date_base],
                   @prev_ytd_actual_incr AS [prev_year_to_date_incremental],
                   @num_of_days AS [number_of_days],
                   @total_days AS [total_days]

    DONE:

END
GO

--EXECUTE [dbo].[LRP_ATTEND_BUDGET_VS_ACTUAL_YTD] '11-3-2019'



--SELECT * FROM [dbo].[LTR_ATTENDANCE_BUDGET_DATA] WHERE [title_no] = -1

--SELECT * FROM [dbo].[LTR_ATTENDANCE_BUDGET_DATA] WHERE [title_no] = 0 ORDER BY perf_dt DESC

--DELETE FROM [dbo].[LTR_ATTENDANCE_BUDGET_DATA] WHERE [title_no] = -1

--DECLARE @pDate DATETIME = '7-1-2019', @inc_amt INT = 200
--INSERT INTO [dbo].[LTR_ATTENDANCE_BUDGET_DATA] ([perf_dt],[perf_day],[title_group],[title_no],[budget],[inactive])
--SELECT @pDate, DATENAME(WEEKDAY,@pDate), 'Incremental', -1, @inc_amt, 'N'



--DECLARE @pDate DATETIME = '11-3-2019', @bud_amt INT = 4000
--INSERT INTO [dbo].[LTR_ATTENDANCE_BUDGET_DATA] ([perf_dt],[perf_day],[title_group],[title_no],[budget],[inactive])
--SELECT @pDate, DATENAME(WEEKDAY,@pDate), 'Entire Museum', 0, @bud_amt, 'N'




 --SELECT SUM(ISNULL([budget],0))
    ----SELECT *
    --                         FROM [dbo].[LTR_ATTENDANCE_BUDGET_DATA]
    --                         WHERE [perf_dt] BETWEEN @year_start_dt AND @week_end_dt
    --                           AND [title_no] = @gross_attend_no


    --   SELECT SUM(ISNULL([visit_count],0))
    --   FROM [dbo].[LT_HISTORY_VISIT_COUNT] 
    --   WHERE [history_dt] BETWEEN @year_start_dt AND @week_end_dt


    
    --/*  Get the budget amount for the year to date  */
         
    --    SELECT @ytd_budget = SUM(ISNULL([budget],0))
    --                         FROM [dbo].[LTR_ATTENDANCE_BUDGET_DATA]
    --                         WHERE [perf_dt] BETWEEN @year_start_dt AND @week_end_dt
    --                           AND [title_no] = @gross_attend_no

    --/*  Get the actual gross attendance for the year to date  */

    --    SELECT @ytd_actual = SUM(ISNULL([visit_count],0))
    --                         FROM [dbo].[LT_HISTORY_VISIT_COUNT] 
    --                         WHERE [history_dt] BETWEEN @year_start_dt AND @week_end_dt

    --/*  Get the actual gross attendance for same time period last year  */

    --    SELECT @prev_ytd_actual = SUM(ISNULL([visit_count],0))
    --                              FROM [dbo].[LT_HISTORY_VISIT_COUNT] 
    --                              WHERE [history_dt] BETWEEN @prev_year_start_dt AND @prev_week_end_dt
