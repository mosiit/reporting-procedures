USE impresario
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LP_RPT_EXCEPTION_MOS_SINGLE_VENUE]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LP_RPT_EXCEPTION_MOS_SINGLE_VENUE]
GO

CREATE PROCEDURE [dbo].[LP_RPT_EXCEPTION_MOS_SINGLE_VENUE]
        @title_name varchar(30),
        @return_message varchar(100) = null OUTPUT
AS BEGIN

    DECLARE @today_date char(10), @title_no int, @exh_title_no int, @exLevel varchar(30), @exType varchar(30), @is_exh char(1)
    DECLARE @performance_no int, @production_name varchar(30), @performance_code varchar(10), @performance_dt datetime, @performance_type int, @performance_type_name varchar(30)
    DECLARE @perf_no int, @min_after int, @new_min_after int, @perf_dt datetime, @close_dt datetime, @close_time char(8)
    DECLARE @mos_start_dt datetime, @mos_end_dt datetime    

    DECLARE @perf_table table ([performance_no] int, [performance_code] varchar(10), [performance_dt] datetime, [performance_type] int, [performance_type_name] varchar(30), [performance_status_name] varchar(10), 
                               [production_name_long] varchar(100), [production_name] varchar(30))
    
    DECLARE @work_table table ([performance_no] int, [performance_code] varchar(10), [performance_dt] datetime, [performance_type] int, [performance_type_name] varchar(30), [performance_status_name] varchar(10), 
                               [production_name_long] varchar(100), [production_name] varchar(30), [MOS] int, [MOS_name] varchar(30), [start_dt] datetime, [start_dt_check] datetime NULL, 
                               [end_dt] datetime, [end_dt_check] datetime, [date_before] varchar(15), [months_before] int, [days_before] int, [minutes_before] int, [date_after] varchar(15), 
                               [months_after] int, [days_after] int, [minutes_after] int)

    DECLARE @exceptions_table_mos table ([exception_no] int IDENTITY (1,1), [exception_level] varchar(30), [exception_type] varchar(30), [exception_notes] varchar(255), [title_no] int, [title_name] varchar(30), 
                                         [production_name] varchar(30), [performance_no] int, [performance_dt] datetime, [performance_code] varchar(10), [performance_type_name] varchar(30), [mode_of_sale_no] int, 
                                         [mode_of_sale_name] varchar(30), [start_dt] datetime, [start_dt_should_be] datetime, [end_dt] datetime, [end_dt_should_be] datetime)

    SELECT @return_message = ''
    SELECT @today_date = convert(char(10),getdate(),111)

    SELECT @exh_title_no = [title_no] FROM [dbo].[LV_SS_PRODUCTION_ELEMENTS_TITLE] WHERE title_name = 'Exhibit Halls'
    SELECT @exh_title_no = IsNull(@exh_title_no, 0)

    SELECT @title_no = [title_no] FROM [dbo].[LV_SS_PRODUCTION_ELEMENTS_TITLE] WHERE title_name = @title_name
    SELECT @title_no = IsNull(@title_no, 0)
    IF @title_no <= 0 BEGIN
        SELECT @return_message = 'invalid title (' + @title_name + ').  Unable to determine id number.'
        GOTO DONE
    END

    IF @exh_title_no > 0 and @exh_title_no = @title_no SELECT @is_exh = 'Y' ELSE SELECT @is_exh = 'N'

    INSERT INTO @perf_table
    SELECT DISTINCT prf.[performance_no], prf.[performance_code], prf.[performance_dt], prf.[performance_type], prf.[performance_type_name], prf.[performance_status_name], prf.[production_name_long], prf.[production_name]
    FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] as prf
    WHERE prf.[performance_date] >= @today_date and prf.[title_no] = @title_no
        
    INSERT INTO @work_table
    SELECT DISTINCT prf.[performance_no], prf.[performance_code], prf.[performance_dt], prf.[performance_type], prf.[performance_type_name], prf.[performance_status_name], prf.[production_name_long], prf.[production_name],
           ppm.[MOS], mos.[description], ppm.[start_dt], null, ppm.[end_dt], null, map.[date_before], map.[months_before], map.[days_before], map.[minutes_before], map.[date_after], map.[months_after], map.[days_after], 
           map.[minutes_after]
    FROM @perf_table as prf
         INNER JOIN [dbo].[TX_PERF_PKG_MOS] as ppm ON ppm.[perf_no] = prf.[performance_no]
         INNER JOIN [dbo].[LTR_TITLE_MOS_MAP] as map on map.[tessitura_title] = @title_no and map.[mode_of_sale] = ppm.[MOS] and map.[performance_type] = prf.[performance_type]
         INNER JOIN [dbo].[TR_MOS] as mos ON mos.[id] = ppm.[MOS]

    UPDATE @work_table SET [date_before] = [dbo].[LF_GetFirstOfMonth] ([performance_dt],'') WHERE [date_before] = 'First of Month'
    
    UPDATE @work_table SET [date_after] = [dbo].[LF_GetLastOfMonth] ([performance_dt],'') WHERE [date_after] = 'Last of Month'

    /*  Convert Days To Minutes */
    UPDATE @work_table SET [minutes_before] = (([days_before] * 24) * 60) WHERE [days_before] <> 0
    UPDATE @work_table SET [minutes_after] = (([days_after] * 24) * 60) WHERE [days_after] <> 0
    
    UPDATE @work_table SET [start_dt_check] = [date_before] WHERE isdate([date_before]) = 1
    UPDATE @work_table SET [end_dt_check] = [date_after] WHERE isdate([date_after]) = 1

    /* If the stop time is based on what time the museum closes, then determine what time that is and adjust the number of minutes before
       accordingly to back-track from the performance time (which is usually 23:30:00).   */

    DECLARE CloseTimeCursor INSENSITIVE CURSOR FOR
    SELECT [performance_no], [performance_dt], [minutes_after] FROM @work_table WHERE [date_after] = 'Close Time'
    OPEN CloseTimeCursor
    BEGIN_CLOSE_TIME_LOOP:

        FETCH NEXT FROM CloseTimeCursor INTO @perf_no, @perf_dt, @min_after
        IF @@FETCH_STATUS = -1 GOTO END_CLOSE_TIME_LOOP

        EXECUTE [dbo].[LP_DetermineExhibitHallCloseTime] @performance_dt = @perf_dt, @suppress_selection = 'Y', @museum_close_time = @close_time OUTPUT

        IF isdate(@close_time) = 0 GOTO BEGIN_CLOSE_TIME_LOOP

        SELECT @close_dt = convert(char(10),@perf_dt,111) + ' ' + @close_time
        SELECT @new_min_after = (datediff(minute, @perf_dt, @close_dt) + @min_after)
        
        UPDATE @work_table SET [minutes_after] = @new_min_after WHERE [performance_no] = @perf_no
        
        GOTO BEGIN_CLOSE_TIME_LOOP

    END_CLOSE_TIME_LOOP:
    CLOSE CloseTimeCursor
    DEALLOCATE CloseTimeCursor

    UPDATE @work_table SET [start_dt_check] = dateadd(month,([months_before] * -1),[performance_dt]) WHERE [start_dt] is not null and [start_dt_check] is null and [months_before] <> 0
    UPDATE @work_table SET [end_dt_check] = dateadd(month,[months_after],[performance_dt]) WHERE [start_dt] is not null and [end_dt_check] is null and [months_after] <> 0

    UPDATE @work_table SET [start_dt_check] = dateadd(minute,([minutes_before] * -1),[performance_dt]) WHERE [start_dt] is not null and [start_dt_check] is null
    UPDATE @work_table SET [end_dt_check] = dateadd(minute,[minutes_after],[performance_dt]) WHERE [start_dt] is not null and [end_dt_check] is null
    
    SELECT @exLevel = '2. MOS Date Check'

    SELECT @exType = 'Incorrect Start and End Date'
    INSERT INTO @exceptions_table_mos
    SELECT @exLevel, @exType, [MOS_name] + ' mode of sale for ' + [production_name] + ' on ' + convert(char(10),[performance_dt],111) + ' has an incorrect START AND END date/time.',
           @title_no, @title_name, [production_name], [performance_no], [performance_dt], [performance_code], [performance_type_name], [MOS], [MOS_name], [start_dt], [start_dt_check], [end_dt], [end_dt_check]
    FROM @work_table 
    WHERE ([start_dt] <> [start_dt_check]) and ([end_dt] <> end_dt_check)


    SELECT @exType = 'Incorrect Start Date'
    INSERT INTO @exceptions_table_mos
    SELECT @exLevel, @exType, [MOS_name] + ' mode of sale for ' + [production_name] + ' on ' + convert(char(10),[performance_dt],111) + ' has an incorrect START date/time.',
           @title_no, @title_name, [production_name], [performance_no], [performance_dt], [performance_code], [performance_type_name], [MOS], [MOS_name], [start_dt], [start_dt_check], [end_dt], [end_dt_check]
    FROM @work_table 
    WHERE ([start_dt] <> [start_dt_check]) and ([end_dt] = [end_dt_check])

    SELECT @exType = 'Incorrect End Date'
    INSERT INTO @exceptions_table_mos
    SELECT @exLevel, @exType, [MOS_name] + ' mode of sale for ' + [production_name] + ' on ' + convert(char(10),[performance_dt],111) + ' has an incorrect END date/time.',
           @title_no, @title_name, [production_name], [performance_no], [performance_dt], [performance_code], [performance_type_name], [MOS], [MOS_name], [start_dt], [start_dt_check], [end_dt], [end_dt_check]
    FROM @work_table 
    WHERE ([start_dt] = [start_dt_check]) and ([end_dt] <> end_dt_check)

    SELECT @exLevel = '1. MOS Exists Check'

    DECLARE ModeOfSaleExistsCursor INSENSITIVE CURSOR FOR
    SELECT DISTINCT [performance_no], [production_name], [performance_code], [performance_dt], [performance_type], [start_dt], [end_dt] FROM @work_table
    OPEN ModeOfSaleExistsCursor 
    BEGIN_MODE_OF_SALE_EXISTS_LOOP:

        FETCH NEXT FROM ModeOfSaleExistsCursor INTO @performance_no, @production_name, @performance_code, @performance_dt, @performance_type, @mos_start_dt, @mos_end_dt
        IF @@FETCH_STATUS = -1 GOTO END_MODE_OF_SALE_EXISTS_LOOP

        SELECT @performance_type_name = [description] FROM [dbo].[TR_PERF_TYPE] WHERE [id] = @performance_type
        SELECT @performance_type_name = IsNull(@performance_type_name, convert(varchar(10),@performance_type))
        
        SELECT @exType = 'Invalid mode of sale'
        INSERT INTO @exceptions_table_mos
        SELECT  @exLevel, @exType, mos.[description] + ' mode of sale exists on the ' + convert(char(10), @performance_dt,111) + ' ' + @production_name + ' performance and should not.',
                @title_no, @title_name, @production_name, @performance_no, @performance_dt, @performance_code, @performance_type_name, mos.[id], mos.[description], @mos_start_dt, null, @mos_end_dt, null
        FROM TX_PERF_PKG_MOS as ppm
             INNER JOIN [TR_MOS] as mos on mos.[id] = ppm.[MOS]
        WHERE ppm.[perf_no] = @performance_no
              and ppm.[MOS] not in (SELECT [mode_of_sale] FROM LTR_TITLE_MOS_MAP WHERE tessitura_title = @title_no and [performance_type] = @performance_type)

        SELECT @exType = 'Missing mode of sale'
        INSERT INTO @exceptions_table_mos
        SELECT @exLevel, @exType, mos.[description] + ' mode of sale does NOT exists on the ' + convert(char(10), @performance_dt,111) + ' ' + @production_name + ' performance and it should.',
               @title_no, @title_name, @production_name, @performance_no, @performance_dt, @performance_code, @performance_type_name, mos.[id], mos.[description], null, null, null, null
        FROM LTR_TITLE_MOS_MAP as map
             INNER JOIN [TR_MOS] as mos on mos.[id] = map.[mode_of_sale]
        WHERE map.[tessitura_title] = @title_no and map.performance_type = @performance_type and map.[mode_of_sale] not in (SELECT [MOS] FROM TX_PERF_PKG_MOS WHERE perf_no = @performance_no)

        GOTO BEGIN_MODE_OF_SALE_EXISTS_LOOP

    END_MODE_OF_SALE_EXISTS_LOOP:
    CLOSE ModeOfSaleExistsCursor
    DEALLOCATE ModeOfSaleExistsCursor
    
    IF @return_message = '' SELECT @return_message = 'success'

    DONE:

        IF @return_message = '' SELECT @return_message = 'unknown error'

        SELECT [exception_level], [exception_type], [exception_notes], [title_no], [title_name], [production_name], [performance_no], [performance_dt], [performance_code], [performance_type_name], 
               [mode_of_sale_no], [mode_of_sale_name], [start_dt], [start_dt_should_be], [end_dt], [end_dt_should_be]
        FROM @exceptions_table_mos ORDER BY exception_level, exception_type

END
GO

GRANT EXECUTE ON [dbo].[LP_RPT_EXCEPTION_MOS_SINGLE_VENUE] TO impusers
GO


