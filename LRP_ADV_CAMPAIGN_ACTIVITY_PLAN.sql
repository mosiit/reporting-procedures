USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_ADV_CAMPAIGN_ACTIVITY_PLAN]') AND type IN (N'P', N'PC'))
BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_ADV_CAMPAIGN_ACTIVITY_PLAN] AS' 
    EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LRP_ADV_CAMPAIGN_ACTIVITY_PLAN] TO impusers'
END
GO

/*      LRP_ADV_CAMPAIGN_ACTIVITY_PLAN
        Retrueves fiscal year closed and booked (contributions) broken down by business unit, 
        as well as the plans expected to close by the end of the current fiscal year broken down by giving levels.  

        NOTE: Uses LV_CAMPAIGN_ACTIVITY_CONTRIBUTIONS view which is a view that was created using the
              same logic in the MOS Advancement Contributions report (LRP_MOS_ADVANCEMENT_CONTRIBUTION_DETAIL).

*/
ALTER PROCEDURE [dbo].[LRP_ADV_CAMPAIGN_ACTIVITY_PLAN]
         @fiscal_year INT = NULL
WITH RECOMPILE AS BEGIN

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

    /*  Check Fiscal Year Parameter - If none passed, set to current  */
        IF @fiscal_year IS NULL SELECT @fiscal_year = dbo.LF_GetFiscalYear(GETDATE())

    /*  Procedure Variables  */

        --Hard coded for the campaign (may change)
        DECLARE @overall_campaign_no INT = 1

        --Determine current fiscal year date information
        DECLARE @fiscal_start_dt DATETIME = [dbo].[LF_GetFiscalYearStartDt](@fiscal_year)
        DECLARE @fiscal_end_dt DATETIME = [dbo].[LF_GetFiscalYearEndDt](@fiscal_year)
        DECLARE @fiscal_end_dt_formatted VARCHAR(50) = FORMAT(@fiscal_end_dt,'MMMM d, yyyy','en-US')
        DECLARE @current_dt DATETIME = GETDATE()
        IF @current_dt > @fiscal_end_dt SELECT @current_dt = @fiscal_end_dt

        DECLARE @campaign_goal DECIMAL (18,2) = 0.00, @fiscal_year_goal DECIMAL(18,2) = 0.0

        --Status list table will hold a list of specific status numbers to include in the output
        DECLARE @status_list TABLE ([status_no] INT NOT NULL DEFAULT (0), 
                                    [status_name] VARCHAR(30) NOT NULL DEFAULT (''),
                                    [status_category] VARCHAR(30) NOT NULL DEFAULT (''))

    /*  Temporary Tables  */

        IF OBJECT_ID('tempdb..#plan_detail') IS NOT NULL DROP TABLE [#plan_detail]

        CREATE TABLE [#plan_detail] ([report_section_no] INT NOT NULL DEFAULT (0),
                                     [report_section] VARCHAR(50) NOT NULL DEFAULT(''),
                                     [Business_Unit] VARCHAR(50) NOT NULL DEFAULT (''),
                                     [business_unit_sort] VARCHAR(75) NOT NULL DEFAULT (''),
                                     [plan_level] VARCHAR(50) NOT NULL DEFAULT (''),
                                     [plan_level_order] INT NOT NULL DEFAULT (0),
                                     [plan_no] INT NOT NULL DEFAULT (0), 
                                     [campaign_no] INT NOT NULL DEFAULT (0), 
                                     [customer_no] INT NOT NULL DEFAULT (0),
                                     [customer_name] VARCHAR(160) NOT NULL DEFAULT (''),
                                     [customer_name_long] VARCHAR(230) NOT NULL DEFAULT (''),
                                     [customer_name_sort] VARCHAR(75) NOT NULL DEFAULT(''),
                                     [customer_city] VARCHAR(50) NOT NULL DEFAULT (''),
                                     [customer_state] VARCHAR(50) NOT NULL DEFAULT (''),
                                     [customer_postal_code] VARCHAR(50) NOT NULL DEFAULT (''),
                                     [customer_geoloc_no] INT NOT NULL DEFAULT (0),
                                     [customer_geoloc] VARCHAR(50) NOT NULL DEFAULT (''),
                                     [worker_no] INT NOT NULL DEFAULT (0),
                                     [worker_name] VARCHAR(75) NOT NULL DEFAULT (''),
                                     [worker_initials] VARCHAR(75) NOT NULL DEFAULT (''),
                                     [worker_inactive] CHAR(1) NOT NULL DEFAULT ('N'),
                                     [status] INT NOT NULL DEFAULT (0), 
                                     [status_name] VARCHAR(30) NOT NULL DEFAULT (''),
                                     [status_category] VARCHAR(30) NOT NULL DEFAULT (''),
                                     [type] INT NOT NULL DEFAULT (0), 
                                     [goal_amt] DECIMAL (18,2) NULL DEFAULT (0.0),
                                     [ask_amt] DECIMAL (18,2) NULL DEFAULT (0.0),
                                     [plan_yield] DECIMAL (18,4) NOT NULL DEFAULT (0.0),
                                     [campaign_goal] DECIMAL (18,4) NOT NULL DEFAULT (0.0),
                                     [fiscal_year_goal] DECIMAL (18,4) NOT NULL DEFAULT (0.0),
                                     [plan_complete_by_dt] DATETIME NULL,
                                     [cont_designation] INT NOT NULL DEFAULT (0),
                                     [designation] VARCHAR(30) NOT NULL DEFAULT(''))
 

    /*  Compile status list  */

        INSERT INTO @status_list ([status_no], [status_name], [status_category])
        SELECT [id], 
               [description],
               CASE WHEN [id] IN (22, 23) THEN 'Open'
                    WHEN [id] IN (1, 2, 4) THEN 'Planned'
                    ELSE 'No Category' END 
        FROM [dbo].[TR_PLAN_STATUS]
        WHERE inactive = 'N' 
          AND id NOT IN (11, 26, 29, 35, 37, 38, 39)      --Ommitted Statuses:  11=(none)                           26=06-Solicitation: Decline   
                                                          --                    29=11-Disqualify: Future Prospect   35=07-Stewardship       
                                                          --                    37 = 09-Disqualify                  38=13-No Response_Closed
                                                          --                    39=08-Involvement Only

    /*  When retrieving the various pieces of data, each is flagged as report section 1, 2, or 3  
        This allows them to be broken up more easily on the report without having to run multiple procedures  */

    /*  Get Contribution Data (report section 1)  */

        INSERT INTO [#plan_detail] ([report_section_no], [report_section], [Business_Unit], [business_unit_sort], [ask_amt], [goal_amt])
        SELECT 1,
               'Fiscal Year To Date Closed and Booked',
               [Business_Unit],
               [Business_Unit],
               SUM([contribution_amount]),
               SUM([contribution_amount]) 
        FROM [dbo].[LV_CAMPAIGN_ACTIVITY_CONTRIBUTIONS]
        WHERE [contribution_date] BETWEEN @fiscal_start_dt AND @current_dt 
          AND [campaign_category] <> 'Skip'
        GROUP BY business_unit

    /*  Get Plan Data (report section 2)  */

  
        INSERT INTO [#plan_detail] ([report_section_no], [report_section], [Business_Unit], [business_unit_sort], [plan_no], [campaign_no], [customer_no], 
                                    [customer_name], [customer_name_long], [customer_name_sort], [customer_city], [customer_state],
                                    [customer_postal_code], [customer_geoloc_no], [customer_geoloc], [worker_no], [worker_name], 
                                    [worker_initials], [worker_inactive], [status], [status_name], [status_category], [type], 
                                    [goal_amt], [ask_amt], [plan_complete_by_dt], [cont_designation], [designation], 
                                    [plan_level], [plan_level_order])
        SELECT  2,
               'Expected to Close By ' + @fiscal_end_dt_formatted,
               CASE WHEN pln.[ask_amt] < 25000 THEN 'Under $25,000.00'
                    WHEN pln.[ask_amt] BETWEEN 25000 AND 99999.00 THEN '$25,000.00 - $99,999.99'
                    WHEN pln.[ask_amt] BETWEEN 100000 AND 999999.00 THEN '$100,000.00 - $999,999.99'
                    ELSE '$1,000,000.00 +' END,
               CASE WHEN pln.[ask_amt] < 25000 THEN 'DDD'
                    WHEN pln.[ask_amt] BETWEEN 25000 AND 99999.00 THEN 'CCC'
                    WHEN pln.[ask_amt] BETWEEN 100000 AND 999999.00 THEN 'BBB'
               ELSE 'AAA' END,
               pln.[plan_no], 
               pln.[campaign_no], 
               pln.[customer_no], 
               cus.[display_name],
               cus.display_name_long,
               cus.[sort_name],
               ISNULL(adr.[city],''),
               ISNULL(adr.[state],''),
               ISNULL(adr.[postal_code],''),
               ISNULL(adr.[geo_area],0),
               ISNULL(geo.[description],'Unknown'),
               ISNULL(xpp.[customer_no],0),
               ISNULL(wrk.[worker_name],'Unknown'),
               ISNULL(wrk.[worker_initials],'???'),
               ISNULL(wrk.[inactive],'N'),
               pln.[status], 
               sta.[status_name],
               sta.[status_category],
               pln.[type], 
               --CASE WHEN ISNULL(pln.[goal_amt],0) = 0 THEN pln.[ask_amt] ELSE pln.[goal_amt] END,
               pln.goal_amt,
               ISNULL(pln.[ask_amt],0.0),
               pln.[complete_by_dt],
               pln.[cont_designation],
               dsg.[description],
               CASE WHEN pln.[ask_amt] < 25000 THEN 'Under $25,000.00'
                    WHEN pln.[ask_amt] BETWEEN 25000 AND 99999.00 THEN '$25,000.00 - $99,999.00'
                    WHEN pln.[ask_amt] BETWEEN 100000 AND 999999.00 THEN '$100,000.00 - $999,999.99'
                    ELSE '$1,000,000.00 +' END,
               CASE WHEN pln.[ask_amt] < 25000 THEN 4
                    WHEN pln.[ask_amt] BETWEEN 25000 AND 99999.00 THEN 5
                    WHEN pln.[ask_amt] BETWEEN 100000 AND 999999.00 THEN 2
                    ELSE 1 END               
      FROM [dbo].[T_PLAN] AS pln
           INNER JOIN @status_list AS sta ON sta.[status_no] = pln.[status] 
           INNER JOIN dbo.LTR_CAMPAIGN_BUSINESS_UNIT AS cbu ON cbu.campaign_no = pln.campaign_no
           LEFT OUTER JOIN [dbo].[FT_CONSTITUENT_DISPLAY_NAME]() AS cus ON cus.[customer_no] = pln.[customer_no]
           LEFT OUTER JOIN [dbo].[TX_CUST_PLAN] AS xpp ON xpp.[plan_no] = pln.[plan_no] AND xpp.[role_no] = 1
           LEFT OUTER JOIN [dbo].[LV_CAMPAIGN_ACTIVITY_WORKERS] AS wrk ON wrk.[worker_customer_no] = xpp.[customer_no] AND wrk.[inactive] = 'N'
           LEFT OUTER JOIN [dbo].[TR_CONT_DESIGNATION] AS dsg ON dsg.[id] = pln.[cont_designation]
           LEFT OUTER JOIN [dbo].[T_ADDRESS] AS adr ON adr.[customer_no] = pln.[customer_no] AND adr.[primary_ind] = 'Y'
           LEFT OUTER JOIN [dbo].[TR_GEO_AREA] AS geo ON geo.[id] = adr.[geo_area]
      WHERE pln.[complete_by_dt] BETWEEN @fiscal_start_dt AND @fiscal_end_dt
        AND ISNULL(pln.[ask_amt],0.0) > 0.0
        AND ISNULL(pln.[cont_amt],0.0) = 0.0
        AND pln.[type] = 7

    /*  Get overall campaign goal and fiscal year goal  */
      
       SELECT @campaign_goal = ISNULL(SUM(ct.[goal]),0)
       FROM [dbo].[LTR_CM_CATEGORY1] AS ct 
             INNER JOIN [dbo].[LTR_FUND_DETAIL] AS dt ON dt.cm_category1 = ct.[id] AND dt.[overall_cm_no] = @overall_campaign_no

        SELECT @fiscal_year_goal = ISNULL(SUM(ISNULL(bu.[Unrestricted_Goal],0) + ISNULL(bu.[Restricted_Goal],0)),0)
        FROM [dbo].[LTR_CAMPAIGN_BUSINESS_UNIT] AS bu
             INNER JOIN [dbo].[T_CAMPAIGN] AS cm ON cm.[campaign_no] = bu.[campaign_no] AND cm.[fyear] = @fiscal_year
       
    /*  Add final record to the temp table with the goals in it (report section 3)  */
        
        INSERT INTO [#plan_detail] ([report_section_no], [report_section], [campaign_goal], [fiscal_year_goal])
        VALUES (3, 'goals', @campaign_goal, @fiscal_year_goal)

    FINISHED:

        /*  Select final data set to return to the report  */
            

            --SELECT SUM([ask_amt]), SUM([goal_amt]) FROM [#plan_detail] WHERE report_section_no = 2
            --SELECT plan_no, COUNT(*) FROM [#plan_detail] WHERE report_section_no = 2 GROUP BY plan_no HAVING COUNT(*) > 1

            --SELECT plan_no, COUNT(*) FROM [#plan_detail] WHERE report_section_no = 2 GROUP BY plan_no HAVING COUNT(*) > 1
    
            SELECT [report_section_no],
                   [report_section],
                   [Business_Unit],
                   [business_unit_sort],
                   [plan_level],
                   [plan_level_order],
                   [plan_no], 
                   [campaign_no], 
                   [customer_no], 
                   [customer_name],
                   [customer_name_long],
                   [customer_name_sort],
                   [customer_city],
                   [customer_state],
                   [customer_postal_code],
                   [customer_geoloc_no],
                   [customer_geoloc],
                   [worker_no],
                   [worker_name],
                   [worker_initials],
                   [worker_inactive],
                   [status], 
                   [status_name],
                   [status_category],
                   [type], 
                   [goal_amt],
                   [ask_amt],
                   CASE WHEN [status] = 23 THEN [ask_amt] ELSE 0.0 END AS [ask_amt_open],
                   [plan_yield],
                   [campaign_goal],
                   [fiscal_year_goal],
                   [plan_complete_by_dt],
                   [cont_designation],
                   [designation]
            FROM [#plan_detail]             --WHERE report_section_no = 2
            ORDER BY [report_section_no]

    DONE:

        IF OBJECT_ID('tempdb..#plan_detail') IS NOT NULL DROP TABLE [#plan_detail]

END
GO

EXECUTE [dbo].[LRP_ADV_CAMPAIGN_ACTIVITY_PLAN] @fiscal_year = 2020

