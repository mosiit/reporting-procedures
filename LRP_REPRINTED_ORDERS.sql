USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_REPRINTED_ORDERS]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LRP_REPRINTED_ORDERS]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LRP_REPRINTED_ORDERS]
        @report_start_dt DATETIME = NULL,
        @report_end_dt DATETIME = NULL,
        @title_str VARCHAR(4000) = '',
        @reprint_threshold INT = 3
AS BEGIN

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    SET NOCOUNT ON;
    SET ANSI_WARNINGS OFF;

    /* Procedure Variables  */

        DECLARE @report_msg VARCHAR(100) = ''
        DECLARE @consolodate_line_items CHAR(1) = 'Y'  --Used to be a parameter.  Removed because changing to N produces a much larger report.

        DECLARE @title_list TABLE ([title_no] INT NOT NULL DEFAULT(0))
        
        --DECLARE @order_table TABLE ([order_no] INT)
        DECLARE @sli_table TABLE ([sli_no] INT)
        DECLARE @reprint_table TABLE ([order_no] INT, [customer_no] INT, [customer_name] VARCHAR(150), [created_by] VARCHAR(10), [create_dt] DATETIME, [last_updated_by] VARCHAR(10), 
                                      [last_update_dt] DATETIME, [tot_due_amt] MONEY, [tot_paid_amt] MONEY, [balance_due] MONEY, [sub_lineitem_count] INT, [sli_no] INT, [sli_status] INT, 
                                      [sli_status_name] VARCHAR(30), [perf_no] INT, [zone_no] INT, [title_name] VARCHAR(30), [production_name] VARCHAR(30), [performance_dt] DATETIME, 
                                      [performance_date] CHAR(10), [performance_time] CHAR(8), [reprinted_by] VARCHAR(10), [reprint_count] INT, [reprint_ind] CHAR(1), [last_reprint_dt] DATETIME, 
                                      [reprint_str] VARCHAR(100), [report_msg] VARCHAR(100))

    /*  Check Parameters - If nulls passed to date parameters, run for today. */
          
        SELECT @report_start_dt = IsNull(@report_start_dt, getdate()),
               @report_end_dt = IsNull(@report_end_dt, getdate())

        SELECT @reprint_threshold = ISNULL(@reprint_threshold,3)

        SELECT @consolodate_line_items = ISNULL(@consolodate_line_items,'Y')

        SELECT @report_start_dt = convert(char(10),@report_start_dt,111) + ' 00:00:00',
               @report_end_dt = convert(char(10), @report_end_dt,111) + ' 23:59:59.957'

        IF ISNULL(@title_str,'') <> ''
            INSERT INTO @title_list ([title_no])
            SELECT CONVERT(INT,Element) FROM dbo.FT_SPLIT_LIST(REPLACE(@title_str,'"',''),',')
        ELSE
            INSERT INTO @title_list ([title_no])
            SELECT [title_no] FROM [dbo].[T_TITLE]

    /*  Determine which orders were reprinted within the date range specificied at least the number of times specified.  */

        INSERT INTO @sli_table ([sli_no])
        SELECT DISTINCT(sli_no)
        FROM [dbo].[TX_SLI_TICKET] 
        WHERE [printed_date] BETWEEN @report_start_dt AND @report_end_dt AND reprint_ind = 'Y' AND reprint_No >= @reprint_threshold
        
        --INSERT INTO @order_table
        --SELECT DISTINCT(order_no)
        --FROM [dbo].[T_SUB_LINEITEM]
        --WHERE [sli_no] IN (SELECT sli_no FROM [dbo].[TX_SLI_TICKET] WHERE [printed_date] BETWEEN @report_start_dt AND @report_end_dt AND reprint_ind = 'Y' AND reprint_No >= @reprint_threshold)

    /*  Using that order list, get the number of times and by whom the order items were reprinted.  */
     
        INSERT INTO @reprint_table
        SELECT ord.[order_no], ord.[customer_no], ISNULL(LTRIM(RTRIM(cus.[fname] + ' ' + CASE WHEN ISNULL(cus.[mname],'') = '' THEN '' ELSE cus.[fname] + ' ' END + cus.[lname])),''), 
               ord.[created_by], ord.[create_dt], ord.[last_updated_by], ord.[last_update_dt], ord.[tot_due_amt], ord.[tot_paid_amt], (ord.[tot_due_amt] - ord.[tot_paid_amt]), 0,
               sli.[sli_no], sli.[sli_status], sls.[description], sli.[perf_no], ISNULL(sli.[zone_no],0), ISNULL(prf.[title_name],'UNKNOWN'), ISNULL(prf.[production_name],'UNKNOWN'), 
               prf.[performance_dt], ISNULL(prf.[performance_date],'UNKNOWN'), ISNULL(prf.[performance_time],'UNKNOWN'), ISNULL(tik.[printed_by],'Unknown'), 
               ISNULL(COUNT(DISTINCT tik.[ticket_no]),0), ISNULL(tik.reprint_ind,'Y'), MAX(tik.[printed_date]), '', ''
        FROM [dbo].[T_ORDER] AS ord
             LEFT OUTER JOIN [dbo].[T_CUSTOMER] AS cus ON cus.[customer_no] = ord.[customer_no]
             LEFT OUTER JOIN [dbo].[T_SUB_LINEITEM] AS sli ON sli.[order_no] = ord.[order_no]
             INNER JOIN @sli_table AS slt ON slt.[sli_no] = sli.[sli_no]
             LEFT OUTER JOIN [dbo].[TR_SLI_STATUS] AS sls ON sls.[id] = sli.[sli_status]
             LEFT OUTER JOIN [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] AS prf ON prf.[performance_no] = sli.[perf_no] AND prf.[performance_zone] = sli.[zone_no]
             INNER JOIN @title_list AS ttl ON ttl.[title_no] = prf.[title_no]
             LEFT OUTER JOIN [dbo].[TX_SLI_TICKET] AS tik ON tik.[sli_no] = sli.[sli_no] --AND tik.[reprint_ind] = 'Y'
        GROUP BY ord.[order_no], ord.[customer_no], ISNULL(LTRIM(RTRIM(cus.[fname] + ' ' + CASE WHEN ISNULL(cus.[mname],'') = '' THEN '' ELSE cus.[fname] + ' ' END + cus.[lname])),''), 
                 ord.[created_by], ord.[create_dt], ord.[last_updated_by], ord.[last_update_dt], ord.[tot_due_amt], ord.[tot_paid_amt], (ord.[tot_due_amt] - ord.[tot_paid_amt]),
                 sli.[sli_no], sli.[sli_status], sls.[description], sli.[perf_no], ISNULL(sli.[zone_no],0), ISNULL(prf.[title_name],'UNKNOWN'), ISNULL(prf.[production_name],'UNKNOWN'), 
                 prf.[performance_dt], ISNULL(prf.[performance_date],'UNKNOWN'), ISNULL(prf.[performance_time],'UNKNOWN'), ISNULL(tik.[printed_by],'Unknown'), ISNULL(tik.reprint_ind,'Y')

    /*  Delete Return Sublineitems  */

        DELETE FROM @reprint_table WHERE sli_status_name = 'Return'

    /*  Count the number of subitems in the order where the status is "paid" - Zero = order was canceled/returned */    
    
        UPDATE @reprint_table 
        SET [sub_lineitem_count] = (SELECT COUNT(*) FROM [dbo].[T_SUB_LINEITEM] WHERE [T_SUB_LINEITEM].[order_no] = [@reprint_table].[order_no] AND [T_SUB_LINEITEM].[sli_status] IN (3, 6, 12))

    /*  If there is a performance number but no zone, retrieve title name, production name, and performance datetime  */

        UPDATE @reprint_table 
        SET [title_name] = (SELECT MAX([title_name]) FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] WHERE [performance_no] = [@reprint_table].perf_no),
            [production_name] = (SELECT MAX([production_name]) FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] WHERE [performance_no] = [@reprint_table].perf_no),
            [performance_dt] = (SELECT MAX([performance_dt]) FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] WHERE [performance_no] = [@reprint_table].perf_no)
        WHERE [zone_no] = 0
        
        UPDATE @reprint_table SET [performance_date] = CONVERT(CHAR(10),[performance_dt],111)
        WHERE [performance_date] = 'UNKNOWN' AND [performance_dt] IS NOT NULL

    /* Set customer name to "Annonymous" on orders with no customer */

        UPDATE @reprint_table SET [customer_name] = 'Annonymous' WHERE [customer_no] = 0 AND [customer_Name] = ''

    /*  Create reprint string for easier display on the report  */

        UPDATE @reprint_table SET [reprint_str] = 'Reprinted 1 time by ' + [reprinted_by] WHERE reprint_count = 1
        UPDATE @reprint_table SET [reprint_str] = 'Reprinted ' + CAST([reprint_count] AS VARCHAR(10)) + ' times by ' + [reprinted_by] WHERE reprint_count <> 1

    FINISHED:

        /*  If no records found, insert single record with a "No records found" message  */

            IF NOT EXISTS (SELECT * FROM @reprint_table) BEGIN

                IF @report_msg = '' SELECT @report_msg = 'No records found for the criteria you entered.'
                INSERT INTO @reprint_table
                VALUES  (0 , 0 , '' , '' , NULL , '' , NULL , 0.00 , 0.00 , 0.00 , 0 , 0 , 0 , '' , 0 , 0 , '' , '' , Null , '' , '' , '' , 0, '' , NULL , '' , @report_msg)

            END

        /*  Select final data set out of @reprint_table to send to the report  */


            IF @consolodate_line_items = 'Y'
                SELECT [order_no], [customer_no], [customer_name], [created_by], [create_dt], [last_updated_by], [last_update_dt], [tot_due_amt], [tot_paid_amt], [balance_due], 
                       [sub_lineitem_count], 0 AS 'sli_no', 0 AS 'sli_status', '' AS 'sli_status_name', [perf_no], [zone_no], [title_name], [production_name], [performance_dt], [performance_date], 
                       [performance_time], [reprinted_by], [reprint_ind], MAX([reprint_count]) AS 'reprint_count', MAX([last_reprint_dt]) AS 'last_reprint_dt', [report_msg]
                FROM @reprint_table
                GROUP BY [order_no], [customer_no], [customer_name], [created_by], [create_dt], [last_updated_by], [last_update_dt], [tot_due_amt], [tot_paid_amt], [balance_due], 
                         [sub_lineitem_count], [perf_no], [zone_no], [title_name], [production_name], [performance_dt], [performance_date], 
                         [performance_time], [reprinted_by], [report_msg], [reprint_ind]
                ORDER BY [order_no], [title_name], [performance_dt], [performance_time], [reprint_ind], [last_reprint_dt]
            ELSE
                SELECT [order_no], [customer_no], [customer_name], [created_by], [create_dt], [last_updated_by], [last_update_dt], [tot_due_amt], [tot_paid_amt], [balance_due], 
                       [sub_lineitem_count], [sli_no], [sli_status], [sli_status_name], [perf_no], [zone_no], [title_name], [production_name], [performance_dt], [performance_date], 
                       [performance_time], [reprinted_by], [reprint_ind], [reprint_count], [last_reprint_dt], [report_msg]
                FROM @reprint_table

    DONE:

            SET ANSI_WARNINGS ON;

END
GO


GRANT EXECUTE ON [dbo].[LRP_REPRINTED_ORDERS] TO ImpUsers
GO

EXECUTE [dbo].[LRP_REPRINTED_ORDERS] '8-1-2018', '8-31-2018', '"161"', 2


--SELECT * FROM dbo.T_INVENTORY WHERE type = 'T'

--SELECT @report_start_dt = '10-1-2016', @report_end_dt = '10-31-2016 23:59:59.957', @reprint_threshold = 3    
--SELECT * FROM dbo.T_FACILITY ORDER BY facil_no
--SELECT * FROM dbo.TR_NSCAN_PROFILES WHERE facility_str LIKE '%17%' ORDER BY device_name
--SELECT * FROM dbo.T_ATTENDANCE WHERE perf_no = 14266
--SELECT * FROM dbo.T_NSCAN_EVENT_CONTROL WHERE perf_no = 14266
--SELECT * FROM dbo.LV_PRODUCTION_ELEMENTS_PERFORMANCE WHERE performance_no IN (13251,14265,19010)
--SELECT * FROM dbo.LV_PRODUCTION_ELEMENTS_PERFORMANCE WHERE performance_no = 14265
--SELECT * FROM dbo.T_ATTENDANCE WHERE ticket_no = 732406
--SELECT * FROM dbo.T_TICKET_HISTORY WHERE = 732406
--SELECT * FROM dbo.TX_SLI_TICKET WHERE ticket_no = 730021
--SELECT * FROM dbo.TX_SLI_TICKET
--SELECT * FROM dbo.TX_SLI_TICKET WHERE reprint_ind = 'Y' ORDER BY reprint_no DESC
--SELECT sli_no, COUNT(*) FROM dbo.TX_SLI_TICKET GROUP BY sli_no HAVING COUNT(*) > 2
--SELECT sli_no, COUNT(*) FROM dbo.TX_SLI_TICKET WHERE sli_no = 1225055 GROUP BY sli_no HAVING COUNT(*) > 2
--SELECT * FROM dbo.T_SUB_LINEITEM WHERE sli_no = 1225055
--SELECT * FROM dbo.TX_SLI_TICKET WHERE sli_no = 1225055
--SELECT * FROM dbo.T_ATTENDANCE WHERE ticket_no in
--    (SELECT ticket_no FROM dbo.TX_SLI_TICKET WHERE sli_no IN (SELECT sli_no FROM dbo.T_SUB_LINEITEM WHERE order_no = 447136))
--SELECT * FROM dbo.T_NSCAN_EVENT_CONTROL WHERE ticket_no IN
--    (SELECT ticket_no FROM dbo.TX_SLI_TICKET WHERE sli_no IN (SELECT sli_no FROM dbo.T_SUB_LINEITEM WHERE order_no = 447136))
--SELECT inv_no, description FROM dbo.T_INVENTORY WHERE type = 'T'