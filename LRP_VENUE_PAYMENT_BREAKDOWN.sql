USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_VENUE_PAYMENT_BREAKDOWN]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LRP_VENUE_PAYMENT_BREAKDOWN]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LRP_VENUE_PAYMENT_BREAKDOWN]
        @report_start_dt DATETIME = NULL,
        @report_end_dt DATETIME = Null,
        @report_title VARCHAR(1000) = '0',
        @separate_productions CHAR(1) = 'N'
AS BEGIN

    /*  Report Variables  */

        DECLARE @rpt_message VARCHAR(100) = ''

    /*  Create Temporary Tables  */

        IF OBJECT_ID('tempdb..#payment_title_table') IS NOT NULL DROP TABLE [#payment_title_table]

        CREATE TABLE [#payment_title_table] ([title_no] INT, [title_no_str] VARCHAR(25))

        IF OBJECT_ID('tempdb..#payment_perf_table') IS NOT NULL DROP TABLE [#payment_perf_table]

        CREATE TABLE [#payment_perf_table] ([perf_no] INT, [perf_title_no] INT, [perf_title_name] VARCHAR(30), [perf_production_no] INT, [perf_production_name] VARCHAR(30), [perf_dt] DATETIME)

        CREATE CLUSTERED INDEX [ix_payment_perf_table_perf_no] ON [#payment_perf_table] ([perf_no] ASC) ON [PRIMARY]

        IF OBJECT_ID('tempdb..#venue_payment_breakdown_table') IS NOT NULL DROP TABLE [#venue_payment_breakdown_table]

        CREATE TABLE [#venue_payment_breakdown_table] ([order_no] INT, [transaction_no] INT, [transaction_reference_no] INT, [payment_no] INT, [payment_sequence_no] INT, [payment_created_by] VARCHAR(50), 
                                                       [payment_dt] DATETIME, [payment_amount] MONEY, [payment_type_no] INT, [payment_type_name] VARCHAR(30), [payment_method_no] INT, 
                                                       [payment_method_name] VARCHAR(30), [payment_customer_no] INT, [customer_name] VARCHAR(150), [transaction_perf_no] INT, [title_name] VARCHAR(30), 
                                                       [production_name] VARCHAR(30), [performance_dt] DATETIME, [report_message] VARCHAR(100))

        CREATE CLUSTERED INDEX [ix_venue_payment_breakdown_table_transaction_perf_no] ON [#venue_payment_breakdown_table] ([transaction_perf_no] ASC) ON [PRIMARY]

    /*  Check Parameters  */
    
        SELECT @report_start_dt = ISNULL(@report_start_dt, DATEADD(DAY,-1,GETDATE()))
        SELECT @report_end_dt = ISNULL(@report_end_dt, DATEADD(DAY,-1,GETDATE()))

        SELECT @report_title = ISNULL(@report_title, 0)

        SELECT @separate_productions = ISNULL(@separate_productions,'N')

        SELECT @report_start_dt = CONVERT(CHAR(10),@report_start_dt,111) + ' 00:00:00',
               @report_end_dt = CONVERT(CHAR(10),@report_end_dt, 111) + ' 23:59:59.927'

    /*  Parse Out Title String  */

        INSERT INTO [#payment_title_table] 
        SELECT 0, LTRIM(RTRIM(Item)) FROM [dbo].[LFT_SPLIT_DILEMETED_STRING] (@report_title,',')

        DELETE FROM #payment_title_table WHERE ISNUMERIC([title_no_str]) = 0

        UPDATE [#payment_title_table] SET [title_no] = CAST([title_no_str] AS INT)
        
    /*  Generate a list of all performances (stored in the #payment_perf_table) for the designated date range  */
    
        INSERT INTO [#payment_perf_table]
        SELECT DISTINCT [performance_no], [title_no], [title_name], [production_no], [production_name], [performance_dt]
        FROM dbo.LV_PRODUCTION_ELEMENTS_PERFORMANCE
        WHERE [performance_dt] BETWEEN @report_start_dt AND @report_end_dt AND title_no IN (SELECT [title_no] FROM [#payment_title_table])
   
    /*  Pull all payments for the designated performances */

        INSERT INTO [#venue_payment_breakdown_table]
        SELECT pay.[order_no], pay.[transaction_no], pay.[transaction_reference_no], pay.[payment_no], pay.[payment_sequence_no], pay.[payment_created_by], 
               pay.[payment_dt], pay.[payment_amount], pay.[payment_type_no], pay.[payment_type_name],  pay.[payment_method_no], pay.[payment_method_name], 
               pay.[payment_customer_no], pay.[payment_customer_name], pay.[transaction_perf_no], prf.[perf_title_name], prf.[perf_production_name], 
               prf.[perf_dt], ''
        FROM [dbo].[LV_ORDER_PAYMENTS] AS pay (NOLOCK)
             INNER JOIN [#payment_perf_table] AS prf (NOLOCK) ON prf.[perf_no] = pay.[transaction_perf_no]

    /*  If sorting is going to be on title only and not production, set all production names to blank */    

        IF @separate_productions = 'N'
            UPDATE [#venue_payment_breakdown_table] SET [production_name] = '' 

    FINISHED:

        /*  If nothing found, add a single record with a report message  */

            IF NOT EXISTS (SELECT * FROM [#venue_payment_breakdown_table]) BEGIN

                IF @rpt_message = '' SELECT @rpt_message = 'No records found for the criteria entered.'

                INSERT INTO [#venue_payment_breakdown_table]
                VALUES  (0, 0, 0, 0, 0, '', NULL, 1.00, 0, '', 0, '', 0, '', 0, '', '', Null, @rpt_message)

            END
 
         /*  Pull final data set to be returned to the report.  */

            SELECT [title_name], 
                   [production_name], 
                   CASE WHEN [payment_type_name] = '' THEN [payment_method_name] ELSE [payment_type_name] END AS 'payment_type_name',
                   COUNT(DISTINCT [order_no]) AS 'order_count', 
                   COUNT(DISTINCT [payment_no]) AS 'payment_count',
                   SUM([payment_amount]) AS 'payment_amount',
                   [report_message]
            FROM [#venue_payment_breakdown_table]
            GROUP BY [title_name], [production_name], CASE WHEN [payment_type_name] = '' THEN [payment_method_name] ELSE [payment_type_name] END, [report_message]
            HAVING SUM(payment_amount) <> 0.00


    DONE:

        /*  Clean up  */

            IF OBJECT_ID('tempdb..#payment_perf_table') IS NOT NULL DROP TABLE [#payment_perf_table]

            IF OBJECT_ID('tempdb..#venue_payment_breakdown_table') IS NOT NULL DROP TABLE [#venue_payment_breakdown_table]

END
GO

GRANT EXECUTE ON [dbo].[LRP_VENUE_PAYMENT_BREAKDOWN] TO ImpUsers
GO


--EXECUTE [dbo].[LRP_VENUE_PAYMENT_BREAKDOWN] '4-1-2017', '4-30-2017', '1132, 1343, 161', 'N'

--SELECT * FROM dbo.LV_SS_PRODUCTION_ELEMENTS_TITLE


