USE [impresario]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Tanya Hoffmann (Tessitura Network)
-- Create date: 5/18/2017
-- Description:	Created for MOS - procedure to run invoices only populate tables that can be 
--				used for Mail2 Email process

-- 8/25/2017 -- TAH  added parameter to include paid orders
-- 8/31 2017 -- TAH  only return orders notes for Traveling Programs MOS = 6
--					 change payment description for Scholarship to just say "Scholarship"

--9/25/2017 --  TAH edited the Update Past - was not marking rows from today as having been sent.

--10/31.2017 -- TAH edited, the report was not taking into account that it could be run multiple times in a day, so it was still
-- having duplicate contacts in the views.


--4/10/2018 -- TAH edited, the report was not taking into account that it could be run multiple times in a day, so it was still
-- having duplicate contacts in the views.
-- 5/4/2018  TAH - trying a new tactic for multiple runs in the same day - deleting rows from previous runs for the same email, order, run_dt
--6/6/2018 - TAH for @mark_prev_sent - making this only for the MOS that was passed in.
-- 7/3/2018 TAH change the sort order

--exec [LRP_ORDER_ACK_INVOICE_EMAIL]

--	@order_start_dt	 = '11/26/16',
--	@order_end_dt	 = '12/8/16',	
--	@mos_str	 = '5',
--	@reprint_ind	 = 'Y',
--	@list_no	 = NULL,	
--	@p_order_no 	 = null

-- =============================================
ALTER PROCEDURE [dbo].[LRP_ORDER_ACK_INVOICE_EMAIL]

	-- Add the parameters for the stored procedure here
	(@order_start_dt	datetime = NULL,
	@order_end_dt	datetime = NULL,
	@perf_start_dt datetime = null,
	@perf_end_dt datetime = null,
	@mos_str	varchar(255) = NULL,
	@reprint_ind	char(1) = 'Y',
	@list_no	int = NULL,
	@mailing_type 	varchar(8) = null,	
	@p_order_no 	int = null,
	@eaddress_type int = NULL,
	@purpose int = NULL,
	@mark_prev_sent char(1) = 'N',
	@include_paid char(1) = 'N')

WITH EXECUTE AS OWNER 	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @run_dt datetime = (select getdate())
	if @p_order_no = 0
	begin set @p_order_no = null end


	create table #tmos(id int null, description varchar(30) null)
 
IF isnull(@p_order_no, -1) <> -1
--IF @mos_str is null or ISNULL(Datalength(ltrim(@mos_str)),0) = 0
	Insert	into #tmos
	Select	a.id, a.description
	From	[dbo].TR_MOS a
	Join	T_ORDER b on a.id = b.mos and b.order_no = @p_order_no
Else
	Insert 	into #tmos
	Select 	id, description
	from 	[dbo].TR_MOS
	where 	charindex(',' + convert(varchar,id) + ',' , ',' + @mos_str + ',') > 0
 

	-- If asked mark everything from yesterday and beyond as sent
	-- for the MOS used
	IF @mark_prev_sent = 'Y'
	  BEGIN
		update a		
		set sent_ind = 'Y'
		from dbo.LT_ORDER_ACK_INVOICE_EMAIL a
		join #tmos b on b.id = a.mos
		where sent_ind = 'N'
		and cast(run_dt as date)  < =   cast(@run_dt as date)  -- dateadd(dd,-1,cast(@run_dt as date))
	  END

	-- run LRP_ORDER_ACK_INVOICE
	-- this populates the table LTW_ORDER_ACK_INVOICE_EMAIL

	EXEC [dbo].[LRP_ORDER_ACK_INVOICE]
		@order_start_dt	 = @order_start_dt,
		@order_end_dt	 = @order_end_dt,
		@perf_start_dt = @perf_start_dt,
		@perf_end_dt = @perf_end_dt,
		@mos_str	 = @mos_str,
		@reprint_ind	 = @reprint_ind,
		@list_no	 = @list_no,
		@mailing_type 	 = @mailing_type,	
		@p_order_no 	 = @p_order_no,
		@email_ind = 'Y',
		@run_dt = @run_dt



	-- run [dbo].[LRP_ORDER_ACK_INVOICE_DETAIL] for each order now in LTW_ORDER_ACK_INVOICE_EMAIL
	-- this will populate LTW_ORDER_ACK_INVOICE_EMAIL_DETAIL
		truncate table LTW_ORDER_ACK_INVOICE_EMAIL_DETAIL
		declare @next_order_no int
		set @next_order_no = (select min(order_no) from LTW_ORDER_ACK_INVOICE_EMAIL where run_dt = @run_dt)

		while @next_order_no is not null
		begin
			--insert  LT_ORDER_ACK_INVOICE_EMAIL_DETAIL
			execute LRP_ORDER_ACK_INVOICE_DETAIL @reprint_ind = @reprint_ind,	@order_no  = @next_order_no, @email_ind = 'Y'
			set @next_order_no = (select min(order_no) from LTW_ORDER_ACK_INVOICE_EMAIL where order_no > @next_order_no and run_dt = @run_dt)
		end


	-- run [dbo].[LRP_ORDER_ACK_INVOICE_PT_DETAIL] for each order now in LTW_ORDER_ACK_INVOICE_EMAIL
	-- this will populate LTW_ORDER_ACK_INVOICE_EMAIL_PT_DETAIL
		truncate table LTW_ORDER_ACK_INVOICE_EMAIL_PT_DETAIL
		
		set @next_order_no = (select min(order_no) from LTW_ORDER_ACK_INVOICE_EMAIL where run_dt = @run_dt)

		while @next_order_no is not null
		begin			
			execute LRP_ORDER_ACK_INVOICE_PT_DETAIL @reprint_ind = @reprint_ind,	@order_no  = @next_order_no, @email_ind = 'Y'
			set @next_order_no = (select min(order_no) from LTW_ORDER_ACK_INVOICE_EMAIL where order_no > @next_order_no and run_dt = @run_dt)
		end

		Update LTW_ORDER_ACK_INVOICE_EMAIL_DETAIL
		set description = 'Scholarship'
		where item = 'Scholarship'

		-- get eaddress for order
		-- first get the if the order itself has an email address
		update a
		set eaddress = c.eaddress_no
		--select b.eaddress_no
		from LTW_ORDER_ACK_INVOICE_EMAIL a
		join T_ORDER b on a.order_no = b.order_no
		join T_EADDRESS c on b.eaddress_no = c.eaddress_no and c.inactive = 'N'
		where run_dt = @run_dt

		-- Then get it based on type and purpose passed in
		update a
		set a.eaddress = c.address
		from LTW_ORDER_ACK_INVOICE_EMAIL a
		join T_ORDER b on a.order_no = b.order_no
		cross apply dbo.ft_get_eaddress(null, @eaddress_type, @purpose, null, coalesce(b.initiator_no, b.customer_no), 'N') c --on c.customer_no = a.customer_no
		where a.eaddress is null
		and run_dt = @run_dt


	-- Delete rows from LT_ORDER_ACK_INVOICE_EMAIL
	-- for the same email address, order no run date where sent is "N"
	delete b
	from LTW_ORDER_ACK_INVOICE_EMAIL a
	join LT_ORDER_ACK_INVOICE_EMAIL b on a.eaddress = b.eaddress and a.order_no = b.order_no and cast(a.run_dt as date)  = cast(b.run_dt as date) and b.sent_ind = 'N'


	INSERT LT_ORDER_ACK_INVOICE_EMAIL
	(customer_no
	,order_no
	,order_no_prefix
	,order_dt
	,initiator_esal1_desc
	,initiator_esal2_desc
	,initiator_business_title
	,esal1_desc
	,street1
	,street2
	,street3
	,city
	,state
	,postal_code
	,country
	,mos
	,remit_terms
	,remit_name
	,remit_contact
	,remit_street1
	,remit_street2
	,remit_street3
	,remit_city_state_zip
	,remit_info
	,invoice_no
	,tot_ticket_amt
	,tot_fee_amt
	,tot_contribution_amt
	,tot_paid_amt
	,balance
	,notes
	,general_1
	,general_2
	,general_3
	,general_4
	,general_5
	,custom_8
	,eaddress
	,run_dt
	,num_records)

	select customer_no
	,order_no
	,order_no_prefix
	,order_dt
	,initiator_esal1_desc
	,initiator_esal2_desc
	,initiator_business_title
	,esal1_desc
	,street1
	,street2
	,street3
	,city
	,state
	,postal_code
	,country
	,mos
	,remit_terms
	,remit_name
	,remit_contact
	,remit_street1
	,remit_street2
	,remit_street3
	,remit_city_state_zip
	,remit_info
	,invoice_no
	,tot_ticket_amt
	,tot_fee_amt
	,tot_contribution_amt
	,tot_paid_amt
	,balance
	,case when mos = 6 then notes else null end -- only get the notes for Traveling programs MOS 5
	,general_1
	,general_2
	,general_3
	,general_4
	,general_5
	,custom_8
	,eaddress
	,run_dt
	, row_number() over (partition by eaddress order by eaddress) 
	from LTW_ORDER_ACK_INVOICE_EMAIL
	WHERE balance > 
		case when @include_paid = 'N' then 0 
				else -1 
		end
	and eaddress is not null
	-- concatenate detail info using a function

	update a
	set detail = dbo.LFS_M2_INVOICE_DETAIL(order_no)
	--select  order_no, dbo.LFS_M2_INVOICE_DETAIL(order_no)
	from LT_ORDER_ACK_INVOICE_EMAIL a
	where run_dt = @run_dt
	and isnull(sent_ind, 'N')  != 'Y'

	update a
	set a.pt_detail =   dbo.LFS_M2_INVOICE_PT_DETAIL(order_no)
	 --select  order_no, dbo.LFS_M2_INVOICE_PT_DETAIL(order_no)
	from LT_ORDER_ACK_INVOICE_EMAIL a
	where run_dt = @run_dt
	and isnull(sent_ind, 'N')  != 'Y'
	
	-- update if there is more than on email address for this run date	
	-- multiple views can be created.
	
	select   row_number() over (partition by a.eaddress order by a.eaddress) num_records, a.eaddress, @run_dt  run_dt , a.order_no
	into #temp
	from LT_ORDER_ACK_INVOICE_EMAIL a
	where cast(run_dt as date) = cast(@run_dt as date) and sent_ind = 'N'
	--join 
	--(select eaddress, cast(run_dt as date) run_dt, count(*) num_rows 
	--	from LT_ORDER_ACK_INVOICE_EMAIL where cast(run_dt as date) = cast(@run_dt as date) and sent_ind = 'N' group by eaddress, cast(run_dt as date) having count(*) > 1 ) b on a.eaddress = b.eaddress and a.run_dt = b.run_dt

	update a
	set a.num_records = b.num_records
	from LT_ORDER_ACK_INVOICE_EMAIL a
	join #temp b on a.eaddress = b.eaddress and cast(b.run_dt as date) = cast(a.run_dt as date) and a.order_no = b.order_no
	where sent_ind = 'N'


	select a.*, b.description mode_of_sale
	from LT_ORDER_ACK_INVOICE_EMAIL a
	join TR_MOS b on a.mos = b.id
	where cast(run_dt as date) = cast(@run_dt as date)
	and sent_ind = 'N'
	order by b.description, eaddress, order_no
	--select * 
	--from LTW_ORDER_ACK_INVOICE_EMAIL_DETAIL

	--select * 
	--from LTW_ORDER_ACK_INVOICE_EMAIL_PT_DETAIL


END



