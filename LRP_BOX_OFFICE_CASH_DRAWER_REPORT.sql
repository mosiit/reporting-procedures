USE [impresario]
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_BOX_OFFICE_CASH_DRAWER_REPORT]') AND type in (N'P', N'PC')) BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_BOX_OFFICE_CASH_DRAWER_REPORT] AS' 
    EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LRP_BOX_OFFICE_CASH_DRAWER_REPORT] TO ImpUsers'
END
GO


ALTER PROCEDURE [dbo].[LRP_BOX_OFFICE_CASH_DRAWER_REPORT]
        @report_dt DATETIME = NULL,
        @operator_str varchar(4000) = '',
        @exclude_zeros CHAR(1) = 'N'
AS BEGIN

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    SET NOCOUNT ON;
    
    /*  Procedure Variables and temp tables  */
    
        --DECLARE @starting_amount DECIMAL(18,2) = '0.00';
        DECLARE @recommended_keep_amount DECIMAL (18,2) = '100.00';

        DECLARE @operator_list AS TABLE ([operator_id] VARCHAR(25));

        DECLARE @final_table AS TABLE ([operator] VARCHAR(50) NOT NULL,
                                       [operator_name] VARCHAR(150) NOT NULL DEFAULT (''),
                                       [operator_sort] VARCHAR(150) NOT NULL DEFAULT (''),
                                       [cash_dt] DATETIME NULL,
                                       [start_amount] DECIMAL(18,2) NOT NULL DEFAULT (''),
                                       [cash_count] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                       [cash_amount] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                       [drop_count] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                       [drop_ampount] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                       [cash_on_hand] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                       [drawer_check] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                       [recomended_drop] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                       [recomended_drop_rounded] DECIMAL(18,2) NOT NULL DEFAULT (0.0))

                                                                                                                              
        IF OBJECT_ID('tempdb..#cash_table') IS NOT NULL DROP TABLE [#cash_table];
    
        CREATE TABLE [#cash_table] ([operator] VARCHAR(50) NOT NULL,
                                    [operator_name] VARCHAR(150) NOT NULL DEFAULT(''),
                                    [operator_sort] VARCHAR(150) NOT NULL DEFAULT(''),
                                    [cash_dt] DATETIME NULL,
                                    [value_type] VARCHAR(30) NOT NULL DEFAULT(''),
                                    [value_amount] DECIMAL(18,2) NOT NULL DEFAULT(0.0));

    /*  Check Parameters  */

        SELECT @report_dt = ISNULL(@report_dt,GETDATE());

        SELECT @report_dt = CAST(@report_dt AS DATE);
        
        IF ISNULL(@operator_str,'') = ''
            INSERT INTO @operator_list ([operator_id])
            SELECT [userid] FROM [dbo].[LV_MuseumUserInfo] WHERE location = 'Box Office'
        ELSE
            INSERT INTO @operator_list ([operator_id])
            SELECT [element] FROM [dbo].[FT_SPLIT_LIST](REPLACE(@operator_str,'"',''),',');
   
        SELECT @exclude_zeros = ISNULL(@exclude_zeros,'N')
   
    /*  Get Cash Drops  */      

        INSERT INTO [#cash_table] ([operator], [operator_name], [operator_sort], [cash_dt], [value_type], [value_amount])
            SELECT det.[created_by],
                   usr.[full_name],
                   usr.[sort_name],
                   CAST(det.[create_dt] AS DATE),
                   'Drop Count',
                   COUNT(*)
            FROM [dbo].[LV_ORDER_DETAIL] AS det
                 INNER JOIN @operator_list AS opr ON opr.[operator_id] = det.[created_by]
                 LEFT OUTER JOIN [dbo].[LV_MuseumUserInfo] AS usr ON usr.[userid] = det.[created_by]
            WHERE CAST(det.[create_dt] AS DATE) = @report_dt
              AND det.[production_name] = 'Cash Drop'
            GROUP BY det.[created_by], usr.[full_name], usr.[sort_name], CAST(det.[create_dt] AS DATE)

            UNION ALL

            SELECT det.[created_by],
                   usr.[full_name],
                   usr.[sort_name],
                   CAST(det.[create_dt] AS DATE),
                   'Drop Amount',
                   SUM(CASE WHEN [dbo].[FS_isReallyNumeric]([price_type_name_short]) = 0 THEN 0
                        ELSE CAST(det.[price_type_name_short] AS DECIMAL(18,2)) END)
            FROM [dbo].[LV_ORDER_DETAIL] AS det
                  INNER JOIN @operator_list AS opr ON opr.[operator_id] = det.[created_by]
                  LEFT OUTER JOIN [dbo].[LV_MuseumUserInfo] AS usr ON usr.[userid] = det.[created_by]
            WHERE CAST(det.[create_dt] AS DATE) = @report_dt
              AND det.[production_name] = 'Cash Drop'
            GROUP BY det.[created_by], usr.[full_name], usr.[sort_name], CAST(det.[create_dt] AS DATE)


    /*  Get Cash Payments (count and amount) */    

        INSERT INTO [#cash_table] ([operator], [operator_name], [operator_sort], [cash_dt], [value_type], [value_amount])
            SELECT pay.[payment_operator],
                   (pay.[payment_operator_first_name] + ' ' + pay.[payment_operator_last_name]),
                   (pay.[payment_operator_last_name] + ', ' + pay.[payment_operator_first_name]),
                   CAST(pay.[payment_dt] AS DATE),
                   'Cash Count',
                   COUNT(*)
            FROM [dbo].[LV_RPT_CASHOUT_PAYMENTS] AS pay
                 INNER JOIN @operator_list AS opr ON opr.[operator_id] = pay.[payment_operator]
            WHERE CAST(pay.[payment_dt] AS DATE) = @report_dt
              AND pay.[payment_type_name] = 'Cash'
            GROUP BY pay.[payment_operator], (pay.[payment_operator_first_name] + ' ' + pay.[payment_operator_last_name]), (pay.[payment_operator_last_name] + ', ' + pay.[payment_operator_first_name]), CAST(pay.[payment_dt] AS DATE)

            UNION ALL

            SELECT pay.[payment_operator],
                   (pay.[payment_operator_first_name] + ' ' + pay.[payment_operator_last_name]),
                   (pay.[payment_operator_last_name] + ', ' + pay.[payment_operator_first_name]),
                   CAST(pay.[payment_dt] AS DATE),
                   'Cash Amount',
                   SUM(pay.[payment_amount])
            FROM [dbo].[LV_RPT_CASHOUT_PAYMENTS] AS pay
                  INNER JOIN @operator_list AS opr ON opr.[operator_id] = pay.[payment_operator]
            WHERE CAST(pay.[payment_dt] AS DATE) = @report_dt
              AND pay.[payment_type_name] = 'Cash'
            GROUP BY pay.[payment_operator], (pay.[payment_operator_first_name] + ' ' + pay.[payment_operator_last_name]), (pay.[payment_operator_last_name] + ', ' + pay.[payment_operator_first_name]), CAST(pay.[payment_dt] AS DATE)

    --/*  Get Starting Amounts (hard coded in variable declaration above  */

    --    INSERT INTO [#cash_table] ([operator], [operator_name], [operator_sort], [cash_dt], [value_type], [value_amount])
    --        SELECT DISTINCT [operator],
    --                        [operator_name],
    --                        [operator_sort],              --Removed at the Angela's request
    --                        @report_dt,                   --Commented rather than deleted in case she
    --                        'Start Amount',               --decides she wants to use a starting amount
    --                        @starting_amount              --after all
    --        FROM [#cash_table]

   
    /*  Pivot data into final table for report */

        INSERT INTO @final_table ([operator], [operator_name], [operator_sort], [cash_dt], [start_amount], [cash_count], [cash_amount], [drop_count], [drop_ampount])
            SELECT [operator],
                   [operator_name],
                   [operator_sort],
                   [cash_dt], 
                   ISNULL([start amount],0.0) AS [start_amount], 
                   ISNULL([cash count],0) AS [cash_count],
                   ISNULL([cash amount],0.0) AS [cash_amount],
                   ISNULL([Drop Count],0) AS [drop_count],
                   ISNULL([Drop Amount],0.0) AS [drop_ampount]
            FROM  (SELECT [operator],
                          [operator_name],
                          [operator_sort],
                          [cash_dt], 
                          [value_amount], 
                          [value_type] 
                   FROM [#cash_table]) AS C
            PIVOT (SUM([value_amount]) 
              FOR [value_type] IN ([Start Amount], [Cash Count], [Cash Amount], [Drop Count], [Drop Amount])) AS pvt

    /*  Do the math  */


        UPDATE @final_table
        SET [cash_on_hand] = ([start_amount] + [cash_amount] - [drop_ampount])

        UPDATE @final_table 
        SET [drawer_check] = ([cash_on_hand] - @recommended_keep_amount)

        UPDATE @final_table 
        SET [recomended_drop] = ROUND([drawer_check],0)

        UPDATE @final_table
        SET [recomended_drop_rounded] =  CASE WHEN [recomended_drop] % 50 = 0 THEN [recomended_drop]
                                              WHEN [recomended_drop] % 50 >= 25 THEN [recomended_drop] + (50 - ([recomended_drop] % 50))
                                              ELSE [recomended_drop] - ([recomended_drop] % 50) END

        UPDATE @final_table 
        SET [recomended_drop_rounded] = 0.00
        WHERE [recomended_drop_rounded] < 100.00

    FINISHED:


        /*  Pull Final Data Set  */

            SELECT [operator], 
                   [operator_name], 
                   [operator_sort],
                   [cash_dt], 
                   [start_amount], 
                   [cash_count], 
                   [cash_amount], 
                   [drop_count], 
                   [drop_ampount],
                   [cash_on_hand],
                   [drawer_check],
                   [recomended_drop],
                   [recomended_drop_rounded]
            FROM @final_table
            WHERE [recomended_drop_rounded] > 0 OR @exclude_zeros <> 'Y'

    DONE:

        IF OBJECT_ID('tempdb..#cash_table') IS NOT NULL DROP TABLE [#cash_table];

END
GO

EXECUTE [dbo].[LRP_BOX_OFFICE_CASH_DRAWER_REPORT] @report_dt = '1-22-2019', @operator_str = '', @exclude_zeros = 'N'



--SELECT userid, full_name, sort_name, location FROM [dbo].[LV_MuseumUserInfo] WHERE location = 'Box Office'
--SELECT * FROM dbo.TR_GOOESOFT_DROPDOWN WHERE code = 1