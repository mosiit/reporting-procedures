USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_SALES_STATS]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LRP_SALES_STATS]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LRP_SALES_STATS]
        @report_start_dt DATETIME = NULL,
        @report_end_dt DATETIME = NULL,
        @report_start_dt_2 DATETIME = NULL,
        @report_end_dt_2 DATETIME = NULL,
        @date_type VARCHAR(25) = NULL,
        @title_str VARCHAR(4000) = NULL,
        @production_str VARCHAR(4000) = NULL,
        @mode_of_sale_str varchar(4000) = NULL,
        @channel_str VARCHAR(4000) = NULL,
        @data_grids VARCHAR(4000) = NULL
AS BEGIN

    /*  Procedure Variables  */

        DECLARE @rpt_message VARCHAR(100) = ''
        DECLARE @total_orders_1 DECIMAL(18,2), @total_tickets_1 DECIMAL(18,2), @total_paid_1 DECIMAL(18,2)
        DECLARE @total_orders_2 DECIMAL(18,2), @total_tickets_2 DECIMAL(18,2), @total_paid_2 DECIMAL(18,2)
        DECLARE @title_table TABLE ([title_no] INT, [title_no_str] VARCHAR(25))
        DECLARE @production_table TABLE ([production_no] INT, [production_no_str] VARCHAR(25))
        DECLARE @mode_of_sale_table TABLE ([mode_of_sale_no] INT, [mode_of_sale_no_str] VARCHAR(25))
        DECLARE @channel_table TABLE ([channel_no] INT, [channel_no_str] VARCHAR(25))
        DECLARE @data_grid_table TABLE ([grid_name] VARCHAR(50))

        DECLARE @sales_channel_cache TABLE ([id] INT, [description] VARCHAR(30))
        DECLARE @mode_of_sale_cache TABLE ([id] INT, [description] VARCHAR(30))
        DECLARE @performance_cache TABLE ([date_range] INT, [performance_no] INT, [performance_dt] DATETIME, [performance_code] VARCHAR(10), [performance_title_no] INT)

    /* Check Parameters  */

        SELECT @report_start_dt = ISNULL(@report_start_dt,DATEADD(DAY,-1,GETDATE()))
        SELECT @report_end_dt = ISNULL(@report_end_dt,DATEADD(day,-1,GETDATE()))
        IF @report_start_dt_2 IS NULL OR @report_end_dt_2 IS NULL SELECT @report_start_dt_2 = NULL, @report_end_dt_2 = NULL
        SELECT @date_type = ISNULL(@date_type,'Performance Date')
        SELECT @title_str = ISNULL(@title_str,'')
        SELECT @production_str = ISNULL(@production_str,'')
        SELECT @mode_of_sale_str = ISNULL(@mode_of_sale_str,'')
        SELECT @channel_str = ISNULL(@channel_str,'')
        SELECT @data_grids = ISNULL(@data_grids,'')
        
        SELECT @report_start_dt = CONVERT(VARCHAR(10),@report_start_dt,111)
        SELECT @report_end_dt = CONVERT(VARCHAR(10),@report_end_dt,111) + ' 23:59:59.957'

        IF @report_start_dt_2 IS NOT NULL
            SELECT @report_start_dt_2 = CONVERT(VARCHAR(10),@report_start_dt_2,111)

        IF @report_end_dt_2 IS NOT NULL
            SELECT @report_end_dt_2 = CONVERT(VARCHAR(10),@report_end_dt_2,111) + ' 23:59:59.957'

    /*  For some reason I don't quite understand, joining to the TR_SALES_CHANNEL table increased the run time by over 400% 
        despite the fact that there are only 50ish records in the table.  When I cache the table into a variable first, 
        it runs fine and since I am doing that for Sales Channel, I decided to do it for mode of sale as well and performances  */

        INSERT INTO @sales_channel_cache SELECT [id], [description] FROM [dbo].[TR_SALES_CHANNEL] (NOLOCK)

        INSERT INTO @mode_of_sale_cache SELECT [id], [description] FROM [dbo].[TR_MOS] (NOLOCK)

        INSERT INTO @performance_cache SELECT 1, [performance_no], [performance_dt], [performance_code], [title_no] 
        FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] (NOLOCK)
        WHERE [performance_dt] BETWEEN @report_start_dt AND @report_end_dt

        IF @report_start_dt_2 IS NOT NULL AND @report_end_dt_2 IS NOT NULL 
            INSERT INTO @performance_cache SELECT 2, [performance_no], [performance_dt], [performance_code], [title_no] 
            FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] (NOLOCK)
            WHERE [performance_dt] BETWEEN @report_start_dt_2 AND @report_end_dt_2

    /*  Create two tables for the statistics, one to keep the list of orders to work with and the other to keep the stats from the order detail
        NOTE: Tried to do this with a table variable.  Changing it to a temp table with indexes shortened the run time from 51 seconds to 4.  */

        IF OBJECT_ID('tempdb..#trx_stat_orders') IS NOT NULL DROP TABLE [#trx_stat_Orders]

                    CREATE TABLE [#trx_stat_orders] ([date_range] INT, [order_no] INT, [order_dt] DATETIME, [order_mode_of_sale] INT, [order_sales_channel] INT, 
                                                     [order_customer_no] INT, [order_customer_name] VARCHAR(150), [order_customer_type] CHAR(1), 
                                                     [order_customer_ind] VARCHAR(25))

                    CREATE UNIQUE CLUSTERED INDEX [ix_trx_stat_orders_order_no] ON [#trx_stat_orders] ([date_range] ASC, [order_no] ASC) ON [PRIMARY]
                    CREATE NONCLUSTERED INDEX [ix_trx_stat_orders_order_dt] ON [#trx_stat_orders] ([order_dt] ASC) ON [PRIMARY]
                    CREATE NONCLUSTERED INDEX [ix_trx_stat_orders_customer_no] ON [#trx_stat_orders] ([order_customer_no] ASC) ON [PRIMARY]
                    CREATE NONCLUSTERED INDEX [ix_trx_stat_orders_order_customer_type] ON [#trx_stat_orders] ([order_customer_type] ASC) ON [PRIMARY]
                    CREATE NONCLUSTERED INDEX [ix_trx_stat_orders_order_mode_of_sale] ON [#trx_stat_orders] ([order_mode_of_sale] ASC) ON [PRIMARY]
                    CREATE NONCLUSTERED INDEX [ix_trx_stat_orders_order_sales_channel] ON [#trx_stat_orders] ([order_sales_channel] ASC) ON [PRIMARY]

        IF OBJECT_ID('tempdb..#trx_stat_table') IS NOT NULL DROP TABLE [#trx_stat_table]

                    CREATE TABLE [#trx_stat_table]  ([order_mode_of_sale] INT, [mode_of_sale_name] VARCHAR(30), 
                                                     [order_sales_channel] INT, [sales_channel_name] VARCHAR(30), [order_customer_no] INT, [order_customer_name] VARCHAR(150), 
                                                     [order_customer_type] CHAR(1), [order_customer_ind] VARCHAR(25), [title_no] INT, [title_name] VARCHAR(30), [production_no] INT, 
                                                     [production_name] VARCHAR(30), [order_count] INT, [counter] INT, [due_amount] MONEY, [paid_amount] MONEY, [total_paid] MONEY,
                                                     [order_count_2] INT, [counter_2] INT, [due_amount_2] MONEY, [paid_amount_2] MONEY, [total_paid_2] MONEY, [rpt_message] VARCHAR(100))

                    CREATE CLUSTERED INDEX [ix_trx_stat_table_title_no] ON [#trx_stat_table] ([title_no] ASC) ON [PRIMARY]
                    CREATE NONCLUSTERED INDEX [ix_trx_stat_table_production_no] ON [#trx_stat_table] ([production_no] ASC) ON [PRIMARY]
                    CREATE NONCLUSTERED INDEX [ix_trx_stat_table_order_mode_of_sale] ON [#trx_stat_table] ([order_mode_of_sale] ASC) ON [PRIMARY]
                    CREATE NONCLUSTERED INDEX [ix_trx_stat_table_order_sales_channel] ON [#trx_stat_table] ([order_sales_channel] ASC) ON [PRIMARY]

        IF OBJECT_ID('tempdb..#final_data_table') IS NOT NULL DROP TABLE [#trx_final_table]

                    CREATE TABLE [#final_data_table]  ([category_letter] CHAR(1), [category] VARCHAR(50), [subcategory_1] VARCHAR(50), [subcategory_2] VARCHAR(50), [subcategory_3] VARCHAR(50),
                                                       [orders_1] INT, [orders_pct_1] DECIMAL(18,2), [tickets_1] INT, [tickets_pct_1] DECIMAL(18,2), [paid_1] MONEY, [paid_pct_1] DECIMAL(18,2),
                                                       [orders_2] INT, [orders_pct_2] DECIMAL(18,2), [tickets_2] INT, [tickets_pct_2] DECIMAL(18,2), [paid_2] MONEY, [paid_pct_2] DECIMAL(18,2),
                                                       [rpt_message] VARCHAR(100))

    /*  Parse out table, production, and mode of sale id numbers from the lists of values passed to the report  */

        IF @title_str <> '' BEGIN
            INSERT INTO @title_table SELECT 0, LTRIM(RTRIM(Item)) FROM [dbo].[LFT_SPLIT_DILEMETED_STRING] (@title_str,',')
            DELETE FROM @title_table WHERE ISNUMERIC([title_no_str]) = 0
            UPDATE @title_table SET [title_no] = CAST([title_no_str] AS INT)
        END

        IF @production_str <> '' BEGIN
            INSERT INTO @production_table SELECT 0, LTRIM(RTRIM(Item)) FROM [dbo].[LFT_SPLIT_DILEMETED_STRING] (@production_str,',')
            DELETE FROM @production_table WHERE ISNUMERIC([production_no_str]) = 0
            UPDATE @production_table SET [production_no] = CAST([production_no_str] AS INT)
        END

        IF @mode_of_sale_str <> '' BEGIN
            INSERT INTO @mode_of_sale_table SELECT 0, LTRIM(RTRIM(Item)) FROM [dbo].[LFT_SPLIT_DILEMETED_STRING] (@mode_of_sale_str,',')
            DELETE FROM @mode_of_sale_table WHERE ISNUMERIC([mode_of_sale_no_str]) = 0
            UPDATE @mode_of_sale_table SET [mode_of_sale_no] = CAST([mode_of_sale_no_str] AS INT)
        END

        IF @channel_str <> '' BEGIN
            INSERT INTO @channel_table SELECT 0, LTRIM(RTRIM(Item)) FROM [dbo].[LFT_SPLIT_DILEMETED_STRING] (@channel_str,',')
            DELETE FROM @channel_table WHERE ISNUMERIC([channel_no_str]) = 0
            UPDATE @channel_table SET [channel_no] = CAST([channel_no_str] AS INT)
        END

        IF @data_grids <> '' BEGIN
            INSERT INTO @data_grid_table SELECT LTRIM(RTRIM(Item)) FROM [dbo].[LFT_SPLIT_DILEMETED_STRING] (@data_grids,',')
            UPDATE @data_grid_table SET [grid_name] = REPLACE([grid_name],'"','')
        END

    /*  If titles were passed, delete all the other performances from the cache  */

        IF EXISTS (SELECT * FROM @title_table)
            DELETE FROM @performance_cache WHERE [performance_title_no] NOT IN (SELECT [title_no] FROM @title_table)

    /*  Get a distinct list of all orders for the title list passed to the report  */

        IF @date_type = 'Order Date' BEGIN

            INSERT INTO [#trx_stat_orders]
            SELECT DISTINCT 1, sli.[order_no], CONVERT(DATE,ord.[order_dt]), ord.[MOS], ord.[channel], ord.[customer_no], 
                            LTRIM(ISNULL(cus.[fname],'') + ' ' + CASE WHEN ISNULL(cus.[mname],'') = '' THEN '' ELSE cus.[mname] + ' ' END + ISNULL(cus.[lname],'')),
                            CASE WHEN ord.[customer_no] = 0 THEN 'A' ELSE 'K' END,
                            CASE WHEN ord.[customer_no] = 0 THEN 'Anonymous' ELSE 'Known' END
            FROM [dbo].[T_SUB_LINEITEM] AS sli (NOLOCK)
                 INNER JOIN [dbo].[T_ORDER] AS ord (NOLOCK) ON ord.[order_no] = sli.[order_no]
                 LEFT OUTER JOIN [dbo].[T_CUSTOMER] AS cus (NOLOCK) ON cus.[customer_no] = ord.[customer_no]
            WHERE ord.[order_dt] BETWEEN @report_start_dt AND @report_end_dt AND sli.[sli_status] IN (3, 12)

            IF @report_start_dt_2 IS NOT NULL AND @report_end_dt_2 IS NOT NULL
                INSERT INTO [#trx_stat_orders]
                SELECT DISTINCT 2, sli.[order_no], CONVERT(DATE,ord.[order_dt]), ord.[MOS], ord.[channel], ord.[customer_no], 
                                LTRIM(ISNULL(cus.[fname],'') + ' ' + CASE WHEN ISNULL(cus.[mname],'') = '' THEN '' ELSE cus.[mname] + ' ' END + ISNULL(cus.[lname],'')),
                                CASE WHEN ord.[customer_no] = 0 THEN 'A' ELSE 'K' END,
                                CASE WHEN ord.[customer_no] = 0 THEN 'Anonymous' ELSE 'Known' END
                FROM [dbo].[T_SUB_LINEITEM] AS sli (NOLOCK)
                     INNER JOIN [dbo].[T_ORDER] AS ord (NOLOCK) ON ord.[order_no] = sli.[order_no]
                     LEFT OUTER JOIN [dbo].[T_CUSTOMER] AS cus (NOLOCK) ON cus.[customer_no] = ord.[customer_no]
                WHERE ord.[order_dt] BETWEEN @report_start_dt_2 AND @report_end_dt_2 AND sli.[sli_status] IN (3, 12)

        END ELSE BEGIN

            INSERT INTO [#trx_stat_orders]
            SELECT DISTINCT 1, sli.[order_no], CONVERT(DATE,ord.[order_dt]), ord.[MOS], ord.[channel], ord.[customer_no], 
                            LTRIM(ISNULL(cus.[fname],'') + ' ' + CASE WHEN ISNULL(cus.[mname],'') = '' THEN '' ELSE cus.[mname] + ' ' END + ISNULL(cus.[lname],'')),
                            CASE WHEN ord.[customer_no] = 0 THEN 'A' ELSE 'K' END,
                            CASE WHEN ord.[customer_no] = 0 THEN 'Anonymous' ELSE 'Known' END
            FROM [dbo].[T_SUB_LINEITEM] AS sli (NOLOCK)
                 INNER JOIN [dbo].[T_ORDER] AS ord (NOLOCK) ON ord.[order_no] = sli.[order_no]
                 LEFT OUTER JOIN [dbo].[T_CUSTOMER] AS cus (NOLOCK) ON cus.[customer_no] = ord.[customer_no]
            WHERE sli.[perf_no] IN (SELECT [performance_no] FROM @performance_cache WHERE [date_range] = 1) AND sli.[sli_status] IN (3, 12)

            IF EXISTS (SELECT * FROM @performance_cache WHERE [date_range] = 2)
                INSERT INTO [#trx_stat_orders]
                SELECT DISTINCT 2, sli.[order_no], CONVERT(DATE,ord.[order_dt]), ord.[MOS], ord.[channel], ord.[customer_no], 
                                LTRIM(ISNULL(cus.[fname],'') + ' ' + CASE WHEN ISNULL(cus.[mname],'') = '' THEN '' ELSE cus.[mname] + ' ' END + ISNULL(cus.[lname],'')),
                                CASE WHEN ord.[customer_no] = 0 THEN 'A' ELSE 'K' END,
                                CASE WHEN ord.[customer_no] = 0 THEN 'Anonymous' ELSE 'Known' END
                FROM [dbo].[T_SUB_LINEITEM] AS sli (NOLOCK)
                     INNER JOIN [dbo].[T_ORDER] AS ord (NOLOCK) ON ord.[order_no] = sli.[order_no]
                    LEFT OUTER JOIN [dbo].[T_CUSTOMER] AS cus (NOLOCK) ON cus.[customer_no] = ord.[customer_no]
                WHERE sli.[perf_no] IN (SELECT [performance_no] FROM @performance_cache WHERE [date_range] = 2) AND sli.[sli_status] IN (3, 12)

        END

    /*  If no orders found, jump to the end  */

        IF NOT EXISTS (SELECT * FROM [#trx_stat_orders]) GOTO FINISHED
    
    /*  Delete the modes of sale that were not passed to the report in the modes of sale list  */

        IF EXISTS (SELECT * FROM @mode_of_sale_table)
            DELETE FROM [#trx_stat_orders] WHERE [order_mode_of_sale] NOT IN (SELECT [mode_of_sale_no] FROM @mode_of_sale_table)

    /*  Delete the channels that were not passed to the report in the channel list  */

        IF EXISTS (SELECT * FROM @channel_table)
            DELETE FROM [#trx_stat_orders] WHERE [order_sales_channel] NOT IN (SELECT [channel_no] FROM @channel_table)

    /*  If no orders remain, jump to the end  */

        IF NOT EXISTS (SELECT * FROM [#trx_stat_orders]) GOTO FINISHED

    /*  If the customer had an active membership at the time of the order, it is counted as a member order  */

        UPDATE #trx_stat_orders SET [order_customer_type] = 'M', [order_customer_ind] = 'Member'
        WHERE [order_customer_no] IN 
            (SELECT [order_customer_no] 
             FROM [#trx_stat_orders] AS trx
                  INNER JOIN [dbo].[TX_CUST_MEMBERSHIP] AS mem (NOLOCK) ON mem.[customer_no] = trx.[order_customer_no] AND trx.[order_dt] BETWEEN mem.[init_dt] AND mem.[expr_dt])

    /*  If it is a known order attributed to the Guest User (web sales) or Kiosk Customer (kiosk sales) then make it anonymous  */
        
        UPDATE #trx_stat_orders SET [order_customer_type] = 'A', [order_customer_ind] = 'Anonymous'
        WHERE [order_customer_no] IN (SELECT DISTINCT customer_no FROM dbo.T_CUSTOMER
                                      WHERE [order_customer_name] IN ('guest user','Kiosk Customer'))

    /*  Any remaining customer who is a known customer is nonmember  */

        UPDATE #trx_stat_orders SET [order_customer_type] = 'N', [order_customer_ind] = 'NonMember'
        WHERE [order_customer_type] = 'K'

    /*  Delete null performance values  */

        DELETE FROM [#trx_stat_table] WHERE [title_no] IS NULL OR [production_no] IS null

    /*  Get order detail information using the list of orders in #trx_stat_orders  */

        INSERT INTO [#trx_stat_table]
        SELECT ord.[order_mode_of_sale], mos.[description], ord.[order_sales_channel], cha.[description], ord.[order_customer_no], [order_customer_name], 
               ord.[order_customer_type], ord.[order_customer_ind], det.[title_no], det.[title_name], det.[production_no], det.[production_name], 
               COUNT(DISTINCT ord.[order_no]), COUNT(*), SUM(det.[due_amount]), SUM(det.[paid_amount]), SUM(det.[total_paid]), 0, 0, 0.00, 0.00, 0.00, ''
        FROM [#trx_stat_orders] AS ord
             INNER JOIN @mode_of_sale_cache AS mos ON mos.[id] = ord.[order_mode_of_sale]
             INNER JOIN @sales_channel_cache AS cha ON cha.[id] = ord.[order_sales_channel]
             INNER JOIN [dbo].[LV_ORDER_DETAIL] AS det (NOLOCK) ON det.[order_no] = ord.[order_no]
        WHERE ord.[date_range] = 1
        GROUP BY ord.[order_mode_of_sale], mos.[description], ord.[order_sales_channel], cha.[description], ord.[order_customer_no], [order_customer_name], 
                 ord.[order_customer_type], ord.[order_customer_ind], det.[title_no], det.[title_name], det.[production_no], det.[production_name]

        IF EXISTS (SELECT * FROM [#trx_stat_orders] WHERE [date_range] = 2)
            INSERT INTO [#trx_stat_table]
            SELECT ord.[order_mode_of_sale], mos.[description], ord.[order_sales_channel], cha.[description], ord.[order_customer_no], [order_customer_name], 
                   ord.[order_customer_type], ord.[order_customer_ind], det.[title_no], det.[title_name], det.[production_no], det.[production_name], 
                   0, 0, 0.00, 0.00, 0.00, COUNT(DISTINCT ord.[order_no]), COUNT(*), SUM(det.[due_amount]), SUM(det.[paid_amount]), SUM(det.[total_paid]), ''
            FROM [#trx_stat_orders] AS ord
                 INNER JOIN @mode_of_sale_cache AS mos ON mos.[id] = ord.[order_mode_of_sale]
                 INNER JOIN @sales_channel_cache AS cha ON cha.[id] = ord.[order_sales_channel]
                 INNER JOIN [dbo].[LV_ORDER_DETAIL] AS det (NOLOCK) ON det.[order_no] = ord.[order_no]
            WHERE ord.[date_range] = 2
            GROUP BY ord.[order_mode_of_sale], mos.[description], ord.[order_sales_channel], cha.[description], ord.[order_customer_no], [order_customer_name], 
                     ord.[order_customer_type], ord.[order_customer_ind], det.[title_no], det.[title_name], det.[production_no], det.[production_name]

    /*  Delete the productions that were not passed to the report in the production list  */
      
        IF EXISTS (SELECT [production_no] FROM @production_table)
            DELETE FROM [#trx_stat_table] WHERE [production_no] NOT IN (SELECT [production_no] FROM @production_table)


    /*  Combine all the various kiosks sales channels into one kiosk channel to shorten the report.  */

        UPDATE [#trx_stat_table] SET [sales_channel_name] = 'Kiosk'
        WHERE [sales_channel_name] like '%Kiosk%'

    /* Delete Null performance info  */

        DELETE FROM [#trx_stat_table] WHERE [title_no] IS NULL

    /*  Create final data set.  */

        INSERT INTO [#final_data_table]
        SELECT 'A', 'Totals', '', '', '', SUM([order_count]), 0.00, SUM([counter]), 0.00, SUM([total_paid]), 0.00, SUM([order_count_2]), 0.00, SUM([counter_2]), 0.00, SUM([total_paid_2]), 0.00, '' 
        FROM [#trx_stat_table]

        SELECT @total_orders_1 = ISNULL(SUM([order_count]),0.00), @total_tickets_1 = ISNULL(SUM([counter]),0.00), @total_paid_1 = ISNULL(SUM([total_paid]),0.00) FROM [#trx_stat_table]
        SELECT @total_orders_2 = ISNULL(SUM([order_count_2]),0.00), @total_tickets_2 = ISNULL(SUM([counter_2]),0.00), @total_paid_2 = ISNULL(SUM([total_paid_2]),0.00) FROM [#trx_stat_table]
                
        IF @data_grids = '' OR EXISTS (SELECT * FROM @data_grid_table WHERE [grid_name] = 'Anonymous vs Known')
            INSERT INTO [#final_data_table]
            SELECT 'B', 'Anonymous vs Known', '', '', [order_customer_ind], SUM([order_count]), 0.00, SUM([counter]), 0.00, SUM([total_paid]), 0.00, 
                   SUM([order_count_2]), 0.00, SUM([counter_2]), 0.00, SUM([total_paid_2]), 0.00, ''
            FROM [#trx_stat_table] GROUP BY [order_customer_ind]

        IF @data_grids = '' OR EXISTS (SELECT * FROM @data_grid_table WHERE [grid_name] = 'Sales by Mode of Sale')
            INSERT INTO [#final_data_table]
            SELECT 'C', 'Sales by Mode of Sale', '', '', [mode_of_sale_name], SUM([order_count]), 0.00, SUM([counter]), 0.00, SUM([total_paid]), 0.00, 
                   SUM([order_count_2]), 0.00, SUM([counter_2]), 0.00, SUM([total_paid_2]), 0.00, ''
            FROM [#trx_stat_table] GROUP BY [mode_of_sale_name]

        IF @data_grids = '' OR EXISTS (SELECT * FROM @data_grid_table WHERE [grid_name] = 'Sales by Channel')
            INSERT INTO [#final_data_table]
            SELECT 'D', 'Sales by Channel', '', '', [sales_channel_name], SUM([order_count]), 0.00, SUM([counter]), 0.00, SUM([total_paid]), 0.00, 
                   SUM([order_count_2]), 0.00, SUM([counter_2]), 0.00, SUM([total_paid_2]), 0.00, ''
            FROM [#trx_stat_table] GROUP BY [sales_channel_name]

        IF @data_grids = '' OR EXISTS (SELECT * FROM @data_grid_table WHERE [grid_name] = 'Sales by Venue')
            INSERT INTO [#final_data_table] 
            SELECT 'E', 'Sales by Venue', '', [title_name], [order_customer_ind], SUM([order_count]), 0.00, SUM([counter]), 0.00, SUM([total_paid]), 0.00, 
                   SUM([order_count_2]), 0.00, SUM([counter_2]), 0.00, SUM([total_paid_2]), 0.00, ''
            FROM [#trx_stat_table] GROUP BY [title_name], [order_customer_ind]

        IF @data_grids = '' OR EXISTS (SELECT * FROM @data_grid_table WHERE [grid_name] = 'Sales by Venue/MOS')
            INSERT INTO [#final_data_table] 
            SELECT 'F', 'Sales by Venue/Mode of Sale', '', [title_name], [mode_of_sale_name], SUM([order_count]), 0.00, SUM([counter]), 0.00, SUM([total_paid]), 0.00, 
                   SUM([order_count_2]), 0.00, SUM([counter_2]), 0.00, SUM([total_paid_2]), 0.00, ''
            FROM [#trx_stat_table] GROUP BY [title_name], [mode_of_sale_name]

        IF @data_grids = '' OR EXISTS (SELECT * FROM @data_grid_table WHERE [grid_name] = 'Sales by Venue/Channel')
            INSERT INTO [#final_data_table] 
            SELECT 'G', 'Sales by Venue/Channel', '', [title_name], [sales_channel_name], SUM([order_count]), 0.00, SUM([counter]), 0.00, SUM([total_paid]), 0.00, 
                   SUM([order_count_2]), 0.00, SUM([counter_2]), 0.00, SUM([total_paid_2]), 0.00, ''
            FROM [#trx_stat_table] GROUP BY [title_name], [sales_channel_name]

        IF @data_grids = '' OR EXISTS (SELECT * FROM @data_grid_table WHERE [grid_name] = 'Sales by Production')
            INSERT INTO [#final_data_table] 
            SELECT 'H', 'Sales by Production Name', [title_name], [production_name], [order_customer_ind], SUM([order_count]), 0.00, SUM([counter]), 0.00, SUM([total_paid]), 0.00, 
                   SUM([order_count_2]), 0.00, SUM([counter_2]), 0.00, SUM([total_paid_2]), 0.00, ''
            FROM [#trx_stat_table] GROUP BY [title_name], [production_name], [order_customer_ind]
           
        IF @data_grids = '' OR EXISTS (SELECT * FROM @data_grid_table WHERE [grid_name] = 'Sales by Production/MOS')
            INSERT INTO [#final_data_table]
            SELECT 'I', 'Sales by Production Name/Mode of Sale', [title_name], [production_name], [mode_of_sale_name], SUM([order_count]), 0.00, SUM([counter]), 0.00, SUM([total_paid]), 0.00, 
                   SUM([order_count_2]), 0.00, SUM([counter_2]), 0.00, SUM([total_paid_2]), 0.00, ''
            FROM [#trx_stat_table] GROUP BY [title_name], [production_name], [mode_of_sale_name]

        IF @data_grids = '' OR EXISTS (SELECT * FROM @data_grid_table WHERE [grid_name] = 'Sales by Production/Channel')
            INSERT INTO [#final_data_table] 
            SELECT 'J', 'Sales by Production Name/Channel', [title_name], [production_name], [sales_channel_name], SUM([order_count]), 0.00, SUM([counter]), 0.00, SUM([total_paid]), 0.00, 
                   SUM([order_count_2]), 0.00, SUM([counter_2]), 0.00, SUM([total_paid_2]), 0.00, ''
            FROM [#trx_stat_table] GROUP BY [title_name], [production_name], [sales_channel_name]

    /*  Determine Percentages  */

        IF @total_orders_1 > 0
            UPDATE [#final_data_table] SET [orders_pct_1] = (CONVERT(DECIMAL(18,2),[orders_1]) / @total_orders_1)
        IF @total_tickets_1 > 0
            UPDATE [#final_data_table] SET [tickets_pct_1] = (CONVERT(DECIMAL(18,2),[tickets_1]) / @total_tickets_1)
        IF @total_paid_1 > 0
            UPDATE [#final_data_table] SET [paid_pct_1] = (CONVERT(DECIMAL(18,2),[paid_1]) / @total_paid_1)
        
        IF @total_orders_2 > 0
            UPDATE [#final_data_table] SET [orders_pct_2] = (CONVERT(DECIMAL(18,2),[orders_2]) / @total_orders_2)
        IF @total_tickets_2 > 0
            UPDATE [#final_data_table] SET [tickets_pct_2] = (CONVERT(DECIMAL(18,2),[tickets_2]) / @total_tickets_2)
        IF @total_paid_2 > 0
            UPDATE [#final_data_table] SET [paid_pct_2] = (CONVERT(DECIMAL(18,2),[paid_2]) / @total_paid_2)
       
    FINISHED:

        /*  If no records, add a single record with a "No Records Found" message  */

            --IF NOT EXISTS (SELECT * FROM [#trx_stat_table]) BEGIN
            --    IF @rpt_message = '' SELECT @rpt_message = 'No records found for the criteria you entered'
            --    INSERT INTO #trx_stat_table VALUES  (0, '', 0, '', 0, '', '', '', 0, '', 0, '', 0, 1, 0.00, 0.00, 0.00, 0, 0, 0.00, 0.00, 0.00, @rpt_message)
            --END
            IF NOT EXISTS (SELECT * FROM [#final_data_table]) BEGIN
                IF @rpt_message = '' SELECT @rpt_message = 'No records found for the criteria you entered'
                INSERT INTO [#final_data_table] VALUES ('',  '', '', '', '', 0, 0.00, 0, 0.00, 0.00, 0.00, 0, 0.00, 0, 0.00, 0.00, 0.00, @rpt_message)
            END

        /*  Pull final data set to be passed back to the report  */

            SELECT    [category_letter]
                    , [category] 
                    , [subcategory_1] 
                    , [subcategory_2] 
                    , [subcategory_3] 
                    , [orders_1]
                    , [orders_pct_1] 
                    , [tickets_1] 
                    , [tickets_pct_1]
                    , [paid_1] 
                    , [paid_pct_1]
                    , [orders_2]
                    , [orders_pct_2]
                    , [tickets_2] 
                    , [tickets_pct_2]
                    , [paid_2] 
                    , [paid_pct_2]
                    , [rpt_message]
            FROM [#final_data_table]
            ORDER BY [category_letter], [category], [subcategory_1], [subcategory_2], [subcategory_3]

            --SELECT   [order_mode_of_sale]
            --       , [mode_of_sale_name]
            --       , [order_sales_channel]
            --       , [sales_channel_name]
            --       , [order_customer_type]
            --       , [order_customer_ind]
            --       , [title_no]
            --       , [title_name]
            --       , [production_no]
            --       , [production_name]
            --       , SUM([order_count]) AS 'order_count_1'
            --       , SUM([counter]) AS 'visitor_count_1'
            --       , SUM([due_amount]) AS 'due_amount_1'
            --       , SUM([paid_amount]) AS 'paid_amount_1'
            --       , SUM([total_paid]) AS 'total_paid_1'
            --       , SUM([order_count_2]) AS 'order_count_2'
            --       , SUM([counter_2]) AS 'visitor_count_2'
            --       , SUM([due_amount_2]) AS 'due_amount_2'
            --       , SUM([paid_amount_2]) AS 'paid_amount_2'
            --       , SUM([total_paid_2]) AS 'total_paid_2'
            --       , [rpt_message]
            --FROM [#trx_stat_table]
            --GROUP BY [order_mode_of_sale], [mode_of_sale_name], [order_sales_channel], [sales_channel_name],
            --         [order_customer_type], [order_customer_ind], [title_no], [title_name], [production_no], [production_name], [rpt_message]

    DONE:

        /*  Cleanup  */

            IF OBJECT_ID('tempdb..#final_data_table') IS NOT NULL DROP TABLE [#trx_stat_table]
            IF OBJECT_ID('tempdb..#trx_stat_table') IS NOT NULL DROP TABLE [#trx_stat_table]
            IF OBJECT_ID('tempdb..#trx_stat_orders') IS NOT NULL DROP TABLE [#trx_stat_orders]

END
GO

GRANT EXECUTE ON [dbo].[LRP_SALES_STATS] TO ImpUsers
GO

--EXECUTE [dbo].[LRP_SALES_STATS] 
--          @report_start_dt = '1-1-2017'
--        , @report_end_dt = '1-31-2017'
--        , @report_start_dt_2 = '12-1-2016'
--        , @report_end_dt_2 = '12-31-2017'
--        , @date_type = 'Performance Date' 
--        , @title_str = '173, 161'
--        --, @production_str = '1201,15268,18916,1203,1392,11943,1207,1208'
--        , @production_str = '1201,15268,18916,1203,1392,11943,1207,1208,18766,1210,15276,1211,1222,1223,1292,1224,1403,1397,11942'
--        , @mode_of_sale_str = '3,7,10,11,12,15,17,23,28,29,30,35,36'
--        , @channel_str = '6,7,20,21'
--        , @data_grids = ''
--        , @data_grids = 'Anonymous vs Known,Sales by Venue/Channel,Sales by Production Name/Channel'
--        --, @title_str = NULL
--        --, @production_str = NULL
--        --, @mode_of_sale_str = null

--SELECT @report_start_dt = '4-1-2017', @report_end_dt = '4-30-2017', @date_type = 'Performance Date'
--SELECT @report_start_dt_2 = '3-1-2017', @report_end_dt_2 = '3-31-2017'
--SELECT @title_str = '',
--       @production_str = '',
--       @mode_of_sale_str = ''

--SELECT * FROM dbo.TR_SALES_CHANNEL

