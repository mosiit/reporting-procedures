USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_MEMBERSHIP_PROGRESS_REPRORT]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_MEMBERSHIP_PROGRESS_REPRORT] AS' 
END
GO

ALTER PROCEDURE [dbo].[LRP_MEMBERSHIP_PROGRESS_REPRORT]
    @month_FY_YTD_Calc VARCHAR(15) = 'FY',
	@requestDt DATETIME = NULL,
    @get_previous_year CHAR(1) = 'N' 
AS BEGIN

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    SET ANSI_WARNINGS OFF
    SET NOCOUNT ON

    DECLARE @start_dt_range DATETIME,
            @end_dt_range DATETIME

    DECLARE @fiscal_year VARCHAR(4);
    DECLARE @previous_year_dt DATETIME;

    DECLARE @campaignTbl TABLE ([campaign_no] INT NOT NULL,
	                            [RevenueGoal] DECIMAL(18,2),
	                            [CountGoal] INT);

    DECLARE @previous_year TABLE ([campaign_no] INT, 
                                  [campaignName] VARCHAR(30),
                                  [campaignAbbrev] VARCHAR(30), 
                                  [RevenueGoal] DECIMAL(18,2), 
                                  [CountGoal] INT, 
                                  [start_dt] DATETIME, 
                                  [end_dt] DATETIME, 
                                  [totalAmt] DECIMAL(18,2), 
                                  [totMembs] INT,
                                  [LYcampaign_no] INT, 
                                  [LYcampaignName] VARCHAR(30),
                                  [LYcampaignAbbrev] VARCHAR(30),
                                  [LYRevenueGoal] DECIMAL(18,2), 
                                  [LYCountGoal] INT, 
                                  [LYstart_dt] DATETIME, 
                                  [LYend_dt] DATETIME, 
                                  [LYtotalAmt] DECIMAL(18,2), 
                                  [LYtotMembs] INT);

    SELECT @fiscal_year = CAST(MAX([fyear]) AS VARCHAR(4)) FROM [dbo].[TR_Batch_Period] WHERE @requestDt BETWEEN [start_dt] AND [end_dt];

    IF @get_previous_year = 'Y' BEGIN

        SELECT @previous_year_dt = DATEADD(YEAR,-1,@requestDt);

        INSERT INTO @previous_year
        EXECUTE [dbo].[LRP_MEMBERSHIP_PROGRESS_REPRORT] @month_FY_YTD_Calc, @previous_year_dt, 'N';
    
    END;
        
	INSERT INTO @campaignTbl (campaign_no, RevenueGoal, CountGoal)
	SELECT [campaign_no], 0.0, 0 FROM [dbo].[T_CAMPAIGN] WHERE [description] IN ('Household Memb FY' + @fiscal_year,'Library Memb FY' + @fiscal_year);

    IF @month_FY_YTD_Calc = 'FY' BEGIN

        --
        -- MEMBERSHIP WANTS TO GET THE GOALS FOR THE ENTIRE FY BUT THEY WANT THE NUMBERS/CALCULATIONS TO BE BASED ON THE REPORT DATE PARAMETER!!!!!!!
        --
	    --GET DATES FOR ENTIRE FY IN ORDER TO GET THE GOALS. 
	    SELECT @start_dt_range = MIN([start_dt]), @end_dt_range = MAX([end_dt])
	    FROM [dbo].[TR_BATCH_PERIOD] 
	    WHERE [inactive] = 'n' AND [fyear] = (SELECT [fyear] FROM [dbo].[TR_BATCH_Period] WHERE @requestDt BETWEEN [start_dt] AND [end_dt]);

	    WITH goals
	    AS 
	    (
		    SELECT c.campaign_no, 
				SUM(base_revenue) + SUM(ISNULL(g.supp_revenue,0)) AS MRevenue, 
				SUM(base_count) + SUM(ISNULL(g.supp_count,0)) AS MCount
		    FROM LTR_MEMBERSHIP_GOALS g
		    INNER JOIN @campaignTbl c
			    ON g.campaign_no = c.campaign_no
		    WHERE DATEDIFF(d,g.start_dt, @start_dt_range) <= 0 AND DATEDIFF(d,g.end_dt, @end_dt_range) >= 0 	
		    GROUP BY c.campaign_no
	    )
	    UPDATE c 
	    SET RevenueGoal = g.MRevenue, CountGoal = g.MCount
	    FROM goals g
	    INNER JOIN @campaignTbl c
		    ON g.campaign_no = c.campaign_no

	    --GET DATES BASED ON REPORT DATE PARAMETER IN ORDER TO CALCULATE AMOUNTS RECEIVED
	    SELECT @start_dt_range = MIN(start_dt)
	    FROM TR_BATCH_PERIOD 
	    WHERE inactive = 'n' AND fyear = (SELECT fyear FROM dbo.TR_BATCH_Period WHERE @requestDt BETWEEN start_dt AND end_dt);

	    SELECT @end_dt_range = DATEADD(s,-1,DATEADD(mm, DATEDIFF(m, 0, @requestDt)+1,0));
	
    END ELSE IF @month_FY_YTD_Calc = 'Month' BEGIN

	    SELECT @start_dt_range = DATEADD(month, DATEDIFF(month, 0, @requestDt), 0)
	    SELECT @end_dt_range = DATEADD(s,-1,DATEADD(mm, DATEDIFF(m, 0, @requestDt)+1,0))

    	UPDATE c
	    SET RevenueGoal = base_revenue + ISNULL(g.supp_revenue,0), 
			CountGoal = base_count + ISNULL(g.supp_count,0) 
    	FROM LTR_MEMBERSHIP_GOALS g
	    	INNER JOIN @campaignTbl c
		    ON g.campaign_no = c.campaign_no
	       AND DATEDIFF(d,g.start_dt, @start_dt_range) = 0 AND DATEDIFF(d,g.end_dt, @end_dt_range) = 0 

    END ELSE IF @month_FY_YTD_Calc = 'YTD' BEGIN

	    SELECT @start_dt_range = MIN(start_dt)
	    FROM TR_BATCH_PERIOD 
	    WHERE inactive = 'n' AND fyear = (SELECT fyear FROM dbo.TR_BATCH_Period WHERE @requestDt BETWEEN start_dt AND end_dt);

	    SELECT @end_dt_range = DATEADD(s,-1,DATEADD(mm, DATEDIFF(m, 0, @requestDt)+1,0));

    	WITH goals
	    AS 
	    (
    		SELECT c.campaign_no, 
				SUM(base_revenue) + SUM(ISNULL(g.supp_revenue,0)) AS MRevenue, 
				SUM(base_count) + SUM(ISNULL(g.supp_count,0)) AS MCount
	    	FROM LTR_MEMBERSHIP_GOALS g
		    INNER JOIN @campaignTbl c
			    ON g.campaign_no = c.campaign_no
    		WHERE DATEDIFF(d,g.start_dt, @start_dt_range) <= 0 AND DATEDIFF(d,g.end_dt, @end_dt_range) >= 0 	
	    	GROUP BY c.campaign_no
	    )
	    UPDATE c 
	    SET RevenueGoal = g.MRevenue, CountGoal = g.MCount
	    FROM goals g
	    INNER JOIN @campaignTbl c
    		ON g.campaign_no = c.campaign_no
	
    END;

    WITH campaignCounts (campaign_no, RevenueGoal, CountGoal, ticket_revenue, ticket_count)
    AS 
    (
    	-- code (before inline view) 
	    --borrowed from Campaigns > Reports > Monthly Revenue Report for Campaign (from Profiler)
    	SELECT 
		t.campaign_no,
    		tbl.RevenueGoal,
	    	tbl.CountGoal,
		    ticket_revenue = SUM(ISNULL(trn_amt, 0)),
    		ticket_count = SUM(X.seats_pur) 
	    FROM T_TRANSACTION t
		    INNER JOIN T_ORDER o
			    ON t.order_no = o.order_no
    		INNER JOIN @campaignTbl tbl
	    		ON t.campaign_no = tbl.campaign_no
		    LEFT JOIN 
		    (
		     SELECT li.order_no, count(*) seats_pur
		     FROM dbo.T_SUB_LINEITEM li
		     INNER JOIN dbo.T_PERF p
			    ON p.perf_no = li.perf_no
		    INNER JOIN @campaignTbl tbl
			    ON p.campaign_no = tbl.campaign_no
    		WHERE li.sli_status IN (3,12)
              AND li.zone_no NOT IN (SELECT [zone_no] FROM [dbo].[T_ZONE] 
                                     WHERE [zmap_no] IN (27, 49) AND LEFT([description],4) IN ('Unas','upgr','temp','Lost','Lega'))
		    GROUP BY li.order_no
		    ) X
		    ON X.order_no = o.order_no
	    WHERE  CAST(o.order_dt AS DATE) BETWEEN @start_dt_range AND @end_dt_range
		    AND t.trn_type IN (31, 32, 33, 51, 52, 53) 
    	GROUP BY t.campaign_no, tbl.RevenueGoal, tbl.CountGoal
    )
    SELECT cc.campaign_no, 
           ca.description AS campaignName,
           REPLACE(ca.description,@fiscal_year,'') AS campaignAbbrev,  
	       cc.RevenueGoal RevenueGoal, 
           cc.CountGoal CountGoal, 
           @start_dt_range AS start_dt, 
           @end_dt_range AS end_dt,
	       cc.ticket_revenue AS totalAmt, 
           cc.ticket_count AS totMembs,
           ISNULL(py.campaign_no, 0) AS [LYcampaign_no], 
           ISNULL(py.campaignName, '') AS [LYcampaignname],
           ISNULL(py.campaignAbbrev, '') AS [LYcampaignAbbrev],
           ISNULL(py.RevenueGoal, 0) AS [LYRevenueGoal], 
           ISNULL(py.CountGoal, 0) AS [LYCountGoal], 
           py.[start_dt] AS [LYstart_dt], 
           py.[end_dt] AS [LYend_dt],
    	   ISNULL(py.[totalAmt], 0) AS [LYtotalamt], 
           ISNULL(py.[totMembs], 0) AS [LYtotMembs]
    FROM campaignCounts cc
    LEFT JOIN dbo.T_CAMPAIGN ca ON cc.campaign_no = ca.campaign_no
    LEFT JOIN @previous_year AS py ON py.campaignAbbrev = REPLACE(ca.description,@fiscal_year,'')

    DONE:

END
GO

GRANT EXECUTE ON [dbo].[LRP_MEMBERSHIP_PROGRESS_REPRORT] TO ImpUsers
GO

--EXECUTE [dbo].[LRP_MEMBERSHIP_PROGRESS_REPRORT] @month_FY_YTD_Calc = 'FY', @requestDt = '6-30-2018', @get_previous_year = 'Y' 

