USE [impresario]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[LRP_GIFT_NOTIFICATION_OUTSIDE_DMS]
(
	@start_dt DATETIME,
	@end_dt DATETIME
)
AS

SET NOCOUNT ON  
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED 

-- This SP is called from the Gift Notification report that only has one date parameter: step_dt. AP_NEW_CONT_REPORT needs a start and an end date.
-- Zero out time for @start_dt AND make it for transactions made one week prior
-- Make @end_dt the last possible time for given @end_dt
SELECT @start_dt = DATEADD(WEEK, -1, DATEADD(dd, DATEDIFF(dd, 0, @start_dt), 0))
SELECT @end_dt = CAST(CONVERT(CHAR(8), @end_dt, 112) + ' 23:59:59.99' AS DATETIME)


DECLARE @work1 TABLE 
(
	customer_no INT,
	ref_no INT,
	cust_name VARCHAR(160),
	cont_dt DATETIME,
	cont_type CHAR(1),
	cont_amt MONEY,
	cont_fund VARCHAR(30),
	designation VARCHAR(30) NULL,
	plan_primary_worker CHAR(8) NULL,
	cont_worker CHAR(8) NULL,
	state VARCHAR(20) NULL,
	memb_level CHAR(3) NULL,
	memb_trend VARCHAR(30) NULL,
	nrr_status CHAR(2) NULL,
	letter_desc VARCHAR(30) NULL,
	expr_dt DATETIME NULL,
	sort_name VARCHAR(55),
	initiator_no INT NULL,
	initiator_name VARCHAR(160) NULL,
	creditee_no INT NULL,
	creditee_name VARCHAR(160) NULL,
	credit_amt MONEY,
	credit_type_desc VARCHAR(30)
)

INSERT INTO @work1
EXEC dbo.AP_NEW_CONT_REPORT 
	@start_dt = @start_dt, 
	@end_dt = @end_dt, 
	@fund_str = NULL, 
	@type_str = NULL, 
	@list_no = 0, 
	@list_includes_owner_ind = 'N',
	@list_includes_initiator_ind = 'N',
	@list_includes_creditee_ind = 'N',
	@creditee_str = NULL

SELECT w.customer_no,
       w.ref_no,
       w.cust_name,
       w.cont_dt,
       w.cont_type,
       w.cont_amt,
       w.cont_fund,
       w.designation,
       w.plan_primary_worker,
       w.cont_worker,
       w.state,
       w.memb_level,
       w.memb_trend,
       w.nrr_status,
       w.letter_desc,
       w.expr_dt,
       w.sort_name,
       w.initiator_no,
       w.initiator_name 
FROM @work1 w
	INNER JOIN V_CUSTOMER_WITH_PRIMARY_GROUP vc
		ON w.customer_no = vc.customer_no
	INNER JOIN VS_CONTRIBUTION_WITH_INITIATOR a1
		ON vc.expanded_customer_no = a1.customer_no
		AND w.ref_no = a1.ref_no
WHERE 
	a1.created_by not in (select userid from TX_USER_GROUP where UG_id in ('DMS-ADMN', 'DMS-DATA'))
    AND a1.cont_amt >= 25.00
    AND a1.cont_dt >= DATEADD(WEEK, -1, @start_dt)
	AND vc.customer_no <> 2653093 -- Stewardship Soft Credit
	AND ISNULL(vc.inactive, 1) = 1
ORDER BY w.cust_name

