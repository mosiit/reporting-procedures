USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LRP_ADMITS_CASHIER_RPT_MEMBERSHIPS]    Script Date: 5/9/2016 4:40:06 PM ******/
DROP PROCEDURE [dbo].[LRP_ADMITS_CASHIER_RPT_MEMBERSHIPS]
GO

/****** Object:  StoredProcedure [dbo].[LRP_ADMITS_CASHIER_RPT_MEMBERSHIPS]    Script Date: 5/9/2016 4:40:06 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE  [dbo].[LRP_ADMITS_CASHIER_RPT_MEMBERSHIPS]
       @rpt_dt DATETIME,
       @rpt_operator VARCHAR(8),
	   @recon_breakdown CHAR(1)
AS
	BEGIN

		SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

			DECLARE	@basic_sold	INT,
					@basic_one	INT,
					@premier_sold INT,
					@premier_one INT

		DECLARE @membership_table TABLE ([membership_date] CHAR(10),
										 [membership_operator] VARCHAR(8),
										 [operator_first] VARCHAR(50),
										 [operator_last] VARCHAR(50),
										 [operator_location] VARCHAR(50),
										 [membership_type] VARCHAR(30),
										 [total_memberships] INT,
										 [total_with_one_step] INT,
										 [elapsed_time] INT,
										 [rpt_message] VARCHAR(100))

		DECLARE	@tblRtn TABLE	(
								[membership_type] VARCHAR(30),
								[basic_sold] INT,
								[basic_one]  INT,
								[premier_sold] INT,
								[premier_one]  INT
								)

		INSERT INTO @membership_table
			EXEC [dbo].[LP_RPT_CASHOUT_MEMBERSHIPS] @rpt_dt, @rpt_operator

	IF @recon_breakdown = 'B'
		BEGIN
		
			INSERT INTO @tblRtn
				SELECT	[membership_type], 
						SUM([total_memberships]) AS [total_memberships],
						SUM([total_with_one_step]) AS [total_with_one_step],
						0,
						0
				FROM	@membership_table 
				WHERE	PATINDEX('%basic%', [membership_type]) > 0
				OR		PATINDEX('%premier%', [membership_type]) > 0
				GROUP BY [membership_type]
		END
	ELSE
		BEGIN

			SELECT	@basic_sold = SUM([total_memberships])
			FROM	@membership_table 
			WHERE	PATINDEX('%basic%', [membership_type]) > 0

			SELECT	@basic_one= SUM([total_with_one_step])
			FROM	@membership_table 
			WHERE	PATINDEX('%basic%', [membership_type]) > 0

			SELECT	@premier_sold = SUM([total_memberships])
			FROM	@membership_table 
			WHERE	PATINDEX('%premier%', [membership_type]) > 0

			SELECT	@premier_one= SUM([total_with_one_step])
			FROM	@membership_table 
			WHERE	PATINDEX('%premier%', [membership_type]) > 0
			
			INSERT INTO @tblRtn
				SELECT	'Recon', @basic_sold, @basic_one, @premier_sold, @premier_one

		END

	END

	SELECT * FROM @tblRtn
GO

GRANT EXECUTE ON [dbo].[LRP_ADMITS_CASHIER_RPT_MEMBERSHIPS] TO impusers
GO
