USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_PERFORMANCE_SCHEDULE]') AND type IN (N'P', N'PC'))
BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_PERFORMANCE_SCHEDULE] AS' 
    EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LRP_PERFORMANCE_SCHEDULE] TO impusers'
END
GO

ALTER PROCEDURE [dbo].[LRP_PERFORMANCE_SCHEDULE]
        @report_start_dt DATETIME = NULL,
        @report_end_dt DATETIME = NULL,
        @title_str VARCHAR(4000) = NULL,
        @production_str VARCHAR(4000) = NULL,
        @sort_by VARCHAR(25) = 'Production'
WITH RECOMPILE AS BEGIN

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    SET NOCOUNT ON;

    /*  Report Variables  */

        DECLARE @title_list TABLE ([title_no] INT);
        DECLARE @production_list TABLE ([production_no] INT);
                               
        DECLARE @conflicts TABLE  ([perf_no] INT NOT NULL DEFAULT (0),
                                   [zone_no] INT NOT NULL DEFAULT (0),
                                   [title_name] VARCHAR(30) NOT NULL DEFAULT (''),
                                   [perf_dt] DATETIME NULL,
                                   [perf_date] CHAR(10) NOT NULL DEFAULT (''),
                                   [perf_time] VARCHAR(10) NOT NULL DEFAULT (''),
                                   [perf_time_display] VARCHAR(15) NOT NULL DEFAULT (''),
                                   [end_time] VARCHAR(10) NOT NULL DEFAULT (''), 
                                   [prod_no] INT NOT NULL DEFAULT (0),
                                   [prod_name] VARCHAR(30) NOT NULL DEFAULT (''),
                                   [conflict_perf_no] INT NOT NULL DEFAULT (0),
                                   [conflict_zone_no] INT NOT NULL DEFAULT (0),
                                   [conflict_title_name] VARCHAR(30) NOT NULL DEFAULT (''),
                                   [conflict_perf_dt] DATETIME NULL,
                                   [conflict_perf_date] CHAR(10) NOT NULL DEFAULT (''),
                                   [conflict_perf_time] VARCHAR(10) NOT NULL DEFAULT (''),
                                   [conflict_perf_time_display] VARCHAR(15) NOT NULL DEFAULT (''),
                                   [conflict_end_time] VARCHAR(10) NOT NULL DEFAULT (''),
                                   [conflict_prod_no] INT NOT NULL DEFAULT (0),
                                   [conflict_prod_name] VARCHAR(30) NOT NULL DEFAULT (''))

    /*  Check Parameters  */

        SELECT @report_start_dt = CAST(@report_start_dt AS DATE),
               @report_end_dt = CONVERT(CHAR(10),@report_end_dt,111) + ' 23:59:59.957';

        IF ISNULL(@title_str,'') = ''
            INSERT INTO @title_list ([title_no])
            SELECT [title_no] FROM [dbo].[T_TITLE]
        ELSE
            INSERT INTO @title_list ([title_no])
            SELECT CAST(Element AS INT) FROM [dbo].[FT_SPLIT_LIST](REPLACE(@title_str,'"',''),',');

        IF ISNULL(@production_str,'') = ''
            INSERT INTO @production_list ([production_no])
            SELECT [prod_no] FROM [dbo].[T_PRODUCTION]
        ELSE
            INSERT INTO @production_list ([production_no])
            SELECT CAST(Element AS INT) FROM [dbo].[FT_SPLIT_LIST](REPLACE(@production_str,'"',''),',');


    /*  Pull all the performance information where two or more performances are scheduled in the same title at the same time  */

        INSERT INTO @conflicts ([perf_no],[zone_no],[title_name],[perf_dt],[perf_date],[perf_time],[perf_time_display],[end_time],[prod_no],[prod_name],
                                  [conflict_perf_no],[conflict_zone_no],[conflict_title_name],[conflict_perf_dt],[conflict_perf_date],[conflict_perf_time],
                                  [conflict_perf_time_display],[conflict_end_time],[conflict_prod_no],[conflict_prod_name])
            SELECT prf.[performance_no],
                   ISNULL(prf.[performance_zone],0),
                   prf.[title_name], 
                   prf.[performance_dt],
                   prf.[performance_date], 
                   prf.[performance_time],
                   prf.[performance_time_display],
                   prf.[performance_end_time],
                   prf.[production_no],
                   prf.[production_name],
                   cfl.[performance_no],
                   ISNULL(cfl.[performance_zone],0),
                   cfl.[title_name], 
                   cfl.[performance_dt],
                   cfl.[performance_date], 
                   cfl.[performance_time],
                   cfl.[performance_time_display],
                   cfl.[performance_end_time],
                   cfl.[production_no],
                   cfl.[production_name]
            FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] AS prf
                 INNER JOIN @title_list AS ttl ON ttl.[title_no] = prf.[title_no]
                 INNER JOIN @production_list AS pro ON pro.[production_no] = prf.[production_no]
                 LEFT OUTER JOIN [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] AS cfl ON cfl.performance_date = prf.performance_date 
                                                                                   AND cfl.title_no = prf.title_no 
                                                                                   AND cfl.performance_time BETWEEN prf.performance_time AND prf.performance_end_time
                                                                                   AND cfl.performance_no <> prf.performance_no
            WHERE prf.performance_dt BETWEEN @report_start_dt AND @report_end_dt 
              AND prf.[title_no] NOT IN (27,1343)       --27 = Exhibit Halls / 1343 = Thrill Ride 360
              AND ISNULL(cfl.[performance_no],0) > 0

    FINISHED: 
            
        /*  Pull recordset for designated date range and title list - combine with conflicts found to indicate which performances have conflicts  */

            SELECT prf.[performance_no],
                   prf.[performance_zone], 
                   prf.[performance_dt], 
                   prf.[performance_date],
                   prf.[performance_time],
                   prf.[performance_time_display],
                   prf.[performance_end_time],
                   prf.[performance_location],
                   prf.[performance_code],
                   prf.[performance_name],
                   prf.[production_no],
                   prf.[production_name],
                   prf.[title_no],
                   prf.[title_name],
                   prf.[performance_avail_sale_ind],
                   prf.[performance_type_name],
                   prf.[performance_status_name],
                   CASE @sort_by
                        WHEN 'Production' THEN prf.[production_name]
                        ELSE prf.[performance_date] END AS [sort_01],

                   CASE @sort_by
                        WHEN 'Production' THEN prf.[performance_date]
                        ELSE prf.[production_name] END AS [sort_02],
                    '' AS [sort_03],
                    ISNULL(cfl.[conflict_perf_no],0) AS [conflict_perf_no],
                    ISNULL(cfl.[conflict_zone_no],0) AS [conflict_zone_no],
                    cfl.[title_name] AS [conflict_title_name],
                    cfl.[perf_dt] AS [conflict_perf_dt], 
                    ISNULL(cfl.[conflict_perf_date],'') AS [conflict_perf_date],
                    ISNULL(cfl.[conflict_perf_time],'') AS [conflict_perf_time],
                    ISNULL(cfl.[conflict_perf_time_display],'') AS [conflict_perf_time_display],
                    ISNULL(cfl.[conflict_end_time],'') AS [conflict_end_time],
                    ISNULL(cfl.[conflict_prod_no],'') AS [conflict_prod_no],
                    ISNULL(cfl.[conflict_prod_name],'') AS [conflict_prod_name]
            FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] AS prf
                 INNER JOIN @title_list AS ttl ON ttl.[title_no] = prf.[title_no]
                 INNER JOIN @production_list AS pro ON pro.[production_no] = prf.[production_no]
                 LEFT OUTER JOIN @conflicts AS cfl ON cfl.[title_name] = prf.[title_name]
                                                  AND cfl.[perf_date] = prf.[performance_date]
                                                  AND cfl.[perf_time] = prf.[performance_time]
                                                  AND cfl.[conflict_perf_no] <> prf.[performance_no]
            WHERE prf.performance_dt BETWEEN @report_start_dt AND @report_end_dt 
        
    DONE:

END
GO

--EXECUTE [dbo].[LRP_PERFORMANCE_SCHEDULE] @report_start_dt = '4-22-2019',
--                                         @report_end_dt = '4-22-2019',
--                                         @title_str = '',--"1395","1398"',
--                                         @production_str = '',
--                                         @sort_by = 'Production'

