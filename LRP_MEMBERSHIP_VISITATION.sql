USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LRP_MEMBERSHIP_VISITATION]    Script Date: 4/18/2019 8:52:41 AM ******/
DROP PROCEDURE [dbo].[LRP_MEMBERSHIP_VISITATION]
GO

/****** Object:  StoredProcedure [dbo].[LRP_MEMBERSHIP_VISITATION]    Script Date: 4/18/2019 8:52:41 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






CREATE PROCEDURE [dbo].[LRP_MEMBERSHIP_VISITATION]
(
	@MembLevel_str VARCHAR(MAX) = NULL,
	@NRR_str VARCHAR(MAX) = NULL,
	@init_dt_start DATETIME = NULL
)
AS

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
SET NOCOUNT ON

--DECLARE @init_dt_end DATETIME = DATEADD(YEAR, 1, @init_dt_start)
-- H. Sheridan, 20190419 - Scot requested dropping the 13th month.  Easiest done via replacing YEAR with MONTH in assignment.
DECLARE @init_dt_end DATETIME = DATEADD(MONTH, 11, @init_dt_start)

DECLARE @fy INT
DECLARE @fystart DATETIME
DECLARE @fyend DATETIME

----------- POPULATE TABLE TO HOLD THE NRR STATUS ----------- 
-- String from Tess application comes delimited with double quotes and commas. 
-- Removing double quotes
SET @NRR_str = REPLACE(@NRR_str,'"', '')

CREATE TABLE #Nrr (
	NRR_str VARCHAR(2)
)

IF @NRR_str IS NULL
	INSERT INTO #Nrr (NRR_str)
	SELECT description FROM dbo.TR_GOOESOFT_DROPDOWN WHERE code = 31
ELSE 
	INSERT INTO #Nrr  (NRR_str)
	SELECT CONVERT(VARCHAR(2),Element) FROM dbo.FT_SPLIT_LIST (@NRR_str,',')

--select 'debug - nrr', * from #Nrr

----------- POPULATE TABLE TO HOLD THE MEMBERSHIP LEVEL ----------- 
-- String from Tess application comes delimited with double quotes and commas. 
-- Removing double quotes
SET @MembLevel_str = REPLACE(@MembLevel_str, '"', '')

CREATE TABLE #MembLevel (
	MembLevel_str VARCHAR(3)
)

IF @MembLevel_str IS NULL 
	INSERT INTO #MembLevel (MembLevel_str)
	SELECT memb_level FROM dbo.T_MEMB_LEVEL WHERE inactive = 'N'
ELSE 
	INSERT INTO #MembLevel (MembLevel_str)
	SELECT CONVERT(VARCHAR(3),Element) FROM dbo.FT_SPLIT_LIST(@MembLevel_str,',')

--select 'debug - memblevel', * from #MembLevel

CREATE TABLE #Members (
	row_nbr INT IDENTITY(1,1),
	customer_no INT,
	init_dt DATETIME
	)

INSERT INTO #Members
	SELECT	 mem.customer_no,
			 DATEADD (mm, DATEDIFF (mm, 0, mem.init_dt), 0) AS initiationDt -- made these all the first of the month so that they can be grouped by month in the report
	FROM	 dbo.TX_CUST_MEMBERSHIP mem
			 INNER JOIN #Nrr nrr
					 ON nrr.NRR_str = mem.NRR_status
			 INNER JOIN #MembLevel lev
					 ON lev.MembLevel_str = mem.memb_level
	WHERE	 mem.customer_no <> 0
	AND		 mem.memb_org_no = 4 -- Household 
	AND		 mem.init_dt BETWEEN @init_dt_start AND @init_dt_end
	AND		 mem.current_status IN (1, 2, 3, 9) -- Inactive, Active, Pending, Merged
	ORDER BY mem.customer_no

--select 'debug', * from #Members

CREATE TABLE #DateRank (
	row_nbr INT IDENTITY(1,1),
	init_dt DATETIME
	)

INSERT INTO #DateRank
	SELECT	DISTINCT init_dt 
	FROM	#Members
	ORDER BY init_dt

--select 'debug', * from #DateRank

DECLARE @cntDate INT = 1, @maxDate INT, @initDateStart DATETIME, @initDateEnd DATETIME

SELECT @maxDate = MAX(row_nbr)
FROM	#DateRank

CREATE TABLE #Attend (initiationDt DATETIME, customer_no INT, numVisits INT)

WHILE @cntDate <= @maxDate
	BEGIN

		SELECT @initDateStart = init_dt
		FROM   #DateRank
		WHERE  row_nbr = @cntDate;

		-- H. Sheridan, 20190222 - Add fiscal year start and end date parameters
		-- H. Sheridan, 20190308 - Move fiscal year calculations from top to inside this loop
		SELECT @fy = fyear FROM dbo.TR_Batch_Period WHERE @initDateStart BETWEEN start_dt AND end_dt
		SELECT @fystart = MIN(start_dt) FROM dbo.TR_Batch_Period WHERE fyear = @fy
		SELECT @fyend = MAX(end_dt) FROM dbo.TR_Batch_Period WHERE fyear = @fy

		INSERT INTO #Attend
			SELECT DISTINCT @initDateStart, att.customer_no, COUNT(DISTINCT CONVERT(CHAR(12), att.attend_dt)) 
			FROM   [dbo].[T_ATTENDANCE] att
					LEFT OUTER JOIN [dbo].[T_INVENTORY] inv
								ON	att.[perf_no] = inv.[inv_no]
								-- H. Sheridan, 20190418 - Remove this restriction per Scot's request
								--AND inv.[description] = 'Exhibit Halls'
			JOIN	#Members m
			ON		m.customer_no = att.customer_no
			WHERE   m.init_dt = @initDateStart
			AND		att.attend_dt BETWEEN @fystart AND @fyend
			GROUP BY att.customer_no

		SELECT @cntDate = @cntDate + 1;
	END;

-- Grab all members that didn't visit
INSERT INTO #Attend
	SELECT	m.init_dt, m.customer_no, 0
	FROM	#Members m
	WHERE	m.customer_no NOT IN (SELECT a.customer_no FROM #Attend a)

SELECT customer_no, initiationDt, numVisits FROM #Attend ORDER BY numVisits DESC




GO


