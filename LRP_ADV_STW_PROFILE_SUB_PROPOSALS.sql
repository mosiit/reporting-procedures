USE impresario;
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_ADV_STW_PROFILE_SUB_PROPOSALS]') AND type in (N'P', N'PC'))
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_ADV_STW_PROFILE_SUB_PROPOSALS] AS';
GO

EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LRP_ADV_STW_PROFILE_SUB_PROPOSALS] TO [impusers], [tessitura_app]';
GO

ALTER PROCEDURE [dbo].[LRP_ADV_STW_PROFILE_SUB_PROPOSALS]
         @customer_no INT = NULL
AS BEGIN

    SET NOCOUNT ON;
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    
    /*  Procedute Variables and Temp Tables  */

        --NONE

    /*  Check Parameters */

        SELECT @customer_no = ISNULL(@customer_no, 0)
               
        /*  This procedure is a simple select statement
            It is in a procedure rather than a direct SQL Statement for consistency
            and to make it easier should a need for further data manipulation be needed in the future.  */

    FINISHED:

        /*  Get Final Data Set For Report  */

            SELECT pln.[plan_no],
                   pln.[customer_no],
                   cpl.[customer_no] AS [worker_no],
                   wrk.[worker_name],
                   wrk.[worker_initials],
                   pln.[campaign_no],
                   cmp.[description] AS [campaign],
                   pln.[start_dt],
                   pln.[complete_by_dt],
                   pln.[status] AS [status_no],
                   pst.[description] AS [status],
                   pln.[ask_amt],
                   pln.[probability],
                   pln.[goal_amt] AS [expected_amt]
            FROM [dbo].[T_PLAN] AS pln
                 INNER JOIN [dbo].[T_CAMPAIGN] AS cmp ON cmp.[campaign_no] = pln.[campaign_no]
                 INNER JOIN [dbo].[TR_PLAN_STATUS] AS pst ON pst.[id] = pln.[status]
                 LEFT OUTER JOIN [dbo].[TX_CUST_PLAN] AS cpl ON cpl.[plan_no] = pln.[plan_no] AND cpl.[primary_ind] = 'Y'
                 LEFT OUTER JOIN [dbo].[LV_CAMPAIGN_ACTIVITY_WORKERS] AS wrk ON wrk.[worker_customer_no] = cpl.[customer_no]
            WHERE pln.[customer_no] = @customer_no
              AND pln.[status] IN (1, 2, 4, 5, 8, 22, 23, 39)

END
GO

