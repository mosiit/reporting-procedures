USE impresario;
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_PERFORMANCE_LISTING]') AND type in (N'P', N'PC'))
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_PERFORMANCE_LISTING] AS';
GO

EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LRP_PERFORMANCE_LISTING] TO [impusers], [tessitura_app]';
GO

ALTER PROCEDURE [dbo].[LRP_PERFORMANCE_LISTING]
        @report_start_dt DATETIME = NULL,
        @report_end_dt DATETIME = NULL,
	    @status_str VARCHAR(4000) = NULL,
        @title_str VARCHAR(4000) = NULL
AS BEGIN

    SET NOCOUNT ON;
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

    /*  Procedure Variables  */

        DECLARE @status_list TABLE ([prf_status_no] INT)
        DECLARE @title_list TABLE ([title_no] INT)

    /*  Check Parameters  */

        IF @report_start_dt IS NULL OR @report_end_dt IS NULL
            SELECT @report_start_dt = DATEADD(DAY,1,GETDATE()),
                   @report_end_dt = DATEADD(DAY,1,GETDATE())

        SELECT @report_start_dt = CAST(@report_start_dt AS DATE)
        
        SELECT @report_end_dt = FORMAT(@report_end_dt,'yyyy/MM/dd') +' 23:59:59.957'

        IF ISNULL(@status_str, '') = ''
            INSERT INTO @status_list ([prf_status_no])
            SELECT [id] FROM [dbo].[TR_PERF_STATUS]
        ELSE
            INSERT INTO @status_list ([prf_status_no])
            SELECT [element] FROM [dbo].[FT_SPLIT_LIST](REPLACE(@status_str,'"',''),',')

        IF ISNULL(@title_str, '') = ''
            INSERT INTO @title_list ([title_no])
            SELECT [title_no] FROM [dbo].[T_TITLE]
        ELSE
            INSERT INTO @title_list ([title_no])
            SELECT [element] FROM [dbo].[FT_SPLIT_LIST](REPLACE(@title_str,'"',''),',')

    /*  Get Data  */

        SELECT [season_fiscal_year] AS [fyear],
               stp.[description] AS [season_type],
               pf1.[title_no],
               pf1.[title_name],
               pf1.[production_no],
               pf1.[production_name],
               pf1.[performance_code] AS [perf_code],
               pf1.[performance_facility_name] AS [facility],
               pf1.[performance_name] AS [perf_name],
               pf1.[performance_name_short] AS [perf_short],
               pf1.[performance_dt],
               zmp.[description] AS [zone_map],
               bsm.[description] AS [best_seat],
               prm.[description] AS [price_map],
               pct.[description] AS [price_map_category],
               pf1.[performance_status_name] AS [perf_status],
               pf1.[performance_type_name] AS [perf_type],
               tim.[description] AS [time_slot],
               rnk.[description] AS [rank_type],
               cmp.[description] AS [campaign],
               tmp.[name] AS [template],
               pf1.[performance_no] AS [perf_no],
               ISNULL(lnk.[value],'') AS [event_link],
               CASE WHEN ISNULL(lnk.[value],'') LIKE '%?pwd=%' THEN ISNULL(LEFT(RTRIM(ISNULL(lnk.[value],'')), CHARINDEX('?', RTRIM(lnk.[value])) - 1),'')
                    ELSE ISNULL(lnk.[value],'') END AS [event_url],
               CASE WHEN ISNULL(lnk.[value],'') LIKE '%?pwd=%' THEN ISNULL(RIGHT(RTRIM(ISNULL(lnk.[value],'')), LEN(RTRIM(ISNULL(lnk.[value],0))) - CHARINDEX('?', RTRIM(lnk.[value]))),'')
                    ELSE '' END AS [event_pwd]
        FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE_NO_ZONES] AS pf1
             INNER JOIN @title_list AS ttl ON ttl.[title_no] = pf1.[title_no]
             INNER JOIN [dbo].[T_PERF] AS pf2 ON pf2.[perf_no] = pf1.[performance_no]
             INNER JOIN @status_list AS sls ON sls.[prf_status_no] = pf2.[perf_status]
             INNER JOIN [dbo].[TR_SEASON] sea ON sea.[id] = pf2.[season]
             INNER JOIN [dbo].[TR_SEASON_TYPE] AS stp ON stp.[id] = sea.[type]
             INNER JOIN [dbo].[TR_TIME_SLOT] AS tim ON tim.[id] = pf2.[time_slot]
             INNER JOIN [dbo].[T_ZMAP] AS zmp ON zmp.[zmap_no] = pf2.[zmap_no]
             INNER JOIN [dbo].[T_BSMAP] AS bsm ON bsm.[bsmap_no] = pf2.[bsmap_no]
             INNER JOIN [dbo].[T_PERF_PRICE_TYPE] AS ptp ON ptp.[perf_no] = pf1.[performance_no] AND ptp.[base_ind] = 'Y'
             INNER JOIN [dbo].[T_PERF_PRICE_LAYER] AS prm ON prm.[id] = ptp.[perf_price_layer]
             INNER JOIN [dbo].TR_PRICE_LAYER_TYPE ply ON ply.[id] = prm.[price_layer_type]
             INNER JOIN [dbo].[TR_PRICE_CATEGORY] pct ON pct.[id] = ply.[price_category_id]
             INNER JOIN [dbo].[TR_RANK_TYPE] AS rnk ON rnk.[id] = pf2.[rank_type]
             INNER JOIN [dbo].[T_CAMPAIGN] AS cmp ON cmp.[campaign_no] = pf2.[campaign_no]
             LEFT OUTER JOIN [dbo].[T_PRICE_TEMPLATE] AS tmp ON tmp.[id] = prm.template 
             LEFT OUTER JOIN [dbo].[TX_INV_CONTENT] AS lnk ON lnk.[inv_no] = pf2.[perf_no] AND lnk.[content_type] = 41
        WHERE pf1.[performance_dt] BETWEEN @report_start_dt AND @report_end_dt

            --AND ISNULL(lnk.[value],'') LIKE '% %'

    DONE:

END
GO

--EXECUTE [dbo].[LRP_PERFORMANCE_LISTING] @report_start_dt = '11-1-2020', @report_end_dt = '12-1-2020', @status_str = '', @title_str = '84069'



--SELECT title_name, performance_code, [performance_date], [performance_time], [production_name]
--FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE]
--WHERE [performance_date] >= '2020/11/01'
--  AND [performance_no] IN (SELECT [inv_no] 
--                           FROM [dbo].[T_INVENTORY]
--                           WHERE [inv_no] IN (SELECT [inv_no] 
--                                              FROM [dbo].[TX_INV_CONTENT] 
--                                              WHERE [content_type] = 41 
--                                                AND RIGHT([value],1) = ' '))
--ORDER BY [performance_date], [performance_time]


