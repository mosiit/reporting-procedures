USE [impresario]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[LRP_PLEDGE_SCHEDULE]
	@customer_no INT, 
	@printable_fund VARCHAR(80),
	@contrib_dt DATE -- not datetime  
AS
BEGIN

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
SET NOCOUNT ON;

--EXEC [LRP_PLEDGE_SCHEDULE] 90009, 'Blue Wing Founders Fund', '4/24/2017'

--/*outstanding pledge schedule � hard credit*/
SELECT 
	c.ref_no,
	s.sch_no,
    c.customer_no,
    b.best_name hard_credit_donor,
    b.best_name main_donor,
    s.amt_due,
    s.due_dt,
	s.rec_status,
	per.fyear AS due_fy, 
    ISNULL(s.amt_recd, 0.00) AS amt_rcd,
    s.amt_due - ISNULL(s.amt_recd, 0.00) AS due_bal,
	c.cont_amt - recd_amt AS pledge_balance,
	c.custom_1 softCreditType,
    t.sort_name,
	NULL AS via 
FROM T_CONTRIBUTION AS c
INNER JOIN T_CAMPAIGN AS g
    ON c.campaign_no = g.campaign_no
INNER JOIN TR_CAMPAIGN_CATEGORY AS cc
    ON g.category = cc.id
INNER JOIN T_FUND AS f
    ON c.fund_no = f.fund_no
INNER JOIN T_SCHEDULE AS s
    ON c.ref_no = s.ref_no
INNER JOIN dbo.TR_Batch_Period per
	ON s.due_dt BETWEEN per.start_dt AND per.end_dt
LEFT JOIN dbo.LTR_FUND_DETAIL fdet
	ON fdet.fund_no = f.fund_no
LEFT OUTER JOIN LV_ADV_BEST_NAME AS b
    ON c.customer_no = b.customer_no
LEFT OUTER JOIN TR_CONT_DESIGNATION AS cd
    ON c.cont_designation = cd.id
INNER JOIN T_CUSTOMER AS t
    ON c.customer_no = t.customer_no
WHERE c.cont_type = 'P'
	AND c.cont_amt - recd_amt > 0
	AND c.custom_1 IN ('(none)', 'Matching Gift Credit')
	AND c.custom_4 <> 'Converted Match Pledge'
	AND fdet.printable_fund = @printable_fund
	AND DATEDIFF(d,c.cont_dt,@contrib_dt) = 0 
	AND s.amt_due - ISNULL(s.amt_recd, 0.00) <> 0
	AND c.customer_no = @customer_no

UNION ALL 

/*outstanding pledge schedule � soft credit*/
SELECT c.ref_no,
	s.sch_no,
    c.customer_no,
    b.best_name hard_credit_donor,
    b2.best_name main_donor,
    s.amt_due,
    s.due_dt,
	s.rec_status,
	per.fyear AS due_fy,
    ISNULL(s.amt_recd, 0.00) AS amt_rcd,
	s.amt_due - ISNULL(s.amt_recd, 0.00) AS due_bal,
	c.cont_amt - recd_amt AS pledge_balance,
	c.custom_1 softCreditType,
    t.sort_name,
	CASE 
		WHEN b.best_name <> b2.best_name THEN 'via ' + b.best_name
		ELSE NULL
	END AS via 
FROM T_CONTRIBUTION AS c
INNER JOIN T_CAMPAIGN AS g
    ON c.campaign_no = g.campaign_no
INNER JOIN TR_CAMPAIGN_CATEGORY AS cc
    ON g.category = cc.id
INNER JOIN T_CREDITEE AS r
    ON c.ref_no = r.ref_no
INNER JOIN T_FUND AS f
    ON c.fund_no = f.fund_no
INNER JOIN T_SCHEDULE AS s
    ON c.ref_no = s.ref_no
INNER JOIN dbo.TR_Batch_Period per
	ON s.due_dt BETWEEN per.start_dt AND per.end_dt
LEFT JOIN dbo.LTR_FUND_DETAIL fdet
	ON fdet.fund_no = f.fund_no
LEFT OUTER JOIN LV_ADV_BEST_NAME AS b
    ON c.customer_no = b.customer_no
LEFT OUTER JOIN TR_CONT_DESIGNATION AS cd
    ON c.cont_designation = cd.id
LEFT OUTER JOIN LV_ADV_BEST_NAME AS b2
    ON r.creditee_no = b2.customer_no
INNER JOIN T_CUSTOMER AS t
    ON r.creditee_no = t.customer_no
WHERE c.cont_type = 'P'
	AND c.cont_amt - recd_amt > 0
	AND c.custom_1 IN ('Primary Soft Credit', 'Stewardship Soft Credit', 'Matching Gift Credit')
	AND c.custom_4 <> 'Converted Match Pledge'
	AND fdet.printable_fund = @printable_fund
	AND DATEDIFF(d,c.cont_dt,@contrib_dt) = 0
	AND s.amt_due - ISNULL(s.amt_recd, 0.00) <> 0 
	AND c.customer_no = @customer_no

END 
