USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_CUST_STATEMENT_PAYMENTS]') AND type in (N'P', N'PC')) BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_CUST_STATEMENT_PAYMENTS] AS' 
    EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LRP_CUST_STATEMENT_PAYMENTS] TO impusers' 
END
GO

ALTER PROCEDURE [dbo].[LRP_CUST_STATEMENT_PAYMENTS]
        @customer_no INT = NULL,
        @report_start_dt DATETIME = NULL,
        @report_end_dt DATETIME = NULL,
        @mode_of_sale_str VARCHAR(4000) = NULL,
        @include_returns CHAR(1) = 'N',
        @order_no INT = 0,
        @inc_invoices CHAR(1) = 'N',
        @summarize_methods CHAR(1) = 'N'
WITH RECOMPILE AS BEGIN

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    SET NOCOUNT ON;

    /* Procedure Variables  */

        DECLARE @order_list TABLE ([order_no] INT NOT NULL DEFAULT (0), 
                                   [customer_no] INT NOT NULL DEFAULT (0),
                                   [po_no] VARCHAR(255) NOT NULL DEFAULT (''))

        DECLARE @payment_table TABLE ([customer_no] INT NOT NULL DEFAULT (0),
                                      [order_no] INT NOT NULL DEFAULT (0),
                                      [po_no] VARCHAR(255) NOT NULL DEFAULT (''),
                                      [payment_dt] DATE NULL, 
                                      [payment_method_no] INT NOT NULL DEFAULT (0),
                                      [payment_method_name] VARCHAR(100) NOT NULL DEFAULT (''),
                                      [payment_Amount] DECIMAL (18,2) NOT NULL DEFAULT (0.0),
                                      [payment_check_no] VARCHAR(30) NOT NULL DEFAULT (0.0),
                                      [payment_notes] VARCHAR(1024) NOT NULL DEFAULT (''))

    /*  Temporary Tables  */

        IF OBJECT_ID('tempdb..#temp_order_data') IS NOT NULL DROP TABLE [#order_data]

        CREATE TABLE [#temp_order_data] ([customer_no] INT NOT NULL DEFAULT (0),
                                    [initiator_no] INT NOT NULL DEFAULT (0),
                                    [customer_is_initator] CHAR(1) NOT NULL DEFAULT ('N'),
                                    [initiator_name] VARCHAR(100) NOT NULL DEFAULT (''),
                                    [initiator_sort_name] VARCHAR(100) NOT NULL DEFAULT (''),
                                    [customer_name] VARCHAR(100) NOT NULL DEFAULT (''),
                                    [customer_sort_name] VARCHAR(100) NOT NULL DEFAULT (''),
                                    [order_no] INT NOT NULL DEFAULT (0),
                                    [order_type] VARCHAR(10) NOT NULL DEFAULT (''),
                                    [order_dt] DATETIME NULL,
                                    [order_created_by] VARCHAR(25) NOT NULL DEFAULT(''),
                                    [order_category_no] INT NOT NULL DEFAULT (0),
                                    [order_category] VARCHAR(30) NOT NULL DEFAULT(''),
                                    [mode_of_sale_no] INT NOT NULL DEFAULT (0),
                                    [mode_of_sale] VARCHAR(30) NOT NULL DEFAULT(''),
                                    [audience] VARCHAR(255) NOT NULL DEFAULT (''),
                                    [po_no] VARCHAR(255) NOT NULL DEFAULT (''),
                                    cust_max_performance_dt DATETIME NULL,
                                    [min_performance_dt] DATETIME NULL,
                                    [performance_dt] DATETIME NULL,
                                    [title_no] INT NOT NULL DEFAULT (0),
                                    [title_name] VARCHAR(30) NOT NULL DEFAULT(''),
                                    [title_order] CHAR(1) NOT NULL DEFAULT ('Z'),
                                    [total_amount] DECIMAL (18,2) NOT NULL DEFAULT (0.0),
                                    [quantity] INT NOT NULL DEFAULT (0),
                                    [total_due] DECIMAL (18,2) NOT NULL DEFAULT (0.0),
                                    [total_paid] DECIMAL (18,2) NOT NULL DEFAULT (0.0),
                                    [invoice_payment_amount] DECIMAL (18,2) NOT NULL DEFAULT (0.0),
                                    [visit_date_count] INT NOT NULL DEFAULT (0))


    /*  Check Parameters  */

            --Check for null values and correct
            SELECT @customer_no = ISNULL(@customer_no, 0);
            SELECT @report_start_dt = ISNULL(@report_start_dt, '5-16-2016');
            SELECT @report_end_dt = ISNULL(@report_end_dt, '12-31-2999')
            SELECT @include_returns = ISNULL(@include_returns, 'N')
            SELECT @order_no = ISNULL(@order_no, 0)
            SELECT @inc_Invoices = ISNULL(@inc_Invoices, 'N')
            SELECT @summarize_methods = ISNULL(@summarize_methods, 'Y')


    /*  Jump to the end if no customer numnber was passed to the report  */

        IF @customer_no <= 0 GOTO FINISHED

    /*  Get Orders  */

        IF @order_no > 0

            INSERT INTO @order_list ([order_no], [customer_no], [po_no])
            SELECT [order_no], 
                   [customer_no],
                   ISNULL([custom_8],'')
            FROM [dbo].[T_ORDER]
            WHERE [order_no] = @order_no

        ELSE BEGIN

            INSERT INTO [#temp_order_data] ([customer_no], [initiator_no], [customer_is_initator], [initiator_name], [initiator_sort_name], [customer_name], [customer_sort_name], [order_no],
                                            [order_type], [order_dt], [order_created_by], [order_category_no], [order_category], [mode_of_sale_no], [mode_of_sale], [audience], [po_no],
                                            [cust_max_performance_dt], [min_performance_dt], [performance_dt], [title_no], [title_name], [title_order], [total_amount], [quantity], 
                                            [total_due], [total_paid], [invoice_payment_amount], [visit_date_count])
            EXECUTE [dbo].[LRP_CUST_STATEMENT_ORDERS] @customer_no, @report_start_dt, @report_end_dt, @mode_of_sale_str, @include_returns, 0
    
            INSERT INTO @order_list ([order_no], [customer_no], [po_no])
            SELECT DISTINCT ord.[order_no], 
                            ord.[customer_no], 
                            ord.[po_no] 
            FROM [#temp_order_data] AS ord
                 
        END;


    /*  Get Payments  */        
    
        WITH CTE_CREDIT_CARD_METHODS ([method_id], [credit_card])
        AS (
            SELECT mth.[id], 
                   ISNULL(atp.[description],mth.[description])
            FROM [dbo].[TR_PAYMENT_METHOD] AS mth
                 LEFT OUTER JOIN [dbo].[TR_ACCOUNT_TYPE] AS atp ON mth.[act_type] = atp.[id]
            )
        INSERT INTO @payment_table ([customer_no], [order_no], [po_no], [payment_dt], [payment_method_no], [payment_method_name], [payment_Amount], [payment_check_no], [payment_notes])
        SELECT pay.[payment_customer_no],
               pay.[order_no],
               ord.[po_no],
               CAST(pay.[payment_dt] AS DATE),
               pay.[payment_method_no],
               COALESCE(sch.[scholarship_name], mth.[credit_card], pay.[payment_method_name]),
               pay.[payment_amount],
               pay.[payment_check_no],
               pay.[payment_notes]
        FROM [dbo].[LV_ORDER_PAYMENTS] AS pay
             INNER JOIN @order_list AS ord ON ord.[order_no] = pay.[order_no]
             LEFT OUTER JOIN [dbo].[LV_SCHOLARSHIP_PAYMENTS] AS sch ON sch.[payment_no] = pay.[payment_no] AND sch.[transaction_no] = pay.[transaction_no] AND sch.transaction_Sequence_no = pay.[transaction_sequence_no]
             LEFT OUTER JOIN [CTE_CREDIT_CARD_METHODS] AS mth ON mth.[method_id] = pay.[payment_method_no]
        WHERE (@inc_Invoices = 'Y' OR ISNULL(sch.[scholarship_name],pay.[payment_method_name]) NOT LIKE '%invoice%')
          AND pay.[payment_amount] <> 0.0 
          

    /*  Catch the orders that have no payments yet.  */

        INSERT INTO @payment_table ([customer_no], [order_no], [po_no], [payment_dt], [payment_method_no], [payment_method_name], [payment_Amount], [payment_check_no], [payment_notes])
        SELECT [customer_no],
               [order_no],
               [po_no],
               NULL,
               0,
               '',
               0.0,
               '',
               ''
        FROM @order_list
        WHERE [order_no] NOT IN (SELECT [order_no] FROM @payment_table)


    /*  Update payment methods to make it more readable.  */

        UPDATE @payment_table
        SET [payment_method_name] = CASE WHEN [payment_method_name] LIKE '%Check%' AND ISNULL([payment_check_no],'') <> '' THEN 'Check # ' + [payment_check_no]
                                         WHEN [payment_method_name] LIKE '%Check%' THEN 'Check'
                                         ELSE [payment_method_name] END

        UPDATE @payment_table
        SET [payment_method_name] = CASE WHEN [payment_method_name] LIKE '%Lockbox%' AND ISNULL([payment_check_no],'') <> '' THEN 'Check # ' + [payment_check_no]
                                         WHEN [payment_method_name] LIKE '%Lockbox%' THEN 'Check'
                                         ELSE [payment_method_name] END

    FINISHED:

        IF @summarize_methods = 'Y'
            SELECT [customer_no],
                   [order_no],
                   [po_no],
                   CAST([payment_dt] AS DATE) AS [payment_dt],
                   [payment_method_no],
                   [payment_method_name],
                   [payment_check_no],
                   SUM([payment_Amount]) AS [payment_Amount]
            FROM @payment_table
            GROUP BY [customer_no], [order_no], [po_no], CAST([payment_dt] AS DATE), [payment_method_no], [payment_method_name], [payment_check_no]
            ORDER BY [order_no], [payment_dt], [payment_method_name]
        ELSE
            SELECT [customer_no],
                   [order_no],
                   [po_no],
                   CAST([payment_dt] AS DATE) AS [payment_dt],
                   [payment_method_no],
                   [payment_method_name],
                   [payment_check_no],
                   [payment_Amount]
            FROM @payment_table
            ORDER BY [order_no], [payment_dt]

    DONE:

END
GO

--EXECUTE [dbo].[LRP_CUST_STATEMENT_PAYMENTS] @customer_no = 2589506, @report_start_dt = NULL, @report_end_dt = NULL, @include_returns = 'N', @order_no = 2579, @inc_Invoices = 'N', @summarize_methods = 'Y'
--EXECUTE [dbo].[LRP_CUST_STATEMENT_PAYMENTS] @customer_no = 2601197, @report_start_dt = '10-1-2016', @report_end_dt = '6-30-2020', @include_returns = 'N', @order_no = 774038, @inc_invoices = 'N', @summarize_methods = 'Y'

--EXECUTE [dbo].[LRP_CUST_STATEMENT_PAYMENTS] @customer_no = 67226, @report_start_dt = '10-1-2016', @report_end_dt = '6-30-2020', @mode_of_sale_str = '22,32', @include_returns = 'N', @order_no = 0, @inc_invoices = 'N', @summarize_methods = 'Y'

--EXECUTE [dbo].[LRP_CUST_STATEMENT_ORDERS] @customer_no = 2589506, @report_start_dt = NULL, @report_end_dt = NULL, @include_returns = 'N'
--EXECUTE [dbo].[LRP_CUST_STATEMENT_PAYMENTS] 
--SELECT * FROM dbo.LV_SCHOLARSHIP_PAYMENTS WHERE order_no = 2005835
--SELECT order_no, count(*) FROM dbo.LV_SCHOLARSHIP_PAYMENTS WHERE performance_dt >= '10-1-2019' GROUP BY order_no HAVING count(*) > 1
--SELECT order_no, count(*) FROM dbo.LV_SCHOLARSHIP_PAYMENTS WHERE performance_dt >= '10-1-2019' GROUP BY order_no HAVING count(*) > 1


--SELECT * FROM T_ORDER WHERE [order_no] = 2005835
--SELECT * FROM [dbo].[T_CUSTOMER] WHERE [customer_no] = 2589869


