USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LRP_REPORT_SUMMARY]    Script Date: 2/19/2019 8:44:00 AM ******/
DROP PROCEDURE [dbo].[LRP_REPORT_SUMMARY]
GO

/****** Object:  StoredProcedure [dbo].[LRP_REPORT_SUMMARY]    Script Date: 2/19/2019 8:44:00 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[LRP_REPORT_SUMMARY]
	--@StartDate DATETIME,
	--@EndDate DATETIME,
	@TimePeriodMins INT
WITH EXECUTE AS 'dbo' 
AS
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

DECLARE @tblLog TABLE ([ReportNbr] INT,
					   [StepNbr] INT,
					   [ScheduleType] VARCHAR(30),
					   [ExecutionId] VARCHAR(200),
					   [SQL Server] VARCHAR(30),
					   [Full Report Path] VARCHAR(MAX),
					   [Report Path] VARCHAR(MAX),
					   [Report Name] VARCHAR(100),
					   [User Name] VARCHAR(100),
					   [Request Type] VARCHAR(100),
					   [Format] VARCHAR(100),
					   [Custom Type] VARCHAR(30),
					   [Full Parameters] VARCHAR(MAX),
					   [User Group] VARCHAR(30),
					   [User ID] VARCHAR(30),
					   [Company Name] VARCHAR(100),
					   [Report Parameters] VARCHAR(MAX),
					   [Action] VARCHAR(100),
					   [Start Time] DATETIME,
					   [End Time] DATETIME,
					   [Duration (Sec)] DECIMAL(18, 2),
					   [Data Retrieval Time (Sec)] DECIMAL(18, 2),
					   [Data Retrieval Time (MS)] DECIMAL(18, 2),
					   [Processing Time (Sec)] DECIMAL(18, 2),
					   [Processing Time (MS)] DECIMAL(18, 2),
					   [Rendering Time (Sec)] DECIMAL(18, 2),
					   [Rendering Time (MS)] DECIMAL(18, 2),
					   [Type of Execution] VARCHAR(100),
					   [Status] VARCHAR(100),
					   [Byte Count] INT,
					   [Record Count] INT,
					   [Additional Info] XML);

INSERT INTO @tblLog
			-- Reports run ON-DEMAND 
			SELECT DENSE_RANK () OVER (ORDER BY ExecutionId ASC),
				   ROW_NUMBER () OVER (PARTITION BY ExecutionId ORDER BY TimeStart ASC),
				   'On Demand',
				   ExecutionId,
				   LTRIM (RTRIM (SUBSTRING (InstanceName, 1, PATINDEX ('%\%', InstanceName) - 1))) AS 'SQL Server',
				   ItemPath AS 'Full Report Path',
				   REPLACE (
					   REVERSE (
						   SUBSTRING (REVERSE (itemPath), CHARINDEX ('/', REVERSE (itemPath)), LEN (REVERSE (itemPath)))
					   ),
					   '/Tessitura/',
					   ''
				   ) AS reportPath,
				   CASE
						WHEN ItemPath = 'Unknown' THEN ''
						ELSE REVERSE (SUBSTRING (REVERSE (itemPath), 1, CHARINDEX ('/', REVERSE (itemPath)) - 1))
				   END AS reportName,
				   CASE
						WHEN [Parameters] NOT LIKE 'customer_no%' THEN 'Report Account'
						ELSE [UserName]
				   END AS 'User Name',
				   RequestType,
				   CASE
						WHEN [Format] IS NULL THEN 'Subreport or Sort'
						WHEN [Format] = 'RPL' THEN 'Web/Internal'
						ELSE [Format]
				   END AS 'Format',
				   CASE
						WHEN [Parameters] LIKE 'customer_no%' THEN 'Screen'
						ELSE 'Report'
				   END AS 'Custom Type',
				   REPLACE (
					   REPLACE (
						   REPLACE (
							   REPLACE (REPLACE (CONVERT (VARCHAR(MAX), [Parameters]), '%20', ' '), '%2F', '/'),
							   '%3A',
							   ':'
						   ),
						   '%22',
						   '"'
					   ),
					   '%2C',
					   ','
				   ),
				   '' AS [User Group],
				   '' AS [User ID],
				   '' AS [Company Name],
				   '' AS [Report Parameters],
				   ItemAction AS [Action],
				   TimeStart AS [Start Time],
				   TimeEnd AS [End Time],
				   CONVERT (DECIMAL(18, 2), (DATEDIFF (ms, TimeStart, TimeEnd) / 1000)) AS [Duration (Sec)],
				   CONVERT (DECIMAL(18, 2), TimeDataRetrieval) / 1000 AS [Data Retrieval Time (Sec)],
				   CONVERT (DECIMAL(18, 2), TimeDataRetrieval) AS [Data Retrieval Time (MS)],
				   CONVERT (DECIMAL(18, 2), TimeProcessing) / 1000 AS [Processing Time (Sec)],
				   CONVERT (DECIMAL(18, 2), TimeProcessing) AS [Processing Time (MS)],
				   CONVERT (DECIMAL(18, 2), TimeRendering) / 1000 AS [Rendering Time (Sec)],
				   CONVERT (DECIMAL(18, 2), TimeRendering) AS [Rendering Time (MS)],
				   [Source] AS [Type of Execution],
				   [Status],
				   ByteCount,
				   [RowCount],
				   AdditionalInfo
			FROM   [ReportServer].[dbo].[ExecutionLog3]
			WHERE  ExecutionId IS NOT NULL
			--AND	   (
			--	 TimeStart >= @StartDate
			--	AND TimeStart <= @EndDate + 1
			--)
			AND	   DATEDIFF (mi, TimeStart, GETDATE ()) <= @TimePeriodMins;

--select 'debug 1', * from @tblLog order by [Start Time]

INSERT INTO @tblLog
			-- Scheduled reports 
			SELECT 0,
				   0,
				   'Scheduled',
				   ExecutionId,
				   LTRIM (RTRIM (SUBSTRING (InstanceName, 1, PATINDEX ('%\%', InstanceName) - 1))) AS 'SQL Server',
				   ItemPath AS 'Full Report Path',
				   REPLACE (
					   REVERSE (
						   SUBSTRING (REVERSE (itemPath), CHARINDEX ('/', REVERSE (itemPath)), LEN (REVERSE (itemPath)))
					   ),
					   '/Tessitura/',
					   ''
				   ) AS reportPath,
				   CASE
						WHEN ItemPath = 'Unknown' THEN ''
						ELSE REVERSE (SUBSTRING (REVERSE (itemPath), 1, CHARINDEX ('/', REVERSE (itemPath)) - 1))
				   END AS reportName,
				   CASE
						WHEN [Parameters] NOT LIKE 'customer_no%' THEN 'Report Account'
						ELSE [UserName]
				   END AS 'User Name',
				   RequestType,
				   CASE
						WHEN [Format] IS NULL THEN 'Subreport or Sort'
						WHEN [Format] = 'RPL' THEN 'Web/Internal'
						ELSE [Format]
				   END AS 'Format',
				   CASE
						WHEN [Parameters] LIKE 'customer_no%' THEN 'Screen'
						ELSE 'Report'
				   END AS 'Custom Type',
				   REPLACE (
					   REPLACE (
						   REPLACE (
							   REPLACE (REPLACE (CONVERT (VARCHAR(MAX), [Parameters]), '%20', ' '), '%2F', '/'),
							   '%3A',
							   ':'
						   ),
						   '%22',
						   '"'
					   ),
					   '%2C',
					   ','
				   ),
				   '' AS [User Group],
				   '' AS [User ID],
				   '' AS [Company Name],
				   '' AS [Report Parameters],
				   ItemAction AS [Action],
				   TimeStart AS [Start Time],
				   TimeEnd AS [End Time],
				   CONVERT (DECIMAL(18, 2), (DATEDIFF (ms, TimeStart, TimeEnd) / 1000)) AS [Duration (Sec)],
				   CONVERT (DECIMAL(18, 2), TimeDataRetrieval) / 1000 AS [Data Retrieval Time (Sec)],
				   CONVERT (DECIMAL(18, 2), TimeDataRetrieval) AS [Data Retrieval Time (MS)],
				   CONVERT (DECIMAL(18, 2), TimeProcessing) / 1000 AS [Processing Time (Sec)],
				   CONVERT (DECIMAL(18, 2), TimeProcessing) AS [Processing Time (MS)],
				   CONVERT (DECIMAL(18, 2), TimeRendering) / 1000 AS [Rendering Time (Sec)],
				   CONVERT (DECIMAL(18, 2), TimeRendering) AS [Rendering Time (MS)],
				   [Source] AS [Type of Execution],
				   [Status],
				   ByteCount,
				   [RowCount],
				   AdditionalInfo
			FROM   [ReportServer].[dbo].[ExecutionLog3]
			WHERE  ExecutionId IS NULL
			--AND	   (
			--	 TimeStart >= @StartDate
			--	AND TimeStart <= @EndDate + 1
			--)
			AND	   DATEDIFF (mi, TimeStart, GETDATE ()) <= @TimePeriodMins;

--SELECT 'debug 2', * FROM @tblLog order by [Start Time]

UPDATE @tblLog
SET	   [User Group] = CASE WHEN PATINDEX('%UserGroup%', [Full Parameters]) > 0 THEN LTRIM (
						  RTRIM (
							  REPLACE (
								  LEFT(SUBSTRING (
										   LTRIM (RTRIM ([Full Parameters])),
										   PATINDEX ('%UserGroup=%', LTRIM (RTRIM ([Full Parameters]))) + 10,
										   LEN ([Full Parameters])
									   ), PATINDEX (
											  '%&%',
											  SUBSTRING (
												  LTRIM (RTRIM ([Full Parameters])),
												  PATINDEX ('%UserGroup=%', LTRIM (RTRIM ([Full Parameters]))) + 10,
												  LEN ([Full Parameters])
											  )
										  )),
								  '&',
								  ''
							  )
						  )
					  ) END,
	   [User ID] = CASE WHEN PATINDEX('%UserId%', [Full Parameters]) > 0 THEN LTRIM (
					   RTRIM (
						   REPLACE (
							   LEFT(SUBSTRING (
										LTRIM (RTRIM ([Full Parameters])),
										PATINDEX ('%UserId=%', LTRIM (RTRIM ([Full Parameters]))) + 7,
										LEN ([Full Parameters])
									), PATINDEX (
										   '%&%',
										   SUBSTRING (
											   LTRIM (RTRIM ([Full Parameters])),
											   PATINDEX ('%UserId=%', LTRIM (RTRIM ([Full Parameters]))) + 7,
											   LEN ([Full Parameters])
										   )
									   )),
							   '&',
							   ''
						   )
					   )
				   ) ELSE [User Name] END,
	   [Company Name] = CASE WHEN PATINDEX('%CompanyName%', [Full Parameters]) > 0 THEN LTRIM (
							RTRIM (
								REPLACE (
									LEFT(SUBSTRING (
											 LTRIM (RTRIM ([Full Parameters])),
											 PATINDEX ('%CompanyName=%', LTRIM (RTRIM ([Full Parameters]))) + 12,
											 LEN ([Full Parameters])
										 ), PATINDEX (
												'%&%',
												SUBSTRING (
													LTRIM (RTRIM ([Full Parameters])),
													PATINDEX ('%CompanyName=%', LTRIM (RTRIM ([Full Parameters]))) + 12,
													LEN ([Full Parameters])
												)
											)),
									'&',
									''
								)
							)
						) ELSE 'Museum of Science' END,
	   [Report Parameters] = REPLACE ([Full Parameters], '&', ', ');

--SELECT 'debug 3', * FROM @tblLog order by [Start Time]

INSERT INTO @tblLog ([Full Report Path], [Report Name], [Full Parameters], [Custom Type], [User ID], [User Group], [Start Time], [End Time], [Status])
	SELECT	rc.[description],
			rep.[name],
			ISNULL(rep.[psr_file], ''),
			rt.[description],
			--req.id AS 'Report ID',
			req.[user_id],
			req.[ug_id],
			req.request_date_time,
			req.end_date_time,
			CASE WHEN req.[result_code] = 'c' THEN 'Success' ELSE 'Unknown' END
			--ISNULL(req.[printer_name], '') AS 'Printer Name'
			--ISNULL(req.[email_recipients], '') AS 'Email Recipients'
	FROM	gooesoft_request req
	JOIN	gooesoft_report rep
	ON		req.[report_id] = rep.id
	JOIN	[TR_REPORT_TYPE] rt
	ON		rt.[id] = rep.[report_type]		
	JOIN	[gooesoft_report_category] rc
	ON		rc.[id] = rep.[category]
	WHERE	DATEDIFF (mi, req.request_date_time, GETDATE ()) > 0  AND DATEDIFF (mi, req.request_date_time, GETDATE ()) <= @TimePeriodMins
	AND		rt.[description] = 'InfoMaker';

--select 'debug', * from @tblLog

SELECT	 x.[Full Report Path] AS 'Report Path',
		 CASE
			WHEN x.[Custom Type] = 'Screen' THEN ct.[description]
		 	WHEN x.[Custom Type] = 'InfoMaker' THEN x.[Report Name]
			WHEN rep.[name] IS NOT NULL THEN rep.[name]
			WHEN PATINDEX('%/%', x.[Full Report Path]) > 0 THEN REVERSE(SUBSTRING(REVERSE(x.[Full Report Path]), 1, PATINDEX('%/%', REVERSE(x.[Full Report Path])) - 1))
			ELSE ISNULL(rep.[name], '') 
		 END AS 'Report Name',
		 ISNULL(REPLACE (x.[Report Parameters], ' ', ''), '') AS [Report Parameters],
		 DATEDIFF (SECOND, x.[Start Time], x.[End Time]) AS 'Duration',
		 CASE
			  WHEN x.[Custom Type] IN ('Screen', 'InfoMaker') THEN x.[Custom Type]
			  WHEN [Full Report Path] LIKE '/Tessitura/CustomReports/TessituraCreatedCustomReports/%' THEN
				  'Tessitura Custom'
			  WHEN [Full Report Path] LIKE '/Tessitura/CustomReports/%' THEN 'SSRS Custom'
			  WHEN [Full Report Path] LIKE '/Tessitura/Reports/%' OR [Full Report Path] LIKE '/Tessitura/ScheduledReports/%' THEN 'SSRS Standard'
			  ELSE 'Unknown'
		 END AS 'Type',
		 CASE WHEN x.[Custom Type] = 'InfoMaker' THEN x.[Status] ELSE SUBSTRING (x.[Status], 3, LEN (x.[Status])) END AS 'Status',
		 [Start Time],
		 [End Time],
		 CASE
			  WHEN x.[Custom Type] IN ('Screen', 'InfoMaker') THEN x.[User ID]
			  WHEN x.[User Name] = 'Report Account' THEN x.[User ID] -- x.[User Group] + ' > ' + x.[User ID]
			  ELSE x.[User Name]
		 END AS 'User ID',
		 x.[User Group],
		 --CASE WHEN  ISNULL(rep.[name], REVERSE(SUBSTRING(REVERSE(x.[Full Report Path]), 1, PATINDEX('%/%', REVERSE(x.[Full Report Path])) - 1))) LIKE 'MOS%' THEN 'MOS' ELSE 'Tessitura' END AS [Created By],
		 CASE
			  WHEN [Full Report Path] LIKE '/Tessitura/ScheduledReports/%' THEN 'Y'
			  ELSE 'N'
		 END AS [Scheduled],
		 FORMAT (x.[Record Count], '###,###,###') AS [Record Count]
FROM	 @tblLog x
LEFT JOIN	gooesoft_report rep
ON		x.[Full Report Path] = '/Tessitura/' + rep.[psr_file]
OR		x.[Full Report Path] = '/' + rep.[psr_file]
LEFT JOIN [TR_CUSTOM_TAB] ct
ON		x.[Report Name] = REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(ct.d_object, 'http://superman/ReportServer?/Tessitura/CustomReports/ConstituentCustomScreens/', ''), 'http://superman/ReportServer?/Tessitura/CustomReports/EventsCustomScreens/', ''), 'http://superman/ReportServer?/Tessitura/CustomReports/CampaignCustomScreens/', ''), '&customer_no=<<key>>', ''), '&campaign_no=<<key>>', ''), '&UserGroup=<<ug>>&UserId=<<user>>', '')
--WHERE	 [ReportNbr] > 0
GROUP BY x.[ReportNbr],
		 x.[SQL Server],
		 x.[Full Report Path],
		 rep.[name],
		 x.[Report Path],
		 x.[Report Name],
		 x.[User Name],
		 x.[Format],
		 x.[Custom Type],
		 x.[User Group],
		 x.[User ID],
		 x.[Company Name],
		 x.[Report Parameters],
		 x.[Action],
		 [Start Time],
		 [End Time],
		 x.[Duration (Sec)],
		 x.[Data Retrieval Time (Sec)],
		 x.[Processing Time (Sec)],
		 x.[Rendering Time (Sec)],
		 x.[Type of Execution],
		 [Status],
		 x.[Byte Count],
		 x.[Record Count],
		 ct.[description]
ORDER BY x.[Start Time] DESC;




GO


