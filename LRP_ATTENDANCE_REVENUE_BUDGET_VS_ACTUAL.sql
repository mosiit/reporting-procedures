USE impresario;
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_ATTENDANCE_REVENUE_BUDGET_VS_ACTUAL]') AND type in (N'P', N'PC'))
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_ATTENDANCE_REVENUE_BUDGET_VS_ACTUAL] AS';
GO

EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LRP_ATTENDANCE_REVENUE_BUDGET_VS_ACTUAL] TO [impusers], [tessitura_app]';
GO

ALTER PROCEDURE [dbo].[LRP_ATTENDANCE_REVENUE_BUDGET_VS_ACTUAL]
        @report_start_dt DATETIME = NULL,
        @report_end_dt DATETIME = NULL,
        @title_str VARCHAR(4000) = NULL
AS BEGIN

    SET NOCOUNT ON;
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

    /*  Procedure Variables and Temp Tables  */

         --These are the title numbers for the various venues
        DECLARE @gross_attend_no INT = 0,           @exhibit_halls_no INT = 27,         @special_exhibitions_no INT = 37,   
                @butterfly_garden_no INT = 157,     @mugar_omni_theater_no INT = 161,   @4d_theater_no INT = 173,
                @hayden_planetarium_no INT = 1132,  @thrill_ride_no INT = 1343,         @entertainment_no INT = 25;

        DECLARE @special_exh_name VARCHAR(30) = ''
        DECLARE @week_num INT = 0, @work_dt DATETIME

        DECLARE @report_titles TABLE ([title_no] INT NOT NULL DEFAULT (0),
                                      [title_name] VARCHAR(30) NOT NULL DEFAULT (''))
        
        IF OBJECT_ID('tempdb..#attendance_table') is not null DROP TABLE [#attendance_table]
        
        CREATE TABLE [#attendance_table] ([performance_dt] DATE NULL,
                                          [performance_day] VARCHAR(20) NOT NULL DEFAULT (''),
                                          [performance_day_abbrev] VARCHAR(3) NOT NULL DEFAULT (''),
                                          [week_num] INT NOT NULL DEFAULT (0),
                                          [title_no] INT NOT NULL DEFAULT(0),
                                          [title_name] VARCHAR(30) NOT NULL DEFAULT(''),
                                          [title_color] VARCHAR(30) NOT NULL DEFAULT(''),
                                          [title_group] VARCHAR(30) NOT NULL DEFAULT(''),
                                          [title_group_color] VARCHAR(30) NOT NULL DEFAULT (''),
                                          [title_group_order] CHAR(1) NOT NULL DEFAULT(''),
                                          [title_order] CHAR(1) NOT NULL DEFAULT(''),
                                          [base_attendance] DECIMAL(18,2) NOT NULL DEFAULT(0.0),
                                          [additional_attendance] DECIMAL(18,2) NOT NULL DEFAULT(0.0),
                                          [total_attendance] DECIMAL(18,2) NOT NULL DEFAULT(0.0),
                                          [attendance_budget] DECIMAL(18,2) NOT NULL DEFAULT(0.0),
                                          [attendance_variance] DECIMAL(18,2) NOT NULL DEFAULT(0.0),
                                          [attendance_variance_percent] DECIMAL(18,4) NOT NULL DEFAULT(0.0),
                                          [total_revenue] DECIMAL(18,2) NOT NULL DEFAULT(0.0),
                                          [revenue_budget] DECIMAL(18,2) NOT NULL DEFAULT(0.0),
                                          [revenue_variance] DECIMAL(18,2) NOT NULL DEFAULT(0.0),
                                          [revenue_variance_percent] DECIMAL(18,4) NOT NULL DEFAULT(0.0))

    /*  Check Parameters  */

        --If either of the dates is null, date range as the past week
        IF @report_start_dt IS NULL OR @report_end_dt IS NULL
            SELECT @report_start_dt = DATEADD(DAY, -7, GETDATE()),
                   @report_end_dt = DATEADD(DAY, -1, GETDATE())

        SELECT @report_start_dt = CAST(@report_start_dt AS DATE)
        SELECT @report_end_dt = CONVERT(CHAR(10),@report_end_dt,111) + ' 23:59:59.957'

    /*  Create Title List  */

        INSERT INTO @report_titles ([title_no])
        VALUES (@exhibit_halls_no),         (@butterfly_garden_no),     (@special_exhibitions_no),      (@mugar_omni_theater_no),    
               (@4d_theater_no),            (@thrill_ride_no),          (@hayden_planetarium_no)--,       (@entertainment_no) 
       
        IF ISNULL(@title_str,'') <> ''
            DELETE FROM @report_titles
            WHERE [title_no] NOT IN (SELECT [element] 
                                     FROM [dbo].[FT_SPLIT_LIST](REPLACE(@title_str,'"',''),','));

        UPDATE ttl
        SET ttl.[title_name] = CASE WHEN ttl.[title_no] = @entertainment_no THEN 'Entertainment'
                                    ELSE ISNULL([description],'') END
        FROM @report_titles AS ttl
             LEFT OUTER JOIN [dbo].[T_INVENTORY] AS inv ON inv.[inv_no] = ttl.[title_no]

    /*  Get History Data For Ticketed Attendance (venues) */
        
        INSERT INTO [#attendance_table] ([performance_dt], [performance_day], [performance_day_abbrev], [title_no], [title_name], [base_attendance], [total_revenue])
        SELECT tik.[performance_date], 
               DATENAME(WEEKDAY,tik.[performance_date]),
               LEFT(DATENAME(WEEKDAY,tik.[performance_date]),3),
               tik.[title_no], 
               tik.[title_name], 
               SUM(tik.[sale_total]),
               SUM(tik.[paid_amt])
        FROM [dbo].[LT_HISTORY_TICKET] AS tik
             INNER JOIN @report_titles AS ttl ON ttl.[title_no] = tik.[title_no]
        WHERE CAST(tik.[performance_date] AS DATE) BETWEEN @report_start_dt AND @report_end_dt
          AND tik.[attendance_type] = 'GATE A (Ticketed)'
        GROUP BY tik.[performance_date], 
                 tik.[title_no], 
                 tik.[title_name];

    --**Added 12/18/2020
    /*  Get History Data For Ticketed Attendance (visit count)  */

        WITH CTE_DAILY_REVENUE([perf_dt], [rev_amount])
        AS (SELECT CAST([performance_date] AS DATE), 
                   SUM([paid_amt])
            FROM [dbo].[LT_HISTORY_TICKET]
            WHERE CAST([performance_date] AS DATE) BETWEEN @report_start_dt AND @report_end_dt
              AND [attendance_type] = 'GATE A (Ticketed)'
              AND [title_no] IN (@exhibit_halls_no, @butterfly_garden_no, @special_exhibitions_no, @mugar_omni_theater_no,    
                                 @4d_theater_no, @thrill_ride_no, @hayden_planetarium_no)
            GROUP BY [performance_date])
        INSERT INTO [#attendance_table] ([performance_dt],[performance_day],[performance_day_abbrev],[week_num],[title_no],[title_name],
                                         [base_attendance],[additional_attendance],[total_attendance],[total_revenue])
        SELECT CAST(vis.[history_dt] AS date),
               DATENAME(WEEKDAY, vis.[history_dt]), 
               LEFT(DATENAME(WEEKDAY, vis.[history_dt]), 3),
               0,
               0,
               'Gross Attendance',
               SUM(vis.[visit_count]),
               0.0,
               SUM(vis.[visit_count]),
               rev.[rev_amount]
        FROM [dbo].[LT_HISTORY_VISIT_COUNT] AS vis
             LEFT OUTER JOIN [CTE_DAILY_REVENUE] AS rev ON CAST(vis.[history_dt] AS DATE) = rev.[perf_dt]
        WHERE [history_dt] BETWEEN @report_start_dt AND @report_end_dt
        GROUP BY CAST([history_dt] AS date), DATENAME(WEEKDAY,history_dt),  LEFT(DATENAME(WEEKDAY,history_dt),3), rev.[rev_amount]
        ORDER BY CAST([history_dt] AS date);

    /*  Add In Missing Dates with zero tickets sold  */

        WITH CTE_TITLES ([title_no], [title_name])
        AS (SELECT DISTINCT [title_no], [title_name] FROM [#attendance_table])
        INSERT INTO [#attendance_table] ([performance_dt], [performance_day], [performance_day_abbrev], [title_no], [title_name], [base_attendance], [total_revenue])
        SELECT dte.[return_dt],
               dte.[day_of_week],
               LEFT(dte.[day_of_week], 3),
               cte.[title_no],
               cte.[title_name],
               0,
               0.00
        FROM [dbo].[LFT_INDIVIDUAL_DATES](@report_start_dt, @report_end_dt) AS dte
             CROSS JOIN [CTE_TITLES] AS cte
             LEFT OUTER JOIN [#attendance_table] AS att ON CAST(att.[performance_dt] AS DATE) = CAST(dte.[return_dt] AS DATE)
                                                 AND att.[title_no] = cte.[title_no]
         WHERE ISNULL(att.[title_no],0) = 0;

    /*  Remove Duplicates (if any)  */

        WITH CTE_DUPES (perf_dt, title_no)
        AS (SELECT [performance_dt], 
                   [title_no]
            FROM [#attendance_table] 
            GROUP BY [performance_dt], [title_no] 
            HAVING COUNT(*) > 1)
        DELETE att
        FROM [#attendance_table] AS att 
             INNER JOIN [CTE_DUPES] AS dup ON dup.[perf_dt] = att.[performance_dt] AND dup.[title_no] = att.[title_no]
        WHERE [att].[base_attendance] = 0;
        
    /*  Add extra Non ticketed attendance to the Exhibit Halls
        - Membership Card Scans - Show and Go - Show and Collected -  */

        WITH [CTE_ADDITIONAL_ATTENDANCE] ([performance_dt], [add_attendance]) 
        AS (SELECT [history_date], 
                   SUM([visit_count])
            FROM [dbo].[LT_HISTORY_VISIT_COUNT] 
            WHERE [history_dt] BETWEEN @report_start_dt AND @report_end_dt
              AND [attendance_type] <> 'Ticketed Events'
            GROUP BY [history_date])
        UPDATE d
        SET d.[additional_attendance] = ISNULL(a.[add_attendance], 0)
        FROM [#attendance_table] AS d
             LEFT OUTER JOIN [CTE_ADDITIONAL_ATTENDANCE] AS a ON a.[performance_dt] = d.[performance_dt]
        WHERE d.[title_no] = @exhibit_halls_no;

        --SET Total Attendance to include extra non-ticketed attendance
        UPDATE [#attendance_table]
        SET [total_attendance] = ([base_attendance] + [additional_attendance]);

    /*  Add attendance budget information for each venue from the Budget Data system table  */

        WITH [CTE_ATTENDANCE_BUDGET] ([perf_dt], [title_no], [budget], [incremental])
        AS (SELECT CAST([perf_dt] AS DATE),
                   [title_no],
                   [budget],
                   [incremental]
            FROM [dbo].[LTR_ATTENDANCE_BUDGET_DATA]
            WHERE [perf_dt] BETWEEN @report_start_dt AND @report_end_dt)
        UPDATE d
        SET d.[attendance_budget] = ISNULL(b.[budget], 0)
        FROM [#attendance_table] AS d
             LEFT OUTER JOIN [CTE_ATTENDANCE_BUDGET] AS b ON b.[perf_dt] = d.[performance_dt] AND b.[title_no] = d.[title_no]

    /*  Determine variance = attendance - budget
        -Over budget will show as a positive number and below budget will show as a negative number  */

        UPDATE [#attendance_table] 
        SET [attendance_variance] = ([total_attendance] - [attendance_budget])
        
        UPDATE [#attendance_table]
        SET [attendance_variance_percent] = CASE WHEN [attendance_budget] = 0 THEN 0.0
                                                 ELSE ([attendance_variance] / [attendance_budget]) END;

    /*  Add revenue budget information for each venue from the Budget Data system table  */

        WITH [CTE_REVENUE_BUDGET] ([perf_dt], [title_no], [budget_revenue])
        AS (SELECT CAST([perf_dt] AS DATE),
                        [title_no],
                        [budget_revenue]
            FROM [dbo].[LTR_ATTENDANCE_BUDGET_DATA]
            WHERE [perf_dt] BETWEEN @report_start_dt AND @report_end_dt)
        UPDATE d
        SET d.[revenue_budget] = ISNULL(b.[budget_revenue], 0)
        FROM [#attendance_table] AS d
             LEFT OUTER JOIN [CTE_REVENUE_BUDGET] AS b ON b.[perf_dt] = d.[performance_dt] AND b.[title_no] = d.[title_no]
             
    /*  Determine variance = revenue - budget
        -Over budget will show as a positive number and below budget will show as a negative number  */
                
        UPDATE [#attendance_table] 
        SET [attendance_variance] = ([total_attendance] - [attendance_budget])

        UPDATE [#attendance_table] 
        SET [revenue_variance] = ([total_revenue] - [revenue_budget])
        
        UPDATE [#attendance_table]
        SET [revenue_variance_percent] = CASE WHEN [revenue_budget] = 0 THEN 0.0
                                              ELSE ([revenue_variance] / [revenue_budget]) END

                
    /*  Set the Title Group, Title Group Order, and Title Order for the selected data  */
        
        UPDATE [#attendance_table]
        SET [title_group] =  CASE WHEN [title_no] = @gross_attend_no THEN 'Entire Museum'
                                  WHEN [title_no] IN (@exhibit_halls_no, @butterfly_garden_no, @special_exhibitions_no) THEN 'Exhibits'
                                  WHEN [title_no] IN (@4d_theater_no, @mugar_omni_theater_no, @thrill_ride_no) THEN 'Theaters'
                                  WHEN [title_no] IN (@hayden_planetarium_no, @entertainment_no) THEN 'Planetarium'
                                  ELSE '' END,
            [title_group_order] =  CASE WHEN [title_no] = @gross_attend_no THEN 'A'
                                        WHEN [title_no] IN (@exhibit_halls_no, @butterfly_garden_no, @special_exhibitions_no) THEN 'B'
                                        WHEN [title_no] IN (@4d_theater_no, @mugar_omni_theater_no, @thrill_ride_no) THEN 'C'
                                        WHEN [title_no] IN (@hayden_planetarium_no, @entertainment_no) THEN 'D'
                                        ELSE 'Z' END,
             [title_order] = CASE [title_no]
                             WHEN @gross_attend_no THEN 'A'
                             WHEN @exhibit_halls_no THEN 'B'
                             WHEN @special_exhibitions_no THEN 'C'
                             WHEN @butterfly_garden_no THEN 'D'
                             WHEN @mugar_omni_theater_no THEN 'E'
                             WHEN @4d_theater_no THEN 'F'
                             WHEN @thrill_ride_no THEN 'G'
                             WHEN @hayden_planetarium_no THEN 'H'
                             WHEN @entertainment_no THEN 'I'
                             ELSE 'Z' END;

    /*  Delete anything not in one of the designated title groups  */

        DELETE FROM [#attendance_table] 
        WHERE [title_group] = '';    

    /*  Shorten Title Names - To Save Space  */

        UPDATE [#attendance_table]
        SET [title_name] =  CASE WHEN [title_no] = @mugar_omni_theater_no THEN 'Omni Theater'
                                 WHEN [title_no] = @hayden_planetarium_no THEN 'Planetarium'
                                 ELSE [title_name] END

    /*  Assign colors to each title Group  */

        UPDATE [#attendance_table]
        SET [title_group_color] = CASE WHEN [title_group] = 'Entire Museum' THEN 'LightSteelBlue'
                                       WHEN [title_group] = 'Exhibits' THEN 'LightSkyBlue'
                                       WHEN [title_group] = 'Theaters' THEN 'LightSteelBlue'
                                       WHEN [title_group] = 'Planetarium' THEN 'LightSkyBlue'
                                       ELSE 'White' END

    /*  Assign specific colors to each title  */

        UPDATE [#attendance_table]
        SET [title_color] = CASE [title_no] 
                            WHEN @gross_attend_no THEN 'LightSteelBlue'
                            WHEN @exhibit_halls_no THEN 'LightSkyBlue'
                            WHEN @butterfly_garden_no THEN 'LightSkyBlue'
                            WHEN @special_exhibitions_no THEN 'LightSkyBlue'
                            WHEN @mugar_omni_theater_no THEN 'LightSteelBlue'
                            WHEN @4d_theater_no THEN 'LightSteelBlue'
                            WHEN @thrill_ride_no THEN 'LightSteelBlue'
                            WHEN @hayden_planetarium_no THEN 'LightSkyBlue'
                            WHEN @entertainment_no THEN 'LightSkyBlue'
                            ELSE 'White' END


        /*****  Special Request - Remove Special Exhibitions From this particular
                Report which will only be used during the Covid crisis  *****/

                DELETE FROM [#attendance_table] WHERE title_no = @special_exhibitions_no

                /*  If Above Delete Statement is removed or commented out, below code will change title name 
                    from Special Exhibitions to the name of the exhibit  */

    IF EXISTS (SELECT * FROM [#attendance_table] WHERE title_no = @special_exhibitions_no) BEGIN
    
        UPDATE [#attendance_table]
        SET [#attendance_table].[title_name] = (SELECT ISNULL(MAX([production_name]),'Special Exhibitions') 
                                                FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE_NO_ZONES] AS p
                                                WHERE CAST(p.[performance_dt] AS DATE) = CAST([#attendance_table].[performance_dt] AS DATE) 
                                                  AND p.[title_no] = @special_exhibitions_no
                                                  AND p.[performance_type_name] = 'Public'
                                                  AND p.[production_name] NOT LIKE '%Buyout%')
        WHERE [#attendance_table].[title_no] = @special_exhibitions_no

        --Shorten titles if necessary
        UPDATE [#attendance_table] SET [title_name] = 'Body Worlds' WHERE [title_name] LIKE '%Body Worlds%'
        UPDATE [#attendance_table] SET [title_name] = 'Pixar' WHERE [title_name] LIKE '%Pixar%'
            
    END

    /*  Set Week Number  */

        IF DATEPART(WEEKDAY,@report_start_dt) <> 2 SELECT @week_num = 1

        SELECT @work_dt = CAST(@report_start_dt AS DATE)
        WHILE @work_dt <= CAST(@report_end_dt AS DATE) BEGIN

            IF DATEPART(WEEKDAY,@work_dt) = 2 SELECT @week_num += 1

            IF @week_num > 0
                UPDATE [#attendance_table]
                SET [week_num] = @week_num
                WHERE [performance_dt] = @work_dt

            SELECT @work_dt = DATEADD(DAY,1,@work_dt)

        END;

    FINISHED:

        --SELECT title_name, [performance_dt], SUM(total_revenue) FROM [#attendance_table] GROUP BY [title_name], [performance_dt] ORDER BY [title_name], [performance_dt]
    
        /*  Pull final data set for the report  */

            SELECT  [performance_dt],
                    [performance_day],
                    [performance_day_abbrev],
                    [week_num],
                    [title_no],
                    [title_group],
                    [title_group_color],
                    [title_group_order],
                    [title_order],
                    [title_name],
                    [title_color],
                    [base_attendance],
                    [additional_attendance],
                    [total_attendance],
                    [attendance_budget],
                    [attendance_variance],
                    [attendance_variance_percent],
                    [total_revenue],
                    [revenue_budget],
                    [revenue_variance],
                    [revenue_variance_percent]
            FROM [#attendance_table]
            ORDER BY [performance_dt], [title_group_order], [title_order]

    DONE:        

        /*  Clean up Temp Tables  */

            IF OBJECT_ID('tempdb..#attendance_table') is not null DROP TABLE [#attendance_table]

END
GO



--EXECUTE [dbo].[LRP_ATTENDANCE_REVENUE_BUDGET_VS_ACTUAL] @report_start_dt = '8-1-2020', @report_end_dt = '8-31-2020', @title_str = ''
--EXECUTE [dbo].[LRP_ATTENDANCE_REVENUE_BUDGET_VS_ACTUAL] @report_start_dt = '7-1-2020', @report_end_dt = '7-31-2020', @title_str = ''
--EXECUTE [dbo].[LRP_ATTENDANCE_REVENUE_BUDGET_VS_ACTUAL] @report_start_dt = '7-1-2019', @report_end_dt = '7-31-2019', @title_str = ''
EXECUTE [dbo].[LRP_ATTENDANCE_REVENUE_BUDGET_VS_ACTUAL] @report_start_dt = '2-1-2021', @report_end_dt = '2-28-2021', @title_str = ''

--SELECT * FROM [dbo].[LT_HISTORY_TICKET] WHERE [perf_date] = '2020/07/29' AND [title_name] = 'Mugar Omni Theater'
--SELECT * FROM [dbo].[LT_HISTORY_TICKET] WHERE [perf_date] = '2020/07/21' AND [title_name] = 'Mugar Omni Theater'


