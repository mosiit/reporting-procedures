USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_SCHOOL_ATTENDANCE_VISIT_COUNT]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LRP_SCHOOL_ATTENDANCE_VISIT_COUNT]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LRP_SCHOOL_ATTENDANCE_VISIT_COUNT]
        @report_start_dt DATETIME = NULL,
        @report_end_dt DATETIME = NULL
AS BEGIN

    /*  Procedure Variables  */

        DECLARE @rpt_msg varchar(100)       SELECT @rpt_msg = ''
        DECLARE @final_orders TABLE ([order_no] INT
                                     PRIMARY KEY CLUSTERED ([order_no] ASC))

        DECLARE @mos_buyout_no INT = (SELECT ISNULL([id],0) FROM [dbo].[TR_MOS] WHERE [description] = 'Buyouts')

    /* Null Dates = Yesterday's Date  */
    
        SELECT @report_start_dt = ISNULL(@report_start_dt,DATEADD(DAY,-1,GETDATE())),
               @report_end_dt = ISNULL(@report_end_dt,DATEADD(DAY,-1,GETDATE()))

    /*  Make sure the dates passed to the procedure include the entire day (from midnight on the start date to 11:59 PM on the end date)  */

        SELECT @report_start_dt = convert(char(10),@report_start_dt,111) + ' 00:00:00',
               @report_end_dt = convert(char(10),@report_end_dt,111) + ' 23:59:59.997'

    /*  Create the Temp Table to hold raw data from the T_SUB_LINEITEM TABLE  */

        IF OBJECT_ID('tempdb..#sub_lineitems') IS NOT NULL DROP TABLE [#sub_lineitems]

        CREATE TABLE [#sub_lineitems] ([sli_no] INT NOT NULL, [order_no] INT, [mode_of_sale] INT, [perf_no] int, [perf_date] CHAR(10), [perf_time] VARCHAR(8), [perf_type] INT, 
                                       [due_amt] decimal (18,2), [paid_amt] decimal(18,2), [sli_status] int, [price_type] INT)
                                                
        CREATE UNIQUE NONCLUSTERED INDEX [sub_lineitems_sli_no] ON [#sub_lineitems] ([sli_no] ASC) ON [PRIMARY]

        CREATE CLUSTERED INDEX [sub_lineitems_order_no] ON [#sub_lineitems] ([order_no] ASC) ON [PRIMARY]
          
    /*  Create temp tables to hold the visit count data  */
        
        IF OBJECT_ID('tempdb..#visit_count_raw_data') is not null DROP TABLE [#visit_count_raw_data]
        
        CREATE TABLE [#visit_count_raw_data] ([attend_type] varchar(30), [order_no] int, [perf_no] int, [zone_no] int, [prod_name] varchar(30), [sale_total] int, [scan_admission_total] int)

        IF OBJECT_ID('tempdb..#visit_count_final_data') is not null DROP TABLE [#visit_count_final_data]

        CREATE TABLE [#visit_count_final_data] ([attendance_type] varchar(30), [order_no] int, [sale_total] int, [scan_admission_total] INT, [report_message] VARCHAR(100))
    
    /*  Check Date Range to make sure report is being run for past dates  */

        IF CONVERT(DATE,@report_start_dt) > CONVERT(DATE,GETDATE()) OR CONVERT(DATE,@report_end_dt) > CONVERT(DATE,GETDATE()) BEGIN
            SELECT @rpt_msg = 'This report can only be run for PAST dates.'
            GOTO FINISHED
        END
                         
    /*  Get Sublineitem raw data  */

        INSERT INTO [#sub_lineitems]
        SELECT sli.[sli_no], sli.[order_no], ord.[MOS], sli.[perf_no], prf.[performance_date], prf.[performance_time], prf.[performance_type], 
               sli.[due_amt], sli.[paid_amt], sli.[sli_status], sli.[price_type]
        FROM [dbo].[T_SUB_LINEITEM] as sli (NOLOCK)
             INNER JOIN  [dbo].[T_ORDER] AS ord (NOLOCK) ON sli.[order_no] = ord.[order_no]
             INNER JOIN [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] AS prf (NOLOCK) ON prf.[performance_no] = sli.[perf_no] AND prf.[performance_zone] = sli.[zone_no]
        WHERE prf.[performance_dt] BETWEEN @report_start_dt AND @report_end_dt 
          AND sli.[sli_status] in (2, 3, 12)                            --SLI Status 2 = Seated, Unpaid/3 = Seated, Paid/12 = Ticketed, Paid
          AND (prf.[performance_type] = 3 or ord.[mos] IN (12,13))      --perf type 3 = School Only/mos 12 = Schools/mos 13 = Web Sales School)

    /*  Double-check performance dates  */
                                                 
        DELETE FROM [#sub_lineitems] WHERE [perf_date] not between convert(char(10), @report_start_dt,111) and convert(char(10),@report_end_dt,111)

    /*  Generate the list of orders  */

        INSERT INTO @final_orders
        SELECT DISTINCT [order_no] FROM #sub_lineitems


    /*  Retrieve the Visit Count data for ticketed events from the database  */
            
        INSERT INTO [#visit_count_raw_data]
        SELECT 'ticketed', [order_no], [perf_no], [zone_no], [production_name], sum([sale_total]), sum([scan_admission_total]) 
        FROM [dbo].[LT_HISTORY_TICKET]
        WHERE [order_no] IN (SELECT [order_no] FROM @final_orders) and [title_name] in (SELECT title_name FROM [dbo].[LV_SS_PRODUCTION_ELEMENTS_TITLE] WHERE [visit_count_title] = 'Y')
        GROUP BY [order_no], [perf_no], [zone_no], [production_name] ORDER BY [order_no]

        DELETE FROM [#visit_count_raw_data] WHERE prod_name like '%Buyout%'

    /*  Added 2/7/2018:  Hard Coded exception added for the Exhibit Halls Special production, which is in the 
                         Exhibit Halls title but should not be counted in with the visit count.  */
         
         DELETE FROM [#visit_count_raw_data] WHERE [prod_name] = 'Exhibit Halls Special'

        /*  Added 10/26/2017:  Deletes orders from the visit count that were created in the buyouts mode of sale regardless of what the production name is.
                           @mos_buyout_no is defined above and gets the mode of sale number from the TR_MOS table.  
                           It must find and if number > 0 for this to happen.  */

            DELETE FROM [#visit_count_raw_data]
            WHERE [order_no] IN (SELECT [order_no] FROM [dbo].[T_ORDER] WHERE [order_no] IN (SELECT [order_no] FROM [#visit_count_raw_data]) AND [MOS] = @mos_buyout_no)

    /*  Retrieve Final Visit Count Data For Each School Order*/

        --DECLARE @ord_no INT
        --DECLARE order_cursor INSENSITIVE CURSOR FOR SELECT DISTINCT order_no FROM #visit_count_raw_data ORDER BY order_no
        --OPEN order_cursor
        --BEGIN_ORDER_LOOP:
        --    FETCH NEXT FROM order_cursor INTO @ord_no
        --    IF @@FETCH_STATUS = -1 GOTO END_ORDER_LOOP
        --    SELECT order_no, perf_no, prod_name, sale_total FROM #visit_count_raw_data WHERE order_no = @ord_no ORDER BY sale_total DESC 
        --    GOTO BEGIN_ORDER_LOOP
        --END_ORDER_LOOP:
        --CLOSE order_cursor
        --DEALLOCATE order_cursor
        
        INSERT INTO [#visit_count_final_data] SELECT 'Ticketed Events', [order_no], max([sale_total]), max([scan_admission_total]), ''
        FROM [#visit_count_raw_data] 
        GROUP By [Order_no]

    FINISHED:

        IF NOT EXISTS (SELECT * FROM [#visit_count_final_data]) BEGIN
            IF @rpt_msg = '' SELECT @rpt_msg = 'No records found for the criteria you entered'
            INSERT INTO [#visit_count_final_data] VALUES ('', 0, 0, 0, @rpt_msg)
        END        

        /* Select aggregated date into the report  */

            SELECT [attendance_type], count([order_no]) as 'transaction_count', sum([sale_total]) as 'visit_count', sum([scan_admission_total]) as 'visit_count_scan', [report_message]
            FROM [#visit_count_final_data] 
            GROUP BY [attendance_type], [report_message]
            

    CLEAN_UP:

        /*  Not really necessary, but the programmer in me wants to clean up  */

        IF OBJECT_ID('tempdb..#visit_count_raw_data') is not null DROP TABLE [#visit_count_raw_data]
        IF OBJECT_ID('tempdb..#visit_count_final_data') is not null DROP TABLE [#visit_count_final_data]
        IF OBJECT_ID('tempdb..#sub_lineitems') IS NOT NULL DROP TABLE [#sub_lineitems]

    DONE:

END
GO

GRANT EXECUTE ON [dbo].[LRP_SCHOOL_ATTENDANCE_VISIT_COUNT] to impusers
GO

--EXECUTE [dbo].[LRP_SCHOOL_ATTENDANCE_VISIT_COUNT] @report_start_dt = '1-1-2017', @report_end_dt = '1-31-2017'






