USE [impresario]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- T_PLAN Exception reporting
-- DSJ 1/29/2019
-- MS Update with two additional exceptions 4/2020
-- MS Updated per SCTASK0002086 4/2021
ALTER PROCEDURE [dbo].[LRP_EXCEPTION_PLANS]
    	@FY INT 
AS BEGIN

        SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
        SET NOCOUNT ON 

	    --Plan Exceptions � For Advancement Pipeline plans to be prompted by T_CAMPAIGN.fyear.

	    --No Ask Date
        SELECT  g.[description] AS [Campaign], 
                p.[customer_no], 
                c.[sort_name], 
                p.[last_updated_by], 
                p.[last_update_dt], 
                'No Ask date' AS [Reason]
        FROM [dbo].[T_PLAN] AS p
             INNER JOIN [dbo].[T_CUSTOMER] AS c ON p.[customer_no] = c.[customer_no]
             INNER JOIN [dbo].[T_CAMPAIGN] AS g ON p.[campaign_no] = g.[campaign_no]
             INNER JOIN [dbo].[TR_PLAN_STATUS] AS s ON p.[status] = s.[id]
        WHERE p.[type] = 7 
          AND g.[fyear] = @FY 
          AND p.[start_dt] IS NULL 
          AND p.[status] NOT IN (1,2,39)

    UNION ALL 

        --No Close Date
        SELECT	g.[description] AS [Campaign], 
                p.[customer_no], 
                c.[sort_name], 
                p.[last_updated_by], 
                p.[last_update_dt], 
                'No close date' AS [Reason]
        FROM [dbo].[T_PLAN] AS p
             INNER JOIN [dbo].[T_CUSTOMER] AS c ON p.[customer_no] = c.[customer_no]
             INNER JOIN [dbo].[T_CAMPAIGN] AS g ON p.[campaign_no] = g.[campaign_no]
             INNER JOIN [dbo].[TR_PLAN_STATUS] AS s ON p.[status] = s.[id]
        WHERE p.[type] = 7 
          AND g.[fyear] = @FY 
          AND p.[complete_by_dt] IS NULL 
          AND p.[status] NOT IN (1, 2, 39)

    UNION ALL

        --AF Designation Missing
        SELECT  g.[description] AS [Campaign], 
                p.[customer_no], 
                c.[sort_name], 
                p.[last_updated_by], 
                p.[last_update_dt], 
                'AF Designation is missing' AS [Reason]
        FROM [dbo].[T_PLAN] AS p 
             INNER JOIN [dbo].[T_CUSTOMER] AS c ON p.[customer_no] = c.[customer_no]
             INNER JOIN [dbo].[T_CAMPAIGN] AS g ON p.[campaign_no] = g.[campaign_no]
             INNER JOIN [dbo].[TR_CONT_DESIGNATION] AS d ON p.[cont_designation] = d.[id]
        WHERE p.[type] = 7 
          AND g.[fyear] = @FY
          AND g.[category] IN (28,45,7)
          AND g.[description] NOT LIKE 'Annual Giving No Memb%'
          AND p.[status] NOT IN (37,39)
          AND (p.[cont_designation] IS NULL OR p.[cont_designation] NOT IN (2,116,117)) 
    

    UNION ALL

        --Adv Pipeline Plan Should Be On Household
        SELECT  g.[description] AS [Campaign], 
                p.[customer_no], 
                c.[sort_name], 
                p.[last_updated_by], 
                p.[last_update_dt], 
                'Adv Pipeline Plan Should Be On Household' AS [Reason]
        FROM [dbo].[T_PLAN] AS p 
             INNER JOIN [dbo].[T_CAMPAIGN] AS g ON p.[campaign_no] = g.[campaign_no]
             INNER JOIN [dbo].[T_CUSTOMER] AS c ON p.[customer_no] = c.[customer_no]
        WHERE p.[type] = 7 
          AND g.[fyear] = @FY
          AND p.[customer_no] IN (SELECT [individual_customer_no] 
                                  FROM [dbo].[T_AFFILIATION]  
                                  WHERE [affiliation_type_id] = 10002)
          AND p.[campaign_no] NOT IN (2051, 793, 2166)
          AND p.[cont_designation] NOT IN (108, 110, 150)
          AND p.[status] <> 39

    UNION ALL 

        --Plan Status and Linked Contribution Do Not Match
        SELECT  g.[description] AS [Campaign], 
                p.[customer_no], 
                c.[sort_name], 
                p.[last_updated_by],
                p.[last_update_dt],
                'Plan Status and Linked Contribution Do Not Match' AS [Reason]
        FROM [dbo].[T_PLAN] AS p
             INNER JOIN [dbo].[T_CAMPAIGN] AS g ON p.[campaign_no] = g.[campaign_no]
             INNER JOIN [dbo].[TR_PLAN_STATUS] AS s ON p.[status] = s.[id]
             INNER JOIN [dbo].[T_CUSTOMER] AS c ON p.[customer_no] = c.[customer_no]
        WHERE p.[type] = 7 
          AND g.[fyear] = @FY 
          AND p.[status] NOT IN (24,35,39) 
          AND p.[cont_amt] > 0 
          AND p.[cont_designation] NOT IN (108, 110, 150)

    UNION ALL 

        --Plan Primary Worker and Linked Contribution Solicitor Do Not Match
        SELECT  g.[description] AS [Campaign], 
                p.[customer_no], 
                c.[sort_name], 
                p.[last_updated_by], 
                p.[last_update_dt], 
                'Plan Primary Solicitor and Linked Contribution Solicitor Do Not Match' AS [Reason]
        FROM [dbo].[T_CONTRIBUTION] as m
             INNER JOIN [dbo].[T_PLAN] as p ON m.[plan_no] = p.[plan_no]
             INNER JOIN [dbo].[TX_CUST_PLAN] as cp ON p.[plan_no] = cp.[plan_no]
             INNER JOIN [dbo].[T_CAMPAIGN] as g ON p.[campaign_no] = g.[campaign_no]
             INNER JOIN [dbo].[T_CUSTOMER] AS c ON p.[customer_no] = c.[customer_no]
             INNER JOIN [dbo].[TR_PLAN_STATUS] AS s ON p.[status] = s.[id]
        WHERE g.[fyear] = @FY
          AND m.[cont_amt] > 0
          AND m.[worker_customer_no] <> 2653100
          AND cp.[role_no] = 1
          AND cp.[primary_ind] = 'Y'
          AND m.[worker_customer_no] <> cp.[customer_no]
          AND p.[status] <> 39
          --CAST (m.cont_dt AS DATE) >= '2018-07-01'  

    UNION ALL 

        --Plan Primary Solicitor Not Checked Primary
        SELECT  g.[description] AS [Campaign], 
                p.customer_no, 
                c.sort_name, 
                p.last_updated_by, 
                p.last_update_dt, 
                'Plan Primary Solicitor Primary Indicator Is Not Checked' AS [Reason]
        FROM [dbo].[T_PLAN] AS p 
             INNER JOIN [dbo].[T_CAMPAIGN] AS g ON p.[campaign_no] = g.[campaign_no]
             INNER JOIN [dbo].[TR_PLAN_STATUS] AS s ON p.[status] = s.[id]
             INNER JOIN [dbo].[T_CUSTOMER] AS c ON p.[customer_no] = c.[customer_no]
             INNER JOIN [dbo].[TX_CUST_PLAN] as cp ON p.[plan_no] = cp.[plan_no]
        WHERE g.[fyear] = @FY
	      AND p.[type] = 7 
	      AND p.[cont_designation] NOT IN (108, 110, 150) 
	      AND cp.[role_no] = 1 
	      AND cp.[primary_ind] <> 'Y' 
          AND p.[status] <> 39

    UNION ALL 

        --Plan With No Assigned Primary Solicitor
        SELECT  g.[description] AS [Campaign], 
                p.[customer_no], 
                c.[sort_name], 
                p.[last_updated_by], 
                p.[last_update_dt], 
                'Plan With No Assigned Primary Solicitor' AS [Reason]
        FROM [dbo].[T_PLAN] AS p 
             INNER JOIN [dbo].[T_CUSTOMER] AS c ON p.[customer_no] = c.[customer_no]
             INNER JOIN [dbo].[T_CAMPAIGN] AS g ON p.[campaign_no] = g.[campaign_no]
             INNER JOIN [dbo].[TR_PLAN_STATUS] AS s ON p.[status] = s.[id]
        WHERE p.[type] = 7 
          AND g.[fyear] = @FY
          AND p.[cont_designation] NOT IN (108, 110, 150) 
          AND p.[plan_no] NOT IN (SELECT [plan_no] 
                                  FROM [dbo].[TX_CUST_PLAN] 
                                  WHERE [role_no] = 1)
          AND p.[status] <> 39

    UNION ALL 

        --Multiple Primary Workers
        SELECT DISTINCT g.[description] as [Campaign], 
                        p.[customer_no], 
                        c.[sort_name], 
                        p.[last_updated_by], 
                        p.[last_update_dt], 
                        'Multiple Primary Solicitors' AS [Reason] 
        FROM [dbo].[T_PLAN] AS p
             INNER JOIN [dbo].[T_CAMPAIGN] AS g ON p.[campaign_no] = g.[campaign_no]
             INNER JOIN [dbo].[T_CUSTOMER] AS c ON p.[customer_no] = c.[customer_no]
             INNER JOIN (SELECT p.plan_no, 
                                COUNT(cp.plan_no) cnt
                         FROM TX_CUST_PLAN AS cp
                              INNER JOIN [dbo].[T_PLAN] AS p ON cp.[plan_no] = p.[plan_no]
                              INNER JOIN [dbo].[T_CAMPAIGN] AS g ON p.[campaign_no] = g.[campaign_no]
                         WHERE cp.[role_no] = 1 
                           AND g.[fyear] = @FY
                         GROUP BY p.[plan_no]
                         HAVING COUNT(cp.[plan_no]) > 1) X ON X.[plan_no] = p.[plan_no]
        WHERE g.[fyear] = @FY
          AND p.[status] <> 39

    UNION ALL 

        --Multiple Secondary Workers
        SELECT  g.[description] AS [Campaign], 
                p.[customer_no], 
                c.[sort_name], 
                p.[last_updated_by], 
                p.[last_update_dt], 
                'Multiple Secondary Solicitors' AS [Reason] 
        FROM [dbo].[T_PLAN] AS p
             INNER JOIN [dbo].[T_CAMPAIGN] AS g ON p.[campaign_no] = g.[campaign_no]
             INNER JOIN [dbo].[T_CUSTOMER] AS c ON p.[customer_no] = c.[customer_no]
             INNER JOIN (SELECT p.[plan_no], 
                                COUNT(cp.[plan_no]) AS cnt 
                         FROM [dbo].[TX_CUST_PLAN] AS cp
                              INNER JOIN [dbo].[T_PLAN] AS p ON cp.[plan_no] = p.[plan_no]
                              INNER JOIN [dbo].[T_CAMPAIGN] AS g ON p.[campaign_no] = g.[campaign_no]
                         WHERE cp.[role_no] = 11 
                           AND g.[fyear] = @FY
                         GROUP BY p.[plan_no]
                         HAVING COUNT(cp.[plan_no]) > 1) X ON X.[plan_no] = p.[plan_no]
        WHERE g.[fyear] = @FY
          AND p.[status] <> 39

    UNION ALL

        --Close Date FY and CM FY Mismatch
        SELECT  g.[description] AS [Campaign], 
                p.[customer_no], 
                c.[sort_name], 
                p.[last_updated_by], 
                p.[last_update_dt],
                'Close Date FY and CM FY Mismatch' AS [Reason]
        FROM [dbo].[T_PLAN] AS p
             INNER JOIN [dbo].[T_CUSTOMER] AS c ON p.[customer_no] = c.[customer_no]
             INNER JOIN [dbo].[T_CAMPAIGN] AS g ON p.[campaign_no] = g.[campaign_no]
             INNER JOIN [dbo].[TR_PLAN_STATUS] AS s ON p.[status] = s.[id]
        WHERE p.[type] = 7 
          AND g.[fyear] = @FY 
          AND p.[cont_designation] <> 2 
          AND p.[campaign_no] <> 31 
          AND p.[status] NOT IN (1,2,39) 
          AND (SELECT [fyear] 
               FROM [dbo].[TR_Batch_Period]
               WHERE CAST(p.[complete_by_dt] AS DATE) >= [start_dt] 
                 AND CAST(p.[complete_by_dt] AS DATE) <= [end_dt]) <> g.[fyear]

    UNION ALL

        --Select Plans Should Be On Individual
        SELECT  g.[description] AS [campaign], 
                p.[customer_no], 
                c.[sort_name], 
                p.[last_updated_by], 
                p.[last_update_dt], 
                'Select Plans Should Be On Individual' AS [Reason] 
        FROM [dbo].[T_PLAN] AS p
             INNER JOIN [dbo].[T_CUSTOMER] AS c ON p.[customer_no] = c.[customer_no]
             INNER JOIN [dbo].[T_CAMPAIGN] AS g ON p.[campaign_no] = g.[campaign_no]
             INNER JOIN [dbo].[TR_PLAN_STATUS] AS s ON p.[status] = s.[id]
        WHERE c.[cust_type] = 7 
          AND p.[status] <> 39 
          AND (p.[campaign_no] IN (2051,793) 
               OR p.[cont_designation] IN (108,110)) 

    --Exceptions Below Added 4/2021
    UNION ALL

        --Linked Plan But No Contribution Solicitor
        SELECT g.[description] AS [campaign], 
               p.[customer_no], 
               ISNULL(u.[sort_name], '') AS [sort_name],
               p.[last_updated_by], 
               p.[last_update_dt],
               'Linked Plan But No Contribution Solicitor' AS [reason]
        FROM [dbo].[T_CONTRIBUTION] AS c
             INNER JOIN [dbo].[T_PLAN] AS p ON c.[plan_no] = p.[plan_no]
             LEFT OUTER JOIN [dbo].[T_CUSTOMER] AS u ON ISNULL(p.[customer_no], -1) = u.[customer_no]
             INNER JOIN [dbo].[T_CAMPAIGN] AS g ON p.[campaign_no] = g.[campaign_no]
        WHERE p.[type] = 7 
          AND g.[fyear] = @FY
          AND CAST (c.[cont_dt] AS DATE) >= '2020-07-01' 
          AND c.[customer_no] <> 2653093
          AND c.[cont_amt] > 0
          AND c.[plan_no] IS NOT NULL
          AND c.[worker_customer_no] IS NULL

    UNION ALL

        --Stew SC Linked to Plan
        SELECT g.[description],
               p.[customer_no],
               ISNULL(u.[sort_name], '') AS [sort_name],
               p.[last_updated_by],
               p.[last_update_dt],
               'Stew SC Linked to Plan' AS [reason]
        FROM [dbo].[T_CREDITEE] AS r
             LEFT OUTER JOIN [dbo].[T_CUSTOMER] AS u ON ISNULL(r.[creditee_no], -1) = u.[customer_no]
             INNER JOIN [dbo].[T_CONTRIBUTION] c ON r.[ref_no] = c.[ref_no]
             INNER JOIN [dbo].[T_PLAN] AS p ON c.[plan_no] = p.[plan_no]
             INNER JOIN [dbo].[T_CAMPAIGN] AS g ON p.[campaign_no] = g.[campaign_no]
        WHERE g.[fyear] = @FY
          AND CAST(c.[cont_dt] AS DATE) >= '2020-07-01'
          AND r.[creditee_type] IN (10, 16)
          AND c.[plan_no] IS NOT NULL
          AND c.[custom_1] <> 'Primary Soft Credit'
          
    UNION ALL

        --Lead Org is (none)
        SELECT g.[description] AS [campaign],
               p.[customer_no], 
               ISNULL(u.[sort_name], '') AS [sort_name],
               p.last_updated_by, 
               p.[last_update_dt],
               'Lead Org is (none)' AS [reason]
        FROM [dbo].[T_PLAN] AS p
             INNER JOIN [dbo].[T_CAMPAIGN] AS g ON p.[campaign_no] = g.[campaign_no]
             LEFT OUTER JOIN [dbo].[T_CUSTOMER] AS u ON ISNULL(p.[customer_no], -1) = u.[customer_no]
        WHERE p.[type] = 7 
          AND g.[fyear] = @FY
          AND p.[custom_2] = 701 

    UNION ALL

        --Pipeline Type But Campaign Issue
        SELECT g.description AS [campaign], 
               p.customer_no, 
               ISNULL(u.[sort_name], '') AS [sort_name],
               p.last_updated_by,
               p.[last_update_dt],
               'Pipeline Type But Campaign Issue' AS [reason]
        FROM [dbo].[T_PLAN] AS p
             INNER JOIN [dbo].[T_CAMPAIGN] AS g ON p.[campaign_no] = g.[campaign_no]
             LEFT OUTER JOIN [dbo].[T_CUSTOMER] AS u ON p.[customer_no] = u.[customer_no]
        WHERE p.[type] = 7
          AND p.[campaign_no] <> 2166 
          AND p.[campaign_no] NOT IN (SELECT [campaign_no] 
                                      FROM [dbo].[T_CAMPAIGN] 
                                      WHERE [camp_type] IN ('C','E'))

    UNION ALL

        --Not Pipeline Type But Campaign Issue
        SELECT g.[description] AS [campaign], 
               p.customer_no, 
               ISNULL(u.[sort_name], '') AS [sort_name],
               p.last_updated_by,
               p.[last_update_dt],
               'Not Pipeline Type But Campaign Issue' AS [reason]
        FROM [dbo].[T_PLAN] AS p
             INNER JOIN [dbo].[T_CAMPAIGN] AS g ON p.[campaign_no] = g.[campaign_no]
             LEFT OUTER JOIN [dbo].[T_CUSTOMER] AS u ON ISNULL(p.[customer_no], -1) = u.[customer_no]
        WHERE p.[type] <> 7 
          AND p.[campaign_no] IN (SELECT [campaign_no] 
                                  FROM [dbo].[T_CAMPAIGN] 
                                  WHERE [camp_type] IN ('C','E')
                                    AND [campaign_no] NOT IN (2166, 2063))

    UNION ALL

        SELECT g.[description] AS [campaign], 
               p.customer_no, 
               ISNULL(u.[sort_name], '') AS [sort_name],      
               p.last_updated_by, 
               p.[last_update_dt],
               'DELETE Worker on Plan' AS [reason]
        FROM [dbo].[TX_CUST_PLAN] AS cp 
             INNER JOIN [dbo].[T_PLAN] as p ON cp.[plan_no] = p.[plan_no]
             INNER JOIN [dbo].[T_CAMPAIGN] AS g ON p.[campaign_no] = g.[campaign_no]
             LEFT OUTER JOIN [dbo].[T_CUSTOMER] AS u ON ISNULL(p.[customer_no], -1) = u.[customer_no]
        WHERE cp.[role_no] = 10 
          AND cp.[customer_no] NOT IN (3212543, 3742863)




END
GO

EXECUTE [dbo].[LRP_EXCEPTION_PLANS] @FY = 2022