USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_DEFERRED_ADMISSIONS]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LRP_DEFERRED_ADMISSIONS];
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LRP_DEFERRED_ADMISSIONS]
        @report_start_dt DATETIME,
        @date_field VARCHAR(25) = 'Payment Date',     --Can be: Payment Date or Batch Post Date
        @this_months_perfs CHAR(1) = 'N'              --Only performances in same month as report_start_dt
AS BEGIN

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    SET NOCOUNT ON;

    /*  Check Parameters  */
        
        SELECT @report_start_dt = convert(DATE,@report_start_dt);
        
        SELECT @date_field = ISNULL(@date_field,'Payment Date');

    /*  Temporary Table  */

        IF OBJECT_ID('tempdb..#deferred_final_table') IS NOT NULL DROP TABLE [#deferred_final_table];

        CREATE TABLE #deferred_final_table ([payment_no] int, [sequence_no] int, [payment_dt] datetime, [payment_date] char(10), [posted_dt] DATETIME, [posted_date] CHAR(10), 
                                            [performance_no] int, [performance_dt] datetime, [performance_date] char(10), [title_no] INT, [title_name] varchar(30), [production_name] VARCHAR(30), 
                                            [production_name_long] varchar(255), [payment_type_name] varchar(30), [payment_method_name] varchar(30), [payment_amt] decimal(18,2), [order_no] int, 
                                            [check_no] varchar(30), [check_name] varchar(100), [batch_no] int, [customer_no] int, [posted_status] char(1), [tckt_tran_flag] char(1), [gl_account] varchar(30), 
                                            [gl_description] varchar(30), [pmt_notes] varchar(750), [customer_name] VARCHAR(160), [report_message] varchar(100));

        --Unique Index insures that no payment gets counted more than once...
        IF NOT EXISTS (SELECT * FROM tempdb.sys.indexes WHERE name = N'uq_deferred_admissions_payment_no_sequence_no')
            CREATE UNIQUE NONCLUSTERED INDEX [uq_deferred_admissions_payment_no_sequence_no] ON [#deferred_final_table] ([payment_no] ASC, sequence_no ASC);

    /*  Get payment lists  */
    
        IF @date_field = 'Batch Posted Date' BEGIN

            WITH [CTE_GL_NUMBERS] ([payment_no], [sequence_no], [gl_account_no], [gl_account_description])
            AS (SELECT [payment_no], 
                       [sequence_no], 
                       [gl_account_no], 
                       [gl_account_description]
                FROM [dbo].[LV_PAYMENT_GL_NUMBERS]
                WHERE [payment_no] IN (SELECT [payment_no]
                                       FROM [dbo].[LV_PERFORMANCE_PAYMENT_INFO]
                                       WHERE [posted_dt] < @report_start_dt
                                         AND [perf_dt] >= @report_start_dt))
            INSERT INTO [#deferred_final_table] ([payment_no], [sequence_no], [payment_dt], [payment_date], [posted_dt], [posted_date], [performance_no], [performance_dt],
                                                 [performance_date], [title_no], [title_name], [production_name], [production_name_long], [payment_type_name], [payment_method_name],
                                                 [payment_amt], [order_no], [check_no], [check_name], [batch_no], [customer_no], [posted_status], [tckt_tran_flag], [gl_account],
                                                 [gl_description], [pmt_notes], [customer_name], [report_message])
            SELECT pay.[payment_no],
                   pay.[sequence_no],
                   pay.[pmt_dt],
                   pay.[pmt_date],
                   pay.[posted_dt],
                   pay.[posted_date],
                   pay.[perf_no],
                   pay.[perf_dt], 
                   pay.[perf_date],
                   pay.[title_no],
                   pay.[title_name],
                   pay.[production_name], 
                   pay.[production_name_long],
                   pay.[pmt_type_name],
                   pay.[pmt_method_name],
                   pay.[pmt_amt], 
                   pay.[order_no],
                   pay.[check_no],
                   pay.[check_name],
                   pay.[batch_no],
                   pay.[customer_no],
                   pay.[posted_status],
                   pay.[tckt_tran_flag],
                   cte.[gl_account_no],
                   cte.[gl_account_description],
                   pay.[pmt_notes],
                   nam.display_name,
                   ''
            FROM [dbo].[LV_PERFORMANCE_PAYMENT_INFO] AS pay
                  LEFT OUTER JOIN [dbo].[FT_CONSTITUENT_DISPLAY_NAME]() AS nam ON nam.[customer_no] = pay.[customer_no]
                  LEFT OUTER JOIN [CTE_GL_NUMBERS] AS cte ON cte.[payment_no] = pay.[payment_no] AND cte.[sequence_no] = pay.[sequence_no]
            WHERE pay.[posted_dt] < @report_start_dt
              AND pay.[perf_dt] >= @report_start_dt
              AND pay.[title_name] <> 'Membership';
        
        END ELSE BEGIN

            WITH [CTE_GL_NUMBERS] ([payment_no], [sequence_no], [gl_account_no], [gl_account_description])
            AS (SELECT [payment_no], 
                       [sequence_no], 
                       [gl_account_no], 
                       [gl_account_description]
                FROM [dbo].[LV_PAYMENT_GL_NUMBERS]
                WHERE [payment_no] IN (SELECT [payment_no]
                                       FROM [dbo].[LV_PERFORMANCE_PAYMENT_INFO]
                                       WHERE [pmt_dt] < @report_start_dt
                                         AND [perf_dt] >= @report_start_dt))
            INSERT INTO [#deferred_final_table] ([payment_no], [sequence_no], [payment_dt], [payment_date], [posted_dt], [posted_date], [performance_no], [performance_dt],
                                                 [performance_date], [title_no], [title_name], [production_name], [production_name_long], [payment_type_name], [payment_method_name],
                                                 [payment_amt], [order_no], [check_no], [check_name], [batch_no], [customer_no], [posted_status], [tckt_tran_flag], [gl_account],
                                                 [gl_description], [pmt_notes], [customer_name], [report_message])
            SELECT pay.[payment_no],
                   pay.[sequence_no],
                   pay.[pmt_dt],
                   pay.[pmt_date],
                   pay.[posted_dt],
                   pay.[posted_date],
                   pay.[perf_no],
                   pay.[perf_dt], 
                   pay.[perf_date],
                   pay.[title_no],
                   pay.[title_name],
                   pay.[production_name], 
                   pay.[production_name_long],
                   pay.[pmt_type_name],
                   pay.[pmt_method_name],
                   pay.[pmt_amt], 
                   pay.[order_no],
                   pay.[check_no],
                   pay.[check_name],
                   pay.[batch_no],
                   pay.[customer_no],
                   pay.[posted_status],
                   pay.[tckt_tran_flag],
                   cte.[gl_account_no],
                   cte.[gl_account_description],
                   pay.[pmt_notes],
                   nam.display_name,
                   ''
            FROM [dbo].[LV_PERFORMANCE_PAYMENT_INFO] AS pay
                  LEFT OUTER JOIN [dbo].[FT_CONSTITUENT_DISPLAY_NAME]() AS nam ON nam.[customer_no] = pay.[customer_no]
                  LEFT OUTER JOIN [CTE_GL_NUMBERS]  AS cte ON cte.[payment_no] = pay.[payment_no] AND cte.[sequence_no] = pay.[sequence_no]
            WHERE pay.[pmt_dt] < @report_start_dt
              AND pay.[perf_dt] >= @report_start_dt
              AND pay.[title_name] <> 'Membership';

        END;

    /* If only this month's performances wanted, delete the others  */

        IF @this_months_perfs = 'Y'
            DELETE FROM [#deferred_final_table]
            WHERE DATEDIFF(MONTH,[performance_dt], @report_start_dt) <> 0;

    /*  Populate production name where no long title was found  */    

        UPDATE [#deferred_final_table] SET [production_name_long] = [production_name] WHERE [production_name_long] = '';

    FINISHED:

        /*  Get final data set to pass to the report  */
                
            SELECT [payment_no], 
                   [sequence_no], 
                   [payment_dt], 
                   [payment_date], 
                   [posted_dt],
                   [posted_date],
                   [performance_no], 
                   [performance_dt], 
                   [performance_date], 
                   [title_name], 
                   [production_name_long], 
                   [payment_type_name], 
                   [payment_method_name], 
                   [payment_amt], 
                   [order_no], 
                   [check_no], 
                   [check_name], 
                   [batch_no], 
                   [customer_no], 
                   [posted_status], 
                   [tckt_tran_flag],
                   [gl_account], 
                   [gl_description], 
                   REPLACE(REPLACE([pmt_notes], CHAR(10), ' '), CHAR(13), ' ') AS [pmt_notes], 
                   [customer_name],
                   [report_message]
            FROM [#deferred_final_table];

    DONE:

        /*  Destroy Temp Table  */
           
            IF OBJECT_ID('tempdb..#deferred_final_table') IS NOT NULL DROP TABLE [#deferred_final_table];

END
GO

GRANT EXECUTE ON [dbo].[LRP_DEFERRED_ADMISSIONS] TO impusers;
GO

EXECUTE [dbo].[LRP_DEFERRED_ADMISSIONS] '8-1-2019', 'Payment Date', 'N'
--EXECUTE [dbo].[LRP_DEFERRED_ADMISSIONS] @report_start_dt = '6-1-2019', @date_field = 'Payment Date', @this_months_perfs = 'Y'



    /*  Update GL Number using the new function created on 8-29-2019  */

        --UPDATE [#deferred_final_table] 
        --SET [gl_account] = [dbo].[LFS_GET_PAYMENT_GL_ACCOUNT] ([payment_no], [sequence_no], 'gl_no')
        ----WHERE [gl_account] = 'Unknown Account'

        --UPDATE [#deferred_final_table] 
        --SET [gl_description] = [dbo].[LFS_GET_PAYMENT_GL_ACCOUNT] ([payment_no], [sequence_no], 'gl_descrip')
        ----WHERE [gl_account] = 'Unknown Account'
        

    --/*  Update GL Number using the new function created on 6-1-2019  */

    --    UPDATE [#deferred_final_table] 
    --    SET [gl_account] = [dbo].[LFS_GET_PERFORMANCE_GL_ACCOUNT] ([performance_no], 0)
    --    WHERE [gl_account] = 'Unknown Account'


