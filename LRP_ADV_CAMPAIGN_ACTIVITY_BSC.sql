USE impresario
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_ADV_CAMPAIGN_ACTIVITY_BSC]') AND type IN (N'P', N'PC'))
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_ADV_CAMPAIGN_ACTIVITY_BSC] AS' 
GO

EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LRP_ADV_CAMPAIGN_ACTIVITY_BSC] TO [impusers], [tessitura_app]'
GO

/*      LRP_ADV_CAMPAIGN_ACTIVITY_BSC


*/
ALTER PROCEDURE [dbo].[LRP_ADV_CAMPAIGN_ACTIVITY_BSC]
        @fiscal_year INT = NULL
WITH RECOMPILE AS BEGIN

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

    /* Procedure Variables  */

        --Determine current fiscal year, fiscal start date, fiscal end date, and current date
        DECLARE @fiscal_start_dt DATETIME = NULL, @fiscal_end_dt DATETIME = NULL
        DECLARE @current_dt DATETIME = GETDATE()
        DECLARE @donor_count INT = 0

        DECLARE @worker_list TABLE ([worker_no] INT NOT NULL DEFAULT (0))

    /*  Temporary Tables  */
                                    
        IF OBJECT_ID('tempdb..#bsc_data') IS NOT NULL DROP TABLE #bsc_data

        CREATE TABLE [#bsc_data] ([ref_no] INT NOT NULL DEFAULT (0), 
                                  [customer_no] INT NOT NULL DEFAULT (0),   
                                  [cont_type] VARCHAR(30) NOT NULL DEFAULT (''), 
                                  [cont_dt] DATETIME NULL, 
                                  [cont_fy] INT NOT NULL DEFAULT (0),
                                  [recd_amt] DECIMAL(18,2) NOT NULL DEFAULT (0.0), 
                                  [cont_amt] DECIMAL(18,2) NOT NULL DEFAULT (0.0), 
                                  [cont_current] DECIMAL(18,2) NOT NULL DEFAULT (0.0), 
                                  [cont_previous]DECIMAL(18,2) NOT NULL DEFAULT (0.0), 
                                  [goal_amt]DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                  [fyear] INT NOT NULL DEFAULT (0), 
                                  [specific_goal] VARCHAR(75) NOT NULL DEFAULT (''), 
                                  [intermediate_goal] VARCHAR(75) NOT NULL DEFAULT (''),
                                  [pillar_goal] VARCHAR(75) NOT NULL DEFAULT (''), 
                                  [sort_name] VARCHAR(100) NOT NULL DEFAULT (''),
                                  [donor_count] INT NOT NULL DEFAULT (0))

    /*  Check Parameters  */

        IF @fiscal_year IS NULL SELECT @fiscal_year = dbo.LF_GetFiscalYear(GETDATE())
        
        SELECT @fiscal_start_dt = [dbo].[LF_GetFiscalYearStartDt](@fiscal_year)

        SELECT @fiscal_end_dt = [dbo].[LF_GetFiscalYearEndDt](@fiscal_year)
        
        IF @current_dt > @fiscal_end_dt SELECT @current_dt = @fiscal_end_dt

    /*  Get Goal Data  */

        INSERT INTO [#bsc_data] ([ref_no], [customer_no], [cont_type], [cont_dt], [cont_fy], [recd_amt], [cont_amt],
                                 [goal_amt], [fyear], [specific_goal], [intermediate_goal], [pillar_goal], [sort_name])
        SELECT 0 AS [ref_no],
               0 AS [customer_no],
               'G' AS [cont_type],
               GETDATE() AS [cont_dt],
               dbo.LF_GetFiscalYear(GETDATE()) AS [cont_fy],
               0.0 AS [recd_amt],
               0.0 AS [cont_amt],
               spg.[goal] AS [goal_amt],
               0 AS [fyear],
               spg.[description] "specific goal", 
               ing.[description] "intermediate goal", 
               pig.[description] "pillar goal",
               '' AS [sort_name]
        FROM [dbo].[LTR_CM_CATEGORY1] AS spg 
             INNER JOIN [dbo].[LTR_CM_LEVEL] AS ing ON spg.[cm_level_no] = ing.[id]
             INNER JOIN [dbo].[LTR_CM_LEVEL] AS pig ON ing.[cm_level_grp] = pig.[id]
        WHERE LEFT(spg.[description],3) = 'BSC'

    /*  Get Contribution Data  */
        
        INSERT INTO [#bsc_data] ([ref_no], [customer_no], [cont_type], [cont_dt], [cont_fy], [recd_amt], [cont_amt],
                                 [goal_amt], [fyear], [specific_goal], [intermediate_goal], [pillar_goal], [sort_name])
              SELECT con.[ref_no], 
                     cus.[customer_no],  
                     con.[cont_type], 
                     con.[cont_dt], 
                     dbo.LF_GetFiscalYear(con.[cont_dt]) AS [cont_fy],
                     con.[recd_amt], 
                     con.[cont_amt], 
                     0.00 AS [goal_amt],
                     cmp.[fyear],  
                     spg.[description] AS [specific_goal],
                     ing.[description] AS [intermediate_goal],
                     pig.[description] AS [pillar_goal],
                     cus.[sort_name]
              FROM [DBO].[T_CONTRIBUTION] AS con
                   INNER JOIN [dbo].[LTR_FUND_DETAIL] AS fnd ON con.[fund_no] = fnd.[fund_no]
                   INNER JOIN [dbo].[T_CAMPAIGN] AS cmp ON con.[campaign_no] = cmp.[campaign_no]
                   INNER JOIN [dbo].T_CUSTOMER AS cus ON con.[customer_no] = cus.customer_no
                   LEFT OUTER JOIN [dbo].[LTR_CM_CATEGORY1] AS spg ON fnd.cm_category1 = spg.id
                   LEFT OUTER JOIN [dbo].[LTR_CM_LEVEL] AS ing ON spg.cm_level_no = ing.id
                   LEFT OUTER JOIN [dbo].[LTR_CM_LEVEL] AS pig ON ing.cm_level_grp = pig.id
              WHERE con.[cont_type] IN ('G', 'P') 
                AND con.[custom_1] in ('(none)', 'Matching Gift Credit') 
                AND con.[cont_amt] > 0 
                AND fnd.[overall_cm_no] = 13

        UNION SELECT con.[ref_no], 
                     cus.[customer_no],   
                     con.[cont_type], 
                     con.[cont_dt], 
                     dbo.LF_GetFiscalYear(con.[cont_dt]) AS [cont_fy],
                     con.[recd_amt], 
                     con.[cont_amt], 
                     0.0 AS [goal_amt],
                     cmp.[fyear], 
                     spg.[description] [specific_goal], 
                     ing.[description] [intermediate_goal],
                     pig.[description] [pillar_goal], 
                     cus.[sort_name]
              FROM [dbo].[T_CONTRIBUTION] AS con
                   INNER JOIN [dbo].[T_CREDITEE] AS crd on con.[ref_no] = crd.[ref_no]
                   INNER JOIN [dbo].[LTR_FUND_DETAIL] AS fnd ON con.[fund_no] = fnd.[fund_no]
                   INNER JOIN [dbo].[T_CUSTOMER] AS cus ON crd.[creditee_no] = cus.[customer_no]
                   INNER JOIN [dbo].[T_CAMPAIGN] AS cmp ON con.campaign_no = cmp.campaign_no
                   LEFT OUTER JOIN [dbo].[LTR_CM_CATEGORY1] AS spg ON fnd.cm_category1 = spg.id
                   LEFT OUTER JOIN [dbo].[LTR_CM_LEVEL] AS ing ON spg.cm_level_no = ing.id
                   LEFT OUTER JOIN [dbo].[LTR_CM_LEVEL] AS pig ON ing.cm_level_grp = pig.id
              WHERE con.cont_type IN ('G', 'P')
                AND con.custom_1 = 'Primary Soft Credit' 
                AND crd.creditee_type in ('12', '15', '5') 
                AND con.cont_amt > 0 
                AND fnd.[overall_cm_no] = 13;

                        --/*  SIMULATED DATA - COMMENT OR REMOVE WHEN REPORT GOES LIVE  */
                        --INSERT INTO [#bsc_data] ([customer_no], [cont_type], [cont_dt], [cont_fy], [recd_amt], [cont_amt],
                        --                         [goal_amt], [fyear], [specific_goal], [intermediate_goal], [pillar_goal])
                        --VALUES (1, 'P', '8-1-2020', 2021, 0.0, 1000.00, 0.00, 2021, 'BScC_Endwmnt_Community Specific', 'BScC_Endwmnt_Community Intermediate', 'BScC_Endowment Pillar'), 
                        --       (2, 'P', '7-1-2020', 2021, 0.0, 2000.00, 0.00, 2021, 'BScC_Endwmnt_Community Specific', 'BScC_Endwmnt_Community Intermediate', 'BScC_Endowment Pillar'), 
                        --       (3, 'P', '6-1-2020', 2020, 0.0, 11299.00, 0.00, 2021, 'BScC_Endwmnt_Community Specific', 'BScC_Endwmnt_Community Intermediate', 'BScC_Endowment Pillar'),
                        --       (4, 'P', '8-1-2020', 2021, 0.0, 10000.00, 0.00, 2021, 'BScC_Endwmnt_Depreciation Specific', 'BScC_Endwmnt_Depreciation Intermediate', 'BScC_Endowment Pillar'), 
                        --       (5, 'P', '7-1-2020', 2021, 0.0, 20000.00, 0.00, 2021, 'BScC_Endwmnt_Depreciation Specific', 'BScC_Endwmnt_Depreciation Intermediate', 'BScC_Endowment Pillar'), 
                        --       (6, 'P', '6-1-2020', 2020, 0.0, 41399.00, 0.00, 2021, 'BScC_Endwmnt_Depreciation Specific', 'BScC_Endwmnt_Depreciation Intermediate', 'BScC_Endowment Pillar'),
                        --       (7, 'P', '8-1-2020', 2021, 0.0, 51000.00, 0.00, 2021, 'BScC_Endwmnt_Programming Specific', 'BScC_Endwmnt_Programming Intermediate', 'BScC_Endowment Pillar'), 
                        --       (8, 'P', '7-1-2020', 2021, 0.0, 61000.00, 0.00, 2021, 'BScC_Endwmnt_Programming Specific', 'BScC_Endwmnt_Programming Intermediate', 'BScC_Endowment Pillar'), 
                        --       (9, 'P', '6-1-2020', 2020, 0.0, 75499.00, 0.00, 2021, 'BScC_Endwmnt_Programming Specific', 'BScC_Endwmnt_Programming Intermediate', 'BScC_Endowment Pillar'),
                        --       (11, 'P', '8-1-2020', 2021, 0.0, 55550.00, 0.00, 2021, 'BScC_Covid-19 Exhibits Specific', 'BScC_Covid-19 Exhibits Intermediate', 'BScC_Exhibits Pillar'), 
                        --       (12, 'P', '7-1-2020', 2021, 0.0, 43245.00, 0.00, 2021, 'BScC_Covid-19 Exhibits Specific', 'BScC_Covid-19 Exhibits Intermediate', 'BScC_Exhibits Pillar'), 
                        --       (13, 'P', '6-1-2020', 2020, 0.0, 32563.00, 0.00, 2021, 'BScC_Covid-19 Exhibits Specific', 'BScC_Covid-19 Exhibits Intermediate', 'BScC_Exhibits Pillar'),
                        --       (14, 'P', '8-1-2020', 2021, 0.0, 12345.00, 0.00, 2021, 'BScC_Covid-19 Exhibits_EF', 'BScC_Covid-19 Exhibits Intermediate', 'BScC_Exhibits Pillar'), 
                        --       (15, 'P', '7-1-2020', 2021, 0.0, 98765.00, 0.00, 2021, 'BScC_Covid-19 Exhibits_EF', 'BScC_Covid-19 Exhibits Intermediate', 'BScC_Exhibits Pillar'), 
                        --       (16, 'P', '6-1-2020', 2020, 0.0, 56789.00, 0.00, 2021, 'BScC_Covid-19 Exhibits_EF', 'BScC_Covid-19 Exhibits Intermediate', 'BScC_Exhibits Pillar'),
                        --       (17, 'P', '8-1-2020', 2021, 0.0, 23456.00, 0.00, 2021, 'BScC_Exhibits EF', 'BScC_Exhibits Intermediate', 'BScC_Exhibits Pillar'), 
                        --       (18, 'P', '7-1-2020', 2021, 0.0, 74125.00, 0.00, 2021, 'BScC_Exhibits EF', 'BScC_Exhibits Intermediate', 'BScC_Exhibits Pillar'), 
                        --       (19, 'P', '6-1-2020', 2020, 0.0, 65893.00, 0.00, 2021, 'BScC_Exhibits EF', 'BScC_Exhibits Intermediate', 'BScC_Exhibits Pillar'),
                        --       (20, 'P', '8-1-2020', 2021, 0.0, 85214.00, 0.00, 2021, 'BScC_Exhibits Specific', 'BScC_Exhibits Intermediate', 'BScC_Exhibits Pillar'), 
                        --       (21, 'P', '7-1-2020', 2021, 0.0, 85236.00, 0.00, 2021, 'BScC_Exhibits Specific', 'BScC_Exhibits Intermediate', 'BScC_Exhibits Pillar'), 
                        --       (22, 'P', '6-1-2020', 2020, 0.0, 45682.00, 0.00, 2021, 'BScC_Exhibits Specific', 'BScC_Exhibits Intermediate', 'BScC_Exhibits Pillar'),
                        --       (23, 'P', '8-1-2020', 2021, 0.0, 85214.00, 0.00, 2021, 'BScC_Boston Sci Cmn Studio EF', 'BScC_Theater Intermediate', 'BScC_Theater Pillar'), 
                        --       (23, 'P', '7-1-2020', 2021, 0.0, 85236.00, 0.00, 2021, 'BScC_Boston Sci Cmn Studio EF', 'BScC_Theater Intermediate', 'BScC_Theater Pillar'), 
                        --       (22, 'P', '6-1-2020', 2020, 0.0, 45682.00, 0.00, 2021, 'BScC_Boston Sci Cmn Studio EF', 'BScC_Theater Intermediate', 'BScC_Theater Pillar'),
                        --       (24, 'P', '8-1-2020', 2021, 0.0, 85214.00, 0.00, 2021, 'BScC_Boston Sci Cmn Studio Specific', 'BScC_Theater Intermediate', 'BScC_Theater Pillar'), 
                        --       (24, 'P', '7-1-2020', 2021, 0.0, 85236.00, 0.00, 2021, 'BScC_Boston Sci Cmn Studio Specific', 'BScC_Theater Intermediate', 'BScC_Theater Pillar'), 
                        --       (24, 'P', '6-1-2020', 2020, 0.0, 45682.00, 0.00, 2021, 'BScC_Boston Sci Cmn Studio Specific', 'BScC_Theater Intermediate', 'BScC_Theater Pillar'),
                        --       (25, 'P', '8-1-2020', 2021, 0.0, 85214.00, 0.00, 2021, 'BScC_Theater EF', 'BScC_Theater Intermediate', 'BScC_Theater Pillar'), 
                        --       (26, 'P', '7-1-2020', 2021, 0.0, 85236.00, 0.00, 2021, 'BScC_Theater EF', 'BScC_Theater Intermediate', 'BScC_Theater Pillar'), 
                        --       (27, 'P', '6-1-2020', 2020, 0.0, 45682.00, 0.00, 2021, 'BScC_Theater EF', 'BScC_Theater Intermediate', 'BScC_Theater Pillar'),
                        --       (28, 'P', '8-1-2020', 2021, 0.0, 85214.00, 0.00, 2021, 'BScC_Theater Specific', 'BScC_Theater Intermediate', 'BScC_Theater Pillar'), 
                        --       (29, 'P', '7-1-2020', 2021, 0.0, 85236.00, 0.00, 2021, 'BScC_Theater Specific', 'BScC_Theater Intermediate', 'BScC_Theater Pillar'), 
                        --       (30, 'P', '6-1-2020', 2020, 0.0, 45682.00, 0.00, 2021, 'BScC_Theater Specific', 'BScC_Theater Intermediate', 'BScC_Theater Pillar');

    
    /*  Update donor counts with total on the goal record for each specific goal  */

        SELECT @donor_count = COUNT(DISTINCT [customer_no]) FROM [#bsc_data] WHERE [customer_no] > 0
        --SELECT * FROM [#bsc_data]

        INSERT INTO [#bsc_data] ([fyear], [specific_goal], [intermediate_goal], [pillar_goal], [donor_count])
        VALUES (@fiscal_year, 'BScC_Ctr_Data Sci_ExhPrg Specific',  'BScC_Ctr_Data Sci Intermediate', 'BScC_The Centers Pillar', @donor_count)

        --Changed from because goal no longer exists
        --INSERT INTO [#bsc_data] ([fyear], [specific_goal], [intermediate_goal], [pillar_goal], [donor_count])
        --VALUES (@fiscal_year, 'BScC_Theater EF',  'BScC_Theater Intermediate', 'BScC_Theater Pillar', @donor_count)

        --Changed from because goal no longer exists
        --INSERT INTO [#bsc_data] ([fyear], [specific_goal], [intermediate_goal], [pillar_goal], [donor_count])
        --VALUES (@fiscal_year, 'BScC_Endwmnt_Community Specific',  'BScC_Endwmnt_Community Intermediate', 'BScC_Endowment Pillar', @donor_count)

    /*  Update Fiscal Year Totals  */
           
        UPDATE [#bsc_data] 
        SET [cont_current] = [cont_amt]
        WHERE [cont_fy] = @fiscal_year

        UPDATE [#bsc_data] 
        SET [cont_previous] = [cont_amt]
        WHERE [cont_fy] < @fiscal_year

        UPDATE [#bsc_data]
        SET [cont_amt] = 0.00
        WHERE [cont_fy] > @fiscal_year

    FINISHED:

        /*  Get Final Data Set  */

        SELECT [pillar_goal],
               [intermediate_goal],
               [specific_goal],
               SUM([donor_count]) AS [donor_count],
               SUM([recd_amt]) AS [recd_amt], 
               SUM([cont_amt]) AS [cont_amt],
               SUM([cont_current]) AS [cont_current],
               SUM([cont_previous]) AS [cont_previous],
               SUM([goal_amt]) AS [goal_amt]
        FROM [#bsc_data]
        GROUP BY [pillar_goal], [intermediate_goal], [specific_goal]
        ORDER BY [pillar_goal], [intermediate_goal], [specific_goal]

        --SELECT [pillar_goal],[intermediate_goal],[specific_goal],1 AS [donor_count],[recd_amt], [cont_amt],[cont_current],[cont_previous],[goal_amt] FROM [#bsc_data] ORDER BY [pillar_goal], [intermediate_goal], [specific_goal]

    DONE:

        IF OBJECT_ID('tempdb..#bsc_data') IS NOT NULL DROP TABLE #bsc_data

END
GO


     EXECUTE [dbo].[LRP_ADV_CAMPAIGN_ACTIVITY_BSC] @fiscal_year = 2021

