USE [impresario]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO	


ALTER PROCEDURE [dbo].[LRP_TODAYS_UPCOMING_EVENTS_CAPACITY]
AS
-- ==============================================================================================================
-- DSJ 2016/04/18 
-- This will provide capacity information for all of today's remaining shows
-- ==============================================================================================================

BEGIN

	SELECT 
		CAST(cap.title_name AS nvarchar(255)) AS Venue,
		CAST(cap.production_name_long AS NVARCHAR(255)) AS Title,
		CAST(cap.performance_time_display AS NVARCHAR(100)) AS [Show Time],
		CAST(ISNULL(cap.available, 0) AS NVARCHAR(10)) AS Available,
		CAST(FLOOR(ISNULL(cap.available, 0) * .90) AS NVARCHAR(10)) AS ForSaleRegularAttendance,
		CAST(FLOOR(ISNULL(cap.available, 0) * .75) AS NVARCHAR(10)) AS ForSalePeakAttendance
	FROM [dbo].[LV_PERFORMANCE_CAPACITY_AVAILABLITY] cap
	WHERE DATEDIFF(d, cap.performance_date, GETDATE()) = 0 
		AND CAST(cap.performance_date as datetime) + cast(cap.performance_time as datetime) > GETDATE()
		AND cap.title_name IN 
		('Mugar Omni Theater', 'Hayden Planetarium','Special Exhibits', 
		'Butterfly Garden', '4D-Theater', 'Thrill Ride 360')
	ORDER BY Venue, performance_time

END
