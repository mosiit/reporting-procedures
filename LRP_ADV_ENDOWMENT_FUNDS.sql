USE [impresario]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[LRP_ADV_ENDOWMENT_FUNDS]
AS

SET NOCOUNT ON 
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

DECLARE @Endowments TABLE 
(
	[EndowFundNumber] [VARCHAR](255) NULL,
	[EndowFund_custNum] [INT] NOT NULL,
	[EndowFundName] [VARCHAR](55) NOT NULL,
	[EndowFundSortName] [VARCHAR](55) NULL,
	[EndowMailingProgramName] [VARCHAR](70) NULL,
	[contact_custNum] [INT] NULL,
	[contact_fname] [VARCHAR](20) NULL,
	[contact_lname] [VARCHAR](55) NULL,
	[contact_displayName] [VARCHAR](160) NULL,
	[contact_sortName] [VARCHAR](55) NULL,
	[AddressFlag] [VARCHAR](19) NULL,
	[street1] [VARCHAR](64) NULL,
	[street2] [VARCHAR](64) NULL,
	[street3] [VARCHAR](64) NULL,
	[city] [VARCHAR](30) NULL,
	[state] [VARCHAR](20) NULL,
	[postal_code] [VARCHAR](255) NULL
);


WITH EndowFundContact
AS
(
	SELECT aff.group_customer_no AS EndowFund_custNum, 
		aff.individual_customer_no contact_custNum, 
		cust.fname AS contact_fname, 
		cust.lname contact_lname,
		contactName.display_name AS contact_displayName, 
		contactName.sort_name AS contact_sortName
	FROM dbo.T_AFFILIATION aff
	INNER JOIN dbo.FT_CONSTITUENT_DISPLAY_NAME() AS contactName 
		ON aff.individual_customer_no = contactName.customer_no
	INNER JOIN dbo.T_CUSTOMER cust
		ON aff.individual_customer_no = cust.customer_no
	WHERE aff.affiliation_type_id = 10030 --  Endowment Fund Contact
)
INSERT INTO @Endowments
(
    EndowFundNumber,
    EndowFund_custNum,
    EndowFundName,
    EndowFundSortName,
    EndowMailingProgramName,
    contact_custNum,
    contact_fname,
    contact_lname,
    contact_displayName,
    contact_sortName
)
SELECT 
	p.custom_10 AS EndowFundNumber,
	c.customer_no AS EndowFund_custNum,
	c.lname AS EndowFundName, 
	c.sort_name EndowFundSortName,
	prog.cust_pname AS EndowMailingProgramName, 
	EFC.contact_custNum, 
	EFC.contact_fname,
	EFC.contact_lname,
	EFC.contact_displayName, 
	EFC.contact_sortName
FROM dbo.T_PLAN p
INNER JOIN dbo.T_CUSTOMER c
	ON c.customer_no = p.customer_no
LEFT JOIN EndowFundContact EFC
	ON EFC.EndowFund_custNum = c.customer_no
LEFT JOIN dbo.TX_CUST_PROGRAM prog 
	ON prog.customer_no = efc.contact_custNum
	AND prog.program_no = 7
WHERE p.campaign_no = 788; -- MOS Endowment Fund


WITH Addressx
AS
(
	SELECT adr.address_no,
		adr.customer_no,
		adr.street1,
		adr.street2,
		adr.street3,
		adr.city,
		adr.state,
		dbo.AF_FORMAT_STRING(adr.postal_code, '@@@@@-@@@@') AS postal_code,
		adr.address_type,
		adr.inactive
	FROM dbo.TX_CONTACT_POINT_PURPOSE cpp 
	LEFT JOIN dbo.T_ADDRESS adr
		ON adr.address_no = cpp.contact_point_id
	WHERE cpp.purpose_id = 13 -- Endowment Report Mailing
		AND adr.last_updated_by <> 'NCOA$DNM'
		AND adr.inactive = 'N'
)
UPDATE @Endowments
	SET AddressFlag = 
	CASE 
		WHEN addr.address_type = 20 THEN 'No Good Addr Flag'
		ELSE NULL 
	END,
	street1 = addr.street1, 
	street2 = addr.street2,
	street3 = addr.street3,
	city = addr.city, 
	state = addr.state, 
	postal_code = addr.postal_code
FROM @Endowments endow
INNER JOIN V_CUSTOMER_WITH_HOUSEHOLD custhh
	ON endow.contact_custNum = custhh.customer_no
LEFT JOIN Addressx addr
	ON addr.customer_no = custhh.expanded_customer_no -- this is to get the "Endowment Report Mailing" CPP if it is on the HH record
WHERE addr.street1 IS NOT NULL 


SELECT 
	EndowFundNumber
	--,EndowFund_custNum
	,EndowFundName
	,EndowFundSortName
	,EndowMailingProgramName
	,contact_custNum
	,contact_fname
	,contact_lname
	,contact_displayName
	,contact_sortName
	,AddressFlag
	,street1
	,CASE 
		WHEN LTRIM(RTRIM(street2)) = '' THEN NULL
		ELSE street2
	END AS street2
	,CASE 
		WHEN LTRIM(RTRIM(street3)) = '' THEN NULL
		ELSE street3
	END AS street3
	,city
	,state
	,postal_code
FROM @Endowments ;
