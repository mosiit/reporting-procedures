USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_CUST_STATEMENT_ORDERS]') AND type in (N'P', N'PC')) BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_CUST_STATEMENT_ORDERS] AS' 
    EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LRP_CUST_STATEMENT_ORDERS] TO impusers' 
END
GO

ALTER PROCEDURE [dbo].[LRP_CUST_STATEMENT_ORDERS]
        @customer_no INT = NULL,
        @report_start_dt DATETIME = NULL,
        @report_end_dt DATETIME = NULL,
        @mode_of_sale_str VARCHAR(4000) = NULL,
        @include_returns CHAR(1) = 'N',
        @order_no INT = NULL
WITH RECOMPILE AS BEGIN

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    SET NOCOUNT ON;

    /*  One of multiple procedures called for the Customer Statement report  - This one pulls customer order information  */

    /*  Procedure Variables  */
       
            --Was having severe performance issues with this one.  Query would run in seconds by itself, but same
            --query inside stored procedure was taking minutes.  Recommened fix was to not use parameters directly in
            --the query.  Store parameter values in local variables and use those.  That's what these variables are for.
            --Once these were added and used, run time went back down to seconds.
            DECLARE @start_dt DATETIME = @report_start_dt,
                    @end_dt DATETIME = @report_end_dt,
                    @inc_returns CHAR(1) = @include_returns,
                    @cust_no INT = @customer_no

            DECLARE @cust_max_perf_dt DATETIME = NULL 

            DECLARE @title_order_list TABLE ([title_no] INT, 
                                             [title_name] varchar(30), 
                                             [title_order] CHAR(1))

            DECLARE @modes_of_sale TABLE ([mos_no] INT)

    /*  Temporary Tables  */

        IF OBJECT_ID('tempdb..#order_data') IS NOT NULL DROP TABLE [#order_data]

        CREATE TABLE [#order_data] ([customer_no] INT NOT NULL DEFAULT (0),
                                    [initiator_no] INT NOT NULL DEFAULT (0),
                                    [customer_is_initator] CHAR(1) NOT NULL DEFAULT ('N'),
                                    [initiator_name] VARCHAR(100) NOT NULL DEFAULT (''),
                                    [initiator_sort_name] VARCHAR(100) NOT NULL DEFAULT (''),
                                    [customer_name] VARCHAR(100) NOT NULL DEFAULT (''),
                                    [customer_sort_name] VARCHAR(100) NOT NULL DEFAULT (''),
                                    [order_no] INT NOT NULL DEFAULT (0),
                                    [order_type] VARCHAR(10) NOT NULL DEFAULT (''),
                                    [order_dt] DATETIME NULL,
                                    [order_created_by] VARCHAR(25) NOT NULL DEFAULT(''),
                                    [order_category_no] INT NOT NULL DEFAULT (0),
                                    [order_category] VARCHAR(30) NOT NULL DEFAULT(''),
                                    [mode_of_sale_no] INT NOT NULL DEFAULT (0),
                                    [mode_of_sale] VARCHAR(30) NOT NULL DEFAULT(''),
                                    [audience] VARCHAR(255) NOT NULL DEFAULT (''),
                                    [po_num] VARCHAR(255) NOT NULL DEFAULT (''),
                                    [sli_status] INT NOT NULL DEFAULT (0),
                                    [sli_status_name] VARCHAR(30) NOT NULL DEFAULT(''),
                                    [performance_dt] DATE NULL,
                                    [performance_time] VARCHAR(10) NOT NULL DEFAULT (''),
                                    [title_no] INT NOT NULL DEFAULT (0),
                                    [title_name] VARCHAR(30) NOT NULL DEFAULT(''),
                                    [production_no] INT NOT NULL DEFAULT (''),
                                    [production_name] VARCHAR(30) NOT NULL DEFAULT(''),
                                    [price_type_name] VARCHAR(30) NOT NULL DEFAULT(''),
                                    [price_type_name_short] VARCHAR(30) NOT NULL DEFAULT(''),
                                    [comp_code_name] VARCHAR(30) NOT NULL DEFAULT(''),
                                    [comp_code_name_short] VARCHAR(30) NOT NULL DEFAULT(''),
                                    [quantity] INT NOT NULL DEFAULT (0),
                                    [total_amount] DECIMAL (18,2) NOT NULL DEFAULT (0.0),
                                    [total_due] DECIMAL (18,2) NOT NULL DEFAULT (0.0),
                                    [total_paid] DECIMAL (18,2) NOT NULL DEFAULT (0.0))

        /*  Check Parameters  */

            --Check for null values and correct
            SELECT @cust_no = ISNULL(@cust_no, 0);
            SELECT @start_dt = ISNULL(@start_dt, '5-16-2016');
            SELECT @end_dt = ISNULL(@end_dt, '12-31-2999')
            SELECT @inc_returns = ISNULL(@inc_returns, 'N')

            IF ISNULL(@mode_of_sale_str,'') = ''
                INSERT INTO @modes_of_sale ([mos_no])
                SELECT [id] FROM [dbo].[TR_MOS]
            ELSE
                INSERT INTO @modes_of_sale ([mos_no])
                SELECT [Element] FROM [dbo].[FT_SPLIT_LIST](REPLACE(@mode_of_sale_str,'"',''),',')
            
        /*  Jump to the end if no customer numnber was passed to the report  */

            IF @cust_no <= 0 GOTO FINISHED
            
        /*  Set Venue order in the order listing  */

        INSERT INTO @title_order_list ([title_no], [title_name], [title_order])
        VALUES  (27,    'Exhibit Halls',       'A'),        (37,    'Special Exhibitions', 'B'),
                (17828, 'Audio Tour',          'C'),        (161,   'Mugar Omni Theater',  'D'),
                (1132,  'Hayden Planetarium',  'E'),        (173,   '4-D Theater',         'F'),
                (157,   'Butterfly Garden',    'G'),        (5508,  'Cahners Theater',     'H'),
                (1148,  'School Lunch',        'I'),        (61,    'Overnight Programs',  'J'),
                (42,    'Traveling Programs',  'K'),        (0,     'Order Fee',           'Z')
        
                                    
        /*  Get the data  */

            INSERT INTO [#order_data] ([customer_no], [initiator_no], [customer_is_initator], [initiator_name], [initiator_sort_name], [customer_name], [customer_sort_name], [order_no],
                                       [order_dt], [order_created_by], [order_category_no], [order_category], [mode_of_sale_no], [mode_of_sale], [audience], [po_num], [sli_status], 
                                       [sli_status_name], [performance_dt], [performance_time], [title_no], [title_name], [production_no], [production_name], [price_type_name], 
                                       [price_type_name_short], [comp_code_name], [comp_code_name_short], [quantity], [total_amount], [total_due], [total_paid])
            SELECT ord.[customer_no],
                   ISNULL(ord.[initiator_no],ord.[customer_no]) AS [initiator_no],
                   CASE WHEN ord.[customer_no] = ISNULL(ord.[initiator_no],ord.[customer_no]) THEN 'Y'
                        ELSE 'N' END AS [customer_is_initator],
                   nam.[display_name] AS [initiator_name],
                   nam.[sort_name] AS [initiator_sort_name],
                   cnam.[display_name] AS [customer_name],
                   cnam.[sort_name] AS [customer_sort_name],
                   sli.[order_no],
                   ord.[order_dt],
                   ISNULL(ord.[created_by],'') AS [order_created_by],
                   ISNULL(ord.[class],'') AS [order_category_no],
                   ISNULL(cat.[description],'None') AS [order_category],
                   ISNULL(ord.[MOS],0) AS [mode_of_sale_no],
                   ISNULL(mos.[description],'') AS [mode_of_sale],
                   ISNULL(ord.[custom_5],''),
                   ISNULL(ord.[custom_8],''),
                   sli.[sli_status],
                   sta.[description] AS [sli_status_name],
                   CAST(prf.[performance_dt] AS DATE) AS [performance_dt],
                   prf.[performance_time],
                   prf.[title_no],
                   prf.[title_name],
                   prf.[production_no],
                   prf.[production_name],
                   pri.[description] AS [price_type_name],
                   pri.[short_desc] AS [price_type_name_short],
                   CASE WHEN ISNULL(cmp.[description],'') IN ('','None') THEN ''
                        ELSE cmp.[description] END AS [comp_code_name],
                   CASE WHEN ISNULL(cmp.[short_desc],'') IN ('','None') THEN ''
                        ELSE cmp.[short_desc] END AS [comp_code_name_short],
                   CASE WHEN sli.[sli_status] = 7 THEN 0
                        ELSE 1 END AS [quantity],
                   sli.[due_amt],
                   CASE WHEN sli.[sli_status] = 7 THEN 0.0
                        ELSE sli.[due_amt] END,
                   CASE WHEN sli.[sli_status] = 7 THEN 0.0
                        ELSE sli.[paid_amt] END
            FROM [dbo].[T_SUB_LINEITEM] AS sli  
                 INNER JOIN dbo.T_ORDER AS ord ON ord.[order_no] = sli.[order_no]
                 INNER JOIN  @modes_of_sale AS lis ON lis.[mos_no] = ord.[MOS]
                 --INNER JOIN @sub_status_list AS sta ON sta.[sli_status_no] = sli.[sli_status]
                 LEFT OUTER JOIN [dbo].[TR_SLI_STATUS] AS sta ON sta.[id] = sli.[sli_status]
                 LEFT OUTER JOIN [dbo].[TR_ORDER_CATEGORY] AS cat ON cat.[id] = ord.[class]
                 LEFT OUTER JOIN [dbo].[TR_MOS] AS mos ON mos.[id] = ord.[MOS]
                 LEFT OUTER JOIN [dbo].[TR_PRICE_TYPE] AS pri ON pri.[id] = sli.[price_type]
                 LEFT OUTER JOIN [dbo].[TR_COMP_CODE] as cmp ON cmp.[id] = sli.[comp_code]
                 LEFT OUTER JOIN [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] AS prf ON prf.[performance_no] = sli.[perf_no] AND prf.[performance_zone] = sli.[zone_no]
                 LEFT OUTER JOIN dbo.FT_CONSTITUENT_DISPLAY_NAME() AS nam ON nam.[customer_no] = ISNULL(ord.[initiator_no],ord.[customer_no])
                 LEFT OUTER JOIN dbo.FT_CONSTITUENT_DISPLAY_NAME() AS cnam ON cnam.[customer_no] = ord.[customer_no]
            WHERE prf.[performance_dt] BETWEEN @start_dt AND @end_dt
              AND sli.[sli_status] IN (2, 3, 7, 12)  --Seated, Unpaid -- Seated, Paid -- Ticketed, Paid
              AND (ISNULL(@order_no,0) = 0 OR [ord].[order_no] = @order_no)
              AND (ord.[customer_no] = @cust_no OR ord.[initiator_no] = @cust_no);

    /*  If not reporting on returns, delete everything with a status of 7 (Returned Same Order)
        If any item is completely returned, it will no longer show up on the report  */
    
        IF @inc_returns <> 'Y'
            DELETE FROM [#order_data] WHERE [sli_status] = 7;


            WITH CTE_ORDER_PERF_DT ([order_no], [max_dt])
            AS (SELECT order_no,
                   MAX([performance_dt])
                FROM [#order_data]
                GROUP BY [order_no])
            INSERT INTO [#order_data] ([customer_no], [initiator_no], [customer_is_initator], [initiator_name], [initiator_sort_name], [customer_name], [customer_sort_name], [order_no],
                                           [order_dt], [order_created_by], [order_category_no], [order_category], [mode_of_sale_no], [mode_of_sale], [audience], [po_num], [sli_status], 
                                           [sli_status_name], [performance_dt], [performance_time], [title_no], [title_name], [production_no], [production_name], [price_type_name], 
                                           [price_type_name_short], [comp_code_name], [comp_code_name_short], [quantity], [total_amount], [total_due], [total_paid])
            SELECT fee.[customer_no], 
                   ISNULL(ord.[initiator_no], ord.[customer_no]) AS [initiator_no],
                   CASE WHEN ord.[customer_no] = ISNULL(ord.[initiator_no],ord.[customer_no]) THEN 'Y'
                        ELSE 'N' END AS [customer_is_initator],
                   nam.[display_name] AS [initiator_name],
                   nam.[sort_name] AS [initiator_sort_name],
                   cnam.[display_name] AS [customer_name],
                   cnam.[sort_name] AS [customer_sort_name],
                   fee.[order_no],
                   ord.[order_dt], 
                   ISNULL(ord.[created_by],'') AS [order_created_by],
                   ISNULL(ord.[class],'') AS [order_category_no],
                   ISNULL(cat.[description],'None') AS [order_category],
                   ISNULL(ord.[MOS],0) AS [mode_of_sale_no],
                   ISNULL(mos.[description],'') AS [mode_of_sale],
                   ISNULL(ord.[custom_5],''),
                   ISNULL(ord.[custom_8],''),
                   fee.[fee_sli_status],
                   fee.[fee_sli_status_name],
                   pdt.[max_dt], 
                   '', 
                   0, 
                   fee.[fee_name], 
                   0, 
                   fee.[fee_name], 
                   '', 
                   'Fee', 
                   '', 
                   '', 
                   1, 
                   fee.[fee_amt_due], 
                   fee.[fee_amt_due], 
                   fee.[fee_amt_paid]
            FROM [dbo].[LV_ORDER_FEES] AS fee
                 LEFT OUTER JOIN [CTE_ORDER_PERF_DT] AS pdt ON pdt.[order_no] = fee.[order_no]
                 LEFT OUTER JOIN [dbo].[T_ORDER] AS ord ON ord.[order_no] = fee.[order_no]
                 LEFT OUTER JOIN [dbo].[TR_ORDER_CATEGORY] AS cat ON cat.[id] = ord.[class]
                 LEFT OUTER JOIN [dbo].[TR_MOS] AS mos ON mos.[id] = ord.[MOS]
                 LEFT OUTER JOIN dbo.FT_CONSTITUENT_DISPLAY_NAME() AS nam ON nam.[customer_no] = ISNULL(ord.[initiator_no],ord.[customer_no])
                 LEFT OUTER JOIN dbo.FT_CONSTITUENT_DISPLAY_NAME() AS cnam ON cnam.[customer_no] = ord.[customer_no]
            WHERE fee.[order_no] IN (SELECT [order_no]
                                     FROM [#order_data])

    /*  Identify Overnight Programs and Traveling Programs Orders  */

        UPDATE [#order_data]
        SET [order_type] = 'ON'
        WHERE [order_no] IN (SELECT [order_no]   
                             FROM [#order_data] 
                             WHERE [title_name] = 'Overnight Programs')

        UPDATE [#order_data]
        SET [order_type] = CASE WHEN [order_type] = 'ON' THEN 'ON/TP'
                                ELSE 'TP' END
        WHERE [order_no] IN (SELECT [order_no]   
                             FROM [#order_data] 
                             WHERE [title_name] = 'Traveling Programs')

    /*  Separate Mileage Fee From Traveling Program  */

        UPDATE [#order_data]
        SET title_name = 'Mileage Fee'
        WHERE [price_type_name_short] = 'TPGMILE'

    /*  Get the customers latest visit date  */

        SELECT @cust_max_perf_dt = MAX(performance_dt)
        FROM [#order_data]

    /*  If customer_no <> @customer_no, Initiator Name to Customer Name */

        UPDATE [#order_data]
        SET [initiator_name] = [customer_name],
            [initiator_sort_name] = [customer_sort_name]
        WHERE [customer_no] <> @customer_no
          AND ISNULL([customer_name], '') <> ''


    FINISHED:

                    WITH [CTE_ORDER_PERF_DATES] ([order_no], [min_performance_dt], [visit_date_count])
                    AS   (
                          SELECT [order_no], 
                                 MIN([performance_dt]),
                                 COUNT(DISTINCT [performance_dt])
                          FROM [#order_data] 
                          GROUP BY [order_no]),
                         [CTE_ORDER_INVOICE_PAYMENTS] ([order_no], [invoice_payment_amount])
                    AS   (
                          SELECT [order_no],
                                 SUM([payment_amount])
                          FROM [dbo].[LV_ORDER_PAYMENTS]
                          WHERE [order_no] IN (SELECT [order_no] FROM [#order_data])
                            AND payment_method_name LIKE '%Invoice%'
                          GROUP BY [order_no]
                          HAVING SUM([payment_amount]) <> 0.0)
            SELECT ord.[customer_no],
                   ord.[initiator_no],
                   ord.[customer_is_initator],
                   ord.[initiator_name],
                   ord.[initiator_sort_name],
                   ord.[customer_name],
                   ord.[customer_sort_name],
                   ord.[order_no],
                   ord.[order_type],
                   ord.[order_dt],
                   ord.[order_created_by],
                   ord.[order_category_no],
                   ord.[order_category],
                   ord.[mode_of_sale_no],
                   ord.[mode_of_sale],
                   ord.[audience] AS [order_audience],
                   ord.[po_num] AS [order_PO],
                   CAST(@cust_max_perf_dt AS DATE) AS [cust_max_performance_dt],
                   ISNULL(mnd.[min_performance_dt], ord.[performance_dt]) AS [min_performance_dt],
                   ord.[performance_dt],
                   ord.[title_no],
                   ord.[title_name],
                   ISNULL(lst.[title_order],'Z') AS [title_order],
                   ord.[total_amount],
                   SUM(ord.[quantity]) AS [quantity],
                   SUM(ord.[total_due]) AS [total_due],
                   SUM(ord.[total_paid]) AS [total_paid],
                   ISNULL(inv.[invoice_payment_amount], 0.0) AS [invoice_payment_amount],
                   mnd.[visit_date_count]
            FROM [#order_data] AS ord
                 LEFT OUTER JOIN @title_order_list AS lst ON lst.[title_no] = ord.[title_no]
                 LEFT OUTER JOIN [CTE_ORDER_PERF_DATES] AS mnd ON mnd.[order_no] = ord.[order_no]
                 LEFT OUTER JOIN [CTE_ORDER_INVOICE_PAYMENTS] AS inv ON inv.[order_no] = ord.[order_no]
            GROUP BY ord.[customer_no], ord.[initiator_no], ord.[customer_is_initator], ord.[initiator_name], ord.[initiator_sort_name], ord.[customer_name], ord.[customer_sort_name],
                     ord.[order_no], ord.[order_type], ord.[order_dt], ord.[order_created_by], ord.[order_category_no], ord.[order_category], ord.[mode_of_sale_no], ord.[mode_of_sale],
                     ord.[audience], ord.[po_num], ISNULL(mnd.[min_performance_dt], ord.[performance_dt]), ord.[performance_dt], ord.[title_no], ord.[title_name], lst.[title_order], 
                     ord.[total_amount], ISNULL(inv.[invoice_payment_amount], 0.0), mnd.[visit_date_count]
            ORDER BY ord.[order_no], ord.[performance_dt], lst.[title_order], ord.[total_amount]

    DONE:

END
GO


--EXECUTE [dbo].[LRP_CUST_STATEMENT_ORDERS] @customer_no = 3267933, @report_start_dt = '9-1-2019', @report_end_dt = '5-15-2020', @mode_of_sale_str = '', @include_returns = 'N' 


--EXECUTE [dbo].[LRP_CUST_STATEMENT_ORDERS] @customer_no = 2589506, @report_start_dt = NULL, @report_end_dt = NULL, @mode_of_sale_str = '', @include_returns = 'N' 
--EXECUTE [dbo].[LRP_CUST_STATEMENT_ORDERS] @customer_no = 3539068, @report_start_dt = NULL, @report_end_dt = NULL, @mode_of_sale_str = '', @include_returns = 'N', @order_no = 0
--EXECUTE [dbo].[LRP_CUST_STATEMENT_ORDERS] @customer_no = 67226, @report_start_dt = NULL, @report_end_dt = NULL, @mode_of_sale_str = '', @include_returns = 'N', @order_no = 0

--EXECUTE [dbo].[LRP_CUST_STATEMENT_ORDERS] @customer_no = NULL, @report_start_dt = NULL, @report_end_dt = NULL, @include_returns = Null



/*
BEGIN TRAN
SELECT * FROM T_ORDER WHERE [order_no] = 2088546
UPDATE T_ORDER SET custom_8 = '1234_Test' WHERE [order_no] = 2088546
PRINT @@ROWCOUNT
SELECT * FROM T_ORDER WHERE [order_no] = 2088546
--COMMIT TRAN
--ROLLBACK TRAN
*/
