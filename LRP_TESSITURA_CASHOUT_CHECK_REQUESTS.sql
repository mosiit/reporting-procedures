USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_TESSITURA_CASHOUT_CHECK_REQUESTS]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LRP_TESSITURA_CASHOUT_CHECK_REQUESTS]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LRP_TESSITURA_CASHOUT_CHECK_REQUESTS]
        @report_dt datetime,
        @report_operator varchar(10),
        @machine_locations VARCHAR(4000)
AS BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

    /*  Report Variables  */

        DECLARE @refund_pay_method_no INT       SELECT @refund_pay_method_no = 145  --145 is the id number for the School and Group Refunds payment methid
        DECLARE @report_date CHAR(10)           SELECT @report_date = CONVERT(CHAR(10),@report_dt, 111)
        DECLARE @location_list TABLE ([location_no_str] VARCHAR(20), [location_no] INT)
        DECLARE @machine_list TABLE ([machine_name] VARCHAR(50))

    /*  Check Parameters  */

        SELECT @report_dt = ISNULL(@report_dt,GETDATE())            -- If null passed to date parameter, assume today
        SELECT @report_operator = ISNULL(@report_operator,'')       -- If null passed to operator parameter, assume all operators
        SELECT @machine_locations = ISNULL(@machine_locations,'')

    /*  Parse machine_locations parameter  */

        INSERT INTO @location_list ([location_no_str]) 
        SELECT [item] FROM [dbo].[LFT_SPLIT_DILEMETED_STRING] (@machine_locations, ',')

        DELETE FROM @location_list WHERE [dbo].[FS_isReallyNumeric] ([location_no_str]) = 0

        UPDATE @location_list SET [location_no] = CONVERT(INT,[location_no_str])

        INSERT INTO @machine_list ([machine_name])
        SELECT [machine_name] FROM [dbo].[TX_MACHINE_LOCATION] WHERE [location] IN (SELECT [location_no] FROM @location_list)

   
    /*  Two Temp tables used:  @check_request_orders stores a list of all the individual orders where the refund payment method was used 
                               @check_request_info stores the specfic information about the refunded orders  */
       
        DECLARE @check_request_orders TABLE ([order_no] INT, [customer_no] INT, [payment_user] VARCHAR(10), [transaction_location] VARCHAR(50), [payment_date] CHAR(10), 
                                             [payment_method] INT, [payment_amount] DECIMAL(18,2))

        DECLARE @check_request_info TABLE ([order_no] INT, [customer_no] INT, [batch_no] INT, [mos_no] INT, [mos_name] VARCHAR(30), [transaction_no] INT, [payment_date] CHAR(10), [payment_amount] MONEY,
                                           [payment_user] VARCHAR(10), [tot_due_amt] MONEY, [tot_paid_amt] MONEY, [custom_billing] VARCHAR(255), [notes] VARCHAR(255), [first_name] VARCHAR(20), 
                                           [middle_name] VARCHAR(20), [last_name] VARCHAR(55), [address_type] VARCHAR(30), [address_line_1] VARCHAR(64), [address_line_2] VARCHAR(64), [city] VARCHAR(30), 
                                           [state] VARCHAR(20), [postal_code] VARCHAR(20), [country] VARCHAR(30), [customer_info] VARCHAR(255), [refund_memo] varchar(100))


    /*  Find all the orders with payments that have the refund payment method in them for a specific operator on a specific day  */
    
        INSERT INTO @check_request_orders
        SELECT trx.[order_no], pay.[customer_no], pay.[created_by], trx.[create_loc], CONVERT(CHAR(10),pay.[pmt_dt],111), pay.[pmt_method], SUM(pay.[pmt_amt])
        FROM [dbo].[T_PAYMENT] AS pay (NOLOCK)
             LEFT OUTER JOIN [dbo].[T_TRANSACTION] AS trx (NOLOCK) ON trx.[transaction_no] = pay.[transaction_no] AND trx.[sequence_no] = pay.[sequence_no]
        WHERE CONVERT(CHAR(10),pay.[create_dt],111) = @report_date AND pay.[pmt_method] = @refund_pay_method_no
        GROUP BY trx.[order_no], pay.[customer_no], pay.[created_by], trx.[create_loc], CONVERT(CHAR(10),pay.[pmt_dt],111), pay.[pmt_method]
        ORDER BY  CONVERT(CHAR(10),pay.[pmt_dt],111), pay.[created_by]

    /*  If a specific operator was passed to the procedure, limit to only payments processed by that operator  */

       IF @report_operator <> '' DELETE FROM @check_request_orders WHERE [payment_user] <> @report_operator

    /*  If specific machine locations were passed to the procedure, limit to only those machines  */

        IF EXISTS (SELECT * FROM @machine_list)
            DELETE FROM @check_request_orders WHERE [transaction_location] NOT IN (SELECT [machine_name] FROM @machine_list)

    /*  If no payments found for that operator on that day, skip to the end  */

        IF NOT exists (SELECT * FROM @check_request_orders) GOTO FINISHED

    /*  Gather the specifics about the individual requests into the @check_request_info table  */
    
        INSERT INTO @check_request_info
        SELECT req.[order_no], req.[customer_no], ord.[batch_no], ord.[MOS], mos.[description], ord.[transaction_no], req.[payment_date], req.[payment_amount],
               req.[payment_user], ord.[tot_due_amt], ord.[tot_paid_amt], ISNULL(ord.[custom_2],''), ord.[notes], ISNULL(cus.[fname],''), ISNULL([mname],''), 
               ISNULL([lname],''), aty.[description], ISNULL(adr.[street1],''), ISNULL(adr.[street2],''), ISNULL(adr.[city],''), ISNULL(adr.[state],''),
               ISNULL(adr.[postal_code],''), ISNULL(cou.[description],''), '', ''
        FROM @check_request_orders AS req
             LEFT OUTER JOIN [dbo].[T_ORDER] AS ord (NOLOCK) ON ord.[order_no] = req.[order_no]
             LEFT OUTER JOIN [dbo].[T_CUSTOMER] AS cus (NOLOCK) ON cus.[customer_no] = ord.[customer_no]
             LEFT OUTER JOIN [dbo].[T_ADDRESS] AS adr (NOLOCK) ON adr.[customer_no] = ord.[customer_no] AND adr.[primary_ind] = 'Y'
             LEFT OUTER JOIN [dbo].[TR_ADDRESS_TYPE] AS aty (NOLOCK) ON aty.[id] = adr.[address_type]
             LEFT OUTER JOIN [dbo].[TR_COUNTRY] AS cou (NOLOCK) ON cou.[id] = adr.[country]
             LEFT OUTER JOIN [dbo].[TR_MOS] AS mos (NOLOCK) ON mos.[id] = ord.[MOS]

    /*  Add the dash into the postal code if it has the Plus 4 */
    
        UPDATE @check_request_info SET postal_code = LEFT(postal_code,5) + '-' + RIGHT(RTRIM(postal_code),4) WHERE LEN(RTRIM(postal_code)) = 9

    /*  Populate the customer info with the name and address information for the customer on the order including carriage returns  */

        UPDATE @check_request_info SET [customer_info] = LTRIM([first_name] + ' ' + [middle_name] + ' ' + [last_name]) + CHAR(13) + CHAR(10)
                                                       + CASE WHEN [address_line_1] = '' THEN '' ELSE [address_line_1] + CHAR(13) + CHAR(10) END
                                                       + CASE WHEN [address_line_2] = '' THEN '' ELSE [address_line_2] + CHAR(13) + CHAR(10) END
                                                       + [city] + ', ' + [state] + '  ' + [postal_code]


    /*  If the custom_billing field is blank, make it the same as the customer_info field*/

    UPDATE @check_request_info SET [custom_billing] = [customer_info] WHERE [custom_billing] = ''

    /*  Update the refund memo to include the first fifteen characters of the last name field plus thr order date (mm/dd/yy format)  */

    UPDATE  @check_request_info 
    SET refund_memo = 'Refund ' + LEFT(last_name,15) + ' for visit on ' + SUBSTRING([payment_date],6,2) + '/' + RIGHT([payment_date],2) + '/' + SUBSTRING([payment_date],3,2)
                    + ' (Order # ' + CONVERT(VARCHAR(20),[order_no]) + ')'

    /*  Remove any non-negative amounts, then change negative payment amounts to positive for display on the report  */

    DELETE FROM @check_request_info WHERE [payment_amount] >= 0.00

    UPDATE @check_request_info SET [payment_amount] = ([payment_amount] * -1)


    FINISHED:

        SELECT [order_no], [customer_no], [batch_no], [mos_no], [mos_name], [transaction_no], [payment_date], [payment_amount], [payment_user],
               [tot_due_amt], [tot_paid_amt], [custom_billing], [notes], [first_name], [middle_name], [last_name], [address_type], [address_line_1], 
               [address_line_2], [city], [state], [postal_code], [country], [customer_info], [refund_memo]
        FROM @check_request_info
       
END
GO

GRANT EXECUTE ON [dbo].[LRP_TESSITURA_CASHOUT_CHECK_REQUESTS] TO impusers
GO


EXECUTE [dbo].[LRP_TESSITURA_CASHOUT_CHECK_REQUESTS] @report_dt = '8-3-2016', @report_operator = 'cmonag00', @machine_locations = '15,16'
    

    