USE impresario;
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_COURSES_GetCoursesInformation]') AND type in (N'P', N'PC')) BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_COURSES_GetCoursesInformation] AS';
    EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LRP_COURSES_GetCoursesInformation] TO impusers';
END;
GO

ALTER PROCEDURE [dbo].[LRP_COURSES_GetCoursesInformation]
(
 @startDate DATETIME,
 @endDate DATETIME = NULL 
)
AS BEGIN

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    SET NOCOUNT ON;
    
    WITH CTE_ALTERNATE_EMAIL ([household_no], [name_ind], [customer_no], [customer_name], [email_address])
    AS (  
           SELECT aff.[group_customer_no],
                  CASE WHEN aff.[name_ind] = -1 THEN 'A1'
                       WHEN aff.[name_ind] = -2 THEN 'A2'
                       ELSE '' END AS [name_ind],
                  aff.[individual_customer_no],
                  nam.[display_name],
                  ISNULL(ead.[address],'')
           FROM [dbo].[T_AFFILIATION] AS aff
                LEFT OUTER JOIN [dbo].[FT_CONSTITUENT_DISPLAY_NAME]() AS nam ON nam.[customer_no] = aff.[individual_customer_no]
                LEFT OUTER JOIN [dbo].[T_EADDRESS] AS ead ON ead.[customer_no] = aff.[individual_customer_no] AND ead.[primary_ind] = 'Y'
           WHERE aff.[name_ind] IN (-2, -1)
             AND ISNULL(ead.[address],'') <> ''
       )           
    SELECT LTRIM(inf.[customer_first_name]
           + CASE WHEN ISNULL(inf.[customer_middle_name], '') = '' THEN ''
                     ELSE ' ' + inf.[customer_middle_name] END 
           + ' ' + inf.[customer_last_name]) AS [customer_name],
           inf.[address_type],
           inf.[street1],
           inf.[street2],
           inf.[city],
           inf.[state],
           dbo.AF_FORMAT_STRING(inf.postal_code, '@@@@@-@@@@') AS PostalCode,
           dbo.AF_FORMAT_STRING(inf.[home_phone_no], '@@@-@@@-@@@@ @@@@') AS homePhone,
           dbo.AF_FORMAT_STRING(inf.[business_phone_no], '@@@-@@@-@@@@ @@@@') AS [businessPhone],
           CASE WHEN ISNULL(inf.[email_address],'') <> '' THEN inf.[email_address]
                WHEN ISNULL(ea1.[email_address],'') <> '' THEN ea1.[email_address]
                ELSE ISNULL(ea2.[email_address],'') END AS [email_address],
           inf.[order_no],
           inf.[sale_status],
           LTRIM(inf.[participant_first_name]
                 + CASE WHEN ISNULL(inf.[participant_middle_name], '') = '' THEN ''
                        ELSE ' ' + inf.[participant_middle_name] END 
                 + ' ' + inf.[participant_last_name]) AS [participant_name],
           inf.[course_date],
           inf.[course_time],
           CAST(inf.[course_date] AS datetime) + CAST (inf.[course_time] AS datetime) AS [coursedatetime],
           inf.[course_time_display] AS [course_type],
           inf.[course_title_long] AS [course_title],
           inf.[Early Drop-Off],
           inf.[Late Stay],
           inf.[participant_dob],
           inf.[participant_grade]
    FROM [dbo].[LV_COURSE_PARTICIPANT_INFORMATION] AS inf
         LEFT OUTER JOIN [CTE_ALTERNATE_EMAIL] AS ea1 ON ea1.[household_no] = inf.[customer_no] AND ea1.[name_ind] = 'A1'
         LEFT OUTER JOIN [CTE_ALTERNATE_EMAIL] AS ea2 ON ea2.[household_no] = inf.[customer_no] AND ea2.[name_ind] = 'A2'
    WHERE CAST(inf.[course_dt] AS DATETIME) BETWEEN @startDate AND ISNULL(@endDate , DATEADD(d, 4, @startDate))
     AND inf.[Quantity] > 0 
     AND inf.[sale_status_no]  IN (3,12)
    ORDER BY inf.[course_date],
             inf.[course_time],
             inf.[course_title_long],
             inf.[participant_last_name],
             inf.[participant_first_name];

END;
GO


--EXECUTE [dbo].[LRP_COURSES_GetCoursesInformation] @startDate = '7-8-2019', @endDate = '8-31-2019' 
