USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_ONE_STEP_ENROLLMENT]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LRP_ONE_STEP_ENROLLMENT]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LRP_ONE_STEP_ENROLLMENT]
        @include_memb_level_breakdown CHAR(1) = 'Y'
AS BEGIN

    /*  procedure variables  */

        DECLARE @one_step_const_no INT = 30  --30 is the constituency number for "One-Step" in TR_CONSTITUENCY
        DECLARE @memb_org_no INT = 4         --4 is the membership organization number for "Household Membership" in T_MEMB_ORG
    
        DECLARE @exclude_inactive_memberships CHAR(1) = 'Y'
                /*  @exclude_inactive_memberships was originally going to be a parameter, but if these memberships 
                    are not excluded, it skews the numbers because they are not included in the totals.  For this 
                    purpose, an active memberships is defined as a membership with an expiration date > today.  
                    Left as a variable instead of hard coded in case it ever needs to be made a parameter  */

   
        DECLARE @membership_expiration_dates TABLE ([customer_no] INT, [membership_no] INT, [expiration_dt] DATETIME, [expiration_month] INT, [current_status] INT)

        DECLARE @final_table TABLE ([data_pass] INT, [data_sort] CHAR(1), [expire_month] INT, [expire_month_name] VARCHAR(15), [memb_level] VARCHAR(10), 
                                    [memb_level_name] VARCHAR(30), [memb_level_count] INT, [memb_level_count_os] INT, [memb_level_percent_os] DECIMAL(18,4))


    /* membership info cache - tried as a variable but the update statements that use it were taking too long to complete - indexes were needed  */

        IF OBJECT_ID('tempdb..#membership_info_cache') IS NOT NULL DROP TABLE [#membership_info_cache]

        CREATE TABLE [#membership_info_cache] ([cust_memb_no] INT, [cust_no] INT, [memb_org_no] INT, [memb_level] VARCHAR(25), [init_dt] DATETIME, 
                                           [expr_dt] DATETIME, [current_status] INT, [cur_record] CHAR(1))
    
        CREATE CLUSTERED INDEX [ix_membership_info_cache_cust_no] ON [#membership_info_cache] ([cust_no] ASC) ON [PRIMARY]
        CREATE NONCLUSTERED INDEX [ix_membership_info_cache_expr_dt] ON [#membership_info_cache] ([expr_dt] ASC) ON [PRIMARY]
        CREATE NONCLUSTERED INDEX [ix_membership_info_cache_memb_org_no] ON [#membership_info_cache] ([memb_org_no] ASC) ON [PRIMARY]
        CREATE NONCLUSTERED INDEX [ix_membership_info_cache_cur_record] ON [#membership_info_cache] ([cur_record] ASC) ON [PRIMARY]

    /*  check parameters  */

        SELECT @exclude_inactive_memberships = ISNULL(@exclude_inactive_memberships,'Y')
        SELECT @include_memb_level_breakdown = ISNULL(@include_memb_level_breakdown,'Y')

    /*  Get latest expiration dates for all customers in the One-Step constituency (#30)  */

        INSERT INTO @membership_expiration_dates ([customer_no], [expiration_dt])
        SELECT [customer_no], MAX([expr_dt]) FROM [dbo].[TX_CUST_MEMBERSHIP]
        WHERE [customer_no] IN (SELECT [customer_no] FROM [dbo].[TX_CONST_CUST] WHERE [constituency] = @one_step_const_no)
              AND [memb_org_no] = @memb_org_no AND [expr_dt] IS NOT null
        GROUP BY [customer_no]
                
        UPDATE @membership_expiration_dates SET expiration_month = DATEPART(MONTH,[expiration_dt])

    /*  If choosing to exclude customers who do not have a current active membership, delete records where expiration date is in the past  */

        IF @exclude_inactive_memberships = 'Y'
            DELETE FROM @membership_expiration_dates WHERE expiration_dt < GETDATE()

    /*  Get all membership information for the customers in the One-Step constituency (#30)  */
    
        INSERT INTO [#membership_info_cache] ([cust_memb_no], [cust_no], [memb_org_no], [memb_level], [init_dt], [expr_dt], [current_status], [cur_record])
        SELECT [cust_memb_no], [customer_no], [memb_org_no], [memb_level], [init_dt], [expr_dt], [current_status], [cur_record]
        FROM [dbo].[TX_CUST_MEMBERSHIP]
        WHERE [customer_no] IN (SELECT [customer_no] FROM @membership_expiration_dates)

    
    /*  determine membership number for the latest membership expiration date for an active membership that is the current record  */

        UPDATE @membership_expiration_dates SET membership_no = (SELECT MAX([cust_memb_no]) FROM [#membership_info_cache] AS txm 
                                                                 WHERE txm.[cust_no] = customer_no AND txm.[expr_dt] = [expiration_dt] AND txm.[expr_dt] IS NOT NULL 
                                                                   AND [txm].[memb_org_no] = @memb_org_no AND [current_status] IN (2,3))

    /*  there are some customers without a membership that is the current record (espiecially if not excluding inactive memberships  
        second update statement determines membership number for these.  if multiple exist, highest number (or most recently entered) is used  */
        
        IF @exclude_inactive_memberships = 'Y'
            DELETE FROM @membership_expiration_dates WHERE ISNULL(membership_no, 0) = 0
        ELSE
            UPDATE @membership_expiration_dates SET membership_no = (SELECT MAX([cust_memb_no]) FROM [#membership_info_cache] AS txm 
                                                                     WHERE txm.[cust_no] = customer_no AND txm.[expr_dt] = [expiration_dt] AND txm.[expr_dt] IS NOT NULL 
                                                                       AND [txm].[memb_org_no] = @memb_org_no)
        WHERE [membership_no] IS null
      
    /*  Gather data for the report (first data pass)  */

        IF @include_memb_level_breakdown = 'Y' BEGIN

            INSERT INTO @final_table ([data_pass], [data_sort], [expire_month], [expire_month_name], [memb_level], [memb_level_name], [memb_level_count], [memb_level_count_os])
            SELECT 1, 'A', DATEPART(MONTH,txm.[expr_dt]), DATENAME(MONTH,txm.[expr_dt]), txm.[memb_level], mem.[description], COUNT(*), 0
            FROM [dbo].[TX_CUST_MEMBERSHIP] AS txm (NOLOCK)
                 JOIN [dbo].[T_MEMB_LEVEL] AS mem (NOLOCK) ON mem.[memb_org_no] = txm.[memb_org_no] AND mem.[memb_level] = txm.[memb_level]
            WHERE txm.[memb_org_no] = 4 AND txm.[expr_dt] >= GETDATE()
            GROUP BY DATEPART(MONTH,txm.[expr_dt]), DATENAME(MONTH,txm.[expr_dt]), txm.[memb_level], mem.[description]
                              
            INSERT INTO @final_table ([data_pass], [data_sort], [expire_month], [expire_month_name], [memb_level], [memb_level_name], [memb_level_count], [memb_level_count_os])
            SELECT 1, 'A', DATEPART(MONTH,txm.[expr_dt]), DATENAME(MONTH,txm.[expr_dt]), txm.[memb_level], mem.[description], 0, COUNT(*)
            FROM [dbo].[TX_CUST_MEMBERSHIP] AS txm (NOLOCK)
                 JOIN [dbo].[T_MEMB_LEVEL] AS mem (NOLOCK) ON mem.[memb_org_no] = txm.[memb_org_no] AND mem.[memb_level] = txm.[memb_level]
            WHERE txm.[cust_memb_no] IN (SELECT [membership_no] FROM @membership_expiration_dates) and txm.[memb_org_no] = 4 AND txm.[expr_dt] >= GETDATE()
            GROUP BY DATEPART(MONTH,txm.[expr_dt]), DATENAME(MONTH,txm.[expr_dt]), txm.[memb_level], mem.[description]
        
        END
        
        INSERT INTO @final_table ([data_pass], [data_sort], [expire_month], [expire_month_name], [memb_level], [memb_level_name], [memb_level_count], [memb_level_count_os])
        SELECT 1, 'A', DATEPART(MONTH,txm.[expr_dt]), DATENAME(MONTH,txm.[expr_dt]), 'YY', DATENAME(MONTH,txm.[expr_dt]) + ' Total', COUNT(*), 0
        FROM [dbo].[TX_CUST_MEMBERSHIP] AS txm (NOLOCK)
             JOIN [dbo].[T_MEMB_LEVEL] AS mem (NOLOCK) ON mem.[memb_org_no] = txm.[memb_org_no] AND mem.[memb_level] = txm.[memb_level]
        WHERE txm.[memb_org_no] = 4 AND txm.[expr_dt] >= GETDATE()
        GROUP BY DATEPART(MONTH,txm.[expr_dt]), DATENAME(MONTH,txm.[expr_dt]), DATENAME(MONTH,txm.[expr_dt]) + ' Total'

        INSERT INTO @final_table ([data_pass], [data_sort], [expire_month], [expire_month_name], [memb_level], [memb_level_name], [memb_level_count], [memb_level_count_os])
        SELECT 1, 'A', DATEPART(MONTH,txm.[expr_dt]), DATENAME(MONTH,txm.[expr_dt]), 'YY', DATENAME(MONTH,txm.[expr_dt]) + ' Total', 0, COUNT(*)
        FROM [dbo].[TX_CUST_MEMBERSHIP] AS txm (NOLOCK)
             JOIN [dbo].[T_MEMB_LEVEL] AS mem (NOLOCK) ON mem.[memb_org_no] = txm.[memb_org_no] AND mem.[memb_level] = txm.[memb_level]
        WHERE txm.[cust_memb_no] IN (SELECT [membership_no] FROM @membership_expiration_dates) and txm.[memb_org_no] = 4 AND txm.[expr_dt] >= GETDATE()
        GROUP BY DATEPART(MONTH,txm.[expr_dt]), DATENAME(MONTH,txm.[expr_dt]), DATENAME(MONTH,txm.[expr_dt]) + ' Total'


        INSERT INTO @final_table ([data_pass], [data_sort], [expire_month], [expire_month_name], [memb_level], [memb_level_name], [memb_level_count], [memb_level_count_os])
        SELECT 1, 'C', 13, 'Grand Total', 'ZZ', 'Grand Total', COUNT(*), 0
        FROM [dbo].[TX_CUST_MEMBERSHIP] AS txm (NOLOCK)
             JOIN [dbo].[T_MEMB_LEVEL] AS mem (NOLOCK) ON mem.[memb_org_no] = txm.[memb_org_no] AND mem.[memb_level] = txm.[memb_level]
        WHERE txm.[memb_org_no] = 4 AND txm.[expr_dt] >= GETDATE()
        GROUP BY DATEPART(MONTH,txm.[expr_dt]), DATENAME(MONTH,txm.[expr_dt])

        INSERT INTO @final_table ([data_pass], [data_sort], [expire_month], [expire_month_name], [memb_level], [memb_level_name], [memb_level_count], [memb_level_count_os])
        SELECT 1, 'C', 13, 'Grand Total', 'ZZ', 'Grand Total', 0, COUNT(*)
        FROM [dbo].[TX_CUST_MEMBERSHIP] AS txm (NOLOCK)
             JOIN [dbo].[T_MEMB_LEVEL] AS mem (NOLOCK) ON mem.[memb_org_no] = txm.[memb_org_no] AND mem.[memb_level] = txm.[memb_level]
        WHERE txm.[cust_memb_no] IN (SELECT [membership_no] FROM @membership_expiration_dates) and txm.[memb_org_no] = 4 AND txm.[expr_dt] >= GETDATE()

        
    /*  aggregate the numbers (second data pass)  */
      
        INSERT INTO @final_table ([data_pass], [data_sort], [expire_month], [expire_month_name], [memb_level], [memb_level_name], [memb_level_count], [memb_level_count_os])
        SELECT 2, [data_sort], [expire_month], [expire_month_name], [memb_level], [memb_level_name], SUM([memb_level_count]), SUM([memb_level_count_os])
        FROM @final_table WHERE [data_pass] = 1
        GROUP BY [data_sort], [expire_month], [expire_month_name], [memb_level], [memb_level_name]

        DELETE FROM @final_table WHERE [data_pass] = 1

    /*  calculate percentages and add month name  */
    
        UPDATE @final_table SET [memb_level_percent_os] = (CONVERT(DECIMAL(18,4),[memb_level_count_os]) / CONVERT(DECIMAL(18,4),[memb_level_count]))


    FINISHED:

        /*  No check for empty table needed because the results table should never be empty
            If it is, there's a problem  */


        /*  select final data set to pass back to the report  */        

            SELECT [data_sort], [expire_month], [expire_month_name], [memb_level], [memb_level_name], [memb_level_count], [memb_level_count_os], [memb_level_percent_os]
            FROM @final_table
            --ORDER BY [data_sort], [expire_month], [memb_level]

END
GO

GRANT EXECUTE ON [dbo].[LRP_ONE_STEP_ENROLLMENT] TO ImpUsers
GO

EXECUTE [dbo].[LRP_ONE_STEP_ENROLLMENT] @include_memb_level_breakdown = 'Y'


      --SELECT memb_org_no, memb_level FROM dbo.T_MEMB_LEVEL GROUP BY memb_org_no, memb_level HAVING COUNT(*) > 1
      --SELECT * FROM dbo.T_MEMB_LEVEL WHERE memb_level = 'D' 
    
--    SELECT * FROM @membership_expiration_dates --WHERE membership_no IS NULL --expiration_dt <> GETDATE() ORDER BY expiration_dt

    --SELECT customer_no, memb_org_no, expr_dt, COUNT(*) 
    --FROM dbo.TX_CUST_MEMBERSHIP 
    --WHERE customer_no IN (SELECT [customer_no] FROM [dbo].[TX_CONST_CUST] WHERE [constituency] = 30)
    --  AND memb_org_no = 4 and expr_dt IS NOT NULL AND cur_record = 'Y'
    --GROUP BY customer_no, memb_org_no, expr_dt HAVING COUNT(*) > 1
    

    /*
    SELECT * FROM T_CUSTOMER WHERE customer_no = 2995
    SELECT * FROM dbo.TX_CONST_CUST WHERE customer_no = 2995
    SELECT * FROM dbo.TX_CUST_MEMBERSHIP WHERE customer_no = 2995 ORDER BY expr_dt DESC
    */
--    SELECT * FROM dbo.T_MEMB_ORG

