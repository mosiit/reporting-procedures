USE impresario;
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_SPECIAL_EXHIBITION_SALES]') AND type in (N'P', N'PC')) BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_SPECIAL_EXHIBITION_SALES] AS';
    EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LRP_SPECIAL_EXHIBITION_SALES] TO impusers';
END;
GO

ALTER PROCEDURE [dbo].[LRP_SPECIAL_EXHIBITION_SALES]
        @report_start_dt DATETIME = '7-1-2019',
        @report_end_dt DATETIME = '7-31-2019 23:59:59',
        @date_type VARCHAR(20) = 'Visit Date',         --Visit Date or Sale Date
        @include_days_with_no_sales CHAR(1) = 'Y'
AS BEGIN

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    SET NOCOUNT ON;
    SET ANSI_WARNINGS OFF;

    /*  Procedure Variables  */

        DECLARE @special_exhibition_no AS INT = 37;           --37 = Special Exhibitions Title Number
        DECLARE @audio_tour_no AS INT = 17828;                --17828 = Audio Tour Title Number
        DECLARE @special_exhibition_name AS VARCHAR(30) = '';
        DECLARE @include_audio_tour CHAR(1) = 'N';       --This used to be a parameter (and may be again)
    
        DECLARE @exhibit_start_dt DATETIME = '6-16-2019',        --HARD CODED FOR BODY WORLDS
                @exhibit_end_dt DATETIME = '1-5-2020',           --WILL NEED TO BE CHANGED FOR FUTURE EXHIBITS
                @sale_start_dt DATETIME = '5-15-2019'


    /*  Temp Table  */

        CREATE TABLE [#special_sales] ([perf_dt] DATETIME NULL,
                                       [title_no] INT NOT NULL DEFAULT(0),
                                       [title_name] VARCHAR(30) NOT NULL DEFAULT(''),
                                       [tickets_sold] DECIMAL(19,4) NOT NULL DEFAULT(0.0),
                                       [budgeted] DECIMAL(19,4) NOT NULL DEFAULT(0.0),
                                       [percent_of_budget] DECIMAL(19,4) NOT NULL DEFAULT(0.0),
                                       [due_amount] DECIMAL(19,2) NOT NULL DEFAULT (0.0),
                                       [paid_amount] DECIMAL(19,2) NOT NULL DEFAULT(0.0),
                                       [total_budgeted] INT NOT NULL DEFAULT(0));

    /*  Check Parameters  */

        IF ISNULL(@date_type,'') <> 'Sale Date' SELECT @date_type = 'Visit Date'
        
        IF @report_start_dt IS NULL AND @date_type = 'Sale Date' SELECT @report_start_dt = @sale_start_dt
        IF @report_start_dt IS NULL AND @date_type = 'Visit Date' SELECT @report_start_dt = @exhibit_start_dt

        IF @report_end_dt IS NULL SELECT @report_end_dt = @exhibit_end_dt        

        IF ISNULL(@include_days_with_no_sales,'') <> 'N' SELECT @include_days_with_no_sales = 'Y'

    
    /*  Get Production Name of the current special exhibition  */

        SELECT @special_exhibition_name = ISNULL(MAX([production_name]),'')
                     FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] 
                 WHERE [performance_dt] between @report_start_dt AND @report_end_dt
                   AND [title_no] = @special_exhibition_no


    /*  Insert sale information  */

        INSERT INTO [#special_sales] ([perf_dt], [title_no], [title_name],[tickets_sold],[budgeted],[due_amount],[paid_amount])
            SELECT CAST(prf.performance_dt AS DATE),
                   prf.title_no,
                   prf.title_name,
                   COUNT(sli.[sli_no]),
                   ISNULL(MAX(bud.budget),0),
                   SUM(sli.due_amt),
                   SUM(sli.paid_amt)
            FROM [dbo].[T_SUB_LINEITEM] AS sli
                 INNER JOIN [dbo].[T_ORDER] AS ord ON ord.[order_no] = sli.[order_no]
                 LEFT OUTER JOIN dbo.LV_PRODUCTION_ELEMENTS_PERFORMANCE AS prf ON prf.[performance_no] = sli.[perf_no] AND prf.[performance_zone] = sli.[zone_no]
                 LEFT OUTER JOIN dbo.LTR_ATTENDANCE_BUDGET_DATA AS bud ON bud.[perf_dt] = CAST(prf.[performance_dt] AS DATE) AND bud.[title_no] = prf.[title_no]
            WHERE 
                (
                    (@date_type = 'Sale Date' AND sli.create_dt BETWEEN @report_start_dt AND @report_end_dt)
                 OR (@date_type <> 'Sale Date' AND prf.performance_dt BETWEEN @report_start_dt AND @report_end_dt)
                )
              AND prf.title_no IN (@special_exhibition_no, @audio_tour_no)
              AND sli.[sli_status] IN (2, 3, 12)
              AND ord.[mos] NOT IN (9,33,34,35,36,37,38)
            GROUP BY CAST(prf.performance_dt AS DATE), prf.title_no, prf.title_name

    /*  If Audio Tours are not asked for, delete them  
        Note: As the report is currently writtn, Audio Tours will never be included
              but that might change as time goes on (or it might not)  */
        
        IF @include_audio_tour <> 'Y' 
            DELETE FROM [#special_sales] WHERE [title_no] = @audio_tour_no


    /*  If report is based on visit date, we don't want any visit dates included that are before
        the designated start date or after the designated end date  */

    IF @date_type = 'Visit Date' BEGIN

        IF @exhibit_start_dt < @report_start_dt SELECT @exhibit_start_dt = @report_start_dt
        IF @exhibit_end_dt > @report_end_dt SELECT @exhibit_end_dt = @report_end_dt

    END

    /*  Set the name of the exhibit to the specific exhibit name, rather than just Special Exhibitions  */

        IF @special_exhibition_name <> ''
            UPDATE[#special_sales]
            SET [title_name] = @special_exhibition_name
            WHERE [title_no] = @special_exhibition_no

    /*  If all visit dates are requested, regardless of whether or not there are sales to that date,
        add the zero dates in using the LFT_INDIVIDUAL_DATES function  */

    IF @include_days_with_no_sales = 'Y' BEGIN

        INSERT INTO [#special_sales] ([perf_dt], [title_no], [title_name],[tickets_sold],[budgeted],[due_amount],[paid_amount])
        SELECT dat.[return_dt],
               @special_exhibition_no,
               CASE WHEN @special_exhibition_name <> '' THEN @special_exhibition_name ELSE '' END,
               0,
               0,
               0.0,
               0.0
        FROM [dbo].[LFT_INDIVIDUAL_DATES](@exhibit_start_dt,@exhibit_end_dt) AS dat
             LEFT OUTER JOIN [dbo].[LTR_ATTENDANCE_BUDGET_DATA] AS bud ON bud.[perf_dt] = dat.[return_dt]
        WHERE 
            dat.[return_dt] NOT IN (SELECT perf_dt FROM #special_sales)
        GROUP BY dat.[return_dt]

    END


    /*  Determine total amount budged for the exhibit  */

        UPDATE #special_sales 
        SET [total_budgeted] = (SELECT ISNULL(SUM(budget),0.0)
                                FROM [dbo].[LTR_ATTENDANCE_BUDGET_DATA] 
                                WHERE [perf_dt] BETWEEN (SELECT MIN(perf_dt) FROM [#special_sales] WHERE title_no = @special_exhibition_no)
                                                    AND (SELECT MAX(perf_dt) FROM [#special_sales] WHERE title_no = @special_exhibition_no)
                                  AND [title_no] = @special_exhibition_no)

    /*  Determine the percent of budget for each day based on tickets sold  */

        UPDATE [#special_sales] SET [percent_of_budget] = CASE WHEN [budgeted] = 0 THEN 0
                                                               ELSE ([tickets_sold] / [budgeted]) END


    FINISHED:

        /*  Select final data set to pass back to the report  */
    
            SELECT [perf_dt],
                   [title_no],
                   [title_name],
                   [tickets_sold],
                   [budgeted],
                   [percent_of_budget],
                   [due_amount],
                   [paid_amount],
                   [total_budgeted]
            FROM [#special_sales]

        
END;
GO


EXECUTE [dbo].[LRP_SPECIAL_EXHIBITION_SALES]

    --SELECT * FROM dbo.TR_MOS




--SELECT * FROM LTR_ATTENDANCE_BUDGET_data



