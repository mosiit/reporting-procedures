USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_BOX_OFFICE_OVER_SHORT_DAILY]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LRP_BOX_OFFICE_OVER_SHORT_DAILY]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LRP_BOX_OFFICE_OVER_SHORT_DAILY]
        @payment_dt_start DATETIME, 
        @payment_dt_end DATETIME, 
        @payment_operator_location VARCHAR(30),
        @summary_only CHAR(1) = 'N',
        @variances_only CHAR(1) = 'N'
WITH RECOMPILE AS BEGIN

    /*  procedure variables  */

        DECLARE @report_message VARCHAR(100) = '';
        
        DECLARE @tblRec TABLE	([payment_date] CHAR(10), [payment_operator_location]  VARCHAR(30), [payment_operator] VARCHAR(8), [payment_operator_full_name] VARCHAR(80),
		    				     [payment_operator_first_name] VARCHAR(50), [payment_operator_last_name] VARCHAR(75), [payment_type] VARCHAR(30), [total_sales] MONEY, 
                                 [total_received] MONEY, [over_under] MONEY, [variance_unique_identifier] VARCHAR(100), [report_message] VARCHAR(100));
        
    /*  check parameters  */

        SELECT @payment_dt_start = ISNULL(@payment_dt_start,DATEADD(DAY,-1,GETDATE()));
        SELECT @payment_dt_end = ISNULL(@payment_dt_end,DATEADD(DAY,-1,GETDATE()));
        SELECT @payment_operator_location = ISNULL(@payment_operator_location,'');
        SELECT @summary_only = ISNULL(@summary_only,'N');
        
    /*  if no operator is passed to the procedure, pull all Box Office cashiers, otherwise, pull just the one operator  
            Note: procedure originally just pulled by location which is why the variable is named what it is
                  when change was made to pull by operator, variable name was not changed */
           
            WITH [total_payments] ([payment_date], [payment_operator], [payment_amount]) AS
            (
             SELECT [payment_date], [payment_operator], SUM([payment_amount])
             FROM [dbo].[LT_CASH_OUT_DATA] 
             WHERE CONVERT(DATE,[payment_date]) BETWEEN CONVERT(DATE,@payment_dt_start) AND CONVERT(DATE,@payment_dt_end) 
               AND [payment_type_name] IN ('Cash','Check','Gift Certificate')
             GROUP BY [payment_date], [payment_operator]
            )
            INSERT INTO @tblRec ([payment_date], [payment_operator_location], [payment_operator], [payment_operator_full_name], [payment_operator_first_name], 
                                 [payment_operator_last_name], [payment_type], [total_sales], [total_received], [over_under], [variance_unique_identifier], 
                                 [report_message])
            SELECT rcp.[payment_date],
                   rcp.[payment_operator_location],
                   rcp.[payment_operator],
                   rcp.[payment_operator_first_name] + ' ' + rcp.[payment_operator_last_name],
                   rcp.[payment_operator_first_name],
                   rcp.[payment_operator_last_name],
                   '',
                   SUM(ISNULL(rcp.[payment_amount], 0)),
                   MAX(ISNULL(pay.[payment_amount], 0)),
                   (MAX(ISNULL(pay.[payment_amount], 0)) - SUM(ISNULL(rcp.[payment_amount], 0))) AS [Over/Under],
                   '',
                   ''
            FROM [dbo].[LV_RPT_CASHOUT_PAYMENTS] rcp
                 LEFT OUTER JOIN [total_payments] AS pay ON pay.[payment_date] = rcp.[payment_date] AND pay.[payment_operator] = rcp.[payment_operator]
            WHERE rcp.[payment_dt] between @payment_dt_start AND @payment_dt_end
              AND [payment_operator_location] = 'Box Office'
              AND (@payment_operator_location = '' OR rcp.[payment_operator] = @payment_operator_location)
              AND rcp.[payment_type_name] IN ('Cash', 'Check', 'Gift Certificate')
            GROUP BY  rcp.[payment_date], rcp.payment_operator_location, rcp.[payment_operator], rcp.[payment_operator_first_name] + ' ' + rcp.[payment_operator_last_name], 
                      rcp.[payment_operator_first_name], rcp.[payment_operator_last_name]

            UPDATE @tblRec SET [variance_unique_identifier] = (payment_date + '_' + payment_operator)
            UPDATE @tblRec SET [variance_unique_identifier] = ISNULL([variance_unique_identifier],'')

            INSERT INTO [dbo].[LTR_CASHIER_VARIANCES] ([operator_location], [operator_id], [operator_first_name], [operator_last_name], [variance_date], 
                                                       [variance_type], [variance_amount], [variance_status], [inactive], [variance_unique_identifier])
            SELECT [payment_operator_location]
                 , [payment_operator]
                 , [payment_operator_first_name]
                 , [payment_operator_last_name]
                 , [payment_date]
                 , CASE WHEN SUM([over_under]) > 0 THEN 'Over'
                        WHEN SUM([over_under]) < 0 THEN 'Under' 
                        ELSE 'even' END
                 , SUM([over_under])
                 , 'Open'
                 , 'N'
                 , [variance_unique_identifier]
            FROM @tblRec
            WHERE [over_under] <> 0.0 
              AND [variance_unique_identifier] <> '' 
              AND [variance_unique_identifier] NOT IN (SELECT [variance_unique_identifier] FROM dbo.[LTR_CASHIER_VARIANCES])
            GROUP BY [payment_operator_location], [payment_operator], [payment_operator_first_name], [payment_operator_last_name], [payment_date], [variance_unique_identifier]
            HAVING SUM([over_under]) <> 0.00

        IF @summary_only = 'Y' UPDATE @tblRec SET [payment_date] = ''

    FINISHED:

        /*  If no records found, enter one blank record so that something is passed back to the report  */

            IF NOT EXISTS (SELECT * FROM @tblRec) BEGIN
                INSERT INTO @tblRec ([payment_date], [payment_operator_location], [payment_operator], [payment_operator_full_name], [payment_type], [total_sales], [total_received], [over_under], [report_message])
                VALUES ('', '', '', '', '', 0.00, 0.00, 0.00, 'No data found for the criteria entered.')
            END

        /*  select final data set to pass back to the report  */

       IF @summary_only = 'Y'

           SELECT r.[payment_date],
                   r.[payment_operator_location],
                   r.[payment_operator],
                   r.[payment_operator_full_name],
                   r.[payment_type], 
                   sum(r.[total_sales]) as [total_sales],
                   sum(r.[total_received]) as [total_received],
                   sum(r.[over_under]) as [over_under],
                   'Summary' AS [variance_type],
                   ISNULL(v.[variance_status],'') AS [variance_status],
                   ISNULL(UPPER(LEFT(v.variance_status,2)),'') AS [variance_type_abbrev],
                   SUM(ISNULL(v.variance_amount,0.0)) AS [variance_amount],
                   CASE WHEN ISNULL(v.[variance_reason],0) = 0 THEN '' ELSE ISNULL(vr.[description],'') END AS [variance_reason],
                   '' AS [variance_notes],
                   r.[report_message]
            FROM @tblRec AS r
                 LEFT OUTER JOIN [dbo].[LTR_CASHIER_VARIANCES] AS v (NOLOCK) ON v.[variance_unique_identifier] =  r.[variance_unique_identifier] --(r.[payment_date] + '_' + r.[payment_operator])
                 LEFT OUTER JOIN [LTR_CASHIER_VARIANCE_REASONS] AS vr (NOLOCK) ON vr.[id] = v.[variance_reason]
            WHERE @variances_only = 'N'
              OR (@variances_only = 'Y' AND ISNULL(v.[variance_type],'') <> '')
              OR (@variances_only = 'Z' AND ISNULL(v.[variance_reason],0) = 2)
            GROUP BY r.[payment_date], r.[payment_operator_location], r.[payment_operator], r.[payment_operator_full_name], r.[payment_type],
                     [variance_status], ISNULL(UPPER(LEFT(v.variance_status,2)),''), CASE WHEN ISNULL(v.[variance_reason],0) = 0 THEN '' ELSE ISNULL(vr.[description],'') END, 
                     r.[report_message]

       ELSE

            SELECT r.[payment_date],
                   r.[payment_operator_location],
                   r.[payment_operator],
                   r.[payment_operator_full_name],
                   r.[payment_type], 
                   r.[total_sales],
                   r.[total_received],
                   r.[over_under],
                   ISNULL(v.[variance_type],'') AS [variance_type],
                   ISNULL(v.[variance_status],'') AS [variance_status],
                   ISNULL(UPPER(LEFT(v.variance_status,2)),'') AS [variance_type_abbrev],
                   ISNULL(v.variance_amount,0.0) AS [variance_amount],
                   CASE WHEN ISNULL(v.[variance_reason],0) = 0 THEN '' ELSE ISNULL(vr.[description],'') END AS [variance_reason],
                   ISNULL(v.[variance_notes],'') AS [variance_notes],
                   r.[report_message]
            FROM @tblRec AS r
                 LEFT OUTER JOIN [dbo].[LTR_CASHIER_VARIANCES] AS v (NOLOCK) ON v.[variance_unique_identifier] =  r.[variance_unique_identifier] --(r.[payment_date] + '_' + r.[payment_operator])
                 LEFT OUTER JOIN [LTR_CASHIER_VARIANCE_REASONS] AS vr (NOLOCK) ON vr.[id] = v.[variance_reason]
            WHERE @variances_only = 'N'
              OR (@variances_only = 'Y' AND ISNULL(v.[variance_type],'') <> '')
              OR (@variances_only = 'Z' AND ISNULL(v.[variance_reason],0) = 2)
               
END
GO

GRANT EXECUTE ON [dbo].[LRP_BOX_OFFICE_OVER_SHORT_DAILY] TO ImpUsers
GO

EXECUTE LRP_BOX_OFFICE_OVER_SHORT_DAILY @payment_dt_start = '11-1-2018', @payment_dt_end = '11-30-2018 23:59:59.957', @payment_operator_location = '', @summary_only = 'N', @variances_only = 'Z'


