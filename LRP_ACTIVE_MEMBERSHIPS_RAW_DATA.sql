USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[LRP_ACTIVE_MEMBERSHIPS_RAW_DATA]
	@MembershipActiveDate DATETIME = NULL,
	@category_str VARCHAR(4000) = ''  
AS BEGIN 

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    --Slightly updated version of LRP_ACTIVE_MEMBERSHIPS that produces the raw data used in the original report with a few additional columns.
    
    /*  Procedure Variables and Temp Tables  */

        DECLARE @MembCategory TABLE (category VARCHAR(30) NOT NULL);

        IF OBJECT_ID('tempdb..#ActiveMembershipsTbl') IS NOT NULL DROP TABLE [#ActiveMembershipsTbl]

	    CREATE TABLE #ActiveMembershipsTbl ([customer_no] INT NOT NULL DEFAULT (0),
                                            [customer_display_name] VARCHAR(200) NOT NULL DEFAULT (''),
                                            [memb_level_category_description] VARCHAR(30) NOT NULL DEFAULT (''), 
                                            [memb_level_description] VARCHAR(30) NOT NULL DEFAULT (''), 
                                            [initiation_date] DATETIME NULL, 
                                            [expiration_date] DATETIME NULL, 
                                            [current_status_desc] VARCHAR(30) NOT NULL DEFAULT (''),
                                            [membership_counter] INT NOT NULL DEFAULT (0),
                                            [onestep_counter] INT NOT NULL DEFAULT (0),
                                            [premier_counter] INT NOT NULL DEFAULT (0));

        CREATE CLUSTERED INDEX [ActiveMembershipsTbl_customer_no] ON [#ActiveMembershipsTbl] ([customer_no] ASC) ON [PRIMARY];

    /*  Check Parameters  */

        SELECT @MembershipActiveDate = ISNULL(@MembershipActiveDate, GETDATE());
        
        --IF @category_str is NULL, search ALL TR_MEMB_LEVEL_CATEGORY
        IF ISNULL(@category_str, '') <> '' BEGIN
	        INSERT INTO @MembCategory
	        SELECT REPLACE(CONVERT(VARCHAR(30),Element), CHAR(34), '') FROM [dbo].[FT_SPLIT_LIST] (@category_str,',');
        END ELSE BEGIN
	        INSERT @MembCategory
    	    SELECT [description] FROM [dbo].[TR_MEMB_LEVEL_CATEGORY];
        END;

    /*  Get Raw Data  */

        INSERT INTO #ActiveMembershipsTbl ([customer_no], [customer_display_name], [memb_level_category_description], [memb_level_description], [initiation_date],
                                           [expiration_date], [current_status_desc], [membership_counter], [onestep_counter], [premier_counter])
	    SELECT info.[customer_no],
               info.[customer_display_name], 
               info.[memb_level_category_description], 
               info.[memb_level_description], 
               info.[initiation_date], 
               info.[expiration_date], 
               'Active' AS [current_status_desc],
               1,
               CASE WHEN info.[memb_org_description] LIKE '%Household%' AND ISNULL(info.[ship_method_desc], '') = 'One Step' THEN 1 ELSE 0 END,
               CASE WHEN info.[memb_org_description] LIKE '%Household%' AND info.[memb_level_description] LIKE 'Premier%' THEN 1.0 ELSE 0.0 END
	    FROM [dbo].[LV_CURRENT_MEMBERSHIP_INFO] AS info
		     INNER JOIN @MembCategory cat ON info.[memb_level_category_description] = cat.[category]
	    WHERE @MembershipActiveDate BETWEEN info.[initiation_date] AND info.[expiration_date];
	

        INSERT INTO #ActiveMembershipsTbl ([customer_no], [customer_display_name], [memb_level_category_description], [memb_level_description], [initiation_date], 
                                           [expiration_date], [current_status_desc], [membership_counter], [onestep_counter], [premier_counter])
        -- Don't want to double count any non-current memberships if they are already ACTIVE.
	    SELECT DISTINCT info.[customer_no],
                        info.[customer_display_name],
                        info.[memb_level_category_description],
                        info.[memb_level_description],
                        info.[initiation_date],
                        info.[expiration_date],
                        info.[current_status_desc],
                        1,
                        CASE WHEN info.[memb_org_description] LIKE '%Household%' AND ISNULL(info.[ship_method_desc], '') = 'One Step' THEN 1 ELSE 0 END,
                        CASE WHEN info.[memb_org_description] LIKE '%Household%' AND info.[memb_level_description] LIKE 'Premier%' THEN 1.0 ELSE 0.0 END
	    FROM [dbo].[LV_PAST_MEMBERSHIP_INFO] AS info
		     INNER JOIN @MembCategory cat ON info.[memb_level_category_description] = cat.[category]
	    WHERE @MembershipActiveDate BETWEEN info.[initiation_date] AND info.[expiration_date]
         AND info.[customer_no] NOT IN (SELECT [customer_no] FROM #ActiveMembershipsTbl);

    FINISHED:

        SELECT [customer_no],
               [customer_display_name],
               [memb_level_category_description],
               [memb_level_description],
               [initiation_date],
               [expiration_date],
               [current_status_desc],
               [membership_counter],
               [onestep_counter],
               [premier_counter]
        FROM [#ActiveMembershipsTbl]
	    ORDER BY [memb_level_category_description], [memb_level_description];

    DONE:

        IF OBJECT_ID('tempdb..#ActiveMembershipsTbl') IS NOT NULL DROP TABLE [#ActiveMembershipsTbl];

END;
GO

GRANT EXECUTE ON [dbo].[LRP_ACTIVE_MEMBERSHIPS_RAW_DATA] TO [ImpUsers], [tessitura_app]
GO

EXECUTE [dbo].[LRP_ACTIVE_MEMBERSHIPS_RAW_DATA] @MembershipActiveDate = '12-31-2020', @category_str = 'Household'
GO



