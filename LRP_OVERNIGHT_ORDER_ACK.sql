USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LTP_ORDER_ACK]    Script Date: 11/18/2015 4:33:53 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LRP_OVERNIGHT_ORDER_ACK](
	@order_start_dt	DATETIME = NULL,
	@order_end_dt	DATETIME = NULL,
	@reprint_ind	CHAR(1) = 'N',
	@mos_str	VARCHAR(255) = NULL
)
 
AS
SET NOCOUNT ON 

 
/*********************************************************************************************************
New Procedure 8/20/2001 by CWR 

This procedure returns data for order acknowledgements and confirmations



The window which calls this is responsible for consolidating result set into DW records with the appropriate number of
LI on a page.

Modified CWR 2/25/2002 so that this can be called for a single order from the Orders Print window.
Modified CWR 3/4/2002 -- added habo_flag, exception_flag season_str, perf_end_dt and perf_start_dt parameters
	habo_flag and exception_flag are used only in confirmation mode.  The others are used in confirmation and ack modes.
Modified 3/18/2002 by CWR -- reworked where clauses where it was pulling printed or non-printed items
Modified 3/19/2002 by CWR -- added section for order bills (and additional parameter, ob_no)
Modified 5/17/2002 by CWR -- clearing out e_address variable before each constituent
Modified 5/20/2002 by CWR -- was not filtering out non-primary LI's.  Now it is
Modified 7/5/2002 by CWR -- clearing out e_address variable before each constituent
Modified 9/17/2002 by CWR -- added order notes and delivery as new columns in result set
Modified CWR 2/26/2003 -- Now using control group security views to limit output (only on season)
Modified 5/28/2003 by CWR -- fixed error whereby tot_tickets_returned_amt was being subtracted from 
	tot_tickets_purch_amt instead of added (it is a negative number)
Modified 6/30/2003 by MAR fixed problem with NULL zip and phone masks by changing the Char in the 
	convert statements where they are assigned to Varchar
Modified 9/3/2003 by CWR -- an error occurred if you tried to pass in a value for @eaddress_market_ind_no which is now fixed.
Modified 10/27/2003 by CWR -- t_default setting for label address can now be 'Yes' and 'No as well as 'Y' and 'N'
Modified 11/8/2003 by CWR -- added @balance_type parameter to further control selection of orders
Modified 11/12/2003 by CWR -- added @called_from_parameter to ap_get_phones
Modified 4/9/2004 by CWR -- if TR_UNSEATABLE_REASON was not used, the unseatable_code was always null and so selecting on
	@exception_flag = 3 would never return any rows.  This is now changed so that it checks for sli_Status = 14 (unseatable)
	as well
Modified CWR 2/15/2005 -- updated for address structure changes
Modified CWR 11/25/2005 -- now returning custom data columns
Modified CWR 1/21/2006 -- added owner name to table references
Modified CWR 7/26/2007 -- now using fs_get_default_value to get address selection defaults so that they can be organizational based
	Also fixed bug whereby mail purposes 7,8,9,0 were not being counted as valid
Modified CWR 4/8/2008 FP312.  Now getting eaddress_no from t_order if there is one and retrieving that address
Modified CWR 5/13/2008 FP572. Now using address_no from t_order if there is one
Modified CWR 4/23/2009 FP1143. This will now allow an order with no SLI's to have a one off acknowledgement printed
Modified RWC 6/2/2011 FP1912. Error fixed in 5/28/2003 regarding tot_ticketsreturnedamount was never applied to modes other
	than 'O'...
Modified CWR 9/1/2011 #4214 -- added support for street3
Modified CWR 2/15/2012 #352 -- adapted this to work with new model so that the customer_no for the order might be different
	than the customer_no on the order
Modified CWR 4/9/2012 #410 -- now using temp variable for affiliates
Modified CWR 3/12/2012 #423 -- fix on 2/15 caused the salutation to be missing.
Modified CWR 10/14/2012 #1439 -- added perf_dt to sort order
Modified RWC 4/9/2013 #2028 -- can now retrieve alt address/eaddress Initiator.  No longer validating address/eaddress 
	against primary affiliates of owner, just owner, initiator, and household(s) for inheritance.
Modified CWR 4/20/2013 #2236 -- new purpose changes
Modified RWC 6/6/2013 #2554 -- New code to validate address/eaddress constituent against order roles (owner, initiator).
	will now pick owner in the event the address is owned by both roles
Modified RAP 8/15/2013 #2156 -- Add initiator saluation and sort name to output
Modified RAP 8/26/2013 #2181 -- If owner doesn't have address, check initiator

TP_ORDER_ACK
	@order_start_dt	= '8/1/2010',
	@order_end_dt	= '8/20/2010',
	@mos_str	= '2',
	@reprint_ind	= 'N',
	@list_no	= null,
	@mailing_dt = null,
	@mailing_type  = null,
	@signer = null,
	@label_ind  = null,
	@include_general_public  = 'N',
	@report_type = 'A',  -- A, C, O, B
	@season_str = '173',
	@exception_flag = 3,
	@habo_flag = 3

select * from t_order where order_dt between '8/1/2010' and '8/20/2010' and mos in (1,3)
select * from t_customer where customer_no = 23538234

sp_what_proc 'ticket print mail purpose'

TP_GET_TICKET_ADR_ELEMENTS
TP_ORDER_ACK

Modified by: D. Jacob 11/18/2015
Used by Overnight Invoice report. Copied from TP_ORDER_ACK.

**********************************************************************************************************/
 -- DSJ 11/20/15
 -- The following variables used to be part of the stored procedure defintion. 
 -- I was not using them for the ON invoice so I removed them from the stored procedure call.
 DECLARE
 	@season_str	VARCHAR(255) = NULL,
	@perf_start_dt	DATETIME = NULL,
	@perf_end_dt	DATETIME = NULL,
	@list_no	INT = NULL,
	@mailing_dt 	DATETIME = NULL,
	@mailing_type 	VARCHAR(8) = NULL,
	@signer 	INT = NULL,
	@label_ind 	CHAR(1) = NULL,
	@eaddress_type 	INT = NULL,
	@eaddress_purpose VARCHAR(8) = NULL,
	@eaddress_market_ind_no CHAR(1) = NULL,
	@p_order_no 	INT = NULL,
	@habo_flag	INT = NULL,	-- 1 = yes, 2 = no, 3 = all; only used for confirmations
	@exception_flag INT = NULL,	-- 1 = yes, 2 = no, 3 = all; only used for confirmations
	@ob_no 	INT = 0,
	@include_general_public CHAR(1) = 'N',
	@report_type 	CHAR(1) = 'A',		-- 'A' for ack, 'C' for confirmation, 'O' for single order, 'B' for billing
	@balance_type	INT = 4		-- default is all orders; 1=no payment, 2=fully paid, 3=partially paid, 5=any balance due	

Declare @order_no int,
		@customer_no int,
		@selected_address_no int,
		@signor int,
		@initiator_no int,
		@initiator_signor int

Declare	@street1 varchar(64),
	@street2 varchar(64),
	@street3 varchar(64),
	@city varchar(30),
	@state varchar(20),
	@postal_code varchar(255),
	@zip_mask varchar(30), 
	@phone_mask varchar(255),
	@country varchar(30),
	@country_short varchar(3),
	@esal1_desc varchar(55),
	@esal2_desc varchar(55),
	@lsal_desc varchar(55),
	@business_title varchar(55),
	@day_phone varchar(30),
	@eve_phone varchar(30),
	@fax_phone varchar(30),
	@const_str varchar(255),
	@addr_customer_no int,
	@initiator_esal1_desc varchar(55),
	@initiator_esal2_desc varchar(55),
	@initiator_lsal_desc varchar(55),
	@initiator_business_title varchar(55)

Declare @eaddress varchar(255),
	@eaddress_customer_no int,
	@eaddress_no int,
	@eaddress_market_ind char(1),
	@address_no int

if isnumeric(@eaddress_market_ind_no) = 1 	-- added isnumeric CWR 9/3/2003
  Begin
	If @eaddress_market_ind_no = 1 	
		select @eaddress_market_ind = 'Y'
	ELSE
		SELECT @eaddress_market_ind = 'N'
  END

Declare @default_country int
Select 	@default_country = ISNULL(convert(int, dbo.FS_GET_DEFAULT_VALUE(null, null, 'DEFAULT_COUNTRY')), 1)

Declare @primary_affiliates Table (expanded_customer_no int)

-- make a temp table for orders
Create	Table #orders(
	customer_no int null,
	initiator_no int null,
	order_no int null,
	order_dt datetime null,
	habo_ind char(1) null,
	tot_ticket_amt money null,
	tot_fee_amt money null,
	tot_contribution_amt money null,
	tot_paid_amt money null,
	esal1_desc varchar(55) null,
	esal2_desc varchar(55) null,
	lsal_desc varchar(55) null,
	business_title varchar(55) null,
	street1 varchar(64) null,
	street2 varchar(64) null,
	street3 varchar(64) null,
	city varchar(30) null,
	state varchar(20) null,
	postal_code varchar(10) null,
	country	varchar(30) null,
	country_short varchar(3) null,
	day_phone varchar(30) null,
	eve_phone varchar(30) null,
	fax_phone varchar(30) null,
	eaddress varchar(80) null,
	li_count int null,
	const_str varchar(255) null,
	ord_notes varchar(255) null,
	delivery int null,
	custom_1 varchar(255) null,
	custom_2 varchar(255) null,
	custom_3 varchar(255) null,
	custom_4 varchar(255) null,
	custom_5 varchar(255) null,
	custom_6 varchar(255) null,
	custom_7 varchar(255) null,
	custom_8 varchar(255) null,
	custom_9 varchar(255) null,
	custom_0 varchar(255) null,
	address_no int null,
	eaddress_no int,
	initiator_esal1_desc varchar(55),
	initiator_esal2_desc varchar(55),
	initiator_lsal_desc varchar(55),
	initiator_business_title varchar(55))

Create Index orders ON #orders(order_no)

Create	table #lineitems(
	order_no int null,
	pkg_no int null,
	perf_no int null,
	perf_dt datetime null,
	perf_desc varchar(30) null,
	pkg_desc varchar(30) null,
	no_of_seats_requested int null,
	no_of_seats_assigned int null,
	price_zone varchar(30) null,
	total_li_amt money null,
	unseatable_reason int null,
	li_priority int null)

Create Index lineitems ON #lineitems(order_no)

-- temp table for MOS's

create table #tmos(id int null, description varchar(30) null)
 
IF @mos_str is null or ISNULL(Datalength(ltrim(@mos_str)),0) = 0
	Insert	into #tmos
	Select	id, description
	From	[dbo].TR_MOS
ELSE
	INSERT 	INTO #tmos
	SELECT 	id, description
	FROM 	[dbo].TR_MOS
	WHERE 	CHARINDEX(',' + CONVERT(VARCHAR,id) + ',' , ',' + @mos_str + ',') > 0
 
-- temp table for seasons

create table #tseason(id int null, description varchar(30) null)
 
IF @season_str is null or ISNULL(Datalength(ltrim(@season_str)),0) = 0
	Insert 	into #tseason
	Select 	id, description
	from 	[dbo].VRS_SEASON
ELSE
	INSERT 	INTO #tseason
	SELECT 	id, description
	FROM 	[dbo].VRS_SEASON
	WHERE 	CHARINDEX(',' + CONVERT(VARCHAR,id) + ',' , ',' + @season_str + ',') > 0


If @report_type = 'O'
  Begin

	-- get settings from defaults table
	Select	@mailing_dt = getdate()

	-- call to this function instead so we can get organizational based addresses  CWR 7/26/2007
	Select	@mailing_type = dbo.FS_GET_DEFAULT_VALUE(null, null, 'ticket print mail purpose')
	Select	@signor = Convert(int, dbo.FS_GET_DEFAULT_VALUE(null, null, 'ticket print sal type'))
	Select	@label_ind = Substring(dbo.FS_GET_DEFAULT_VALUE(null, null, 'ticket print label'), 1, 1)

	If @mailing_type is not null and IsNumeric(@mailing_type) = 0
		Select @mailing_type = null

	If @label_ind not in ('Y', 'N')
		Select @label_ind = 'N'

	-- fill up orders table
	INSERT	#orders(
		customer_no,
		initiator_no,
		order_no,
		order_dt,
		habo_ind, 
		tot_ticket_amt,
		tot_fee_amt,
		tot_contribution_amt,
		tot_paid_amt,
		ord_notes,
		delivery,
		custom_1,
		custom_2,
		custom_3,
		custom_4,
		custom_5,
		custom_6,
		custom_7,
		custom_8,
		custom_9,
		custom_0,
		address_no,
		eaddress_no)
	SELECT			
		customer_no,
		initiator_no,
		order_no,
		order_dt,
		habo_ind, 
		tot_ticket_purch_amt + tot_ticket_return_amt,
		tot_fee_amt,
		tot_contribution_amt,
		tot_paid_amt,
		notes,
		delivery,
		custom_1,
		custom_2,
		custom_3,
		custom_4,
		custom_5,
		custom_6,
		custom_7,
		custom_8,
		custom_9,
		custom_0,
		address_no,
		eaddress_no
	FROM	[dbo].T_ORDER a WITH (NOLOCK)
	WHERE	order_no = @p_order_no
  END

If @report_type = 'B'
  Begin
	-- fill up orders table
	Insert	#orders(
		customer_no,
		initiator_no,
		order_no,
		order_dt,
		habo_ind, 
		tot_ticket_amt,
		tot_fee_amt,
		tot_contribution_amt,
		tot_paid_amt,
		ord_notes,
		delivery,
		custom_1,
		custom_2,
		custom_3,
		custom_4,
		custom_5,
		custom_6,
		custom_7,
		custom_8,
		custom_9,
		custom_0,
		address_no,
		eaddress_no)
	Select			
		a.customer_no,
		a.initiator_no,
		a.order_no,
		a.order_dt,
		a.habo_ind, 
		a.tot_ticket_purch_amt + a.tot_ticket_return_amt,
		a.tot_fee_amt,
		a.tot_contribution_amt,
		a.tot_paid_amt,
		a.notes,
		a.delivery,
		custom_1,
		custom_2,
		custom_3,
		custom_4,
		custom_5,
		custom_6,
		custom_7,
		custom_8,
		custom_9,
		custom_0,
		address_no,
		eaddress_no
	From	[dbo].T_ORDER a WITH (NOLOCK)
		JOIN [dbo].T_ORDER_BILL_DETAIL b ON a.order_no = b.order_no
	Where	b.ob_no = @ob_no
  End

If @report_type = 'A'
  Begin
	-- fill up orders table
	Insert	#orders(
		customer_no,
		initiator_no,
		order_no,
		order_dt,
		habo_ind, 
		tot_ticket_amt,
		tot_fee_amt,
		tot_contribution_amt,
		tot_paid_amt,
		ord_notes,
		delivery,
		custom_1,
		custom_2,
		custom_3,
		custom_4,
		custom_5,
		custom_6,
		custom_7,
		custom_8,
		custom_9,
		custom_0,
		address_no,
		eaddress_no)
	Select			
		customer_no,
		initiator_no,
		order_no,
		order_dt,
		habo_ind, 
		tot_ticket_purch_amt + tot_ticket_return_amt,
		tot_fee_amt,
		tot_contribution_amt,
		tot_paid_amt,
		notes,
		delivery,
		custom_1,
		custom_2,
		custom_3,
		custom_4,
		custom_5,
		custom_6,
		custom_7,
		custom_8,
		custom_9,
		custom_0,
		address_no,
		eaddress_no
	From	[dbo].T_ORDER a WITH (NOLOCK)
		JOIN #tmos b ON a.mos = b.id
	Where	order_dt between @order_start_dt and @order_end_dt
		and (@list_no is null OR customer_no in (select customer_no from [dbo].T_LIST_CONTENTS where list_no = @list_no))
		and (@include_general_public = 'Y' OR customer_no > 0 )
		and (
			(@balance_type = 1 and a.tot_due_amt > 0 and a.tot_paid_amt = 0) -- no payment
			OR
			(@balance_type = 2 and a.tot_due_amt = a.tot_paid_amt)	-- totally paid
			OR
			(@balance_type = 3 and a.tot_due_amt > a.tot_paid_amt and a.tot_paid_amt > 0)  -- partially paid
			OR
			(@balance_type = 5 and a.tot_due_amt > a.tot_paid_amt)  -- any amount due
			OR
			(@balance_type = 4)	-- don't care about how much has been paid
		)		
		and (
			(@reprint_ind = 'N' and not exists (select * from [dbo].T_ORDER_SEAT_HIST WITH (NOLOCK)
				where order_no = a.order_no and event_code = 20))
			OR
			(@reprint_ind = 'Y' and exists (select * from [dbo].T_ORDER_SEAT_HIST WITH (NOLOCK)
				where order_no = a.order_no and event_code = 20))
		)
		and exists (select * from [dbo].t_lineitem x WITH (NOLOCK)
				JOIN [dbo].T_PERF y ON x.perf_no = y.perf_no
				JOIN #tseason aa ON y.season = aa.id
				where 	x.order_no = a.order_no and
					x.primary_ind = 'Y' and
					y.perf_dt between ISNULL(@perf_start_dt, '1/1/1900') and ISNULL(@perf_end_dt, '12/31/2999'))
  End


-- this is the code for confirmations in which we only look at seated SLI's and those marked unseatable.
If @report_type = 'C'		
  Begin

	-- fill up orders table
	Insert	#orders(
		customer_no,
		initiator_no,
		order_no,
		order_dt,
		habo_ind, 
		tot_ticket_amt,
		tot_fee_amt,
		tot_contribution_amt,
		tot_paid_amt,
		ord_notes,
		delivery,
		custom_1,
		custom_2,
		custom_3,
		custom_4,
		custom_5,
		custom_6,
		custom_7,
		custom_8,
		custom_9,
		custom_0,
		address_no,
		eaddress_no)
	Select			
		customer_no,
		initiator_no,
		order_no,
		order_dt,
		habo_ind, 
		tot_ticket_purch_amt + tot_ticket_return_amt,
		tot_fee_amt,
		tot_contribution_amt,
		tot_paid_amt,
		notes,
		delivery,
		custom_1,
		custom_2,
		custom_3,
		custom_4,
		custom_5,
		custom_6,
		custom_7,
		custom_8,
		custom_9,
		custom_0,
		address_no,
		eaddress_no
	From	[dbo].T_ORDER a WITH (NOLOCK)
		JOIN #tmos b ON a.mos = b.id
	Where	order_dt between @order_start_dt and @order_end_dt
		and (@list_no is null OR customer_no in (select customer_no from [dbo].T_LIST_CONTENTS where list_no = @list_no))
		and (@include_general_public = 'Y' OR customer_no > 0 )
		and (
			(@balance_type = 1 and a.tot_due_amt > 0 and a.tot_paid_amt = 0) -- no payment
			OR
			(@balance_type = 2 and a.tot_due_amt = a.tot_paid_amt)	-- totally paid
			OR
			(@balance_type = 3 and a.tot_due_amt > a.tot_paid_amt and a.tot_paid_amt > 0)  -- partially paid
			OR
			(@balance_type = 5 and a.tot_due_amt > a.tot_paid_amt)  -- any amount due
			OR
			(@balance_type = 4)	-- don't care about how much has been paid
		)		
		and (
			(@reprint_ind = 'N' and not exists (select * from [dbo].t_order_seat_hist WITH (NOLOCK)
				where order_no = a.order_no and event_code = 21))
			OR
			(@reprint_ind = 'Y' and exists (select * from [dbo].t_order_seat_hist WITH (NOLOCK)
				where order_no = a.order_no and event_code = 21))
		)
		and (
			(@habo_flag = 3) 
			OR 
			(@habo_flag = 1 and a.delivery = -1) 
			OR 
			(@habo_flag = 2 and a.delivery <> -1)
		)
		and (
			(@exception_flag = 3 and not exists (select * from [dbo].t_lineitem y WITH (NOLOCK)	-- all orders
					JOIN [dbo].t_sub_lineitem z WITH (NOLOCK) ON y.li_seq_no = z.li_seq_no 
					where 	y.order_no = a.order_no and
						z.order_no = a.order_no and
						y.primary_ind = 'Y' and
						ISNULL(z.seat_no, 0) = 0 and 
						ISNULL(z.unseatable_code, 0) = 0 ))
			OR
			(@exception_flag = 2 and not exists (select * from [dbo].t_lineitem y WITH (NOLOCK)	-- only orders with no unseated sli's
					JOIN [dbo].t_sub_lineitem z WITH (NOLOCK) ON y.li_seq_no = z.li_seq_no 
					where 	y.order_no = a.order_no and
						z.order_no = a.order_no and
						y.primary_ind = 'Y' and
						ISNULL(z.seat_no, 0) = 0))
			OR
			(@exception_flag = 1 and not exists (select * from [dbo].t_lineitem y WITH (NOLOCK)	-- only order with some unseatables and everything else seated
					JOIN [dbo].t_sub_lineitem z WITH (NOLOCK) ON y.li_seq_no = z.li_seq_no 
					where 	y.order_no = a.order_no and
						z.order_no = a.order_no and
						y.primary_ind = 'Y' and
						ISNULL(z.seat_no, 0) = 0 and 
						ISNULL(z.unseatable_code, 0) = 0 )
				and exists (select * from [dbo].T_LINEITEM y WITH (NOLOCK)	
					JOIN [dbo].t_sub_lineitem z WITH (NOLOCK) ON y.li_seq_no = z.li_seq_no 
					where 	y.order_no = a.order_no and
						z.order_no = a.order_no and
						y.primary_ind = 'Y' and
						(ISNULL(z.unseatable_code, 0) > 0 OR z.sli_status = 14)))	-- added check to sli_status CWR 4/9/2004
		)
		and exists (select * from [dbo].t_lineitem x WITH (NOLOCK)
				JOIN [dbo].T_PERF y ON x.perf_no = y.perf_no
				JOIN #tseason aa ON y.season = aa.id
				where 	x.order_no = a.order_no and
					x.primary_ind = 'Y' and
					y.perf_dt between ISNULL(@perf_start_dt, '1/1/1900') and ISNULL(@perf_end_dt, '12/31/2999'))
  End

-- run cursor to get address and salutation information
declare adr_cur insensitive cursor for
select	order_no,
	customer_no,
	initiator_no,
	eaddress_no,
	address_no		-- address_no is not currently used for anything CWR 4/7/2008
from 	#orders
OPTION	(keep plan)

open 	adr_cur
fetch 	adr_cur into @order_no, @customer_no, @initiator_no, @eaddress_no, @address_no

while	@@fetch_status = 0
  begin

	Select @const_str = null, @day_phone = null, @eve_phone = null, @fax_phone = null, @addr_customer_no = null

	If @address_no > 0
	  Begin
		select @addr_customer_no = Case When a.customer_no in (@customer_no, @initiator_no) Then a.customer_no
										When g.customer_no > 0 Then g.expanded_customer_no Else null end
		from dbo.T_ADDRESS a 
			LEFT JOIN [dbo].V_CUSTOMER_WITH_PRIMARY_AFFILIATES g on a.customer_no = g.customer_no 
				and g.expanded_customer_no in (@customer_no, @initiator_no) and g.inactive = 1
			where address_no = @address_no and a.inactive = 'N'
	  End
		
	If (@addr_customer_no) > 0
	  Begin
		Select @selected_address_no = @address_no		-- this is what we do if there is an address on the order FP572.
	  End
	ELSE
	  BEGIN
		-- only do this if there's not an address associated with the order FP572.
		Select @addr_customer_no = @customer_no
		exec	@selected_address_no = [dbo].AP_GET_ADDRESS 
				@customer_no = @customer_no,
				@mail_dt = @mailing_dt,
				@mail_type = @mailing_type,
				@label = @label_ind
		-- if no address found on owner, try initiator
		IF ISNULL(@selected_address_no, 0) = 0 
		  BEGIN
			select @addr_customer_no = @initiator_no
		  	EXEC	@selected_address_no = [dbo].AP_GET_ADDRESS 
				@customer_no = @initiator_no,
				@mail_dt = @mailing_dt,
				@mail_type = @mailing_type,
				@label = @label_ind
		  END
	  END
		
	Select @eaddress = null, @eaddress_customer_no = null

	If @eaddress_no > 0
	  Begin
		select @eaddress_customer_no = Case When a.customer_no in (@customer_no, @initiator_no) Then a.customer_no
				When g.customer_no > 0 Then g.expanded_customer_no Else null end
		from dbo.T_EADDRESS a
			LEFT JOIN [dbo].V_CUSTOMER_WITH_PRIMARY_AFFILIATES g on a.customer_no = g.customer_no 
				and g.expanded_customer_no in (@customer_no, @initiator_no) and g.inactive = 1
			where eaddress_no = @eaddress_no and a.inactive = 'N'
	  End

	-- if we don't have the eaddress_no already or if it doesn't exist, use the procedure to get it.  CWR FP312.
	If @eaddress_customer_no is NULL
	  Begin
		EXEC 	@eaddress_no = [dbo].AP_GET_EADDRESS 
				@customer_no = @customer_no,
				@mail_dt = @mailing_dt,
				@eaddress_type = @eaddress_type, 
				@mail_purpose = @eaddress_purpose, 
				@market_ind = @eaddress_market_ind
	  End

	Select	@eaddress = address
	From	[dbo].T_EADDRESS
	Where	eaddress_no = @eaddress_no

	select	@street1 = isnull(a.street1,''),
		@street2 = isnull(a.street2,''),
		@street3 = isnull(a.street3,''),
		@city = isnull(a.city,''),
		@state = isnull(a.state,''),
		@postal_code = isnull(a.postal_code,''),
		@zip_mask = isnull(convert(Varchar(10), b.zip_mask),''),
		@country = CASE WHEN b.id = @default_country THEN '' ELSE isnull(b.description, ' ') END,
		@country_short = CASE WHEN b.id = @default_country THEN '' ELSE isnull(b.short_desc, ' ') END,
		@phone_mask = isnull(convert(Varchar(30), b.phone_mask), '')
	from 	[dbo].T_ADDRESS a
		JOIN [dbo].TR_COUNTRY b ON a.country = b.id
	where	a.address_no = @selected_address_no 

	Exec [dbo].AP_FORMAT_STRING @string = @postal_code OUTPUT, @mask = @zip_mask

	exec @signor = [dbo].AP_GET_SALUTATION 
					@addr_customer_no, 
					@selected_address_no, 
					@label_ind, 
					@signer		-- changed from @customer_no to @addr_customer_no CWR 2/15/2012

	select 	@esal1_desc = esal1_desc,
			@esal2_desc = esal2_desc,
			@lsal_desc = lsal_desc,
			@business_title = business_title
	from 	[dbo].tx_cust_sal
	where 	customer_no = @addr_customer_no	
		and	signor = @signor

	if @initiator_no > 0
	  begin
	  	exec @initiator_signor = [dbo].AP_GET_SALUTATION 
					@initiator_no, 
					NULL, 
					@label_ind, 
					@signer		-- changed from @customer_no to @addr_customer_no CWR 2/15/2012

		SELECT 	@initiator_esal1_desc = esal1_desc,
				@initiator_esal2_desc = esal2_desc,
				@initiator_lsal_desc = lsal_desc,
				@initiator_business_title = business_title
		FROM 	[dbo].tx_cust_sal
		WHERE 	customer_no = @initiator_no	
			AND	signor = @initiator_signor
	  END

	Exec [dbo].AP_GET_PHONES @customer_no = @customer_no, 
			@address_no = @selected_address_no,
			@day_phone = @day_phone OUTPUT,
			@eve_phone = @eve_phone OUTPUT,
			@fax_phone = @fax_phone OUTPUT,
			@called_from = 'TP_ORDER_ACK'

	Exec [dbo].AP_FORMAT_STRING @string = @day_phone OUTPUT, @mask = @phone_mask
	Exec [dbo].AP_FORMAT_STRING @string = @eve_phone OUTPUT, @mask = @phone_mask
	Exec [dbo].AP_FORMAT_STRING @string = @fax_phone OUTPUT, @mask = @phone_mask

	Exec [dbo].AP_CONST_STRING @customer_no = @customer_no, @const_str = @const_str OUTPUT

	Update	#orders
	Set	street1 = @street1,
		street2 = @street2,
		street3 = @street3,
		city = @city,
		state = @state,
		postal_code = @postal_code,
		country = @country,
		country_short = @country_short,
		esal1_desc = @esal1_desc,
		esal2_desc = @esal2_desc,
		lsal_desc = @lsal_desc,
		business_title = @business_title,
		day_phone = @day_phone,
		eve_phone = @eve_phone,
		fax_phone = @fax_phone,
		eaddress = @eaddress,
		const_str = @const_str,
		initiator_esal1_desc = @initiator_esal1_desc,
		initiator_esal2_desc = @initiator_esal2_desc,
		initiator_lsal_desc = @initiator_lsal_desc,
		initiator_business_title = @initiator_business_title
	Where	order_no = @order_no

	FETCH 	adr_cur INTO @order_no, @customer_no, @initiator_no, @eaddress_no, @address_no
  END

close		adr_cur
deallocate 	adr_cur


-- Populate #lineitems
Insert	#lineitems(
	order_no,
	pkg_no,
	perf_no,
	perf_dt,
	perf_desc,
	pkg_desc,
	no_of_seats_requested,
	no_of_seats_assigned,
	price_zone,
	total_li_amt,
	unseatable_reason,
	li_priority)
Select	a.order_no,
	b.pkg_no,
	b.perf_no,
	c.perf_dt,
	d.description,
	e.description,
	no_of_seats_requested = ISNULL(b.num_seats_pur, 0) - ISNULL(b.num_seats_ret, 0),
	no_of_seats_assigned = (Select count(*) from [dbo].T_SUB_LINEITEM where li_seq_no = b.li_seq_no and seat_no > 0 and sli_status in (1,2,3,12)),
	price_zone = CASE 
		WHEN (Select Count(distinct zone_no) 
			from [dbo].T_SUB_LINEITEM 
			where li_seq_no = b.li_seq_no and seat_no > 1 and sli_status in (1,2,3,12)) > 1 
			Then '**mixed**' 
		Else (Select description from [dbo].T_ZONE where zone_no = b.zone_no and zmap_no = c.zmap_no) END,
	total_li_amt = (Select sum(due_amt) from [dbo].T_SUB_LINEITEM where li_seq_no = b.li_seq_no and sli_status <> 14),
	unseatable_reason = (select max(unseatable_code) from [dbo].T_SUB_LINEITEM where li_seq_no = b.li_seq_no and sli_status = 14),
	li_priority = ISNULL(b.priority, 1)
from	#orders a
	JOIN [dbo].T_LINEITEM b WITH (NOLOCK) ON a.order_no = b.order_no and IsNull(b.primary_ind, 'Y') = 'Y'
	JOIN [dbo].T_PERF c ON b.perf_no = c.perf_no and c.perf_no > 0
	JOIN [dbo].T_INVENTORY d ON c.perf_no = d.inv_no
	LEFT OUTER JOIN [dbo].T_PKG e ON b.pkg_no = e.pkg_no and b.pkg_no > 0

-- fill in the li_count column in #lineitems
Update	#orders 
Set	li_count = (select count(*) from #lineitems where order_no = #orders.order_no)

-- add rows to order history table, unless this is a reprint
If @reprint_ind = 'N'
	Insert 	[dbo].t_order_seat_hist(event_code, order_no, customer_no)
	Select	CASE WHEN @report_type = 'A' Then 20 Else 21 END,
			order_no,
			customer_no
	From	#orders


-- return rows with temp tables joined 
SELECT	a.customer_no,
	a.order_no,
	a.order_dt,
	a.habo_ind,
	a.tot_ticket_amt  ,
	a.tot_fee_amt,
	a.tot_contribution_amt,
	a.tot_paid_amt,
	a.ord_notes,
	delivery = ISNULL(d.description, ''),
	a.esal1_desc,
	a.esal2_desc,
	a.lsal_desc,
	a.business_title,
	a.street1,
	a.street2,
	a.street3,
	a.city,
	a.state,
	a.postal_code,
	a.country,
	a.country_short,
	a.day_phone,
	a.eve_phone,
	a.fax_phone,
	a.const_str,
	a.eaddress,
	ISNULL(a.li_count, 0) AS li_count,	
	0 AS current_page_no,	-- placeholder for current page_no
	0 AS total_pages,	-- placeholder for total pages for this order
	a.custom_1,
	a.custom_2,
	a.custom_3,
	a.custom_4,
	a.custom_5,
	a.custom_6,
	a.custom_7,
	a.custom_8,
	a.custom_9,
	a.custom_0,
	a.initiator_no,
	di.display_name AS initiator_name, 
	di.sort_name AS initiator_sort_name,
	a.initiator_esal1_desc,
	a.initiator_esal2_desc,
	a.initiator_lsal_desc,
	a.initiator_business_title,
	b.pkg_no,
	b.perf_no,
	b.perf_dt,
	b.perf_desc,
	b.pkg_desc,
	ISNULL(b.no_of_seats_requested, 0) AS no_of_seats_requested,
	ISNULL(b.no_of_seats_assigned, 0) AS no_of_seats_assigned,
	b.price_zone,
	ISNULL(b.total_li_amt, 0) AS total_li_amt,
	unseatable_reason = CASE WHEN b.unseatable_reason = 0 THEN 'Unseatable' 
							WHEN b.unseatable_reason IS NULL THEN '' 
							ELSE ISNULL(c.description, 'Unseatable') END ,
	b.li_priority,
	'The following invoice summarizes the fees your organization has been charged to attend an Overnight Program at the Museum of Science. Your group''s date of attendance ' +
	CASE 
		WHEN DATEDIFF(d,GETDATE(), b.perf_dt) <= 0 THEN 'was '
		ELSE 'is '
	END + CONVERT(VARCHAR(10), b.perf_dt, 101) + '.' AS invoice_summ
FROM	#orders a
	LEFT OUTER JOIN #lineitems b ON a.order_no = b.order_no
	LEFT OUTER JOIN [dbo].TR_UNSEATABLE_REASON c ON b.unseatable_reason = c.id
	LEFT OUTER JOIN [dbo].TR_ORDER_SHIP_METHOD d ON a.delivery = d.id
	LEFT OUTER JOIN [dbo].FT_CONSTITUENT_DISPLAY_NAME() di ON a.initiator_no = di.customer_no
WHERE	(b.perf_no IS NOT NULL OR @report_type = 'O')
ORDER BY a.customer_no, a.order_no, b.perf_dt
RETURN


GO