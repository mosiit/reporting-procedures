USE [impresario]
GO
/****** Object:  StoredProcedure [dbo].[LP_UPDATE_HISTORY_CONTRACT_ANALYSIS_MONTHLY]    Script Date: 10/1/2021 12:31:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[LP_UPDATE_HISTORY_CONTRACT_ANALYSIS_MONTHLY]
        @contract_id INT = NULL
AS BEGIN

    SET NOCOUNT ON;
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

    /*  Procedure Variables  */

        DECLARE @pub_prod_no INT = 0, 
                @sch_prod_no INT = 0, 
                @prod_no INT = 0,
                @prod_name VARCHAR(30) = ''

        DECLARE @start_dt DATE = NULL, 
                @end_dt DATE = NULL,
                @month_num INT = 1,
                @month_start_dt DATE = NULL,
                @month_end_dt DATE = NULL
        
        DECLARE @license_admission INT = 1, 
                @license_revenue INT = 2, 
                @license_greater INT = 3
        
        DECLARE @license_type INT,
                @total_payments DECIMAL(18,2)

        DECLARE @contract_start AS DATE, @contract_end AS DATE
        DECLARE @trigger_month_map TABLE (month_id VARCHAR(10), trigger_no INT)

        DECLARE @buyouts_are_per_screening INT = 0,
                @per_screening_total DECIMAL (18,2) =0.0

    /*  Temporary Tables  */

        IF OBJECT_ID('tempdb..#monthly_contract_data') is not null DROP TABLE [#monthly_contract_data]

        CREATE TABLE [#monthly_contract_data]   ([contract_id] INT NOT NULL DEFAULT (0),
                                         [contract_name] VARCHAR(100) NOT NULL DEFAULT (''),
                                         [vendor_no] INT NOT NULL DEFAULT (0),
                                         [vendor_name] VARCHAR(100) NOT NULL DEFAULT (''),
                                         [vendor_address_1] VARCHAR(100) NOT NULL DEFAULT (''),
                                         [vendor_Address_2] VARCHAR(100) NOT NULL DEFAULT (''),
                                         [vendor_city] VARCHAR(50) NOT NULL DEFAULT (''),
                                         [vendor_state] VARCHAR(25) NOT NULL DEFAULT (''),
                                         [vendor_zip_code] VARCHAR(25) NOT NULL DEFAULT (''),
                                         [public_prod_no] INT NOT NULL DEFAULT (0),
                                         [public_prod_name] VARCHAR(30) NOT NULL DEFAULT (''),
                                         [school_prod_no] INT NOT NULL DEFAULT (0),
                                         [school_prod_name] VARCHAR(30) NOT NULL DEFAULT (''),
                                         [reporting_type] VARCHAR(30) NOT NULL DEFAULT (''),
                                         [license_type] INT NOT NULL DEFAULT (0),
                                         [license_type_name] VARCHAR(30) NOT NULL DEFAULT (''),
                                         [start_dt] DATETIME NULL,
                                         [end_dt] DATETIME NULL,
                                         [contract_comp_percentage] DECIMAL(18,4) NOT NULL DEFAULT (0.0),
                                         [minmum_shows] INT NOT NULL DEFAULT (0),
                                         [minimum_shows_achieved] DATE NULL,
                                         [attendance_guarantee] INT NOT NULL DEFAULT (0),
                                         [attendance_guarantee_achieved] DATE NULL,
                                         [schedule_guarantee_percentage] DECIMAL (18,4) NOT NULL DEFAULT (0.0),
                                         [schedule_guarantee_days] INT NOT NULL DEFAULT (0),
                                         [schedule_actual_percentage] DECIMAL(18,4) NOT NULL DEFAULT (0.0),
                                         [schedule_guarantee_achieved] CHAR(1) NOT NULL DEFAULT ('N'),
                                         [total_contract_payments] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                         [first_trigger] CHAR(1) NOT NULL DEFAULT ('N'),
                                         [trigger_no] INT NOT NULL DEFAULT (0),
                                         [trigger_type] VARCHAR(30) NOT NULL DEFAULT (''),
                                         [trigger_range_low] INT NOT NULL DEFAULT (0),
                                         [trigger_range_high] INT NOT NULL DEFAULT (0),
                                         [trigger_description] VARCHAR(50) NOT NULL DEFAULT (''),
                                         [trigger_start_date] DATE NULL,
                                         [trigger_end_date] DATE NULL,
                                         [trigger_achieved] CHAR(1) NOT NULL DEFAULT ('N'),
                                         [trigger_status] VARCHAR(50) NOT NULL DEFAULT ('Pending'),
                                         [show_count] INT NOT NULL DEFAULT (0),
                                         [comp_attendance] INT NOT NULL DEFAULT (0),
                                         [comp_attendance_percentage] DECIMAL(18,4) NOT NULL DEFAULT (0.0),
                                         [comp_difference] INT NOT NULL DEFAULT (0),
                                         [public_attendance] INT NOT NULL DEFAULT (0),
                                         [public_revenue] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                         [public_owed] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                         [public_net] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                         [school_attendance] INT NOT NULL DEFAULT (0),
                                         [school_revenue] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                         [school_owed] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                         [school_net] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                         [buyout_attendance] INT NOT NULL DEFAULT (0),
                                         [buyout_revenue] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                         [buyout_owed] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                         [buyout_net] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                         [comp_owed] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                         [total_attendance] INT NOT NULL DEFAULT (0),
                                         [total_revenue] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                         [total_owed] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                         [total_net] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                         [comp_percentage_allowed] DECIMAL(18,4) NOT NULL DEFAULT (0.0),
                                         [comp_allowed] INT NOT NULL DEFAULT(0),
                                         [per_admission_public] DECIMAL(18,2) NOT NULL DEFAULT (0.0), 
                                         [per_admission_school] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                         [per_admission_private] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                         [per_admission_comp] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                         [revenue_percent_public] DECIMAL(18,4) NOT NULL DEFAULT (0.0),
                                         [revenue_percent_school] DECIMAL(18,4) NOT NULL DEFAULT (0.0),
                                         [revenue_percent_private] DECIMAL(18,4) NOT NULL DEFAULT (0.0),
                                         [revenue_amount_comp] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                         [free_screenings_private] INT NOT NULL DEFAULT (0),
                                         [per_screening_private] DECIMAL(18,2) NOT NULL DEFAULT (0.0))
        
        IF OBJECT_ID('tempdb..#monthly_attend_data') is not null DROP TABLE [#monthly_attend_data]

        CREATE TABLE [#monthly_attend_data] ([rec_no] INT NOT NULL IDENTITY (1,1), 
                                             [contract_id] INT NOT NULL DEFAULT (0),
                                             [contract_name] VARCHAR(100) NOT NULL DEFAULT (''),
                                             [license_type] INT NOT NULL DEFAULT (0),
                                             [license_type_name] VARCHAR(30) NOT NULL DEFAULT (''),
                                             [contract_payments] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                             [title_no] INT NOT NULL DEFAULT (0), 
                                             [title_name] VARCHAR(30) NOT NULL DEFAULT (''), 
                                             [prod_no] INT NOT NULL DEFAULT (0), 
                                             [prod_name] VARCHAR(30) NOT NULL DEFAULT (''), 
                                             [perf_month] INT NOT NULL DEFAULT (0),
                                             [perf_month_name] VARCHAR(30) NOT NULL DEFAULT (''),
                                             [perf_year] INT NOT NULL DEFAULT (0),
                                             [perf_month_sort] CHAR(7) NOT NULL DEFAULT (''),
                                             [perf_month_multiple] SMALLINT NOT NULL DEFAULT (0),
                                             [trigger_no] INT NOT NULL DEFAULT (0),
                                             [month_start_date] DATE NULL,
                                             [month_end_date] DATE NULL,
                                             [show_count] INT NOT NULL DEFAULT (0), 
                                             [show_count_running] INT NOT NULL DEFAULT (0),
                                             [comp_attendance] INT NOT NULL DEFAULT (0),
                                             [comp_attendance_running] INT NOT NULL DEFAULT (0),
                                             [comp_attendance_percentage] DECIMAL(18,4) NOT NULL DEFAULT (0.0),
                                             [comp_allowed] INT NOT NULL DEFAULT(0),
                                             [comp_difference] INT NOT NULL DEFAULT (0),
                                             [comp_owed_per] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                             [comp_owed_rev] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                             [comp_owed] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                             [public_attendance] INT NOT NULL DEFAULT (0),
                                             [public_attendance_running] INT NOT NULL DEFAULT (0),
                                             [public_revenue] DECIMAL (18,2) NOT NULL DEFAULT (0.0),
                                             [public_revenue_running] DECIMAL (18,2) NOT NULL DEFAULT (0.0),
                                             [public_owed_per] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                             [public_owed_rev] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                             [public_owed] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                             [public_net] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                             [school_show_count] INT NOT NULL DEFAULT (0), 
                                             [school_show_count_running] INT NOT NULL DEFAULT (0),
                                             [school_attendance] INT NOT NULL DEFAULT (0),
                                             [school_attendance_running] INT NOT NULL DEFAULT (0),
                                             [school_revenue] DECIMAL (18,2) NOT NULL DEFAULT (0.0),
                                             [school_revenue_running] DECIMAL (18,2) NOT NULL DEFAULT (0.0),
                                             [school_owed_per] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                             [school_owed_rev] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                             [school_owed] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                             [school_net] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                             [buyout_show_count] INT NOT NULL DEFAULT (0), 
                                             [buyout_show_count_running] INT NOT NULL DEFAULT (0),
                                             [buyout_attendance] INT NOT NULL DEFAULT (0),
                                             [buyout_attendance_running] INT NOT NULL DEFAULT (0),
                                             [buyout_revenue] DECIMAL (18,2) NOT NULL DEFAULT (0.0),
                                             [buyout_revenue_running] DECIMAL (18,2) NOT NULL DEFAULT (0.0),
                                             [buyout_owed_per] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                             [buyout_owed_rev] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                             [buyout_owed_per_perf]  DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                             [buyout_owed] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                             [buyout_net] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                             [total_attendance] INT NOT NULL DEFAULT (0),
                                             [total_attendance_running] INT NOT NULL DEFAULT (0),
                                             [total_revenue] DECIMAL (18,2) NOT NULL DEFAULT (0.0),
                                             [total_revenue_running] DECIMAL (18,2) NOT NULL DEFAULT (0.0),
                                             [total_owed] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                             [total_net] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                             [comp_percentage_allowed] DECIMAL(18,4) NOT NULL DEFAULT (0.0),
                                             [per_admission_public] DECIMAL(18,2) NOT NULL DEFAULT (0.0), 
                                             [per_admission_school] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                             [per_admission_private] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                             [per_admission_comp] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                             [revenue_percent_public] DECIMAL(18,4) NOT NULL DEFAULT (0.0),
                                             [revenue_percent_school] DECIMAL(18,4) NOT NULL DEFAULT (0.0),
                                             [revenue_percent_private] DECIMAL(18,4) NOT NULL DEFAULT (0.0),
                                             [revenue_amount_comp] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                             [free_screenings_private] INT NOT NULL DEFAULT (0),
                                             [per_screening_private] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                             [amt_display_public] VARCHAR(30) NOT NULL DEFAULT (''), 
                                             [amt_display_school] VARCHAR(30) NOT NULL DEFAULT (''),
                                             [amt_display_private] VARCHAR(30) NOT NULL DEFAULT (''),
                                             [amt_display_comp] VARCHAR(30) NOT NULL DEFAULT (''))

        IF OBJECT_ID('tempdb..#monthly_attend_raw') is not null DROP TABLE [#monthly_attend_raw]

        CREATE TABLE [#monthly_attend_raw]    ([title_no] INT NOT NULL DEFAULT (0), 
                                             [title_name] VARCHAR(30) NOT NULL DEFAULT (''), 
                                             [prod_no] INT NOT NULL DEFAULT (0), 
                                             [prod_name] VARCHAR(30) NOT NULL DEFAULT (''), 
                                             [perf_date] DATE NULL, 
                                             [perf_day] VARCHAR(30) NOT NULL DEFAULT (''),
                                             [perf_month] INT NOT NULL DEFAULT (0),
                                             [perf_month_name] VARCHAR(30) NOT NULL DEFAULT (''),
                                             [perf_year] INT NOT NULL DEFAULT (0),
                                             [perf_month_sort] CHAR(7) NOT NULL DEFAULT (''),
                                             [trigger_no] INT NOT NULL DEFAULT (0),
                                             [show_count] INT NOT NULL DEFAULT (0), 
                                             [show_count_running] INT NOT NULL DEFAULT (0),
                                             [comp_attendance] INT NOT NULL DEFAULT (0),
                                             [comp_attendance_running] INT NOT NULL DEFAULT (0),
                                             [public_attendance] INT NOT NULL DEFAULT (0),
                                             [public_attendance_running] INT NOT NULL DEFAULT (0),
                                             [public_revenue] DECIMAL (18,2) NOT NULL DEFAULT (0.0),
                                             [public_revenue_running] DECIMAL (18,2) NOT NULL DEFAULT (0.0),
                                             [school_show_count] INT NOT NULL DEFAULT (0), 
                                             [school_show_count_running] INT NOT NULL DEFAULT (0),
                                             [school_attendance] INT NOT NULL DEFAULT (0),
                                             [school_attendance_running] INT NOT NULL DEFAULT (0),
                                             [school_revenue] DECIMAL (18,2) NOT NULL DEFAULT (0.0),
                                             [school_revenue_running] DECIMAL (18,2) NOT NULL DEFAULT (0.0),
                                             [buyout_show_count] INT NOT NULL DEFAULT (0), 
                                             [buyout_show_count_running] INT NOT NULL DEFAULT (0),
                                             [buyout_attendance] INT NOT NULL DEFAULT (0),
                                             [buyout_attendance_running] INT NOT NULL DEFAULT (0),
                                             [buyout_revenue] DECIMAL (18,2) NOT NULL DEFAULT (0.0),
                                             [buyout_revenue_running] DECIMAL (18,2) NOT NULL DEFAULT (0.0),
                                             [total_attendance] INT NOT NULL DEFAULT (0),
                                             [total_attendance_running] INT NOT NULL DEFAULT (0),
                                             [total_revenue] DECIMAL (18,2) NOT NULL DEFAULT (0.0),
                                             [total_revenue_running] DECIMAL (18,2) NOT NULL DEFAULT (0.0))

        IF OBJECT_ID('tempdb..#monthly_comp') is not null DROP TABLE [#monthly_comp]

        CREATE TABLE [#monthly_comp] (          [comp_year] INT NOT NULL DEFAULT (0),
                                                [comp_month] INT NOT NULL DEFAULT (0),
                                                [comp_month_id] VARCHAR (10) NOT NULL DEFAULT (''),
                                                [comp_per_admission_amount] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                                [comp_percent_allowed] DECIMAL (18,4) NOT NULL DEFAULT (0.0),
                                                [month_total_attendance] DECIMAL (18,4) NOT NULL DEFAULT (0.0),
                                                [month_comp_allowed] DECIMAL (18,4) NOT NULL DEFAULT (0.0),
                                                [month_comp_attendance] DECIMAL (18,4) NOT NULL DEFAULT (0.0),
                                                [month_comp_difference] DECIMAL (18,4) NOT NULL DEFAULT (0.0),
                                                [month_comp_owed] DECIMAL (18,2) NOT NULL DEFAULT (0.0),
                                                [trigger_no] INT NOT NULL DEFAULT (0))

        IF OBJECT_ID('tempdb..#monthly_buyouts') is not null DROP TABLE [#monthly_buyouts]

        CREATE TABLE [#monthly_buyouts] (       [buyout_year] INT NOT NULL DEFAULT (0),
                                                [buyout_month] INT NOT NULL DEFAULT (0),
                                                [buyout_month_id] VARCHAR(25) NOT NULL DEFAULT (''),
                                                [buyout_per_admission_amount] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                                [month_total_attendance] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                                [month_buyout_attendance] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                                [buyout_per_show_cost] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                                [buyout_show_count] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                                [buyout_show_running] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                                [buyout_show_count_paid] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                                [month_buyout_owed] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                                [trigger_no] INT NOT NULL DEFAULT (0))
    


    /**************************************************
        Check Parameters
        If no vaid contract id number is passed to the report, it cannot continue.
     **************************************************/

        SELECT @contract_id = ISNULL(@contract_id, 0)
        IF @contract_id <= 0 BEGIN

            THROW 51000, 'Invalid Contract Id Number.', 1;  

            GOTO FINISHED

        END

    /**************************************************
        Pull the contract data.
     **************************************************/

        INSERT INTO [#monthly_contract_data] 
                    ([contract_id], [contract_name], [vendor_no], [vendor_name], [vendor_address_1], [vendor_Address_2], [vendor_city],
                     [vendor_state], [vendor_zip_code], [public_prod_no], [public_prod_name], [school_prod_no], [school_prod_name],
                     [reporting_type], [license_type], [license_type_name], [start_dt], [end_dt], [contract_comp_percentage],
                     [minmum_shows], [minimum_shows_achieved], [attendance_guarantee], [attendance_guarantee_achieved], 
                     [schedule_guarantee_percentage], [schedule_guarantee_days], [schedule_actual_percentage], [schedule_guarantee_achieved],
                     [total_contract_payments], [first_trigger], [trigger_no], [trigger_type], [trigger_range_low], [trigger_range_high],
                     [trigger_description], [trigger_start_date], [trigger_end_date], [trigger_achieved], [trigger_status], [show_count],
                     [comp_attendance], [comp_attendance_percentage], [comp_difference], [public_attendance], [public_revenue], [public_owed],
                     [public_net], [school_attendance], [school_revenue], [school_owed], [school_net], [buyout_attendance], [buyout_revenue], 
                     [buyout_owed], [buyout_net], [comp_owed], [total_attendance], [total_revenue], [total_owed], [total_net], 
                     [comp_percentage_allowed], [comp_allowed], [per_admission_public], [per_admission_school], [per_admission_private], 
                     [per_admission_comp], [revenue_percent_public], [revenue_percent_school], [revenue_percent_private], [revenue_amount_comp], 
                     [free_screenings_private], [per_screening_private])
        SELECT       [contract_id], [contract_name], [vendor_no], [vendor_name], [vendor_address_1], [vendor_Address_2], [vendor_city],
                     [vendor_state], [vendor_zip_code], [public_prod_no], [public_prod_name], [school_prod_no], [school_prod_name],
                     [reporting_type], [license_type], [license_type_name], [start_dt], [end_dt], [contract_comp_percentage],
                     [minmum_shows], [minimum_shows_achieved], [attendance_guarantee], [attendance_guarantee_achieved], 
                     [schedule_guarantee_percentage], [schedule_guarantee_days], [schedule_actual_percentage], [schedule_guarantee_achieved],
                     [total_contract_payments], [first_trigger], [trigger_no], [trigger_type], [trigger_range_low], [trigger_range_high],
                     [trigger_description], [trigger_start_date], [trigger_end_date], [trigger_achieved], [trigger_status], [show_count],
                     [comp_attendance], [comp_attendance_percentage], [comp_difference], [public_attendance], [public_revenue], [public_owed],
                     [public_net], [school_attendance], [school_revenue], [school_owed], [school_net], [buyout_attendance], [buyout_revenue], 
                     [buyout_owed], [buyout_net], [comp_owed], [total_attendance], [total_revenue], [total_owed], [total_net], 
                     [comp_percentage_allowed], [comp_allowed], [per_admission_public], [per_admission_school], [per_admission_private], 
                     [per_admission_comp], [revenue_percent_public], [revenue_percent_school], [revenue_percent_private], [revenue_amount_comp], 
                     [free_screenings_private], [per_screening_private]
        FROM [dbo].[LT_HISTORY_OMNI_CONTRACT]
        WHERE contract_id = @contract_id

        SELECT @per_screening_total = SUM(per_screening_private) FROM [#monthly_contract_data]
        SELECT @buyouts_are_per_screening = CASE WHEN ISNULL(@per_screening_total, 0) > 0 THEN 1 ELSE 0 END
 
     /**************************************************
        Determine earliest and latest date reporting on, as well as the production name to use
        These values are used throughout the procedure.  [#contract_info] *should* only have
        a single row, but used MIN and MAX anyway, just in case.
     **************************************************/

        SELECT @start_dt = MIN(start_dt), 
               @end_dt = MAX(end_dt),
               @license_type = MAX([license_type])
        FROM [#monthly_contract_data]
        WHERE [first_trigger] = 'Y'


    /**************************************************
        Extract public and school production numbers as well as total payments from [#contract_info]
        Procedure picks one to use as the title for everything. It will choose the public title, 
        unless there isn't one.  If no public title, it uses the school title.
     **************************************************/

        SELECT @pub_prod_no = ISNULL(MAX([public_prod_no]), 0),
               @sch_prod_no = ISNULL(MAX([school_prod_no]), 0),
               @total_payments = ISNULL(MAX([total_contract_payments]),0.0)
        FROM [#monthly_contract_data] 
        WHERE [first_trigger] = 'Y'
        
        IF @pub_prod_no > 0 SELECT @prod_no = @pub_prod_no
        ELSE SELECT @prod_no = @sch_prod_no

        SELECT @prod_name = ISNULL([description], 'Unknown Title') 
                            FROM [dbo].[T_INVENTORY] 
                            WHERE [inv_no] = @prod_no
  
        SELECT @month_start_dt = @start_dt,
               @month_end_dt = EOMONTH(@month_start_dt)

        WHILE @month_end_dt <= @end_dt BEGIN

            /**************************************************
                Generate the Attendance Matrix with running totals 
                This is a stored procedure written specifically for this report but it may
                be used for other reports as well.  Pass it a date range and one or two
                production numbers and it generates a grid of attendance data day by day 
                with running totals so that the total attendance can be pulled for any given day 
                within the run of the movie.  Takes two productions in case there is a
                public and a school production for the same show.
             **************************************************/

                INSERT INTO [#monthly_attend_raw] ([title_no],[title_name],[prod_no],[prod_name],[perf_date],[perf_day],[show_count],[show_count_running], 
                                                [comp_attendance], [comp_attendance_running], [public_attendance],[public_attendance_running], 
                                                [public_revenue], [public_revenue_running], [school_show_count], [school_show_count_running],
                                                [school_attendance], [school_attendance_running], [school_revenue], [school_revenue_running],
                                                [buyout_show_count], [buyout_show_count_running], [buyout_attendance], [buyout_attendance_running], 
                                                [buyout_revenue], [buyout_revenue_running], [total_attendance],[total_attendance_running],[total_revenue], 
                                                [total_revenue_running])
                --EXECUTE [dbo].[LRP_CREATE_ATTENDANCE_MATRIX] @report_start_dt = @month_start_dt, @report_end_dt = @month_end_dt, @public_prod_no = @pub_prod_no, @school_prod_no = @sch_prod_no
                SELECT [title_no],
                       [title_name],
                       [prod_no],
                       [prod_name],
                       [perf_date],
                       [perf_day],
                       [show_count],
                       [show_count_running], 
                       [cmp_sale_total], 
                       [cmp_sale_total_running], 
                       [pub_sale_total],
                       [cmp_sale_total_running], 
                       [pub_paid_amount], 
                       [pub_paid_amount_running],
                       [sch_show_count], 
                       [sch_show_count_running],
                       [sch_sale_total], 
                       [sch_sale_total_running], 
                       [sch_paid_amount],
                       [sch_paid_amount_running],
                       [buy_show_count],
                       [buy_show_count_running], 
                       [buy_sale_total], 
                       [buy_sale_total_running], 
                       [buy_paid_amount],
                       [buy_paid_amount_running], 
                       [tot_sale_total],
                       [tot_sale_total_running],
                       [tot_paid_amount], 
                       [tot_paid_amount_running]
                FROM [dbo].[LT_HISTORY_ATTENDANCE_MATRIX]
                WHERE [perf_date] BETWEEN @month_start_dt AND  @month_end_dt
                  AND [prod_no] IN (@pub_prod_no, @sch_prod_no)
                ORDER BY [perf_date]

            /*  Move to the next month  */

                SELECT @month_start_dt = DATEADD(DAY, 1, @month_End_dt)
                SELECT @month_end_dt = EOMONTH(@month_start_dt)
                IF @month_start_dt < @end_dt and @month_end_dt > @end_dt SELECT @month_end_dt = @end_dt
                SELECT @month_num += 1

    END

    UPDATE [#monthly_attend_raw]
    SET [perf_month] = DATEPART(MONTH,[perf_date]),
        [perf_month_name] = DATENAME(MONTH, [perf_date]),
        [perf_year] = DATEPART(YEAR,[perf_date]),
        [perf_month_sort] = FORMAT([perf_date],'yyyy/MM')

    UPDATE rd
    SET rd.[trigger_no] = cd.[trigger_no]
    FROM [#monthly_attend_raw] AS rd
         INNER JOIN [#monthly_contract_data] AS cd ON rd.[perf_date] BETWEEN cd.[trigger_start_date] AND ISNULL(cd.[trigger_end_date], [end_dt])

        INSERT INTO [#monthly_attend_data] ([contract_id], [contract_name], [license_type], [license_type_name], [title_no], [title_name],
                                            [prod_no], [prod_name], [perf_month], [perf_month_name], [perf_year],[perf_month_sort],[trigger_no], [month_start_date],
                                            [month_end_date], [show_count], [comp_attendance], [public_attendance], [public_revenue], [school_show_count], 
                                            [school_attendance], [school_revenue], [buyout_show_count], [buyout_attendance], [buyout_revenue],
                                            [total_attendance],[total_revenue], [comp_percentage_allowed],[comp_allowed],
                                            [per_admission_public], [per_admission_school],[per_admission_private], [per_admission_comp],[revenue_percent_public],
                                            [revenue_percent_school], [revenue_percent_private],[revenue_amount_comp],[per_screening_private])
        SELECT cd.[contract_id],
               cd.[contract_name],
               cd.[license_type],
               cd.[license_type_name],
               rd.[title_no],
               rd.[title_name],
               rd.[prod_no],
               rd.[prod_name],
               rd.[perf_month],
               rd.[perf_month_name],
               rd.[perf_year],
               rd.[perf_month_sort],
               rd.[trigger_no],
               MIN(rd.[perf_date]),
               MAX(rd.[perf_date]),
               SUM(rd.[show_count]),
               SUM(rd.[comp_attendance]),
               SUM(rd.[public_attendance]),
               SUM(rd.[public_revenue]),
               SUM(rd.[school_show_count]),
               SUM(rd.[school_attendance]),
               SUM(rd.[school_revenue]),
               SUM(rd.[buyout_show_count]),
               SUM(rd.[buyout_attendance]),
               SUM(rd.[buyout_revenue]),
               SUM(rd.[total_attendance]),
               SUM(rd.[total_revenue]),
               cd.[comp_percentage_allowed],
               cd.[comp_allowed],
               cd.[per_admission_public],
               cd.[per_admission_school],
               cd.[per_admission_private],
               cd.[per_admission_comp],
               cd.[revenue_percent_public],
               cd.[revenue_percent_school],
               cd.[revenue_percent_private],
               cd.[revenue_amount_comp],
               cd.[per_screening_private]
        FROM [#monthly_attend_raw] rd
             LEFT OUTER JOIN [#monthly_contract_data] AS cd ON cd.[trigger_no] = rd.[trigger_no]
        GROUP BY cd.[contract_id], cd.[contract_name], cd.[license_type], cd.[license_type_name], rd.[title_no], rd.[title_name], 
                 rd.[prod_no], rd.[prod_name], rd.[perf_month], rd.[perf_month_name], rd.[perf_year], rd.[perf_month_sort], rd.[trigger_no], 
                 cd.[comp_percentage_allowed], cd.[comp_allowed], cd.[per_admission_public], cd.[per_admission_school], cd.[per_admission_private], 
                 cd.[per_admission_comp], cd.[revenue_percent_public], cd.[revenue_percent_school], cd.[revenue_percent_private], cd.[revenue_amount_comp], 
                 cd.[per_screening_private]  
        ORDER BY rd.[perf_month_sort], rd.[trigger_no]

      /**************************************************
        Determine the percent of comp tickets in each of the date ranges.
        Allowed comps takes the total attendance and multiplies it by the percent allowed.
        The actual comp percentage is the comp attendance divided by the total attedance.
       **************************************************/
    
        UPDATE [#monthly_attend_data]
        SET [comp_allowed] = (CAST([total_attendance] AS DECIMAL(18,4)) * [comp_percentage_allowed])

        UPDATE [#monthly_attend_data]
        SET [comp_attendance_percentage] = (CAST([comp_attendance] AS DECIMAL(18,4)) / CAST([total_attendance] AS DECIMAL(18,4)))
        WHERE [Total_attendance] <> 0

        UPDATE [#monthly_attend_data]
        SET [comp_difference] = CASE WHEN ([comp_attendance] - [comp_allowed]) < 0 THEN 0
                                     ELSE ([comp_attendance] - [comp_allowed]) END

      /**************************************************
        Calculate what is owed by the museum based on the contract terms of each trigger
        Both the per_admission and the revenue rates are calculated because one of the license types
        is the greater of the two.  After the values are calculated, one is chosen based on
        what the license type is for the contract.
     **************************************************/
       
       UPDATE [#monthly_attend_data]
       SET [public_owed_rev] = [public_revenue] * [revenue_percent_public],
           [school_owed_rev] = [school_revenue] * [revenue_percent_school],
           [buyout_owed_rev] = [buyout_revenue] * [revenue_percent_private],
           [comp_owed_rev] = CASE WHEN [comp_difference] < 0 THEN 0.0 
                                  ELSE (([comp_difference] * [revenue_amount_comp]) * [revenue_percent_public]) END
       
        UPDATE [#monthly_attend_data]
        SET [public_attendance] = ([public_attendance] - [comp_attendance])

        UPDATE [#monthly_attend_data]
        SET [public_owed_per] = CAST([public_attendance] AS DECIMAL(18,2)) * [per_admission_public],
            [school_owed_per] = CAST([school_attendance] AS DECIMAL(18,2)) * [per_admission_school],
            [buyout_owed_per] = CAST([buyout_attendance] AS DECIMAL(18,2)) * [per_admission_private],
            [comp_owed_per] = CASE WHEN [comp_difference] < 0 THEN 0.0 
                                   ELSE ([comp_difference] * [per_admission_comp]) END
        
        IF @license_type = @license_greater 
  
            UPDATE [#monthly_attend_data]
            SET [public_owed] = CASE WHEN [public_owed_rev] > [public_owed_per] THEN [public_owed_rev]
                                     ELSE [public_owed_per] END,
                [school_owed] = CASE WHEN [school_owed_rev] > [school_owed_per] THEN [school_owed_rev]
                                     ELSE [school_owed_per] END,
                [buyout_owed] = CASE WHEN [buyout_owed_rev] > [buyout_owed_per] THEN [buyout_owed_rev]
                                     ELSE [buyout_owed_per] END,
                [comp_owed] =  CASE WHEN [comp_owed_rev] > [comp_owed_per] THEN [comp_owed_rev]
                                    ELSE [comp_owed_per] END,
                [amt_display_public] = CASE WHEN [public_owed_rev] > [public_owed_per] THEN FORMAT(([revenue_percent_public] * 100), '##0.00') + '%'
                                            ELSE '$' + FORMAT([per_admission_public], '##0.00') END,
                [amt_display_school] = CASE WHEN [school_owed_rev] > [school_owed_per] THEN FORMAT(([revenue_percent_school] * 100), '##0.00') + '%'
                                            ELSE '$' + FORMAT([per_admission_school], '##0.00') END,
                [amt_display_private] = CASE WHEN [buyout_owed_rev] > [buyout_owed_per] THEN FORMAT(([revenue_percent_private] * 100), '##0.00') + '%'
                                            ELSE '$' + FORMAT([per_admission_private], '##0.00') END,
                [amt_display_comp] = CASE WHEN [comp_owed_rev] > [comp_owed_per] THEN '$' + FORMAT([revenue_amount_comp], '##0.00')
                                     ELSE '$' + FORMAT([per_admission_comp], '##0.00') END
        ELSE IF @license_type = @license_revenue 
            UPDATE [#monthly_attend_data]
            SET [public_owed] = [public_owed_rev],
                [school_owed] = [school_owed_rev],
                [comp_owed] = [comp_owed_rev],
                [buyout_owed] = [buyout_owed_rev],
                [amt_display_public] = FORMAT(([revenue_percent_public] * 100), '##0.00') + '%',
                [amt_display_school] = FORMAT(([revenue_percent_school] * 100), '##0.00') + '%',
                [amt_display_comp] = '$' + FORMAT([revenue_amount_comp], '##0.00')
        ELSE        
            UPDATE [#monthly_attend_data]
            SET [public_owed] = [public_owed_per],
                [school_owed] = [school_owed_per],
                [comp_owed] = [comp_owed_per],
                [buyout_owed] = [buyout_owed_per],
                [amt_display_public] = '$' + FORMAT([per_admission_public], '##0.00'),
                [amt_display_school] = '$' + FORMAT([per_admission_school], '##0.00'),
                [amt_display_comp] = '$' + FORMAT([per_admission_comp], '##0.00')
            WHERE [total_revenue] <> 0.0;

        /****************************************
         DETERMINE MONTHLY COMP OWED
        ****************************************/

        INSERT INTO [#monthly_comp] ([comp_year], [comp_month], [comp_month_id], [comp_per_admission_amount], 
                                       [comp_percent_allowed], [month_total_attendance], [month_comp_attendance],
                                       [month_comp_allowed], [month_comp_difference], [month_comp_owed], [trigger_no])
        SELECT [comp_year],
               [comp_month],
               [comp_month_id],
               [comp_per_admission_amount],
               [comp_percent_allowed],
               [month_total_attendance],
               [month_comp_attendance],
               [month_comp_allowed],
               [month_comp_difference],
               [month_comp_owed],
               [trigger_no]
        FROM [dbo].[LT_HISTORY_OMNI_CONTRACT_COMPS]
        WHERE contract_id = @contract_id;

        WITH CTE_PRIMARY ([month], [trigger])
        AS (SELECT [perf_month_sort], MIN([trigger_no])
            FROM [#monthly_attend_data]
            GROUP BY [perf_month_sort])
        UPDATE at1
        SET at1.[comp_attendance] = (at1.[comp_attendance] + ISNULL(at2.[comp_attendance],0))
        FROM [#monthly_attend_data] AS at1
             INNER JOIN [CTE_PRIMARY] AS pri ON pri.[month] = at1.[perf_month_sort] and pri.[trigger] = at1.[trigger_no]
             LEFT OUTER JOIN [#monthly_attend_data] AS at2 ON at2.[perf_month_sort] = at1.[perf_month_sort] AND at2.[trigger_no] = (at1.[trigger_no] + 1);

        WITH CTE_PRIMARY ([month], [trigger])
        AS (SELECT [perf_month_sort], MIN([trigger_no])
            FROM [#monthly_attend_data]
            GROUP BY [perf_month_sort])
        UPDATE at2
        SET at2.[comp_attendance] = 0
        FROM [#monthly_attend_data] AS at1
             INNER JOIN [CTE_PRIMARY] AS pri ON pri.[month] = at1.[perf_month_sort] and pri.[trigger] = at1.[trigger_no]
             LEFT OUTER JOIN [#monthly_attend_data] AS at2 ON at2.[perf_month_sort] = at1.[perf_month_sort] AND at2.[trigger_no] = (at1.[trigger_no] + 1);

       UPDATE [at1]
       SET at1.[comp_allowed] = cmp.[month_comp_allowed],
           at1.[comp_difference] = (at1.[comp_attendance] - cmp.[month_comp_allowed]),
           at1.[comp_owed_per] = ((at1.[comp_attendance] - cmp.[month_comp_allowed]) * cmp.[comp_per_admission_amount]),
           at1.[comp_owed] = ((at1.[comp_attendance] - cmp.[month_comp_allowed]) * cmp.[comp_per_admission_amount])
       FROM [#monthly_attend_data] AS at1
            INNER JOIN [#monthly_comp] AS cmp ON cmp.[comp_month_id] = at1.[perf_month_sort]
       WHERE at1.[comp_attendance] > 0

       UPDATE [at1]
       SET at1.[comp_allowed] = 0,
           at1.[comp_difference] = 0,
           at1.[comp_owed_per] = 0,
           at1.[comp_owed] = 0,
           at1.[comp_attendance_percentage] = 0,
           at1.[amt_display_comp] = ''
       FROM [#monthly_attend_data] AS at1
            INNER JOIN [#monthly_comp] AS cmp ON cmp.[comp_month_id] = at1.[perf_month_sort]
       WHERE at1.[comp_attendance] = 0

     /****************************************
         DETERMINE MONTHLY BUYOUT OWED
        ****************************************/

        IF @buyouts_are_per_screening = 1 BEGIN

            INSERT INTO [#monthly_buyouts] ([buyout_year], [buyout_month], [buyout_month_id], [buyout_per_admission_amount], [month_total_attendance],
                                            [month_buyout_attendance], [buyout_per_show_cost], [buyout_show_count], [buyout_show_running], [buyout_show_count_paid],
                                            [month_buyout_owed], [trigger_no])
            SELECT  [buyout_year],
                    [buyout_month],
                    [buyout_month_id],
                    [buyout_per_admission_amount],
                    [month_total_attendance],
                    [month_buyout_attendance],
                    [buyout_per_show_cost],
                    [buyout_show_count],
                    [buyout_show_running],
                    [buyout_show_count_paid],
                    [month_buyout_owed],
                    [trigger_no]
            FROM [dbo].[LT_HISTORY_OMNI_CONTRACT_BUYOUTS]
            WHERE [contract_id] = @contract_id

            UPDATE md
            SET md.[buyout_show_count_running] = ISNULL(bo.[buyout_show_count_paid], 0),
                [buyout_owed_per_perf] = ISNULL(bo.[buyout_per_show_cost], 0),
                [buyout_owed] = ISNULL(bo.[month_buyout_owed], 0)
            FROM [#monthly_attend_data] AS md
                 LEFT OUTER JOIN [#monthly_buyouts] AS bo ON bo.[buyout_month_id] = md.[perf_month_sort] AND bo.[trigger_no] = md.[trigger_no]

        END

    /**************************************************
        Total up the amount owed, then calculate the net difference betweeen
        what came in and what we are paying out.
     **************************************************/

        UPDATE [#monthly_attend_data]
        SET [total_owed] = ([public_owed] + [school_owed] + [buyout_owed] + [comp_owed])

        UPDATE [#monthly_attend_data]
        SET [public_net] = ([public_revenue] - [public_owed] - [comp_owed]),
            [school_net] = ([school_revenue] - [school_owed]),
            [buyout_net] = ([buyout_revenue] - [buyout_owed]),
            [total_net] = ([total_revenue] - [total_owed])

        UPDATE [#monthly_attend_data]
        SET [perf_month_multiple] = 1
        WHERE [perf_month_sort] IN (SELECT [perf_month_sort] 
                                    FROM [#monthly_attend_data]
                                    GROUP BY [perf_month_sort]
                                    HAVING COUNT(*) > 1)

     /**************************************************
        Add the total payments made to the first record only.  This field remains as
        zero on all the other records so that a sum of the column produces the same
        results as pulling it directly from the one record where it resides.
      **************************************************/                               

        UPDATE [#monthly_attend_data]
        SET [contract_payments] = @total_payments
        WHERE [rec_no] = 1
    
    FINISHED:

            DELETE FROM [dbo].[LT_HISTORY_OMNI_CONTRACT_MONTHLY] WHERE [contract_id] = @contract_id

            INSERT INTO [dbo].[LT_HISTORY_OMNI_CONTRACT_MONTHLY]
                       ([contract_id], [contract_name], [license_type], [license_type_name], [contract_payments], [title_no], [title_name], [prod_no], [prod_name],
                        [perf_month], [perf_month_name], [perf_year], [perf_month_sort], [perf_month_multiple], [trigger_no], [month_start_date], [month_end_date],
                        [show_count], [show_count_running], [comp_attendance], [comp_attendance_running], [comp_attendance_percentage], [comp_allowed], [comp_difference],
                        [comp_owed_per], [comp_owed_rev], [comp_owed], [public_attendance], [public_attendance_running], [public_revenue], [public_revenue_running],
                        [public_owed_per], [public_owed_rev], [public_owed], [public_net], [school_show_count], [school_show_count_running], [school_attendance],
                        [school_attendance_running], [school_revenue], [school_revenue_running], [school_owed_per], [school_owed_rev], [school_owed], [school_net],
                        [buyout_show_count], [buyout_show_count_running], [buyout_owed_per_perf], [buyout_attendance], [buyout_attendance_running], [buyout_revenue], 
                        [buyout_revenue_running], [buyout_owed_per], [buyout_owed_rev], [buyout_owed], [buyout_net], [total_attendance], [total_attendance_running], 
                        [total_revenue], [total_revenue_running], [total_owed], [total_net], [comp_percentage_allowed], [per_admission_public], [per_admission_school], 
                        [per_admission_private], [per_admission_comp], [revenue_percent_public], [revenue_percent_school], [revenue_percent_private], [revenue_amount_comp], 
                        [free_screenings_private], [per_screening_private], [amt_display_public], [amt_display_school], [amt_display_private], [amt_display_comp])
             SELECT [contract_id],
                    [contract_name],
                    [license_type],
                    [license_type_name],
                    [contract_payments],
                    [title_no],
                    [title_name],
                    [prod_no],
                    [prod_name],
                    [perf_month],
                    [perf_month_name],
                    [perf_year],
                    [perf_month_sort],
                    [perf_month_multiple],
                    [trigger_no],
                    [month_start_date],
                    [month_end_date],
                    [show_count],
                    [show_count_running],
                    [comp_attendance],
                    [comp_attendance_running],
                    [comp_attendance_percentage],
                    [comp_allowed],
                    [comp_difference],
                    [comp_owed_per],
                    [comp_owed_rev],
                    [comp_owed],
                    [public_attendance],
                    [public_attendance_running],
                    [public_revenue],
                    [public_revenue_running],
                    [public_owed_per],
                    [public_owed_rev],
                    [public_owed],
                    [public_net],
                    [school_show_count],
                    [school_show_count_running],
                    [school_attendance],
                    [school_attendance_running],
                    [school_revenue],
                    [school_revenue_running],
                    [school_owed_per],
                    [school_owed_rev],
                    [school_owed],
                    [school_net],
                    [buyout_show_count],
                    [buyout_show_count_running],
                    [buyout_owed_per_perf],
                    [buyout_attendance],
                    [buyout_attendance_running],
                    [buyout_revenue],
                    [buyout_revenue_running],
                    [buyout_owed_per],
                    [buyout_owed_rev],
                    [buyout_owed],
                    [buyout_net],
                    [total_attendance],
                    [total_attendance_running],
                    [total_revenue],
                    [total_revenue_running],
                    [total_owed],
                    [total_net],
                    [comp_percentage_allowed],
                    [per_admission_public],
                    [per_admission_school],
                    [per_admission_private],
                    [per_admission_comp],
                    [revenue_percent_public],
                    [revenue_percent_school],
                    [revenue_percent_private],
                    [revenue_amount_comp],
                    [free_screenings_private],
                    [per_screening_private],
                    [amt_display_public],
                    [amt_display_school],
                    [amt_display_private],
                    [amt_display_comp]
            FROM [#monthly_attend_data] 
            ORDER BY [perf_month_sort], [trigger_no]

    DONE:
    
        IF OBJECT_ID('tempdb..#monthly_contract_data') is not null DROP TABLE [#monthly_contract_data]
        IF OBJECT_ID('tempdb..#monthly_attend_data') is not null DROP TABLE [#monthly_attend_data]
        IF OBJECT_ID('tempdb..#monthly_attend_raw') is not null DROP TABLE [#monthly_attend_raw]
        IF OBJECT_ID('tempdb..#monthly_comp') is not null DROP TABLE [#monthly_comp]
        IF OBJECT_ID('tempdb..#monthly_buyouts') is not null DROP TABLE [#monthly_buyouts]

END;
GO

EXECUTE [dbo].[LP_UPDATE_HISTORY_CONTRACT_ANALYSIS_MONTHLY] @contract_id = 6