USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_ADV_CAMPAIGN_ACTIVITY_MAJOR]') AND type IN (N'P', N'PC'))
BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_ADV_CAMPAIGN_ACTIVITY_MAJOR] AS' 
    EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LRP_ADV_CAMPAIGN_ACTIVITY_MAJOR] TO impusers'
END
GO

/*      LRP_ADV_CAMPAIGN_ACTIVITY_MAJOR
        Pulls a list from the LV_CAMPAIGN_ACTIVITY_CONTRIBUTIONS view of all customers who have given a total gift on a single day of $25,000 or more within the last 90 days.
        It could be comprised of multiple contributions with the same contribution date and long as the campaign category is not �skip�.
        
        NOTE: Uses LV_CAMPAIGN_ACTIVITY_CONTRIBUTIONS view which is a view that was created using the
              same logic in the MOS Advancement Contributions report (LRP_MOS_ADVANCEMENT_CONTRIBUTION_DETAIL).
*/
ALTER PROCEDURE [dbo].[LRP_ADV_CAMPAIGN_ACTIVITY_MAJOR]
        @fiscal_year INT = NULL
WITH RECOMPILE AS BEGIN

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

    /*  Check Fiscal Year Parameter - If none passed, set to current  */
        IF @fiscal_year IS NULL SELECT @fiscal_year = dbo.LF_GetFiscalYear(GETDATE())

    /*  Procedure Variables  */

         --Determine current fiscal year date information
        DECLARE @fiscal_start_dt DATETIME = [dbo].[LF_GetFiscalYearStartDt](@fiscal_year)
        DECLARE @fiscal_end_dt DATETIME = [dbo].[LF_GetFiscalYearEndDt](@fiscal_year)
        DECLARE @current_dt DATETIME = GETDATE()
        IF @current_dt > @fiscal_end_dt SELECT @current_dt = @fiscal_end_dt

        --Determine 90 days ago start date
        DECLARE @ninety_days_ago_dt DATETIME = DATEADD(DAY,-90,CONVERT(DATE,@current_dt))

        DECLARE @major_gifts TABLE ([customer_no] INT NOT NULL DEFAULT(0),
                                    [constituent_name] VARCHAR(100) NOT NULL DEFAULT (''),
                                    [sort_name] VARCHAR(100) NOT NULL DEFAULT (''),
                                    [contribution_date] DATETIME NULL,
                                    [contribution_amount] DECIMAL (18,2) NOT NULL DEFAULT (0))

    /*  Ninety days date should never go beyond the first day of the fiscal year  */

        IF @ninety_days_ago_dt < @fiscal_start_dt SELECT @ninety_days_ago_dt = @fiscal_start_dt

    /*  Get list of 25K + Gifts in the last 90 daya*/

        INSERT INTO @major_gifts ([customer_no], [constituent_name], [sort_name], [contribution_date], [contribution_amount])
        SELECT [customer_no],
               [constituent_name],
               [sort_name],
               CONVERT(DATE,[contribution_date]), 
               SUM([contribution_amount])
        FROM [dbo].[LV_CAMPAIGN_ACTIVITY_CONTRIBUTIONS]
        WHERE [contribution_date] between @ninety_days_ago_dt AND @current_dt
          AND [campaign_category] <> 'Skip'
        GROUP BY [customer_no], [constituent_name], [sort_name], CONVERT(DATE,[contribution_date])
        HAVING SUM([contribution_amount]) >= 25000
    
    FINISHED:
    
        /*  Select final data set to return to the report  */

            SELECT [customer_no],
                   [constituent_name],
                   [sort_name],
                   [contribution_date],
                   [contribution_amount] 
            FROM @major_gifts
             
END 
GO

EXECUTE [dbo].[LRP_ADV_CAMPAIGN_ACTIVITY_MAJOR] @fiscal_year = 2020
