USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_BOX_OFFICE_OVER_SHORT]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LRP_BOX_OFFICE_OVER_SHORT]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LRP_BOX_OFFICE_OVER_SHORT]
        @payment_dt_start DATETIME = Null,
	    @payment_dt_end DATETIME = Null,
	    @payment_operator_location VARCHAR(30) = Null,
        @suppress_dates_with_zero_sales CHAR(1) = NULL,
        @variances_only CHAR(1) = 'N'
AS BEGIN

    /*  procedure variables  */
        DECLARE @report_message VARCHAR(100) = ''

        DECLARE @operator_list TABLE ([payment_operator] VARCHAR(10), [first_name] VARCHAR(30), [last_name] VARCHAR(30), [location] VARCHAR(30))
        
        DECLARE @tblRec TABLE	([payment_operator_location]  VARCHAR(30), [payment_operator] VARCHAR(8), [payment_operator_full_name] VARCHAR(80),
		    				     [payment_type_name] VARCHAR(30), [payment_amt] MONEY, [report_message] VARCHAR(100))

        DECLARE @daily_table TABLE ([payment_date] CHAR(10), [payment_operator_location] VARCHAR(50), [payment_operator] VARCHAR(50), [payment_operator_full_name] varchar(100),
                                    [payment_type] varchar(50), [total_sales] decimal(18,2), [total_received] DECIMAL(18,2), [over_under] DECIMAL(18,2), [variance_type] VARCHAR(50),
                                    [variance_status] VARCHAR(50), [variance_type_abbrev] VARCHAR(10), [variance_amount] DECIMAL(18,2), [variance_reason] VARCHAR(50),
                                    [variance_notes] VARCHAR(100), [report_message] VARCHAR(100))

    /*  check parameters  */

        SELECT @payment_dt_start = ISNULL(@payment_dt_start,DATEADD(DAY,-1,GETDATE()));
        SELECT @payment_dt_end = ISNULL(@payment_dt_end,DATEADD(DAY,-1,GETDATE()));
        SELECT @payment_operator_location = ISNULL(@payment_operator_location,'');
        SELECT @suppress_dates_with_zero_sales = ISNULL(@suppress_dates_with_zero_sales,'Y');


    /*  Get Operator List */

        INSERT INTO @operator_list ([payment_operator], [first_name], [last_name], [location])
        SELECT DISTINCT [payment_operator],
                        [payment_operator_first_name],
                        [payment_operator_last_name],
                        [payment_operator_location] 
        FROM dbo.LT_CASH_OUT_DATA 
        WHERE payment_date BETWEEN CONVERT(CHAR(10),@payment_dt_start,111) AND CONVERT(CHAR(10),@payment_dt_end,111)
          AND (@payment_operator_location = '' or payment_operator = @payment_operator_location)


        INSERT INTO @tblRec ([payment_operator_location], [payment_operator], [payment_operator_full_name], [payment_type_name], [payment_amt], [report_message])
	    SELECT  opr.[location], 
                rcp.[payment_operator], 
                opr.[first_name] + ' ' + opr.[last_name],
                rcp.payment_type_name, 
                SUM(ISNULL(rcp.[payment_amount], 0)), ''
        FROM [dbo].[LV_RPT_CASHOUT_PAYMENTS] rcp
             INNER JOIN @operator_list AS opr ON opr.[payment_operator] = rcp.[payment_operator]
	    WHERE CONVERT(VARCHAR(10), rcp.[payment_dt], 111) BETWEEN @payment_dt_start AND @payment_dt_end
          AND rcp.[payment_type_name] IN ('Cash', 'Check', 'Credit Card', 'Gift Certificate')
	    GROUP BY opr.[location], rcp.[payment_operator], opr.[first_name] + ' ' + opr.[last_name], rcp.[payment_type_name]

        INSERT INTO @tblRec ([payment_operator_location], [payment_operator], [payment_operator_full_name], [payment_type_name], [payment_amt], [report_message])
        SELECT opr.[location], 
               rcp.[payment_operator], 
               opr.[first_name] + ' ' + opr.[last_name],
               'Total Sales', 
               SUM(ISNULL(rcp.[payment_amount], 0)), ''
        FROM [dbo].[LV_RPT_CASHOUT_PAYMENTS] rcp
             INNER JOIN @operator_list AS opr ON opr.[payment_operator] = rcp.[payment_operator]
        WHERE CONVERT(VARCHAR(10), rcp.[payment_dt], 111) BETWEEN @payment_dt_start AND @payment_dt_end
          AND rcp.[payment_type_name] IN ('Cash', 'Check', 'Gift Certificate')
        GROUP BY opr.[location], rcp.[payment_operator], opr.[first_name] + ' ' + opr.[last_name]


        INSERT INTO @tblRec ([payment_operator_location], [payment_operator], [payment_operator_full_name], [payment_type_name], [payment_amt], [report_message])
        SELECT  opr.[location], 
                cod.[payment_operator], 
                opr.[first_name] + ' ' + opr.[last_name],
                'Total Received',
                SUM(ISNULL(cod.[payment_amount], 0)), ''    
        FROM [dbo].[LT_CASH_OUT_DATA] cod
             INNER JOIN @operator_list AS opr ON opr.[payment_operator] = cod.[payment_operator]
        WHERE CONVERT(VARCHAR(10), cod.[payment_date], 111) BETWEEN CONVERT(VARCHAR(10), @payment_dt_start, 111) AND CONVERT(VARCHAR(10), @payment_dt_end, 111)
         AND cod.[payment_type_name] IN ('Cash', 'Check', 'Gift Certificate')
        GROUP BY opr.[location], cod.[payment_operator], opr.[first_name] + ' ' + opr.[last_name]

        /*  This is run to populate the LT_CASHIER_VARIANCES table  */

            INSERT INTO @daily_table
            EXECUTE LRP_BOX_OFFICE_OVER_SHORT_DAILY @payment_dt_start = @payment_dt_start, @payment_dt_end = @payment_dt_end, @payment_operator_location = '', @summary_only = 'N', @variances_only = @variances_only

            IF @variances_only <> 'N'
                DELETE FROM @tblRec
                WHERE [payment_operator] NOT IN (SELECT [payment_operator] FROM @daily_table)

    FINISHED:

        /*  If no records found, set the @report_message variable and enter one blank record so that something is passed back to the report  */

            IF NOT EXISTS (SELECT * FROM @tblRec) BEGIN
                SELECT @report_message = 'No data found for the criteria entered.'
                INSERT INTO @tblRec ([payment_operator_location], [payment_operator], [payment_operator_full_name], [payment_type_name], [payment_amt], [report_message])
                VALUES ('', '', '', '', 0.00, @report_message)
            END

        /*  select final data set to pass back to the report  */

            SELECT  payment_operator_location AS 'Location',
                    payment_operator AS 'Operator', 
                    payment_operator_full_name AS 'Operator Name',
                    ISNULL([Cash],0.00) AS 'Cash',
                    ISNULL([Check],0.00) AS 'Check',
                    ISNULL([Credit Card], 0.00) AS 'Credit Card',
                    ISNULL([Other],0.00) AS 'Other',
		            ISNULL([Total Sales], 0) AS [Total Sales],
		            ISNULL([Total Received], 0) AS [Total Received],
		            ISNULL([Total Received], 0) - ISNULL([Total Sales], 0) AS [Over Under],
                    ISNULL(@report_message,'') AS 'report_message'
            FROM    (SELECT payment_operator_location,
                            payment_operator,
                            payment_operator_full_name,
                            payment_type_name,
                            payment_amt
                     FROM   @tblRec) AS SourceTable PIVOT
            (SUM(payment_amt) FOR payment_type_name IN ([Cash], [Check], [Credit Card], [Other], [Total Sales], [Total Received], [Over Under]) ) AS PivotTable;

END
GO

GRANT EXECUTE ON [dbo].[LRP_BOX_OFFICE_OVER_SHORT] TO impusers
GO


EXECUTE [dbo].[LRP_BOX_OFFICE_OVER_SHORT] @payment_dt_start = '5-15-2019', @payment_dt_end = '5-15-2019 23:59:59', 
                                          @payment_operator_location = 'schals00', @suppress_dates_with_zero_sales = 'Y', @variances_only = 'N'


--EXECUTE [dbo].[LRP_BOX_OFFICE_OVER_SHORT] @payment_dt_start = '5-15-2019', @payment_dt_end = '5-15-2019 23:59:59', 
--                                          @payment_operator_location = '', @suppress_dates_with_zero_sales = 'Y', @variances_only = 'N'

