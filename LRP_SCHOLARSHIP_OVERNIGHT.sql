USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_SCHOLARSHIP_OVERNIGHT]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_SCHOLARSHIP_OVERNIGHT] AS' 
END
GO

ALTER PROCEDURE [dbo].[LRP_SCHOLARSHIP_OVERNIGHT]
        @report_start_dt DATETIME = NULL, 
        @report_end_dt DATETIME = NULL,
        @customer_type_id INT = 0,
        @scholarship_id VARCHAR(30) = '-1',
        @sort_by VARCHAR(30) = NULL,
        @list_no INT = NULL
WITH RECOMPILE AS BEGIN

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    SET NOCOUNT ON;
        
    /*  Procedure Variables  */

        DECLARE @ord_no INT, @perf_date CHAR(10), @prod_name VARCHAR(50);
        DECLARE @cust_type_id_school INT, @cust_type_id_school_official INT;
    
    /*  Check Parameters  */
    
        IF @report_start_dt IS NULL
            SELECT @report_start_dt = dbo.LF_GetFiscalYearStartDt(dbo.LF_GetFiscalYear(GETDATE()));

        IF @report_end_dt IS NULL
            SELECT @report_end_dt = dbo.LF_GetFiscalYearStartDt(dbo.LF_GetFiscalYear(@report_start_dt));

        SELECT @customer_type_id = ISNULL(@customer_type_id,0);
        SELECT @sort_by = ISNULL(@sort_by,'Customer Name');

        SELECT @scholarship_id = ISNULL(@scholarship_id, '-1');
        IF @scholarship_id IN ('','0') SELECT @scholarship_id = '-1';

        SELECT @list_no = ISNULL(@list_no,0)
        
    /*  Make sure the date parameters include the full dates, starting at 00:00 and ending at 23:59  */

        SELECT @report_start_dt = CAST(@report_start_dt AS DATE),
               @report_end_dt = CONVERT(CHAR(10),@report_end_dt,111) + ' 23:59:59.957';

    /*  For the purposes of this report, the School and School Official Record customer types are the same thing.
        The id numbers are needed to combine the two  */

        SELECT @cust_type_id_school = ISNULL([id],0)
                                      FROM [dbo].[TR_CUST_TYPE] 
                                      WHERE [description] = 'School';

         SELECT @cust_type_id_school_official = ISNULL([id],0)
                                                FROM [dbo].[TR_CUST_TYPE] 
                                                WHERE [description] = 'School Official Record';
       
    /*  If customer type id selected was school official record, change to school  */

        IF @customer_type_id = @cust_type_id_school_official 
            SELECT @customer_type_id = @cust_type_id_school;
    
    /*  Create temporary tables  */
            
            IF OBJECT_ID('tempdb..#overnight_orders') IS NOT NULL DROP TABLE [#overnight_orders];

            CREATE TABLE [#overnight_orders] ([order_no] INT);

            IF OBJECT_ID('tempdb..#overnight_data') IS NOT NULL DROP TABLE [#overnight_data];

            CREATE TABLE [#overnight_data] ([data_type] VARCHAR(10), 
                                         [order_no] INT, 
                                         [sli_no] INT, 
                                         [customer_no] INT, 
                                         [performance_date] CHAR(10), 
                                         [title_name] VARCHAR(30), 
                                         [scholarship_no] INT,
                                         [production_name] VARCHAR(50), 
                                         [zone_name] VARCHAR(30), 
                                         [price_type_name] VARCHAR(30), 
                                         [create_dt] DATETIME, 
                                         [due_amount] DECIMAL(18,2), 
                                         [total_paid] DECIMAL(18,2),
                                         [visitor_counter] INT);

            IF OBJECT_ID('tempdb..#overnight_final') IS NOT NULL DROP TABLE [#overnight_final];

            CREATE TABLE [#overnight_final] ([rpt_message] VARCHAR(100), 
                                          [data_type] VARCHAR(10), 
                                          [order_no] INT, 
                                          [sli_no] INT, 
                                          [customer_no] INT, 
                                          [order_performance_date] CHAR(10), 
                                          [performance_date] CHAR(10),
                                          [title_name] VARCHAR(50), 
                                          [scholarship_no] INT, 
                                          [production_name] VARCHAR(50), 
                                          [zone_name] VARCHAR(50), 
                                          [performance_code] VARCHAR(25), 
                                          [price_type_name] VARCHAR(50), 
                                          [create_dt] DATETIME,
                                          [due_amount] MONEY, 
                                          [total_paid] MONEY, 
                                          [customer_first_name] VARCHAR(50), 
                                          [customer_middle_name] VARCHAR(50),
                                          [customer_last_name] VARCHAR(100), 
                                          [customer_type_no] INT, 
                                          [customer_type] VARCHAR(30), 
                                          [address_type_no] INT, 
                                          [address_type] VARCHAR(30), 
                                          [address_street1] VARCHAR(75), 
                                          [address_street2] VARCHAR(75), 
                                          [address_city] VARCHAR(50), 
                                          [address_state] VARCHAR(50), 
                                          [address_zip_code] VARCHAR(50), 
                                          [address_city_state] VARCHAR(100), 
                                          [order_custom_1] VARCHAR(100), 
                                          [order_custom_2] VARCHAR(100), 
                                          [order_custom_3] VARCHAR(100), 
                                          [order_custom_4] VARCHAR(100), 
                                          [order_custom_5] VARCHAR(100), 
                                          [order_custom_6] VARCHAR(100), 
                                          [order_custom_7] VARCHAR(100), 
                                          [order_custom_8] VARCHAR(100), 
                                          [order_custom_9] VARCHAR(100), 
                                          [order_custom_10] VARCHAR(100), 
                                          [order_notes] VARCHAR(255),
                                          [visitor_counter] INT,
                                          [order_scholarship] VARCHAR(50), 
                                          [sort_field] VARCHAR(255), 
                                          [sort_text] VARCHAR(255), 
                                          [program_counter] int);

    /*  Generate a list of all traveling programs sales for designated date range  */

        WITH CTE_SCHOL_PAY ([order_no]) AS 
        (
         SELECT DISTINCT [order_no] 
         FROM [dbo].[LV_SCHOLARSHIP_PAYMENTS]
        )
        INSERT INTO [#overnight_orders] ([order_no])
        SELECT DISTINCT det.[order_no]
        FROM [dbo].[LV_ORDER_DETAIL] AS det
             INNER JOIN [CTE_SCHOL_PAY] AS pay ON pay.[order_no] = det.[order_no]
             INNER JOIN [dbo].[T_ORDER] AS ord ON ord.[order_no] = det.[order_no] 
        WHERE [performance_dt] BETWEEN @report_start_dt AND @report_end_dt 
          AND ord.[MOS] = 5;     --5 = Overnights

    /* Get overnight Order Data */
    
        INSERT INTO [#overnight_data] ([data_type], [order_no], [sli_no], [customer_no], [performance_date], [title_name], [scholarship_no], [production_name], 
                                       [zone_name], [price_type_name], [create_dt], [due_amount], [total_paid], [visitor_counter])
        SELECT 'ord_info', 
                det.[order_no], 
                det.[sli_no], 
                det.[customer_no], 
                det.[performance_date], 
                det.[title_name], 
                0, 
                det.[production_name], 
                det.[zone_name], 
                det.[price_type_name], 
                det.[create_dt], 
                det.[due_amount], 
                0.00, 
                1
        FROM [dbo].[LV_ORDER_DETAIL] AS det
             INNER JOIN [#overnight_orders] AS ord ON ord.[order_no] = det.[order_no];
                          
    /* Get Traveling Programs Payment Data */

        INSERT INTO [#overnight_data] ([data_type], [order_no], [sli_no], [customer_no], [performance_date], [title_name], [scholarship_no], [production_name], 
                                       [zone_name], [price_type_name], [create_dt], [due_amount], [total_paid], [visitor_counter])
        SELECT 'pay_info', 
               pay.[order_no], 
               0, 
               pay.[customer_no], 
               '', 
               'payment', 
               pay.[scholarship_no], 
               pay.[scholarship_name], 
               '', 
               pay.[payment_method], 
               pay.[payment_dt], 
               0.00, 
               SUM(ISNULL(pay.[payment_amount],0.0)),
               0
        FROM [dbo].[LV_SCHOLARSHIP_PAYMENTS] AS pay
             INNER JOIN [#overnight_orders] AS ord ON ord.[order_no] = pay.[order_no] 
        WHERE (pay.[scholarship_no] = @scholarship_id OR @scholarship_id = '-1')
        GROUP BY pay.[order_no], pay.[customer_no], pay.[scholarship_no], pay.[scholarship_name], pay.[payment_method], pay.[payment_dt]
        HAVING SUM(ISNULL([payment_amount],0.0)) <> 0.00;

    /*  Update Interdepartmental Transfer payments  */

        UPDATE [#overnight_data] 
        SET [production_name] = 'Interdepartmental Transfer' 
        WHERE [data_type] = 'pay_info' 
          AND [production_name] = '' 
          AND price_type_name = 'Interdepartmental Transfer';

    /*  Update Unknown Scholarship Names  */

        UPDATE [#overnight_data] 
        SET [production_name] = 'Unknown Scholarship' 
        WHERE [data_type] = 'pay_info' 
          AND  [production_name] = '';

--    /*  This field needs to have a date value in it even if it's not going to be used - Put today's date into any blank fields  */

        UPDATE [#overnight_data] 
        SET [performance_date] = CONVERT(CHAR(10),[create_dt],111) 
        WHERE [performance_date] = '';

    /*  Create final data table  */

        INSERT INTO [#overnight_final] ([rpt_message], [data_type], [order_no], [sli_no], [customer_no], [order_performance_date], [performance_date], [title_name], [scholarship_no],
                                        [production_name], [zone_name], [performance_code], [price_type_name], [create_dt], [due_amount], [total_paid], [customer_first_name], 
                                        [customer_middle_name], [customer_last_name], [customer_type_no], [customer_type], [address_type_no], [address_type], [address_street1],
                                        [address_street2], [address_city], [address_state], [address_zip_code], [address_city_state], [order_custom_1], [order_custom_2], [order_custom_3],
                                        [order_custom_4], [order_custom_5], [order_custom_6], [order_custom_7], [order_custom_8], [order_custom_9], [order_custom_10], [order_notes],
                                        [visitor_counter], [order_scholarship], [sort_field], [sort_text], [program_counter])
        SELECT '', 
               dat.[data_type], 
               dat.[order_no], 
               dat.[sli_no], 
               dat.[customer_no], 
               '', 
               dat.[performance_date], 
               dat.[title_name], 
               dat.[scholarship_no], 
               dat.[production_name], 
               dat.[zone_name], 
               '', 
               dat.[price_type_name], 
               dat.[create_dt], 
               dat.[due_amount], 
               dat.[total_paid], 
               ISNULL(cus.[fname], ''), 
               ISNULL(cus.[mname], ''), 
               ISNULL(cus.[lname], ''), 
               ISNULL(cus.[cust_type], 0) AS [cust_type], 
               ISNULL(ctp.[description],''), 
               ISNULL(adr.[address_type],0), 
               ISNULL(atp.[description],''), 
               ISNULL(adr.[street1], ''), 
               ISNULL(adr.[Street2],''), 
               ISNULL(adr.[city],''), 
               ISNULL(adr.[state],''), 
               ISNULL(adr.[postal_code],''), 
               '', 
               ISNULL(ord.[custom_1],''), 
               ISNULL(ord.[custom_2],''), 
               ISNULL(ord.[custom_3],''), 
               ISNULL(ord.[custom_4],''), 
               ISNULL(ord.[custom_5],''), 
               ISNULL(ord.[custom_6],''), 
               ISNULL(ord.[custom_7],''), 
               ISNULL(ord.[custom_8],''), 
               ISNULL(ord.[custom_9],''), 
               ISNULL(ord.[custom_0],''), 
               ISNULL(ord.[notes],''), 
               dat.[visitor_counter],
               '', 
               '', 
               '', 
               0
        FROM [#overnight_data] AS dat
             LEFT OUTER JOIN [dbo].[T_CUSTOMER] AS cus ON cus.[customer_no] = dat.[customer_no]
             LEFT OUTER JOIN [dbo].[TR_CUST_TYPE] AS ctp ON ctp.[id] = cus.[cust_type]
             LEFT OUTER JOIN [dbo].[T_ADDRESS] AS adr ON adr.[customer_no] = dat.[customer_no] AND adr.[inactive] = 'N' AND adr.primary_ind = 'Y'
             LEFT OUTER JOIN [dbo].[TR_ADDRESS_TYPE] AS atp ON atp.[id] = adr.[address_type]
             LEFT OUTER JOIN [dbo].[T_ORDER] AS ord ON ord.[order_no] = dat.[order_no]

    /* Count the number of programs */

        UPDATE [#overnight_final] 
        SET [program_counter] = 1 
        WHERE [data_type] = 'ord_info';

    /*  Add the dash in between the first five and the last four of zip plus four values  */
        
        UPDATE [#overnight_final] 
        SET [address_zip_code] = LEFT([address_zip_code],5) + '-' + RIGHT([address_zip_code],4) 
        WHERE LEN([address_zip_code]) = 9;

    /*  Combine city and state into one field for sorting purposes  */

        UPDATE [#overnight_final] 
        SET [address_city_state] = [address_city] + ', ' + [address_state];
            
    /*  Combine School and School Official Record into a single customer type  */

        UPDATE [#overnight_final] 
        SET [customer_type_no] = @cust_type_id_school, [customer_type] = 'School' 
        WHERE [customer_type_no] = @cust_type_id_school_official;

    /*  Remove Interdepartmental Transfers */

        DELETE FROM [#overnight_final] 
        WHERE [order_no] IN (SELECT [order_no] 
                             FROM [#overnight_final] 
                             WHERE [title_name] = 'payment' 
                               AND [production_name] = 'Interdepartmental Transfer');

    /*  Remove unwanted constiruent types and schlarships based on parameters  */

        IF @customer_type_id > 0
            DELETE FROM [#overnight_final] 
            WHERE [order_no] NOT IN (SELECT [order_no] 
                                     FROM [#overnight_final] 
                                     WHERE [customer_type_no] = @customer_type_id);


    /*  Set Order Performance Date or Order Scholarship - Only if that's needed for the sort  */
                      
        WITH CTE_PERF_DATE ([order_no], [perf_date])
        AS (SELECT [order_no], MAX(ISNULL([performance_date],''))
            FROM [#overnight_final] 
            WHERE [data_type] = 'ord_info'
            GROUP BY [order_no]
           )
        UPDATE sch
        SET sch.[order_performance_date] = dat.[perf_date]         
        FROM [#overnight_final] AS sch
             INNER JOIN [CTE_PERF_DATE] AS dat ON dat.[Order_no] = sch.[order_no];
           
           
        WITH CTE_PRODUCTIONS ([order_no], [prod_name])
        AS (SELECT [order_no], MAX([production_name]) 
            FROM [#overnight_final] WHERE [data_type] = 'pay_info' 
            GROUP BY [order_no]
           )
        UPDATE sch
        SET sch.[order_scholarship] = pro.[prod_name]
        FROM [#overnight_final] AS sch
             INNER JOIN [CTE_PRODUCTIONS] AS pro ON pro.[order_no] = sch.[order_no];

    /* Remove zero total payments */

        WITH CTE_ZERO_PAYMENTS ([order_no], [prod_name], [total_paid])
        AS (SELECT [order_no], 
                   [production_name],
                   SUM(ISNULL([total_paid],0.0)) 
            FROM [#overnight_final] 
            WHERE [data_type] = 'pay_info' 
            GROUP BY [order_no], [production_name]
            HAVING SUM(ISNULL([total_paid],0.0)) = 0.00
           )
           DELETE sch
           FROM [#overnight_final] AS sch
                INNER JOIN [CTE_ZERO_PAYMENTS] AS pay ON pay.[order_no] = sch.[order_no] AND pay.[prod_name] = sch.[production_name];
          
    /*  Set Sort Field  */

        IF @sort_by = 'Performance Date'
            UPDATE [#overnight_final] 
            SET [sort_field] = [order_performance_date] + ' ' + [customer_last_name] + ' ' + [customer_first_name] + ' ' + [customer_middle_name], 
                [sort_text] = [order_performance_date];
        ELSE IF @sort_by = 'Scholarship'
            UPDATE [#overnight_final] 
            SET [sort_field] = [order_scholarship] + ' ' + [customer_last_name] + ' ' + [customer_first_name] + ' ' + [customer_middle_name], 
                [sort_text] = [order_scholarship];
        ELSE IF @sort_by = 'Customer Type'
            UPDATE [#overnight_final] 
            SET [sort_field] = [customer_type] + ' ' + [customer_last_name] + ' ' + [customer_first_name] + ' ' + [customer_middle_name], 
                [sort_text] = [customer_type];
        ELSE IF @sort_by = 'Customer Name'
            UPDATE [#overnight_final] 
            SET [sort_field] = [customer_last_name] + ' ' + [customer_first_name] + ' ' + [customer_middle_name], 
                [sort_text] = [customer_last_name] + ' ' + [customer_first_name] + ' ' + [customer_middle_name];
        ELSE    --Default sort is Order Number 
            UPDATE [#overnight_final] 
            SET [sort_field] = CONVERT(VARCHAR(100),[order_no]), 
            [sort_text] = CONVERT(VARCHAR(100),[order_no]);
         
    FINISHED:

    
        /*  Select the final record set from #final_table  */

            WITH CTE_VISITOR_COUNT ([order_no], [visitor_count])
            AS (SELECT [order_no], 
                       SUM([visitor_counter]) 
                FROM [#overnight_final]
                WHERE [data_type] = 'ord_info' 
                GROUP BY [order_no]
               )
            SELECT fin.[data_type], 
                   fin.[order_no], 
                   ISNULL(vis.[visitor_count],0) AS [visitor_count],
                   fin.[sli_no], 
                   fin.[customer_no], 
                   fin.[order_performance_date], 
                   fin.[performance_date], 
                   fin.[title_name], 
                   fin.[scholarship_no], 
                   fin.[production_name], 
                   fin.[zone_name], 
                   fin.[performance_code], 
                   fin.[price_type_name], 
                   fin.[program_counter], 
                   fin.[create_dt], 
                   fin.[due_amount], 
                   fin.[total_paid], 
                   fin.[customer_first_name], 
                   fin.[customer_middle_name], 
                   fin.[customer_last_name], 
                   fin.[customer_type_no], 
                   fin.[customer_type], 
                   fin.[address_type_no], 
                   fin.[address_type], 
                   fin.[address_street1], 
                   fin.[address_street2], 
                   fin.[address_city], 
                   fin.[address_state], 
                   fin.[address_zip_code], 
                   fin.[address_city_state], 
                   fin.[order_custom_1], 
                   fin.[order_custom_2], 
                   fin.[order_custom_3], 
                   fin.[order_custom_4], 
                   fin.[order_custom_5], 
                   fin.[order_custom_6], 
                   fin.[order_custom_7], 
                   fin.[order_custom_8], 
                   fin.[order_custom_9], 
                   fin.[order_custom_10], 
                   fin.[order_notes], 
                   fin.[sort_field], 
                   fin.[sort_text], 
                   fin.[order_scholarship], 
                   fin.[visitor_counter],
                   fin.[rpt_message]
            FROM [#overnight_final] AS fin
                 LEFT OUTER JOIN [CTE_VISITOR_COUNT] AS vis ON vis.[order_no] = fin.[order_no]
            WHERE [data_type] = 'pay_info'
              AND (@list_no <= 0 OR [customer_no] IN (SELECT [customer_no] FROM dbo.T_LIST_CONTENTS WHERE list_no = @list_no))
            ORDER BY [sort_field], [order_no], [data_type];

        /*  Clean up and Destroy Temporary Tables  */

            IF OBJECT_ID('tempdb..#overnight_final') IS NOT NULL DROP TABLE [#overnight_final];
            IF OBJECT_ID('tempdb..#overnight_data') IS NOT NULL DROP TABLE [#overnight_data];
            IF OBJECT_ID('tempdb..#overnight_orders') IS NOT NULL DROP TABLE [#overnight_orders];

        DONE:

END

GO

GRANT EXECUTE ON [dbo].[LRP_SCHOLARSHIP_OVERNIGHT] TO [ImpUsers] AS [dbo]
GO


EXECUTE [dbo].[LRP_SCHOLARSHIP_OVERNIGHT] @report_start_dt = '10-1-2018', @report_end_dt = '6-30-2019', @customer_type_id = 0, @scholarship_id = '-1', @sort_by = NULL, @list_no = 0


