USE [impresario]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LRP_BOARD_CommitteeHistory_NoAttendance]
(
	@customer_no INT
)
AS

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

-- Pulls all committees (Active or Former) that a Board member belongs/belonged to.

SELECT com.customer_no, associated_constituent_display_name, relationship_type_description, 
	note, start_dt, end_dt, primary_ind, title
FROM [VS_RELATIONSHIP] com
WHERE relationship_category_description = 'Committee' AND customer_no = @customer_no  
	