USE impresario
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_ADV_CAMPAIGN_ACTIVITY_STAFF_PIPELINE]') AND type IN (N'P', N'PC'))
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_ADV_CAMPAIGN_ACTIVITY_STAFF_PIPELINE] AS' 
GO

EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LRP_ADV_CAMPAIGN_ACTIVITY_STAFF_PIPELINE] TO [impusers], [tessitura_app]'
GO

/*      LRP_ADV_CAMPAIGN_ACTIVITY_STAFF_PIPELINE
        Pulls information for goals, numbers, and YTD percentages for all step activity, vists, ask/yield amounts, and actual contributions taken in.

        NOTE: Contribution amounts are based on what's in The Plan record under cont_amount

*/
ALTER PROCEDURE [dbo].[LRP_ADV_CAMPAIGN_ACTIVITY_STAFF_PIPELINE]
        @fiscal_year INT = NULL,
        @workers_str VARCHAR(4000) = NULL
WITH RECOMPILE AS BEGIN

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

    /* Procedure Variables  */

        --Determine current fiscal year, fiscal start date, fiscal end date, and current date
        DECLARE @fiscal_start_dt DATETIME = NULL, @fiscal_end_dt DATETIME = NULL
        DECLARE @current_dt DATETIME = GETDATE()

        DECLARE @worker_list TABLE ([worker_no] INT NOT NULL DEFAULT (0))

    /*  Temporary Tables  */
                                    
        IF OBJECT_ID('tempdb..#step_raw_data') IS NOT NULL DROP TABLE #step_raw_data

        CREATE TABLE #step_raw_data  ([step_no] INT NOT NULL DEFAULT (0), 
                                      [plan_no] INT NOT NULL DEFAULT (0), 
                                      [step_dt] DATETIME NULL, 
                                      [completed_on_dt] DATETIME NULL,
                                      [month_num] INT NOT NULL DEFAULT (0), 
                                      [month_abbrev] VARCHAR(30) NOT NULL DEFAULT (''),
                                      [month_sort] VARCHAR(7) NOT NULL DEFAULT (''),
                                      [month_year] VARCHAR(4) NOT NULL DEFAULT (''),
                                      [fiscal_year] INT NOT NULL DEFAULT (0),
                                      [is_current_fiscal] INT NOT NULL DEFAULT (0),
                                      [step_type] INT NOT NULL DEFAULT (0), 
                                      [step_type_name] VARCHAR(30) NOT NULL DEFAULT (''), 
                                      [step_description] VARCHAR(30) NOT NULL DEFAULT(''), 
                                      [associate_no] INT NOT NULL DEFAULT (0),
                                      [priority] INT NOT NULL DEFAULT (0), 
                                      [worker_customer_no] INT NOT NULL DEFAULT (0),
                                      [worker_name] VARCHAR(75) NOT NULL DEFAULT (''),
                                      [worker_initials] VARCHAR(10) NOT NULL DEFAULT (''),
                                      [worker_inactive] CHAR(1) NOT NULL DEFAULT ('N'),
                                      [is_visit] CHAR(1) NOT NULL DEFAULT ('N'),
                                      [is_qualification] CHAR(1) NOT NULL DEFAULT ('N')) 

        IF OBJECT_ID('tempdb..#plan_data') IS NOT NULL DROP TABLE #plan_data

        CREATE TABLE #plan_data ([plan_no] INT NOT NULL DEFAULT (0),
                                 [worker_customer_no] INT NOT NULL DEFAULT (0),
                                 [worker_name] VARCHAR(75) NOT NULL DEFAULT (0),
                                 [worker_initials] VARCHAR(75) NOT NULL DEFAULT (0),
                                 [worker_inactive] CHAR(1) NOT NULL DEFAULT ('N'),
                                 [start_dt] DATETIME NULL,
                                 [complete_by_dt] DATETIME NULL,
                                 [ask_amount] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                 [goal_amount] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                 [plan_amount] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                 [contribution_amount] DECIMAL (18,2) NOT NULL DEFAULT (0.0),
                                 [plan_status_id] INT NOT NULL DEFAULT (0),
                                 [plan_status] VARCHAR(50) NOT NULL DEFAULT (''))
    
        IF OBJECT_ID('tempdb..#worker_activity') IS NOT NULL DROP TABLE #worker_activity

        CREATE TABLE #worker_activity ([worker_customer_no] INT NOT NULL DEFAULT (0),
                                       [worker_name] VARCHAR(80) NOT NULL DEFAULT (''),
                                       [worker_initials] VARCHAR(75) NOT NULL DEFAULT (''),
                                       [worker_inactive] CHAR(1) NOT NULL DEFAULT ('N'),
                                       [goal_visits] DECIMAL(18,4) NOT NULL DEFAULT (0.0),
                                       [actual_visits] DECIMAL(18,4) NOT NULL DEFAULT (0.0),
                                       [goal_qualifications] DECIMAL(18,4) NOT NULL DEFAULT (0.0),
                                       [actual_qualifications] DECIMAL(18,4) NOT NULL DEFAULT (0.0),
                                       [goal_solicitations] DECIMAL(18,4) NOT NULL DEFAULT (0.0),
                                       [actual_solicitations] DECIMAL(18,4) NOT NULL DEFAULT (0.0),
                                       [actual_second_sol_amount] DECIMAL (18,4) NOT NULL DEFAULT (0.0),
                                       [goal_prim_sol_amount] DECIMAL(18,4) NOT NULL DEFAULT (0.0),
                                       [actual_prim_sol_amount] DECIMAL(18,4) NOT NULL DEFAULT (0.0))

        IF OBJECT_ID('tempdb..#final_table') IS NOT NULL DROP TABLE #final_table

        CREATE TABLE #final_table ([worker_customer_no] INT NOT NULL DEFAULT (0),
                                   [worker_name] VARCHAR(80) NOT NULL DEFAULT (''),
                                   [worker_initials] VARCHAR(75) NOT NULL DEFAULT (''),
                                   [inactive] CHAR(1) NOT NULL DEFAULT ('N'),
                                   [goal_visits] DECIMAL(18,4) NOT NULL DEFAULT (0.0),
                                   [actual_visits] DECIMAL(18,4) NOT NULL DEFAULT (0.0),
                                   [percent_visits] DECIMAL(18,4) NOT NULL DEFAULT (0.0),
                                   [goal_qualifications] DECIMAL(18,4) NOT NULL DEFAULT (0.0),
                                   [actual_qualifications] DECIMAL(18,4) NOT NULL DEFAULT (0.0),
                                   [percent_qualifications] DECIMAL(18,4) NOT NULL DEFAULT (0.0),
                                   [goal_solicitations] DECIMAL(18,4) NOT NULL DEFAULT (0.0),
                                   [actual_solicitations] DECIMAL(18,4) NOT NULL DEFAULT (0.0),
                                   [percent_solicitations] DECIMAL(18,4) NOT NULL DEFAULT (0.0),
                                   [goal_prim_sol_amount] DECIMAL(18,4) NOT NULL DEFAULT (0.0),
                                   [actual_prim_sol_amount] DECIMAL(18,4) NOT NULL DEFAULT (0.0),
                                   [percent_prim_sol_amount] DECIMAL (18,4) NOT NULL DEFAULT (0.0),
                                   [actual_second_sol_amount] DECIMAL (18,4) NOT NULL DEFAULT (0.0))
    /*  Check Parameters  */

        IF @fiscal_year IS NULL SELECT @fiscal_year = dbo.LF_GetFiscalYear(GETDATE())
        
        SELECT @fiscal_start_dt = [dbo].[LF_GetFiscalYearStartDt](@fiscal_year)

        SELECT @fiscal_end_dt = [dbo].[LF_GetFiscalYearEndDt](@fiscal_year)
        
        IF @current_dt > @fiscal_end_dt SELECT @current_dt = @fiscal_end_dt

        IF ISNULL(@workers_str,'') = ''
            INSERT INTO @worker_list ([worker_no])
            SELECT ISNULL([worker_customer_no], 0)
            FROM [dbo].[LV_CAMPAIGN_ACTIVITY_WORKERS] 
            WHERE [inactive] = 'N'
              AND ISNULL([worker_customer_no], 0) > 0
        ELSE
            INSERT INTO @worker_list ([worker_no])
            SELECT ISNULL(TRY_CAST([Element] AS INTEGER),0) 
            FROM [dbo].[FT_SPLIT_LIST](REPLACE(@workers_str,'"',''),',')
            WHERE ISNULL(TRY_CAST([Element] AS INTEGER),0) > 0

    /*  Pulls a list of all visits that were completed in the past twelve months  */
    
        INSERT INTO [#step_raw_data] ([step_no], [plan_no], [step_dt], [completed_on_dt], [month_num], [month_abbrev], [month_sort], [month_year], 
                                      [fiscal_year], [is_current_fiscal], [step_type], [step_type_name], [step_description], [associate_no], 
                                      [priority], [worker_customer_no], [worker_name], [worker_initials], [worker_inactive], [is_visit], [is_qualification])
        SELECT stp.[step_no],
               stp.[plan_no], 
               stp.[step_dt], 
               stp.[completed_on_dt],
               DATEPART(MONTH,stp.[completed_on_dt]),
               LEFT(DATENAME(MONTH,stp.[completed_on_dt]),3),
               LEFT(CONVERT(VARCHAR(10),stp.[completed_on_dt],111),7),
               DATENAME(YEAR,stp.[completed_on_dt]),
               CASE WHEN  stp.[completed_on_dt] < @fiscal_start_dt THEN (@fiscal_year - 1) ELSE @fiscal_year END,
               CASE WHEN stp.[completed_on_dt] < @fiscal_start_dt THEN 0 ELSE 1 END,
               stp.[step_type], 
               typ.[description], 
               stp.[description], 
               ISNULL(stp.[associate_no],0),
               stp.[priority], 
               stp.[worker_customer_no],
               wrk.[worker_name],
               wrk.[worker_initials],
               wrk.[inactive],
               CASE WHEN typ.[id] IN (29, 70, 76) THEN 'Y' ELSE 'N' END,
               CASE WHEN typ.[id] IN (70, 78) THEN 'Y' ELSE 'N' END
        FROM [dbo].[T_STEP] AS stp
             INNER JOIN @worker_list AS lis ON lis.[worker_no] = stp.[worker_customer_no]
             INNER JOIN [dbo].[LV_CAMPAIGN_ACTIVITY_WORKERS] AS wrk ON wrk.[worker_customer_no] = stp.[worker_customer_no] AND wrk.[inactive] = 'N'
             INNER JOIN [dbo].[TR_STEP_TYPE] AS typ ON typ.[id] = stp.[step_type]
        WHERE stp.[completed_on_dt] BETWEEN @fiscal_start_dt AND @current_dt

    /*  Pull the plan data to be used  */

        INSERT INTO [#plan_data] ([plan_no], [worker_customer_no], [worker_name], [worker_initials], [worker_inactive], [start_dt],
                                  [complete_by_dt], [ask_amount], [goal_amount], [contribution_amount], [plan_status_id], [plan_status])
        SELECT pln.[plan_no],
               xpp.[customer_no],
               wrk.[worker_name],
               wrk.[worker_initials],
               wrk.[inactive],
               pln.[start_dt],
               pln.[complete_by_dt],
               ISNULL(pln.[ask_amt],0.0),
               ISNULL(pln.[goal_amt],0.0),
               ISNULL(pln.[cont_amt],0.0),
               pln.[status],
               sta.[description]
        FROM [dbo].[T_PLAN] AS pln
             LEFT OUTER JOIN [dbo].[TX_CUST_PLAN] AS xpp ON xpp.[plan_no] = pln.[plan_no] AND xpp.[role_no] = 1
             INNER JOIN @worker_list AS lis ON lis.[worker_no] = xpp.[customer_no]
             INNER JOIN [dbo].[LV_CAMPAIGN_ACTIVITY_WORKERS] AS wrk ON wrk.[worker_customer_no] = xpp.[customer_no]
             LEFT OUTER JOIN [dbo].[TR_PLAN_STATUS] AS sta ON sta.[id] = pln.[status]
        WHERE pln.[start_dt] BETWEEN @fiscal_start_dt AND @fiscal_end_dt
          AND pln.[status] IN (23, 26, 35, 38)
          AND ISNULL(pln.[ask_amt], 0) > 0;

    /*  Seed the #Worker_activity table with all active workers and their goals  */

        INSERT INTO [#worker_activity] ([worker_customer_no], [worker_name], [worker_initials], [worker_inactive], [goal_visits], 
                                        [goal_qualifications], [goal_solicitations], [goal_prim_sol_amount])
        SELECT   wrk.[worker_customer_no],
                 wrk.[worker_name],
                 wrk.[worker_initials],
                 wrk.[inactive],
                 wrk.[goal_visits],
                 wrk.[goal_qualifications],
                 wrk.[goal_solicitations],
                 wrk.[goal_prim_sol_rev_total]
          FROM [dbo].[LV_CAMPAIGN_ACTIVITY_WORKERS] AS wrk
               INNER JOIN @worker_list AS lst ON lst.[worker_no] = wrk.[worker_customer_no]
          
    /*  ***Visits***  */
            
        INSERT INTO [#worker_activity] ([worker_customer_no], [worker_name], [worker_initials], [worker_inactive], [actual_visits])
        SELECT stp.[worker_customer_no],
               stp.[worker_name],
               stp.[worker_initials],
               stp.[worker_inactive],
               COUNT(stp.[step_no])
        FROM [#step_raw_data] AS stp 
        WHERE stp.[completed_on_dt] BETWEEN @fiscal_start_dt AND @current_dt 
          AND stp.[is_visit] = 'Y'
          AND stp.[worker_inactive] = 'N'
        GROUP BY stp.[worker_customer_no], stp.[worker_name], stp.[worker_initials], stp.[worker_inactive];

     /*  ***Qualifications***  */

        INSERT INTO [#worker_activity] ([worker_customer_no], [worker_name], [worker_initials], [worker_inactive], [actual_qualifications])
        SELECT stp.[worker_customer_no],
               stp.[worker_name],
               stp.[worker_initials],
               stp.[worker_inactive],
               COUNT(stp.[step_no])
        FROM [#step_raw_data] AS stp
        WHERE stp.[completed_on_dt] BETWEEN @fiscal_start_dt AND @current_dt 
          AND stp.[is_qualification] = 'Y'
          AND stp.[worker_inactive] = 'N'
        GROUP BY stp.[worker_customer_no], stp.[worker_name], stp.[worker_initials], stp.[worker_inactive];

    /*  ***Solicitations***  */

        INSERT INTO [#worker_activity] ([worker_customer_no], [worker_name], [worker_initials], [worker_inactive], [actual_solicitations])
        SELECT pln.[worker_customer_no],
               pln.[worker_name],
               pln.[worker_initials],
               pln.[worker_inactive],
               COUNT(pln.[plan_no])
        FROM [#plan_data] AS pln
        WHERE pln.[start_dt] BETWEEN @fiscal_start_dt AND @current_dt 
          AND pln.[worker_inactive] = 'N'
        GROUP BY pln.[worker_customer_no], pln.[worker_name], pln.[worker_initials], pln.[worker_inactive];

    /*  ***Primary Solicitor Amount*** */

        INSERT INTO [#worker_activity] ([worker_customer_no], [worker_name], [worker_initials], [worker_inactive], [actual_prim_sol_amount])
        SELECT con.[solicitor_number],
               wrk.[worker_name],
               wrk.[worker_initials],
               wrk.[inactive],
               SUM(ISNULL(con.[contribution_amount],0))
        FROM [dbo].[LV_CAMPAIGN_ACTIVITY_CONTRIBUTIONS] AS con
             INNER JOIN @worker_list AS lis ON lis.[worker_no] = con.[solicitor_number]
             INNER JOIN [dbo].[LV_CAMPAIGN_ACTIVITY_WORKERS] AS wrk ON wrk.[worker_customer_no] = con.[solicitor_number]
        WHERE con.[contribution_date] BETWEEN @fiscal_start_dt AND @fiscal_end_dt 
          AND ISNULL(con.[solicitor_number],0) > 0
          AND wrk.[inactive] = 'N'
        GROUP BY con.[solicitor_number], wrk.[worker_name], wrk.[worker_initials], wrk.[inactive]

    /*  Secondary Solicitor Amount  */

        INSERT INTO [#worker_activity]  ([worker_customer_no], [worker_name], [worker_initials], [worker_inactive], [actual_second_sol_amount])
        SELECT wrk.[worker_customer_no], 
               wrk.[worker_name],
               wrk.[worker_initials],
               wrk.[inactive],
               SUM(cont.[cont_amt])
        FROM [dbo].[T_CONTRIBUTION] cont 
             INNER JOIN [dbo].[T_CAMPAIGN] AS cam ON cam.[campaign_no] = cont.[campaign_no]
             INNER JOIN [dbo].[TR_CAMPAIGN_CATEGORY] AS cat ON cam.[category] = cat.[id]
             INNER JOIN [dbo].[t_plan] AS p ON p.[plan_no] = cont.[plan_no]
             INNER JOIN [dbo].[tx_cust_plan] AS cp ON cp.[plan_no] = p.[plan_no]
             INNER JOIN [dbo].[TR_WORKER_ROLE] rl ON rl.[id] = cp.[role_no]
             LEFT OUTER JOIN [dbo].[LV_CAMPAIGN_ACTIVITY_WORKERS] AS wrk ON wrk.[worker_customer_no] = cp.[customer_no]
             INNER JOIN @worker_list AS lis ON lis.[worker_no] = wrk.[worker_customer_no]
        WHERE cont.[cont_dt] BETWEEN @fiscal_start_dt AND @current_dt
          AND rl.[description] = 'Secondary Solicitor'
        GROUP BY wrk.[worker_customer_no], wrk.[worker_name], wrk.[worker_initials], wrk.[inactive]

    /*  Finalize the Data  */

        INSERT INTO [#final_table] ([worker_customer_no], [worker_name], [worker_initials], [inactive], [goal_visits], [actual_visits],
                                    [percent_visits], [goal_qualifications], [actual_qualifications], [percent_qualifications], [goal_solicitations], 
                                    [actual_solicitations], [percent_solicitations], [goal_prim_sol_amount], [actual_prim_sol_amount], 
                                    [percent_prim_sol_amount], [actual_second_sol_amount])
        SELECT  [worker_customer_no],
                [worker_name],
                [worker_initials],
                [worker_inactive],
                SUM([goal_visits]),
                SUM([actual_visits]),
                CASE WHEN SUM([goal_visits]) = 0.0 THEN 0.0 ELSE (SUM([actual_visits]) / SUM([goal_visits])) END,
                SUM([goal_qualifications]),
                SUM([actual_qualifications]),
                CASE WHEN SUM([goal_qualifications]) = 0.0 THEN 0.0 ELSE (SUM([actual_qualifications]) / SUM([goal_qualifications])) END,
                SUM([goal_solicitations]),
                SUM([actual_solicitations]),
                CASE WHEN SUM([goal_solicitations]) = 0.0 THEN 0.0 ELSE (SUM([actual_solicitations]) / SUM([goal_solicitations])) END,
                SUM([goal_prim_sol_amount]),
                SUM([actual_prim_sol_amount]),
                CASE WHEN SUM([goal_prim_sol_amount]) = 0.0 THEN 0.0 ELSE (SUM([actual_prim_sol_amount]) / SUM([goal_prim_sol_amount])) END,
                SUM([actual_second_sol_amount])
            FROM [#worker_activity]  --WHERE worker_initials = 'AGO'
            GROUP BY [worker_customer_no], [worker_name], [worker_initials], [worker_inactive]
      
        DELETE FROM [#final_table]
        WHERE ([goal_visits] + [actual_visits] + [goal_qualifications] + [actual_qualifications] + [goal_solicitations] 
             + [actual_solicitations] + [goal_prim_sol_amount] + [actual_prim_sol_amount] + [actual_second_sol_amount]) = 0
      
      
    FINISHED:       

        /*  Aggregate everything into a final list, one row per worker  */

            SELECT  [worker_customer_no],
                    [worker_name],
                    [worker_initials],
                    [inactive],
                    [goal_visits],
                    [actual_visits],
                    [percent_visits],
                    [goal_qualifications],
                    [actual_qualifications],
                    [percent_qualifications],
                    [goal_solicitations],
                    [actual_solicitations],
                    [percent_solicitations],
                    [goal_prim_sol_amount],
                    [actual_prim_sol_amount],
                    [percent_prim_sol_amount],
                    [actual_second_sol_amount]
            FROM [#final_table]  --WHERE worker_initials = 'AGO'
            ORDER BY [worker_initials]
                
    DONE:

        IF OBJECT_ID('tempdb..#final_table') IS NOT NULL DROP TABLE [#final_table]
        IF OBJECT_ID('tempdb..#worker_activity') IS NOT NULL DROP TABLE #worker_activity
        IF OBJECT_ID('tempdb..#plan_data') IS NOT NULL DROP TABLE #plan_data
        IF OBJECT_ID('tempdb..#step_raw_data') IS NOT NULL DROP TABLE #step_raw_data

END
GO

EXECUTE [dbo].[LRP_ADV_CAMPAIGN_ACTIVITY_STAFF_PIPELINE] @fiscal_year = 2021, @workers_str = NULL
--EXECUTE [dbo].[LRP_ADV_CAMPAIGN_ACTIVITY_STAFF_PIPELINE] @fiscal_year = 2021, @workers_str = '3504561,3657886,672463,817730'

/*
--FOR PARAMETER
SELECT [worker_customer_no],                --DATA
       [worker_name]                        --DISPLAY
FROM  [dbo].[LV_CAMPAIGN_ACTIVITY_WORKERS]  --TABLE
WHERE [inactive] = 'N'                      --WHERE CLAUSE
ORDER BY [worker_sort]                      --SORT
--"3504561","3657886","672463","817730"
*/


