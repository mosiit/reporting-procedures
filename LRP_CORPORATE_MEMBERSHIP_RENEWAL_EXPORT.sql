
USE [impresario]
GO

SET QUOTED_IDENTIFIER ON

SET ANSI_NULLS ON
GO

GO

ALTER PROCEDURE [dbo].[LRP_CORPORATE_MEMBERSHIP_RENEWAL_EXPORT]
        @customer_no INT, 
    	@expr_dt DATETIME, -- should be last day of month
        @expr_end_dt DATETIME
AS BEGIN

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
    SET NOCOUNT ON 

        -- GET COMPANY INFORMATION
        DECLARE @company TABLE ([id] INT IDENTITY, 
	                            [customer_no] INT,
	                            [companyName] VARCHAR(55),
	                            [memberLevel] VARCHAR(30),
	                            [expr_dt] DATETIME,
	                            [membershipNotes] VARCHAR(1024),
	                            [admission_adult] INT,
	                            [street1] VARCHAR(64),
	                            [street2] VARCHAR(64),
	                            [city] VARCHAR(30),
	                            [STATE] VARCHAR(2),
	                            [postal_code] VARCHAR(10),
	                            [phone] VARCHAR(12))

        INSERT INTO @company ([customer_no],[companyName],[memberLevel],[expr_dt],[membershipNotes],[admission_adult],[street1],[street2],[city],[STATE],[postal_code],[phone])
        EXECUTE [dbo].[LRP_CORPORATE_MEMBERSHIP_RENEWAL_LETTER] @expr_dt = @expr_dt, @expr_end_dt = @expr_end_dt, @customer_no = @customer_no

        -- 
        -- GET CONTACTS INFORMATION
        DECLARE @contactTbl TABLE  ([customer_no] INT, 
	                                [contactType] VARCHAR(20) NOT NULL,
	                                [individual_customer_no] INT NULL,
	                                [esal1_desc] VARCHAR(55),
	                                [esal2_desc] VARCHAR(55), 
	                                [display_name] VARCHAR(100) NULL,
	                                [JobTitle] VARCHAR(100) NULL,
	                                [Employer] VARCHAR(100) NULL,
	                                [BAddress1] VARCHAR(100) NULL,
	                                [BAddress2] VARCHAR(100) NULL,
	                                [BAddress3] VARCHAR(100) NULL,
	                                [BCity] VARCHAR(100) NULL,
	                                [BStateCd] VARCHAR(2) NULL,
	                                [BZipCode] VARCHAR(10) NULL,
	                                [BPhone] VARCHAR(20) NULL,
	                                [BFax] VARCHAR(20) NULL,
	                                [BEmail] VARCHAR(100) NULL,
	                                [BNotes] VARCHAR(500) NULL)

        DECLARE @i INT = 1,
	            @maxRow INT, 
	            @current_customer_no INT
		
        SELECT @maxRow = MAX(id) FROM @company

        WHILE @i <= @maxRow BEGIN

	        SELECT @current_customer_no = customer_no FROM @company WHERE id = @i

	        INSERT INTO @contactTbl ([customer_no],[contactType],[individual_customer_no],[esal1_desc],[esal2_desc],[display_name],[JobTitle],[Employer],
                                     [BAddress1],[BAddress2],[BAddress3],[BCity],[BStateCd],[BZipCode],[BPhone],[BFax],[BEmail],[BNotes])
	        EXEC [dbo].[LRP_CORPORATE_MEMBERSHIP_CONTACTS] @group_customer_no = @current_customer_no

	        SET @i = @i + 1 

        END 

    -- GET ENTITLEMENTS INFORMATION (added January 2020)

    DECLARE @entitlementsTbl TABLE ([customer_no] INT,
                                    [total_entitlements] INT,
	                                [total_entitlements_used] INT,
	                                [total_entitlements_unused] INT,
	                                [4-D Theater] INT,
	                                [Butterfly Garden] INT,
	                                [Exhibit Halls] INT,
	                                [Omni] INT,
	                                [Planetarium] INT,
	                                [entitlements_remaining] VARCHAR(MAX) NULL)
		
    INSERT INTO @entitlementsTbl 
    EXEC [dbo].[LRP_CORPORATE_MEMBERSHIP_ENTITLEMENTS] @customer_no, @expr_dt

    --If the Primary and Working contacts are the same person, they only want it to appear once. 
    DELETE con
    FROM @contactTbl con
         INNER JOIN (SELECT [customer_no],
                            [individual_customer_no], 
                            COUNT(*) AS [cnt]
	                 FROM @contactTbl
	                 GROUP BY [customer_no], [individual_customer_no]
	                 HAVING COUNT(*) > 1) X ON X.[customer_no] = con.[customer_no] AND con.[contactType] = 'Working Contact'

    -- DSJ 6/2017 - I commented out some fields because they were not needed but they are available in the dataset if ADV changes their mind. 
    SELECT  com.[customer_no],
	        com.[companyName],
	        com.[memberLevel],
	        CAST(com.[expr_dt] AS DATE) AS [expr_dt],
	        com.[membershipNotes],
	        lev.[start_amt] AS [amount],
	        [total_entitlements] = COALESCE(et.[total_entitlements], 0),
	        [total_entitlements_used] = COALESCE(et.[total_entitlements_used], 0),
	        [total_entitlements_unused] = COALESCE(et.[total_entitlements_unused], 0),
	        et.[4-D Theater],  
	        et.[Butterfly Garden], 
	        et.[Exhibit Halls],  
	        et.[Omni],  
	        et.[Planetarium], 
	        [entitlements_remaining] = COALESCE(et.[entitlements_remaining], 'N/A'),
	        --com.[admission_adult],
	        --com.[street1],
	        --com.[street2],
	        --com.[city],
	        --com.[state],
	        --com.[postal_code],
	        com.[phone],
	        con.[contactType],
	        --con.[individual_customer_no],
	        con.[esal1_desc],
	        con.[esal2_desc],
	        con.[display_name],
	        con.[JobTitle],
	        --con.[Employer],
	        con.[BAddress1],
	        con.[BAddress2],
	        con.[BAddress3],
	        con.[BCity],
	        con.[BStateCd],
	        con.[BZipCode],
	        con.[BPhone],
	        --con.[BFax],
	        con.[BEmail]
        FROM @company com
	         INNER JOIN @contactTbl con ON con.[customer_no] = com.[customer_no]
	         INNER JOIN [dbo].[T_MEMB_LEVEL] lev ON com.[memberLevel] = lev.[description] -- joining to this table to get the amount value for the corp membership
	         LEFT OUTER JOIN @entitlementsTbl et ON et.[customer_no] = com.[customer_no]
        WHERE lev.[memb_org_no] = 7 

    DONE:

END
GO

    EXECUTE [dbo].[LRP_CORPORATE_MEMBERSHIP_RENEWAL_EXPORT] @expr_dt = '5-1-2021', @expr_end_dt = '6-30-2021', @customer_no = 0
