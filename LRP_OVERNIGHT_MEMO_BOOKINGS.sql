USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_OVERNIGHT_MEMO_BOOKINGS]') AND type IN (N'P', N'PC'))
BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_OVERNIGHT_MEMO_BOOKINGS] AS' 
    EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LRP_OVERNIGHT_MEMO_BOOKINGS] TO impusers'
END
GO

/*      
    LRP_OVERNIGHT_MEMO_BOOKINGS
   


*/
ALTER PROCEDURE [dbo].[LRP_OVERNIGHT_MEMO_BOOKINGS]
    @report_dt DATETIME = NULL
WITH RECOMPILE AS BEGIN

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;
    SET ANSI_WARNINGS OFF;

    /* Procedure Variables  */

        DECLARE @next_overnight_dt DATETIME = NULL;

        DECLARE @assignments TABLE ([overnight_dt] DATETIME NULL, 
                                    [overnigt_category] VARCHAR(30) NOT NULL DEFAULT (''),
                                    [customer_no] INT NOT NULL DEFAULT (0),
                                    [group_name] VARCHAR(100) NOT NULL DEFAULT (''),
                                    [contact_name] VARCHAR(100) NOT NULL DEFAULT (''),
                                    [contact_first_name] VARCHAR(100) NOT NULL DEFAULT (''),
                                    [group_city] VARCHAR(50) NOT NULL DEFAULT(''),
                                    [group_state] VARCHAR(50) NOT NULL DEFAULT(''),
                                    [departure_time] VARCHAR(50) NOT NULL DEFAULT (''),
                                    [resource_name] VARCHAR(50) NULL DEFAULT (''),
                                    [scheduled_count] INT NULL DEFAULT (0));
        
        DECLARE @final_table TABLE  ([overnight_dt] DATETIME NULL,
                                     [overnigt_category] VARCHAR(50) NOT NULL DEFAULT (''),
                                     [customer_no] INT NOT NULL DEFAULT (0),
                                     [group_name] VARCHAR(100) NOT NULL DEFAULT (''),
                                     [contact_name] VARCHAR(100) NOT NULL DEFAULT (''),
                                     [contact_first_name] VARCHAR(100) NOT NULL DEFAULT (''),
                                     [group_city] VARCHAR(50) NULL DEFAULT(''),
                                     [group_state] VARCHAR(50) NULL DEFAULT(''),
                                     [departure_time] VARCHAR(50) NOT NULL DEFAULT (''),
                                     [eating_at_cafe] INT NULL DEFAULT (0),
                                     [eating_tbd] INT NULL DEFAULT (0),
                                     [eating_elsewhere] INT NULL DEFAULT (0),
                                     [eating_na] INT NULL DEFAULT (0),
                                     [adults] INT NULL DEFAULT (0),
                                     [children] INT NULL DEFAULT (0),
                                     [bus_drop_off] INT NULL DEFAULT (0),
                                     [bus_stay] INT NULL DEFAULT (0),
                                     [public_trans] INT NULL DEFAULT (0));

    /*  Check Parameters  */
            
        SELECT @next_overnight_dt = MIN([performance_dt]) 
                                    FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] WHERE [performance_dt] >= GETDATE() AND [title_no] = 61;     --61 = Overnight Programs

        IF @report_dt IS NULL AND @next_overnight_dt IS NOT NULL SELECT @report_dt = @next_overnight_dt;
        IF @report_dt IS NULL GOTO FINISHED;

        SELECT @report_dt = CONVERT(DATE,@report_dt);    --REMOVES TIME (if any)
    
    /*  Get all bookings and assignments for the overnight date  */
        
        WITH [bus_departures] ([booking_no], [customer_no], [assigned_resource], [resource_schedule]) AS
                (SELECT [booking_no], [customer_no], [assigned_resource], MAX(CONVERT(CHAR(8),start_dt,108)) --MAX([resource_schedule])
                 FROM [dbo].[LV_OVERNIGHT_RESOURCE_ASSIGNMENTS] 
                 WHERE [overnight_dt] = @report_dt AND [resource_id] = 1
                 GROUP BY [booking_no], [customer_no], [assigned_resource])
        INSERT INTO @assignments ([overnight_dt], [overnigt_category], [customer_no], [group_name], [contact_name], [contact_first_name], 
                                 [group_city], [group_state], [departure_time], [resource_name], [scheduled_count])
        SELECT bkg.[overnight_dt], 
               bkg.[overnight_category], 
               bkg.[customer_no], 
               ISNULL(bkg.[group_name], ''),
               ISNULL(bkg.[contact_name], ''),
               ISNULL(bkg.[contact_first_name], ''),
               ISNULL(bkg.[group_city], ''),
               ISNULL(bkg.[group_state], ''),
               ISNULL(dpt.[resource_schedule],'00:00:00') AS [departure_time],
               rsr.[description], 
               SUM(ISNULL(sch.[count],0)) AS [scheduled_count]
        FROM [dbo].[LV_OVERNIGHT_BOOKINGS] AS bkg 
             LEFT OUTER JOIN [dbo].[T_BOOKING_ASSIGNMENT] AS asn ON asn.[booking_id] = bkg.[booking_no]
             INNER JOIN [dbo].[T_RESOURCE] AS rsr ON rsr.[id] = asn.[resource_id]
             LEFT OUTER JOIN [dbo].[T_RESOURCE_SCHEDULE] AS sch ON sch.[booking_assignment_id] = asn.[id] AND sch.[type] = -3   -- -3 = Booking
             LEFT OUTER JOIN [bus_departures] AS dpt ON dpt.booking_no = bkg.[booking_no]
        WHERE bkg.[overnight_dt] = @report_dt AND bkg.[customer_no] <> 3741534
        GROUP BY bkg.[booking_no], bkg.[overnight_dt], bkg.[overnight_category], bkg.[customer_no], bkg.[group_name], bkg.[contact_name], 
                 bkg.[contact_first_name], bkg.[group_city], bkg.[group_state], dpt.[resource_schedule],bkg.[departure_time], rsr.[description];

    /*  Convert individual records into one record per booking  */

        INSERT INTO @final_table ([overnight_dt], [overnigt_category], [customer_no], [group_name], [contact_name], [contact_first_name], [group_city], [group_state],
                                  [departure_time], [eating_at_cafe], [eating_tbd], [eating_elsewhere], [eating_na], [adults], [children], [bus_drop_off], [bus_stay], [public_trans])
        SELECT * FROM @assignments
        PIVOT (SUM([scheduled_count]) FOR [resource_name] IN ([At Cafe],
                                                              [TBD],
                                                              [Elsewhere], 
                                                              [N/A],
                                                              [Number of Adults], 
                                                              [Number of Youth], 
                                                              [Buses Drop-Off], 
                                                              [Buses Staying], 
                                                              [Train/Public Bus])) AS [scheduled_count];

    FINISHED:

        /*  Create final data set to pass back to the report  */

            SELECT [overnight_dt], 
                   [overnigt_category], 
                   [customer_no], 
                   [group_name], 
                   [contact_name],
                   [contact_first_name],
                   [group_city],
                   [group_state],
                   [departure_time],
                   ISNULL([eating_at_cafe],0) AS [eating_at_cafe], 
                   ISNULL([eating_tbd],0) AS [eating_tbd],
                   ISNULL([eating_elsewhere],0) AS [eating_elsewhere], 
                   ISNULL([eating_na],0) AS [eating_na], 
                   ISNULL([adults],0) AS [adults], 
                   ISNULL([children],0) AS [children],
                   ISNULL([bus_drop_off],0) AS [bus_drop_off], 
                   ISNULL([bus_stay],0) AS [bus_stay], 
                   ISNULL([public_trans],0) AS [public_trans],
                   CASE WHEN [eating_at_cafe] > 0 THEN 'At Cafe' 
                        WHEN [eating_elsewhere] > 0 THEN 'Elsewhere'
                        WHEN [eating_tbd] > 0 THEN 'TBD'
                        ELSE 'N/A' END AS [dining_plans]
            FROM @final_table;

    DONE:

END
GO


    --EXECUTE [dbo].[LRP_OVERNIGHT_MEMO_BOOKINGS] @report_dt = '11-16-2018'



   
    