USE [impresario]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[LP_SEATING_BOOK_RPT](
	@season_no INT = 0,
	@perf_no INT,
	@section_str VARCHAR(4000) = NULL,
	@gen_public INT = 1, -- 1 (yes) or 2 (no)
	@list_no INT = NULL,
	@list_acts_upon INT = 1)
AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
Set NoCount On
/*************************************************************************************************************
-- DSJ 9/29/2016
-- This was modified from RP_SEATING_BOOK_RPT. I added email address to the end of the report. 

New Procedure 9/22/2000 by CWR based on SQL by S. Dorey for Seating Book Report
Shows only seats that have been sold

Modified 11/15/2005 - LB Changed the sortname to handle null values in the lname and fname
Modified CWR 1/21/2006 -- added owner name to table references
Modified RCreps 4/6/2007 -- removed unnecessary join to TR_SEAT_STATUS in result set.
Modified RCreps 5/2/2008 FP541. -- added list_no parameter and expanded section_str to varchar(4000)
			-- some updates for optimization
			-- Added price type category column and single_or_package column that separates status and single/package
			-- added show_price_type_category parameter to front end (not passed to procedure)
Modified PWoods 22 June 2011 #4289 -- Now using FT_CONSTITUENT_DISPLAY_NAME to get names
Modified RWC 6/2/2013 -- Added columns for initiator and recipient, now reading uncommitted instead of using NOLOCK
Modified CWR 6/24/2013 #2553 -- added @list_acts_upon parameter
Modified RWC 4/6/2015 #8650 -- control grouping output, instead of depending 
*************************************************************************************************************/

Declare @tsections Table (id int null, description varchar(30) null)

If @season_no not in (Select id from VRS_SEASON)
  Begin
	RAISERROR('Invalid season.', 11, 2) WITH SETERROR
	RETURN
  END

If @list_no > 0 and not exists (Select * From [dbo].VS_LIST Where list_no = @list_no)
  Begin
	RAISERROR('Invalid List (list_no).', 11, 2) WITH SETERROR
	RETURN
  END

IF @section_str IS NULL OR ISNULL(DATALENGTH(LTRIM(@section_str)),0) = 0
	Insert	into @tsections (id, description)
	Select	id, description
	From	[dbo].tr_section
ELSE
	INSERT 	INTO @tsections (id, description)
	SELECT 	id, description
	FROM 	[dbo].tr_section
	WHERE 	CHARINDEX(',' + CONVERT(VARCHAR,id) + ',' , ',' + @section_str + ',') > 0
 
SELECT	s.seat_no, 
	o.customer_no,
	name = CASE WHEN o.customer_no = 0 THEN '(General Public)' ELSE d.display_name END,
	sort_name = CASE WHEN o.customer_no = 0 THEN '(General Public)' ELSE d.sort_name END,
	sli.order_no,
	section = sc.description, 
	s.seat_row,
	s.seat_num,
	sublineitem_status = slis.description,
	package_or_single = CASE WHEN sli.pkg_no > 0 THEN 'Package' ELSE 'Single' END,
	pricetypecategory = ptc.description,
	initiatorid = o.initiator_no,
	initiatordisplayname = i.display_name,
	initiatorsortname = i.sort_name,
	recipientid = sli.recipient_no,
	recipientdisplayname = r.display_name,
	recipientsortname = r.sort_name,
	e.address AS email,
	[dbo].[LFS_BOARD_GetProspectManager](o.customer_no) AS prospectManager
FROM	[dbo].TX_PERF_SEAT ps
	JOIN [dbo].T_SUB_LINEITEM sli ON ps.sli_no = sli.sli_no
	JOIN [dbo].T_ORDER o ON o.order_no = sli.order_no
	JOIN [dbo].T_SEAT s ON s.seat_no = sli.seat_no
	JOIN @tsections sc ON sc.id = s.section
	JOIN [dbo].TR_SLI_STATUS slis ON slis.id = sli.sli_status
	JOIN [dbo].TR_PRICE_TYPE pt ON pt.id = sli.price_type
	JOIN [dbo].TR_PRICE_TYPE_CATEGORY ptc ON pt.price_type_category = ptc.id
	LEFT JOIN dbo.FT_CONSTITUENT_DISPLAY_NAME() d ON o.customer_no = d.customer_no
	LEFT JOIN dbo.FT_CONSTITUENT_DISPLAY_NAME() i ON o.initiator_no = i.customer_no
	LEFT JOIN dbo.FT_CONSTITUENT_DISPLAY_NAME() r ON sli.recipient_no = r.customer_no
	LEFT JOIN dbo.FT_GET_PRIMARY_EADDRESS() e ON o.customer_no = e.customer_no
WHERE	sli.perf_no = @perf_no AND ps.perf_no = @perf_no 
	AND ISNULL(ps.sli_no,0) > 0
	AND o.customer_no >= CASE WHEN @gen_public = 1 THEN 0 ELSE 1 END
	AND (
		COALESCE(@list_no, 0) = 0 
		OR (@list_acts_upon = 1 AND EXISTS (SELECT * FROM [dbo].T_LIST_CONTENTS x
					WHERE x.customer_no = o.customer_no
					AND x.list_no = @list_no))
		OR (@list_acts_upon = 2 AND EXISTS (SELECT * FROM [dbo].T_LIST_CONTENTS x
					WHERE x.customer_no = COALESCE(o.initiator_no, o.customer_no)
					AND x.list_no = @list_no))
		OR (@list_acts_upon = 4 AND EXISTS (SELECT * FROM [dbo].T_LIST_CONTENTS x
					WHERE x.customer_no = COALESCE(sli.recipient_no, o.customer_no)
					AND x.list_no = @list_no))
	)
RETURN

GO


