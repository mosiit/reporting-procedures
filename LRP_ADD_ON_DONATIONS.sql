SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
-- =============================================
-- Author:		Aileen Duffy-Brown
-- Create date: 9/25/2016
-- Description:	add on donations based on Mode of Sale and Channel
-- =============================================
CREATE PROCEDURE [dbo].[LRP_ADD_ON_DONATIONS]
    @order_start_dt DATETIME = NULL,
    @order_end_dt DATETIME = NULL,
    @mos_str VARCHAR(MAX) = NULL,
    @appeal_name VARCHAR(MAX) = NULL,
    @mode_of_sale_name VARCHAR(MAX) = NULL,
    @memb_staus VARCHAR(MAX) = NULL,
    @customer VARCHAR(MAX) = NULL
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;

    -- Insert statements for procedure here
    SELECT od.order_no,
           od.customer_no,
           od.create_dt,
           od.appeal_no,
           ap.description AS Appeal_Name,
           co.fund_no,
           me.memb_level,
           me.current_status,
           st.description AS Memb_Status,
           mo.description AS Mode_of_Sale,
           od.MOS,
           sa.description AS Sales_Channel,
           od.channel,
           od.solicitor,
           od.tot_contribution_paid_amt,
           od.tot_ticket_purch_amt,
           od.tot_paid_amt,
           od.batch_no,
           cu.display_name AS Customer
    FROM T_ORDER AS od (NOLOCK)
        INNER JOIN T_CONTRIBUTION AS co (NOLOCK)
            ON od.customer_no = co.customer_no
               AND od.batch_no = co.batch_no
               AND od.channel = co.channel
        --    ON od.order_no = su.order_no
        LEFT OUTER JOIN [dbo].[FT_CONSTITUENT_DISPLAY_NAME]() AS cu
            ON od.customer_no = cu.customer_no
        LEFT OUTER JOIN V_MEMBERSHIP_CURRENT AS me (NOLOCK)
            ON od.customer_no = me.customer_no
        LEFT OUTER JOIN TR_MOS AS mo (NOLOCK)
            ON od.MOS = mo.id
        LEFT OUTER JOIN TR_SALES_CHANNEL AS sa (NOLOCK)
            ON od.channel = sa.id
        LEFT OUTER JOIN TR_CURRENT_STATUS AS st (NOLOCK)
            ON me.current_status = st.id
        LEFT OUTER JOIN dbo.T_APPEAL AS ap (NOLOCK)
            ON od.appeal_no = ap.appeal_no
    WHERE co.fund_no = 2303
          AND od.tot_contribution_amt > 0
          AND od.tot_paid_amt > 0;

END;
GO
