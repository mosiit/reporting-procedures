USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LRP_NIGHTLY_SUMM_GRND_TOT_PAY]    Script Date: 12/14/2020 2:26:09 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



ALTER PROCEDURE [dbo].[LRP_NIGHTLY_SUMM_GRND_TOT_PAY]
	@section VARCHAR(300),
	@sortOrder INT,
	@runBy	VARCHAR(300),
	@fy VARCHAR(4),
	@fystart DATETIME,
	@fyend DATETIME
AS

BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE @tblPassOne	TABLE	(
								customer_no	INT,
								value			MONEY
								)

	DECLARE @tblPassTwo TABLE	(
								customer_no		INT,
								value			MONEY
								)

	DECLARE @tblPassBoth TABLE	(
								customer_no		INT,
								value			MONEY
								)

	INSERT INTO @tblPassOne
        SELECT  c.customer_no,
                ISNULL(SUM(n.trn_amt), 0)
        FROM    T_TRANSACTION AS n
        INNER JOIN T_CONTRIBUTION AS c ON n.ref_no = c.ref_no
        INNER JOIN T_PAYMENT AS p ON n.sequence_no = p.sequence_no
        INNER JOIN LTR_FUND_DETAIL AS f ON c.fund_no = f.fund_no
		--2020/12/14, H. Sheridan.  Service Now ticket SCTASK0001710 - join to campaign table
		JOIN	T_CAMPAIGN cam
		ON		cam.campaign_no = c.campaign_no
        WHERE   c.custom_1 IN ('(none)', 'Matching Gift Credit')
                AND c.customer_no > 0
                AND c.cont_amt > 0
                AND n.trn_type IN ('1', '3', '4', '6')
                AND CAST(p.pmt_dt AS DATE) >= @fystart
                AND CAST(p.pmt_dt AS DATE) <= @fyend
  				--2020/12/14, H. Sheridan.  Service Now ticket SCTASK0001710 - add criteria for campaign category
				AND	cam.category NOT IN (8,9)
				--2020/12/14, H. Sheridan.  Service Now ticket SCTASK0001710 - remove criteria for acct_grp_no
                --AND f.acct_grp_no <> '3'
        GROUP BY c.customer_no

	--SELECT 'debug 1', * FROM @tblNightSumm

	INSERT INTO @tblPassTwo
        SELECT  i.customer_no,
                ISNULL(SUM(n.trn_amt), 0)
        FROM    T_TRANSACTION AS n
        INNER JOIN T_CONTRIBUTION AS c ON n.ref_no = c.ref_no
        INNER JOIN T_PAYMENT AS p ON n.sequence_no = p.sequence_no
        INNER JOIN T_CREDITEE AS t ON c.ref_no = t.ref_no
        INNER JOIN LTR_FUND_DETAIL AS f ON c.fund_no = f.fund_no
        INNER JOIN T_CUSTOMER AS i ON t.creditee_no = i.customer_no
		--2020/12/14, H. Sheridan.  Service Now ticket SCTASK0001710 - join to campaign table
		JOIN	T_CAMPAIGN cam
		ON		cam.campaign_no = c.campaign_no
        WHERE   c.custom_1 IN ('Primary Soft Credit', 'Matching Gift Credit', 'Stewardship Soft Credit')
                AND c.customer_no > 0
                AND c.cont_amt > 0
                AND t.creditee_type IN ('12', '15', '5', '10', '16', '1', '14')
                AND n.trn_type IN ('1', '3', '4', '6')
                AND CAST(p.pmt_dt AS DATE) >= @fystart
                AND CAST(p.pmt_dt AS DATE) <= @fyend
  				--2020/12/14, H. Sheridan.  Service Now ticket SCTASK0001710 - add criteria for campaign category
				AND	cam.category NOT IN (8,9)
				--2020/12/14, H. Sheridan.  Service Now ticket SCTASK0001710 - remove criteria for acct_grp_no
                --AND f.acct_grp_no <> '3'
        GROUP BY i.customer_no

	INSERT INTO @tblPassBoth
		SELECT * FROM @tblPassOne

	INSERT INTO @tblPassBoth
		SELECT * FROM @tblPassTwo

	--SELECT * FROM @tblPassBoth

	INSERT INTO LT_NIGHTLY_SUMMARY
		SELECT  customer_no,
				@section,
				SUM(ISNULL(value, 0)),
				'',
				@sortOrder,
				GETDATE(),
				@runBy
		FROM	@tblPassBoth
		GROUP BY customer_no

END

GO


