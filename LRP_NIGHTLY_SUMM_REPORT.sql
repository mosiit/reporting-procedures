USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LRP_NIGHTLY_SUMM_REPORT]    Script Date: 4/6/2016 10:15:36 AM ******/
DROP PROCEDURE [dbo].[LRP_NIGHTLY_SUMM_REPORT]
GO

/****** Object:  StoredProcedure [dbo].[LRP_NIGHTLY_SUMM_REPORT]    Script Date: 4/6/2016 10:15:36 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[LRP_NIGHTLY_SUMM_REPORT]
	@customer_no INT
AS

-- ===========================================================================
-- H. Sheridan, 4/4/2016 - Produce the data for the custom screen
-- ===========================================================================

BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	--IF NOT EXISTS (SELECT customer_no FROM LT_NIGHTLY_SUMMARY WHERE customer_no = @customer_no AND section = 'I3 Comprehensive Campaign Total')
	--	BEGIN
	--		INSERT INTO LT_NIGHTLY_SUMMARY
	--			SELECT	@customer_no, 'I3 Comprehensive Campaign Total', 0.00, '',  1, GetDate(), 'System'
	--	END
		
	--IF NOT EXISTS (SELECT customer_no FROM LT_NIGHTLY_SUMMARY WHERE customer_no = @customer_no AND section = 'Comprehensive Lifetime Total')
	--	BEGIN
	--		INSERT INTO LT_NIGHTLY_SUMMARY
	--			SELECT	@customer_no, 'Comprehensive Lifetime Total', 0.00, '',  2, GetDate(), 'System'
	--	END

	--IF NOT EXISTS (SELECT customer_no FROM LT_NIGHTLY_SUMMARY WHERE customer_no = @customer_no AND section = 'Lifetime Corp Membership Dues')
	--	BEGIN
	--		INSERT INTO LT_NIGHTLY_SUMMARY
	--			SELECT	@customer_no, 'Lifetime Corp Membership Dues', 0.00, '',  3, GetDate(), 'System'
	--	END

	--IF NOT EXISTS (SELECT customer_no FROM LT_NIGHTLY_SUMMARY WHERE customer_no = @customer_no AND section = 'Lifetime Innovator Dues')
	--	BEGIN
	--		INSERT INTO LT_NIGHTLY_SUMMARY
	--			SELECT	@customer_no, 'Lifetime Innovator Dues', 0.00, '',  4, GetDate(), 'System'
	--	END

	--IF NOT EXISTS (SELECT customer_no FROM LT_NIGHTLY_SUMMARY WHERE customer_no = @customer_no AND section = 'FY2016 Funds Raised')
	--	BEGIN
	--		INSERT INTO LT_NIGHTLY_SUMMARY
	--			SELECT	@customer_no, 'FY2016 Funds Raised', 0.00, '',  16, GetDate(), 'System'
	--	END

	--IF NOT EXISTS (SELECT customer_no FROM LT_NIGHTLY_SUMMARY WHERE customer_no = @customer_no AND section = 'FY2015 Funds Raised')
	--	BEGIN
	--		INSERT INTO LT_NIGHTLY_SUMMARY
	--			SELECT	@customer_no, 'FY2015 Funds Raised', 0.00, '',  17, GetDate(), 'System'
	--	END

	--IF NOT EXISTS (SELECT customer_no FROM LT_NIGHTLY_SUMMARY WHERE customer_no = @customer_no AND section = 'FY2014 Funds Raised')
	--	BEGIN
	--		INSERT INTO LT_NIGHTLY_SUMMARY
	--			SELECT	@customer_no, 'FY2014 Funds Raised', 0.00, '',  18, GetDate(), 'System'
	--	END

	--IF NOT EXISTS (SELECT customer_no FROM LT_NIGHTLY_SUMMARY WHERE customer_no = @customer_no AND section = 'FY2013 Funds Raised')
	--	BEGIN
	--		INSERT INTO LT_NIGHTLY_SUMMARY
	--			SELECT	@customer_no, 'FY2013 Funds Raised', 0.00, '',  19, GetDate(), 'System'
	--	END

	--IF NOT EXISTS (SELECT customer_no FROM LT_NIGHTLY_SUMMARY WHERE customer_no = @customer_no AND section = 'FY2016 Unrestricted Annual Fund Payments')
	--	BEGIN
	--		INSERT INTO LT_NIGHTLY_SUMMARY
	--			SELECT	@customer_no, 'FY2016 Unrestricted Annual Fund Payments', 0.00, '',  20, GetDate(), 'System'
	--	END

	--IF NOT EXISTS (SELECT customer_no FROM LT_NIGHTLY_SUMMARY WHERE customer_no = @customer_no AND section = 'FY2015 Unrestricted Annual Fund Payments')
	--	BEGIN
	--		INSERT INTO LT_NIGHTLY_SUMMARY
	--			SELECT	@customer_no, 'FY2015 Unrestricted Annual Fund Payments', 0.00, '',  21, GetDate(), 'System'
	--	END

	--IF NOT EXISTS (SELECT customer_no FROM LT_NIGHTLY_SUMMARY WHERE customer_no = @customer_no AND section = 'FY2014 Unrestricted Annual Fund Payments')
	--	BEGIN
	--		INSERT INTO LT_NIGHTLY_SUMMARY
	--			SELECT	@customer_no, 'FY2014 Unrestricted Annual Fund Payments', 0.00, '',  22, GetDate(), 'System'
	--	END

	--IF NOT EXISTS (SELECT customer_no FROM LT_NIGHTLY_SUMMARY WHERE customer_no = @customer_no AND section = 'FY2013 Unrestricted Annual Fund Payments')
	--	BEGIN
	--		INSERT INTO LT_NIGHTLY_SUMMARY
	--			SELECT	@customer_no, 'FY2013 Unrestricted Annual Fund Payments', 0.00, '',  23, GetDate(), 'System'
	--	END

	--IF NOT EXISTS (SELECT customer_no FROM LT_NIGHTLY_SUMMARY WHERE customer_no = @customer_no AND section = 'FY2016 Grand Total Payments')
	--	BEGIN
	--		INSERT INTO LT_NIGHTLY_SUMMARY
	--			SELECT	@customer_no, 'FY2016 Grand Total Payments', 0.00, '',  24, GetDate(), 'System'
	--	END

	--IF NOT EXISTS (SELECT customer_no FROM LT_NIGHTLY_SUMMARY WHERE customer_no = @customer_no AND section = 'FY2015 Grand Total Payments')
	--	BEGIN
	--		INSERT INTO LT_NIGHTLY_SUMMARY
	--			SELECT	@customer_no, 'FY2015 Grand Total Payments', 0.00, '',  25, GetDate(), 'System'
	--	END

	--IF NOT EXISTS (SELECT customer_no FROM LT_NIGHTLY_SUMMARY WHERE customer_no = @customer_no AND section = 'FY2014 Grand Total Payments')
	--	BEGIN
	--		INSERT INTO LT_NIGHTLY_SUMMARY
	--			SELECT	@customer_no, 'FY2014 Grand Total Payments', 0.00, '',  26, GetDate(), 'System'
	--	END

	--IF NOT EXISTS (SELECT customer_no FROM LT_NIGHTLY_SUMMARY WHERE customer_no = @customer_no AND section = 'FY2013 Grand Total Payments')
	--	BEGIN
	--		INSERT INTO LT_NIGHTLY_SUMMARY
	--			SELECT	@customer_no, 'FY2013 Grand Total Payments', 0.00, '',  27, GetDate(), 'System'
	--	END

	SELECT  customer_no AS 'Customer No',
			section AS 'Section',
			value AS 'Value',
			apply_date AS 'Date'
	FROM	LT_NIGHTLY_SUMMARY
	WHERE	customer_no = @customer_no
	ORDER BY sort_order

END
GO


