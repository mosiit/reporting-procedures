USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_MEMBERSHIP_SALES_STUBS_ORDERS]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_MEMBERSHIP_SALES_STUBS_ORDERS] AS' 
END
GO

ALTER PROCEDURE [dbo].[LRP_MEMBERSHIP_SALES_STUBS_ORDERS]
        @report_start_dt DATETIME = NULL,
        @report_end_dt DATETIME = NULL,
        @days_of_week_str VARCHAR(4000) = '',
        @include_upgrades CHAR(1) = 'Y',
        @secondary_display VARCHAR(30) = 'Time',
        @include_returned_memberships CHAR(1) = 'Y',     --If running for past dates, some memberships were possibly returned after the fact 
        @minimum_days INT = 0,                             --Answer Yes (Y) to this parameter to still include them since they were still stubs 
        @maximum_days INT = 0                              --transactions at the time of sale.
WITH RECOMPILE AS BEGIN                                
                                                       
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    SET NOCOUNT ON

    /*  Procedure Variables  */

        DECLARE @rpt_msg VARCHAR(100) = '';
        DECLARE @temp_min INT = 0, @temp_max INT = 0;

        DECLARE @membership_orders TABLE ([sli_no] INT, [order_no] INT, [create_dt] DATETIME, [created_by] varchar(10), [perf_no] INT, 
                                          [zone_no]INT, [title_name] VARCHAR(30), [production_name] VARCHAR(30), [performance_zone_text] VARCHAR(50), 
                                          [sli_status] INT, [due_amt] MONEY, [paid_amt] MONEY);

        DECLARE @returned_exh_orders TABLE ([order_no] INT, [create_dt] DATETIME, [created_by] varchar(10), [perf_dt] DATETIME, [perf_no] INT, 
                                            [title_name] VARCHAR(30), [production_name] VARCHAR(30), [price_type] SMALLINT, [price_type_name] VARCHAR(30),
                                            [exh_return_days] INT, [returned_tickets] INT, [returned_amount] money);

        DECLARE @final_table TABLE ([order_no] INT, [mem_sale_dt] DATETIME, [mem_sale_hour] VARCHAR(10), [mem_sale_hour_display] VARCHAR(25), [mem_sale_day] VARCHAR(25), 
                                    [mem_sold_by] VARCHAR(10), [exh_return_dt] DATETIME, [exh_returned_by] VARCHAR(10), [exh_perf_dt] DATETIME, [mem_perf_no] INT, 
                                    [mem_zone_no] INT, [mem_title_name] VARCHAR(30), [mem_prod_name] VARCHAR(30), [mem_level] VARCHAR(50), [mem_sli_status] INT, 
                                    [mem_due_amount] MONEY, [mem_paid_amount] MONEY, [exh_perf_no] INT, [exh_title_name] VARCHAR(30), [exh_prod_name] VARCHAR(30), 
                                    [exh_price_type] INT, [exh_price_type_name] VARCHAR(30), [exh_returned_tickets] INT, [exh_returned_amount] MONEY, 
                                    [exh_return_days] INT, [report_message] VARCHAR(100));

        DECLARE @days_of_week_list TABLE (day_of_week VARCHAR(25));

        DECLARE @operator_list TABLE (operator_id VARCHAR(25));


    /*  Check Parameters  */

        --Null dates default to yesterday

        SELECT @report_start_dt = ISNULL(@report_start_dt,DATEADD(DAY,-1,GETDATE())),
               @report_end_dt = ISNULL(@report_end_dt,DATEADD(DAY,-1,GETDATE()));

        --Make sure start date starts at midnight and end date ends at 11:59 PM

        SELECT @report_start_dt = CONVERT(DATE,@report_start_dt),
               @report_end_dt = CONVERT(CHAR(10),@report_end_dt,111) + ' 23:59:59.957';

        --Today and past dates only

        IF CONVERT(DATE,@report_start_dt) > CONVERT(DATE,GETDATE()) OR CONVERT(DATE,@report_end_dt) > CONVERT(DATE,GETDATE()) BEGIN

            SELECT @rpt_msg = 'Invalid Dates...  Today and past dates only.  No future dates.';

            GOTO FINISHED;

        END;

        --Min and Max are zero if null

        SELECT @minimum_days = ISNULL(@minimum_days,0),
               @maximum_days = ISNULL(@maximum_days,0)

        --If Min is greater than Max, reverse them

        IF @minimum_days > @maximum_days
            SELECT @temp_min = @maximum_days, 
                   @temp_max = @minimum_days,
                   @minimum_days = @temp_min, 
                   @maximum_days = @temp_max

        --Parse days of week (if any)

        IF ISNULL(@days_of_week_str,'') <> ''
            INSERT INTO @days_of_week_list ([day_of_week])
            SELECT LTRIM(RTRIM([Item])) FROM [dbo].[LFT_SPLIT_DILEMETED_STRING] (REPLACE(@days_of_week_str,'"',''),',');
        
        --Set default values if null

        SELECT @include_upgrades = ISNULL(@include_upgrades,'Y');
        SELECT @include_returned_memberships = ISNULL(@include_returned_memberships,'Y');

    /*  Get all membership sales that occurred during the specified date range  */

        INSERT INTO @membership_orders ([sli_no], [order_no], [create_dt], [created_by] , [perf_no], [zone_no], 
                                        [title_name], [production_name], [performance_zone_text], [sli_status], 
                                        [due_amt], [paid_amt])
        SELECT sli.[sli_no], 
               sli.[order_no], 
               sli.[create_dt],
               sli.[created_by], 
               sli.[perf_no], 
               sli.[zone_no], 
               prf.[title_name], 
               prf.[production_name], 
               prf.[performance_zone_text], 
               sli.[sli_status], 
               sli.[due_amt], 
               sli.[paid_amt]
        FROM [dbo].[T_SUB_LINEITEM] AS sli 
             LEFT OUTER JOIN [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] AS prf 
                          ON prf.[performance_no] = sli.[perf_no] AND prf.[performance_zone] = sli.[zone_no]
        WHERE sli.[create_dt] BETWEEN @report_start_dt AND @report_end_dt
          AND prf.[title_name] = 'Membership'
          --AND (PATINDEX('%Basic%',prf.performance_zone_text) > 1 OR PATINDEX('%Premier%',prf.performance_zone_text) > 1 or LEFT(prf.[performance_zone_text],5) IN ('Basic','Premie'))
          AND LEFT(prf.[performance_zone_text],5) IN ('Basic','Premi','Upgra')
          AND sli.sli_status IN (3, 7, 12);

        IF @include_upgrades = 'Y'

            WITH memb_upgrades  
            AS (SELECT msu.[order_no], ISNULL(mso.[performance_zone_text],'') AS 'orig_membership', REPLACE(msu.[performance_zone_text],'Upgrade ','') AS 'upgraded_membership'
                FROM @membership_orders msu
                     LEFT OUTER JOIN @membership_orders AS mso ON mso.[order_no] = msu.[order_no] AND mso.[performance_zone_text] NOT LIKE '%Upgrade%'
                WHERE msu.[performance_zone_text] LIKE '%Upgrade%' AND ISNULL(mso.[performance_zone_text],'') <> '')
            UPDATE @membership_orders
            SET performance_zone_text = (SELECT 'Up ' + memb_upgrades.orig_membership + ' to ' + memb_upgrades.upgraded_membership FROM memb_upgrades
                                         WHERE memb_upgrades.order_no = [@membership_orders].order_no)
            WHERE [@membership_orders].order_no IN (SELECT order_no FROM memb_upgrades)
              AND PATINDEX('Upgrade%',[performance_zone_text]) > 0; 

        ELSE

            DELETE FROM @membership_orders 
            WHERE [performance_zone_text] LIKE '%Upgrade%';


    /*  If include returned memberships = "No" Delete memberships with a status of 7 */

        IF @include_returned_memberships <> 'Y'
            DELETE FROM @membership_orders 
            WHERE [sli_status] = 7;

    /*  If specific days of the week were chosen, delete sales from other days  */

        IF EXISTS (SELECT * FROM @days_of_week_list)
            DELETE FROM @membership_orders 
            WHERE DATENAME(WEEKDAY,[create_dt]) NOT IN (SELECT [day_of_week] FROM @days_of_week_list);

    /*  Get the orders where there are returned exhibit hall tickets on the same order as the membership sale  */

        INSERT INTO @returned_exh_orders ([order_no], [create_dt], [created_by], [perf_dt], [perf_no], 
                                          [title_name], [production_name], [exh_return_days], [returned_tickets], [returned_amount])
        SELECT sub.order_no,
               CONVERT(DATE, sub.create_dt), 
               sub.[created_by],
               CONVERT(DATE, prf.[performance_dt]),
               sub.perf_no,
               prf.[title_name],
               prf.[production_name],
               DATEDIFF(DAY, prf.[performance_dt], sub.[create_dt]),
               COUNT(DISTINCT sub.sli_no),
               SUM(sub.[paid_amt])
        FROM dbo.T_SUB_LINEITEM AS sub
             LEFT OUTER JOIN dbo.LV_PRODUCTION_ELEMENTS_PERFORMANCE AS prf ON prf.[performance_no] = sub.[perf_no] AND prf.[performance_zone] = sub.[zone_no]
        WHERE sub.[order_no] IN (SELECT [order_no] FROM @membership_orders)
          AND prf.[title_name] = 'Exhibit Halls'
          AND CONVERT(DATE, sub.create_dt) >= CONVERT(DATE, prf.[performance_dt])
          AND sub.sli_status = 4
        GROUP BY sub.order_no,
                 CONVERT(DATE, sub.create_dt), 
                 sub.[created_by],
                 CONVERT(DATE, prf.[performance_dt]),
                 sub.perf_no,
                 prf.[title_name],
	             prf.[production_name],
                 DATEDIFF(DAY, prf.[performance_dt], sub.[create_dt]);

                 --SELECT * FROM @returned_exh_orders ORDER BY order_no
                 --GOTO DONE

    /*  Combine the two lists together into a final data table  */

        INSERT INTO @final_table ([order_no], [mem_sale_dt], [mem_sale_hour], [mem_sale_hour_display], [mem_sale_day], [mem_sold_by], 
                                  [exh_return_dt], [exh_returned_by], [exh_perf_dt], [mem_perf_no], [mem_zone_no], [mem_title_name], 
                                  [mem_prod_name], [mem_level], [mem_sli_status], [mem_due_amount], [mem_paid_amount], [exh_perf_no], 
                                  [exh_title_name], [exh_prod_name], [exh_returned_tickets], [exh_returned_amount], [exh_return_days], [report_message])
        SELECT mem.[order_no], 
               mem.[create_dt] AS 'mem_sale_dt', 
               LEFT(CONVERT(VARCHAR(8),mem.[create_dt],108),2) + ':00',
               '',
               DATENAME(WEEKDAY,mem.[create_dt]) AS 'mem_sale_dt', 
               mem.[created_by] AS 'mem_sold_by',
               exh.[create_dt] AS 'exh_return_dt',
               exh.[created_by] AS 'exh_returned_by', 
               exh.[perf_dt] AS 'exh_perf_dt',
               mem.[perf_no] AS 'mem_perf_no', 
               mem.[zone_no] AS 'mem_zone_no', 
               mem.[title_name] AS 'mem_title_name', 
               mem.[production_name] AS 'mem_prod_name', 
               mem.[performance_zone_text] AS 'mem_type', 
               mem.[sli_status] AS 'mem_sli_status', 
               mem.[due_amt] AS 'mem_due_amount', 
               mem.[paid_amt] AS 'mem_paid_amount',
               exh.[perf_no] AS 'exh_perf_no',
               exh.[title_name] AS 'exh_title_name',
               exh.[production_name] AS 'exh_prod_name',
               exh.[returned_tickets] AS 'exh_returned_tickets',
               exh.[returned_amount] AS 'exh_returned_amount',
               exh.[exh_return_days],
               ''
        FROM @membership_orders AS mem
             INNER JOIN @returned_exh_orders AS exh ON exh.[order_no] = mem.[order_no];

    /*  Set 12 hour display times for the 24 hour clock  */

        UPDATE @final_table
        SET [mem_sale_hour_display] = REPLACE(RIGHT(CONVERT(VARCHAR(50),CONVERT(DATETIME,[mem_sale_hour]),9),14),':00:000',' ')
        WHERE ISDATE([mem_sale_hour]) = 1;

    /*  Change all dollar amounts to positive numbers for display  */

        UPDATE @final_table
        SET [exh_returned_amount] = ([exh_returned_amount] * -1)
        WHERE [exh_returned_amount] < 0;

    /*  If the secondary display is set to operator, change times to operator Ids  */

        IF @secondary_display = 'Operator'
            UPDATE @final_table
            SET [mem_sale_hour] = [mem_sold_by],
                [mem_sale_hour_display] = [mem_sold_by]
    
    FINISHED:

        /*  If no records in the final table, add one record with a message  */

            IF NOT EXISTS(SELECT * FROM @final_table) BEGIN

                IF @rpt_msg = '' SELECT @rpt_msg = 'No data was found for the criteria you enterted.';
            
                INSERT INTO @final_table ([report_message])
                VALUES (@rpt_msg);

            END;

        /*  Create final data set to pass back to the report file  */

        SELECT [order_no], 
               [mem_sale_dt], 
               [mem_sale_hour],
               [mem_sale_hour_display],
               [mem_sale_day],
               [mem_sold_by], 
               [exh_return_dt], 
               [exh_returned_by], 
               [exh_perf_dt],
               1 AS 'mem_counter',
               [mem_perf_no], 
               [mem_zone_no], 
               [mem_title_name], 
               [mem_prod_name], 
               [mem_level], 
               [mem_sli_status],
               [mem_due_amount], 
               [mem_paid_amount], 
               [exh_perf_no], 
               [exh_title_name], 
               [exh_prod_name], 
               [exh_returned_tickets], 
               [exh_returned_amount],
               [exh_return_days],
               [report_message]
        FROM @final_table
        WHERE [exh_return_days] BETWEEN @minimum_days AND @maximum_days
        ORDER BY mem_sale_dt, order_no;

    DONE:

END
GO

GRANT EXECUTE ON [dbo].[LRP_MEMBERSHIP_SALES_STUBS_ORDERS] TO ImpUsers
GO



--EXECUTE [dbo].[LRP_MEMBERSHIP_SALES_STUBS_ORDERS] 
--        @report_start_dt = '5-1-2019', @report_end_dt = '5-31-2019', @days_of_week_str = '',
--        @include_upgrades = 'Y', @secondary_display = 'Time', @include_returned_memberships = 'N',
--        @minimum_days = 0, @maximum_days = 0
