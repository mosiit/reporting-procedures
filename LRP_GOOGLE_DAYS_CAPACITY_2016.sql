USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_GOOGLE_DAYS_CAPACITY]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LRP_GOOGLE_DAYS_CAPACITY]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LRP_GOOGLE_DAYS_CAPACITY]
AS BEGIN

    DECLARE @google_start_dt datetime, @google_end_dt datetime
    DECLARE @today_dt datetime, @report_start_dt datetime, @report_end_dt datetime
    DECLARE @perf_no int, @perf_time varchar(8), @zone_no int, @total_cap int, @hold_cap int, @soft_cap int, @sold_cap int, @avail_cap int

    SELECT @google_start_dt = '4-3-2017  00:00:00', @google_end_dt = '4-13-2017 23:59:59'
    SELECT @today_dt = getdate()

    IF OBJECT_ID('tempdb..#work_table') IS NOT NULL DROP TABLE [#work_table]

    CREATE TABLE #work_table ([title_name] varchar(30), [perf_no] int, [perf_code] varchar(10), [perf_dt] datetime, [perf_day] varchar(30), [perf_date] char(10), [perf_time] char(8), [prod_name] varchar(30),
                              [prod_name_long] varchar(150), [zone_map_no] int, [zone_map_name] varchar(30), [zone_no] int, [total_capacity] decimal(18,2), [held_seats] decimal(18,2), 
                              [soft_capacity] decimal(18,2), [tix_sold] decimal(18,2), [tix_avail] decimal(18,2))

    CREATE CLUSTERED INDEX [ix_work_table_title_name] ON #work_table ([title_name] ASC) ON [PRIMARY]

    CREATE NONCLUSTERED INDEX [ix_work_table_prod_name] ON #work_table ([prod_name] ASC) ON [PRIMARY]

    CREATE NONCLUSTERED INDEX [ix_work_table_prod_name_long] ON #work_table ([prod_name_long] ASC) ON [PRIMARY]

    CREATE NONCLUSTERED INDEX [ix_work_table_perf_no_perf_time] ON #work_table ([perf_no] ASC, [perf_time] ASC) ON [PRIMARY]
    
    CREATE NONCLUSTERED INDEX [ix_work_table_perf_day] ON #work_table ([perf_day] ASC) ON [PRIMARY]

    CREATE NONCLUSTERED INDEX [ix_work_table_zone_no] ON #work_table ([zone_no] ASC) ON [PRIMARY]

    IF OBJECT_ID('tempdb..#results_table') IS NOT NULL DROP TABLE [#results_table]

    CREATE TABLE #results_table ([title_name] varchar(30), [prod_name] varchar(200), [perf_date] varchar(10), [perf_time] varchar(8), [title_order] int, 
                                 [hard_capacity] decimal(18,2), [soft_capacity] decimal(18,2), [tix_sold] decimal(18,2), [percent_sold] decimal(18,2), [record_count] int)

    CREATE CLUSTERED INDEX [ix_results_table_title_name] ON #results_table ([title_name] ASC) ON [PRIMARY]

    CREATE NONCLUSTERED INDEX [ix_results_table_prod_name] ON #results_table ([prod_name] ASC) ON [PRIMARY]

    CREATE NONCLUSTERED INDEX [ix_results_table_perf_date] ON #results_table ([perf_date] ASC) ON [PRIMARY]

    CREATE NONCLUSTERED INDEX [ix_results_table_perf_time] ON #results_table ([perf_time] ASC) ON [PRIMARY]


    IF convert(char(10),@today_dt,111) > convert(char(10),@google_start_dt,111) SELECT @report_start_dt = convert(char(10),@today_dt,111) + ' 00:00:00'
    ELSE SELECT @report_start_dt = @google_start_dt

    IF convert(char(10),@today_dt,111) > convert(char(10),@google_end_dt,111) SELECT @report_end_dt = convert(char(10),@today_dt,111) + ' 23:59:59'
    ELSE SELECT @report_end_dt = @google_end_dt

    IF @report_start_dt > @google_end_dt and @report_end_dt > @google_end_dt GOTO DONE

    INSERT INTO #work_table ([title_name], [perf_no], [perf_code], [perf_dt], [perf_day], [perf_date], [perf_time], [prod_name], [prod_name_long], [zone_map_no], [zone_map_name], [zone_no])
    SELECT [title_name], [performance_no], [performance_code], [performance_dt], datename(weekday,[performance_dt]), [performance_date], 
           [performance_time], [production_name], [production_name_long], [performance_zone_map], [performance_zone_map_name], [performance_zone]
    FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] as prf
    WHERE prf.[performance_dt] between @report_start_dt and @report_end_dt

    DELETE FROM #work_table 
    WHERE [title_name] in ('Adult Offerings', 'CityPASS', 'Drop-In Activities', 'Exhibit Show And Go', 'Live Presentations', 'Membership', 
                           'Overnight Programs', 'Thrill Ride 360', 'Traveling Programs', 'Vouchers')

    DELETE FROM #work_table WHERE [perf_day] in ('Saturday','Sunday')

    DECLARE ResultsCursor INSENSITIVE CURSOR FOR
    SELECT [perf_no], [perf_time], [zone_no] FROM #work_table ORDER BY [title_name], [perf_no], [perf_time]
    OPEN ResultsCursor
    BEGIN_RESULTS_LOOP:

        FETCH NEXT FROM ResultsCursor INTO @perf_no, @perf_time, @zone_no
        IF @@FETCH_STATUS = -1 GOTO END_RESULTS_LOOP

        SELECT @total_cap = count([seat_no]) FROM [dbo].[TX_PERF_SEAT] (NOLOCK) WHERE [perf_no] = @perf_no and [zone_no] = @zone_no and [seat_status] not in (5,6,9)
        SELECT @hold_cap =  count([seat_no]) FROM [dbo].[TX_PERF_SEAT] (NOLOCK) WHERE [perf_no] = @perf_no and [zone_no] = @zone_no and [seat_status] in (1,4)
        SELECT @sold_cap = count([sli_no]) FROM [dbo].[T_SUB_LINEITEM] (NOLOCK) WHERE [perf_no] = @perf_no and [zone_no] = @zone_no and [sli_status] in (2, 3, 6, 12)

        SELECT @soft_cap = @total_cap - @hold_cap
         
        UPDATE #work_table 
        SET [total_capacity] = @total_cap, [held_seats] = @hold_cap, [soft_capacity] = @soft_cap, [tix_sold] = @sold_cap, [tix_avail] = @avail_cap
        WHERE [perf_no] = @perf_no and [perf_time] = @perf_time

        GOTO BEGIN_RESULTS_LOOP

    END_RESULTS_LOOP:
    CLOSE ResultsCursor
    DEALLOCATE ResultsCursor

    INSERT INTO #results_table
    SELECT [title_name], [prod_name], [perf_date], [perf_time], 0, sum([total_capacity]), sum([soft_capacity]), sum([tix_sold]),  
           CASE WHEN sum([soft_capacity]) = 0 THEN 0.00 ELSE (sum([tix_sold]) / sum([soft_capacity])) END, count([prod_name])
    FROM #work_table 
    GROUP BY [title_name], [prod_name], [perf_date], [perf_time]
    
    DELETE FROM #results_table WHERE prod_name like '%Buyout%'

    DELETE FROM #results_table WHERE perf_date = '2016/11/11'

    UPDATE [#results_table] SET [title_order] = 1 WHERE [title_name] = 'Exhibit Halls'
    UPDATE [#results_table] SET [title_order] = 2 WHERE [title_name] = 'Mugar Omni Theater'
    UPDATE [#results_table] SET [title_order] = 3 WHERE [title_name] = 'Hayden Planetarium'
    UPDATE [#results_table] SET [title_order] = 4 WHERE [title_name] = '4-D Theater'
    UPDATE [#results_table] SET [title_order] = 5 WHERE [title_name] = 'Butterfly Garden'
    UPDATE [#results_table] SET [title_order] = 6 WHERE [title_name] = 'School Lunch'
    UPDATE [#results_table] SET [title_order] = 7 WHERE [title_name] = 'Cahners Theater'
    UPDATE [#results_table] SET [title_order] = 8 WHERE [title_name] = 'Current Science & Technology'
    UPDATE [#results_table] SET [title_order] = 9 WHERE [title_order] = 0
    
    DONE:

        IF not exists (SELECT * FROM #results_table) INSERT INTO #results_table VALUES ('Nothing Found', '', '', '', 0, 0, 0, 0, 0, 0)
            
        SELECT [perf_date], [title_name],  [perf_time], [prod_name], [title_order], [soft_capacity] as 'total_capacity', [tix_sold], [percent_sold], [record_count]
        FROM #results_table ORDER BY [perf_date], [title_order], [title_name], [perf_time], [prod_name]

    CLEAN_UP:

        IF OBJECT_ID('tempdb..#results_table') IS NOT NULL DROP TABLE [#results_table]

        IF OBJECT_ID('tempdb..#work_table') IS NOT NULL DROP TABLE [#work_table]

END
GO

GRANT EXECUTE ON [dbo].[LRP_GOOGLE_DAYS_CAPACITY] TO impusers
GO


EXECUTE [dbo].[LRP_GOOGLE_DAYS_CAPACITY]
--GO
