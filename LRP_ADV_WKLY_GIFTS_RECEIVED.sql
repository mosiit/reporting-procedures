USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_ADV_WKLY_GIFTS_RECEIVED]') AND type IN (N'P', N'PC'))
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_ADV_WKLY_GIFTS_RECEIVED] AS'
GO

EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LRP_ADV_WKLY_GIFTS_RECEIVED] TO [impusers], [tessitura_app]'
GO

ALTER PROCEDURE [dbo].[LRP_ADV_WKLY_GIFTS_RECEIVED]
        @numOfDays INT = NULL,
        @min_cont_amt DECIMAL(18,2) = NULL,
        @workers_str VARCHAR(4000) = NULL
AS BEGIN

    SET NOCOUNT ON;
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

    /*  Procedure Variables  */

        DECLARE @start_dt DATETIME,
	            @end_dt DATETIME

        DECLARE @worker_list TABLE ([worker_no] INT NOT NULL DEFAULT (0))

        DECLARE @work1 TABLE ([customer_no] INT,
	                          [ref_no] INT,
	                          [cust_name] VARCHAR(160),
	                          [cont_dt] DATETIME,
	                          [cont_type] CHAR(1),
	                          [cont_amt] MONEY,
	                          [cont_fund] VARCHAR(30),
	                          [designation] VARCHAR(30) NULL,
	                          [plan_primary_worker] CHAR(8) NULL,
	                          [cont_worker] CHAR(8) NULL,
	                          [state] VARCHAR(20) NULL,
	                          [memb_level] CHAR(3) NULL,
	                          [memb_trend] VARCHAR(30) NULL,
	                          [nrr_status] CHAR(2) NULL,
                              [letter_desc] VARCHAR(30) NULL,
	                          [expr_dt] DATETIME NULL,
	                          [sort_name] VARCHAR(55),
	                          [initiator_no] INT NULL,
	                          [initiator_name] VARCHAR(160) NULL,
	                          [creditee_no] INT NULL,
	                          [creditee_name] VARCHAR(160) NULL,
	                          [credit_amt] MONEY,
	                          [credit_type_desc] VARCHAR(30))

    /*  Check Parameters  */

        -- Code is taken from LRP_GIFT_NOTIFICATION_OUTSIDE_DMS
        -- This calls AP_NEW_CONT_REPORT which needs a start and an end date.
        -- @end_dt is today and make the @start_dt @numOfDays before 

        SELECT @numOfDays = ISNULL(@numOfDays, 7)

        SET @numOfDays = -1 * @numOfDays

        SELECT @end_dt = DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0) + ' 23:59:59.99'
        SELECT @start_dt = DATEDIFF(dd, 0, DATEADD(d, @numOfDays, @end_dt))

        SELECT @min_cont_amt = ISNULL(@min_cont_amt, 1.00)

        IF @min_cont_amt < 1.00 SELECT @min_cont_amt = 1.00

        IF ISNULL(@workers_str,'') = ''
            INSERT INTO @worker_list ([worker_no])
            SELECT ISNULL([worker_customer_no], 0)
            FROM [dbo].[LV_CAMPAIGN_ACTIVITY_WORKERS] 
            WHERE [inactive] = 'N'
              AND ISNULL([worker_customer_no], 0) > 0
        ELSE
            INSERT INTO @worker_list ([worker_no])
            SELECT ISNULL(TRY_CAST([Element] AS INTEGER),0) 
            FROM [dbo].[FT_SPLIT_LIST](REPLACE(@workers_str,'"',''),',')
            WHERE ISNULL(TRY_CAST([Element] AS INTEGER),0) > 0

        IF ISNULL(@workers_str,'') = ''
            INSERT INTO @worker_list ([worker_no])
            VALUES (0)
            

    /*  Get Work Data  */
        INSERT INTO @work1
        EXEC [dbo].[AP_NEW_CONT_REPORT] @start_dt = @start_dt, 
                                        @end_dt = @end_dt, 
                                        @fund_str = NULL, 
                                        @type_str = NULL, 
                                        @list_no = 0, 
                                        @list_includes_owner_ind = 'N',
                                        @list_includes_initiator_ind = 'N',
                                        @list_includes_creditee_ind = 'N',
                                        @creditee_str = '1,5,8,11,12,14,15'

        UPDATE @work1
        SET [plan_primary_worker] = RIGHT(RTRIM([plan_primary_worker]),3)
        WHERE LEN(RTRIM([plan_primary_worker])) = 4

        UPDATE @work1
        SET [cont_worker] = RIGHT(RTRIM([cont_worker]),3)
        WHERE LEN(RTRIM([cont_worker])) = 4

    /*  If no specific workers are passed to the procedure, all gifts should be included even if the worker number is not in
        the LV_CAMPAIGN_ACTIVITY_WORKERS list.  Since the @worker_list table contains a list of all active workers (again assuming
        no specific list was provided) this statement inserts into the list any worker who has a contribution in the output set
        but who is not an active worker and not already in the list.  This prevents the inner join below from ommitting those contributions.
        If a specific list of workers is provided, this is not necessary. */

        IF ISNULL(@workers_str,'') = ''
            INSERT INTO @worker_list ([worker_no])
            SELECT DISTINCT a1.[worker_customer_no]--, lis.[worker_no]
            FROM @work1 AS w
                 INNER JOIN [dbo].[V_CUSTOMER_WITH_PRIMARY_GROUP] AS vc ON w.[customer_no] = vc.[customer_no]
                 INNER JOIN [dbo].[VS_CONTRIBUTION_WITH_INITIATOR] AS a1 ON vc.[expanded_customer_no] = a1.[customer_no] AND w.[ref_no] = a1.[ref_no]
                 LEFT OUTER JOIN @worker_list AS lis ON lis.[worker_no] = a1.[worker_customer_no]
            WHERE a1.[worker_customer_no] IS NOT NULL AND lis.[worker_no] IS NULL

    FINISHED:

   

        /*  Final Data Set  */

            SELECT  w.[customer_no],
                    w.[ref_no],
                    w.[cust_name],
                    w.[cont_dt],
                    w.[cont_type],
                    w.[cont_amt],
                    w.[cont_fund],
                    w.[designation],
                    a1.[worker_customer_no],
                    w.[plan_primary_worker],
                    w.[cont_worker],
                    w.[state],
                    ISNULL(w.[memb_level],'') AS [memb_level],
                    ISNULL(w.[memb_trend],'') AS [memb_trend],
                    ISNULL(w.[nrr_status],'') AS [nrr_status],
                    w.[letter_desc],
                    w.[expr_dt],
                    w.[sort_name],
                    ISNULL(w.[initiator_no],0) AS [initiator_no],
                    ISNULL(w.[initiator_name],'') AS [initiator_name],
                    ISNULL(w.[creditee_no],0) AS [creditee_no],
                    CASE WHEN ISNULL(w.[creditee_name],'') = '' THEN ''
                         ELSE RTRIM(LEFT(ISNULL(w.[credit_type_desc],''),4)) + ' - ' + ISNULL(w.[creditee_name],'') END AS [creditee_name],
                    ISNULL(w.[credit_amt],0.0) AS [credit_amt],
                    ISNULL(w.[credit_type_desc],'') AS [credit_type_desc],
                    a1.[source_no],
                    ap.[source_name]
            FROM @work1 AS w
                 INNER JOIN [dbo].[V_CUSTOMER_WITH_PRIMARY_GROUP] AS vc ON w.[customer_no] = vc.[customer_no]
                 INNER JOIN [dbo].[VS_CONTRIBUTION_WITH_INITIATOR] AS a1 ON vc.[expanded_customer_no] = a1.[customer_no] AND w.[ref_no] = a1.[ref_no]
                 INNER JOIN @worker_list AS lst ON lst.[worker_no] = ISNULL(a1.[worker_customer_no],0)
                 LEFT OUTER JOIN [dbo].[TX_APPEAL_MEDIA_TYPE] AS ap ON ap.[source_no] = a1.[source_no]
            WHERE a1.[cont_amt] >= @min_cont_amt
              AND ISNULL(w.[letter_desc], '') <> 'Adjustment/Reentry' -- omitting Reentires.
              AND vc.[customer_no] <> 2653093 -- Stewardship Soft Credit
              AND ISNULL(vc.[inactive], 1) = 1
              --AND ISNULL(a1.[creditee_type],0) IN (0, 5, 12, 15)
            ORDER BY w.[cont_amt] DESC, w.[plan_primary_worker], w.[sort_name]

    DONE:

  
END
GO

EXECUTE [dbo].[LRP_ADV_WKLY_GIFTS_RECEIVED]  @numOfDays = 25, @min_cont_amt = 0, @workers_str = ''-- @workers_str = '817730,701702,3954906'
GO

/*
SELECT * FROM [dbo].[T_PROMOTION] WHERE source_no = 11937

SELECT col.[object_id], obj.[name], col.[name] 
FROM sys.COLUMNS AS col
     INNER JOIN sys.[objects] AS obj ON obj.[object_id] = col.[object_id]
WHERE col.[name] = 'source_no'
  AND obj.[name] NOT LIKE 'C_%'
ORDER BY obj.[name]

SELECT * FROM dbo.TX_APPEAL_MEDIA_TYPE WHERE [source_no] = 11937

--SELECT [source_no], COUNT(*) FROM dbo.TX_APPEAL_MEDIA_TYPE GROUP BY [source_no] HAVING COUNT(*) <> 1
*/

