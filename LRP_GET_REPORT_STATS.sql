USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LRP_GET_REPORT_STATS]    Script Date: 2/20/2018 1:48:06 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

DROP PROCEDURE [dbo].[LRP_GET_REPORT_STATS]
GO

CREATE PROCEDURE [dbo].[LRP_GET_REPORT_STATS]
	@StartDate DATETIME,
	@EndDate DATETIME,
	@ResultType  VARCHAR(10) -- Detail, Summary
AS 

	   SET NOCOUNT ON

       DECLARE @tblLog TABLE ([ReportNbr] INT,
                              [StepNbr] INT,
                              [ScheduleType] VARCHAR(30),
                              [ExecutionId] VARCHAR(200),
                              [SQL Server] VARCHAR(30),
                              [Report Path] VARCHAR(MAX),
                              [User Name] VARCHAR(100),
                              [Request Type] VARCHAR(100),
                              [Format] VARCHAR(100),
                              [Custom Type] VARCHAR(30),
                              [Full Parameters] VARCHAR(MAX),
                              [User Group] VARCHAR(30),
                              [User ID] VARCHAR(30),
                              [Company Name] VARCHAR(100),
                              [Report Parameters] VARCHAR(MAX),
                              [Action] VARCHAR(100),
                              [Start Time] DATETIME,
                              [End Time] DATETIME,
                              [Duration (Sec)] DECIMAL(18, 2),
                              [Data Retrieval Time (Sec)] DECIMAL(18, 2),
                              [Data Retrieval Time (MS)] DECIMAL(18, 2),
                              [Processing Time (Sec)] DECIMAL(18, 2),
                              [Processing Time (MS)] DECIMAL(18, 2),
                              [Rendering Time (Sec)] DECIMAL(18, 2),
                              [Rendering Time (MS)] DECIMAL(18, 2),
                              [Type of Execution] VARCHAR(100),
                              [Status] VARCHAR(100),
                              [Byte Count] INT,
                              [Record Count] INT,
                              [Additional Info] XML)

       INSERT   INTO @tblLog
                SELECT  DENSE_RANK() OVER (ORDER BY ExecutionId ASC),
                        ROW_NUMBER() OVER (PARTITION BY ExecutionId ORDER BY TimeStart ASC),
                        'On Demand',
                        ExecutionId,
                        LTRIM(RTRIM(SUBSTRING(InstanceName, 1, PATINDEX('%\%', InstanceName) - 1))) AS 'SQL Server',
                        ItemPath AS 'Report Path',
                        CASE WHEN [Parameters] NOT LIKE 'customer_no%' THEN 'Report Account'
                             ELSE [UserName]
                        END AS 'User Name',
                        RequestType,
                        CASE WHEN [Format] IS NULL THEN 'Subreport or Sort'
                             WHEN [Format] = 'RPL' THEN 'Web/Internal'
                             ELSE [Format]
                        END AS 'Format',
                        CASE WHEN [Parameters] LIKE 'customer_no%' THEN 'Screen'
                             ELSE 'Report'
                        END AS 'Custom Type',
                        REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(CONVERT(VARCHAR(MAX), [Parameters]), '%20', ' '), '%2F', '/'), '%3A', ':'), '%22', '"'), '%2C', ','),
                        '' AS [User Group],
                        '' AS [User ID],
                        '' AS [Company Name],
                        '' AS [Report Parameters],
                        ItemAction AS [Action],
                        TimeStart AS [Start Time],
                        TimeEnd AS [End Time],
                        CONVERT(DECIMAL(18, 2), (DATEDIFF(ms, TimeStart, TimeEnd) / 1000)) AS [Duration (Sec)],
                        CONVERT(DECIMAL(18, 2), TimeDataRetrieval) / 1000 AS [Data Retrieval Time (Sec)],
                        CONVERT(DECIMAL(18, 2), TimeDataRetrieval) AS [Data Retrieval Time (MS)],
                        CONVERT(DECIMAL(18, 2), TimeProcessing) / 1000 AS [Processing Time (Sec)],
                        CONVERT(DECIMAL(18, 2), TimeProcessing) AS [Processing Time (MS)],
                        CONVERT(DECIMAL(18, 2), TimeRendering) / 1000 AS [Rendering Time (Sec)],
                        CONVERT(DECIMAL(18, 2), TimeRendering) AS [Rendering Time (MS)],
                        [Source] AS [Type of Execution],
                        [Status],
                        ByteCount,
                        [RowCount],
                        AdditionalInfo
                FROM    [ReportServer].[dbo].[ExecutionLog3] (NOLOCK)
                WHERE   ExecutionId IS NOT NULL
				AND		(TimeStart >= @StartDate 
				AND		TimeStart <= @EndDate + 1)

--select 'debug 1', * from @tblLog order by [Start Time]

       INSERT   INTO @tblLog
                SELECT  0,
                        0,
                        'Scheduled',
                        ExecutionId,
                        LTRIM(RTRIM(SUBSTRING(InstanceName, 1, PATINDEX('%\%', InstanceName) - 1))) AS 'SQL Server',
                        ItemPath AS 'Report Path',
                        CASE WHEN [Parameters] NOT LIKE 'customer_no%' THEN 'Report Account'
                             ELSE [UserName]
                        END AS 'User Name',
                        RequestType,
                        CASE WHEN [Format] IS NULL THEN 'Subreport or Sort'
                             WHEN [Format] = 'RPL' THEN 'Web/Internal'
                             ELSE [Format]
                        END AS 'Format',
                        CASE WHEN [Parameters] LIKE 'customer_no%' THEN 'Screen'
                             ELSE 'Report'
                        END AS 'Custom Type',
                        REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(CONVERT(VARCHAR(MAX), [Parameters]), '%20', ' '), '%2F', '/'), '%3A', ':'), '%22', '"'), '%2C', ','),
                        '' AS [User Group],
                        '' AS [User ID],
                        '' AS [Company Name],
                        '' AS [Report Parameters],
                        ItemAction AS [Action],
                        TimeStart AS [Start Time],
                        TimeEnd AS [End Time],
                        CONVERT(DECIMAL(18, 2), (DATEDIFF(ms, TimeStart, TimeEnd) / 1000)) AS [Duration (Sec)],
                        CONVERT(DECIMAL(18, 2), TimeDataRetrieval) / 1000 AS [Data Retrieval Time (Sec)],
                        CONVERT(DECIMAL(18, 2), TimeDataRetrieval) AS [Data Retrieval Time (MS)],
                        CONVERT(DECIMAL(18, 2), TimeProcessing) / 1000 AS [Processing Time (Sec)],
                        CONVERT(DECIMAL(18, 2), TimeProcessing) AS [Processing Time (MS)],
                        CONVERT(DECIMAL(18, 2), TimeRendering) / 1000 AS [Rendering Time (Sec)],
                        CONVERT(DECIMAL(18, 2), TimeRendering) AS [Rendering Time (MS)],
                        [Source] AS [Type of Execution],
                        [Status],
                        ByteCount,
                        [RowCount],
                        AdditionalInfo
                FROM    [ReportServer].[dbo].[ExecutionLog3] (NOLOCK)
                WHERE   ExecutionId IS NULL
				AND		(TimeStart >= @StartDate 
				AND		TimeStart <= @EndDate + 1)

 --SELECT 'debug 2', * FROM @tblLog order by [Start Time]

       UPDATE   @tblLog
       SET      [User Group] = LTRIM(RTRIM(REPLACE(LEFT(SUBSTRING(LTRIM(RTRIM([Full Parameters])), PATINDEX('%UserGroup=%', LTRIM(RTRIM([Full Parameters]))) + 10, LEN([Full Parameters])), PATINDEX('%&%', SUBSTRING(LTRIM(RTRIM([Full Parameters])), PATINDEX('%UserGroup=%', LTRIM(RTRIM([Full Parameters]))) + 10, LEN([Full Parameters])))), '&', ''))),
                [User Id] = LTRIM(RTRIM(REPLACE(LEFT(SUBSTRING(LTRIM(RTRIM([Full Parameters])), PATINDEX('%UserId=%', LTRIM(RTRIM([Full Parameters]))) + 7, LEN([Full Parameters])), PATINDEX('%&%', SUBSTRING(LTRIM(RTRIM([Full Parameters])), PATINDEX('%UserId=%', LTRIM(RTRIM([Full Parameters]))) + 7, LEN([Full Parameters])))), '&', ''))),
                [Company Name] = LTRIM(RTRIM(REPLACE(LEFT(SUBSTRING(LTRIM(RTRIM([Full Parameters])), PATINDEX('%CompanyName=%', LTRIM(RTRIM([Full Parameters]))) + 12, LEN([Full Parameters])), PATINDEX('%&%', SUBSTRING(LTRIM(RTRIM([Full Parameters])), PATINDEX('%CompanyName=%', LTRIM(RTRIM([Full Parameters]))) + 12, LEN([Full Parameters])))), '&', '')))

       --UPDATE   @tblLog
       --SET      [Report Parameters] = REPLACE(SUBSTRING(LTRIM(RTRIM([Full Parameters])), PATINDEX('%RequestId=%', LTRIM(RTRIM([Full Parameters]))) + 16, LEN([Full Parameters])), '&', ', ')
       --WHERE    PATINDEX('UserGroup=%', LTRIM(RTRIM([Full Parameters]))) > 0

--SELECT 'debug 3', * FROM @tblLog order by [Start Time]

       UPDATE   @tblLog
       SET      [Report Parameters] = REPLACE([Full Parameters], '&', ', ')
       --WHERE    PATINDEX('%UserGroup=%', LTRIM(RTRIM([Full Parameters]))) = 0
       --         AND PATINDEX('%customer_no=%', LTRIM(RTRIM([Full Parameters]))) = 0

--SELECT 'debug 4', * FROM @tblLog order by [Start Time]

	   IF @ResultType = 'Detail'
		   SELECT   x.[ReportNbr],
					x.[SQL Server],
					x.[Report Path],
					CASE
						WHEN [Report Path] LIKE '/Tessitura/CustomReports/TessituraCreatedCustomReports/%' THEN 'Tessitura Custom'
						WHEN [Report Path] LIKE '/Tessitura/CustomReports/%' THEN 'MOS Custom'
						WHEN [Report Path] LIKE '/Tessitura/Reports/%' THEN 'Tessitura Standard'
						ELSE 'Unknown'
					END AS [Created By],
					CASE WHEN [Report Path] LIKE '/Tessitura/ScheduledReports/%' THEN 'Y' ELSE 'N' END AS [Scheduled],
					x.[User Name],
					x.[Format],
					x.[Custom Type],
					x.[User Group],
					x.[User ID],
					x.[Company Name],
					REPLACE(x.[Report Parameters], ' ', '') AS [Report Parameters],
					x.[Action],
					CONVERT(VARCHAR(10), x.[Start Time], 101) AS [Start Time],
					CONVERT(VARCHAR(10), x.[End Time], 101) AS [End Time],
					x.[Start Time] AS 'Raw Start time',
					x.[End Time] AS 'Raw End Time'					,
					CASE
						WHEN DATEPART(hh, x.[Start Time]) < 12 THEN CONVERT(VARCHAR(10), DATEPART(hh, x.[Start Time])) + ' AM - ' + CONVERT(VARCHAR(10), DATEPART(hh, x.[Start Time]) + 1) + ' AM'
						WHEN DATEPART(hh, x.[Start Time]) = 12 THEN CONVERT(VARCHAR(10), DATEPART(hh, x.[Start Time])) + ' PM - ' + CONVERT(VARCHAR(10), (DATEPART(hh, x.[Start Time]) + 1) - 12) + ' PM'
						WHEN DATEPART(hh, x.[Start Time]) > 12 THEN CONVERT(VARCHAR(10), DATEPART(hh, x.[Start Time]) - 12) + ' PM - ' + CONVERT(VARCHAR(10), (DATEPART(hh, x.[Start Time]) + 1) - 12) + ' PM'
						ELSE 'xxx'
					END AS [Time Of Day - Start],
					CASE
						WHEN DATEPART(hh, x.[End Time]) < 12 THEN CONVERT(VARCHAR(10), DATEPART(hh, x.[End Time])) + ' AM - ' + CONVERT(VARCHAR(10), DATEPART(hh, x.[End Time]) + 1) + ' AM'
						WHEN DATEPART(hh, x.[End Time]) = 12 THEN CONVERT(VARCHAR(10), DATEPART(hh, x.[End Time])) + ' PM - ' + CONVERT(VARCHAR(10), (DATEPART(hh, x.[End Time]) + 1) - 12) + ' PM'
						WHEN DATEPART(hh, x.[End Time]) > 12 THEN CONVERT(VARCHAR(10), DATEPART(hh, x.[End Time]) - 12) + ' PM - ' + CONVERT(VARCHAR(10), (DATEPART(hh, x.[End Time]) + 1) - 12) + ' PM'
						ELSE 'xxx'
					END AS [Time Of Day - End],
					CASE
						WHEN DATEPART(hh, x.[Start Time]) < 12 THEN CONVERT(VARCHAR(5), x.[Start Time], 108) + ' AM'
						WHEN DATEPART(hh, x.[Start Time]) = 12 THEN CONVERT(VARCHAR(5), x.[Start Time], 108) + ' PM'
						ELSE CONVERT(VARCHAR(5), DATEADD(hh, -12, x.[Start Time]), 108) + ' PM'
					END	AS [Actual Start Time],
					CASE
						WHEN DATEPART(hh, x.[End Time]) < 12 THEN CONVERT(VARCHAR(5), x.[End Time], 108) + ' AM'
						WHEN DATEPART(hh, x.[End Time]) = 12 THEN CONVERT(VARCHAR(5), x.[End Time], 108) + ' PM'
						ELSE CONVERT(VARCHAR(5), DATEADD(hh, -12, x.[End Time]), 108) + ' PM'
					END	AS [Actual End Time],
					x.[Duration (Sec)],
					SUM((x.[Duration (Sec)]/60)) AS [Duration (Min)],
					x.[Data Retrieval Time (Sec)],
					x.[Processing Time (Sec)],
					x.[Rendering Time (Sec)],
					dd.[MonthNameShort],
					dd.[MonthName],
					dd.[DayNameShort],
					dd.[DayName],
					dd.[IsWeekday],
					dd.[IsWeekend],
					x.[Type of Execution],
					SUBSTRING(x.[Status], 3, LEN(x.[Status])) AS [Status],
					FORMAT(x.[Byte Count], '###,###,###') AS [Byte Count],
					FORMAT(x.[Record Count], '###,###,###') AS [Record Count]
		   FROM     @tblLog x
		   JOIN     [admin].[dbo].[DimDate] dd ON CONVERT(VARCHAR(10), x.[Start Time], 101) = dd.[StandardDate]
		   WHERE    [ReportNbr] > 0
		   GROUP BY x.[ReportNbr], dd.[MonthNameShort], dd.[MonthName], dd.[DayNameShort], dd.[DayName], dd.[IsWeekday], dd.[IsWeekend], x.[SQL Server],
					x.[Report Path], x.[User Name], x.[Format], x.[Custom Type], x.[User Group], x.[User ID], x.[Company Name], x.[Report Parameters],x.[Action],
					[Start Time], [End Time], x.[Duration (Sec)],
					x.[Data Retrieval Time (Sec)], x.[Processing Time (Sec)], x.[Rendering Time (Sec)], x.[Type of Execution], [Status], x.[Byte Count], x.[Record Count]
		   ORDER BY x.[Start Time], x.[ReportNbr]
	   ELSE
		   SELECT   x.[SQL Server],
					x.[Report Path],
					x.[Custom Type],
					CASE
						WHEN [Report Path] LIKE '/Tessitura/CustomReports/TessituraCreatedCustomReports/%' THEN 'Tessitura Custom'
						WHEN [Report Path] LIKE '/Tessitura/CustomReports/%' THEN 'MOS Custom'
						WHEN [Report Path] LIKE '/Tessitura/Reports/%' THEN 'Tessitura Standard'
						ELSE 'Unknown'
						--WHEN [Report Path] LIKE '/Tessitura/ScheduledReports/%' THEN 'Scheduled'
					END AS [Created By],
					CASE WHEN [Report Path] LIKE '/Tessitura/ScheduledReports/%' THEN 'Y' ELSE 'N' END AS [Scheduled],
					CAST(SUM(x.[Duration (Sec)]) AS DECIMAL(18,2)) AS [Total Duration (Sec)],
					CAST(AVG(x.[Duration (Sec)]) AS DECIMAL(18,2)) AS [Average Duration (Sec)],
					CAST(SUM((x.[Duration (Sec)]/60)) AS DECIMAL(18,2)) AS [Total Duration (Min)],
					CAST(AVG((x.[Duration (Sec)]/60)) AS DECIMAL(18,2)) AS [Average Duration (Min)],
					FORMAT(SUM(x.[Byte Count]), '###,###,###') AS [Total Byte Count],
					FORMAT(AVG(x.[Byte Count]), '###,###,###') AS [Average Byte Count],
					FORMAT(SUM(x.[Record Count]), '###,###,###') AS [Total Record Count],
					FORMAT(AVG(x.[Record Count]), '###,###,###') AS [Average Record Count],
					FORMAT(COUNT(x.[Report Path]), '###,###,###') AS [Number of Times Run]
		   FROM     @tblLog x
		   WHERE    [ReportNbr] > 0
		   GROUP BY x.[SQL Server], x.[Report Path], x.[Custom Type]
		   ORDER BY [Average Duration (Min)] DESC



GO


