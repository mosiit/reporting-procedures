USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_CORPORATE_MEMBERSHIP_PROGRESS_REPRORT]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_CORPORATE_MEMBERSHIP_PROGRESS_REPRORT] AS' 
END
GO

ALTER PROCEDURE [dbo].[LRP_CORPORATE_MEMBERSHIP_PROGRESS_REPRORT]
    @month_FY_YTD_Calc VARCHAR(15) = 'FY',
	@requestDt DATETIME = NULL,
    @get_previous_year CHAR(1) = 'N' --NOT USED ANY MORE
AS BEGIN

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    SET ANSI_WARNINGS OFF;
    SET NOCOUNT ON;

    DECLARE @start_dt_range DATETIME,
            @end_dt_range DATETIME;

    DECLARE @fiscal_year VARCHAR(4);
    DECLARE @previous_year_dt DATETIME;

    DECLARE @campaignTbl TABLE ([campaign_no] INT NOT NULL,
	                            [RevenueGoal] DECIMAL(18,2),
	                            [CountGoal] INT);

    DECLARE @previous_year TABLE ([campaign_no] INT, 
                                  [campaignName] VARCHAR(30),
                                  [campaignAbbrev] VARCHAR(30), 
                                  [RevenueGoal] DECIMAL(18,2), 
                                  [CountGoal] INT, 
                                  [start_dt] DATETIME, 
                                  [end_dt] DATETIME, 
                                  [totalAmt] DECIMAL(18,2), 
                                  [totMembs] INT,
                                  [LYcampaign_no] INT, 
                                  [LYcampaignName] VARCHAR(30),
                                  [LYcampaignAbbrev] VARCHAR(30),
                                  [LYRevenueGoal] DECIMAL(18,2), 
                                  [LYCountGoal] INT, 
                                  [LYstart_dt] DATETIME, 
                                  [LYend_dt] DATETIME, 
                                  [LYtotalAmt] DECIMAL(18,2), 
                                  [LYtotMembs] INT);

    SELECT @fiscal_year = CAST(MAX([fyear]) AS VARCHAR(4)) FROM [dbo].[TR_Batch_Period] WHERE @requestDt BETWEEN [start_dt] AND [end_dt];

    IF @get_previous_year = 'Y' BEGIN

        SELECT @previous_year_dt = DATEADD(YEAR,-1,@requestDt);

        INSERT INTO @previous_year
        EXECUTE [dbo].[LRP_CORPORATE_MEMBERSHIP_PROGRESS_REPRORT] @month_FY_YTD_Calc, @previous_year_dt, 'N';
    
    END;
        
	INSERT INTO @campaignTbl (campaign_no, RevenueGoal, CountGoal)
	SELECT [campaign_no], 0.0, 0 FROM [dbo].[T_CAMPAIGN] WHERE [description] = 'Corporate Memb FY' + @fiscal_year;
   
    IF @month_FY_YTD_Calc = 'FY' BEGIN

        -- MEMBERSHIP WANTS TO GET THE GOALS FOR THE ENTIRE FY BUT THEY WANT THE NUMBERS/CALCULATIONS TO BE BASED ON THE REPORT DATE PARAMETER!!!!!!!
        --GET DATES FOR ENTIRE FY IN ORDER TO GET THE GOALS. 
	    SELECT @start_dt_range = MIN([start_dt]), 
               @end_dt_range =   MAX([end_dt])
	                             FROM [dbo].[TR_BATCH_PERIOD] 
	                             WHERE [inactive] = 'n' 
                                   AND [fyear] = (SELECT [fyear] 
                                                  FROM [dbo].[TR_BATCH_Period] 
                                                  WHERE @requestDt BETWEEN [start_dt] AND [end_dt]);

	    WITH goals 
        AS (SELECT c.[campaign_no], 
                   SUM(g.[base_revenue]) + SUM(ISNULL(g.[supp_revenue],0)) AS [MRevenue], 
                   SUM(g.[base_count]) + SUM(ISNULL(g.[supp_count],0)) AS [MCount]
		    FROM [dbo].[LTR_MEMBERSHIP_GOALS] AS g
		         INNER JOIN @campaignTbl AS c ON g.[campaign_no] = c.[campaign_no]
		    WHERE DATEDIFF(day,g.[start_dt], @start_dt_range) <= 0 AND DATEDIFF(day,g.[end_dt], @end_dt_range) >= 0 	
		    GROUP BY c.[campaign_no])
        UPDATE c 
	    SET [RevenueGoal] = g.[MRevenue],
            [CountGoal] = g.[MCount]
	    FROM [goals] g
	         INNER JOIN @campaignTbl AS c ON g.[campaign_no] = c.campaign_no

        --Change date to same as current request date
        SELECT @end_dt_range = CAST(EOMONTH(@requestDt) AS VARCHAR (25)) + ' 23:59:59';

    END ELSE IF @month_FY_YTD_Calc = 'Month' BEGIN
        
	    SELECT @start_dt_range = DATEFROMPARTS(DATEPART(YEAR,@requestDt),DATEPART(MONTH,@requestDt),1)
	    SELECT @end_dt_range = CAST(EOMONTH(@start_dt_range) AS VARCHAR (25)) + ' 23:59:59'
            
    	UPDATE c
	    SET [RevenueGoal] = [base_revenue] + ISNULL(g.[supp_revenue],0), 
			[CountGoal] = [base_count] + ISNULL(g.[supp_count],0) 
    	FROM [dbo].[LTR_MEMBERSHIP_GOALS] AS g
             INNER JOIN @campaignTbl AS c ON g.[campaign_no] = c.[campaign_no]
                                         AND g.[start_dt] >= @start_dt_range
                                         AND g.[end_dt] <= @end_dt_range

    END ELSE IF @month_FY_YTD_Calc = 'YTD' BEGIN

        SELECT @start_dt_range = MIN(start_dt)
                                 FROM [dbo].[TR_BATCH_PERIOD]
                                 WHERE [inactive] = 'n' 
                                   AND [fyear] = (SELECT [fyear] 
                                                  FROM [dbo].[TR_BATCH_Period] 
                                                  WHERE @requestDt BETWEEN [start_dt] AND [end_dt]);
        SELECT @end_dt_range = CAST(EOMONTH(@requestDt) AS VARCHAR (25)) + ' 23:59:59';
	    
    	WITH goals 
        AS (SELECT c.[campaign_no], 
                   SUM(g.[base_revenue]) + SUM(ISNULL(g.[supp_revenue],0)) AS [MRevenue], 
                   SUM(g.[base_count]) + SUM(ISNULL(g.[supp_count],0)) AS [MCount]
		    FROM [dbo].[LTR_MEMBERSHIP_GOALS] AS g
		         INNER JOIN @campaignTbl AS c ON g.[campaign_no] = c.[campaign_no]
		    WHERE DATEDIFF(day,g.[start_dt], @start_dt_range) <= 0 AND DATEDIFF(day,g.[end_dt], @end_dt_range) >= 0 	
		    GROUP BY c.[campaign_no])
	    UPDATE c 
	    SET [RevenueGoal] = g.[MRevenue],
            [CountGoal] = g.[MCount]
	    FROM [goals] g
	         INNER JOIN @campaignTbl c ON g.campaign_no = c.campaign_no
	
    END;

      WITH [CTE_MEMBERSHIP_COUNTS] ([campaign_no], [memb_count])
    AS (SELECT mem.[campaign_no], 
               COUNT(*) 
        FROM [dbo].[TX_CUST_MEMBERSHIP] AS mem
             INNER JOIN @campaignTbl tbl ON tbl.campaign_no = mem.[campaign_no]
             INNER JOIN [dbo].[TX_CONT_MEMB] AS cme ON cme.[cust_memb_no] = mem.[cust_memb_no]
             INNER JOIN [dbo].[T_CONTRIBUTION] AS con ON con.[ref_no] = cme.[cont_ref_no]
        WHERE mem.[current_status] IN (1, 2, 3, 9)
          AND CAST(con.[cont_dt] AS DATE) BETWEEN @start_dt_range AND @end_dt_range
        GROUP BY mem.[campaign_no]),
        [CTE_CAMPAIGN_COUNTS] ([campaign_no], [RevenueGoal], [CountGoal], [membership_count], [membership_revenue])
    AS (SELECT trx.[campaign_no],
    		   tbl.[RevenueGoal],
	    	   tbl.[CountGoal],
               mem.[memb_count] AS [membership_count],
		       SUM(ISNULL(trx.[trn_amt], 0)) AS [ticket_revenue]
	    FROM [dbo].[T_TRANSACTION] AS trx
             INNER JOIN [dbo].[T_CONTRIBUTION] AS con ON con.[ref_no] = trx.[ref_no]
		     INNER JOIN @campaignTbl tbl ON tbl.[campaign_no] = trx.[campaign_no]
		     LEFT OUTER JOIN [CTE_MEMBERSHIP_COUNTS] AS mem ON mem.[campaign_no] = tbl.[campaign_no]
        WHERE CAST(con.[cont_dt] AS DATE) BETWEEN @start_dt_range AND @end_dt_range
          AND trx.[trn_type] IN (1, 2, 4, 5) 
    	GROUP BY trx.[campaign_no], tbl.[RevenueGoal], tbl.[CountGoal], mem.[memb_count])

        SELECT cc.campaign_no, 
               ca.description AS campaignName,
               REPLACE(ca.description,@fiscal_year,'') AS campaignAbbrev,  
	           cc.RevenueGoal RevenueGoal, 
               cc.CountGoal CountGoal, 
               @start_dt_range AS start_dt, 
               @end_dt_range AS end_dt,
	           cc.[membership_revenue] AS totalAmt, 
               cc.[membership_count] AS totMembs,
               ISNULL(py.campaign_no, 0) AS [LYcampaign_no], 
               ISNULL(py.campaignName, '') AS [LYcampaignname],
               ISNULL(py.campaignAbbrev, '') AS [LYcampaignAbbrev],
               ISNULL(py.RevenueGoal, 0) AS [LYRevenueGoal], 
               ISNULL(py.CountGoal, 0) AS [LYCountGoal], 
               py.[start_dt] AS [LYstart_dt], 
               py.[end_dt] AS [LYend_dt],
    	       ISNULL(py.[totalAmt], 0) AS [LYtotalamt], 
               ISNULL(py.[totMembs], 0) AS [LYtotMembs]
        FROM [CTE_CAMPAIGN_COUNTS] as cc
             LEFT JOIN [dbo].[T_CAMPAIGN] ca ON cc.[campaign_no] = ca.[campaign_no]
             LEFT JOIN @previous_year AS py ON py.[campaignAbbrev] = REPLACE(ca.[description],@fiscal_year,'')
        

    DONE:

END
GO

GRANT EXECUTE ON [dbo].[LRP_CORPORATE_MEMBERSHIP_PROGRESS_REPRORT] TO ImpUsers
GO

EXECUTE [dbo].[LRP_CORPORATE_MEMBERSHIP_PROGRESS_REPRORT] @month_FY_YTD_Calc = 'YTD', @requestDt = '9-30-2021', @get_previous_year = 'Y' 



--SELECT * FROM [dbo].[T_TRANSACTION] WHERE [campaign_no] = 2088
--SELECT DISTINCT [trn_type] FROM [dbo].[T_TRANSACTION] WHERE [campaign_no] = 2088
--SELECT * FROM [dbo].[TR_TRANSACTION_TYPE] WHERE id IN (31, 32, 33, 51, 52, 53)
--SELECT * FROM [dbo].[TR_TRANSACTION_TYPE] WHERE id IN (1, 2, 3, 4) 


--SELECT SUM([cont_amt]), COUNT(DISTINCT [customer_no]) FROM [dbo].[T_CONTRIBUTION] WHERE [campaign_no] = 2377
--SELECT * FROM [dbo].[T_CONTRIBUTION] WHERE [campaign_no] = 2377
