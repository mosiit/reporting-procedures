USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LRP_NIGHTLY_SUMM_ENGINE]    Script Date: 2/8/2021 1:56:52 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




ALTER PROCEDURE [dbo].[LRP_NIGHTLY_SUMM_ENGINE]
	@runBy	VARCHAR(300)
AS

-- ============================================================================================
-- H. Sheridan, 4/4/2016
--
-- This is the main driver for gathering all of the nightly summary data for
-- the Advancement/DMS group.  Either the entire transaction takes or none
-- of it takes.  I don't want some numbers updated one night if others can't
-- be.  Hence, the transaction wrapper.
--
-- Update: Service Now ticket SCTASK0001710 (Update nightly summary logic for Boston Common)
--
-- Details: Advancement would like to make some updates to the Nightly Summaries logic. We would 
-- like to add a new row for Boston Science Common Campaign total, and remove the row for 
-- Lifetime Innovator Dues. In addition, we want to update most rows to stop excluding 
-- Account Group = Membership, and start excluding Household Memb and Library Memb. I will 
-- attach a Word document with the updates needed for each row. It would be great if this could 
-- be completed within the next 6-8 weeks, but we can be flexible with the date so please let 
-- me know if this is not possible. Please contact Katie Yates or Melanie Sherwood with any 
-- questions about this request. Thank you!
--
-- ============================================================================================

BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SET NOCOUNT ON

	DECLARE	@section VARCHAR(300)
	DECLARE	@sortOrder INT

	DECLARE	@fy INT
	DECLARE	@fystart DATETIME
	DECLARE	@fyend DATETIME

	DECLARE	@fyx INT
	DECLARE	@fystartx DATETIME
	DECLARE	@fyendx DATETIME
	DECLARE	@fyearx INT

	DECLARE	@fym1 INT
	DECLARE	@fystartm1 DATETIME
	DECLARE	@fyendm1 DATETIME

	DECLARE	@fym2 INT
	DECLARE	@fystartm2 DATETIME
	DECLARE	@fyendm2 DATETIME

	DECLARE	@fym3 INT
	DECLARE	@fystartm3 DATETIME
	DECLARE	@fyendm3 DATETIME

	DECLARE	@cyear	VARCHAR(2)
	DECLARE	@cystart DATETIME
	DECLARE	@cyend DATETIME

	DECLARE	@wcnt INT
	DECLARE	@mcnt INT
	DECLARE @rpcnt INT

	DECLARE	@washburn_yrs INT
	DECLARE	@marathon_yrs INT
	DECLARE @run_program_yrs INT

	--DECLARE	@washburn_yrs INT

	BEGIN TRY

		-- Not sure if I want to clear these down every time or keep past data.  Clear down for testing, at least.
		DELETE FROM LT_NIGHTLY_SUMMARY
		DELETE FROM LT_NIGHTLY_SUMMARY_LOG

		-- --------------------------------------------------------------------------------------------------------------------
		-- To change the order of the list, move around the blocks of code.  The only '@sortOrder' that's explicitly set is
		-- the first one (SET @sortOrder = 1).  Everything after that is based on 'SET @sortOrder = @sortOrder + 1', so you
		-- don't have to explicitly set anything.  Moving the block of code should reset the order of the list.
		-- --------------------------------------------------------------------------------------------------------------------
		SET	@sortOrder = 0

		-- ---------------------------------------------------------------
		-- Boston Science Common Campaign Total
		-- ---------------------------------------------------------------
		--2020/12/14, H. Sheridan. Service Now ticket SCTASK0001710 - new section added, position 1
		SET @section = 'Boston Science Common Campaign Total'
		SET	@sortOrder = @sortOrder + 1

		EXEC LRP_NIGHTLY_SUMM_BOSTON_SCIENCE_COMMON_CAMPAIGN @section, @sortOrder, @runBy	

		INSERT INTO LT_NIGHTLY_SUMMARY_LOG
			SELECT	@sortOrder, @section, ISNULL(COUNT(customer_no), 0), ISNULL(SUM(value), 0), ISNULL(MIN(apply_date), GetDate()), ISNULL(MAX(apply_date), GetDate()), GETDATE(), @runBy
			FROM	LT_NIGHTLY_SUMMARY
			WHERE	section = @section

		-- ---------------------------------------------------------------
		-- Comprehensive Lifetime Total
		-- ---------------------------------------------------------------
		SET @section = 'Comprehensive Lifetime Total'
		SET	@sortOrder = @sortOrder + 1

		EXEC LRP_NIGHTLY_SUMM_COMP_LIFE_TOTAL @section, @sortOrder, @runBy	

		INSERT INTO LT_NIGHTLY_SUMMARY_LOG
			SELECT	@sortOrder, @section, ISNULL(COUNT(customer_no), 0), ISNULL(SUM(value), 0), ISNULL(MIN(apply_date), GetDate()), ISNULL(MAX(apply_date), GetDate()), GETDATE(), @runBy
			FROM	LT_NIGHTLY_SUMMARY
			WHERE	section = @section

		-- ---------------------------------------------------------------
		-- Lifetime Corp Membership Dues
		-- ---------------------------------------------------------------
		SET @section = 'Lifetime Corp Membership Dues'
		SET	@sortOrder = @sortOrder + 1

		EXEC LRP_NIGHTLY_SUMM_LIFE_CORP_MEM @section, @sortOrder, @runBy	

		INSERT INTO LT_NIGHTLY_SUMMARY_LOG
			SELECT	@sortOrder, @section, ISNULL(COUNT(customer_no), 0), ISNULL(SUM(value), 0), ISNULL(MIN(apply_date), GetDate()), ISNULL(MAX(apply_date), GetDate()), GETDATE(), @runBy
			FROM	LT_NIGHTLY_SUMMARY
			WHERE	section = @section

		-- ---------------------------------------------------------------
		-- Lifetime Innovator Dues
		-- ---------------------------------------------------------------
		--2020/12/14, H. Sheridan. Service Now ticket SCTASK0001710 - remove this section
		--SET @section = 'Lifetime Innovator Dues'
		--SET	@sortOrder = @sortOrder + 1

		--EXEC LRP_NIGHTLY_SUMM_LIFE_INNOV_DUES @section, @sortOrder, @runBy	

		--INSERT INTO LT_NIGHTLY_SUMMARY_LOG
		--	SELECT	@sortOrder, @section, ISNULL(COUNT(customer_no), 0), ISNULL(SUM(value), 0), ISNULL(MIN(apply_date), GetDate()), ISNULL(MAX(apply_date), GetDate()), GETDATE(), @runBy
		--	FROM	LT_NIGHTLY_SUMMARY
		--	WHERE	section = @section

		-- ---------------------------------------------------------------
		-- I3 Comprehensive Campaign Total
		-- ---------------------------------------------------------------
		--2020/12/14, H. Sheridan. Service Now ticket SCTASK0001710 - move to position 4
		SET @section = 'I3 Comprehensive Campaign Total'
		SET	@sortOrder = @sortOrder + 1

		EXEC LRP_NIGHTLY_SUMM_I3_CAMPAIGN @section, @sortOrder, @runBy	

		INSERT INTO LT_NIGHTLY_SUMMARY_LOG
			SELECT	@sortOrder, @section, ISNULL(COUNT(customer_no), 0), ISNULL(SUM(value), 0), ISNULL(MIN(apply_date), GetDate()), ISNULL(MAX(apply_date), GetDate()), GETDATE(), @runBy
			FROM	LT_NIGHTLY_SUMMARY
			WHERE	section = @section

		-- ---------------------------------------------------------------
		-- Outstanding Pledge Balance
		-- ---------------------------------------------------------------
		SET @section = 'Outstanding Pledge Balance'
		SET	@sortOrder = @sortOrder + 1

		EXEC LRP_NIGHTLY_SUMM_OUT_PLEDGE_BAL @section, @sortOrder, @runBy

		INSERT INTO LT_NIGHTLY_SUMMARY_LOG
			SELECT	@sortOrder, @section, ISNULL(COUNT(customer_no), 0), ISNULL(SUM(value), 0), ISNULL(MIN(apply_date), GetDate()), ISNULL(MAX(apply_date), GetDate()), GETDATE(), @runBy
			FROM	LT_NIGHTLY_SUMMARY
			WHERE	section = @section

		-- ---------------------------------------------------------------
		-- Outstanding AF Pledge Balance_Current FY
		-- ---------------------------------------------------------------
		-- SN SCTASK0001710 - No change
		SELECT	@fy = [fyear]
		FROM	[dbo].[TR_BATCH_PERIOD]
		WHERE	GetDate() BETWEEN start_dt AND end_dt

		SELECT	@fystart = CAST(MIN(start_dt) AS DATE),
				@fyend = CAST(MAX(end_dt) AS DATE)
		FROM	[dbo].[TR_BATCH_PERIOD]
		WHERE	[fyear] = @fy

		SET @section = 'Outstanding AF Pledge Balance Current FY'
		SET	@sortOrder = @sortOrder + 1

		EXEC LRP_NIGHTLY_SUMM_OUT_AFPLEDGE_CFY @section, @sortOrder, @runBy, @fy, @fystart, @fyend

		INSERT INTO LT_NIGHTLY_SUMMARY_LOG
			SELECT	@sortOrder, @section, ISNULL(COUNT(customer_no), 0), ISNULL(SUM(value), 0), ISNULL(MIN(apply_date), GetDate()), ISNULL(MAX(apply_date), GetDate()), GETDATE(), @runBy
			FROM	LT_NIGHTLY_SUMMARY
			WHERE	section = @section

		-- ---------------------------------------------------------------
		-- Gift excl GIK_First
		-- ---------------------------------------------------------------
		SET @section = 'Gift excl GIK First'
		SET	@sortOrder = @sortOrder + 1

		EXEC LRP_NIGHTLY_SUMM_GIK_EXCL_FIRST @section, @sortOrder, @runBy

		INSERT INTO LT_NIGHTLY_SUMMARY_LOG
			SELECT	@sortOrder, @section, ISNULL(COUNT(customer_no), 0), ISNULL(SUM(value), 0), ISNULL(MIN(apply_date), GetDate()), ISNULL(MAX(apply_date), GetDate()), GETDATE(), @runBy
			FROM	LT_NIGHTLY_SUMMARY
			WHERE	section = @section

		-- ---------------------------------------------------------------
		-- Gift excl GIK_Largest
		-- ---------------------------------------------------------------
		SET @section = 'Gift excl GIK Largest'
		SET	@sortOrder = @sortOrder + 1

		EXEC LRP_NIGHTLY_SUMM_GIK_EXCL_LARGEST @section, @sortOrder, @runBy

		INSERT INTO LT_NIGHTLY_SUMMARY_LOG
			SELECT	@sortOrder, @section, ISNULL(COUNT(customer_no), 0), ISNULL(SUM(value), 0), ISNULL(MIN(apply_date), GetDate()), ISNULL(MAX(apply_date), GetDate()), GETDATE(), @runBy
			FROM	LT_NIGHTLY_SUMMARY
			WHERE	section = @section

		-- ---------------------------------------------------------------
		-- Gift excl GIK_Latest
		-- ---------------------------------------------------------------
		SET @section = 'Gift excl GIK Latest'
		SET	@sortOrder = @sortOrder + 1

		EXEC LRP_NIGHTLY_SUMM_GIK_EXCL_LATEST @section, @sortOrder, @runBy

		INSERT INTO LT_NIGHTLY_SUMMARY_LOG
			SELECT	@sortOrder, @section, ISNULL(COUNT(customer_no), 0), ISNULL(SUM(value), 0), ISNULL(MIN(apply_date), GetDate()), ISNULL(MAX(apply_date), GetDate()), GETDATE(), @runBy
			FROM	LT_NIGHTLY_SUMMARY
			WHERE	section = @section

		-- ---------------------------------------------------------------
		-- GIK_First
		-- ---------------------------------------------------------------
		SET @section = 'GIK First'
		SET	@sortOrder = @sortOrder + 1

		EXEC LRP_NIGHTLY_SUMM_GIK_FIRST @section, @sortOrder, @runBy

		INSERT INTO LT_NIGHTLY_SUMMARY_LOG
			SELECT	@sortOrder, @section, ISNULL(COUNT(customer_no), 0), ISNULL(SUM(value), 0), ISNULL(MIN(apply_date), GetDate()), ISNULL(MAX(apply_date), GetDate()), GETDATE(), @runBy
			FROM	LT_NIGHTLY_SUMMARY
			WHERE	section = @section

		-- ---------------------------------------------------------------
		-- GIK_Largest
		-- ---------------------------------------------------------------
		SET @section = 'GIK Largest'
		SET	@sortOrder = @sortOrder + 1

		EXEC LRP_NIGHTLY_SUMM_GIK_LARGEST @section, @sortOrder, @runBy

		INSERT INTO LT_NIGHTLY_SUMMARY_LOG
			SELECT	@sortOrder, @section, ISNULL(COUNT(customer_no), 0), ISNULL(SUM(value), 0), ISNULL(MIN(apply_date), GetDate()), ISNULL(MAX(apply_date), GetDate()), GETDATE(), @runBy
			FROM	LT_NIGHTLY_SUMMARY
			WHERE	section = @section

		-- ---------------------------------------------------------------
		-- GIK_Latest
		-- ---------------------------------------------------------------
		SET @section = 'GIK Latest'
		SET	@sortOrder = @sortOrder + 1

		EXEC LRP_NIGHTLY_SUMM_GIK_LATEST @section, @sortOrder, @runBy

		INSERT INTO LT_NIGHTLY_SUMMARY_LOG
			SELECT	@sortOrder, @section, ISNULL(COUNT(customer_no), 0), ISNULL(SUM(value), 0), ISNULL(MIN(apply_date), GetDate()), ISNULL(MAX(apply_date), GetDate()), GETDATE(), @runBy
			FROM	LT_NIGHTLY_SUMMARY
			WHERE	section = @section

		-- ---------------------------------------------------------------
		-- Pledge_First
		-- ---------------------------------------------------------------
		SET @section = 'Pledge First'
		SET	@sortOrder = @sortOrder + 1

		EXEC LRP_NIGHTLY_SUMM_PLEDGE_FIRST @section, @sortOrder, @runBy

		INSERT INTO LT_NIGHTLY_SUMMARY_LOG
			SELECT	@sortOrder, @section, ISNULL(COUNT(customer_no), 0), ISNULL(SUM(value), 0), ISNULL(MIN(apply_date), GetDate()), ISNULL(MAX(apply_date), GetDate()), GETDATE(), @runBy
			FROM	LT_NIGHTLY_SUMMARY
			WHERE	section = @section

		-- ---------------------------------------------------------------
		-- Pledge_Largest
		-- ---------------------------------------------------------------
		SET @section = 'Pledge Largest'
		SET	@sortOrder = @sortOrder + 1

		EXEC LRP_NIGHTLY_SUMM_PLEDGE_LARGEST @section, @sortOrder, @runBy

		INSERT INTO LT_NIGHTLY_SUMMARY_LOG
			SELECT	@sortOrder, @section, ISNULL(COUNT(customer_no), 0), ISNULL(SUM(value), 0), ISNULL(MIN(apply_date), GetDate()), ISNULL(MAX(apply_date), GetDate()), GETDATE(), @runBy
			FROM	LT_NIGHTLY_SUMMARY
			WHERE	section = @section

		-- ---------------------------------------------------------------
		-- Pledge_Latest
		-- ---------------------------------------------------------------
		SET @section = 'Pledge Latest'
		SET	@sortOrder = @sortOrder + 1

		EXEC LRP_NIGHTLY_SUMM_PLEDGE_LATEST @section, @sortOrder, @runBy

		INSERT INTO LT_NIGHTLY_SUMMARY_LOG
			SELECT	@sortOrder, @section, ISNULL(COUNT(customer_no), 0), ISNULL(SUM(value), 0), ISNULL(MIN(apply_date), GetDate()), ISNULL(MAX(apply_date), GetDate()), GETDATE(), @runBy
			FROM	LT_NIGHTLY_SUMMARY
			WHERE	section = @section

		-- ---------------------------------------------------------------
		-- FY Funds Raised (Current) 
		-- ---------------------------------------------------------------
		SET @section = 'FY' + CONVERT(VARCHAR(4), @fy) + ' Funds Raised'
		SET	@sortOrder = @sortOrder + 1

		EXEC LRP_NIGHTLY_SUMM_FUNDS_RAISED @section, @sortOrder, @runBy, @fy, @fystart, @fyend

		INSERT INTO LT_NIGHTLY_SUMMARY_LOG
			SELECT	@sortOrder, @section, ISNULL(COUNT(customer_no), 0), ISNULL(SUM(value), 0), ISNULL(MIN(apply_date), GetDate()), ISNULL(MAX(apply_date), GetDate()), GETDATE(), @runBy
			FROM	LT_NIGHTLY_SUMMARY
			WHERE	section = @section

		-- ---------------------------------------------------------------
		-- FY Funds Raised (Current Year - 1)
		-- ---------------------------------------------------------------
		SET	@fym1 = @fy - 1
		SET @section = 'FY' + CONVERT(VARCHAR(4), @fym1) + ' Funds Raised'
		SET	@sortOrder = @sortOrder + 1

		SELECT	@fystartm1 = CAST(MIN(start_dt) AS DATE),
				@fyendm1 = CAST(MAX(end_dt) AS DATE)
		FROM	[dbo].[TR_BATCH_PERIOD]
		WHERE	fyear = @fym1

		EXEC LRP_NIGHTLY_SUMM_FUNDS_RAISED @section, @sortOrder, @runBy, @fym1, @fystartm1, @fyendm1

		INSERT INTO LT_NIGHTLY_SUMMARY_LOG
			SELECT	@sortOrder, @section, ISNULL(COUNT(customer_no), 0), ISNULL(SUM(value), 0), ISNULL(MIN(apply_date), GetDate()), ISNULL(MAX(apply_date), GetDate()), GETDATE(), @runBy
			FROM	LT_NIGHTLY_SUMMARY
			WHERE	section = @section

		-- ---------------------------------------------------------------
		-- FY Funds Raised (Current Year - 2)
		-- ---------------------------------------------------------------
		SET	@fym2 = @fy - 2
		SET @section = 'FY' + CONVERT(VARCHAR(4), @fym2) + ' Funds Raised'
		SET	@sortOrder = @sortOrder + 1

		SELECT	@fystartm2 = CAST(MIN(start_dt) AS DATE),
				@fyendm2 = CAST(MAX(end_dt) AS DATE)
		FROM	[dbo].[TR_BATCH_PERIOD]
		WHERE	fyear = @fym2

		EXEC LRP_NIGHTLY_SUMM_FUNDS_RAISED @section, @sortOrder, @runBy, @fym2, @fystartm2, @fyendm2

		INSERT INTO LT_NIGHTLY_SUMMARY_LOG
			SELECT	@sortOrder, @section, ISNULL(COUNT(customer_no), 0), ISNULL(SUM(value), 0), ISNULL(MIN(apply_date), GetDate()), ISNULL(MAX(apply_date), GetDate()), GETDATE(), @runBy
			FROM	LT_NIGHTLY_SUMMARY
			WHERE	section = @section

		-- ---------------------------------------------------------------
		-- FY Funds Raised (Current Year - 3)
		-- ---------------------------------------------------------------
		SET	@fym3 = @fy - 3
		SET @section = 'FY' + CONVERT(VARCHAR(4), @fym3) + ' Funds Raised'
		SET	@sortOrder = @sortOrder + 1

		SELECT	@fystartm3 = CAST(MIN(start_dt) AS DATE),
				@fyendm3 = CAST(MAX(end_dt) AS DATE)
		FROM	[dbo].[TR_BATCH_PERIOD]
		WHERE	fyear = @fym3

		EXEC LRP_NIGHTLY_SUMM_FUNDS_RAISED @section, @sortOrder, @runBy, @fym3, @fystartm3, @fyendm3

		INSERT INTO LT_NIGHTLY_SUMMARY_LOG
			SELECT	@sortOrder, @section, ISNULL(COUNT(customer_no), 0), ISNULL(SUM(value), 0), ISNULL(MIN(apply_date), GetDate()), ISNULL(MAX(apply_date), GetDate()), GETDATE(), @runBy
			FROM	LT_NIGHTLY_SUMMARY
			WHERE	section = @section

		-- ---------------------------------------------------------------
		-- Unrestricted Annual Fund Payments Totals FY (Current) 
		-- ---------------------------------------------------------------
		SET @section = 'FY' + CONVERT(VARCHAR(4), @fy) + ' Unrestricted Annual Fund Payments'
		SET	@sortOrder = @sortOrder + 1

		EXEC LRP_NIGHTLY_SUMM_UNRES_ANNUAL_FUND @section, @sortOrder, @runBy, @fy, @fystart, @fyend

		INSERT INTO LT_NIGHTLY_SUMMARY_LOG
			SELECT	@sortOrder, @section, ISNULL(COUNT(customer_no), 0), ISNULL(SUM(value), 0), ISNULL(MIN(apply_date), GetDate()), ISNULL(MAX(apply_date), GetDate()), GETDATE(), @runBy
			FROM	LT_NIGHTLY_SUMMARY
			WHERE	section = @section

		-- ---------------------------------------------------------------
		-- Unrestricted Annual Fund Payments Totals FY (Current Year - 1)
		-- ---------------------------------------------------------------
		SET	@fym1 = @fy - 1
		SET @section = 'FY' + CONVERT(VARCHAR(4), @fym1) + ' Unrestricted Annual Fund Payments'
		SET	@sortOrder = @sortOrder + 1

		SELECT	@fystartm1 = CAST(MIN(start_dt) AS DATE),
				@fyendm1 = CAST(MAX(end_dt) AS DATE)
		FROM	[dbo].[TR_BATCH_PERIOD]
		WHERE	fyear = @fym1

		EXEC LRP_NIGHTLY_SUMM_UNRES_ANNUAL_FUND @section, @sortOrder, @runBy, @fym1, @fystartm1, @fyendm1

		INSERT INTO LT_NIGHTLY_SUMMARY_LOG
			SELECT	@sortOrder, @section, ISNULL(COUNT(customer_no), 0), ISNULL(SUM(value), 0), ISNULL(MIN(apply_date), GetDate()), ISNULL(MAX(apply_date), GetDate()), GETDATE(), @runBy
			FROM	LT_NIGHTLY_SUMMARY
			WHERE	section = @section

		-- ---------------------------------------------------------------
		-- Unrestricted Annual Fund Payments Totals FY (Current Year - 2)
		-- ---------------------------------------------------------------
		SET	@fym2 = @fy - 2
		SET @section = 'FY' + CONVERT(VARCHAR(4), @fym2) + ' Unrestricted Annual Fund Payments'
		SET	@sortOrder = @sortOrder + 1

		SELECT	@fystartm2 = CAST(MIN(start_dt) AS DATE),
				@fyendm2 = CAST(MAX(end_dt) AS DATE)
		FROM	[dbo].[TR_BATCH_PERIOD]
		WHERE	fyear = @fym2

		EXEC LRP_NIGHTLY_SUMM_UNRES_ANNUAL_FUND @section, @sortOrder, @runBy, @fym2, @fystartm2, @fyendm2

		INSERT INTO LT_NIGHTLY_SUMMARY_LOG
			SELECT	@sortOrder, @section, ISNULL(COUNT(customer_no), 0), ISNULL(SUM(value), 0), ISNULL(MIN(apply_date), GetDate()), ISNULL(MAX(apply_date), GetDate()), GETDATE(), @runBy
			FROM	LT_NIGHTLY_SUMMARY
			WHERE	section = @section

		-- ---------------------------------------------------------------
		-- Unrestricted Annual Fund Payments Totals FY (Current Year - 3)
		-- ---------------------------------------------------------------
		SET	@fym3 = @fy - 3
		SET @section = 'FY' + CONVERT(VARCHAR(4), @fym3) + ' Unrestricted Annual Fund Payments'
		SET	@sortOrder = @sortOrder + 1

		SELECT	@fystartm3 = CAST(MIN(start_dt) AS DATE),
				@fyendm3 = CAST(MAX(end_dt) AS DATE)
		FROM	[dbo].[TR_BATCH_PERIOD]
		WHERE	fyear = @fym3

		EXEC LRP_NIGHTLY_SUMM_UNRES_ANNUAL_FUND @section, @sortOrder, @runBy, @fym3, @fystartm3, @fyendm3

		INSERT INTO LT_NIGHTLY_SUMMARY_LOG
			SELECT	@sortOrder, @section, ISNULL(COUNT(customer_no), 0), ISNULL(SUM(value), 0), ISNULL(MIN(apply_date), GetDate()), ISNULL(MAX(apply_date), GetDate()), GETDATE(), @runBy
			FROM	LT_NIGHTLY_SUMMARY
			WHERE	section = @section

		-- ---------------------------------------------------------------
		-- Grand Total Payments Totals FY (Current) 
		-- ---------------------------------------------------------------
		SET @section = 'FY' + CONVERT(VARCHAR(4), @fy) + ' Grand Total Payments'
		SET	@sortOrder = @sortOrder + 1

		EXEC LRP_NIGHTLY_SUMM_GRND_TOT_PAY @section, @sortOrder, @runBy, @fy, @fystart, @fyend

		INSERT INTO LT_NIGHTLY_SUMMARY_LOG
			SELECT	@sortOrder, @section, ISNULL(COUNT(customer_no), 0), ISNULL(SUM(value), 0), ISNULL(MIN(apply_date), GetDate()), ISNULL(MAX(apply_date), GetDate()), GETDATE(), @runBy
			FROM	LT_NIGHTLY_SUMMARY
			WHERE	section = @section

		-- ---------------------------------------------------------------
		-- Grand Total Payments Totals FY (Current Year - 1)
		-- ---------------------------------------------------------------
		SET	@fym1 = @fy - 1
		SET @section = 'FY' + CONVERT(VARCHAR(4), @fym1) + ' Grand Total Payments'
		SET	@sortOrder = @sortOrder + 1

		SELECT	@fystartm1 = CAST(MIN(start_dt) AS DATE),
				@fyendm1 = CAST(MAX(end_dt) AS DATE)
		FROM	[dbo].[TR_BATCH_PERIOD]
		WHERE	fyear = @fym1

		EXEC LRP_NIGHTLY_SUMM_GRND_TOT_PAY @section, @sortOrder, @runBy, @fym1, @fystartm1, @fyendm1

		INSERT INTO LT_NIGHTLY_SUMMARY_LOG
			SELECT	@sortOrder, @section, ISNULL(COUNT(customer_no), 0), ISNULL(SUM(value), 0), ISNULL(MIN(apply_date), GetDate()), ISNULL(MAX(apply_date), GetDate()), GETDATE(), @runBy
			FROM	LT_NIGHTLY_SUMMARY
			WHERE	section = @section

		-- ---------------------------------------------------------------
		-- Grand Total Payments Totals FY (Current Year - 2)
		-- ---------------------------------------------------------------
		SET	@fym2 = @fy - 2
		SET @section = 'FY' + CONVERT(VARCHAR(4), @fym2) + ' Grand Total Payments'
		SET	@sortOrder = @sortOrder + 1

		SELECT	@fystartm2 = CAST(MIN(start_dt) AS DATE),
				@fyendm2 = CAST(MAX(end_dt) AS DATE)
		FROM	[dbo].[TR_BATCH_PERIOD]
		WHERE	fyear = @fym2

		EXEC LRP_NIGHTLY_SUMM_GRND_TOT_PAY @section, @sortOrder, @runBy, @fym2, @fystartm2, @fyendm2

		INSERT INTO LT_NIGHTLY_SUMMARY_LOG
			SELECT	@sortOrder, @section, ISNULL(COUNT(customer_no), 0), ISNULL(SUM(value), 0), ISNULL(MIN(apply_date), GetDate()), ISNULL(MAX(apply_date), GetDate()), GETDATE(), @runBy
			FROM	LT_NIGHTLY_SUMMARY
			WHERE	section = @section

		-- ---------------------------------------------------------------
		-- Grand Total Payments Totals FY (Current Year - 3)
		-- ---------------------------------------------------------------
		SET	@fym3 = @fy - 3
		SET @section = 'FY' + CONVERT(VARCHAR(4), @fym3) + ' Grand Total Payments'
		SET	@sortOrder = @sortOrder + 1

		SELECT	@fystartm3 = CAST(MIN(start_dt) AS DATE),
				@fyendm3 = CAST(MAX(end_dt) AS DATE)
		FROM	[dbo].[TR_BATCH_PERIOD]
		WHERE	fyear = @fym3

		EXEC LRP_NIGHTLY_SUMM_GRND_TOT_PAY @section, @sortOrder, @runBy, @fym3, @fystartm3, @fyendm3

		INSERT INTO LT_NIGHTLY_SUMMARY_LOG
			SELECT	@sortOrder, @section, ISNULL(COUNT(customer_no), 0), ISNULL(SUM(value), 0), ISNULL(MIN(apply_date), GetDate()), ISNULL(MAX(apply_date), GetDate()), GETDATE(), @runBy
			FROM	LT_NIGHTLY_SUMMARY
			WHERE	section = @section

        -- ---------------------------------------------------------------
		-- Giving Total for Last 365 Days Including Corporate Membership
		-- ---------------------------------------------------------------
		SET @section = 'Last 365 Total Incl Corp Mem'
		SET	@sortOrder = @sortOrder + 1
        
        EXECUTE [dbo].[LRP_NIGHTLY_SUMM_COMP_LAST_365] @section, @sortOrder, @runBy

        INSERT INTO [dbo].[LT_NIGHTLY_SUMMARY_LOG]
        SELECT	@sortOrder, @section, ISNULL(COUNT(customer_no), 0), ISNULL(SUM(value), 0), ISNULL(MIN(apply_date), GetDate()), 
                ISNULL(MAX(apply_date), GetDate()), GETDATE(), @runBy
		FROM	LT_NIGHTLY_SUMMARY
		WHERE	section = @section

		-- ---------------------------------------------------------------
		-- Solicitor Total FY Totals (Current Fiscal Year)
		-- ---------------------------------------------------------------
		-- Service Now INC0118166 - Move solicitor FY## Totals before other solicitor totals
		SET @section = 'Solicitor Total FY' + RIGHT(@fy, 2) + ' Totals'
		SET	@sortOrder = @sortOrder + 1

		EXEC LRP_NIGHTLY_SUMM_SOL_TOTAL @section, @sortOrder, @runBy, @fystart, @fyend

		INSERT INTO LT_NIGHTLY_SUMMARY_LOG
			SELECT	@sortOrder, @section, ISNULL(COUNT(customer_no), 0), ISNULL(SUM(value), 0), ISNULL(MIN(apply_date), GetDate()), ISNULL(MAX(apply_date), GetDate()), GETDATE(), @runBy
			FROM	LT_NIGHTLY_SUMMARY
			WHERE	section = @section

		---- -----------------------------------------------------------------------------
		---- Solicitor Total FY Totals (Current Fiscal Year - 1)
		---- -----------------------------------------------------------------------------
		SET	@fym1 = @fy - 1
		SET @section = 'Solicitor Total FY' + RIGHT(@fym1, 2) + ' Totals'
		SET	@sortOrder = @sortOrder + 1

		SELECT	@fystartm1 = CAST(MIN(start_dt) AS DATE),
				@fyendm1 = CAST(MAX(end_dt) AS DATE)
		FROM	[dbo].[TR_BATCH_PERIOD]
		WHERE	fyear = @fym1

		EXEC LRP_NIGHTLY_SUMM_SOL_TOTAL @section, @sortOrder, @runBy, @fystartm1, @fyendm1

		INSERT INTO LT_NIGHTLY_SUMMARY_LOG
			SELECT	@sortOrder, @section, ISNULL(COUNT(customer_no), 0), ISNULL(SUM(value), 0), ISNULL(MIN(apply_date), GetDate()), ISNULL(MAX(apply_date), GetDate()), GETDATE(), @runBy
			FROM	LT_NIGHTLY_SUMMARY
			WHERE	section = @section

		---- -----------------------------------------------------------------------------
		---- Solicitor Total FY Totals (Current Fiscal Year - 2)
		---- -----------------------------------------------------------------------------
		SET	@fym2 = @fy - 2
		SET @section = 'Solicitor Total FY' + RIGHT(@fym2, 2) + ' Totals'
		SET	@sortOrder = @sortOrder + 1

		SELECT	@fystartm2 = CAST(MIN(start_dt) AS DATE),
				@fyendm2 = CAST(MAX(end_dt) AS DATE)
		FROM	[dbo].[TR_BATCH_PERIOD]
		WHERE	fyear = @fym2

		EXEC LRP_NIGHTLY_SUMM_SOL_TOTAL @section, @sortOrder, @runBy, @fystartm2, @fyendm2

		INSERT INTO LT_NIGHTLY_SUMMARY_LOG
			SELECT	@sortOrder, @section, ISNULL(COUNT(customer_no), 0), ISNULL(SUM(value), 0), ISNULL(MIN(apply_date), GetDate()), ISNULL(MAX(apply_date), GetDate()), GETDATE(), @runBy
			FROM	LT_NIGHTLY_SUMMARY
			WHERE	section = @section

		---- -----------------------------------------------------------------------------
		---- Solicitor Total FY Totals (Current Fiscal Year - 3)
		---- -----------------------------------------------------------------------------
		SET	@fym3 = @fy - 3
		SET @section = 'Solicitor Total FY' + RIGHT(@fym3, 2) + ' Totals'
		SET	@sortOrder = @sortOrder + 1

		SELECT	@fystartm3 = CAST(MIN(start_dt) AS DATE),
				@fyendm3 = CAST(MAX(end_dt) AS DATE)
		FROM	[dbo].[TR_BATCH_PERIOD]
		WHERE	fyear = @fym3

		EXEC LRP_NIGHTLY_SUMM_SOL_TOTAL @section, @sortOrder, @runBy, @fystartm3, @fyendm3

		INSERT INTO LT_NIGHTLY_SUMMARY_LOG
			SELECT	@sortOrder, @section, ISNULL(COUNT(customer_no), 0), ISNULL(SUM(value), 0), ISNULL(MIN(apply_date), GETDATE()), ISNULL(MAX(apply_date), GETDATE()), GETDATE(), @runBy
			FROM	LT_NIGHTLY_SUMMARY
			WHERE	section = @section

        -- ---------------------------------------------------------------
		-- Solicitor Total_CY Washburn Totals (Current)
		-- ---------------------------------------------------------------
		-- SN SCTASK0001710 - No change
		SELECT	@cystart = CONVERT(VARCHAR(4), DATEPART(yy, GetDate())) + '-01-01',
				@cyend = CONVERT(VARCHAR(4), DATEPART(yy, GetDate())) + '-12-31'

		SELECT	@cyear = RIGHT(CONVERT(VARCHAR(4), DATEPART(yy, @cystart)), 2)

		SET @section = 'Solicitor Total CY' + @cyear + ' Washburn Totals'
		SET	@sortOrder = @sortOrder + 1

		EXEC LRP_NIGHTLY_SUMM_WASHBURN @section, @sortOrder, @runBy, @cystart, @cyend

		INSERT INTO LT_NIGHTLY_SUMMARY_LOG
			SELECT	@sortOrder, @section, ISNULL(COUNT(customer_no), 0), ISNULL(SUM(value), 0), ISNULL(MIN(apply_date), GetDate()), ISNULL(MAX(apply_date), GetDate()), GETDATE(), @runBy
			FROM	LT_NIGHTLY_SUMMARY
			WHERE	section = @section

		---- -----------------------------------------------------------------------------
		---- Solicitor Total_CY Washburn Totals (Past Years (controlled by @washburn_yrs))
		---- -----------------------------------------------------------------------------
		-- SN SCTASK0001710 - No change
		
		-- Pull data from 2007 on...
		SET	@wcnt = 1
		SET	@washburn_yrs = DATEDIFF(yy, '01/01/2007', GetDate())

		WHILE @wcnt <= @washburn_yrs
			BEGIN

					SELECT	@cystart = '',
							@cyend = ''

					SELECT	@cystart = CONVERT(VARCHAR(4), DATEPART(yy, GetDate()) - @wcnt) + '-01-01',
							@cyend = CONVERT(VARCHAR(4), DATEPART(yy, GetDate()) - @wcnt) + '-12-31'

					SELECT	@cyear = RIGHT(CONVERT(VARCHAR(4), DATEPART(yy, @cystart)), 2)

					SET @section = 'Solicitor Total CY' + @cyear + ' Washburn Totals'
					SET	@sortOrder = @sortOrder + 1

					EXEC LRP_NIGHTLY_SUMM_WASHBURN @section, @sortOrder, @runBy, @cystart, @cyend

					INSERT INTO LT_NIGHTLY_SUMMARY_LOG
						SELECT	@sortOrder, @section, ISNULL(COUNT(customer_no), 0), ISNULL(SUM(value), 0), ISNULL(MIN(apply_date), GetDate()), ISNULL(MAX(apply_date), GetDate()), GETDATE(), @runBy
						FROM	LT_NIGHTLY_SUMMARY
						WHERE	section = @section

					SELECT	@wcnt = @wcnt + 1

			END

		-- ---------------------------------------------------------------
		-- Solicitor Total_FY Marathon Totals (Current)
		-- ---------------------------------------------------------------
		-- SN SCTASK0001710 - No change
		SELECT	@fyearx = RIGHT(@fy, 2)

		SELECT @section = 'Solicitor Total FY' + CONVERT(VARCHAR(2), @fyearx) + ' Marathon Totals'

		SET	@sortOrder = @sortOrder + 1

		EXEC LRP_NIGHTLY_SUMM_MARATHON @section, @sortOrder, @runBy, @fystart, @fyend

		INSERT INTO LT_NIGHTLY_SUMMARY_LOG
			SELECT	@sortOrder, @section, ISNULL(COUNT(customer_no), 0), ISNULL(SUM(value), 0), ISNULL(MIN(apply_date), GetDate()), ISNULL(MAX(apply_date), GetDate()), GETDATE(), @runBy
			FROM	LT_NIGHTLY_SUMMARY
			WHERE	section = @section

		---- -----------------------------------------------------------------------------
		---- Solicitor Total_FY Marathon Totals (Past Years (controlled by @marathon_yrs))
		---- -----------------------------------------------------------------------------
		-- SN SCTASK0001710 - No change

		-- Pull data from 2010 on...
		SET	@mcnt = 1
		SET	@marathon_yrs = DATEDIFF(yy, '01/01/2010', GetDate()) + 1

		WHILE @mcnt <= @marathon_yrs
			BEGIN

					SELECT @fyx = '', @fystartx = '', @fyendx = '', @fyearx = ''

					SET	@fyx = @fy - @mcnt
					SET	@fyearx = RIGHT(@fyx, 2)
					SET	@sortOrder = @sortOrder + 1

					SELECT	@fystartx = CAST(MIN(start_dt) AS DATE),
							@fyendx = CAST(MAX(end_dt) AS DATE)
					FROM	[dbo].[TR_BATCH_PERIOD]
					WHERE	fyear = @fyx

					SET @section = 'Solicitor Total FY' +  CONVERT(VARCHAR(2), @fyearx) + ' Marathon Totals'
					SET	@sortOrder = @sortOrder + 1

					--SELECT 'debug', @mcnt AS 'mcnt', @marathon_yrs AS 'marathon_yrs', @fyx AS 'fyx', @fyearx AS 'fyearx', @fystartx AS 'fystartx', @fyendx AS 'fyendx', @section AS 'section', @sortOrder AS 'sortOrder'

					EXEC LRP_NIGHTLY_SUMM_MARATHON @section, @sortOrder, @runBy, @fystartx, @fyendx

					INSERT INTO LT_NIGHTLY_SUMMARY_LOG
						SELECT	@sortOrder, @section, ISNULL(COUNT(customer_no), 0), ISNULL(SUM(value), 0), ISNULL(MIN(apply_date), GetDate()), ISNULL(MAX(apply_date), GetDate()), GETDATE(), @runBy
						FROM	LT_NIGHTLY_SUMMARY
						WHERE	section = @section

					SELECT	@mcnt = @mcnt + 1

			END

		---- -----------------------------------------------------------------------------
		---- Solicitor Total FY## Run Program Totals
		---- -----------------------------------------------------------------------------
		-- Service Now INC0118166 - New section added
		SET	@rpcnt = 0
		SET	@run_program_yrs = DATEDIFF(yy, '01/01/2010', GetDate()) + 1

		WHILE @rpcnt <= @run_program_yrs
		BEGIN

			SELECT @fyx = '',
				   @fystartx = '',
				   @fyendx = '',
				   @fyearx = '';

			SET @fyx = @fy - @rpcnt;
			SET @fyearx = RIGHT(@fyx, 2);
			SET @sortOrder = @sortOrder + 1;

			SELECT @fystartx = CAST(MIN(start_dt) AS DATE),
				   @fyendx = CAST(MAX(end_dt) AS DATE)
			FROM [dbo].[TR_BATCH_PERIOD]
			WHERE fyear = @fyx;

			SET @section = 'Solicitor Total FY' + CONVERT(VARCHAR(2), @fyearx) + ' Run Program Totals';
			SET @sortOrder = @sortOrder + 1;

			EXEC LRP_NIGHTLY_SUMM_SOL_RUN_PROGRAM_TOTAL @section, @sortOrder, @runBy, @fystartx, @fyendx;

			IF (SELECT ISNULL(SUM(value), 0) FROM LT_NIGHTLY_SUMMARY WHERE section = @section) > 0
				BEGIN

				INSERT INTO LT_NIGHTLY_SUMMARY_LOG
					SELECT @sortOrder,
						   @section,
						   ISNULL(COUNT(customer_no), 0),
						   ISNULL(SUM(value), 0),
						   ISNULL(MIN(apply_date), GETDATE()),
						   ISNULL(MAX(apply_date), GETDATE()),
						   GETDATE(),
						   @runBy
					FROM LT_NIGHTLY_SUMMARY
					WHERE section = @section;

				END

			SELECT @rpcnt = @rpcnt + 1;

		END;

		-- ---------------------------------------------------------------
		-- Return all rows (for testing only)
		-- ---------------------------------------------------------------
		--SELECT  customer_no,
		--		section,
		--		value,
		--		apply_date,
		--		sort_order,
		--		created_on,
		--		created_by 
		--FROM LT_NIGHTLY_SUMMARY

		--SELECT  sort_order,
		--		section,
		--		ISNULL(total_recs, 0) AS total_recs,
		--		ISNULL(total_val, 0.00) AS total_val,
		--		ISNULL(CONVERT(VARCHAR(10), min_date), '') AS min_date,
		--		ISNULL(CONVERT(VARCHAR(10), max_date), '') AS max_date,
		--		created_on,
		--		created_by 
		--FROM	LT_NIGHTLY_SUMMARY_LOG
		
	END TRY

	BEGIN CATCH

		INSERT INTO LT_NIGHTLY_SUMMARY_LOG
			SELECT	0,
					'Failed on ' + @section,
					0,
					0,
					'',
					'',
					GETDATE(),
					@RunBy

		--SELECT  sort_order,
		--		section,
		--		ISNULL(total_recs, 0) AS total_recs,
		--		ISNULL(total_val, 0.00) AS total_val,
		--		ISNULL(CONVERT(VARCHAR(10), min_date), '') AS min_date,
		--		ISNULL(CONVERT(VARCHAR(10), max_date), '') AS max_date,
		--		created_on,
		--		created_by 
		--FROM LT_NIGHTLY_SUMMARY_LOG

	END CATCH

END



GO


