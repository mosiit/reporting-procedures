USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LRP_LIBRARY_PASS_FULFILLMENT]    Script Date: 6/22/2016 2:19:06 PM ******/
DROP PROCEDURE [dbo].[LRP_LIBRARY_PASS_FULFILLMENT]
GO

/****** Object:  StoredProcedure [dbo].[LRP_LIBRARY_PASS_FULFILLMENT]    Script Date: 6/22/2016 2:19:06 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[LRP_LIBRARY_PASS_FULFILLMENT]
	@expiration_dt DATETIME
AS

	BEGIN

		SELECT	cus.customer_no AS library_id,
				cus.lname AS library_name,
				adr.[street3] AS library_care_of_name,
				adr.[street1] AS library_address_1,
				adr.[street2] AS library_address_2,
				adr.city AS library_city,
				adr.[state] AS library_state,
				dbo.AF_FORMAT_STRING(adr.postal_code, '@@@@@-@@@@') AS library_postal_code,
				dbo.AF_FORMAT_STRING(pho.phone, '@@@-@@@-@@@@') AS library_phone, 
				cm.memb_amt AS library_membership_paid_amount,
				CASE cm.NRR_status
					WHEN 'NE' THEN 'New'
					WHEN 'RN' THEN 'Renewed'
					WHEN 'RI' THEN 'Reinstated'
				END AS library_membership_status,
				CONVERT(VARCHAR(10), cm.init_dt, 101) AS library_membership_initial_date,
				CONVERT(VARCHAR(10), cm.expr_dt, 101) AS library_membership_expiration_date,
				CASE DATEPART(mm, cm.expr_dt) 
					WHEN 1 THEN 'January'
					WHEN 2 THEN 'February'
					WHEN 3 THEN 'March'
					WHEN 4 THEN 'April'
					WHEN 5 THEN 'May'
					WHEN 6 THEN 'June'
					WHEN 7 THEN 'July'
					WHEN 8 THEN 'August'
					WHEN 9 THEN 'September'
					WHEN 10 THEN 'October'
					WHEN 11 THEN 'November'
					WHEN 12 THEN 'December'
					ELSE 'Unknown'
				END AS library_expiration_month_name
		FROM	dbo.T_CUSTOMER cus
		JOIN	dbo.T_ADDRESS adr
		ON		cus.customer_no = adr.customer_no
		JOIN	dbo.TR_ADDRESS_TYPE adt
		ON		adt.[id] = adr.address_type
		JOIN	dbo.T_PHONE pho
		ON		cus.customer_no = pho.customer_no
		JOIN    dbo.TX_CUST_MEMBERSHIP cm
		ON		cm.customer_no = cus.customer_no
		JOIN    dbo.T_MEMB_LEVEL ml1 
		ON		cm.memb_org_no = ml1.memb_org_no 
		AND		cm.memb_level = ml1.memb_level
		JOIN    dbo.TR_CURRENT_STATUS cs 
		ON		cm.current_status = cs.id
		WHERE	adt.[description] = 'Business'
		AND		ml1.memb_level = 'L'
		AND		cs.[description] = 'Active'
		AND		adr.[inactive] = 'N'
		AND		(DATEPART(yyyy, cm.expr_dt) = DATEPART(yyyy, @expiration_dt)
		AND		 DATEPART(mm, cm.expr_dt) = DATEPART(mm, @expiration_dt))

	END

GO


