USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_TPP_MEMBER_LIST_EXCEPTIONS]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_TPP_MEMBER_LIST_EXCEPTIONS] AS' 
END
GO

ALTER PROCEDURE [dbo].[LRP_TPP_MEMBER_LIST_EXCEPTIONS]
        @active_only CHAR(1) = 'N',
        @exceptions_only CHAR(1) = 'N',
        @exclude_empty_barcode CHAR(1) = 'N'
AS BEGIN

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    SET NOCOUNT ON;

    IF 1 = 2 BEGIN
        SELECT CAST(NULL AS DATETIME) AS [run_dt], CAST(NULL AS INT) AS [school_no], CAST(NULL AS varchar(100)) AS [school_name], 
               CAST(NULL AS INT) AS [teacher_no],  CAST(NULL AS VARCHAR(50)) AS [teacher_first_name], CAST(NULL AS VARCHAR(50)) AS [teacher_last_name], 
               CAST(NULL AS VARCHAR(100)) AS [teacher_email], CAST(NULL AS VARCHAR(100)) AS [school_address_1], CAST(NULL AS VARCHAR(100)) AS [school_address_2], 
               CAST(NULL AS VARCHAR(50)) AS [school_city], CAST(NULL AS varchar(50)) AS [school_state], CAST(NULL AS VARCHAR(50)) AS [school_postal_code], 
               CAST(NULL AS varchar(1000)) AS [grades_taught], CAST(NULL AS VARCHAR(1000)) AS [subjects_taught], CAST(NULL AS int) AS [memb_no], 
               CAST(NULL AS VARCHAR(4)) AS [memb_pin], CAST(NULL AS VARCHAR(50)) AS [memb_status], CAST(NULL AS CHAR(10)) AS [memb_expire_dt], 
               CAST(NULL AS VARCHAR(50)) AS [memb_level], CAST(NULL AS VARCHAR(25)) AS [memb_card_barcode], CAST(NULL AS VARCHAR(25)) AS [memb_card_barcode_with_pin],
               CAST(NULL AS INT) AS [exception_count], CAST(NULL AS INT) AS [exception_order], CAST(NULL AS VARCHAR(125)) AS [exception_type]
    END

    /*  Temporary Tables  */
    
        IF OBJECT_ID('tempdb..#tpp_info') IS NOT NULL DROP TABLE [#tpp_info]
        
        CREATE TABLE [#tpp_info] ([run_dt] DATETIME, [school_no] INT, [school_name] VARCHAR(100), [teacher_no] INT, [teacher_first_name] VARCHAR(50), 
                                  [teacher_last_name] VARCHAR(50), [teacher_email] VARCHAR(100), [school_address_1] VARCHAR(100), [school_address2] VARCHAR(100), 
                                  [school_city] VARCHAR(50), [school_state] VARCHAR(50), [school_postal_code] VARCHAR(50), [grades_taught] VARCHAR(1000), 
                                  [subjects_taught] VARCHAR(1000), [memb_no] INT, [memb_pin] VARCHAR(4), [memb_status] VARCHAR(50), [memb_expire_dt] DATETIME, 
                                  [memb_level] VARCHAR(50), [memb_card_barcode] VARCHAR(25), [exception_count] INT, [exception_order] INT, [exception_type] VARCHAR(125));

        IF OBJECT_ID('tempdb..#tpp_multiple') IS NOT NULL DROP TABLE [#tpp_multiple];

        CREATE TABLE [#tpp_multiple] ([teacher_no] INT, [teacher_first_name] VARCHAR(50), [teacher_last_name] VARCHAR(50), [mem_count] INT);


    /*  Execute the LRP_TPP_MEMBER_LIST procedure to get the current TPP Member List  */

        INSERT INTO [#tpp_info] ([run_dt], [school_no], [school_name], [teacher_no], [teacher_first_name], [teacher_last_name], [teacher_email], 
                                 [school_address_1], [school_address2], [school_city], [school_state], [school_postal_code], [grades_taught], 
                                 [subjects_taught], [memb_no], [memb_pin], [memb_status], [memb_expire_dt], [memb_level], [memb_card_barcode])
        EXECUTE [dbo].[LRP_TPP_MEMBER_LIST] @active_only;

    /*  Default the exception count and exception order on each record to zero  */
        UPDATE [#tpp_info] SET [exception_count] = 0,
                               [exception_order] = 0;

    /*  Exception Search #1 - Records with missing email - order = 3  */

        UPDATE [#tpp_info]
        SET [exception_type] = ISNULL([exception_type],'') 
                             + CASE WHEN ISNULL([exception_type],'') <> '' THEN '/' ELSE '' END 
                             + 'Missing Email Address',
            [exception_order] = CASE WHEN [exception_order] = 0 THEN 3 ELSE [exception_order] END,
            [exception_count] = ([exception_count] + 1)
        WHERE [teacher_email] = '';

    /*  Exception Search #2 - Records without a membership card PIN - order = 5  */

        UPDATE [#tpp_info]
        SET [exception_type] = ISNULL([exception_type],'') 
                             + CASE WHEN ISNULL([exception_type],'') <> '' THEN '/' ELSE '' END 
                             + 'Missing Card PIN',
            [exception_order] = CASE WHEN [exception_order] = 0 THEN 5 ELSE [exception_order] END,
            [exception_count] = ([exception_count] + 1)
        WHERE [memb_pin] = '';

    /*  Exception Search #3 - Records missing a school name - order = 7  */

        UPDATE [#tpp_info]
        SET [exception_type] = ISNULL([exception_type],'') 
                             + CASE WHEN ISNULL([exception_type],'') <> '' THEN '/' ELSE '' END 
                             + 'Missing School Name',
            [exception_order] = CASE WHEN [exception_order] = 0 THEN 7 ELSE [exception_order] END,
            [exception_count] = ([exception_count] + 1)
        WHERE [school_name] = '';

    /*  Exception Search #4 - Records missing school address information -  order = 9  */

        UPDATE [#tpp_info]
        SET [exception_type] = ISNULL([exception_type],'') 
                             + CASE WHEN ISNULL([exception_type],'') <> '' THEN '/' ELSE '' END 
                             + 'Missing School Address Info',
            [exception_order] = CASE WHEN [exception_order] = 0 THEN 9 ELSE [exception_order] END,
            [exception_count] = ([exception_count] + 1)
        WHERE [school_address_1] = '' OR [school_city] = '' OR [school_state] = '' OR [school_postal_code] = '';

    /*  Exception Search #5 - Records with missing Grades Taught - order = 11  */

        UPDATE [#tpp_info]
        SET [exception_type] = ISNULL([exception_type],'') 
                             + CASE WHEN ISNULL([exception_type],'') <> '' THEN '/' ELSE '' END 
                             + 'Missing Grades',
            [exception_order] = CASE WHEN [exception_order] = 0 THEN 11 ELSE [exception_order] END,
            [exception_count] = ([exception_count] + 1)
        WHERE [grades_taught] = '';

    /*  Exception Search #6 - Records with Missing Subjects Taught - order = 13  */

    UPDATE [#tpp_info]
    SET [exception_type] = ISNULL([exception_type],'') 
                         + CASE WHEN ISNULL([exception_type],'') <> '' THEN '/' ELSE '' END 
                         + 'Missing Subjects',
        [exception_order] = CASE WHEN [exception_order] = 0 THEN 13 ELSE [exception_order] END,
        [exception_count] = ([exception_count] + 1)
    WHERE [subjects_taught] = '';

    /*  Exception Search # 7 - Teachers who have multiple active TPP memberships
        If found, the order is set to 1 so that they sort to the top of the list  */

        INSERT INTO [#tpp_multiple] ([teacher_no], [teacher_first_name], [teacher_last_name], [mem_count])
        SELECT mem.[customer_no], cus.[fname], cus.[lname], COUNT(*)
        FROM [dbo].[TX_CUST_MEMBERSHIP] AS mem 
             LEFT OUTER JOIN [dbo].[T_CUSTOMER] AS cus ON cus.[customer_no] = mem.[customer_no]
        WHERE mem.memb_org_no = 13 AND mem.current_status = 2
        GROUP BY mem.[customer_no], cus.[fname], cus.[lname]
        HAVING COUNT(*) > 1

        UPDATE[#tpp_info] 
        SET [exception_type] = 'Multiple Active Memberships ('
                             + (SELECT CONVERT(VARCHAR(5),[mem_count]) FROM [#tpp_multiple] AS mtp WHERE [#tpp_info].[teacher_no] = mtp.[teacher_no])
                             + ')' + CASE WHEN ISNULL([exception_type],'') <> '' THEN '/' ELSE '' END + ISNULL(exception_type,''),
            [exception_order] = 1,
            [exception_count] = ([exception_count] + 1)
        WHERE [teacher_no] IN (SELECT [teacher_no] FROM [#tpp_multiple]);


    /*  On any remaining records, the order is set to 99 so that those records sort to the bottom of the list  */

        UPDATE [#tpp_info]
        SET [exception_type] = 'No Exceptions Found',
            [exception_order] = 99
        WHERE[exception_order] = 0

    /*  Delete Records With No Bar Code  (if requested)  */

        IF @exclude_empty_barcode = 'Y'
            DELETE FROM [#tpp_info] 
            WHERE memb_card_barcode = ''
    

    FINISHED:
    
    /*  Select final data set - ORDER BY clause added so that when writing directly to a csv file from SSIS, 
        the records write to the file in the proper sort order  */

        SELECT CONVERT(VARCHAR(25),FORMAT([run_dt],'MMM dd, yyyy HH:mm:ss')) AS [run_dt],
               [exception_count],
               [exception_order],
               [exception_type],
               [school_no], 
               [school_name], 
               [teacher_no], 
               [teacher_first_name], 
               [teacher_last_name], 
               [teacher_email], 
               [school_address_1],
               [school_address2], 
               [school_city], 
               [school_state], 
               [school_postal_code], 
               [grades_taught], 
               [subjects_taught], 
               [memb_no],
               [memb_pin], 
               [memb_status], 
               CONVERT(CHAR(10),[memb_expire_dt],111) AS [memb_expire_dt], 
               [memb_level], 
               LEFT([memb_card_barcode],10) AS [memb_card_barcode],
               [memb_card_barcode] AS [memb_card_barcode_with_pin],
               'Teacher Partner' AS [patron_type],
               'ERC' AS [short_site]
        FROM #tpp_info
        WHERE (@exceptions_only = 'N' OR [exception_count] > 0)
        ORDER BY [exception_order],
                 [exception_count],
                 [exception_type],
                 [school_name],
                 [teacher_last_name],
                 [teacher_first_name]

    DONE:

        IF OBJECT_ID('tempdb..#tpp_info') IS NOT NULL DROP TABLE [#tpp_info]

        IF OBJECT_ID('tempdb..#tpp_multiple') IS NOT NULL DROP TABLE [#tpp_multiple];

END
GO

GRANT EXECUTE ON [dbo].[LRP_TPP_MEMBER_LIST_EXCEPTIONS] TO ImpUsers
GO

EXECUTE [dbo].[LRP_TPP_MEMBER_LIST_EXCEPTIONS] @active_only = 'N', @exceptions_only = 'N', @exclude_empty_barcode = 'Y'        

