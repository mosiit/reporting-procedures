USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LRP_ADMITS_CASHIER_RPT_PAYMENTS]    Script Date: 5/9/2016 1:50:31 PM ******/
DROP PROCEDURE [dbo].[LRP_ADMITS_CASHIER_RPT_PAYMENTS]
GO

/****** Object:  StoredProcedure [dbo].[LRP_ADMITS_CASHIER_RPT_PAYMENTS]    Script Date: 5/9/2016 1:50:31 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LRP_ADMITS_CASHIER_RPT_PAYMENTS]
       @rpt_dt DATETIME,
       @rpt_operator VARCHAR(8),
	   @rpt_location VARCHAR(30)
AS 
	BEGIN

	   SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	   SET NOCOUNT ON

       DECLARE @tblPays TABLE ([payment_date] CHAR(10),
                               [payment_operator] VARCHAR(8),
                               [operator_first] VARCHAR(50),
                               [operator_last] VARCHAR(50),
                               [operator_location] VARCHAR(50),
                               [payment_method_name] VARCHAR(30),
                               [payment_type_name] VARCHAR(30),
                               [payment_count] INT,
                               [payment_amount] DECIMAL(18, 2),
                               [elapsed_time] INT,
                               [rpt_message] VARCHAR(100))

       DECLARE @tblRecordedAmts TABLE ([Date] CHAR(10),
                                       [Operator] VARCHAR(8),
                                       [Accts Rec] DECIMAL(18, 2),
                                       [Cash] DECIMAL(18, 2),
                                       [Check] DECIMAL(18, 2),
                                       [Credit Card] DECIMAL(18, 2),
                                       [Gift Certificate] DECIMAL(18, 2),
                                       [Refund To Check] DECIMAL(18, 2),
                                       [Travelers Check] DECIMAL(18, 2),
									   [Invoice] DECIMAL(18, 2),
                                       [Returns] DECIMAL(18, 2),
                                       [Refunds] DECIMAL(18, 2),
                                       [Cancel Sales] DECIMAL(18, 2),
                                       [Drawer Total] DECIMAL(18, 2))

       DECLARE @tblRecordedCnts TABLE ([Date] CHAR(10),
                                       [Operator] VARCHAR(8),
                                       [Accts Rec] DECIMAL(18, 2),
                                       [Cash] DECIMAL(18, 2),
                                       [Check] DECIMAL(18, 2),
                                       [Credit Card] DECIMAL(18, 2),
                                       [Gift Certificate] DECIMAL(18, 2),
                                       [Refund To Check] DECIMAL(18, 2),
                                       [Travelers Check] DECIMAL(18, 2),
									   [Invoice] DECIMAL(18, 2),
                                       [Returns] DECIMAL(18, 2),
                                       [Refunds] DECIMAL(18, 2),
                                       [Cancel Sales] DECIMAL(18, 2),
                                       [Drawer Total] DECIMAL(18, 2))


       DECLARE @tblActualAmts TABLE ([Date] CHAR(10),
                                     [Operator] VARCHAR(8),
                                     [Accts Rec] DECIMAL(18, 2),
                                     [Cash] DECIMAL(18, 2),
                                     [Check] DECIMAL(18, 2),
                                     [Credit Card] DECIMAL(18, 2),
                                     [Gift Certificate] DECIMAL(18, 2),
                                     [Refund To Check] DECIMAL(18, 2),
                                     [Travelers Check] DECIMAL(18, 2),
									 [Invoice] DECIMAL(18, 2),
                                     [Returns] DECIMAL(18, 2),
                                     [Refunds] DECIMAL(18, 2),
                                     [Cancel Sales] DECIMAL(18, 2),
                                     [Drawer Total] DECIMAL(18, 2))

       DECLARE @tblActualCnts TABLE ([Date] CHAR(10),
                                     [Operator] VARCHAR(8),
                                     [Accts Rec] DECIMAL(18, 2),
                                     [Cash] DECIMAL(18, 2),
                                     [Check] DECIMAL(18, 2),
                                     [Credit Card] DECIMAL(18, 2),
                                     [Gift Certificate] DECIMAL(18, 2),
                                     [Refund To Check] DECIMAL(18, 2),
                                     [Travelers Check] DECIMAL(18, 2),
									 [Invoice] DECIMAL(18, 2),
                                     [Returns] DECIMAL(18, 2),
                                     [Refunds] DECIMAL(18, 2),
                                     [Cancel Sales] DECIMAL(18, 2),
                                     [Drawer Total] DECIMAL(18, 2))

						
       INSERT   INTO @tblPays
                EXEC [dbo].[LP_RPT_CASHOUT_PAYMENTS] @rpt_dt, @rpt_operator

       INSERT   INTO @tblRecordedAmts
                SELECT  payment_date AS 'Date',
                        payment_operator AS 'Operator',
                        ISNULL([Accts Rec], 0) AS [Accts Rec],
                        ISNULL([Cash], 0) AS [Cash],
                        ISNULL([Check], 0) AS [Check],
                        ISNULL([Credit Card], 0) AS [Credit Card],
                        ISNULL([Gift Certificate], 0) AS [Gift Certificate],
                        ISNULL([Refund To Check], 0) AS [Refund To Check],
                        ISNULL([Travelers Check], 0) AS [Travelers Check],
						ISNULL([Invoice], 0) AS [Invoice],
                        ISNULL([Returns], 0) AS [Returns],
                        ISNULL([Refunds], 0) AS [Refunds],
                        ISNULL([Cancel Sales], 0) AS [Cancel Sales],
                        (ISNULL([Accts Rec], 0) + ISNULL([Cash], 0) + ISNULL([Check], 0) + ISNULL([Credit Card], 0) + ISNULL([Gift Certificate], 0) + ISNULL([Refund To Check], 0) + ISNULL([Travelers Check], 0) + ISNULL([Invoice], 0)) - (ISNULL([Returns], 0) + ISNULL([Refunds], 0) + ISNULL([Cancel Sales], 0)) AS [Drawer Total]
                FROM    (SELECT payment_date,
                                payment_operator,
                                payment_type_name,
                                [payment_amount]
                         FROM   @tblPays
						 WHERE	[operator_location] = @rpt_location) AS SourceTable PIVOT
	( SUM([payment_amount]) FOR payment_type_name IN ([Accts Rec], [Cash], [Check], [Credit Card], [Gift Certificate], [Refund To Check], [Travelers Check], [Invoice], [Returns], [Refunds], [Cancel Sales], [Drawer Total]) ) AS PivotTable; 

       INSERT   INTO @tblRecordedCnts
                SELECT  payment_date AS 'Date',
                        payment_operator AS 'Operator',
                        ISNULL([Accts Rec], 0) AS [Accts Rec],
                        ISNULL([Cash], 0) AS [Cash],
                        ISNULL([Check], 0) AS [Check],
                        ISNULL([Credit Card], 0) AS [Credit Card],
                        ISNULL([Gift Certificate], 0) AS [Gift Certificate],
                        ISNULL([Refund To Check], 0) AS [Refund To Check],
                        ISNULL([Travelers Check], 0) AS [Travelers Check],
						ISNULL([Invoice], 0) AS [Invoice],
                        ISNULL([Returns], 0) AS [Returns],
                        ISNULL([Refunds], 0) AS [Refunds],
                        ISNULL([Cancel Sales], 0) AS [Cancel Sales],
                        (ISNULL([Accts Rec], 0) + ISNULL([Cash], 0) + ISNULL([Check], 0) + ISNULL([Credit Card], 0) + ISNULL([Gift Certificate], 0) + ISNULL([Refund To Check], 0) + ISNULL([Travelers Check], 0) + ISNULL([Invoice], 0)) - (ISNULL([Returns], 0) + ISNULL([Refunds], 0) + ISNULL([Cancel Sales], 0)) AS [Drawer Total]
                FROM    (SELECT payment_date,
                                payment_operator,
                                payment_type_name,
                                [payment_count]
                         FROM   @tblPays
						 WHERE	[operator_location] = @rpt_location) AS SourceTable PIVOT
	( SUM([payment_count]) FOR payment_type_name IN ([Accts Rec], [Cash], [Check], [Credit Card], [Gift Certificate], [Refund To Check], [Travelers Check], [Invoice], [Returns], [Refunds], [Cancel Sales], [Drawer Total]) ) AS PivotTable; 

       INSERT   INTO @tblActualAmts
                SELECT  payment_date AS 'Date',
                        payment_operator AS 'Operator',
                        ISNULL([Accts Rec], 0) AS [Accts Rec],
                        ISNULL([Cash], 0) AS [Cash],
                        ISNULL([Check], 0) AS [Check],
                        ISNULL([Credit Card], 0) AS [Credit Card],
                        ISNULL([Gift Certificate], 0) AS [Gift Certificate],
                        ISNULL([Refund To Check], 0) AS [Refund To Check],
                        ISNULL([Travelers Check], 0) AS [Travelers Check],
						ISNULL([Invoice], 0) AS [Invoice],
                        ISNULL([Returns], 0) AS [Returns],
                        ISNULL([Refunds], 0) AS [Refunds],
                        ISNULL([Cancel Sales], 0) AS [Cancel Sales],
                        (ISNULL([Accts Rec], 0) + ISNULL([Cash], 0) + ISNULL([Check], 0) + ISNULL([Credit Card], 0) + ISNULL([Gift Certificate], 0) + ISNULL([Refund To Check], 0) + ISNULL([Travelers Check], 0) + ISNULL([Invoice], 0)) - (ISNULL([Returns], 0) + ISNULL([Refunds], 0) + ISNULL([Cancel Sales], 0)) AS [Drawer Total]
                FROM    (SELECT payment_date,
                                payment_operator,
                                payment_type_name,
                                [payment_amount]
                         FROM   [dbo].[LT_CASH_OUT_DATA]
                         WHERE  payment_date = @rpt_dt
                                AND payment_operator = @rpt_operator
								AND payment_operator_location = @rpt_location) AS SourceTable PIVOT
	( SUM([payment_amount]) FOR payment_type_name IN ([Accts Rec], [Cash], [Check], [Credit Card], [Gift Certificate], [Refund To Check], [Travelers Check], [Invoice], [Returns], [Refunds], [Cancel Sales], [Drawer Total]) ) AS PivotTable; 

	-- Check for coupons if no payments
	IF (SELECT COUNT([Operator]) FROM @tblRecordedAmts) = 0
		BEGIN
			IF (SELECT COUNT(sli_no) FROM [dbo].[LV_RPT_CASHOUT_COUPONS] WHERE CONVERT(VARCHAR(10), coupon_dt, 111) = CONVERT(VARCHAR(10), @rpt_dt, 111) AND coupon_operator = @rpt_operator AND coupon_operator_location = @rpt_location) > 0
				SELECT	TOP 1 'CoupsOnly' AS [Date],
						[coupon_operator] AS [Operator],
						[coupon_operator_first_name] AS [Operator First Name],
						[coupon_operator_last_name] AS [Operator Last Name],
						[coupon_operator_location] AS [Operator Location],
						0 AS [Recorded Accts Rec],
						0 AS [Actual Accts Rec],
						0 AS [Variance Accts Rec],
						0 AS [Recorded Cash],
						0 AS [Actual Cash],
						0 AS [Variance Cash],
						0 AS [Recorded Check],
						0 AS [Actual Check],
						0 AS [Variance Check],
						0 AS [Recorded Credit Card],
						0 AS [Actual Credit Card],
						0 AS [Variance Credit Card],
						0 AS [Recorded Gift Certificate],
						0 AS [Actual Gift Certificate],
						0 AS [Variance Gift Certificate],
						0 AS [Recorded Refund To Check],
						0 AS [Actual Refund To Check],
						0 AS [Variance Refund To Check],
						0 AS [Recorded Travelers Check],
						0 AS [Actual Travelers Check],
						0 AS [Variance Travelers Check],
						0 AS [Recorded Invoice],
						0 AS [Actual Invoice],
						0 AS [Variance Invoice],
						0 AS [Recorded Returns],
						0 AS [Actual Returns],
						0 AS [Variance Returns],
						0 AS [Recorded Refunds],
						0 AS [Actual Refunds],
						0 AS [Variance Refunds],
						0 AS [Recorded Cancel Sales],
						0 AS [Actual Cancel Sales],
						0 AS [Variance Cancel Sales],
						0 AS [Recorded Drawer Total],
						0 AS [Actual Drawer Total],
						0 AS [Variance Drawer Total]
				FROM	[LV_RPT_CASHOUT_COUPONS]
				WHERE	CONVERT(VARCHAR(10), coupon_dt, 111) = CONVERT(VARCHAR(10), @rpt_dt, 111) 
				AND		coupon_operator = @rpt_operator 
				AND		coupon_operator_location = @rpt_location
		END
	ELSE
       SELECT   DISTINCT ra.[Date],
                ra.[Operator],
				p. [operator_first] AS [Operator First Name],
				p.[operator_last] AS [Operator Last Name],
                p.[operator_location] AS [Operator Location],
                ISNULL(ra.[Accts Rec], 0) AS [Recorded Accts Rec],
                ISNULL(aa.[Accts Rec], 0) AS [Actual Accts Rec],
                ISNULL(aa.[Accts Rec], 0) - ISNULL(ra.[Accts Rec], 0) AS [Variance Accts Rec],
                ISNULL(ra.[Cash], 0) AS [Recorded Cash],
                ISNULL(aa.[Cash], 0) AS [Actual Cash],
                ISNULL(aa.[Cash], 0) - ISNULL(ra.[Cash], 0) AS [Variance Cash],
                ISNULL(ra.[Check], 0) AS [Recorded Check],
                ISNULL(aa.[Check], 0) AS [Actual Check],
                ISNULL(aa.[Check], 0) - ISNULL(ra.[Check], 0) AS [Variance Check],
                ISNULL(ra.[Credit Card], 0) AS [Recorded Credit Card],
                ISNULL(aa.[Credit Card], 0) AS [Actual Credit Card],
                ISNULL(aa.[Credit Card], 0) - ISNULL(ra.[Credit Card], 0) AS [Variance Credit Card],
                ISNULL(ra.[Gift Certificate], 0) AS [Recorded Gift Certificate],
                ISNULL(aa.[Gift Certificate], 0) AS [Actual Gift Certificate],
                ISNULL(aa.[Gift Certificate], 0) - ISNULL(ra.[Gift Certificate], 0) AS [Variance Gift Certificate],
                ISNULL(ra.[Refund To Check], 0) AS [Recorded Refund To Check],
                ISNULL(aa.[Refund To Check], 0) AS [Actual Refund To Check],
                ISNULL(aa.[Refund To Check], 0) - ISNULL(ra.[Refund To Check], 0) AS [Variance Refund To Check],
                ISNULL(ra.[Travelers Check], 0) AS [Recorded Travelers Check],
                ISNULL(aa.[Travelers Check], 0) AS [Actual Travelers Check],
                ISNULL(aa.[Travelers Check], 0) - ISNULL(ra.[Travelers Check], 0) AS [Variance Travelers Check],
                ISNULL(ra.[Invoice], 0) AS [Recorded Invoice],
                ISNULL(aa.[Invoice], 0) AS [Actual Invoice],
                ISNULL(aa.[Invoice], 0) - ISNULL(ra.[Invoice], 0) AS [Variance Invoice],
                ISNULL(ra.[Returns], 0) AS [Recorded Returns],
                ISNULL(aa.[Returns], 0) AS [Actual Returns],
                ISNULL(aa.[Returns], 0) - ISNULL(ra.[Returns], 0) AS [Variance Returns],
                ISNULL(ra.[Refunds], 0) AS [Recorded Refunds],
                ISNULL(aa.[Refunds], 0) AS [Actual Refunds],
                ISNULL(aa.[Refunds], 0) - ISNULL(ra.[Refunds], 0) AS [Variance Refunds],
                ISNULL(ra.[Cancel Sales], 0) AS [Recorded Cancel Sales],
                ISNULL(aa.[Cancel Sales], 0) AS [Actual Cancel Sales],
                ISNULL(aa.[Cancel Sales], 0) - ISNULL(ra.[Cancel Sales], 0) AS [Variance Cancel Sales],
                ISNULL(ra.[Drawer Total], 0) AS [Recorded Drawer Total],
                ISNULL(aa.[Drawer Total], 0) AS [Actual Drawer Total],
                ISNULL(aa.[Drawer Total], 0) - ISNULL(ra.[Drawer Total], 0) AS [Variance Drawer Total]
       FROM     @tblRecordedAmts ra
	   JOIN		@tblPays p
	   ON		ra.[Operator] = p.[payment_operator]
       LEFT OUTER JOIN @tblRecordedCnts rc ON ra.[Date] = rc.[Date]
                                              AND ra.[Operator] = rc.[Operator]
       LEFT OUTER JOIN @tblActualAmts aa ON ra.[Date] = aa.[Date]
                                            AND ra.[Operator] = aa.[Operator]

	END
GO

GRANT EXECUTE ON [dbo].[LRP_ADMITS_CASHIER_RPT_PAYMENTS] TO impusers
GO
