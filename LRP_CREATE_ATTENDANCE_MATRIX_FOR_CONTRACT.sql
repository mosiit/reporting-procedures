USE [impresario]
GO
/****** Object:  StoredProcedure [dbo].[LRP_CREATE_ATTENDANCE_MATRIX_FOR_CONTRACT]    Script Date: 10/1/2021 11:52:54 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[LRP_CREATE_ATTENDANCE_MATRIX_FOR_CONTRACT]
        @contract_id INT = NULL
AS BEGIN

    SET NOCOUNT ON;
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
       
    /*  Procedure Variables  */
    
        DECLARE @title_no INT = 0, @title_name VARCHAR(30) = '', @prod_no INT = 0, @prod_name VARCHAR(30) = '';
        DECLARE @start_dt DATE, @end_dt DATE, @day_count INT = 0;

        DECLARE @report_start_dt DATETIME = NULL, @report_end_dt DATETIME = NULL;
        DECLARE @public_prod_no INT = 0, @school_prod_no INT = 0;

        DECLARE @data_dt DATETIME = GETDATE();

    /*  Temporary Tables  */

        IF OBJECT_ID('tempdb..#contract_matrix_raw') is not null DROP TABLE [#contract_matrix_raw];

        CREATE TABLE [#contract_matrix_raw] ([prod_no] INT NOT NULL DEFAULT (0),
                                         [prod_name] VARCHAR(50) NOT NULL DEFAULT (0),
                                         [perf_date] DATE NULL,
                                         [perf_time] VARCHAR(10) NOT NULL DEFAULT (''),
                                         [perf_dt] DATETIME NULL,
                                         [attendance_type] VARCHAR(30) NOT NULL DEFAULT ('Public'),
                                         [sale_total] INT NOT NULL DEFAULT (1),
                                         [paid_amount] DECIMAL (18,2) NOT NULL DEFAULT (0.0));

        IF OBJECT_ID('tempdb..#contract_matrix_data') is not null DROP TABLE [#contract_matrix_data];

        CREATE TABLE [#contract_matrix_data] ([title_no] INT NOT NULL DEFAULT (0), 
                                     [title_name] VARCHAR(30) NOT NULL DEFAULT (''), 
                                     [attendance_type] VARCHAR(30) NOT NULL DEFAULT (''),
                                     [prod_no] INT NOT NULL DEFAULT (0), 
                                     [prod_name] VARCHAR(30) NOT NULL DEFAULT (''), 
                                     [perf_date] DATE NULL, 
                                     [show_count] INT NOT NULL DEFAULT (0), 
                                     [show_count_running] INT NOT NULL DEFAULT (0),
                                     [sale_total] INT NOT NULL DEFAULT (0),
                                     [sale_total_running] INT NOT NULL DEFAULT (0),
                                     [paid_amount] DECIMAL (18,2) NOT NULL DEFAULT (0.0),
                                     [paid_amount_running] DECIMAL (18,2) NOT NULL DEFAULT (0.0));

        IF OBJECT_ID('tempdb..#contract_matrix') is not null DROP TABLE [#contract_matrix];

        CREATE TABLE [#contract_matrix] ([rec_no] INT NOT NULL IDENTITY (1,1),
                                       [title_no] INT NOT NULL DEFAULT (0), 
                                       [title_name] VARCHAR(30) NOT NULL DEFAULT (''), 
                                       [prod_no] INT NOT NULL DEFAULT (0), 
                                       [prod_name] VARCHAR(30) NOT NULL DEFAULT (''), 
                                       [perf_date] DATE NULL, 
                                       [perf_day] VARCHAR(30) NOT NULL DEFAULT (''),
                                       [trigger_no] INT NULL,-- NOT NULL DEFAULT (0),
                                       [show_count] INT NOT NULL DEFAULT (0), 
                                       [show_count_running] INT NOT NULL DEFAULT (0),
                                       [cmp_sale_total] INT NOT NULL DEFAULT (0),
                                       [cmp_sale_total_running] INT NOT NULL DEFAULT (0),
                                       [pub_sale_total] INT NOT NULL DEFAULT (0),
                                       [pub_sale_total_running] INT NOT NULL DEFAULT (0),
                                       [pub_paid_amount] DECIMAL (18,2) NOT NULL DEFAULT (0.0),
                                       [pub_paid_amount_running] DECIMAL (18,2) NOT NULL DEFAULT (0.0),
                                       [sch_show_count] INT NOT NULL DEFAULT (0), 
                                       [sch_show_count_running] INT NOT NULL DEFAULT (0),
                                       [sch_sale_total] INT NOT NULL DEFAULT (0),
                                       [sch_sale_total_running] INT NOT NULL DEFAULT (0),
                                       [sch_paid_amount] DECIMAL (18,2) NOT NULL DEFAULT (0.0),
                                       [sch_paid_amount_running] DECIMAL (18,2) NOT NULL DEFAULT (0.0),
                                       [buy_show_count] INT NOT NULL DEFAULT (0), 
                                       [buy_show_count_running] INT NOT NULL DEFAULT (0),
                                       [buy_sale_total] INT NOT NULL DEFAULT (0),
                                       [buy_sale_total_running] INT NOT NULL DEFAULT (0),
                                       [buy_paid_amount] DECIMAL (18,2) NOT NULL DEFAULT (0.0),
                                       [buy_paid_amount_running] DECIMAL (18,2) NOT NULL DEFAULT (0.0),
                                       [tot_sale_total] INT NOT NULL DEFAULT (0),
                                       [tot_sale_total_running] INT NOT NULL DEFAULT (0),
                                       [tot_paid_amount] DECIMAL (18,2) NOT NULL DEFAULT (0.0),
                                       [tot_paid_amount_running] DECIMAL (18,2) NOT NULL DEFAULT (0.0),
                                       [new_record] CHAR(1) NOT NULL DEFAULT ('N'));

        IF OBJECT_ID('tempdb..#contract_trigger_adjustments') is not null DROP TABLE [#contract_trigger_adjustments];

        CREATE TABLE [#contract_trigger_adjustments] ([perf_date] DATE NULL,  
                                             [trigger_no] INT NOT NULL DEFAULT (0),  
                                             [trigger_type] VARCHAR(30) NOT NULL DEFAULT (''),
                                             [trigger_range_low] INT NOT NULL DEFAULT (0),
                                             [trigger_range_high] INT NOT NULL DEFAULT (0),
                                             [higher_attend_type] CHAR(3) NOT NULL DEFAULT ('pub'),
                                             [show_count] INT NOT NULL DEFAULT(0),
                                             [show_count_running] INT NOT NULL DEFAULT(0),
                                             [tot_sale_total] INT NOT NULL DEFAULT (0), 
                                             [tot_sale_total_running] INT NOT NULL DEFAULT (0),
                                             [adjustment_shows] INT NOT NULL DEFAULT (0),
                                             [adjustment_count] INT NOT NULL DEFAULT (0),
                                             [avg_attend_per_show] INT NOT NULL DEFAULT (0),
                                             [per_paid_amount] DECIMAL (18,2) NOT NULL DEFAULT (0.0),
                                             [tot_paid_amount] DECIMAL (18,2) NOT NULL DEFAULT (0.0),
                                             [tot_paid_amount_running] DECIMAL (18,2) NOT NULL DEFAULT (0.0),
                                             [avg_paid_amount] DECIMAL (18,2) NOT NULL DEFAULT (0.0),
                                             [adjustment_attend] INT NOT NULL DEFAULT (0),
                                             [adjustment_amount] DECIMAL (18,2) NOT NULL DEFAULT (0.0));


    /**************************************************
        Check Parameters
        If no vaid contract id number is passed to the report, it cannot continue.
     **************************************************/

        SELECT @contract_id = ISNULL(@contract_id, 0);

        IF @contract_id <= 0 BEGIN

            THROW 51000, 'Invalid Contract Id Number.', 1;  

            GOTO FINISHED;

        END;


    /**************************************************
        Get Contract Values.
        These values are used throughout the procedure.
        If two productions are passed, the report uses the description of the public title,
        unless there is none.  If no public title, it uses the school title.
        If neither, the procedure cannot continue.
     **************************************************/

        SELECT @report_start_dt = [start_date],
               @report_end_dt = [end_date],
               @public_prod_no = ISNULL([public_production],0),
               @school_prod_no = ISNULL([school_production],0)
        FROM [dbo].[LTR_CONTRACT_TERMS]
        WHERE [id]= @contract_id;

        IF @public_prod_no = 0 AND @school_prod_no = 0 BEGIN;
            THROW 51000, 'No Productions Found.', 1;  
            GOTO FINISHED;
        END;

        IF @public_prod_no > 0 SELECT @prod_no = @public_prod_no;
        ELSE SELECT @prod_no = @school_prod_no;

        SELECT @prod_name = ISNULL([description], 'Unknown Title') FROM [dbo].[T_INVENTORY] WHERE [inv_no] = @prod_no;

        SELECT @title_no = ISNULL([title_no],0) FROM [dbo].[T_PRODUCTION] WHERE [prod_no] = @prod_no;
        SELECT @title_name = ISNULL([description], '') FROM [dbo].[T_INVENTORY] WHERE [inv_no] = @title_no;

        SELECT @start_dt = CAST(@report_start_dt AS DATE),
               @end_dt = CAST(@report_end_dt AS DATE);


    /*  Get the basic matrix using the LRP_CREATE_ATTENDANCE_MATRIX procedure.  */

        --SELECT @report_start_dt, @report_end_dt, @public_prod_no, @school_prod_no

        INSERT INTO [#contract_matrix] ([title_no], [title_name], [prod_no], [prod_name], [perf_date], [perf_day], [show_count], [show_count_running], 
                                     [cmp_sale_total], [cmp_sale_total_running], [pub_sale_total], [pub_sale_total_running], [pub_paid_amount], 
                                     [pub_paid_amount_running], [sch_show_count], [sch_show_count_running], [sch_sale_total], [sch_sale_total_running], 
                                     [sch_paid_amount], [sch_paid_amount_running], [buy_show_count], [buy_show_count_running], [buy_sale_total], 
                                     [buy_sale_total_running], [buy_paid_amount], [buy_paid_amount_running], [tot_sale_total], [tot_sale_total_running],
                                     [tot_paid_amount], [tot_paid_amount_running])
        EXECUTE [dbo].[LRP_CREATE_ATTENDANCE_MATRIX] @report_start_dt, @report_end_dt, @public_prod_no, @school_prod_no;

    /**************************************************
        Assign trigger numbers to each day in the matrix
        Triggers are milestones in the contract based on number of viewers who have seen the show or
        number of shows that have been run.  They are defined in the LTR_CONTRACT_TRIGGERS table.
     **************************************************/
    
        WITH [CTE_TRIGGERS] ([trigger_no], [trigger_type], [trigger_range_low], [trigger_range_high])
        AS (SELECT  [trigger_no],
                    [trigger_type],
                    [trigger_range_low],
                    [trigger_range_high]
            FROM [dbo].[LTR_CONTRACT_TRIGGERS]
            WHERE [contract_id] = @contract_id)
        UPDATE [#contract_matrix]
        SET [trigger_no] = (SELECT MAX(ISNULL([CTE_TRIGGERS].[trigger_no], 0))
                            FROM [CTE_TRIGGERS]
                            WHERE ([CTE_TRIGGERS].[trigger_type] = 'Viewers' and [CTE_TRIGGERS].[trigger_range_low] <= [#contract_matrix].[tot_sale_total_running])
                               OR ([CTE_TRIGGERS].[trigger_type] = 'Shows' and [CTE_TRIGGERS].[trigger_range_low] <= [#contract_matrix].[show_count_running]));
   
   
    /**************************************************
        Admustments may need to be made.
        By default there is a single record for each date in the range in the end results of this procedure.
        However, sometimes a trigger is achieved in the middle of the day and sone of a day's attendance applies to
        one day and some applies to another.  This section figures out what adjustments need to be made.
        The amount of extra attendance is determined.  The amount of extra revenue is determined based on the average ticket price for that day.
        The original record us updated, removing the extra attendance and revenue for the day
        A new record (a second record for that date) is added containing the extra attendance and revenue for the day but assigned to the other trigger
        Attendance will be removed from public or school (whichever has more attendance that day).
     **************************************************/
    
    IF (SELECT MAX(trigger_type) FROM [dbo].[LTR_CONTRACT_TRIGGERS] WHERE contract_id = @contract_id) = 'Viewers' BEGIN
            
            --Determine how the date needs to be adjusted
             WITH [CTE_TRIGGERS] ([trigger_no], [trigger_type], [trigger_range_low], [trigger_range_high])
             AS (SELECT  [trigger_no],
                         [trigger_type],
                         [trigger_range_low],
                         [trigger_range_high]
                 FROM [dbo].[LTR_CONTRACT_TRIGGERS]
                 WHERE [contract_id] = @contract_id)
             INSERT INTO [#contract_trigger_adjustments] ([perf_date], [trigger_no], [trigger_type], [trigger_range_low], [trigger_range_high],
                                                 [tot_sale_total], [tot_sale_total_running], [adjustment_count], [tot_paid_amount], 
                                                 [per_paid_amount], [tot_paid_amount_running], [avg_paid_amount], [adjustment_amount], [higher_attend_type])
             SELECT atf.[perf_date], 
                    atf.[trigger_no], 
                    cte.[trigger_type],
                    cte.[trigger_range_low], 
                    cte.[trigger_range_high],
                    atf.[tot_sale_total], 
                    atf.[tot_sale_total_running],
                    ((cte.[trigger_range_low] - 1) - atf.[tot_sale_total_running]),
                    atf.[tot_paid_amount],
                    CASE WHEN  atf.[pub_sale_total] = 0 THEN 0 ELSE (atf.[pub_paid_amount] / CAST(atf.[pub_sale_total] AS DECIMAL(18,2))) END,
                    atf.[tot_paid_amount_running],
                    CASE WHEN atf.[tot_sale_total] = 0 THEN 0.00 
                         ELSE (atf.[tot_paid_amount] / CAST(atf.[tot_sale_total] AS DECIMAL(18,4))) END,
                    CASE WHEN atf.[tot_sale_total] = 0 THEN 0.00 
                         ELSE ( (atf.[tot_paid_amount] / CAST(atf.[tot_sale_total] AS DECIMAL(18,4))) * ((cte.[trigger_range_low] - 1) - atf.[tot_sale_total_running])) END,
                    CASE WHEN atf.[sch_sale_total] > [at2].[pub_sale_total] THEN 'sch' ELSE 'pub' END
             FROM [#contract_matrix] AS atf
                  LEFT OUTER JOIN [#contract_matrix] AS at2 ON at2.[rec_no] = (atf.[rec_no] - 1)
                  INNER JOIN [CTE_TRIGGERS] AS cte ON cte.[trigger_no] = atf.[trigger_no]
            WHERE atf.[trigger_no] <> at2.[trigger_no];

            --SET ADJUSTMENT AMOUNTS TO POSITIVE NUMBERS
            UPDATE [#contract_trigger_adjustments]
            SET [adjustment_count] = ABS([adjustment_count]),
                [adjustment_amount] = ABS([adjustment_amount])

--SELECT * FROM [#contract_trigger_adjustments]
----SELECT * FROM [#contract_matrix] WHERE [perf_date] between '2016/09/26' and '2016/09/28' ORDER BY [perf_date], [trigger_no]
--SELECT [new_record], [trigger_no], [pub_sale_total], [pub_paid_amount], [pub_paid_amount_running], [tot_paid_amount], [tot_paid_amount_running]
--FROM [#contract_matrix] WHERE [perf_date] between '2016/09/26' and '2016/09/28' ORDER BY [perf_date], [trigger_no]

            --CREATE a new record (a second record for the specific date)  Set the trigger to the previous trigger 
            --If public attendance is higher than school, add the attendance and revenue to public
            --If school attendance is highrt than public, add the attendance to school
            --always add attendance and revenue to totals
            --NOTE: runing totals = the running total from the original record minus the adjusted total from the original record.
            INSERT INTO [#contract_matrix] ([title_no], [title_name], [prod_no], [prod_name], [perf_date], [perf_day], [trigger_no], [show_count], [show_count_running],
                                         [cmp_sale_total], [cmp_sale_total_running], [pub_sale_total], [pub_sale_total_running], [pub_paid_amount], [pub_paid_amount_running],
                                         [sch_show_count], [sch_show_count_running], [sch_sale_total], [sch_sale_total_running], [sch_paid_amount], [sch_paid_amount_running],
                                         [buy_show_count], [buy_show_count_running], [buy_sale_total], [buy_sale_total_running], [buy_paid_amount], [buy_paid_amount_running],
                                         [tot_sale_total], [tot_sale_total_running], [tot_paid_amount], [tot_paid_amount_running], [new_record])                
            SELECT  att.[title_no],
                    att.[title_name],
                    att.[prod_no],
                    att.[prod_name],
                    att.[perf_date],
                    att.[perf_day],
                    (att.[trigger_no] - 1),
                    0,
                    att.[show_count_running],
                    0,
                    (att.[cmp_sale_total_running] - att.[cmp_sale_total]),
                    (att.[pub_sale_total] - adj.[adjustment_count]),
                    (att.[pub_sale_total_running] - adj.[adjustment_count]),
                    (CAST((att.[pub_sale_total] - adj.[adjustment_count]) AS DECIMAL(18,2)) * adj.[per_paid_amount]),
                    att.[pub_paid_amount_running] - att.[pub_paid_amount] + (CAST((att.[pub_sale_total] - adj.[adjustment_count]) AS DECIMAL(18,2)) * adj.[per_paid_amount]),
                    att.[sch_show_count],
                    att.[sch_show_count_running],
                    att.[sch_sale_total],
                    att.[sch_sale_total_running],
                    att.[sch_paid_amount],
                    att.[sch_paid_amount_running],
                    att.[buy_show_count],
                    att.[buy_show_count_running],
                    att.[buy_sale_total],
                    att.[buy_sale_total_running],
                    att.[buy_paid_amount],
                    att.[buy_paid_amount_running],
                    (att.[tot_sale_total] - adj.[adjustment_count]),
                    (att.[tot_sale_total_running] - adj.[adjustment_count]),
                    att.[tot_paid_amount] + (CAST((att.[pub_sale_total] - adj.[adjustment_count]) AS DECIMAL(18,2)) * adj.[per_paid_amount]),
                    att.[tot_paid_amount_running] + (CAST((att.[pub_sale_total] - adj.[adjustment_count]) AS DECIMAL(18,2)) * adj.[per_paid_amount]),
                    'Y'
            FROM    [#contract_matrix] AS att
                    INNER JOIN [#contract_trigger_adjustments] AS adj ON adj.[perf_date] = att.[perf_date] AND adj.[trigger_no] = att.[trigger_no];

            --UPDATE the existing record, subtracting the amounts that need to be subtracted
            --Note: Running Totals do not have to be adjusted on the existing record.
            UPDATE att
            SET att.[pub_sale_total] =  (att.[pub_sale_total] - (att.[pub_sale_total] - adj.[adjustment_count])),
                att.[pub_paid_amount] =  att.[pub_paid_amount] - at2.[pub_paid_amount],
                att.[sch_show_count] = 0,
                att.[sch_sale_total] = 0,
                att.[sch_paid_amount] = 0.00,
                att.[buy_show_count] = 0,
                att.[buy_sale_total] = 0,
                att.[buy_paid_amount] = 0.0,
                att.[tot_sale_total] = (att.[tot_sale_total] - adj.[adjustment_count]),
                att.[tot_paid_amount] = (att.[tot_paid_amount] - at2.[tot_paid_amount])
            FROM [#contract_matrix] AS att
                 INNER JOIN [#contract_trigger_adjustments] AS adj ON adj.[perf_date] = att.[perf_date] AND adj.[trigger_no] = att.[trigger_no]
                 LEFT OUTER JOIN [#contract_matrix] AS at2 ON at2.[perf_date] = att.[perf_date] AND at2.[trigger_no] = (att.[trigger_no] - 1)
            WHERE att.[new_record] = 'N';

    END ELSE BEGIN

            --Determine how the date needs to be adjusted
             WITH [CTE_TRIGGERS] ([trigger_no], [trigger_type], [trigger_range_low], [trigger_range_high])
             AS (SELECT  [trigger_no],
                         [trigger_type],
                         [trigger_range_low],
                         [trigger_range_high]
                 FROM [dbo].[LTR_CONTRACT_TRIGGERS]
                 WHERE [contract_id] = @contract_id)
            INSERT INTO [#contract_trigger_adjustments] ([perf_date], [trigger_no], [trigger_type], [trigger_range_low], [trigger_range_high], [show_count], [show_count_running],
                                                [tot_sale_total], [tot_sale_total_running], [adjustment_shows], [adjustment_count], [adjustment_amount], [tot_paid_amount], 
                                                [tot_paid_amount_running], [avg_paid_amount], [higher_attend_type])
             SELECT atf.[perf_date], 
                    atf.[trigger_no], 
                    cte.[trigger_type],
                    cte.[trigger_range_low], 
                    cte.[trigger_range_high],
                    atf.[show_count], 
                    atf.[show_count_running],
                    atf.[tot_sale_total],
                    atf.[tot_sale_total_running],
                    (atf.[show_count] - ([atf].[show_count_running] - [cte].[trigger_range_low]) - 1),
                    CASE WHEN atf.[sch_sale_total] > [at2].[pub_sale_total] AND atf.[show_count] > 0 THEN
                            ((atf.[sch_sale_total] / atf.[show_count])  * (atf.[show_count] - ([atf].[show_count_running] - [cte].[trigger_range_low]) - 1))
                         WHEN atf.[sch_sale_total] <= [at2].[pub_sale_total] AND atf.[show_count] > 0 THEN 
                            ((atf.[pub_sale_total] / atf.[show_count])  * (atf.[show_count] - ([atf].[show_count_running] - [cte].[trigger_range_low]) - 1))
                         ELSE 0 END,
                    CASE WHEN atf.[sch_sale_total] > [at2].[pub_sale_total] AND atf.[show_count] > 0 THEN
                            ((atf.[sch_paid_amount] / atf.[show_count])  * (atf.[show_count] - ([atf].[show_count_running] - [cte].[trigger_range_low]) - 1))
                         WHEN atf.[sch_sale_total] <= [at2].[pub_sale_total] AND atf.[show_count] > 0 THEN 
                            ((atf.[pub_paid_amount] / atf.[show_count])  * (atf.[show_count] - ([atf].[show_count_running] - [cte].[trigger_range_low]) - 1))
                         ELSE 0.00 END,
                    atf.[tot_paid_amount],
                    atf.[tot_paid_amount_running],
                    CASE WHEN atf.[tot_sale_total] = 0 THEN 0.0
                         ELSE (atf.[tot_paid_amount] / CAST(atf.[tot_sale_total] AS DECIMAL(18,4))) END,
                    CASE WHEN atf.[sch_sale_total] > [at2].[pub_sale_total] THEN 'sch' ELSE 'pub' END
             FROM [#contract_matrix] AS atf
                  LEFT OUTER JOIN [#contract_matrix] AS at2 ON at2.[rec_no] = (atf.[rec_no] - 1)
                  INNER JOIN [CTE_TRIGGERS] AS cte ON cte.[trigger_no] = atf.[trigger_no]
            WHERE atf.[trigger_no] <> at2.[trigger_no];

            --SET ADJUSTMENT AMOUNTS TO POSITIVE NUMBERS
            UPDATE [#contract_trigger_adjustments]
            SET [adjustment_count] = ABS([adjustment_count]),
                [adjustment_amount] = ABS([adjustment_amount]),
                [adjustment_shows] = ABS([adjustment_shows])

            --CREATE a new record (a second record for the specific date)  Set the trigger to the previous trigger 
            --If public attendance is higher than school, add the attendance and revenue to public
            --If school attendance is highrt than public, add the attendance to school
            --always add attendance and revenue to totals
            --NOTE: runing totals = the running total from the original record minus the adjusted total from the original record.
            INSERT INTO [#contract_matrix] ([title_no], [title_name], [prod_no], [prod_name], [perf_date], [perf_day], [trigger_no], [show_count], [show_count_running],
                                         [cmp_sale_total], [cmp_sale_total_running], [pub_sale_total], [pub_sale_total_running], [pub_paid_amount], [pub_paid_amount_running],
                                         [sch_show_count], [sch_show_count_running], [sch_sale_total], [sch_sale_total_running], [sch_paid_amount], [sch_paid_amount_running],
                                         [buy_show_count], [buy_show_count_running], [buy_sale_total], [buy_sale_total_running], [buy_paid_amount], [buy_paid_amount_running],
                                         [tot_sale_total], [tot_sale_total_running], [tot_paid_amount], [tot_paid_amount_running], [new_record])                
            SELECT  att.[title_no],
                    att.[title_name],
                    att.[prod_no],
                    att.[prod_name],
                    att.[perf_date],
                    att.[perf_day],
                    (ISNULL(att.[trigger_no], 1) - 1),
                    adj.[adjustment_shows],
                    (att.[show_count_running] - adj.[adjustment_shows]),
                    0,
                    (att.[cmp_sale_total_running] - att.[cmp_sale_total]),
                    (att.[pub_sale_total] - adj.[adjustment_count]),
                    (att.[pub_sale_total_running] - adj.[adjustment_count]),
                    (CAST((att.[pub_sale_total] - adj.[adjustment_count]) AS DECIMAL(18,2)) * adj.[per_paid_amount]),
                    att.[pub_paid_amount_running] - att.[pub_paid_amount] + (CAST((att.[pub_sale_total] - adj.[adjustment_count]) AS DECIMAL(18,2)) * adj.[per_paid_amount]),
                    att.[sch_show_count],
                    att.[sch_show_count_running],
                    att.[sch_sale_total],
                    att.[sch_sale_total_running],
                    att.[sch_paid_amount],
                    att.[sch_paid_amount_running],
                    att.[buy_show_count],
                    att.[buy_show_count_running],
                    att.[buy_sale_total],
                    att.[buy_sale_total_running],
                    att.[buy_paid_amount],
                    att.[buy_paid_amount_running],
                    (att.[tot_sale_total] - adj.[adjustment_count]),
                    (att.[tot_sale_total_running] - adj.[adjustment_count]),
                    att.[tot_paid_amount] + (CAST((att.[pub_sale_total] - adj.[adjustment_count]) AS DECIMAL(18,2)) * adj.[per_paid_amount]),
                    att.[tot_paid_amount_running] + (CAST((att.[pub_sale_total] - adj.[adjustment_count]) AS DECIMAL(18,2)) * adj.[per_paid_amount]),
                    'Y'
            FROM    [#contract_matrix] AS att
                    INNER JOIN [#contract_trigger_adjustments] AS adj ON adj.[perf_date] = att.[perf_date] AND adj.[trigger_no] = att.[trigger_no];

            --UPDATE the existing record, subtracting the amounts that need to be subtracted
            --Note: Running Totals do not have to be adjusted on the existing record.
            UPDATE att
            SET att.[show_count] = (att.[show_count] - adj.[adjustment_shows]),
                att.[pub_sale_total] =  (att.[pub_sale_total] - (att.[pub_sale_total] - adj.[adjustment_count])),
                att.[pub_paid_amount] =  att.[pub_paid_amount] - at2.[pub_paid_amount],
                att.[sch_show_count] = 0,
                att.[sch_sale_total] = 0,
                att.[sch_paid_amount] = 0.00,
                att.[buy_show_count] = 0,
                att.[buy_sale_total] = 0,
                att.[buy_paid_amount] = 0.0,
                att.[tot_sale_total] = (att.[tot_sale_total] - adj.[adjustment_count]),
                att.[tot_paid_amount] = (att.[tot_paid_amount] - at2.[tot_paid_amount])
            FROM [#contract_matrix] AS att
                 INNER JOIN [#contract_trigger_adjustments] AS adj ON adj.[perf_date] = att.[perf_date] AND adj.[trigger_no] = att.[trigger_no]
                 LEFT OUTER JOIN [#contract_matrix] AS at2 ON at2.[perf_date] = att.[perf_date] AND at2.[trigger_no] = (att.[trigger_no] - 1)
            WHERE att.[new_record] = 'N';

    END

--SELECT * FROM [#contract_matrix] WHERE [perf_date] between '2016/09/26' and '2016/09/28' ORDER BY [perf_date], [trigger_no]
--SELECT [new_record], [trigger_no], [pub_sale_total], [pub_paid_amount], [pub_paid_amount_running], [tot_paid_amount], [tot_paid_amount_running]
--FROM [#contract_matrix] WHERE [perf_date] between '2016/09/26' and '2016/09/28' ORDER BY [perf_date], [trigger_no]

    FINISHED:

        /*  Copy final data set over to History Table  */

            DELETE FROM [dbo].[LT_HISTORY_ATTENDANCE_MATRIX] WHERE [contract_id] = @contract_id;

            INSERT INTO [dbo].[LT_HISTORY_ATTENDANCE_MATRIX] ([contract_id], [data_dt], [title_no], [title_name], [prod_no], [prod_name], 
                                                              [perf_date], [perf_day], [trigger_no], [show_count], [show_count_running], [cmp_sale_total],
                                                              [cmp_sale_total_running], [pub_sale_total], [pub_sale_total_running], [pub_paid_amount],
                                                              [pub_paid_amount_running], [sch_show_count], [sch_show_count_running], [sch_sale_total],
                                                              [sch_sale_total_running], [sch_paid_amount], [sch_paid_amount_running], [buy_show_count], 
                                                              [buy_show_count_running], [buy_sale_total], [buy_sale_total_running], [buy_paid_amount], 
                                                              [buy_paid_amount_running], [tot_sale_total], [tot_sale_total_running], [tot_paid_amount], 
                                                              [tot_paid_amount_running])
            SELECT @contract_id,
                   @data_dt,
                   [title_no], 
                   [title_name], 
                   [prod_no], 
                   [prod_name], 
                   [perf_date], 
                   [perf_day],
                   ISNULL([trigger_no], 1),
                   [show_count], 
                   [show_count_running],
                   [cmp_sale_total],
                   [cmp_sale_total_running],
                   [pub_sale_total],
                   [pub_sale_total_running],
                   [pub_paid_amount],
                   [pub_paid_amount_running],
                   [sch_show_count], 
                   [sch_show_count_running],
                   [sch_sale_total],
                   [sch_sale_total_running],
                   [sch_paid_amount],
                   [sch_paid_amount_running],
                   [buy_show_count], 
                   [buy_show_count_running], 
                   [buy_sale_total], 
                   [buy_sale_total_running], 
                   [buy_paid_amount], 
                   [buy_paid_amount_running],
                   [tot_sale_total],
                   [tot_sale_total_running],
                   [tot_paid_amount],
                   [tot_paid_amount_running]
            FROM [#contract_matrix]
            ORDER BY [perf_date];

    DONE:

        /*  Clean up  */

            IF OBJECT_ID('tempDB..#contract_trigger_adjustments') IS NOT NULL DROP TABLE [#contract_trigger_adjustments];
            IF OBJECT_ID('tempdb..#attend_final') IS NOT NULL DROP TABLE [#attend_final];
            IF OBJECT_ID('tempdb..#contract_matrix_data') IS NOT NUlL  DROP TABLE [#contract_matrix_data];
            IF OBJECT_ID('tempdb..#contract_matrix_raw') IS NOT NULL DROP TABLE [#contract_matrix_raw];

END;
