USE impresario;
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LTR_ATTENDANCE_DEMOGRAPHICS]') AND type in (N'P', N'PC'))
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LTR_ATTENDANCE_DEMOGRAPHICS] AS';
GO

EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LTR_ATTENDANCE_DEMOGRAPHICS] TO [impusers], [tessitura_app]';
GO

ALTER PROCEDURE [dbo].[LTR_ATTENDANCE_DEMOGRAPHICS]
        @report_start_dt DATETIME = NULL,
        @report_end_dt DATETIME = NULL,
        @show_individual_dates CHAR(1) = 'N',
        @suppress_unknown CHAR(1) = 'N',
        @location VARCHAR(50) = ''
AS BEGIN

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    SET NOCOUNT ON;

    /*  Procedure Variables and Temp tables  */

        DECLARE @start_date CHAR(10) = '', @end_date CHAR(10) = '';
        DECLARE @order_no INT = 0;

        IF OBJECT_ID('tempdb..#order_table') is not null DROP TABLE [#order_table];

        CREATE TABLE [#order_table] ([order_no] INT NOT NULL DEFAULT (0),
                                     [customer_no] INT NOT NULL DEFAULT (0),
                                     [mos] INT NOT NULL DEFAULT (0),
                                     [mos_name] VARCHAR(30) NOT NULL DEFAULT (''),
                                     [solicitor] VARCHAR(20) NOT NULL DEFAULT (''),
                                     [solicitor_location] VARCHAR(30) NOT NULL DEFAULT (''),
                                     [performance_date] CHAR(10) NOT NULL DEFAULT (''),
                                     [sale_total] INT NOT NULL DEFAULT (0),
                                     [scan_total] INT NOT NULL DEFAULT (0),
                                     [visit_count] INT NOT NULL DEFAULT (0));

        IF OBJECT_ID('tempdb..#demographic_table') is not null DROP TABLE [#demographic_table];
        
        CREATE TABLE [#demographic_table] ([order_no] INT NOT NULL DEFAULT (0),
                                           [customer_no] INT NOT NULL DEFAULT (0),
                                           [mos] INT NOT NULL DEFAULT (0),
                                           [mos_name] VARCHAR(30) NOT NULL DEFAULT (''),
                                           [solicitor] VARCHAR(20) NOT NULL DEFAULT (''),
                                           [solicitor_location] VARCHAR(30) NOT NULL DEFAULT (''),
                                           [performance_date] CHAR(10) NOT NULL DEFAULT (''),
                                           [city] VARCHAR(50) NOT NULL DEFAULT (''),
                                           [state_1] VARCHAR(50) NOT NULL DEFAULT (''),
                                           [state_name_1] VARCHAR(50) NOT NULL DEFAULT (''),
                                           [state_2] VARCHAR(50) NOT NULL DEFAULT (''),
                                           [state_name_2] VARCHAR(50) NOT NULL DEFAULT (''),
                                           [postal_code] VARCHAR(25) NOT NULL DEFAULT (''),
                                           [country] INT NOT NULL DEFAULT (0),
                                           [country_name] VARCHAR(50) NOT NULL DEFAULT (0),
                                           [state_order] CHAR(1) NOT NULL DEFAULT (''),
                                           [state_display] VARCHAR(50) NOT NULL DEFAULT (''),
                                           [country_order] CHAR(1) NOT NULL DEFAULT (''),
                                           [country_header] VARCHAR(50) NOT NULL DEFAULT (''),
                                           [region_order] CHAR(1) NOT NULL DEFAULT (''),
                                           [region_header] VARCHAR(50) NOT NULL DEFAULT (''),
                                           [state_header] VARCHAR(50) NOT NULL DEFAULT (''),
                                           [visit_count] INT NOT NULL DEFAULT (0));

    /*  Check Parameters  */

        IF @report_start_dt IS NULL OR @report_end_dt IS NULL
            SELECT @report_start_dt = DATEADD(DAY, -1, GETDATE()),
                   @report_end_dt = DATEADD(DAY, -1, GETDATE())

        SELECT @start_date = CONVERT(CHAR(10),@report_start_dt,111),
               @end_date = CONVERT(CHAR(10),@report_end_dt,111);

        IF ISNULL(@show_individual_dates,'N') <> 'Y' SELECT @show_individual_dates = 'N';

        IF ISNULL(@location,'') IN ('','All') SELECT @location = '';


    /*  Get Order and Exhibit Hall Totals  */

        INSERT INTO [#order_table] ([order_no], [customer_no], [mos], [mos_name], [solicitor], [solicitor_location], 
                                    [performance_date], [sale_total], [scan_total], [visit_count])              
        SELECT h.[order_no], 
               h.[customer_no],
               o.[MOS],
               m.[description],
               o.[solicitor],
               u.[location],
               h.[performance_date],
               SUM(h.[sale_total]),
               SUM(h.[scan_admission_total]),
               SUM(CASE WHEN h.[scan_admission_total] > 0 THEN h.[scan_admission_total] ELSE h.[sale_total] END)
        FROM [dbo].[LT_HISTORY_TICKET] AS h
             INNER JOIN [dbo].[T_ORDER] AS o ON o.[order_no] = h.[order_no]
             INNER JOIN [dbo].[TR_MOS] AS m ON m.[id] = o.[MOS]
             LEFT OUTER JOIN [dbo].[LV_MuseumUserInfo] AS u ON u.[userid] = o.[solicitor]
        WHERE h.[performance_date] BETWEEN @start_date AND @end_date
          AND h.[title_name] IN ('Exhibit Halls','Show and Collected','Show and Go','Membership Card Scan')
          AND h.[order_no] > 0
          AND (h.[sale_total] > 0 OR h.[scan_admission_total] > 0)
        GROUP BY h.[order_no], 
                 h.[customer_no], 
                 o.[MOS], 
                 m.[description], 
                 o.[solicitor], 
                 u.[location], 
                 h.[performance_date];

    /*  Remove unwanted data  */

        IF @show_individual_dates <> 'Y'
            UPDATE [#order_table]
            SET [performance_date] = '';

        UPDATE [#order_table] 
        SET [solicitor_location] = 'Web Site'
        WHERE [solicitor_location] LIKE '%system%' 
          AND [solicitor] = 'WebAPI'

        UPDATE [#order_table] 
        SET [solicitor_location] = 'Box Office'
        WHERE [solicitor_location] = 'Visitor Services' 
        
        IF @location = 'Other'
            DELETE FROM [#order_table]
            WHERE [solicitor_location] IN ('Box Office','Science Central','Web Site')
        ELSE IF @location <> ''
            DELETE FROM [#order_table] 
            WHERE [solicitor_location] <> @location

    /*  Get Demographic information  */

        INSERT INTO [#demographic_table] ([order_no], [customer_no], [solicitor_location], [performance_date], [city], [state_1], [state_name_1], 
                                          [state_2], [state_name_2], [postal_code], [country], [country_name], [visit_count])
        SELECT o.[order_no], 
               o.[customer_no], 
               o.[solicitor_location],
               o.[performance_date],
               ISNULL(a.[city],''),
               ISNULL(a.[state],'UN'),
               CASE WHEN ISNULL(s1.[description],'Unknown') LIKE '%unknown%' THEN 'Unknown' ELSE s1.[description] END,
               ISNULL(c.[state],'UN'),
               CASE WHEN ISNULL(s2.[description],'Unknown') LIKE '%unknown%' THEN 'Unknown' ELSE s2.[description] END,
               ISNULL(a.[postal_code],'') AS [postal_code],
               ISNULL(a.[country],0),
               ISNULL(n.[description],'Unknown'),
               o.[visit_count]
        FROM [#order_table] AS o
             LEFT OUTER JOIN [dbo].[T_ADDRESS] AS a ON a.[customer_no] = o.[customer_no] AND a.[primary_ind] = 'Y'
             LEFT OUTER JOIN [dbo].[TR_CITYSTATE] AS c ON c.[zip5] = LEFT(ISNULL(a.[postal_code],''),5)
             LEFT OUTER JOIN [dbo].[TR_STATE] AS s1 ON s1.[id] = ISNULL(a.[state],'')
             LEFT OUTER JOIN [dbo].[TR_STATE] AS s2 ON s2.[id] = ISNULL(c.[state],'')
             LEFT OUTER JOIN [dbo].[TR_COUNTRY] AS n ON n.[id] = a.[country]

    /*  Update date for Display  */

        UPDATE [#demographic_table]
        SET [state_display] = [state_name_1];

        UPDATE [#demographic_table]
        SET [state_display] = [state_name_2]
        WHERE [state_display] = 'UN' AND [state_2] <> 'UN';

        WITH [CTE_COUNTRIES] ([id], [country])
        AS (SELECT [id], [country] FROM [dbo].[TR_STATE])
        UPDATE dt
        SET dt.[country] = ct.[country]
        FROM [#demographic_table] AS dt
             INNER JOIN [CTE_COUNTRIES] AS ct ON ct.[id] = dt.[state_display];
        
        UPDATE [#demographic_table]
        SET [country_order] = CASE WHEN [country] = 1 THEN 'A' ELSE 'B' END,
            [country_header] = CASE WHEN [country] = 1 THEN 'United States' ELSE 'Outside US' END;
        
        UPDATE [#demographic_table]
        SET [region_order] = CASE WHEN [state_display] IN ('Massachusetts','New Hampshire','Connecticut','Rhode Island','Maine','Vermont') THEN 'A' 
                                  WHEN [country_header] = 'United States' THEN 'B' 
                                  ELSE 'C'END,
            [region_header] = CASE WHEN [state_display] IN ('Massachusetts','New Hampshire','Connecticut','Rhode Island','Maine','Vermont') THEN 'New England' 
                                   WHEN [country_header] = 'United States' THEN 'Other US' 
                                   ELSE '' END;

        UPDATE [#demographic_table]
        SET [state_order] = 'A'
        WHERE [state_display] = 'Massachusetts';

        UPDATE [#demographic_table]
        SET [state_order] = 'B'
        WHERE [state_display] <> 'Massachusetts';

        UPDATE [#demographic_table]
        SET [state_order] = 'C',
            [state_display] = 'Armed Forces'
        WHERE [state_display] = 'AA';

        UPDATE [#demographic_table]
        SET [country_order] = 'D',
            [country_header] = 'Unknown',
            [region_order] = 'D',
            [region_header] = 'Unknown',
            [state_order] = 'D',
            [state_display] = 'Unknown'
        WHERE [state_display] IN ('UN','XX','Unknown');

    /*  Remove unwanted data  */

        IF @suppress_unknown = 'Y'
            DELETE FROM [#demographic_table]
            WHERE [state_display] = 'Unknown'

    FINISHED: 
    
        /*  Pull final data set for report  */
        
            SELECT [performance_date],
                   CASE WHEN ISDATE([performance_date]) = 0 THEN NULL ELSE TRY_CONVERT(DATETIME,[performance_date]) END AS [performance_dt],
                   [solicitor_location],
                   [country_order],
                   [country_header],
                   [region_order],
                   [region_header],
                   [state_order],
                   [state_display],
                   CAST(SUM([visit_count]) AS DECIMAL(18,2)) AS [visit_count]
            FROM [#demographic_table] 
            GROUP BY [performance_date], 
                     [solicitor_location], 
                     [country_order], 
                     [country_header], 
                     [region_order], 
                     [region_header], 
                     [state_order], 
                     [state_display]
            ORDER BY [solicitor_location], 
                     [country_order], 
                     [region_order], 
                     [state_order],
                     [performance_date]

    DONE:

        IF OBJECT_ID('tempdb..#demographic_table') is not null DROP TABLE [#demographic_table];
        IF OBJECT_ID('tempdb..#order_table') is not null DROP TABLE [#order_table];

END
GO

--EXECUTE [dbo].[LTR_ATTENDANCE_DEMOGRAPHICS] @report_start_dt = '8-29-2020', @report_end_dt = '9-5-2020',  @show_individual_dates = 'N', @suppress_unknown = 'N', @location = ''
--EXECUTE [dbo].[LTR_ATTENDANCE_DEMOGRAPHICS] @report_start_dt = '7-21-2020', @report_end_dt = '7-31-2020', @show_individual_dates = 'Y', @suppress_unknown = 'N', @location = 'Box Office'
