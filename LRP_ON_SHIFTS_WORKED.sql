USE [impresario]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[LRP_ON_SHIFTS_WORKED]
(
	@start_dt DATE,
	@end_dt DATE  
)
AS

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
SET NOCOUNT ON

--EXEC [LRP_ON_SHIFTS_WORKED] '7/1/2018', '1/30/2019'

SELECT sch.schedule_dt, 
	staff.id AS staff_id,
	staff.resource_name AS staff, 
	1 AS activity_order, 
	sch.first_activity AS activity_id,
	fstact.resource_name AS activity, 
	sch.notes
FROM dbo.LTR_OVERNIGHT_SCHEDULE sch -- 63
INNER JOIN dbo.LV_MOS_RESOURCES staff
	ON sch.schedule_staff = staff.id
	AND staff.resource_type = 'Instructor Staff'
	AND staff.inactive = 'N'
INNER JOIN dbo.LV_MOS_RESOURCES fstact
	ON fstact.id = sch.first_activity
	AND fstact.resource_type IN ('', 'Drop-In Activity') 
	AND fstact.inactive = 'N'
WHERE sch.schedule_dt IS NOT NULL 
	AND sch.schedule_dt BETWEEN @start_dt AND @end_dt

UNION

SELECT sch.schedule_dt, 
	staff.id AS staff_id,
	staff.resource_name AS staff, 
	2 AS activity_order, 
	sch.second_activity AS activity_id,
	sndact.resource_name AS activity, 
	sch.notes
FROM dbo.LTR_OVERNIGHT_SCHEDULE sch -- 63
INNER JOIN dbo.LV_MOS_RESOURCES staff
	ON sch.schedule_staff = staff.id
	AND staff.resource_type = 'Instructor Staff'
	AND staff.inactive = 'N'
INNER JOIN dbo.LV_MOS_RESOURCES sndact
	ON sndact.id = sch.second_activity
	AND sndact.resource_type IN ('', 'Drop-In Activity') 
	AND sndact.inactive = 'N'
WHERE sch.schedule_dt IS NOT NULL 
	AND sch.schedule_dt BETWEEN @start_dt AND @end_dt
