USE [impresario]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[LRP_ADV_CASH_REPORT]
(
	@startDate DATETIME,
	@endDate DATETIME,
	@GIK BIT = 0 -- 0 is default, which means pull everything. 1 means only pull GIK 
)
AS
--EXEC [LRP_ADV_CASH_REPORT] 	@startDate = '1/1/2018', @endDate = '3/31/2018', @GIK = 0

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

DECLARE @results TABLE
(
	[ref_no] [INT] NOT NULL,
	[customer_no] [INT] NOT NULL,
	[main_donor] [VARCHAR](76) NULL,
	[hardcredit_donor] [VARCHAR](76) NULL,
	[pay_type] [VARCHAR](10) NOT NULL,
	[pay_dt] [DATETIME] NULL,
	[pay_amt] [MONEY] NULL,
	[fund_description] [VARCHAR](30) NULL,
	[fyear] [INT] NULL,
	[EF_Unr_Rest] [VARCHAR](15) NOT NULL,
	[unr_notef_flag] [VARCHAR](9) NOT NULL,
	[rest_bdgtrel_flag] [VARCHAR](12) NOT NULL,
	[cmcategory] [VARCHAR](30) NULL,
	[advrevrpt] [VARCHAR](30) NULL,
	[designation] [VARCHAR](30) NULL,
	[acctgoal] [VARCHAR](30) NULL,
	[acctgrp] [VARCHAR](30) NULL,
	[cust_type] [VARCHAR](30) NULL,
	[sort_name] [VARCHAR](55) NULL
)

INSERT INTO @results
(
    ref_no,
    customer_no,
    main_donor,
    hardcredit_donor,
    pay_type,
    pay_dt,
    pay_amt,
    fund_description,
    fyear,
    EF_Unr_Rest,
    unr_notef_flag,
    rest_bdgtrel_flag,
    cmcategory,
    advrevrpt,
    designation,
    acctgoal,
    acctgrp,
    cust_type,
    sort_name
)
-- GIFTS
SELECT c.ref_no,
    c.customer_no,
    b.best_name main_donor,
    b.best_name hardcredit_donor,
    CASE
        WHEN c.cont_type = 'G' AND CAST(c.KG_xfer_dt AS DATE) > '1900-01-01' THEN 'GIK'
        ELSE 'G'
    END AS pay_type,
    c.cont_dt pay_dt,
    c.cont_amt pay_amt,
    f.description fund_description,
    g.fyear,
	CASE 
		WHEN LEFT(fd.notes, 2) = 'EF' THEN 'Enterprise Fund'
		WHEN fd.acct_goal_no = 3 THEN 'Unrestricted'
        WHEN fd.acct_grp_no = 10 AND fd.acct_goal_no <> 3 THEN 'Unrestricted'
        ELSE 'Restricted' 
	END EF_Unr_Rest,
    CASE
        WHEN fd.acct_goal_no = 3
            AND LEFT(fd.notes, 2) <> 'EF'
            AND
            (   c.KG_xfer_dt IS NULL
                OR CAST(c.KG_xfer_dt AS DATE) = '1900-01-01'
            ) THEN 'Unr_NotEF'
        WHEN fd.acct_grp_no = 10
            AND fd.acct_goal_no <> 3
            AND LEFT(fd.notes, 2) <> 'EF'
            AND
            (   c.KG_xfer_dt IS NULL
                OR CAST(c.KG_xfer_dt AS DATE) = '1900-01-01'
            ) THEN 'Unr_NotEF'
        ELSE ''
    END AS unr_notef_flag,
    CASE
        WHEN fd.acct_goal_no IN (1, 2, 4) AND fd.acct_grp_no = 10 THEN 'Rest_BdgtRel'
        ELSE ''
    END AS rest_bdgtrel_flag,
    cc.description cmcategory,
    CASE
        WHEN g.category IN (7, 28) THEN 'Annual Giving'
        WHEN g.category IN (45, 40) THEN 'Leadership Giving'
        WHEN g.category IN (10, 31) THEN 'Corporate'
        WHEN g.category IN (46, 11, 6, 36, 37, 14) THEN 'Fundraising Events'
        WHEN g.category = 29 THEN 'Planned Gifts'
        ELSE cc.description
    END AS advrevrpt,
    cd.description designation,
    ag1.description acctgoal,
    ag2.description acctgrp,
    ct.description cust_type,
    i.sort_name
FROM T_CONTRIBUTION AS c
INNER JOIN T_CAMPAIGN AS g
    ON c.campaign_no = g.campaign_no
INNER JOIN T_FUND AS f
    ON c.fund_no = f.fund_no
INNER JOIN LTR_FUND_DETAIL AS fd
    ON c.fund_no = fd.fund_no
LEFT OUTER JOIN TR_CONT_DESIGNATION AS cd
    ON fd.designation = cd.id
LEFT OUTER JOIN LTR_ACCT_GOAL AS ag1
    ON fd.acct_goal_no = ag1.id
LEFT OUTER JOIN LTR_ACCT_GRP AS ag2
    ON fd.acct_grp_no = ag2.id
INNER JOIN TR_CAMPAIGN_CATEGORY AS cc
    ON g.category = cc.id
LEFT OUTER JOIN LV_ADV_BEST_NAME AS b
    ON c.customer_no = b.customer_no
INNER JOIN T_CUSTOMER AS i
    ON c.customer_no = i.customer_no
INNER JOIN TR_CUST_TYPE AS ct
    ON i.cust_type = ct.id
WHERE c.cont_type = 'G'
      AND c.custom_1 IN ('(none)', 'Matching Gift Credit')
      AND CAST(c.cont_dt AS DATE) BETWEEN @startDate AND @endDate 
      AND c.cont_amt > 0
      AND c.customer_no <> 2653093
      AND g.category <> 24

UNION ALL 

-- GIFTS - CREDITEE
SELECT c.ref_no,
    i.customer_no,
    b.best_name main_donor,
    b2.best_name hardcredit_donor,
    CASE
        WHEN c.cont_type = 'G'
            AND CAST(c.KG_xfer_dt AS DATE) > '1900-01-01' THEN 'GIK'
        ELSE 'G'
    END AS pay_type,
    c.cont_dt pay_dt,
    c.cont_amt pay_amt,
    f.description fund_description,
    g.fyear,
	CASE 
		WHEN LEFT(fd.notes, 2) = 'EF' THEN 'Enterprise Fund'
		WHEN fd.acct_goal_no = 3 THEN 'Unrestricted'
        WHEN fd.acct_grp_no = 10 AND fd.acct_goal_no <> 3 THEN 'Unrestricted'
        ELSE 'Restricted' 
	END EF_Unr_Rest,
    CASE
        WHEN fd.acct_goal_no = 3
            AND LEFT(fd.notes, 2) <> 'EF'
            AND
            (
                c.KG_xfer_dt IS NULL
                OR CAST(c.KG_xfer_dt AS DATE) = '1900-01-01'
            ) THEN 'Unr_NotEF'
        WHEN fd.acct_grp_no = 10
            AND fd.acct_goal_no <> 3
            AND LEFT(fd.notes, 2) <> 'EF'
            AND
            (
                c.KG_xfer_dt IS NULL
                OR CAST(c.KG_xfer_dt AS DATE) = '1900-01-01'
            ) THEN 'Unr_NotEF'
        ELSE ''
    END AS unr_notef_flag,
    CASE
        WHEN fd.acct_goal_no IN (1, 2, 4)
            AND fd.acct_grp_no = 10 THEN 'Rest_BdgtRel'
        ELSE ''
    END AS rest_bdgtrel_flag,
    cc.description cmcategory,
    CASE
        WHEN g.category IN (7, 28) THEN 'Annual Giving'
        WHEN g.category IN (45, 40) THEN 'Leadership Giving'
        WHEN g.category IN (10, 31) THEN 'Corporate'
        WHEN g.category IN (46, 11, 6, 36, 37, 14) THEN 'Fundraising Events'
        WHEN g.category = 29 THEN 'Planned Gifts'
        ELSE cc.description
    END AS advrevrpt,
    cd.description designation,
    ag1.description acctgoal,
    ag2.description acctgrp,
    ct.description cust_type,
    i.sort_name
FROM T_CONTRIBUTION AS c
INNER JOIN T_CREDITEE AS t
    ON c.ref_no = t.ref_no
INNER JOIN T_CAMPAIGN AS g
    ON c.campaign_no = g.campaign_no
INNER JOIN T_FUND AS f
    ON c.fund_no = f.fund_no
INNER JOIN LTR_FUND_DETAIL AS fd
    ON c.fund_no = fd.fund_no
LEFT OUTER JOIN TR_CONT_DESIGNATION AS cd
    ON fd.designation = cd.id
LEFT OUTER JOIN LTR_ACCT_GOAL AS ag1
    ON fd.acct_goal_no = ag1.id
LEFT OUTER JOIN LTR_ACCT_GRP AS ag2
    ON fd.acct_grp_no = ag2.id
INNER JOIN TR_CAMPAIGN_CATEGORY AS cc
    ON g.category = cc.id
INNER JOIN T_CUSTOMER AS i
    ON t.creditee_no = i.customer_no
INNER JOIN TR_CUST_TYPE AS ct
    ON i.cust_type = ct.id
LEFT OUTER JOIN LV_ADV_BEST_NAME AS b
    ON i.customer_no = b.customer_no
LEFT OUTER JOIN LV_ADV_BEST_NAME AS b2
    ON c.customer_no = b2.customer_no
WHERE c.cont_type = 'G'
      AND c.custom_1 = 'Primary Soft Credit'
      AND t.creditee_type IN (12, 15, 5)
      AND CAST(c.cont_dt AS DATE) BETWEEN @startDate AND @endDate
      AND c.cont_amt > 0
      AND c.customer_no <> 2653093
      AND g.category <> 24

UNION ALL 

-- PLEDGE PAYMENT
SELECT c.ref_no,
    c.customer_no,
    b.best_name main_donor,
    b.best_name hardcredit_donor,
    CASE
        WHEN n.trn_type = 6 THEN 'Adj_PlgPay'
        ELSE 'PlgPay'
    END AS pay_type,
    p.pmt_dt pay_dt,
    p.pmt_amt pay_amt,
    f.description fund_description,
    (
        SELECT fyear
        FROM TR_Batch_Period
        WHERE CAST(p.pmt_dt AS DATE) >= start_dt
              AND CAST(p.pmt_dt AS DATE) <= end_dt
    ) fyear,
	CASE 
		WHEN LEFT(fd.notes, 2) = 'EF' THEN 'Enterprise Fund'
		WHEN fd.acct_goal_no = 3 THEN 'Unrestricted'
        WHEN fd.acct_grp_no = 10 AND fd.acct_goal_no <> 3 THEN 'Unrestricted'
        ELSE 'Restricted' 
	END EF_Unr_Rest,
    CASE
        WHEN fd.acct_goal_no = 3
            AND LEFT(fd.notes, 2) <> 'EF' THEN 'Unr_NotEF'
        WHEN fd.acct_grp_no = 10
            AND fd.acct_goal_no <> 3
            AND LEFT(fd.notes, 2) <> 'EF' THEN 'Unr_NotEF'
        ELSE ''
    END AS unr_notef_flag,
    CASE
        WHEN fd.acct_goal_no IN (1, 2, 4)
            AND fd.acct_grp_no = 10 THEN 'Rest_BdgtRel'
        ELSE ''
    END AS rest_bdgtrel_flag,
    cc.description cmcategory,
    CASE
        WHEN g.category IN (7, 28) THEN 'Annual Giving'
        WHEN g.category IN (45, 40) THEN 'Leadership Giving'
        WHEN g.category IN (10, 31) THEN 'Corporate'
        WHEN g.category IN (46, 11, 6, 36, 37, 14) THEN 'Fundraising Events'
        WHEN g.category = 29 THEN 'Planned Gifts'
        ELSE cc.description
    END AS advrevrpt,
    cd.description designation,
    ag1.description acctgoal,
    ag2.description acctgrp,
    ct.description cust_type,
    i.sort_name
FROM T_TRANSACTION AS n
INNER JOIN T_CONTRIBUTION AS c
    ON n.ref_no = c.ref_no
INNER JOIN T_CAMPAIGN AS g
    ON c.campaign_no = g.campaign_no
INNER JOIN T_FUND AS f
    ON c.fund_no = f.fund_no
INNER JOIN T_PAYMENT AS p
    ON n.sequence_no = p.sequence_no
INNER JOIN LTR_FUND_DETAIL AS fd
    ON c.fund_no = fd.fund_no
LEFT OUTER JOIN TR_CONT_DESIGNATION AS cd
    ON fd.designation = cd.id
LEFT OUTER JOIN LTR_ACCT_GOAL AS ag1
    ON fd.acct_goal_no = ag1.id
LEFT OUTER JOIN LTR_ACCT_GRP AS ag2
    ON fd.acct_grp_no = ag2.id
INNER JOIN TR_CAMPAIGN_CATEGORY AS cc
    ON g.category = cc.id
INNER JOIN T_CUSTOMER AS i
    ON c.customer_no = i.customer_no
LEFT OUTER JOIN LV_ADV_BEST_NAME AS b
    ON i.customer_no = b.customer_no
INNER JOIN TR_CUST_TYPE AS ct
    ON i.cust_type = ct.id
WHERE c.custom_1 IN ('(none)', 'Matching Gift Credit')
      AND c.customer_no > 0
      AND c.cont_amt > 0
      AND c.customer_no <> 2653093
      AND n.trn_type IN (3, 6)
      AND CAST(p.pmt_dt AS DATE) BETWEEN @startDate AND @endDate
      AND g.category <> 24

UNION ALL 

-- PLEDGE PAYMENTS - CREDITEE
SELECT c.ref_no,
    i.customer_no,
    b.best_name main_donor,
    b2.best_name hardcredit_donor,
    CASE
        WHEN n.trn_type = 6 THEN 'Adj_PlgPay'
        ELSE 'PlgPay'
    END AS pay_type,
    p.pmt_dt pay_dt,
    p.pmt_amt pay_amt,
    f.description fund_description,
    (
        SELECT fyear
        FROM TR_Batch_Period
        WHERE CAST(p.pmt_dt AS DATE) >= start_dt
              AND CAST(p.pmt_dt AS DATE) <= end_dt
    ) fyear,
	CASE 
		WHEN LEFT(fd.notes, 2) = 'EF' THEN 'Enterprise Fund'
		WHEN fd.acct_goal_no = 3 THEN 'Unrestricted'
        WHEN fd.acct_grp_no = 10 AND fd.acct_goal_no <> 3 THEN 'Unrestricted'
        ELSE 'Restricted' 
	END EF_Unr_Rest,
    CASE
        WHEN fd.acct_goal_no = 3
            AND LEFT(fd.notes, 2) <> 'EF' THEN 'Unr_NotEF'
        WHEN fd.acct_grp_no = 10
            AND fd.acct_goal_no <> 3
            AND LEFT(fd.notes, 2) <> 'EF' THEN 'Unr_NotEF'
        ELSE ''
    END AS unr_notef_flag,
    CASE
        WHEN fd.acct_goal_no IN (1, 2, 4)
            AND fd.acct_grp_no = 10 THEN 'Rest_BdgtRel'
        ELSE ''
    END AS rest_bdgtrel_flag,
    cc.description cmcategory,
    CASE
        WHEN g.category IN (7, 28) THEN 'Annual Giving'
        WHEN g.category IN (45, 40) THEN 'Leadership Giving'
        WHEN g.category IN (10, 31) THEN 'Corporate'
        WHEN g.category IN (46, 11, 6, 36, 37, 14) THEN 'Fundraising Events'
        WHEN g.category = 29 THEN 'Planned Gifts'
        ELSE cc.description
    END AS advrevrpt,
    cd.description designation,
    ag1.description acctgoal,
    ag2.description acctgrp,
    ct.description cust_type,
    i.sort_name
FROM T_TRANSACTION AS n
INNER JOIN T_CONTRIBUTION AS c
    ON n.ref_no = c.ref_no
INNER JOIN T_CAMPAIGN AS g
    ON c.campaign_no = g.campaign_no
INNER JOIN T_FUND AS f
    ON c.fund_no = f.fund_no
INNER JOIN T_PAYMENT AS p
    ON n.sequence_no = p.sequence_no
INNER JOIN T_CREDITEE AS t
    ON c.ref_no = t.ref_no
INNER JOIN LTR_FUND_DETAIL AS fd
    ON c.fund_no = fd.fund_no
LEFT OUTER JOIN TR_CONT_DESIGNATION AS cd
    ON fd.designation = cd.id
LEFT OUTER JOIN LTR_ACCT_GOAL AS ag1
    ON fd.acct_goal_no = ag1.id
LEFT OUTER JOIN LTR_ACCT_GRP AS ag2
    ON fd.acct_grp_no = ag2.id
INNER JOIN TR_CAMPAIGN_CATEGORY AS cc
    ON g.category = cc.id
INNER JOIN T_CUSTOMER AS i
    ON t.creditee_no = i.customer_no
LEFT OUTER JOIN LV_ADV_BEST_NAME AS b
    ON i.customer_no = b.customer_no
INNER JOIN TR_CUST_TYPE AS ct
    ON i.cust_type = ct.id
LEFT OUTER JOIN LV_ADV_BEST_NAME AS b2
    ON c.customer_no = b2.customer_no
WHERE c.custom_1 = ('Primary Soft Credit')
      AND c.customer_no > 0
      AND c.cont_amt > 0
      AND t.creditee_type IN (12, 15, 5)
      AND n.trn_type IN (3, 6)
      AND CAST(p.pmt_dt AS DATE) BETWEEN @startDate AND @endDate
      AND g.category <> 24

SELECT 
	[ref_no]
	,[customer_no]
	,[main_donor]
	,[hardcredit_donor]
	,[pay_type]
	,[pay_dt]
	,[pay_amt]
	,[fund_description]
	,[fyear]
	,[EF_Unr_Rest]
	,[unr_notef_flag]
	,[rest_bdgtrel_flag]
	,[cmcategory]
	,[advrevrpt]
	,[designation]
	,[acctgoal]
	,[acctgrp]
	,[cust_type]
	,[sort_name]
 FROM @results
 WHERE
 (@GIK = 0 AND pay_type = pay_type) -- all records
 OR
 (@GIK = 1 AND pay_type = 'GIK') -- only GIK
