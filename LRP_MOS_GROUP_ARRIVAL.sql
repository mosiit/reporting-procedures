USE [impresario]
GO
/****** Object:  StoredProcedure [dbo].[LRP_MOS_GROUP_ARRIVAL]    Script Date: 6/17/2021 12:38:39 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




/* =============================================

PLEASE NOTE! This procedure is also called by LRP_SchoolChaperoneLabels.
Any changes to this output will need to be accounted for in the chaperone output as
well.	--11/17/2016 TSR

-- Author:		Alex Harris for Tessitura Network
-- Create date: May 27, 2014
-- Description:	Group Arrival Sheet

Modified 10/04/2016 TSR to add recipient info.
Modified 10/11/2016 TSR to add additional parameters:
	 - Customer numbers to include
	 - Owner or Initiator?
	 - Customer numbers to exclude
	 - Owner or initiator?
-- ============================================= */

/*
Sample Exec:
EXEC LRP_MOS_GROUP_ARRIVAL
	@order_start_dt = '01/01/1900',
	@order_end_dt = '12/31/2999',
	@perf_start_dt = '2016/06/02',
	@perf_end_dt = '2016/06/30',
	@mos_str = '12,19',
	@facility_str = NULL,
	@price_types = NULL,
	@transportation = 'Coach/School Bus',
	@customer_include = '3178006,3179652,3178944',
	@include_owner_initiator = 1,
	@customer_exclude = null,
	@exclude_owner_initiator = null 
*/

ALTER PROCEDURE [dbo].[LRP_MOS_GROUP_ARRIVAL] (
	@order_start_dt datetime,
	@order_end_dt datetime,
	@perf_start_dt datetime,
	@perf_end_dt datetime,
	@mos_str varchar(max),
	@facility_str varchar(max) = NULL,
	@price_types varchar(max) = NULL,
	@lunch varchar(max) = NULL,
	@transportation varchar(max) = NULL,
	@arrival_times varchar(max) = NULL,
	@departure_times varchar(max) = NULL,
	@customer_include varchar(max) = null,
	@include_owner_initiator int = null,	-- 1=owner, 2=initiator
	@customer_exclude varchar(max) = null,
	@exclude_owner_initiator int = null	-- 1=owner, 2=initiator
)

AS
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

DECLARE	@crp_vouchers_prod INT,
		@grp_vouchers_prod INT,
		@npr_vouchers_prod INT,
		@sch_vouchers_prod INT,
		@soc_vouchers_prod INT,
		@exhibit_hall_title INT

SET		@crp_vouchers_prod = 6088
SET		@grp_vouchers_prod = 7183
SET		@npr_vouchers_prod = 7184
SET		@sch_vouchers_prod = 7185
SET		@soc_vouchers_prod = 7186
SET		@exhibit_hall_title = 27

--Create and populate a temp table to hold the desired Modes of Sale
CREATE TABLE #mos (
	id int not null,
	mos int not null
)

INSERT INTO #mos
SELECT ElementID, CONVERT(int,Element) FROM dbo.FT_SPLIT_LIST (@mos_str,',')


--Create and populate a temp table to hold the desired Facilities
CREATE TABLE #facility (
	facility int not null
)

IF ISNULL(@facility_str,'') <> ''
  BEGIN
	INSERT INTO #facility
	SELECT CONVERT(int,Element) FROM dbo.FT_SPLIT_LIST(@facility_str,',')
  END
ELSE
  BEGIN
    INSERT INTO #facility
	SELECT facil_no FROM dbo.T_FACILITY
  END


--Create a table to hold the final results for this report
CREATE TABLE #results (
	order_no INT NOT NULL,
	customer_no INT NULL,
	owner VARCHAR(255) NULL,
	initiator_customer_no int null,
	initiator VARCHAR(255) NULL,
	recipient varchar(255),		-- 10/04/2016 TSR
	sort_name VARCHAR(255) NULL,
	total_due MONEY NULL,
	total_paid MONEY NULL,
	order_notes VARCHAR(255) NULL,
	perf_no INT NULL,
	perf_desc VARCHAR(30) NULL,
	facility VARCHAR(30) NULL,
	zone VARCHAR(30) NULL,
	price_type VARCHAR(30) NULL,
	perf_dt DATETIME NULL,
	master_perf_dt DATETIME NULL,
	num_seats INT NULL,
	group_count INT NULL,
	billing_info VARCHAR(255) NULL,
	arrival VARCHAR(255) NULL,
	departure VARCHAR(255) NULL,
	grade VARCHAR(255) NULL,
	transportation VARCHAR(255) NULL,
	lunch VARCHAR(255) NULL,
	po_number VARCHAR(255) NULL,
	master_daily_count int null
	)


--First insert all of the non-cafe voucher performances into the results table
INSERT INTO #results
SELECT	order_no = a.order_no,
		customer_no = a.customer_no,
		owner = b.display_name,
		initiator_customer_no = a.initiator_no,
		initiator = ISNULL(c.display_name,''),
		recipient = isnull(d.recipient_name,''),
		sort_name = b.sort_name,
		total_due = ISNULL(a.tot_due_amt,0),
		total_paid = ISNULL(a.tot_paid_amt,0),
		order_notes = ISNULL(a.notes,''),
		perf_no = d.perf_no,
		perf_desc = f.description,
		facility = fa.description,
		zone = z.description,
		price_type = pt.description,
		perf_dt = e.perf_dt,
		master_perf_dt = Convert(datetime,Convert(date,e.perf_dt)),
		num_seats = d.num_seats,
		group_count = 0,
		billing_info = a.custom_2,
		arrival = LTRIM(a.custom_3),
		departure = LTRIM(a.custom_4),
		grade = a.custom_5,
		transportation = a.custom_6,
		lunch = a.custom_7,
		po_number = a.custom_8,
		master_daily_count = 0 

FROM	T_ORDER AS a
JOIN	dbo.FT_CONSTITUENT_DISPLAY_NAME() AS b ON a.customer_no = b.customer_no
LEFT JOIN	dbo.FT_CONSTITUENT_DISPLAY_NAME() AS c ON a.initiator_no = c.customer_no
--JOIN	(SELECT order_no, perf_no, zone_no, price_type, count(sli_no) AS 'num_seats'
--		FROM T_SUB_LINEITEM 
--		WHERE sli_status IN (1,2,3,6,12)
--		GROUP BY order_no, perf_no, zone_no, price_type) AS d ON a.order_no = d.order_no		--10/04/2016 TSR
JOIN	(SELECT order_no, perf_no, zone_no, price_type, b.display_name as recipient_name,count(sli_no) AS 'num_seats'
		FROM T_SUB_LINEITEM a
		left join FT_CONSTITUENT_DISPLAY_NAME() b on b.customer_no = a.recipient_no
		WHERE sli_status IN (1,2,3,6,12)
		GROUP BY order_no, perf_no, zone_no, price_type, b.display_name) AS d ON a.order_no = d.order_no
JOIN	T_PERF AS e ON d.perf_no = e.perf_no AND e.perf_dt BETWEEN @perf_start_dt AND @perf_end_dt
JOIN	T_PROD_SEASON AS ps ON e.prod_season_no = ps.prod_season_no
JOIN	T_FACILITY AS fa ON e.facility_no = fa.facil_no
JOIN	T_INVENTORY AS f ON e.perf_no = f.inv_no
JOIN	TR_PRICE_TYPE AS pt ON d.price_type = pt.id
JOIN	T_ZONE AS z ON d.zone_no = z.zone_no
JOIN	#mos AS g ON a.mos = g.mos
JOIN	#facility AS fac ON e.facility_no = fac.facility
WHERE	a.order_dt BETWEEN @order_start_dt AND @order_end_dt
AND		(ISNULL(@price_types,'') = '' OR CHARINDEX(','+CONVERT(VARCHAR,d.price_type)+',',','+@price_types+',') > 0)
AND		(ISNULL(@lunch,'') = '' OR CHARINDEX(','+a.custom_7+',',','+REPLACE(@lunch,'"','')+',') > 0)
AND		(ISNULL(@transportation,'') = '' OR CHARINDEX(','+a.custom_6+',',','+REPLACE(@transportation,'"','')+',') > 0)
AND		(ISNULL(@arrival_times,'') = '' OR CHARINDEX(','+a.custom_3+',',','+REPLACE(@arrival_times,'"','')+',') > 0)
AND		(ISNULL(@departure_times,'') = '' OR CHARINDEX(','+a.custom_4+',',','+REPLACE(@departure_times,'"','')+',') > 0)
AND		ps.prod_no NOT IN (@crp_vouchers_prod, @grp_vouchers_prod, @npr_vouchers_prod, @sch_vouchers_prod, @soc_vouchers_prod)


--Second insert all of the cafe voucher performances into the results table
--HOWEVER: Link them to the performance dates of all other performances in the same order
;WITH work AS (
SELECT	DISTINCT sli.order_no,
		master_perf_dt = CONVERT(datetime,CONVERT(date,p.perf_dt))
FROM	T_SUB_LINEITEM AS sli
JOIN	T_PERF as p on sli.perf_no = p.perf_no
JOIN	T_PROD_SEASON AS ps ON p.prod_season_no = ps.prod_season_no
WHERE	ps.prod_no NOT IN (@crp_vouchers_prod, @grp_vouchers_prod, @npr_vouchers_prod, @sch_vouchers_prod, @soc_vouchers_prod)
AND		p.perf_dt BETWEEN @perf_start_dt AND @perf_end_dt
AND		sli.sli_status IN (1,2,3,6,12))
INSERT INTO #results
SELECT	order_no = a.order_no,
		customer_no = a.customer_no,
		owner = b.display_name,
		initiator_customer_no = isnull(a.initiator_no,''),
		initiator = ISNULL(c.display_name,''),
		recipient = isnull(d.recipient_name,''),
		sort_name = b.sort_name,
		total_due = ISNULL(a.tot_due_amt,0),
		total_paid = ISNULL(a.tot_paid_amt,0),
		order_notes = ISNULL(a.notes,''),
		perf_no = d.perf_no,
		perf_desc = f.description,
		facility = fa.description,
		zone = 'VOUCH',
		price_type = pt.description,
		perf_dt = DATEADD(ms, -3, DATEADD(d, 1, w.master_perf_dt)),
		master_perf_dt = Convert(datetime,Convert(date,w.master_perf_dt)),
		num_seats = d.num_seats,
		group_count = 0,
		billing_info = a.custom_2,
		arrival = LTRIM(a.custom_3),
		departure = LTRIM(a.custom_4),
		grade = a.custom_5,
		transportation = a.custom_6,
		lunch = a.custom_7,
		po_number = a.custom_8,
		master_daily_count = 0 

FROM	T_ORDER AS a
JOIN	dbo.FT_CONSTITUENT_DISPLAY_NAME() AS b ON a.customer_no = b.customer_no
LEFT JOIN	dbo.FT_CONSTITUENT_DISPLAY_NAME() AS c ON a.initiator_no = c.customer_no
--JOIN	(SELECT order_no, perf_no, zone_no, price_type, count(sli_no) AS 'num_seats'
--		FROM T_SUB_LINEITEM 
--		WHERE sli_status IN (1,2,3,6,12)
--		GROUP BY order_no, perf_no, zone_no, price_type) AS d ON a.order_no = d.order_no		--10/04/2016 TSR
JOIN	(SELECT order_no, perf_no, zone_no, price_type, b.display_name as recipient_name,count(sli_no) AS 'num_seats'
		FROM T_SUB_LINEITEM a
		left join FT_CONSTITUENT_DISPLAY_NAME() b on b.customer_no = a.recipient_no
		WHERE sli_status IN (1,2,3,6,12)
		GROUP BY order_no, perf_no, zone_no, price_type, b.display_name) AS d ON a.order_no = d.order_no
JOIN	T_PERF AS e ON d.perf_no = e.perf_no
JOIN	T_PROD_SEASON AS ps ON e.prod_season_no = ps.prod_season_no
JOIN	T_FACILITY AS fa ON e.facility_no = fa.facil_no
JOIN	T_INVENTORY AS f ON e.perf_no = f.inv_no
JOIN	TR_PRICE_TYPE AS pt ON d.price_type = pt.id
JOIN	T_ZONE AS z ON d.zone_no = z.zone_no
JOIN	work AS w ON a.order_no = w.order_no
JOIN	#mos AS g ON a.mos = g.mos
JOIN	#facility AS fac ON e.facility_no = fac.facility
WHERE	a.order_dt BETWEEN @order_start_dt AND @order_end_dt
AND		(ISNULL(@price_types,'') = '' OR CHARINDEX(','+CONVERT(VARCHAR,d.price_type)+',',','+@price_types+',') > 0)
AND		(ISNULL(@lunch,'') = '' OR CHARINDEX(','+a.custom_7+',',','+REPLACE(@lunch,'"','')+',') > 0)
AND		(ISNULL(@transportation,'') = '' OR CHARINDEX(','+a.custom_6+',',','+REPLACE(@transportation,'"','')+',') > 0)
AND		(ISNULL(@arrival_times,'') = '' OR CHARINDEX(','+a.custom_3+',',','+REPLACE(@arrival_times,'"','')+',') > 0)
AND		(ISNULL(@departure_times,'') = '' OR CHARINDEX(','+a.custom_4+',',','+REPLACE(@departure_times,'"','')+',') > 0)
AND		ps.prod_no IN (@crp_vouchers_prod, @grp_vouchers_prod, @npr_vouchers_prod, @sch_vouchers_prod, @soc_vouchers_prod)


--Update the total visitor count for orders that have Exhibit Halls
;with work as (SELECT r.order_no, sum(r.num_seats) As group_count
	FROM	#results AS r
	JOIN	dbo.T_PERF AS p ON r.perf_no = p.perf_no
	JOIN	dbo.T_PROD_SEASON AS ps ON p.prod_season_no = ps.prod_season_no
	JOIN	dbo.T_PRODUCTION AS pr ON ps.prod_no = pr.prod_no
	WHERE	pr.title_no IN (@exhibit_hall_title)
	GROUP BY r.order_no)
UPDATE	r
SET		r.group_count = w.group_count
FROM	#results AS r
JOIN	work AS w ON r.order_no = w.order_no

--Update the total visitor count for orders that don't have Exhibit Halls
;with work as (SELECT r.order_no, r.perf_no, sum(r.num_seats) AS group_count,
	row_number() over (partition by r.order_no order by sum(r.num_seats) desc) AS seq
	FROM	#results AS r
	JOIN	dbo.T_PERF AS p ON r.perf_no = p.perf_no
	JOIN	dbo.T_PROD_SEASON AS ps ON p.prod_season_no = ps.prod_season_no
	JOIN	dbo.T_PRODUCTION AS pr ON ps.prod_no = pr.prod_no
	WHERE	pr.title_no NOT IN (@exhibit_hall_title)
	GROUP BY r.order_no, r.perf_no)
UPDATE	r
SET		r.group_count = w.group_count
FROM	#results AS r
JOIN	work AS w ON r.order_no = w.order_no AND w.seq = 1
WHERE	r.group_count = 0
;

select distinct(r.order_no), r.group_count 
into #master_results from #results as r  
where r.group_count > 0 
group by r.order_no, r.group_count

declare @master_count int 
set @master_count = (select sum(mdc.group_count) 
from #master_results mdc )

--Select the final results
if @customer_include is not null and @include_owner_initiator = 1		-- include owner
begin
	SELECT	order_no,
			customer_no,
			owner,
			initiator,
			recipient,
			sort_name,
			total_due,
			total_paid,
			order_notes,
			perf_no,
			perf_desc,
			facility,
			zone,
			price_type,
			perf_dt,
			master_perf_dt,
			num_seats,
			group_count,
			arrival,
			departure,
			grade,
			transportation,
			lunch,
			po_number
	FROM	#results
	where customer_no in (select element from ft_split_list(@customer_include,','))
	ORDER BY sort_name, customer_no, order_no, perf_dt, zone, price_type
end

if @customer_include is not null and @include_owner_initiator = 2		-- include initiator
begin
	SELECT	order_no,
			customer_no,
			owner,
			initiator,
			recipient,
			sort_name,
			total_due,
			total_paid,
			order_notes,
			perf_no,
			perf_desc,
			facility,
			zone,
			price_type,
			perf_dt,
			master_perf_dt,
			num_seats,
			group_count,
			arrival,
			departure,
			grade,
			transportation,
			lunch,
			po_number
	FROM	#results
	where initiator_customer_no in (select element from ft_split_list(@customer_include,','))
	ORDER BY sort_name, customer_no, order_no, perf_dt, zone, price_type
end

if @customer_exclude is not null and @exclude_owner_initiator = 1		-- exclude owner
begin
	SELECT	order_no,
			customer_no,
			owner,
			initiator,
			recipient,
			sort_name,
			total_due,
			total_paid,
			order_notes,
			perf_no,
			perf_desc,
			facility,
			zone,
			price_type,
			perf_dt,
			master_perf_dt,
			num_seats,
			group_count,
			arrival,
			departure,
			grade,
			transportation,
			lunch,
			po_number
	FROM	#results
	where customer_no not in (select element from ft_split_list(@customer_exclude,','))
	ORDER BY sort_name, customer_no, order_no, perf_dt, zone, price_type
end

if @customer_exclude is not null and @exclude_owner_initiator = 2		-- exclude initiator
begin
	SELECT	order_no,
			customer_no,
			owner,
			initiator,
			recipient,
			sort_name,
			total_due,
			total_paid,
			order_notes,
			perf_no,
			perf_desc,
			facility,
			zone,
			price_type,
			perf_dt,
			master_perf_dt,
			num_seats,
			group_count,
			arrival,
			departure,
			grade,
			transportation,
			lunch,
			po_number
	FROM	#results
	where initiator_customer_no not in (select element from ft_split_list(@customer_exclude,','))
	ORDER BY sort_name, customer_no, order_no, perf_dt, zone, price_type
end
-- 
-- calculate counts for final daily numbers 
-- based on price types to include 
-- 

if @customer_exclude is null 
and @customer_include is null

begin
	SELECT	order_no,
			customer_no,
			owner,
			initiator,
			recipient,
			sort_name,
			total_due,
			total_paid,
			order_notes,
			perf_no,
			perf_desc,
			facility,
			zone,
			price_type,
			perf_dt,
			master_perf_dt,
			num_seats,
			group_count,
			arrival,
			departure,
			grade,
			transportation,
			lunch,
			po_number,
			master_daily_count = @master_count 
	FROM	#results
	ORDER BY sort_name, customer_no, order_no, perf_dt, zone, price_type
end

END




