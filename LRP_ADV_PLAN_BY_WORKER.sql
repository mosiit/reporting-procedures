USE [impresario]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[LRP_ADV_PLAN_BY_WORKER]
(
	@worker_customer_no INT = NULL,
	@plan_start_dt DATETIME,
	@plan_end_dt DATETIME  
)
AS

SET NOCOUNT ON 
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

WITH worker 
AS
(
	SELECT  a.customer_no, d2.description AS department, c.fname AS worker
	FROM LT_ATTRIBUTE_DETAIL AS a
	LEFT OUTER JOIN LTR_ATTDET_DESCR2 AS d2
		ON a.descr2_no = d2.id
	INNER JOIN T_CUSTOMER AS c
		ON a.customer_no = c.customer_no
	WHERE a.type_no = 15 -- Prospect Management Record
		AND a.descr2_no <> 1 
		AND c.customer_no = @worker_customer_no
)
SELECT 
	p.plan_no,
	w.worker, 
	w.department, 
	rl.description AS WorkerRole, 
	p.start_dt planStartDt, 
	p.complete_by_dt AS planCloseDt,
	stat.description AS planStatusDesc, 
	CASE p.status
		WHEN 1 THEN 'Open'
		WHEN 2 THEN 'Open'
		WHEN 4 THEN 'Open'
		WHEN 22 THEN 'Solicitation'
		WHEN 23 THEN 'Solicitation'
		WHEN 26 THEN 'Closed'
		WHEN 35 THEN 'Closed'
		WHEN 37 THEN 'Closed'
		WHEN 39 THEN 'Closed'
		ELSE NULL
	END AS planStatusType, 
	p.ask_amt,
	desi.description planDesignation,
	p.custom_1 AS planTitle,
	cam.campaign_no, 
	cam.description AS campaign,
	cat.description AS campaignCat,
	cam.fyear AS campaignFY,
	CASE 
		WHEN cp.role_no = 1 AND cp.primary_ind = 'Y' THEN 'Direct'
		WHEN cp.role_no = 11 THEN 'Indirect'
		ELSE 'Not Applicable'
	END AS Direct,
	p.customer_no,
	cust.display_name,
	cust.sort_name,
	cont.ref_no, 
	cont.cont_dt, 
	cont.cont_amt
--SELECT TOP 100 * 
FROM  t_plan p
INNER JOIN dbo.TR_PLAN_STATUS stat
	ON stat.id = p.status
	AND stat.inactive = 'N'
INNER JOIN tx_cust_plan  cp
	ON cp.plan_no = p.plan_no
INNER JOIN dbo.TR_WORKER_ROLE rl
	ON rl.id = cp.role_no
INNER JOIN worker w
	ON cp.customer_no = w.customer_no
INNER JOIN dbo.T_CAMPAIGN cam
	ON cam.campaign_no = p.campaign_no
INNER JOIN dbo.TR_CAMPAIGN_CATEGORY cat
	ON cam.category = cat.id
INNER JOIN dbo.FT_CONSTITUENT_DISPLAY_NAME() AS cust 
	ON p.customer_no = cust.customer_no
LEFT JOIN dbo.T_CONTRIBUTION cont
	ON p.plan_no = cont.plan_no
LEFT JOIN dbo.TR_CONT_DESIGNATION desi
	ON desi.id = p.cont_designation
WHERE p.start_dt BETWEEN @plan_start_dt AND @plan_end_dt
