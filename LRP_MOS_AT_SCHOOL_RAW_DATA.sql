USE impresario;
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_MOS_AT_SCHOOL_RAW_DATA]') AND type in (N'P', N'PC'))
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_MOS_AT_SCHOOL_RAW_DATA] AS';
GO

EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LRP_MOS_AT_SCHOOL_RAW_DATA] TO [impusers], [tessitura_app]';
GO

ALTER PROCEDURE [dbo].[LRP_MOS_AT_SCHOOL_RAW_DATA]
        @report_start_dt DATETIME = NULL,
        @report_end_dt DATETIME = NULL
AS BEGIN

    SET NOCOUNT ON;
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

    /*  Procedure Variables and Temp Tables  */

        DECLARE @total_participants DECIMAL(18,4) = 0
        DECLARE @total_groups DECIMAL(18,4) = 0

        DECLARE @econ_dis_percentage AS INTEGER = 25
        DECLARE @econ_lunch_percentage AS INTEGER = 35

        IF OBJECT_ID('tempdb..#mos_at_school_raw') IS NOT NULL DROP TABLE [#mos_at_school_raw];

        CREATE TABLE [#mos_at_school_raw] ([title_no] INT,
                                           [title_name] VARCHAR(30),
                                           [order_no] INT,
                                           [order_category] VARCHAR(30),
                                           [customer_no] INT,
                                           [consituentName] VARCHAR(100),
                                           [sort_name] VARCHAR(100),
                                           [city] VARCHAR(50),
                                           [state] VARCHAR(50),
                                           [postal_code] VARCHAR(50),
                                           [initiator_cust_no] INT,
                                           [initiatorName] VARCHAR(100),
                                           [InitiatorEmail] VARCHAR(255),
                                           [InitiatorEmailType] VARCHAR(30),
                                           [grades] VARCHAR(100),
                                           [arrivalTime] VARCHAR(30),
                                           [perf_code] VARCHAR(20),
                                           [production_name] VARCHAR(30),
                                           [visitDate] DATETIME,
                                           [price_type_name] VARCHAR(30),
                                           [order_notes] VARCHAR(1000),
                                           [zone_enabled] CHAR(1),
                                           [zone_descr] VARCHAR(30),
                                           [zone_short_descr] VARCHAR(30),
                                           [cnt] INT,
                                           [disadv_counter] INT,
                                           [free_lunch_counter] INT);

    /*  Check Parameters  */

        IF @report_start_dt IS NULL
            SELECT @report_start_dt = MIN(performance_dt) 
                                 FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] 
                                 WHERE title_no = 84069     --84069 = MOS at School

        IF @report_end_dt IS NULL
            SELECT @report_end_dt = MAX(performance_dt) 
                               FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] 
                              WHERE title_no = 84069     --84069 = MOS at School

        SELECT @report_start_dt = CAST(@report_start_dt AS DATE)
        SELECT @report_end_dt = (FORMAT(@report_end_dt,'MM/dd/yyyy') + ' 11:59:59.957')

    /*  Get Raw Data Using the Advanced Reservations By Production Report  */

                               
        INSERT INTO [#mos_at_school_raw]([title_no],[title_name],[order_no],[order_category],[customer_no],[consituentName],[sort_name],[city],[state],
                                         [postal_code],[initiator_cust_no],[initiatorName],[InitiatorEmail],[InitiatorEmailType],[grades],[arrivalTime],
                                         [perf_code],[production_name],[visitDate],[price_type_name],[order_notes],[zone_enabled],[zone_descr],[zone_short_descr],[cnt])
        EXECUTE [dbo].[LRP_ADVANCED_RESERVATIONS_BY_PRODUCTION]
    	        @order_start_dt = NULL,     @order_end_dt = NULL,               @visit_start_dt = @report_start_dt,     @visit_end_dt = @report_end_dt, 
                @title_str = '84069',       @include_disabled_zones = 'N',      @list_no = 0;


    /*  Determine Economically Disadvanteged and Free Lunch Numbers  */

        WITH [CTE_DISADV] (customer_no, econ_dis, free_lunch)
        AS (SELECT DISTINCT mas.[customer_no],
                            CASE WHEN ISNULL(kw1.[key_value],0) >= @econ_dis_percentage THEN 'Y' ELSE 'N' END AS [EconomicallyDisadvantaged],
                            CASE WHEN ISNULL(kw2.[key_value],0) >= @econ_lunch_percentage THEN 'Y' ELSE 'N' END AS [TotalFreeandReducedLunch]
           FROM [#mos_at_school_raw] AS mas
                LEFT OUTER JOIN [dbo].[TX_CUST_KEYWORD] AS kw1 ON kw1.[customer_no] = mas.[customer_no] AND kw1.[keyword_no] = 483
                LEFT OUTER JOIN [dbo].[TX_CUST_KEYWORD] AS kw2 ON kw2.[customer_no] = mas.[customer_no] AND kw2.[keyword_no] = 404)
        UPDATE [mas]
        SET mas.[disadv_counter] = CASE WHEN cte.[econ_dis] = 'Y' THEN 1 ELSE 0 END,
            mas.[free_lunch_counter] = CASE WHEN cte.[free_lunch] = 'Y' THEN 1 ELSE 0 END
        FROM [#mos_at_school_raw] AS mas
             LEFT OUTER JOIN [CTE_DISADV] AS cte ON cte.[customer_no] = mas.[customer_no];

    FINISHED:

        /*  Final Data Set For the Report  */

            SELECT [title_no],
                   [title_name],
                   [order_no],
                   [order_category],
                   [customer_no],
                   [consituentName],
                   [sort_name],
                   [city],
                   [state],
                   [postal_code],
                   [initiator_cust_no],
                   [initiatorName],
                   [InitiatorEmail],
                   [InitiatorEmailType],
                   [grades],
                   [arrivalTime],
                   [production_name],
                   [visitDate],
                   [price_type_name],
                   [order_notes],
                   [zone_enabled],
                   [zone_descr],
                   [zone_short_descr],
                   [cnt],
                   [disadv_counter],
                   [free_lunch_counter],
                   [dbo].[LF_GetFiscalYear]([visitDate]) AS [fiscal_year]
            FROM [#mos_at_school_raw];

            --SELECT * FROM [#mos_at_school_raw]

    DONE:

        IF OBJECT_ID('tempdb..#mos_at_school') IS NOT NULL DROP TABLE [#mos_at_school];
        --IF OBJECT_ID('tempdb..#mos_at_school_raw') IS NOT NULL DROP TABLE [#mos_at_school_raw];        

END
GO

EXECUTE [dbo].[LRP_MOS_AT_SCHOOL_STATS] @report_start_dt = '4-1-2021', @report_end_dt = '6-18-2021'

--EXECUTE [dbo].[LRP_MOS_AT_SCHOOL_RAW_DATA]-- @report_start_dt = '11-10-2020', @report_end_dt = '12-17-2020'



