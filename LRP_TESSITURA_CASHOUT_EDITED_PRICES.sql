
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_TESSITURA_CASHOUT_EDITED_PRICES]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LRP_TESSITURA_CASHOUT_EDITED_PRICES]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE  [dbo].[LRP_TESSITURA_CASHOUT_EDITED_PRICES]
        @report_dt datetime,
        @report_operator varchar(10),
        @machine_locations VARCHAR(4000)
AS BEGIN


    /*  Report Parameters  */
    DECLARE @report_end_dt datetime
    DECLARE @location_list TABLE ([location_no_str] VARCHAR(20), [location_no] INT)
    DECLARE @machine_list TABLE ([machine_name] VARCHAR(50))
    DECLARE @order_detail table ([create_dt] datetime, [created_by] varchar(10), [create_loc] VARCHAR(50), [order_no] int, [performance_date] char(10), [title_name] varchar(30), [production_name] varchar(30), 
                                 [zone_name] varchar(30), [price_type_name] varchar(30), [comp_code_name] varchar(30), [due_amount] decimal(18,2), 
                                 [current_price] decimal(18,2), [total_paid] decimal(18,2), [rule_ind] char(1), [rule_name] varchar(30))

    /*  Check Parameters  - Null Date = Today, Null Operator = All  */

        SELECT @report_dt = ISNULL(@report_dt, GETDATE())
        SELECT @report_operator = ISNULL(@report_operator,'')
        SELECT @machine_locations = ISNULL(@machine_locations,'')

        SELECT @report_dt = convert(char(10),@report_dt,111) + ' 00:00:00',
               @report_end_dt = convert(char(10),@report_dt,111) + ' 23:59:59.957'

          
    /*  Parse machine_locations parameter  */

        INSERT INTO @location_list ([location_no_str]) 
        SELECT [item] FROM [dbo].[LFT_SPLIT_DILEMETED_STRING] (@machine_locations, ',')

        DELETE FROM @location_list WHERE [dbo].[FS_isReallyNumeric] ([location_no_str]) = 0

        UPDATE @location_list SET [location_no] = CONVERT(INT,[location_no_str])

        INSERT INTO @machine_list ([machine_name])
        SELECT [machine_name] FROM [dbo].[TX_MACHINE_LOCATION] WHERE [location] IN (SELECT [location_no] FROM @location_list)


    /*  Get all order detail for this operator on this date from the LV_ORDER_DETAIL View  */

        INSERT INTO @order_detail
        SELECT [create_dt], [created_by], [create_loc], [order_no], [performance_date], [title_name], [production_name], [zone_name], [price_type_name], 
               [comp_code_name], [due_amount], [current_price], [total_paid], [rule_ind], [rule_name]
        FROM [dbo].[LV_ORDER_DETAIL]
        WHERE [create_dt] between @report_dt and @report_end_dt and [created_by] = @report_operator 
    
    /*  Delete where the amount due is the same as the current price or the negative current price (for refunds)  */
    
        DELETE FROM @order_detail WHERE [due_amount] = [current_price] or ([due_amount] * -1) = [current_price]

    /*  Delete where the prices have been edited by a pricing rule  */

        DELETE FROM @order_detail WHERE [rule_ind] = 'R' 

    /*  Delete from invalid machines  */

        IF EXISTS (SELECT * FROM @machine_list)
            DELETE FROM @order_detail WHERE [create_loc] NOT IN (SELECT [machine_name] FROM @machine_list)

    /*  Delete Returns  */

--        DELETE FROM @order_detail WHERE current_price < 0.00

    /*  Ignore membership upgrades  */

        DELETE FROM @order_detail WHERE [title_name] = 'Membership' and [zone_name] LIKE 'Upgrade%'
    
    FINISHED:

        /*  NOTE: Do not add a blank record with a "none found" message if no records are found.  
                  This will keep the subreport from showing up at all on the report if no edited prices exist.  */

        SELECT [create_dt], [created_by], [create_loc], [order_no], [performance_date], [title_name], [production_name], [zone_name], [price_type_name], [comp_code_name], 
               [due_amount], [current_price], [total_paid], [rule_ind], [rule_name]
        FROM @order_detail

END 
GO

GRANT EXECUTE ON [dbo].[LRP_TESSITURA_CASHOUT_EDITED_PRICES] TO impusers
GO


EXECUTE [dbo].[LRP_TESSITURA_CASHOUT_EDITED_PRICES] '4-22-2019', 'mdalto00', '15'
GO

--SELECT  DISTINCT convert(char(10),[create_dt],111), [created_by] FROM [dbo].[LV_ORDER_DETAIL] as odt WHERE [create_dt] between '4-1-2017' and '5-3-2017' and  [due_amount] <> [current_price] and ([due_amount] * -1) <> [current_price] and rule_ind <> 'R' and [zone_name] not LIKE 'Upgrade%'
--SELECT * FROM [dbo].[LV_ORDER_DETAIL] WHERE convert(char(10),[create_dt],111) = '2016/07/05' and title_name <> 'Membership' and rule_ind <> 'R' and current_price <> due_amount


    


--SELECT * FROM T_ORDER WHERE Order_dt > '10-8-2016'