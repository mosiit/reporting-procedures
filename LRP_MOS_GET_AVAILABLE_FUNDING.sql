USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LRP_MOS_GET_AVAILABLE_FUNDING]    Script Date: 8/16/2016 12:08:27 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Alex Harris for Tessitura Network
-- Create date: February 11, 2016
-- Description:	Funding Flow - Funding Available Report
--
-- Sample Exec:	Execute LRP_MOS_GET_AVAILABLE_FUNDING @funding_allocation_str = '3,7,10', @min_balance = 0
-- =============================================
CREATE PROCEDURE [dbo].[LRP_MOS_GET_AVAILABLE_FUNDING]

	@funding_allocation_str VARCHAR(MAX),
	@min_balance MONEY = 0.00

AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @allocations TABLE (allocation_id INT NULL)

	INSERT INTO @allocations (allocation_id)
	SELECT Element FROM dbo.FT_SPLIT_LIST(@funding_allocation_str,',')


	SELECT	sa.customer_no,
			sa.allocation,
			allocation_category = sac.description,
			cs.esal1_desc,
			notes = ISNULL(sa.notes,''),
			allocation_amt = ISNULL(sa.allocation_amt,0),
			pmt_amt = SUM(ISNULL(p.pmt_amt,0))
	FROM	[dbo].[LT_SCHOLARSHIP_ALLOCATION] AS sa
	JOIN	[dbo].[TX_CUST_SAL] AS cs ON sa.customer_no = cs.customer_no AND cs.default_ind = 'Y'
	JOIN	[dbo].[LTR_SCHOLARSHIP_ALLOCATION_CATEGORY] AS sac ON sa.allocation = sac.id
	JOIN	@allocations AS a ON sa.allocation = a.allocation_id
	LEFT JOIN [dbo].[T_PAYMENT] AS p WITH (NOLOCK) ON sac.pmt_method = p.pmt_method AND sa.customer_no = p.check_no
	GROUP BY sa.customer_no, sa.allocation, sac.description, cs.esal1_desc, sa.allocation_amt, sa.notes
	HAVING ISNULL(sa.allocation_amt,0) - SUM(ISNULL(p.pmt_amt,0)) >= @min_balance


END

GO


