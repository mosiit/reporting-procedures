USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_OVERNIGHT_SHOW_TIMES]') AND type IN (N'P', N'PC'))
BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_OVERNIGHT_SHOW_TIMES] AS' 
    EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LRP_OVERNIGHT_SHOW_TIMES] TO impusers'
END
GO

/*      LRP_OVERNIGHT_SNACK_TIMES
   


*/
ALTER PROCEDURE [dbo].[LRP_OVERNIGHT_SHOW_TIMES]
        @report_dt DATETIME = Null
AS BEGIN

    /*  Procedure Variables  */

        DECLARE @temp_table TABLE ([show_no] INT NOT NULL DEFAULT (0),
                                   [booking_no] INT NOT NULL DEFAULT (0),
                                   [overnight_dt] DATETIME NULL,
                                   [show_location] VARCHAR(50) NOT NULL DEFAULT (''),
                                   [show_presenter] VARCHAR(50) NOT NULL DEFAULT (''),
                                   [show_start_dt] DATETIME NULL,
                                   [show_time] VARCHAR(10) NOT NULL DEFAULT (''),
                                   [show_column] VARCHAR(25) NOT NULL DEFAULT (''));

        DECLARE @show_table TABLE ([booking_no] INT NOT NULL DEFAULT (0),
                                   [overnight_dt] DATETIME NULL,
                                   [show_location] VARCHAR(50) NOT NULL DEFAULT (''),
                                   [show_presenter] VARCHAR(50) NOT NULL DEFAULT (''),
                                   [show_1] VARCHAR(25) NULL DEFAULT (''),
                                   [show_2] VARCHAR(25) NULL DEFAULT (''),
                                   [show_3] VARCHAR(25) NULL DEFAULT (''));

    /*  Retrieve the snack information from the database  */
        
        INSERT INTO @temp_table ([show_no], [booking_no], [overnight_dt], [show_location], [show_presenter], [show_start_dt], [show_time])
        SELECT ROW_NUMBER() OVER(PARTITION BY resource_name, assigned_resource ORDER BY [start_dt]) AS [Row], 
               [booking_no], 
               [overnight_dt], 
               [resource_name],
               [assigned_resource],
               [start_dt], 
               FORMAT([start_dt],'h:mm tt')
        FROM [dbo].[LV_OVERNIGHT_RESOURCE_ASSIGNMENTS]
        WHERE [overnight_dt] = @report_dt and [resource_type] IN ('Omni','Planetarium','Presenter Staff')
        ORDER BY [start_dt]

    /*  Set the column names based on row identity  */

        UPDATE @temp_table SET [show_column] = 'show_' + CONVERT(CHAR(1),[show_no])
        
    /*  Pivot the data into a single row  */
            
       INSERT INTO @show_table ([booking_no], [overnight_dt], [show_location], [show_presenter], [show_1], [show_2], [show_3])
       SELECT [booking_no], [overnight_dt], [show_location], [show_presenter], [show_1], [show_2], [show_3]
       FROM (SELECT [booking_no], [overnight_dt], [show_location], [show_presenter], [show_time], [show_column] FROM @temp_table) d
             PIVOT (MIN([show_time])
                    FOR show_column in (show_1, show_2, show_3)
              ) piv;

    FINISHED:

        /*  Select the single tow and pass it back to the report  */
            
            SELECT [booking_no],
                   [overnight_dt],
                   [show_location],
                   [show_presenter],
                   CASE WHEN [show_location] IN ('Opening Welcome', 'OW') THEN 'A'
                        WHEN [show_location] IN ('Lightning Show', 'TOE') THEN 'B'
                        WHEN [show_location] IN ('Planetarium', 'PLA') THEN 'C'
                        ELSE 'D' END AS [show_sort],
                   ISNULL([show_1],'') AS [show_1],
                   ISNULL([show_2],'') AS [show_2],
                   ISNULL([show_3],'') AS [show_3],
                   LTRIM(CASE WHEN ISNULL([show_1],'') = '' THEN '' ELSE ISNULL([show_1],'') END +
                         CASE WHEN ISNULL([show_2],'') = '' THEN '' ELSE ', ' + ISNULL([show_2],'') END +
                         CASE WHEN ISNULL([show_3],'') = '' THEN '' ELSE ', ' + ISNULL([show_3],'') END) AS [show_list]
            FROM @show_table
           

END
GO


    EXECUTE [dbo].[LRP_OVERNIGHT_SHOW_TIMES] '10-18-2018'








        --select CONVERT(varchar(15),CAST('17:30:00.0000000' AS TIME),100)
        --SELECT FORMAT(CONVERT(DATETIME,'3-1-2019 18:00:00'),'h:mm tt')


      
GO  
--SELECT FirstName, LastName, TerritoryName, ROUND(SalesYTD,2,1) AS SalesYTD,  

--FROM Sales.vSalesPerson  
--WHERE TerritoryName IS NOT NULL AND SalesYTD <> 0  
--ORDER BY TerritoryName;  

        