USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_LIBRARY_ATTENDANCE]') AND type in (N'P', N'PC'))
BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_LIBRARY_ATTENDANCE] AS' 
    EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LRP_LIBRARY_ATTENDANCE] TO ImpUsers' 
END
GO

ALTER PROCEDURE [dbo].[LRP_LIBRARY_ATTENDANCE]
        @report_start_dt DATETIME = NULL,
        @report_end_dt DATETIME = NULL,
        @free_or_paid VARCHAR(5) = 'All',
        @library_list_str VARCHAR(4000) = '',
        @exhibit_halls_only CHAR(1) = 'N',
        @include_no_visits CHAR(1) = 'N'
WITH RECOMPILE AS BEGIN

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    SET NOCOUNT ON;
     
    /* Procedure Variables  */

        DECLARE @report_start_date CHAR(10) = '', @report_end_date CHAR(10) = '';

        DECLARE @library_list TABLE ([library_no] INT);

    /* Check Parameters  */

        --If null dates, run for yesterday
        SELECT @report_start_dt = ISNULL(@report_start_dt,DATEADD(DAY,-1,GETDATE()));
        SELECT @report_end_dt = ISNULL(@report_end_dt,DATEADD(DAY,-1,GETDATE()));

        --Set the times correctly
        SELECT @report_start_date = CONVERT(DATE,@report_start_dt);
        SELECT @report_end_date = CONVERT(CHAR(10),@report_end_dt,111) + ' 23:59:59.957';

        --Convert dates to text becayuse the history table uses the yyyy/mm/dd format
        SELECT @report_start_date = CONVERT(CHAR(10),@report_start_dt,111);
        SELECT @report_end_date = CONVERT(CHAR(10),@report_end_dt,111);

        --Set Flags to Default Values
        SELECT @exhibit_halls_only = ISNULL(@exhibit_halls_only,'N');
        SELECT @free_or_paid = ISNULL(@free_or_paid,'All')
        IF @free_or_paid = 'Both' SELECT @free_or_paid = 'All'
        
        --Parse Library List String (if any)
        IF ISNULL(@library_list_str,'') <> ''
            INSERT INTO @library_list ([library_no])
            SELECT CONVERT(INT,Element) 
            FROM [dbo].[FT_SPLIT_LIST](REPLACE(@library_list_str,'"',''),',');
        ELSE
            INSERT INTO @library_list ([library_no])
            SELECT DISTINCT [customer_no] FROM [dbo].[LV_LIBRARY_MEMBERSHIP_LIST]
            WHERE @free_or_paid = 'All' OR [library_type] = @free_or_paid

        DELETE FROM @library_list WHERE [library_no] = 0

    /*  Temp Tables
        This report deals with tables that have a lot of rows in them.  All pertinent information based on the parameters passed to the report
        will be cached into three temp tables so that the working data set is relatively small  */
     
        IF OBJECT_ID('tempdb..#library_cache') IS NOT NULL DROP TABLE [#library_cache];

        CREATE TABLE [#library_cache] ([cust_memb_no] INT, [customer_no] INT, [customer_type] VARCHAR(30), [scan_customer_no] INT, [scan_customer_type] VARCHAR(30),
                                       [memb_level] VARCHAR(10), [init_dt] DATETIME, [expr_dt] DATETIME, [current_status_no] INT, [current_status] VARCHAR(30), 
                                       [library_name] VARCHAR(100), [library_type] VARCHAR(20), [street1] VARCHAR(100), [street2] VARCHAR(100), [street3] VARCHAR(100),
                                       [city] VARCHAR(50), [state] VARCHAR(25), [postal_code] VARCHAR(25), [currently_active] CHAR(1), [active_during_range] char(1));

                INSERT INTO [#library_cache] ([cust_memb_no],[customer_no],[customer_type], [scan_customer_no], [scan_customer_type], [memb_level],[init_dt],
                                              [expr_dt],[current_status_no],[current_status],[library_name],[library_type],[street1],[street2],[street3],
                                              [city],[state],[postal_code],[currently_active],[active_during_range])
                SELECT lib.[cust_memb_no],
                       lib.[customer_no],
                       lib.[customer_type],
                       lib.[scan_customer_no],
                       lib.[scan_customer_type],
                       lib.[memb_level],
                       lib.[init_dt],
                       lib.[expr_dt],
                       lib.[current_status_no],
                       lib.[current_status],
                       lib.[library_name],
                       lib.[library_type],
                       lib.[street1],
                       lib.[street2],
                       lib.[street3],
                       lib.[city],
                       lib.[state],
                       lib.[postal_code],
                       CASE WHEN lib.[current_status] = 'Active' THEN 'Y' ELSE 'N' END,
                       [dbo].[LFS_ActiveMemberInDateRange] (lib.[customer_no], 8, @report_start_dt, @report_end_dt)
                FROM [dbo].[LV_LIBRARY_MEMBERSHIP_LIST] AS lib
                     INNER JOIN @library_list AS lst ON lst.[library_no] = lib.[customer_no]

        -------------------------------------------------------------------------------------------------------------------------------------------------------------------
        
        IF OBJECT_ID('tempdb..#scan_cache') IS NOT NULL DROP TABLE [#scan_cache];

        CREATE TABLE [#scan_cache] ([id] INT, [ticket_no] INT, [customer_no] INT, [perf_no] INT, [pkg_no] INT, [seat_no] INT, [entrance] VARCHAR(20), [sli_no] INT,
                                    [user_id] VARCHAR(10), [status] CHAR(1), [error_text] VARCHAR(255), [update_dt] DATETIME, [online_mode] CHAR(1), [update_mode] char(1),
                                    [ticket_ok] CHAR(1), [ticket_msg] VARCHAR(100), [ticket_updated] CHAR(1), [exit_mode] CHAR(1), [exit_logged] CHAR(5), [exit_dt] DATETIME,
                                    [scan_str] VARCHAR(30), [perf_str] VARCHAR(4000), [user_entrance] VARCHAR(255), [device_name] VARCHAR(20), [composite_no] INT,
                                    [cust_memb_no] INT, [memb_level_no] INT, [attendance_id] INT, [create_loc] VARCHAR(16), [create_dt] DATETIME, [created_by] VARCHAR(8),
                                    [last_update_dt] DATETIME, [last_updated_by] VARCHAR(8));

                INSERT INTO [#scan_cache] ([id],[ticket_no],[customer_no],[perf_no],[pkg_no],[seat_no],[entrance],[sli_no],[user_id],[status],[error_text],
                                           [update_dt],[online_mode],[update_mode],[ticket_ok],[ticket_msg],[ticket_updated],[exit_mode],[exit_logged],
                                           [exit_dt],[scan_str],[perf_str],[user_entrance],[device_name],[composite_no],[cust_memb_no],[memb_level_no],
                                           [attendance_id],[create_loc],[create_dt],[created_by],[last_update_dt],[last_updated_by])
                SELECT scn.[id],
                       scn.[ticket_no],
                       scn.[customer_no],
                       scn.[perf_no],
                       scn.[pkg_no],
                       scn.[seat_no],
                       scn.[entrance],
                       scn.[sli_no],
                       scn.[user_id],
                       scn.[status],
                       scn.[error_text],
                       scn.[update_dt],
                       scn.[online_mode],
                       scn.[update_mode],
                       scn.[ticket_ok],
                       scn.[ticket_msg],
                       scn.[ticket_updated],
                       scn.[exit_mode],
                       scn.[exit_logged],
                       scn.[exit_dt],
                       scn.[scan_str],
                       scn.[perf_str],
                       scn.[user_entrance],
                       scn.[device_name],
                       scn.[composite_no],
                       scn.[cust_memb_no],
                       scn.[memb_level_no],
                       scn.[attendance_id],
                       scn.[create_loc],
                       scn.[create_dt],
                       scn.[created_by],
                       scn.[last_update_dt],
                       scn.[last_updated_by]
                FROM [dbo].[T_NSCAN_EVENT_CONTROL] AS scn
                WHERE scn.[update_dt] BETWEEN @report_start_dt AND @report_end_dt

        -------------------------------------------------------------------------------------------------------------------------------------------------------------------
            
        IF OBJECT_ID('tempdb..#history_cache') IS NOT NULL DROP TABLE [#history_cache];

        CREATE TABLE [#history_cache] ([history_no] INT, [attendance_type] VARCHAR(50), [performance_type] VARCHAR(50), [order_no] INT, [sli_no] INT, [li_seq_no] INT,
                                       [title_name] VARCHAR(30), [perf_no] INT, [perf_date] CHAR(10), [perf_time] CHAR(8), [zone_no] INT, [production_name] VARCHAR(30),
                                       [production_name_short] VARCHAR(50), [production_name_long] VARCHAR(150), [comp_code] INT, [comp_code_name] VARCHAR(50), 
                                       [order_payment_status] VARCHAR(50), [price_type] INT, [price_type_name] VARCHAR(30), [due_amt] DECIMAL(18,2), [paid_amt] DECIMAL(18,2),
                                       [sale_total] INT, [performance_date] CHAR(10), [performance_time] CHAR(8), [scan_admission_date] CHAR(10), [scan_admission_time] CHAR(8),
                                       [scan_device] VARCHAR(30), [scan_admission_adult] INT, [scan_admission_child] INT, [scan_admission_other] INT, [scan_admission_auto] INT,
                                       [scan_admission_total] INT, [sli_status] INT, [customer_no] INT, [create_dt] DATETIME)

                INSERT INTO [#history_cache] ([history_no],[attendance_type],[performance_type],[order_no],[sli_no],[li_seq_no],[title_name],[perf_no],[perf_date],[perf_time],[zone_no],
                                              [production_name],[production_name_short],[production_name_long],[comp_code],[comp_code_name],[order_payment_status],[price_type],
                                              [price_type_name],[due_amt],[paid_amt],[sale_total],[performance_date],[performance_time],[scan_admission_date],[scan_admission_time],
                                              [scan_device],[scan_admission_adult],[scan_admission_child],[scan_admission_other],[scan_admission_auto],[scan_admission_total],
                                              [sli_status],[customer_no],[create_dt])
                SELECT his.[history_no],
                       his.[attendance_type],
                       his.[performance_type],
                       his.[order_no],
                       his.[sli_no],
                       his.[li_seq_no],
                       his.[title_name],
                       his.[perf_no],
                       his.[perf_date],
                       his.[perf_time],
                       his.[zone_no],
                       his.[production_name],
                       his.[production_name_short],
                       his.[production_name_long],
                       his.[comp_code],
                       his.[comp_code_name],
                       his.[order_payment_status],
                       his.[price_type],
                       his.[price_type_name],
                       his.[due_amt],
                       his.[paid_amt],
                       his.[sale_total],
                       his.[performance_date],
                       his.[performance_time],
                       his.[scan_admission_date],
                       his.[scan_admission_time],
                       his.[scan_device],
                       his.[scan_admission_adult],
                       his.[scan_admission_child],
                       his.[scan_admission_other],
                       his.[scan_admission_auto],
                       his.[scan_admission_total],
                       his.[sli_status],
                       his.[customer_no],
                       his.[create_dt]
                FROM [dbo].[LT_HISTORY_TICKET] AS his
                WHERE his.[performance_date] BETWEEN @report_start_date AND @report_end_date AND his.[attendance_type] = 'Gate A (Ticketed)'

        -------------------------------------------------------------------------------------------------------------------------------------------------------------------

        IF OBJECT_ID('tempdb..#library_attendance') IS NOT NULL DROP TABLE [#library_attendance];

        CREATE TABLE [#library_attendance] ([library_no] INT, [membership_no] INT, [library_type] VARCHAR(25), [customer_type] VARCHAR(30), [library_name] VARCHAR(100), 
                                            [street1] VARCHAR(100), [street2] VARCHAR(100), [street3] VARCHAR(100), [city] VARCHAR(50), [state] VARCHAR(25), 
                                            [postal_code] VARCHAR(25), [current_status] VARCHAR(30), [data_type] VARCHAR(30), [visit_date] CHAR(10), [scan_status] VARCHAR(10), 
                                            [scan_ticket_ok] CHAR(1), [scan_ticket_msg] VARCHAR(255), [scan_str] VARCHAR(255), [sli_no] INT, [order_no] INT, [perf_no] INT, 
                                            [zone_no] INT, [title_name] VARCHAR(30), [production_name] VARCHAR(30), [production_name_long] VARCHAR(150), [comp_code] VARCHAR(30), 
                                            [price_type] VARCHAR(30), [currently_active] CHAR(1), [active_on_visit_date] CHAR(1), [active_member_no] INT, [total_count] INT, 
                                            [total_paid] DECIMAL(18,2))

    /*  Get attendance information from the History table for all libraries during the date range passed to the report  */

        --Group One: Free Library Scans at the Gate

        INSERT INTO [#library_attendance] ([library_no],[membership_no],[library_type],[customer_type],[library_name],[street1],[street2],[street3],[city],[state],
                                           [postal_code],[current_status],[data_type],[visit_date], [scan_status],[scan_ticket_ok],[scan_ticket_msg],[scan_str],
                                           [sli_no],[order_no],[perf_no],[zone_no],[title_name],[production_name], [production_name_long],[comp_code],[price_type], 
                                           [currently_active],[active_on_visit_date],[active_member_no],[total_count],[total_paid])
        SELECT lib.[customer_no],
               lib.[cust_memb_no],
               lib.[library_type],
               lib.[customer_type],
               lib.[library_name],
               lib.[street1],
               lib.[street2],
               lib.[street3],
               lib.[city],
               lib.[state],
               lib.[postal_code],
               lib.[current_status],
               'Gate Scans',
               CONVERT(CHAR(10),scn.[update_dt],111),
               scn.[status],
               scn.[ticket_ok],
               ISNULL(scn.[ticket_msg],'') AS [ticket_msg],
               scn.[scan_str],
               scn.[sli_no],
               sli.[order_no],
               sli.[perf_no],
               sli.[zone_no],
               prf.[title_name],
               prf.[production_name],
               prf.[production_name_long],
               ISNULL(cmp.[description],''),
               ISNULL(pty.[description],''),
               lib.[currently_active],
               'U',
               0,
               COUNT(scn.[id]),
               SUM(sli.[paid_amt])
        FROM [#library_cache] AS lib
             INNER JOIN [#scan_cache] AS scn ON scn.[customer_no] = lib.[scan_customer_no]
             INNER JOIN [dbo].[T_SUB_LINEITEM] AS sli ON sli.[sli_no] = scn.[sli_no]
             LEFT OUTER JOIN [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] AS prf ON prf.[performance_no] = sli.[perf_no] AND prf.[performance_zone] = sli.[zone_no]
             LEFT OUTER JOIN [dbo].[TR_COMP_CODE] AS cmp ON cmp.[id] = sli.[comp_code]
             LEFT OUTER JOIN [dbo].[TR_PRICE_TYPE] AS pty ON pty.[id] = sli.[price_type]
        WHERE lib.[scan_customer_no] > 0
        GROUP BY lib.[customer_no], lib.[cust_memb_no], lib.[library_type], lib.[customer_type], lib.[library_name], lib.[street1], lib.[street2], lib.[street3], 
                 lib.[city], lib.[state], lib.[postal_code], lib.[current_status], CONVERT(CHAR(10),scn.[update_dt],111), scn.[status], scn.[ticket_ok], 
                 ISNULL(scn.[ticket_msg],''), scn.[scan_str], scn.[sli_no], sli.[order_no], sli.[perf_no], sli.[zone_no], prf.[title_name], prf.[production_name],
                 prf.[production_name_long], ISNULL(cmp.[description],''), ISNULL(pty.[description],''), lib.[currently_active]

        --Group Two: Orders made at the Box Office

        INSERT INTO [#library_attendance] ([library_no],[membership_no],[library_type],[customer_type],[library_name],[street1],[street2],[street3],[city],[state],
                                           [postal_code],[current_status],[data_type],[visit_date], [scan_status],[scan_ticket_ok],[scan_ticket_msg],[scan_str],
                                           [sli_no],[order_no],[perf_no],[zone_no],[title_name],[production_name], [production_name_long],[comp_code],[price_type], 
                                           [currently_active],[active_on_visit_date], [active_member_no],[total_count],[total_paid])
        SELECT  lib.[customer_no],
                lib.[cust_memb_no],
                lib.[library_type],
                lib.[customer_type],
                lib.[library_name],
                lib.[street1],
                lib.[street2],
                lib.[street3],
                lib.[city],
                lib.[state],
                lib.[postal_code],
                lib.[current_status],
                'Orders',
                his.perf_date,
                '',
                '',
                '',
                '',
                his.[sli_no],
                his.[order_no],
                his.[perf_no],
                his.[zone_no],
                his.[title_name],
                his.[production_name],
                his.[production_name_long],
                his.[comp_code_name],
                his.[price_type_name],
                lib.[currently_active],
                'U',
                0,
                SUM(his.[sale_total]),
                SUM(his.[paid_amt])
        FROM [#library_cache] AS lib
             INNER JOIN [#history_cache] AS his ON his.[customer_no] = lib.[customer_no]
        WHERE @exhibit_halls_only = 'N' OR [his].[title_name] IN ('Exhibit Halls','Exhibit Show and Go')
        GROUP BY lib.[customer_no], lib.[cust_memb_no], lib.[library_type], lib.[customer_type], lib.[library_name], lib.[street1], lib.[street2], lib.[street3], 
                 lib.[city], lib.[state], lib.[postal_code], lib.[current_status], his.perf_date, his.[sli_no], his.[order_no], his.[perf_no], his.[zone_no], 
                 his.[title_name], his.[production_name], his.[production_name_long], his.[comp_code_name], his.[price_type_name], lib.[currently_active]

        --Group Three: Libraries that had an active membership at any point in the report date range that are not allready on the report (added with zero visits)
        --             Only Include if asked for
        
        

        IF @include_no_visits = 'Y'
            INSERT INTO [#library_attendance] ([library_no],[membership_no],[library_type],[customer_type],[library_name],[street1],[street2],[street3],[city],[state],
                                            [postal_code],[current_status],[data_type],[visit_date], [scan_status],[scan_ticket_ok],[scan_ticket_msg],[scan_str],
                                            [sli_no],[order_no],[perf_no],[zone_no],[title_name],[production_name], [production_name_long],[comp_code],[price_type], 
                                            [currently_active],[active_on_visit_date], [active_member_no],[total_count],[total_paid])
            SELECT  lib.[customer_no],
                    lib.[cust_memb_no],
                    lib.[library_type],
                    lib.[customer_type],
                    lib.[library_name],
                    lib.[street1],
                    lib.[street2],
                    lib.[street3],
                    lib.[city],
                    lib.[state],
                    lib.[postal_code],
                    lib.[current_status],
                    'No Visit',
                    '',
                    '',
                    '',
                    '',
                    '',
                    0,
                    0,
                    0,
                    0,
                    'No Visits',
                    'No Visits',
                    'No Visits',
                    '',
                    '',
                    lib.[currently_active],
                    'U',
                    0,
                    0,
                    0
            FROM [#library_cache] AS lib
            WHERE [customer_no] NOT IN (SELECT [library_no] FROM [#library_attendance]) AND [active_during_range] = 'Y'

        


    /*  Data Cleanup to make things look nicer on the report  */

        UPDATE [#library_attendance] 
        SET [customer_type] = 'N-Scan Pass'
        WHERE [data_type] = 'Gate Scans'

        UPDATE [#library_attendance]
        SET [active_on_visit_date] = [dbo].[LFS_ActiveMemberOnDate] ([library_no], 8, CONVERT(DATE,[visit_date]), 'Y')
        
        UPDATE [#library_attendance]
        SET [active_member_no] = [dbo].[LFS_ActiveMemberNumberOnDate] ([library_no], 8, CONVERT(DATE,[visit_date]))
        WHERE [active_on_visit_date] = 'Y'

        UPDATE [#library_attendance]
        SET [street2] = [street3], [street3] = ''
        WHERE [street2] = '' AND [street3] <> ''

        UPDATE [#library_attendance]
        SET [street1] = [street2], [street2] = ''
        WHERE [street1] = '' AND [street2] <> ''
        
    FINISHED:

        /*  Select final data set for the report */
                       
            SELECT [library_no],
                   [membership_no],
                   [library_type],
                   [currently_active],
                   [active_on_visit_date] AS [active_member_on_perf_date],
                   [active_member_no],
                   [library_name],
                   [customer_type],
                   [street1],
                   [street2],
                   [street3],
                   [city],
                   [state],
                   [postal_code],
                   [order_no],
                   CONVERT(DATE,[visit_date]) AS [visit_dt],
                   [visit_date] AS [visit_date],
                   CASE WHEN [title_name] IN ('Exhibit Halls','Exhibit Show and Go') THEN [visit_date] ELSE '' END AS [visit_date_exh],
                   CASE WHEN [title_name] IN ('Exhibit Halls','Exhibit Show and Go') THEN '' ELSE [visit_date] END AS [visit_date_not_exh],
                   [title_name],
                   CASE WHEN [title_name] = 'Exhibit Halls' THEN 'A_' WHEN [title_name] = 'Exhibit Show and Go' THEN 'B_' ELSE 'C_' END + [title_name] AS [title_name_sort],
                   [production_name],
                   [production_name_long],
                   [comp_code],
                   [price_type],
                   [total_count],
                   CASE WHEN [title_name] IN ('Exhibit Halls','Exhibit Show and Go') THEN [total_count] ELSE 0.0 END AS [total_count_exh],
                   CASE WHEN [title_name] IN ('Exhibit Halls','Exhibit Show and Go') THEN 0.0 ELSE [total_count] END AS [total_count_not_exh],
                   [total_paid] as [total_paid],
                   CASE WHEN [title_name] IN ('Exhibit Halls','Exhibit Show and Go') THEN [total_paid] ELSE 0.0 END AS [total_paid_exh],
                   CASE WHEN [title_name] IN ('Exhibit Halls','Exhibit Show and Go') THEN 0.0 ELSE [total_paid] END AS [total_paid_not_exh]
            FROM [#library_attendance]

    DONE:

        IF OBJECT_ID('tempdb..#library_attendance') IS NOT NULL DROP TABLE [#library_attendance];

END
GO

EXECUTE [dbo].[LRP_LIBRARY_ATTENDANCE] '10-1-2017', '11-30-2017', 'All', '14591', 'N', 'Y'

