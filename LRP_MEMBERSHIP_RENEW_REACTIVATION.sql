USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LRP_MEMBERSHIP_RENEW_REACTIVATION]    Script Date: 10/14/2016 9:56:20 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[LRP_MEMBERSHIP_RENEW_REACTIVATION]
(
	@MembershipExpireDate DATETIME,
	@MembLevel_str VARCHAR(MAX) = NULL,
	@NRR_str VARCHAR(MAX) = NULL
)
AS

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET NOCOUNT ON

-- Finding the last day of the month entered
DECLARE @LastDayExpirationMonth DATETIME
SET @LastDayExpirationMonth = DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@MembershipExpireDate)+1,0))

----------- POPULATE TABLE TO HOLD THE NRR STATUS ----------- 
-- String from Tess application comes delimited with double quotes and commas. 
-- Removing double quotes
SET @NRR_str = REPLACE(@NRR_str,'"', '')

CREATE TABLE #Nrr (
	NRR_str VARCHAR(2)
)

IF @NRR_str IS NULL
	INSERT INTO #Nrr (NRR_str)
	SELECT description FROM dbo.TR_GOOESOFT_DROPDOWN WHERE code = 31
ELSE 
	INSERT INTO #Nrr  (NRR_str)
	SELECT CONVERT(VARCHAR(2),Element) FROM dbo.FT_SPLIT_LIST (@NRR_str,',')

----------- POPULATE TABLE TO HOLD THE MEMBERSHIP LEVEL ----------- 
-- String from Tess application comes delimited with double quotes and commas. 
-- Removing double quotes
SET @MembLevel_str = REPLACE(@MembLevel_str, '"', '')

CREATE TABLE #MembLevel (
	MembLevel_str VARCHAR(3)
)

IF @MembLevel_str IS NULL 
	INSERT INTO #MembLevel (MembLevel_str)
	SELECT memb_level FROM dbo.T_MEMB_LEVEL WHERE inactive = 'N'
ELSE 
	INSERT INTO #MembLevel (MembLevel_str)
	SELECT CONVERT(VARCHAR(3),Element) FROM dbo.FT_SPLIT_LIST(@MembLevel_str,',')

DECLARE @results TABLE
(
	cust_memb_no INT NOT NULL, 
	customer_no INT NULL,
	NRR_Status VARCHAR(2) NULL,
	memb_level VARCHAR(3) NULL,
	memb_org_no INT, 
	expr_dt DATE NULL,
	create_dt DATE NULL,
	current_status INT,
	next_create_dt DATE 
)

--First insert the expired membership info into the results table
INSERT INTO @results
        (cust_memb_no,
		 customer_no,
         NRR_Status,
         memb_level,
		 memb_org_no,
         expr_dt,
         create_dt,
         current_status)
SELECT custMemb.cust_memb_no,
	custMemb.customer_no,
	   custMemb.NRR_status,
	   custMemb.memb_level,
	   custMemb.memb_org_no,
	   CAST(custMemb.expr_dt AS DATE),
	   CAST(custMemb.create_dt AS DATE),
	   custMemb.current_status	   
FROM dbo.TX_CUST_MEMBERSHIP custMemb 
	INNER JOIN #Nrr AS nrr 
		ON custMemb.NRR_status = nrr.NRR_str 
	INNER JOIN #MembLevel AS ml
		ON custMemb.memb_level = ml.MembLevel_str
WHERE DATEDIFF(MONTH,@LastDayExpirationMonth,custMemb.expr_dt) BETWEEN -11 AND 0 
	AND custMemb.current_status <> 8 -- not Deactivated

--SELECT 'after insert', COUNT(*) FROM @results

---- below code stolen from [dbo].[AP_REPORT_MEMB_ACT] 
DELETE r
FROM @results r
WHERE r.cust_memb_no <> (SELECT MAX(cust_memb_no) FROM @results b WHERE r.customer_no = b.customer_no)

--SELECT 'after delete', COUNT(*) FROM @results

UPDATE r
SET r.next_create_dt = CAST(m.create_dt AS DATE)
--SELECT m.*
FROM @results r
	INNER JOIN dbo.TX_CUST_MEMBERSHIP m
		ON r.customer_no = m.customer_no AND r.memb_org_no = m.memb_org_no
WHERE DATEDIFF(d, r.expr_dt, m.expr_dt) > 0
	AND (m.cur_record = 'Y' OR m.current_status = 3) -- Pending

--SELECT 'after update', COUNT(*) FROM @results

-- SELECT THE FINAL RESULTS
SELECT distinct
	customer_no,
	NRR_Status,
	memb_level,
	current_status,
	expr_dt,
	create_dt,
	next_create_dt,
	daysToRenew,
	CASE 
		WHEN t.daysToRenew <= -62 THEN '62+'
		WHEN t.daysToRenew BETWEEN -61 AND -32 THEN '32-61'
		WHEN t.daysToRenew BETWEEN -31 AND 0 THEN '0-31'
		WHEN t.daysToRenew BETWEEN 1 AND 30 THEN '1-30 After'
		WHEN t.daysToRenew BETWEEN 31 AND 180 THEN '31-180 After'
		WHEN t.daysToRenew BETWEEN 181 AND 365 THEN '181-365 After'
		WHEN t.daysToRenew IS NULL THEN 'Did not renew'
		ELSE NULL
	END AS label
FROM 
(	
	SELECT	
		customer_no,
		NRR_Status,
		memb_level,
		expr_dt,
		create_dt,
		current_status,
		DATEDIFF(d, expr_dt, next_create_dt) daysToRenew,
		next_create_dt
	FROM @results x
)t


GO


