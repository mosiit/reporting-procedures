USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_MEMBER_ONE_STEP_HISTORY]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_MEMBER_ONE_STEP_HISTORY] AS' 
END
GO    

ALTER PROCEDURE [dbo].[LRP_MEMBER_ONE_STEP_HISTORY]
        @report_start_dt DATETIME = Null,
        @report_end_dt DATETIME = NULL,
        @exclude_lifetime_memberships CHAR(1) = 'Y',
        @include_member_level_breakdown CHAR(1) = 'Y'
AS BEGIN

    /*  Procedure Variables  */

        DECLARE @server_name VARCHAR(100) = @@SERVERNAME

        DECLARE @data_date DATETIME 

        DECLARE @month_str VARCHAR(10) = '', @last_month_str VARCHAR(10) = ''

        DECLARE @month_start_dt DATETIME, @month_end_dt DATETIME, @month_counter INT = 0
        
        DECLARE @month_name VARCHAR(50) = '', @rpt_msg VARCHAR(100) = ''

        DECLARE @one_step_info TABLE ([cust_memb_no] INT, [customer_no] INT, [memb_org_no] INT, [memb_org_name] VARCHAR(30), 
                                      [memb_level] VARCHAR(3), [memb_level_name] VARCHAR(30), [init_dt] DATETIME, [expr_dt] DATETIME, 
                                      [NRR_status] VARCHAR(3), [NRR_status_name] VARCHAR(30), [current_status] INT, 
                                      [current_status_name] VARCHAR(30), [ship_method] INT, [ship_method_name] VARCHAR(30), 
                                      [one_step_month] VARCHAR(10), [one_step_month_display] VARCHAR(50), [has_one_step] TINYINT, 
                                      [no_one_step] INT, report_message VARCHAR(100))

   
       DECLARE @one_step_total TABLE ([row_num] INT, [cust_memb_no] INT, [customer_no] INT, [memb_org_no] INT, [memb_org_name] VARCHAR(30), 
                                      [memb_level] VARCHAR(3), [memb_level_name] VARCHAR(30), [init_dt] DATETIME, [expr_dt] DATETIME, 
                                      [NRR_status] VARCHAR(3), [NRR_status_name] VARCHAR(30), [current_status] INT, 
                                      [current_status_name] VARCHAR(30), [ship_method] INT, [ship_method_name] VARCHAR(30), 
                                      [one_step_month] VARCHAR(10), [one_step_month_display] VARCHAR(50), [has_one_step] TINYINT, 
                                      [no_one_step] INT, report_message VARCHAR(100))



    /*  Check Parameters
        If null value passed to either date, run for last month only.  
        Make sure start and end dates are set to the first and last day of the month respectively  */

        IF @report_start_dt IS NULL OR @report_end_dt IS NULL
            SELECT @report_start_dt = DATEADD(MONTH,-1,GETDATE()),
                   @report_end_dt = DATEADD(MONTH,-1,GETDATE())        

        WHILE DATEPART(DAY,@report_start_dt) > 1 
            SELECT @report_start_dt = DATEADD(DAY,-1,@report_start_dt)

        WHILE DATEPART(MONTH,DATEADD(DAY,1,@report_end_dt)) = DATEPART(MONTH,@report_end_dt)
            SELECT @report_end_dt = DATEADD(DAY,1,@report_end_dt)

    /*  If anything other than 'N' for No (including a null) is passed to the exclude lifetime or 
        include member level break down parameters then the default is Yes  */

        IF ISNULL(@exclude_lifetime_memberships,'Y') <> 'N' SELECT @exclude_lifetime_memberships = 'Y'

        IF ISNULL(@include_member_level_breakdown,'Y') <> 'N' SELECT @include_member_level_breakdown = 'Y'


            /*  *****During testing, I almost accidentally ran the report for 50 years worth of data, so I added 
                this code to limit the run to no more than two years.  The number of years can be increased 
                or it can come out all together once the report is live  */

                IF DATEDIFF(MONTH,@report_start_dt,DATEADD(MONTH,1,@report_end_dt)) > 24 BEGIN
                    SELECT @rpt_msg = 'Invalid date range.  Maximum = 2 years at a time'
                    GOTO FINISHED
                END

    /*  Get the backup date  */

        SELECT @data_date = [backup_start_date] 
                            FROM [msdb].[dbo].[backupset] 
                            WHERE [backup_set_id] = (SELECT TOP 1 [backup_set_id] 
                                                     FROM [msdb].[dbo].[restorehistory] 
                                                     WHERE [destination_database_name] = 'impresario' 
                                                     ORDER BY [restore_date] DESC)
        
    /*  The month string is the month and year reversed (yyyy/mm)  It starts with the month and year of the start date.
        The last month string is the month and year of the end date  */

        SELECT @month_str = LEFT(CONVERT(CHAR(10),@report_start_dt,111),7),
               @last_month_str = LEFT(CONVERT(CHAR(10),@report_end_dt,111),7)
    
    /*  Months are processed one at a time
        This is trying to pull a list of all memberships that were active in each month, meaning a lot if not most of the memberships 
        will appear in this report more than once.  The only memberships that will not be in the table more than once are those
        that ended in the start month and those that began in the end month.  */
        
        WHILE @month_str <= @last_month_str BEGIN

            SELECT @month_start_dt = CONVERT(DATETIME,(RTRIM(@month_str) + '/01 00:00:00'))

            SELECT @month_end_dt = DATEADD(MILLISECOND,-43,DATEADD(MONTH,1,@month_start_dt))

            SELECT @month_name = DATENAME(MONTH,@month_start_dt) + ', ' + DATENAME(YEAR,@month_start_dt)

            /*  It looks for household (org# - 4) memberships only and excludes memberships with a status of Suspended (5), 
                Cancelled (6), Deactivated (8) or Merged (9).  If the month being checked falls between the month and year 
                of the membership's initialize date and expiration date, then it is considered active for that month.  
                The indication that the membership is on the one-step program is ship_method = 3  */

               INSERT INTO @one_step_info ([cust_memb_no], [customer_no], [memb_org_no], [memb_org_name], [memb_level], [memb_level_name], 
                                           [init_dt], [expr_dt], [NRR_status], [NRR_status_name], [current_status], [current_status_name], 
                                           [ship_method], [ship_method_name], [one_step_month], [one_step_month_display], [has_one_step], 
                                           [no_one_step], [report_message])
                SELECT mem.[cust_memb_no]
                     , mem.[customer_no]
                     , mem.[memb_org_no]
                     , org.[description]
                     , mem.[memb_level]
                     , lev.[description]
                     , mem.[init_dt]
                     , mem.[expr_dt]
                     , mem.[NRR_status]
                     , CASE WHEN mem.[NRR_status] = 'NE' THEN 'New' WHEN mem.[NRR_status] = 'RI' THEN 'Rejoin' WHEN mem.[NRR_status] = 'RN' THEN 'Renew' ELSE 'Unknown' END
                     , mem.[current_status]
                     , sta.[description]
                     , ISNULL(mem.[ship_method],0)
                     , ISNULL(shi.[description],'')
                     , @month_str
                     , @month_name
                     , CASE WHEN ISNULL(mem.[ship_method],0) = 3 THEN 1 ELSE 0 END    --Has one-step
                     , CASE WHEN ISNULL(mem.[ship_method],0) = 3 THEN 0 ELSE 1 END    --Does not have one-step
                     , ''
                FROM dbo.TX_CUST_MEMBERSHIP AS mem (NOLOCK)
                     LEFT OUTER JOIN [dbo].[T_MEMB_ORG] AS org (NOLOCK) ON org.[memb_org_no] = mem.[memb_org_no]
                     LEFT OUTER JOIN [dbo].[T_MEMB_LEVEL] AS lev (NOLOCK) ON lev.memb_level = mem.memb_level
                     LEFT OUTER JOIN [dbo].[TR_SHIP_METHOD] AS shi (NOLOCK) ON shi.[id] = mem.[ship_method]
                     LEFT OUTER JOIN [dbo].[TR_CURRENT_STATUS] AS sta (NOLOCK) ON sta.[id] = mem.[current_status]
                WHERE mem.[memb_org_no] = 4 --Household Memberships
                 AND mem.[current_status] NOT IN (5, 6, 8, 9)  --suspended/canceled/deactivated/merged
                 AND @month_str BETWEEN LEFT(CONVERT(CHAR(10),init_dt,111),7) AND LEFT(CONVERT(CHAR(10),expr_dt,111),7)           

            /*  Increase month counter by 1  */

                SELECT @month_counter = (@month_counter + 1)

            /*  Change month string to the next month  */

                SELECT @month_str = LEFT(CONVERT(CHAR(10),DATEADD(MONTH,1,@month_start_dt),111),7)

        END 


        /*  If lifetime memberships are being excluded, then delete them now
            *****NEED TO VERIFY but it appears that to identify Lifetime memberships, look for an expiration year >= 2050  */

            IF @exclude_lifetime_memberships = 'Y'
                DELETE FROM @one_step_info WHERE DATEPART(YEAR,[expr_dt]) >= 2050

        /*  If no member level breakdown is needed, change the member level name to the same value for all records  */

            IF @include_member_level_breakdown = 'N'
                UPDATE @one_step_info SET [memb_level_name] = 'All Membership Levels'


        /*  If more than one month was processed, calculate totals
            Need the total to be unique memberships rather than just a total count of the records in the table.  
            Inserting a "total" set into a separate table with row numbers to indicate duplicates.  Deleting 
            any record that has row number greater than 1.  */

            IF @month_counter > 1 BEGIN

                INSERT INTO @one_step_total ([row_num], [cust_memb_no], [customer_no], [memb_org_no], [memb_org_name], [memb_level], [memb_level_name], 
                                             [init_dt], [expr_dt], [NRR_status], [NRR_status_name], [current_status], [current_status_name], 
                                             [ship_method], [ship_method_name], [one_step_month], [one_step_month_display], [has_one_step], 
                                             [no_one_step], [report_message])
                SELECT ROW_NUMBER() 
                           OVER (PARTITION BY [cust_memb_no], [customer_no], [memb_org_no], [memb_level], [init_dt], 
                                              [expr_dt], [NRR_status], [current_status], [ship_method] ORDER BY cust_memb_no)
                     , [cust_memb_no]
                     , [customer_no]
                     , [memb_org_no]
                     , [memb_org_name]
                     , [memb_level]
                     , [memb_level_name]
                     , [init_dt]
                     , [expr_dt]
                     , [NRR_status]
                     , [NRR_status_name]
                     , [current_status]
                     , [current_status_name]
                     , [ship_method]
                     , [ship_method_name]
                     , 'TOTAL'  --month string
                     , 'TOTAL'  --month string display
                     , [has_one_step]
                     , [no_one_step]
                     , [report_message]
                FROM @one_step_Info

                
                DELETE FROM @one_step_total WHERE row_num > 1

            END

    FINISHED:

        /*  If nothing in the table, add a single record with a "nothing found" message.  */

            IF NOT EXISTS (SELECT * FROM @one_step_info) BEGIN
               
                IF @rpt_msg = '' SELECT @rpt_msg =  'No data found for the criteria you entered.'
               
                INSERT INTO @one_step_info ([cust_memb_no], [customer_no], [memb_org_no], [memb_org_name], [memb_level], [memb_level_name], 
                                            [init_dt], [expr_dt], [NRR_status], [NRR_status_name], [current_status], [current_status_name], 
                                            [ship_method], [ship_method_name], [one_step_month], [has_one_step], [no_one_step], [report_message])
                VALUES (0, 0, 0, '', '', '', Null, Null, '', '', 0, '', 0, '', '', 0, 0, @rpt_msg)

            END


        /*  Select final data set from the two table to return to the report.  */

            SELECT [one_step_month]
                 , [one_step_month_display]
                 , [memb_level_name]
                 , SUM([has_one_step]) AS 'has_one_step'
                 , SUM([no_one_step]) AS 'no_one_step'
                 , SUM([has_one_step] + [no_one_step]) AS 'total'
                 , CASE WHEN CONVERT(DECIMAL(18,2),SUM([has_one_step] + [no_one_step])) = 0.0 THEN 0.0
                        ELSE (CONVERT(DECIMAL(18,2),SUM(has_one_step)) /  CONVERT(DECIMAL(18,2),SUM([has_one_step] + [no_one_step]))) END AS 'percentage'
                 , [report_message]
                 , @server_name AS 'server_name'
                 , @data_date AS 'data_date'
            FROM @one_step_info 
            GROUP BY [one_step_month], [one_step_month_display], [memb_level_name], [report_message]
   
            UNION SELECT [one_step_month]
                       , [one_step_month_display]
                       , [memb_level_name]
                       , SUM([has_one_step]) AS 'has_one_step'
                       , SUM([no_one_step]) AS 'no_one_step'
                       , SUM([has_one_step] + [no_one_step]) AS 'total'
                       , CASE WHEN CONVERT(DECIMAL(18,2),SUM([has_one_step] + [no_one_step])) = 0.0 THEN 0.0
                         ELSE (CONVERT(DECIMAL(18,2),SUM(has_one_step)) /  CONVERT(DECIMAL(18,2),SUM([has_one_step] + [no_one_step]))) END AS 'percentage'
                       , [report_message]
                       , @server_name AS 'server_name'
                       , @data_date AS 'data_date'
            FROM @one_step_total
            GROUP BY [one_step_month], [one_step_month_display], [memb_level_name], [report_message]
            ORDER BY [one_step_month]

    DONE:

END
GO

GRANT EXECUTE ON [dbo].[LRP_MEMBER_ONE_STEP_HISTORY] TO ImpUsers
GO

--EXECUTE [dbo].[LRP_MEMBER_ONE_STEP_HISTORY] '7-1-2014', '7-31-2014', 'Y', 'Y'
--GO


