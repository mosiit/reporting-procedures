USE impresario;
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LP_DAILY_VISIT_COUNT]') AND type in (N'P', N'PC')) BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LP_DAILY_VISIT_COUNT] AS';
    EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LP_DAILY_VISIT_COUNT] TO impusers';
    EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LP_DAILY_VISIT_COUNT] TO tessitura_app';
END;
GO

SET ANSI_NULLS ON;
SET QUOTED_IDENTIFIER ON;
GO

ALTER PROCEDURE [dbo].[LP_DAILY_VISIT_COUNT]
        @report_start_date datetime,
        @report_end_date datetime,
        @include_collected_passes CHAR(1),
        @include_negative_counts CHAR(1)
AS BEGIN

    SET NOCOUNT ON;
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

    /*  Procedure Parameters  */

        DECLARE @start_date char(10) = '', 
                @end_date char(10) = '';
    
        DECLARE @mos_buyout_no INT = 9;  --9 = the id number for the buyouts mode of sale


    /*  Create temp tables needed for this report  */
        
        IF OBJECT_ID('tempdb..#visit_count_raw_data') is not null DROP TABLE [#visit_count_raw_data];
        
        CREATE TABLE [#visit_count_raw_data] ([attend_type] varchar(30) NOT NULL DEFAULT (''), 
                                              [order_no] INT  NOT NULL DEFAULT (0), 
                                              [order_mos] INT NOT NULL DEFAULT (0),
                                              [perf_no] INT NOT NULL DEFAULT (0), 
                                              [zone_no] INT  NOT NULL DEFAULT (0), 
                                              [prod_name] varchar(30) NOT NULL DEFAULT (''), 
                                              [sale_total] INT NOT NULL DEFAULT (0), 
                                              [scan_admission_total] INT NOT NULL DEFAULT (0));

        IF OBJECT_ID('tempdb..#visit_count_final_data') is not null DROP TABLE [#visit_count_final_data];

        CREATE TABLE [#visit_count_final_data] ([attendance_type] VARCHAR(30) NOT NULL DEFAULT (''), 
                                                [order_no] INT NOT NULL DEFAULT (0), 
                                                [sale_total] INT NOT NULL DEFAULT (0), 
                                                [scan_admission_total] INT NOT NULL DEFAULT (0));

    /*  Check Parameters  */

        SELECT @start_date = convert(char(10),@report_start_date,111), 
               @end_date = convert(char(10),@report_end_date,111);
        
    /* Make sure the dates passed to the procedure include the entire day (from midnight on the start date to 11:59 PM on the end date)  */

        SELECT @report_start_date = convert(char(10),@report_start_date,111) + ' 00:00:00',
               @report_end_date = convert(char(10),@report_end_date,111) + ' 23:59:59';


    /*  Retrieve the Visit Count data for ticketed events from the database  */
        
        INSERT INTO [#visit_count_raw_data] ([attend_type], [order_no], [order_mos], [perf_no], 
                                             [zone_no], [prod_name], [sale_total], [scan_admission_total])
        SELECT 'ticketed', 
               tik.[order_no], 
               ord.[MOS],
               tik.[perf_no], 
               tik.[zone_no], 
               tik.[production_name],
               SUM(tik.[sale_total]), 
               SUM(tik.[scan_admission_total]) 
        FROM [dbo].[LT_HISTORY_TICKET] AS tik
             LEFT OUTER JOIN [dbo].[T_ORDER] AS ord ON ord.[order_no] = tik.[order_no]
        WHERE tik.[performance_date] between @start_date and @end_date
          AND ord.[MOS] <> @mos_buyout_no
          AND (tik.[title_name] in (SELECT [title_name] 
                                    FROM [dbo].[LV_SS_PRODUCTION_ELEMENTS_TITLE] 
                                    WHERE [visit_count_title] = 'Y')
               OR tik.[production_name] IN (SELECT [production_name] 
                                            FROM [dbo].[LV_PRODUCTION_ELEMENTS_PRODUCTION] 
                                            WHERE [visit_count_production] = 'Y'))
        GROUP BY tik.[order_no], ord.[mos], tik.[perf_no], tik.[zone_no], tik.[production_name]; 

    /*  Delete buyout productions - Regardless of the mode of sale the order was placed in  */

        DELETE FROM [#visit_count_raw_data] WHERE prod_name like '%Buyout%';

    /*  Added 2/7/2018:  Hard Coded exception added for the Exhibit Halls Special production, which is in the 
                         Exhibit Halls title but should not be counted in with the visit count.  */
         
        DELETE FROM [#visit_count_raw_data] WHERE [prod_name] = 'Exhibit Halls Special';

    /*  Retrieve visit count final data for ticketed events  */
           
        INSERT INTO [#visit_count_final_data] ([attendance_type],[order_no],[sale_total],[scan_admission_total])
        SELECT 'Ticketed Events', 
               [order_no], 
               MAX([sale_total]), 
               MAX([scan_admission_total]) 
        FROM [#visit_count_raw_data] 
        GROUP By [Order_no];

    /*  Retrieve visit count final data for gate scans from the database  */

        INSERT INTO [#visit_count_final_data] ([attendance_type],[order_no],[sale_total],[scan_admission_total])
        SELECT 'Membership Card Scan', 
               [sale_total], 
               [scan_admission_total], 
               0 
        FROM [dbo].[LT_HISTORY_TICKET]
        WHERE [performance_date] BETWEEN @start_date AND @end_date 
          AND [title_name] = 'Membership Card Scan';
             
    /*  Retrieve visit count final data for show and go (excluding GOBoston and Redeam) scans from the database  */

        INSERT INTO [#visit_count_final_data] ([attendance_type],[order_no],[sale_total],[scan_admission_total])
        SELECT [title_name], 
               [sale_total], 
               [scan_admission_total], 
               0 
        FROM [dbo].[LT_HISTORY_TICKET] 
        WHERE [performance_date] BETWEEN @start_date AND @end_date 
          AND [title_name] = 'Show and Go'
          AND [production_name] NOT IN ('GoBoston', 'Redeam')
          AND [customer_no] NOT IN (SELECT [customer_no] 
                                    FROM [dbo].[TX_CUST_SAL]
                                    WHERE [esal2_desc] LIKE '%NO VISIT COUNT%');

    /*  Retrieve visit count final data for show and go collected passes (including GOBoston) from the database (if requested)  */

        IF @include_collected_passes = 'Y' BEGIN

            INSERT INTO [#visit_count_final_data] ([attendance_type],[order_no],[sale_total],[scan_admission_total])
            SELECT [title_name], 
                   [sale_total], 
                   [scan_admission_total], 
                   0 
            FROM [dbo].[LT_HISTORY_TICKET] 
            WHERE [performance_date] BETWEEN @start_date AND @end_date 
              AND [title_name] = 'Show and Collected'
              AND [production_name] NOT IN ('GoBoston', 'Redeam')
              AND [customer_no] NOT IN (SELECT [customer_no] 
                                        FROM [dbo].[TX_CUST_SAL]
                                        WHERE [esal2_desc] LIKE '%NO VISIT COUNT%');

            INSERT INTO [#visit_count_final_data] ([attendance_type],[order_no],[sale_total],[scan_admission_total])
            SELECT [production_name], 
                   [sale_total], 
                   [scan_admission_total], 
                   0 
            FROM [dbo].[LT_HISTORY_TICKET] 
            WHERE [performance_date] BETWEEN @start_date AND @end_date 
              AND [production_name] IN ('GoBoston','Redeam')
              AND [customer_no] NOT IN (SELECT [customer_no] 
                                    FROM [dbo].[TX_CUST_SAL]
                                    WHERE [esal2_desc] LIKE '%NO VISIT COUNT%');

        END

    /*  Retrieve negative count for members double counted when going to a special exhibition  */

        IF @include_negative_counts = 'Y' BEGIN

            DECLARE @double_count TABLE ([attendance_date] DATE NULL,                                   [attendance_month] VARCHAR(50) NOT NULL DEFAULT(''),    
                                         [attendance_month_sort] VARCHAR(10) NOT NULL DEFAULT(''),      [customer_no] INT NOT NULL DEFAULT(0),
                                         [member_scan] INT NOT NULL DEFAULT(0),                         [venue_tickets] INT NOT NULL DEFAULT(0),
                                         [double_counted] INT NOT NULL DEFAULT(0))


            INSERT INTO @double_count ([attendance_date], [attendance_month], [attendance_month_sort], [customer_no], 
                                       [member_scan], [venue_tickets], [double_counted])
            EXECUTE [dbo].[LRP_MEMBER_DOUBLE_VISIT_COUNT] @report_start_date, @report_end_date, 37;  --37 = Title Number for Special Exhibitions

            IF EXISTS (SELECT * FROM @double_count)
            INSERT INTO [#visit_count_final_data] ([attendance_type],[order_no],[sale_total],[scan_admission_total])
            SELECT 'Special Exhibit Double Count',
                   SUM(double_counted),
                   (SUM(double_counted) * -1),
                   (SUM(double_counted) * -1)
            FROM @double_count;

        END;
        
    FINISHED:

       /* Select aggregated date into the report  */

        SELECT [attendance_type], 
               COUNT([order_no]) AS [transaction_count], 
               SUM([sale_total]) AS [visit_count], 
               SUM([scan_admission_total]) AS [visit_count_scan]
        FROM [#visit_count_final_data]
        GROUP BY [attendance_type] 
        ORDER BY [attendance_type];

    CLEAN_UP:

        /*  Not really necessary, but the programmer in me wants to clean up  */

        IF OBJECT_ID('tempdb..#visit_count_raw_data') is not null DROP TABLE [#visit_count_raw_data];
        IF OBJECT_ID('tempdb..#visit_count_final_data') is not null DROP TABLE [#visit_count_final_data];

        DONE:

END;
GO

GRANT EXECUTE ON [dbo].[LP_DAILY_VISIT_COUNT] to impusers;
GO

--EXECUTE [dbo].[LP_DAILY_VISIT_COUNT] @report_start_date = '1-21-2020', @report_end_date = '1-21-2020', @include_collected_passes = 'Y', @include_negative_counts = 'Y';
