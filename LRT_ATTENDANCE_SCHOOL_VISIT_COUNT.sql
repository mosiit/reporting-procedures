USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_ATTENDANCE_SCHOOL_VISIT_COUNT]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LRP_ATTENDANCE_SCHOOL_VISIT_COUNT]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LRP_ATTENDANCE_SCHOOL_VISIT_COUNT]
        @report_start_dt datetime,
        @report_end_dt DATETIME
AS BEGIN

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

        /*  Procedure Variables  */

        DECLARE @rpt_message VARCHAR(100) = ''

        DECLARE @go_live_date CHAR(10) = '2016/05/18'

        DECLARE @rpt_start_date CHAR(10), @rpt_end_date CHAR(10),
                @his_start_date CHAR(10), @his_end_date CHAR(10),
                @arc_start_date CHAR(10), @arc_end_date CHAR(10)

        DECLARE @mos_buyout_no INT = (SELECT ISNULL([id],0) FROM [dbo].[TR_MOS] WHERE [description] = 'Buyouts')

    /*  Temporary Tables  */

        IF OBJECT_ID('tempdb..#attendance') IS NOT NULL DROP TABLE [#attendance]

        CREATE TABLE [#attendance] ([attendance_type] VARCHAR(50), [order_no] VARCHAR(50), [performance_type] VARCHAR(50), [title_name] VARCHAR(30), [production_name] VARCHAR(50), 
                                    [production_name_short] VARCHAR(50), [production_name_long] VARCHAR(50), [perf_no] VARCHAR(50), [perf_zone] VARCHAR(50), [comp_code_name] VARCHAR(50), 
                                    [order_payment_status] VARCHAR(50), [sale_total] INT, [scan_admission_total] INT, [due_amt] DECIMAL(18,2), [paid_amt] decimal(18,2))

        IF OBJECT_ID('tempdb..#visit_count_raw_data') is not null DROP TABLE [#visit_count_raw_data]
        
        CREATE TABLE [#visit_count_raw_data] ([attend_type] varchar(30), [order_no] VARCHAR(50), [perf_no] VARCHAR(50), [zone_no] VARCHAR(50), [prod_name] varchar(30), [sale_total] int, [scan_admission_total] int)

        IF OBJECT_ID('tempdb..#visit_count_final_data') is not null DROP TABLE [#visit_count_final_data]

        CREATE TABLE [#visit_count_final_data] ([attendance_type] varchar(30), [order_no] VARCHAR(50), [sale_total] int, [scan_admission_total] INT, [report_message] VARCHAR(100))

        CREATE UNIQUE NONCLUSTERED INDEX [visit_count_final_data_order_no] ON [#visit_count_final_data] ([order_no])


    /*  Check Parameters  */

        SELECT @report_start_dt = ISNULL(@report_start_dt,DATEADD(DAY, -1, GETDATE())),
               @report_end_dt = ISNULL(@report_end_dt,DATEADD(DAY,-1,GETDATE()))

        IF @report_end_dt < @report_start_dt BEGIN
            SELECT @rpt_message = 'Invalid dates - End Date Before Start date.'
            GOTO FINISHED
        END

        SELECT @rpt_start_date = CONVERT(CHAR(10),@report_start_dt,111),
               @rpt_end_date = CONVERT(CHAR(10),@report_end_dt,111)

        IF @rpt_start_date < @go_live_date AND @rpt_end_date < @go_live_date
            SELECT @his_start_date = '',
                   @his_end_date = '',
                   @arc_start_date = CONVERT(CHAR(10), @report_start_dt,111),
                   @arc_end_date = CONVERT(CHAR(10),@report_end_dt,111)
        ELSE IF @rpt_start_date > @go_live_date AND @rpt_end_date > @go_live_date
            SELECT @his_start_date = CONVERT(CHAR(10), @report_start_dt,111),
                   @his_end_date = CONVERT(CHAR(10),@report_end_dt,111),
                   @arc_start_date = '',
                   @arc_end_date = ''
        ELSE
           SELECT @his_start_date = @go_live_date,
                  @his_end_date = CONVERT(CHAR(10), @report_end_dt,111),
                  @arc_start_date = CONVERT(CHAR(10), @report_start_dt,111),
                  @arc_end_date = CONVERT(CHAR(10), DATEADD(DAY,-1,CONVERT(DATETIME, @go_live_date)),111)


    /*  Get attendance data for dates on or after go live  */

        IF ISDATE(@his_start_date) = 1 AND ISDATE(@his_end_date) = 1
            INSERT INTO [#attendance]
	        SELECT [attendance_type], CONVERT(VARCHAR(50),[order_no]), [performance_type], [title_name], [production_name], [production_name], [production_name], 
                   CONVERT(VARCHAR(50),[perf_no]), CONVERT(VARCHAR(50),[zone_no]), [comp_code_name], [order_payment_status], [sale_total], [scan_admission_total], [due_amt], [paid_amt]
            FROM [dbo].[LT_HISTORY_TICKET] (NOLOCK)
            WHERE [order_no] IN (SELECT [order_no] FROM [dbo].[LT_HISTORY_TICKET] WHERE [performance_date] BETWEEN @his_start_date AND @his_end_date
                                                                               AND ([performance_type] = 'School Only' OR [price_type_name] LIKE '%school%'))
              AND performance_date between @his_start_date and @his_end_date 
              AND [price_type_name] <> 'Internal Group'
            

    /*  Get attendance date for dates on or after go live  */

        IF ISDATE(@arc_start_date) = 1 AND ISDATE(@arc_end_date) = 1
            INSERT INTO [#attendance]
	        SELECT [attendance_type], [order_no], [performance_type], [title_name], [production_name], [production_name], [production_name], [title_name], [perf_time],
               [comp_code_name], [order_payment_status], [sale_total], [scan_admission_total], [due_amt], [paid_amt]
            FROM [dbo].[LT_HISTORY_TICKET_ARCHIVE] (NOLOCK)
            WHERE [order_no] IN (SELECT [order_no] FROM [dbo].[LT_HISTORY_TICKET_ARCHIVE] WHERE [performance_date] BETWEEN @arc_start_date AND @arc_end_date
                                                                                            AND ([performance_type] = 'School Only' OR [price_type_name] LIKE '%school%'))
              AND [performance_date] between @arc_start_date and @arc_end_date 
              AND [price_type_name] <> 'Internal Group'
              AND [sale_total] <> 0
            --GROUP BY [attendance_type], [performance_type], [title_name], [production_name], [comp_code_name], [order_payment_status]
            --HAVING SUM([sale_total]) <> 0
            --ORDER BY [attendance_type], [performance_type], [title_name], [production_name], [production_name_short], [production_name_long], 
            --         [comp_code_name], [order_payment_status]


            UPDATE [#attendance] SET [perf_zone] = '00:00:00' WHERE [perf_zone] = ''

    /*  Retrieve the Visit Count data for ticketed events from the database  */
            
        INSERT INTO [#visit_count_raw_data]
        SELECT 'ticketed', [order_no], [perf_no], [perf_zone], [production_name], sum([sale_total]), sum([scan_admission_total]) 
        FROM [#attendance]
        WHERE [title_name] in (SELECT [title_name] FROM [dbo].[LV_SS_PRODUCTION_ELEMENTS_TITLE] WHERE [visit_count_title] = 'Y')
        GROUP BY [order_no], [perf_no], perf_zone, [production_name] ORDER BY [order_no]

        DELETE FROM [#visit_count_raw_data] WHERE prod_name like '%Buyout%'

    /*  Added 2/7/2018:  Hard Coded exception added for the Exhibit Halls Special production, which is in the 
                         Exhibit Halls title but should not be counted in with the visit count.  */
         
         DELETE FROM [#visit_count_raw_data] WHERE [prod_name] = 'Exhibit Halls Special'


    /*  Added 10/24/2017:  Deletes orders from the visit count that were created in the buyouts mode of sale regardless of what the production name is.
                           @mos_buyout_no is defined above and gets the mode of sale number from the TR_MOS table.  
                           It must find and if number > 0 for this to happen.  */

            DELETE FROM [#visit_count_raw_data]
            WHERE [order_no] IN (SELECT [order_no] FROM [dbo].[T_ORDER] WHERE [order_no] IN (SELECT [order_no] FROM [#visit_count_raw_data]) AND [MOS] = @mos_buyout_no)



        INSERT INTO [#visit_count_final_data] 
        SELECT 'Ticketed Events', [order_no], max([sale_total]), max([scan_admission_total]), '' FROM [#visit_count_raw_data] GROUP BY [Order_no]

    FINISHED:

        /* Select aggregated data into the report  */

        IF NOT EXISTS (SELECT * FROM [#visit_count_final_data])
            INSERT INTO [#visit_count_final_data] VALUES ('', 0, 0, 0, 'No Rows Found')

        SELECT [attendance_type], count(DISTINCT [order_no]) as 'transaction_count', sum([sale_total]) as 'visit_count', sum([scan_admission_total]) as 'visit_count_scan', [report_message]
        FROM [#visit_count_final_data] GROUP BY [attendance_type], [report_message] ORDER BY [attendance_type]

    CLEAN_UP:

        IF OBJECT_ID('tempdb..#visit_count_final_data') is not null DROP TABLE [#visit_count_final_data]
        IF OBJECT_ID('tempdb..#visit_count_raw_data') is not null DROP TABLE [#visit_count_raw_data]
        IF OBJECT_ID('tempdb..#attendance') IS NOT NULL DROP TABLE [#attendance]

END
GO

GRANT EXECUTE ON [dbo].[LRP_ATTENDANCE_SCHOOL_VISIT_COUNT] to impusers
GO



EXECUTE [dbo].[LRP_ATTENDANCE_SCHOOL_VISIT_COUNT] @report_start_dt = '5-1-2016', @report_end_dt = '5-31-2016'


