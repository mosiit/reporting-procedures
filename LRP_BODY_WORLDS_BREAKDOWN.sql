USE impresario;
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_BODY_WORLDS_BREAKDOWN]') AND type in (N'P', N'PC')) BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_BODY_WORLDS_BREAKDOWN] AS';
    EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LRP_BODY_WORLDS_BREAKDOWN] TO impusers';
    EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LRP_BODY_WORLDS_BREAKDOWN] TO tessitura_app';
END;
GO

ALTER PROCEDURE [dbo].[LRP_BODY_WORLDS_BREAKDOWN]
        @report_start_dt DATETIME = NULL,
        @report_end_dt DATETIME = NULL,
        @include_gate_scans CHAR(1) = 'Y'
AS BEGIN

    SET NOCOUNT ON;
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;


    /*  Procedure Variables and Temp Tables  */
    
        DECLARE @start_date CHAR(10),
                @end_date CHAR(10)

        DECLARE @bw_total_cap DECIMAL (19,6) = 0.0,
                @bw_addon_cap DECIMAL (19,6) = 0.0,
                @on_total_cap DECIMAL (19,6) = 0.0


        DECLARE @percent_table AS TABLE ([performance_date] DATETIME NULL, 
                                         [exhibit_total] DECIMAL (19,4) NOT NULL DEFAULT (0.0), 
                                         [body_total] DECIMAL (19,4) NOT NULL DEFAULT (0.0), 
                                         [body_only_total] DECIMAL (19,4) NOT NULL DEFAULT (0.0),
                                         [body_addon_total] DECIMAL (19,4) NOT NULL DEFAULT (0.0), 
                                         [body_member_total] DECIMAL (19,4) NOT NULL DEFAULT (0.0))       

        IF OBJECT_ID('tempdb..#raw_data') IS NOT NULL DROP TABLE [#raw_data]

        CREATE TABLE [#raw_data] ([performance_date] CHAR(10) NOT NULL DEFAULT (''),
                                  [order_no] INT NOT NULL DEFAULT (0),
                                  [mos_no] INT NOT NULL DEFAULT (0),
                                  [title_no] INT NOT NULL DEFAULT (0),
                                  [title_name] VARCHAR(30) NOT NULL DEFAULT (''),
                                  [production_no] INT NOT NULL DEFAULT (0),
                                  [production_name] VARCHAR(30) NOT NULL DEFAULT (''),
                                  [price_type] INT NOT NULL DEFAULT (0),
                                  [price_type_name] VARCHAR(30) NOT NULL DEFAULT (''),
                                  [due_amount] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                  [category] VARCHAR(30) NOT NULL DEFAULT (''),
                                  [sale_total] INT NOT NULL DEFAULT (0))


    /* Check Parameters  */

        SELECT @report_start_dt = ISNULL(@report_start_dt,'6-16-2018')
        SELECT @report_end_dt = ISNULL(@report_end_dt,DATEADD(DAY,-1,GETDATE()))

        SELECT @start_date = CONVERT(CHAR(10),@report_start_dt,111)
        SELECT @end_date = CONVERT(CHAR(10),@report_end_dt,111)

        SELECT @include_gate_scans = ISNULL(@include_gate_scans,'Y')

    /*  Pull Raw Data  */

        INSERT INTO [#raw_data] ([performance_date], [order_no], [mos_no], [title_no], [title_name], [production_no], [production_name],
                                 [price_type], [price_type_name], [due_amount], [sale_total])
        SELECT tik.[performance_date],
               tik.[order_no],
               ord.[mos],
               tik.[title_no],
               tik.[title_name],
               tik.[production_no],
               tik.[production_name],
               tik.[price_type],
               tik.[price_type_name],
               tik.[due_amt],
               tik.[sale_total]
        FROM [dbo].[LT_HISTORY_TICKET] AS tik
             LEFT OUTER JOIN [dbo].[T_ORDER] AS ord ON ord.[order_no] = tik.[order_no]
        WHERE tik.[performance_date] BETWEEN @start_date AND @end_date
          AND tik.[title_no] IN (27,37)
          AND tik.[production_no] <> 72987      --72987 = Special Exhibitions Buyout


    /*  Pull Exhibit Hall Scans  */

        IF @include_gate_scans = 'Y'
            INSERT INTO [#raw_data] ([performance_date], [order_no], [title_no], [title_name], [production_no], [production_name],
                                     [price_type], [price_type_name], [due_amount], [sale_total])
            SELECT [history_date],
                   0,
                   27,
                   'Exhibit Halls',
                   0,
                   [attendance_type],
                   0,
                   'scan',
                   0.0,
                   visit_count
            FROM [dbo].[LT_HISTORY_VISIT_COUNT]
            WHERE history_date BETWEEN @start_date AND @end_date
              AND [attendance_type] <> 'Ticketed Events'

    /*  Set pricing categories based on amount paid  */

        UPDATE [#raw_data]
        SET [category] = 'Member'
        WHERE [title_no] = 37 AND [due_amount] = 7.0

        UPDATE [#raw_data]
        SET [category] = 'Body Worlds Only'
        WHERE [title_no] = 37 
          AND [mos_no] NOT IN (12,13)
          AND [due_amount] IN (32.0, 29.0, 28.0, 25.0, 27.0, 24.0)

        UPDATE [#raw_data]
        SET [category] = 'Body Worlds School'
        WHERE [title_no] = 37 
          AND [mos_no] IN (12,13)
          AND [due_amount] IN (32.0, 29.0, 28.0, 25.0, 27.0, 24.0)

        UPDATE [#raw_data]
        SET [category] = 'Add-On Public'
        WHERE [title_no] = 37 
          AND [mos_no] NOT IN (12,13)
          AND [category] = ''

        UPDATE [#raw_data]
        SET [category] = 'Add-On School'
        WHERE [title_no] = 37 
          AND [mos_no] IN (12,13)
          AND [category] = ''

    /*  Shorten the Exhibit Halls Title  */

        UPDATE [#raw_data]
        SET [category] = 'Public'
        WHERE [title_no] = 27
          AND [mos_no] NOT IN (12,13)

        UPDATE [#raw_data]
        SET [category] = 'School'
        WHERE [title_no] = 27
          AND [mos_no] IN (12,13)

    /*  Set Special Exhibitions to Body Worlds  */

        UPDATE [#raw_data]
        SET [title_name] = 'Body Worlds'
        WHERE [title_no] = 37;

    /*  Figure out percentages  */
        
        INSERT INTO @percent_table ([performance_date], [exhibit_total], [body_total], [body_only_total], [body_addon_total], [body_member_total])
        SELECT [performance_date], SUM([sale_total]), 0.0, 0.0, 0.0, 0.0
        FROM [#raw_data]
        WHERE title_no = 27
        GROUP BY performance_date, title_name

        INSERT INTO @percent_table ([performance_date], [exhibit_total], [body_total], [body_only_total], [body_addon_total], [body_member_total])
        SELECT [performance_date], 0.0, SUM([sale_total]), 0.0, 0.0, 0.0
        FROM [#raw_data]
        WHERE title_no = 37
        GROUP BY performance_date, title_name

        INSERT INTO @percent_table ([performance_date], [exhibit_total], [body_total], [body_only_total], [body_addon_total], [body_member_total])
        SELECT [performance_date], 0.0, 0.0, SUM([sale_total]), 0.0, 0.0
        FROM [#raw_data]
        WHERE title_no = 37
          AND [category] IN ('Body Worlds Only', 'Body Worlds School')
        GROUP BY performance_date, title_name;

        INSERT INTO @percent_table ([performance_date], [exhibit_total], [body_total], [body_only_total], [body_addon_total], [body_member_total])
        SELECT [performance_date], 0.0, 0.0, 0.0, SUM([sale_total]), 0.0
        FROM [#raw_data]
        WHERE title_no = 37
          AND [category] IN ('Add-On Public', 'Add-On School')
        GROUP BY performance_date, title_name;

        INSERT INTO @percent_table ([performance_date], [exhibit_total], [body_total], [body_only_total], [body_addon_total], [body_member_total])
        SELECT [performance_date], 0.0, 0.0, 0.0, 0.0, SUM([sale_total])
        FROM [#raw_data]
        WHERE title_no = 37
          AND [category] = 'Member'
        GROUP BY performance_date, title_name;

    /*  Figure out total percentages for entire date range in report  */
      
        SELECT @bw_total_cap = CASE WHEN SUM([exhibit_total]) = 0.0 THEN 0.0
                                    ELSE SUM([body_total]) / SUM([exhibit_total] + [body_total]) END,

               @bw_addon_cap = CASE WHEN SUM([exhibit_total]) = 0.0 THEN 0.0
                                    ELSE SUM([body_addon_total]) / SUM([exhibit_total]) END,

               @on_total_cap = CASE WHEN SUM([body_total]) = 0.0 THEN 0.0
                                    ELSE SUM([body_only_total]) / SUM([body_total]) END
        FROM @percent_table;
    
    FINISHED:


        /*  Select final data set for report  */
                    
        WITH CTE_PERCENTAGES (perf_date, exh_total, bw_total, bw_only_total, bw_cap, bw_only_cap)
        AS (SELECT performance_date, 
                   SUM([exhibit_total]) AS [exhibit_total],
                   SUM([body_total]) AS [body_total],
                   SUM([body_addon_total]) AS [body_only_total],
                   CASE WHEN SUM([exhibit_total]) = 0.0 THEN 0.0
                        ELSE SUM([body_total]) / SUM([exhibit_total] + [body_only_total]) END AS [body_cap],
                   CASE WHEN SUM([body_total]) = 0.0 THEN 0.0
                        ELSE SUM([body_only_total]) / SUM([body_total]) END AS [only_cap]
            FROM @percent_table
            GROUP BY [performance_date])
        SELECT CAST(rdt.[performance_date] AS DATE) AS [performance_dt],
               rdt.[title_no],
               rdt.[title_name],
               rdt.[category],
               SUM(rdt.[sale_total]) AS [ticket_count],
               SUM (CASE WHEN rdt.[category] = 'Add-On' THEN rdt.[sale_total] ELSE 0 END) AS [add-on_ticket_count],
               SUM (CASE WHEN rdt.[category] = 'Only' THEN rdt.[sale_total] ELSE 0 END) AS [only_ticket_count],
               SUM (CASE WHEN rdt.[title_no] = 37 THEN rdt.[sale_total] ELSE 0 END) AS [special_ticket_count],
               SUM (CASE WHEN rdt.[title_no] = 27 THEN rdt.[sale_total] ELSE 0 END) AS [exhibit_ticket_count],
               pct.[bw_cap],
               pct.[bw_only_cap],
               @bw_addon_cap AS [bw_cap_total],
               @on_total_cap AS [bw_only_cap_total]
        FROM [#raw_data] AS rdt
             LEFT OUTER JOIN CTE_PERCENTAGES AS pct ON pct.[perf_date] = rdt.[performance_date]
        GROUP BY rdt.[performance_date], rdt.[title_no], rdt.[title_name], rdt.[category], pct.[bw_cap], pct.[bw_only_cap]
        ORDER BY rdt.[performance_date], rdt.[title_no], rdt.[title_name], rdt.[category]

    DONE:


END
GO

EXECUTE [dbo].[LRP_BODY_WORLDS_BREAKDOWN] @report_start_dt = '9-1-2019', @report_end_dt = '9-30-2019'
