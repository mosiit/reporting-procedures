USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_CUST_PROFILE]') AND type in (N'P', N'PC')) BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_CUST_PROFILE] AS' 
    EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LRP_CUST_PROFILE] TO impusers' 
END
GO

ALTER PROCEDURE [dbo].[LRP_CUST_PROFILE]
        @customer_no INT
AS BEGIN

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    SET NOCOUNT ON;

    DECLARE @cust_type_no INT = 0, @cust_type VARCHAR(30) = '', @school_district VARCHAR(100) = ''

    /*  One of multiple procedures called for the Customer Statement report  
        This one pulls basic customer name and address information  */

        /* Determine what kind of constituent it is  */

            SELECT @cust_type_no = ISNULL(cus.[cust_type],0),
                   @cust_type = ISNULL(typ.[description],0)
            FROM dbo.T_CUSTOMER AS cus
                 LEFT OUTER JOIN [dbo].[TR_CUST_TYPE] AS typ ON typ.[id] = cus.[cust_type]
            WHERE cus.[customer_no] = @customer_no

            
        /*  Find the school District if it's a school.  Blank if not a school.
            If multiple found, it uses first one it hits alphabetically  */

            IF @cust_type_no IN (13, 21)        --13 = School / 21 = School Official Record
                SELECT @school_district = ISNULL(MIN(nam.[display_name]),'')
                FROM dbo.T_ASSOCIATION AS aso
                     LEFT OUTER JOIN dbo.FT_CONSTITUENT_DISPLAY_NAME() AS nam ON nam.[customer_no] = aso.[associated_customer_no]
                WHERE aso.[customer_no] = @customer_no
                 AND aso.[association_type_id] = 59  --SCHOOL DISTRICT

    /*  Get the data  */
        SELECT cus.[customer_no], 
               @cust_type_no AS [cust_type_no],
               @cust_type AS [cust_type_name],
               nam.[display_name],
               nam.[display_name_long],
               nam.[display_name_short],
               nam.[sort_name],
               adr.[address_no],
               adr.[address_type] AS [address_type_no],
               typ.[description] AS [address_type_name],
               ISNULL(adr.[street1],'') AS [street1],
               ISNULL(adr.[street2],'') AS [street2],
               ISNULL(adr.[street3],'') AS [street3],
               adr.[city],
               adr.[state],
               CASE WHEN LEN(ISNULL(adr.[postal_code],'')) = 9 
                           THEN LEFT([adr].[postal_code],5) + '-' + RIGHT(adr.[postal_code],4)
                           ELSE ISNULL(adr.[postal_code],'') END AS [postal_code],
               adr.[country] AS [country_no],
               cou.[description] AS [country_name],
               adr.[geo_area] AS [geo_area_no],
               geo.[description] AS [geo_area_name],
               @school_district AS [school_district]
        FROM [dbo].[T_CUSTOMER] AS cus
             LEFT OUTER JOIN [dbo].[FT_CONSTITUENT_DISPLAY_NAME]() AS nam ON nam.[customer_no] = cus.[customer_no]
             LEFT OUTER JOIN [dbo].[FT_GET_PRIMARY_ADDRESS]() AS adr ON adr.[customer_no] = cus.[customer_no]
             LEFT OUTER JOIN [dbo].[TR_ADDRESS_TYPE] AS typ ON typ.[id] = adr.[address_type]
             LEFT OUTER JOIN [dbo].[TR_COUNTRY] AS cou ON cou.[id] = adr.[country]
             LEFT OUTER JOIN [dbo].[TR_GEO_AREA] AS geo ON geo.[id] = adr.[geo_area]
        WHERE cus.[customer_no] = @customer_no

END
GO

EXECUTE [dbo].[LRP_CUST_PROFILE] @customer_no = 3267933

