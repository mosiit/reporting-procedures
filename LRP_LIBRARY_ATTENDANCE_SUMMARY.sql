USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_LIBRARY_ATTENDANCE_SUMMARY]') AND type in (N'P', N'PC'))
BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_LIBRARY_ATTENDANCE_SUMMARY] AS' 
    EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LRP_LIBRARY_ATTENDANCE_SUMMARY] TO ImpUsers' 
END
GO

ALTER PROCEDURE [dbo].[LRP_LIBRARY_ATTENDANCE_SUMMARY]
        @report_start_dt DATETIME = NULL,
        @report_end_dt DATETIME = NULL,
        @free_or_paid VARCHAR(5) = 'All',
        @library_list_str VARCHAR(4000) = '',
        @exhibit_halls_only CHAR(1) = 'N',
        @include_no_visits CHAR(1) = 'Y',
        @summarize_by_visit_date CHAR(1) = 'N'
WITH RECOMPILE AS BEGIN

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    SET NOCOUNT ON;

    /*  Temp Table to hold summarized data  */
        
        IF OBJECT_ID('tempdb..#library_attendance_summary') IS NOT NULL DROP TABLE [#library_attendance_summary];

        CREATE TABLE [#library_attendance_summary] ([library_no] INT, [membership_no] INT, [library_type] VARCHAR(25), [currently_active] CHAR(1), [active_on_visit_date] CHAR(1), 
                                                    [active_member_no] INT, [library_name] VARCHAR(100), [customer_type] VARCHAR(30), [street1] VARCHAR(100), [street2] VARCHAR(100), 
                                                    [street3] VARCHAR(100), [city] VARCHAR(50), [state] VARCHAR(25), [postal_code] VARCHAR(25), [order_no] INT, [visit_dt] DATETIME, 
                                                    [visit_date] CHAR(10), [visit_date_exh] CHAR(10), [visit_date_not_exh] CHAR(10), [title_name] VARCHAR(30), [title_name_sort] VARCHAR(35),
                                                    [production_name] VARCHAR(30), [production_name_long] VARCHAR(150), [comp_code] VARCHAR(30), [price_type] VARCHAR(30), [total_count] INT, 
                                                    [total_count_exh] INT, [total_count_not_exh] INT, [total_paid] DECIMAL(18,2), [total_paid_exh] DECIMAL(18,2), [total_paid_not_exh] DECIMAL(18,2))

        IF OBJECT_ID('tempdb..#final_summaries') IS NOT NULL DROP TABLE [#final_summaries];

        CREATE TABLE [#final_summaries] ([library_no] INT, [library_type] VARCHAR(25), [currently_active] CHAR(1), [active_on_visit_date] CHAR(1), [active_member_no] int, 
                                         [library_name] VARCHAR(100), [customer_type] VARCHAR(30), [street1] VARCHAR(100), [street2] VARCHAR(100), [street3] VARCHAR(100),
                                         [city] VARCHAR(50), [state] VARCHAR(25), [postal_code] VARCHAR(25), [visit_date] CHAR(10), [visit_days] INT, [total_count] INT,
                                         [total_count_exh] int, [total_count_not_exh] INT, [total_paid] DECIMAL(18,2), [total_paid_exh] DECIMAL(18,2), [total_paid_not_exh] DECIMAL(18,2))

    
    /*  Get all data using the LRP_LIBRARY_ATTENDANCE procedure  */

        INSERT INTO [#library_attendance_summary] ([library_no],[membership_no],[library_type],[currently_active],[active_on_visit_date],[active_member_no],[library_name],[customer_type],
                                                   [street1],[street2],[street3],[city],[state],[postal_code],[order_no],[visit_dt],[visit_date],[visit_date_exh],[visit_date_not_exh],[title_name],
                                                   [title_name_sort],[production_name],[production_name_long],[comp_code],[price_type],[total_count],[total_count_exh],[total_count_not_exh],
                                                   [total_paid],[total_paid_exh],[total_paid_not_exh])
        EXECUTE [dbo].[LRP_LIBRARY_ATTENDANCE] @report_start_dt, @report_end_dt, @free_or_paid, @library_list_str, @exhibit_halls_only, @include_no_visits
        
    /*  If not summarizing by visit date, blank out that field  */

        IF @summarize_by_visit_date = 'N'
            UPDATE [#library_attendance_summary]
            SET [visit_date] = ''

    /*  Summarize the data  */
       
        INSERT INTO [#final_summaries] ([library_no],[library_type],[currently_active],[active_on_visit_date],[active_member_no],[library_name],[customer_type],[street1],[street2],[street3],
                                        [city],[state],[postal_code],[visit_date],[visit_days],[total_count],[total_count_exh],[total_count_not_exh],[total_paid],[total_paid_exh],[total_paid_not_exh])
        SELECT [library_no],
               [library_type],
               [currently_active],
               [active_on_visit_date],
               0, --[active_member_no],
               [library_name],
               'Library' AS [customer_type],
               [street1],
               [street2],
               [street3],
               [city],
               [state],
               [postal_code],
               [visit_date],
               COUNT(DISTINCT [visit_dt]) AS [visit_days],
               SUM([total_count_exh]) AS [total_count],
               SUM([total_count_exh]) AS [total_count_exh],
               SUM([total_count_not_exh]) AS [total_count_not_exh],
               SUM([total_paid]) AS [total_paid],
               SUM([total_paid_exh]) AS [total_paid_exh],
               SUM([total_paid_not_exh]) AS [total_paid_not_exh]
        FROM [#library_attendance_summary]
        GROUP BY [library_no], [library_type], [currently_active], [active_on_visit_date], --[active_member_no], 
                 [library_name], [street1], [street2], [street3], [city], [state], [postal_code], [visit_date]

    /* Reset Visit day Numbers for No Visit Libraries  */

        UPDATE [#final_summaries]
        SET [visit_days] = 0
        WHERE [library_no] IN (SELECT [library_no] FROM [#library_attendance_summary] WHERE [visit_dt] = '1900-01-01')

    FINISHED:

        /*  Select final summarized data set for the report */
        
            SELECT [library_no],
                   [library_type],
                   [currently_active],
                   [active_on_visit_date],
                   [active_member_no],
                   [library_name],
                   [customer_type],
                   [street1],
                   [street2],
                   [street3],
                   [city],
                   [state],
                   [postal_code],
                   [visit_date],
                   [visit_days],
                   [total_count],
                   [total_count_exh],
                   [total_count_not_exh],
                   [total_paid],
                   [total_paid_exh],
                   [total_paid_not_exh]
            FROM [#final_summaries]
    DONE:

        IF OBJECT_ID('tempdb..#final_summaries') IS NOT NULL DROP TABLE [#final_summaries];
        IF OBJECT_ID('tempdb..#library_attendance_summary') IS NOT NULL DROP TABLE [#library_attendance_summary];

END
GO

--EXECUTE [dbo].[LRP_LIBRARY_ATTENDANCE_SUMMARY] '10-1-2017', '11-30-2017', 'All', '', 'N', 'Y', 'N'

--SELECT * FROM dbo.T_ATTENDANCE AS att
--SELECT * FROM [#library_list]
--SELECT * FROM [#library_list]
--SELECT * FROM dbo.T_NSCAN_EVENT_CONTROL WHERE scan_str = 'C0000102569' AND ISNULL(attendance_id,0) > 0
--SELECT * FROM dbo.TR_CUST_TYPE
--WHERE id in
--    (SELECT cust_type FROM T_CUSTOMER WHERE lname LIKE '%library%' AND inactive = 1 AND cust_type NOT IN (11,20))
--SELECT * FROM T_CUSTOMER WHERE lname LIKE '%library%' AND inactive = 1 AND cust_type NOT IN (11,20)
--SELECT * FROM T_CUSTOMER WHERE cust_type = 20 AND lname NOT LIKE '%library%'
--SELECT * FROM dbo.TR_CUST_INACTIVE
