USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_SCHOOL_ATTENDANCE]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LRP_SCHOOL_ATTENDANCE]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LRP_SCHOOL_ATTENDANCE]
        @report_start_dt DATETIME = Null,
        @report_end_dt DATETIME = Null
AS BEGIN

        DECLARE @rpt_msg varchar(100)       SELECT @rpt_msg = ''
        DECLARE @final_orders TABLE ([order_no] INT
                                     PRIMARY KEY CLUSTERED ([order_no] ASC))

    /* Null Dates = Yesterday's Date  */
    
        SELECT @report_start_dt = ISNULL(@report_start_dt,DATEADD(DAY,-1,GETDATE())),
               @report_end_dt = ISNULL(@report_end_dt,DATEADD(DAY,-1,GETDATE()))

    /*  Make sure the dates passed to the procedure include the entire day (from midnight on the start date to 11:59 PM on the end date)  */

        SELECT @report_start_dt = convert(char(10),@report_start_dt,111) + ' 00:00:00',
               @report_end_dt = convert(char(10),@report_end_dt,111) + ' 23:59:59.997'

    /*  Check Date Range to make sure report is being run for past dates  */

        IF CONVERT(DATE,@report_start_dt) > CONVERT(DATE,GETDATE()) OR CONVERT(DATE,@report_end_dt) > CONVERT(DATE,GETDATE()) BEGIN
            SELECT @rpt_msg = 'This report can only be run for PAST dates.'
            GOTO FINISHED
        END

    /*  Create the Temp Table to hold raw data from the T_SUB_LINEITEM TABLE  */

        IF OBJECT_ID('tempdb..#sub_lineitems') IS NOT NULL DROP TABLE [#sub_lineitems]

        CREATE TABLE [#sub_lineitems] ([sli_no] INT NOT NULL, [order_no] INT, [mode_of_sale] INT, [perf_no] int, [perf_date] CHAR(10), [perf_time] VARCHAR(8), [perf_type] INT, 
                                       [due_amt] decimal (18,2), [paid_amt] decimal(18,2), [sli_status] int, [price_type] INT)
                                                
        ALTER TABLE [#sub_lineitems] ADD CONSTRAINT [PK_SCH_ATT_SUB] PRIMARY KEY NONCLUSTERED ([sli_no] ASC) ON [PRIMARY]

        CREATE CLUSTERED INDEX [sub_lineitems_order_no] ON [#sub_lineitems] ([order_no] ASC) ON [PRIMARY]
                         
    /*  Get Sublineitem raw data  */

        INSERT INTO [#sub_lineitems]
        SELECT sli.[sli_no], sli.[order_no], ord.[MOS], sli.[perf_no], prf.[performance_date], prf.[performance_time], prf.[performance_type], 
               sli.[due_amt], sli.[paid_amt], sli.[sli_status], sli.[price_type]
        FROM [dbo].[T_SUB_LINEITEM] as sli (NOLOCK)
             INNER JOIN  [dbo].[T_ORDER] AS ord (NOLOCK) ON sli.[order_no] = ord.[order_no]
             INNER JOIN [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] AS prf (NOLOCK) ON prf.[performance_no] = sli.[perf_no] AND prf.[performance_zone] = sli.[zone_no]
        WHERE prf.[performance_dt] BETWEEN @report_start_dt AND @report_end_dt 
          AND sli.[sli_status] in (2, 3, 12)                            --SLI Status 2 = Seated, Unpaid/3 = Seated, Paid/12 = Ticketed, Paid
          AND (prf.[performance_type] = 3 or ord.[mos] IN (12,13))      --perf type 3 = School Only/mos 12 = Schools/mos 13 = Web Sales School)

    /*  Double-check performance dates  */
                                                 
        DELETE FROM [#sub_lineitems] WHERE [perf_date] not between convert(char(10), @report_start_dt,111) and convert(char(10),@report_end_dt,111)

    /*  Generate the list of orders  */

        INSERT INTO @final_orders
        SELECT DISTINCT [order_no] FROM #sub_lineitems

    FINISHED:

        IF NOT EXISTS (SELECT * FROM [dbo].[LT_HISTORY_TICKET] (NOLOCK) WHERE [order_no] IN (SELECT [order_no] FROM @final_orders)) BEGIN
            IF @rpt_msg = '' SELECT @rpt_msg = 'No records found for the criteria you entered.'
            SELECT @rpt_msg AS 'report_message', '' AS 'attendance_type', '' AS 'performance_type', '' AS 'title_name', '' AS 'production_name', 
                   '' AS 'production_name_short', '' AS 'production_name_long', '' AS 'comp_code_name', '' AS 'order_payment_status', '' AS 'price_type_name',
                   0.00 as 'sale_total', 0.00 as 'scan_admission_total', 0.00 as 'due_amt', 0.00 as 'paid_amt', 0 AS 'order_mode_of_sale'
        END ELSE BEGIN
            SELECT @rpt_msg AS 'report_message', 'School' AS 'attendance_type', 'School Only' AS 'performance_type', his.[title_name], his.[production_name], 
                   his.[production_name_short], his.[production_name_long], his.[comp_code_name], his.[order_payment_status], his.[price_type_name], 
                   SUM(his.[sale_total]) as 'sale_total', SUM(his.[scan_admission_total]) as 'scan_admission_total', sum(his.[due_amt]) as 'due_amt', 
                   SUM(his.[paid_amt]) as 'paid_amt', ord.[MOS] AS 'order_mode_of_sale'
            FROM [dbo].[LT_HISTORY_TICKET] AS his (NOLOCK)
                 LEFT OUTER JOIN dbo.T_ORDER AS ord (NOLOCK) ON ord.[order_no] = his.[order_no]
            WHERE his.[order_no] IN (SELECT DISTINCT [order_no] FROM @final_orders)
            GROUP BY his.[title_name], his.[production_name], his.[production_name_short], his.[production_name_long], his.[comp_code_name], 
                     his.[order_payment_status], his.[price_type_name], ord.[MOS]
            --ORDER BY [attendance_type], [performance_type], [title_name], [production_name], [production_name_short], [production_name_long], 
            --         [comp_code_name], [order_payment_status], his.[price_type_name]

        END

    CLEAN_UP:

        /*  Not really necessary, but the programmer in me wants to clean up  */

            IF OBJECT_ID('tempdb..#sub_lineitems') IS NOT NULL DROP TABLE [#sub_lineitems]
            
    DONE:

END
GO

GRANT EXECUTE ON [dbo].[LRP_SCHOOL_ATTENDANCE] TO impusers
GO

EXECUTE [dbo].[LRP_SCHOOL_ATTENDANCE] @report_start_dt = '1-1-2017', @report_end_dt = '1-31-2017'

--SELECT sli.[sli_no], sli.[order_no], ord.[MOS], sli.[perf_no], prf.[performance_date], prf.[performance_time], prf.[performance_type], 
--       sli.[due_amt], sli.[paid_amt], sli.[sli_status], sli.[price_type]
--FROM [dbo].[T_SUB_LINEITEM] as sli (NOLOCK)
--     INNER JOIN  [dbo].[T_ORDER] AS ord (NOLOCK) ON sli.[order_no] = ord.[order_no]
--     INNER JOIN [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] AS prf (NOLOCK) ON prf.[performance_no] = sli.[perf_no] AND prf.[performance_zone] = sli.[zone_no]
--WHERE prf.[performance_dt] BETWEEN '3-1-2017' AND '3-1-2017 23:59:59' --AND sli.[order_no] NOT IN (386029,508931,509017,386029)
--          AND sli.[sli_status] in (2, 3, 12)                            --SLI Status 2 = Seated, Unpaid/3 = Seated, Paid/12 = Ticketed, Paid
--          AND (prf.[performance_type] = 3 or ord.[mos] IN (12,13))      --perf type 3 = School Only/mos 12 = Schools/mos 13 = Web Sales School)

--          SELECT * FROM dbo.LT_HISTORY_TICKET WHERE order_no IN (386029,508931,509017)
--          SELECT * FROM dbo.T_ORDER WHERE order_no = 478675

