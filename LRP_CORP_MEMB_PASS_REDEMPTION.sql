USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LRP_CORP_MEMB_PASS_REDEMPTION]    Script Date: 3/18/2022 8:28:07 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


/*
    [LRP_CORP_MEMB_PASS_REDEMPTION]
    Most reports are designated either as "Attendance" (past dates) or "Sales" (today and future dates).
    This report can be both, so the data is gathered in two steps.  
        The first step uses the history table to pull data for any past dates
        The second step uses the live sales tables to pull any data for any current or future dates

*/

ALTER PROCEDURE [dbo].[LRP_CORP_MEMB_PASS_REDEMPTION]
        @report_start_dt DATETIME = NULL,
        @report_end_dt DATETIME = NULL,
        @title_str VARCHAR(4000) = '',
        @production_str VARCHAR(4000) = '',
        @source_str VARCHAR(4000) = '',
        @source_group_str VARCHAR(4000) = ''
WITH RECOMPILE AS BEGIN

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;
  
    /*  Procedure Variables and Temp Tables  */

        DECLARE @past_start_dt DATETIME = NULL,     @past_end_dt DATETIME = NULL, 
                @future_start_dt DATETIME = NULL,   @future_end_dt DATETIME = NULL;

        DECLARE @today DATETIME = CAST(GETDATE() AS DATE);

        DECLARE @title_list TABLE ([title_no] INT NOT NULL DEFAULT (0));
        DECLARE @production_list TABLE ([production_no] INT NOT NULL DEFAULT (0));
        DECLARE @source_list TABLE ([source_no] INT NOT NULL DEFAULT (0));
        DECLARE @source_group_list TABLE ([source_group_no] INT NOT NULL DEFAULT (0));

        IF OBJECT_ID('tempdb..#corp_pass_redemption') IS NOT NULL DROP TABLE [#corp_pass_redemption];

        CREATE TABLE [#corp_pass_redemption] ([order_no] INT NOT NULL DEFAULT (0),
                                              [appeal_no] INT NOT NULL DEFAULT (0),
                                              [appeal_name] VARCHAR(30) NOT NULL DEFAULT (''),
                                              [appeal_campaign] INT NOT NULL DEFAULT (0),
                                              [source_no] INT NOT NULL DEFAULT (0),
                                              [source_name] VARCHAR(100) NOT NULL DEFAULT (''),
                                              [source_customer_no] VARCHAR(30) NOT NULL DEFAULT (''),
                                              [source_group] INT NOT NULL DEFAULT (0),
                                              [source_group_name] VARCHAR(30) NOT NULL DEFAULT (''),
                                              [sli_no] INT NOT NULL DEFAULT (0),
                                              [price_type] INT NOT NULL DEFAULT (0),
                                              [price_type_name] VARCHAR(30) NOT NULL DEFAULT (''),
                                              [due_amt] DECIMAL (18,2) NOT NULL DEFAULT (0.0),
                                              [paid_amt] DECIMAL (18,2) NOT NULL DEFAULT (0.0),
                                              [ticket_no] INT NOT NULL DEFAULT (0),
                                              [sli_status] INT NOT NULL DEFAULT (0),
                                              [sli_status_name] VARCHAR(30) NOT NULL DEFAULT (''),
                                              [perf_no] INT NOT NULL DEFAULT (0),
                                              [zone_no] INT NOT NULL DEFAULT (0),
                                              [performance_dt] DATETIME NULL,
                                              [performance_time] VARCHAR(10) NOT NULL DEFAULT (''),
                                              [performance_time_display] VARCHAR(15) NOT NULL DEFAULT (''),
                                              [title_no] INT NOT NULL DEFAULT (0),
                                              [title_name] VARCHAR(30) NOT NULL DEFAULT (''),
                                              [production_no] INT NOT NULL DEFAULT (0),
                                              [production_name] VARCHAR(30) NOT NULL DEFAULT (''),
                                              [production_name_long] VARCHAR(100) NOT NULL DEFAULT (''),
                                              [rule_ind] CHAR(1) NOT NULL DEFAULT (''),
                                              [rule_id] INT NOT NULL DEFAULT (0),
                                              [rule_name] VARCHAR(30) NOT NULL DEFAULT (''),
                                              [customer_no] INT NOT NULL DEFAULT (0),
                                              [customer_name] VARCHAR(100) NOT NULL DEFAULT (''),
                                              [customer_sort] VARCHAR(100) NOT NULL DEFAULT (''),
                                              [cust_memb_no] INT NOT NULL DEFAULT (0),
                                              [memb_level] VARCHAR(20) NOT NULL DEFAULT (''),
                                              [memb_level_name] VARCHAR(50) NOT NULL DEFAULT (''),
                                              [memb_init_dt] DATETIME NULL,
                                              [memb_expire_dt] DATETIME NULL,
                                              [memb_fiscal_year] INT NOT NULL DEFAULT (0),
                                              [seat_counter] INT NOT NULL DEFAULT (0));

    /*  Check Parameters  */
    
        SELECT @report_start_dt = CAST(@report_start_dt AS DATE),
               @report_end_dt = CONVERT(CHAR(10),@report_end_dt,111) + ' 23:59:59.957';

        IF @report_end_dt >= @report_start_dt BEGIN

            IF CAST(@report_start_dt AS DATE) < @today AND CAST(@report_end_dt AS DATE) < @today
                SELECT @past_start_dt = @report_start_dt, @past_end_dt = @report_end_dt;

            ELSE IF CAST(@report_start_dt AS DATE) >= @today AND CAST(@report_end_dt AS DATE) >= @today
                SELECT @future_start_dt = @report_start_dt, @future_end_dt = @report_end_dt;

            ELSE IF CAST(@report_start_dt AS DATE) < @today AND CAST(@report_end_dt AS DATE) >= @today
                SELECT @past_start_dt = @report_start_dt, @past_end_dt = DATEADD(SECOND,-1,@today),
                       @future_start_dt = @today, @future_end_dt = @report_end_dt;

        END

        IF ISNULL(@title_str, '') = '' INSERT INTO @title_list ([title_no]) SELECT [title_no] FROM [dbo].[T_TITLE]
        ELSE INSERT INTO @title_list ([title_no]) SELECT [Element] FROM [dbo].[FT_SPLIT_LIST](REPLACE(@title_str,'"',''),',');
        
        IF ISNULL(@production_str, '') = '' INSERT INTO @production_list ([production_no]) SELECT [prod_no] FROM [dbo].[T_PRODUCTION]
        ELSE INSERT INTO @production_list ([production_no]) SELECT [Element] FROM [dbo].[FT_SPLIT_LIST](REPLACE(@production_str,'"',''),',');
        
        IF ISNULL(@source_str, '') = '' INSERT INTO @source_list ([source_no])  SELECT DISTINCT [source_no] FROM [dbo].[LV_ACTIVE_CORPORATE_MEMBERS]
        ELSE INSERT INTO @source_list ([source_no]) SELECT [Element] FROM [dbo].[FT_SPLIT_LIST](REPLACE(@source_str,'"',''),',');

        IF ISNULL(@source_group_str, '') = '' INSERT INTO @source_group_list ([source_group_no]) SELECT DISTINCT [source_group] FROM [dbo].[LV_ACTIVE_CORPORATE_MEMBERS] WHERE ISNULL([source_group],0) > 0
        ELSE INSERT INTO @source_group_list ([source_group_no]) SELECT [Element] FROM [dbo].[FT_SPLIT_LIST](REPLACE(@source_group_str,'"',''),',');

    /*  Get pass redemption data  */

            WITH [CTE_RULES] ([rule_no], [rule_name]) AS (SELECT [id], 
                                                                 [description] 
                                                          FROM [dbo].[T_PRICING_RULE]
                                                          WHERE [appeals] = '1760' 
                                                             OR [appeals] LIKE '%1760,%' 
                                                             OR [appeals] LIKE '%,1760%'),
             [CTE_ORDERS] ([order_no], [customer_no], [source_no]) AS (SELECT DISTINCT ord.[order_no], 
                                                                                       ord.[customer_no], 
                                                                                       lit.[line_source_no]
                                                                        FROM [dbo].[T_LINEITEM] AS lit
                                                                             INNER JOIN @source_list AS sou ON sou.[source_no] = lit.line_source_no
                                                                             INNER JOIN [dbo].[T_ORDER] AS ord ON ord.[order_no] = lit.[order_no])
            INSERT INTO [#corp_pass_redemption] ([order_no],[appeal_no],[appeal_name],[appeal_campaign],[source_no],[source_name],[source_customer_no],[source_group],[source_group_name],[sli_no],
                                                 [price_type],[price_type_name],[due_amt],[paid_amt],[ticket_no],[sli_status],[sli_status_name],[perf_no],[zone_no],[performance_dt],[performance_time],
                                                 [performance_time_display],[title_no],[title_name],[production_no],[production_name],[production_name_long],[rule_ind],[rule_id],[rule_name],
                                                 [customer_no], [customer_name], [customer_sort],[seat_counter])
            SELECT sli.[order_no],
                   sou.[appeal_no],
                   apl.[description],
                   apl.[campaign_no],
                   ord.[source_no],
                   sou.[source_name],
                   CASE WHEN sou.[source_name] LIKE '%(%)%' THEN
                        SUBSTRING (sou.[source_name], CHARINDEX('(', sou.[source_name]) + 1, CHARINDEX(')', sou.[source_name]) - CHARINDEX('(', sou.[source_name]) -1)
                        ELSE '0' END AS [source_customer],
                   sou.[source_group],
                   grp.[description],
                   sli.[sli_no],   
                   sli.[price_type],
                   ptp.[description] AS [price_type_name],
                   sli.[due_amt],
                   sli.[paid_amt],
				   -- H. Sheridan, Service Now ticket INC0124249.  Even though the default in the temp table definition is '0', it appears to still put in a null.  Forcing a '0' if it is null.
                   ISNULL(sli.[ticket_no], 0),
                   sli.[sli_status],
                   sta.[description] AS [sli_status_name],
                   sli.[perf_no],
                   sli.[zone_no],
                   prf.[performance_dt],
                   prf.[performance_time],
                   prf.[performance_time_display],
                   prf.[title_no],
                   prf.[title_name],
                   prf.[production_no],
                   prf.[production_name],
                   prf.[production_name_long],
                   sli.[rule_ind],
                   ISNULL(sli.[rule_id], 0) AS [rule_id],
                   rul.[rule_name],
                   ord.[customer_no],
                   nam.[display_name],
                   nam.[sort_name],
                   1
            FROM [dbo].[T_SUB_LINEITEM] AS sli
                 INNER JOIN [CTE_RULES] AS rul ON rul.[rule_no] = ISNULL(sli.[rule_id], 0)
                 INNER JOIN [dbo].[TR_PRICE_TYPE] AS ptp ON ptp.[id] = sli.[price_type]
                 INNER JOIN [dbo].[TR_SLI_STATUS] AS sta ON sta.[id] = sli.[sli_status]
                 LEFT OUTER JOIN [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] AS prf ON prf.[performance_no] = sli.[perf_no] AND prf.[performance_zone] = sli.[zone_no]
                 INNER JOIN @title_list AS ttl ON ttl.[title_no] = prf.[title_no]
                 INNER JOIN @production_list AS pro ON pro.[production_no] = prf.[production_no]
                 INNER JOIN [CTE_ORDERS] AS ord ON ord.[order_no] = sli.[order_no]
                 INNER JOIN [dbo].[TX_APPEAL_MEDIA_TYPE] AS sou ON sou.[source_no] = ord.[source_no]
                 LEFT OUTER JOIN [dbo].[T_APPEAL] AS apl ON apl.[appeal_no] = sou.[appeal_no]
                 LEFT OUTER JOIN [dbo].[TR_SOURCE_GROUP] AS grp ON grp.[id] = sou.[source_group]
                 LEFT OUTER JOIN [dbo].[FT_CONSTITUENT_DISPLAY_NAME]() AS nam ON nam.[customer_no] = ord.[customer_no]
            WHERE CAST(prf.[performance_dt] AS DATE) BETWEEN @report_start_dt AND @report_end_dt -- @future_start_dt AND @future_end_dt
              AND sli.[sli_status] IN (3, 12)
              AND sli.[rule_ind] = 'R';


    /*  Remove tickets that were paid for  */

        DELETE FROM [#corp_pass_redemption]
        WHERE [due_amt] <> 0.0;
        
    /*  Get Membership information  */

        --Determine Cust Memb Number
        UPDATE [#corp_pass_redemption]
        SET [cust_memb_no] = [dbo].[LFS_ActiveMemberNumberOnDate]([source_customer_no], 7, [performance_dt])
        WHERE ISNULL(TRY_CAST([source_customer_no] AS INT), 0) > 0;

        --Convert null values (if any) to 0
        UPDATE [#corp_pass_redemption]
        SET [cust_memb_no] = 0
        WHERE ISNULL([cust_memb_no], 0) <= 0;

        --Get membership information based on cust memb number
        UPDATE red
        SET red.[memb_level] = mem.[memb_level],
            red.[memb_level_name] = lev.[description],
            red.[memb_init_dt] = mem.[init_dt],
            red.[memb_expire_dt] = mem.[expr_dt]
        FROM [#corp_pass_redemption] AS red
             INNER JOIN [dbo].[TX_CUST_MEMBERSHIP] AS mem ON mem.[cust_memb_no] = red.[cust_memb_no]
             INNER JOIN [dbo].[T_MEMB_LEVEL] AS lev ON lev.[memb_level] = mem.[memb_level]
        WHERE ISNULL(red.[cust_memb_no], 0) > 0;

        --Get fiscal year based on init datetime
        UPDATE [#corp_pass_redemption]
        SET [memb_fiscal_year] = [dbo].[LF_GetFiscalYear](ISNULL([memb_init_dt],GETDATE()));

    FINISHED:

        /*  Get Final Data Set  */

            WITH [CTE_CORP_MEMB] ([source_no],[source_name],[customer_no],[street1],[street2],[street3],[city],[state],[postal_code])
            AS (SELECT DISTINCT [source_no], 
                                [source_name], 
                                [customer_no],
                                [street1],
                                [street2],
                                [street3],
                                [city],
                                [state],
                                [postal_code]
                FROM [dbo].[LV_ACTIVE_CORPORATE_MEMBERS])
            SELECT red.[order_no],
                   red.[appeal_no],
                   red.[appeal_name],
                   red.[appeal_campaign],
                   red.[source_group],
                   red.[source_group_name],
                   red.[source_no],
                   red.[source_customer_no],
                   red.[source_name],
                   ISNULL(mem.[street1], '') AS [street1],
                   ISNULL(mem.[street2], '') AS [street2],
                   ISNULL(mem.[street3], '') AS [street3],
                   ISNULL(mem.[city], '') AS [city],
                   ISNULL(mem.[state], '') AS [state],
                   ISNULL(mem.[postal_code], '') AS [postal_code],
                   red.[cust_memb_no],
                   red.[memb_level],
                   red.[memb_level_name],
                   red.[memb_init_dt],
                   red.[memb_expire_dt],
                   red.[memb_fiscal_year],
                   red.[sli_no],
                   red.[price_type],
                   red.[price_type_name],
                   red.[due_amt],
                   red.[paid_amt],
                   red.[ticket_no],
                   red.[sli_status],
                   red.[sli_status_name],
                   red.[perf_no],
                   red.[zone_no],
                   CAST(red.[performance_dt] AS DATE) AS [performance_dt],
                   red.[performance_time],
                   red.[performance_time_display],
                   red.[title_no],
                   red.[title_name],
                   red.[production_no],
                   red.[production_name],
                   red.[production_name_long],
                   red.[rule_ind],
                   red.[rule_id],
                   red.[rule_name],
                   red.[customer_no],
                   red.[customer_name],
                   red.[customer_sort] + '_' + CAST(red.[customer_no] AS VARCHAR(30)) AS [customer_sort],
                   red.[seat_counter]
            FROM [#corp_pass_redemption] AS red
                 INNER JOIN @source_list AS sou ON sou.[source_no] = red.[source_no]
                 INNER JOIN @source_group_list AS grp ON grp.[source_group_no] = red.[source_group]
                 LEFT OUTER JOIN [CTE_CORP_MEMB] AS mem ON mem.[source_no] = red.[source_no]
                 --LEFT OUTER JOIN [dbo].[LV_ACTIVE_CORPORATE_MEMBERS] AS mem ON mem.[customer_no] = red.[source_customer_no] AND mem.[memb_expr_dt] = red.[memb_expire_dt]

    DONE:

END

GO


