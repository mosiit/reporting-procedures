USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_TESSITURA_CASHOUT_TRX_STATS]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LRP_TESSITURA_CASHOUT_TRX_STATS]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LRP_TESSITURA_CASHOUT_TRX_STATS]
        @report_start_dt datetime,
        @report_end_dt datetime,
        @report_operator varchar(8),
        @machine_locations VARCHAR(4000)
AS BEGIN
    
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

    /*  Report Parameters*/

        DECLARE @rpt_date char(10), @rpt_message varchar(100), @work_dt DATETIME
        DECLARE @location_list TABLE ([location_no_str] VARCHAR(20), [location_no] INT)
        DECLARE @machine_list TABLE ([machine_name] VARCHAR(50))
        
        DECLARE @run_dt DATETIME = GETDATE()

        DECLARE @temp_table TABLE ([run_dt] DATETIME, [history_dt] DATETIME, [history_date] CHAR(10), [user_id] VARCHAR(25), [user_name] VARCHAR(125), [user_name_sort] VARCHAR(125),
                                   [user_location] VARCHAR(50), [orders_created] DECIMAL(18,4), [orders_touched] DECIMAL(18,4), [membership_sales] DECIMAL(18,4), [total_one_step_recovery]  DECIMAL(18,4),
                                   [total_gift_memberships] DECIMAL(18,4), [basic_2_sales] DECIMAL(18,4), [basic_5_sales] DECIMAL(18,4), [basic_8_sales] DECIMAL(18,4), [basic_total_sales] DECIMAL(18,4), 
                                   [basic_one_step_recovery]  DECIMAL(18,4), [basic_gift_memberships] DECIMAL(18,4), [one_step_basic] DECIMAL(18,4), [premier_2_sales] DECIMAL(18,4), 
                                   [premier_5_sales] DECIMAL(18,4), [premier_8_sales] DECIMAL(18,4), [premier_total_sales] DECIMAL(18,4), [premier_one_step_recovery] DECIMAL(18,4), 
                                   [premier_gift_memberships] DECIMAL(18,4), [one_step_premier] DECIMAL(18,4), [one_step_total] DECIMAL(18,4), [orders_counted] DECIMAL(18,4), 
                                   [multi_venue_orders] DECIMAL(18,4), [total_venues_sold] DECIMAL(18,4))

        DECLARE @trx_stat_table table ([rpt_date] datetime, [rpt_operator] varchar(10), [trx_started] decimal(18,4), [trx_touched] decimal(18,4), [trx_memb_basic] decimal(18,4), [trx_memb_premier] decimal(18,4), 
                                       [trx_memb_total] decimal(18,4), [trx_memb_basic_percent_started] decimal(18,4), [trx_memb_basic_percent_touched] decimal(18,4), [trx_memb_premier_percent_started] decimal(18,4), 
                                       [trx_memb_premier_percent_touched] decimal(18,4), [trx_memb_total_percent_started] decimal(18,4), [trx_memb_total_percent_touched] decimal(18,4), [trx_memb_basic_onestep] decimal(18,4), 
                                       [trx_memb_basic_onestep_percent_started] decimal(18,4), [trx_memb_basic_onestep_percent_touched] decimal(18,4), [trx_memb_basic_onestep_percent_Basic] decimal(18,4), 
                                       [trx_memb_basic_onestep_percent_Memberships] decimal(18,4), [trx_memb_premier_onestep] decimal(18,4), [trx_memb_premier_onestep_percent_started] decimal(18,4), 
                                       [trx_memb_premier_onestep_percent_touched] decimal(18,4), [trx_memb_premier_onestep_percent_premier] decimal(18,4), [trx_memb_premier_onestep_percent_memberships] decimal(18,4), 
                                       [trx_memb_total_onestep] decimal(18,4), [trx_memb_total_onestep_percent_started] decimal(18,4), [trx_memb_total_onestep_percent_memberships] decimal(18,4), 
                                       [trx_memb_total_onestep_percent_touched] decimal(18,4))

    /*  Make sure the date range is one day from 00:00 to 23:59  */

        SELECT @report_start_dt = convert(char(10), @report_start_dt, 111) + ' 00:00:00'
        SELECT @report_end_dt = convert(char(10), @report_end_dt,111) + ' 00:00:00'


        SELECT @work_dt = @report_start_dt
        WHILE @work_dt <= @report_end_dt BEGIN


            INSERT INTO @temp_table ([run_dt], [history_dt], [history_date], [user_id], [user_name], [user_name_sort], [user_location], [orders_created], [orders_touched], [membership_sales], [total_one_step_recovery],
                                     [total_gift_memberships], [basic_2_sales], [basic_5_sales], [basic_8_sales], [basic_total_sales], [basic_one_step_recovery], [basic_gift_memberships], [one_step_basic], [premier_2_sales],
                                     [premier_5_sales], [premier_8_sales], [premier_total_sales], [premier_one_step_recovery], [premier_gift_memberships], [one_step_premier], [one_step_total], [orders_counted],
                                     [multi_venue_orders], [total_venues_sold])
            EXECUTE dbo.LP_UPDATE_HISTORY_TRX_STATS @history_dt = @work_dt, @create_dt = @run_dt, @operator = @report_operator, @machine_locations = @machine_locations, @return_or_write = 'R'
        

            SELECT @work_dt = DATEADD(DAY,1,@work_dt)

        END

            
    /*  Seed the table  */

        INSERT INTO @trx_stat_table VALUES (@report_start_dt, @report_operator, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)

    /*  How many transactions created by this operator on this date  */

        UPDATE @trx_stat_table SET [trx_started] = (SELECT SUM([orders_created]) FROM @temp_table)

    /*  How many transactions created or adjusted by this operator on this date  */

        UPDATE @trx_stat_table SET [trx_touched] = (SELECT SUM(orders_touched) FROM @temp_table)

    /*  IF no transactions found, stop now  */

        IF not exists (SELECT * FROM @trx_stat_table WHERE [trx_started] > 0 or [trx_touched] > 0) GOTO FINISHED
    
    /*  Membership counts (Basic, Premier, and Total)  */

        UPDATE @trx_stat_table SET [trx_memb_basic] = (SELECT SUM(basic_total_sales) FROM @temp_table)
        UPDATE @trx_stat_table SET [trx_memb_premier] = (SELECT SUM(premier_total_sales) FROM @temp_table)
        UPDATE @trx_stat_table SET [trx_memb_total] = ([trx_memb_basic] + [trx_memb_premier])

    /*  Percentage of sales (both started and touched) that are membership sales (Basic, Premier, and Total)  */

        UPDATE @trx_stat_table SET [trx_memb_basic_percent_started] = ([trx_memb_basic] / [trx_memb_total]) WHERE [trx_memb_total] > 0
        UPDATE @trx_stat_table SET [trx_memb_basic_percent_touched] = ([trx_memb_basic] / [trx_memb_total]) WHERE [trx_memb_total] > 0
    
        UPDATE @trx_stat_table SET [trx_memb_premier_percent_started] = ([trx_memb_premier] / [trx_memb_total]) WHERE [trx_memb_total] > 0
        UPDATE @trx_stat_table SET [trx_memb_premier_percent_touched] = ([trx_memb_premier] / [trx_memb_total]) WHERE [trx_memb_total] > 0

            
        UPDATE @trx_stat_table SET [trx_memb_total_percent_started] = ([trx_memb_total] / [trx_started]) WHERE [trx_started] > 0
        UPDATE @trx_stat_table SET [trx_memb_total_percent_touched] = ([trx_memb_total] / [trx_touched]) WHERE [trx_touched] > 0

    /*  Number of Membership Sales that have been enrolled into the One-Step Program (Basic, Premier, and Total)  */

        UPDATE @trx_stat_table SET [trx_memb_basic_onestep] = (SELECT SUM(one_step_basic) FROM @temp_table)
        UPDATE @trx_stat_table SET [trx_memb_premier_onestep] = (SELECT SUM(one_step_premier) FROM @temp_table)
        UPDATE @trx_stat_table SET [trx_memb_total_onestep] = ([trx_memb_basic_onestep] + [trx_memb_premier_onestep])

    /* Percentage of Membership Transactions that were enrolled in the one-step program */

        DECLARE @onestep_eligible DECIMAL(18,4)

        SELECT @onestep_eligible = (SUM([basic_total_sales]) - SUM([basic_one_step_recovery]) - SUM([basic_gift_memberships])) FROM @temp_table
        SELECT @onestep_eligible = ISNULL(@onestep_eligible,0)

        IF @onestep_eligible > 0 UPDATE @trx_stat_table SET [trx_memb_basic_onestep_percent_Basic] = ([trx_memb_basic_onestep] / @onestep_eligible)

        --UPDATE @trx_stat_table SET [trx_memb_basic_onestep_percent_memberships] = ([trx_memb_basic_onestep] / [trx_memb_total]) WHERE [trx_memb_total] > 0

        SELECT @onestep_eligible = (SUM([premier_total_sales]) - SUM([premier_one_step_recovery]) - SUM([premier_gift_memberships])) FROM @temp_table
        SELECT @onestep_eligible = ISNULL(@onestep_eligible,0)

        IF @onestep_eligible > 0 UPDATE @trx_stat_table SET [trx_memb_premier_onestep_percent_premier] = ([trx_memb_premier_onestep] / @onestep_eligible)

        --UPDATE @trx_stat_table SET [trx_memb_premier_onestep_percent_memberships] = ([trx_memb_premier_onestep] / [trx_memb_total]) WHERE [trx_memb_total] > 0

        SELECT @onestep_eligible = (SUM([membership_sales]) - SUM([total_one_step_recovery]) - SUM([total_gift_memberships])) FROM @temp_table
        SELECT @onestep_eligible = ISNULL(@onestep_eligible,0)

        IF @onestep_eligible > 0 UPDATE @trx_stat_table SET [trx_memb_total_onestep_percent_memberships] = ([trx_memb_total_onestep] / @onestep_eligible)

    /* Percentage of Membership transactions (created and touched) that were enrolled into the one-step program  */

        UPDATE @trx_stat_table SET [trx_memb_basic_onestep_percent_started] = ([trx_memb_basic_onestep] / [trx_started]) WHERE [trx_started] > 0
        UPDATE @trx_stat_table SET [trx_memb_basic_onestep_percent_touched] = ([trx_memb_basic_onestep] / [trx_touched]) WHERE [trx_touched] > 0

        UPDATE @trx_stat_table SET [trx_memb_premier_onestep_percent_started] = ([trx_memb_premier_onestep] / [trx_started]) WHERE [trx_started] > 0
        UPDATE @trx_stat_table SET [trx_memb_premier_onestep_percent_touched] = ([trx_memb_premier_onestep] / [trx_touched]) WHERE [trx_touched] > 0

        UPDATE @trx_stat_table SET [trx_memb_total_onestep_percent_started] = ([trx_memb_total_onestep] / [trx_started]) WHERE [trx_started] > 0
        UPDATE @trx_stat_table SET [trx_memb_total_onestep_percent_touched] = ([trx_memb_total_onestep] / [trx_touched]) WHERE [trx_touched] > 0
   
    FINISHED:

    /*  If no transactions found, delete the single record from the temp table 
        This will cause the Trx Stats section of the report to be suppressed  */
    
        IF not exists(SELECT * FROM @trx_stat_table WHERE [trx_touched] > 0) DELETE FROM @trx_stat_table
    
    /*  Select the final data set from the temp table  */

        SELECT [rpt_date], [rpt_operator], [trx_started], [trx_touched], [trx_memb_basic], [trx_memb_premier], [trx_memb_total], [trx_memb_basic_percent_started], 
               [trx_memb_basic_percent_touched], [trx_memb_premier_percent_started], [trx_memb_premier_percent_touched], [trx_memb_total_percent_started], 
               [trx_memb_total_percent_touched], [trx_memb_basic_onestep], [trx_memb_basic_onestep_percent_started], [trx_memb_basic_onestep_percent_touched], 
               [trx_memb_basic_onestep_percent_Basic], [trx_memb_basic_onestep_percent_Memberships], [trx_memb_premier_onestep], [trx_memb_premier_onestep_percent_started], 
               [trx_memb_premier_onestep_percent_touched], [trx_memb_premier_onestep_percent_premier], [trx_memb_premier_onestep_percent_Memberships], [trx_memb_total_onestep], 
               [trx_memb_total_onestep_percent_started], [trx_memb_total_onestep_percent_touched], [trx_memb_total_onestep_percent_memberships]
        FROM @trx_stat_table

END
GO

GRANT EXECUTE ON [dbo].[LRP_TESSITURA_CASHOUT_TRX_STATS] TO impusers
GO

--EXECUTE [dbo].[LRP_TESSITURA_CASHOUT_TRX_STATS] @report_start_dt = '11-11-2017', @report_end_dt = '11-11-2017', @report_operator = 'mharri00', @machine_locations = '15'

--SELECT [user_id], sum(membership_sales), SUM(onestep_total) FROM dbo.LT_HISTORY_TRX_STATS WHERE history_date = '2017/11/11' AND user_location = 'Box Office' GROUP BY [user_id]
--EXECUTE dbo.LP_UPDATE_HISTORY_TRX_STATS @history_dt = '11-11-2017', @create_dt = Null, @operator = Null, @machine_locations = Null, @return_or_write = 'R'

--EXECUTE [dbo].[LRP_TESSITURA_CASHOUT_TRX_STATS] @report_start_dt = '12-1-2016', @report_end_dt = '12-1-2016', @report_operator = 'mdalto00', @machine_locations = '15,16'
--SELECT count(DISTINCT [order_no]) FROM T_SUB_LINEITEM 
--WHERE ([create_dt] between '12-1-2016' AND '12-1-2016 23:59:59' AND [created_by] = 'mdalto00') or
--      ([last_update_dt] between '12-1-2016' AND '12-1-2016 23:59:59' and [last_updated_by] = 'mdalto00')




