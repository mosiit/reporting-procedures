USE [impresario]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

DROP PROCEDURE [dbo].[LRP_EXCEPTION_COA_TX_CAMP_FUND]
GO

-- CHART OF ACCOUNTS Exception reporting on TX_CAMP_FUND
-- DSJ 6/16/2016
-- MES 01/20/2021 REMOVED per SCTASK0001778
--                This exception is a part of the Campaign exception now, 
--                so this separate procedure is not longer necessary
ALTER PROCEDURE [dbo].[LRP_EXCEPTION_COA_TX_CAMP_FUND]
AS BEGIN

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
    SET NOCOUNT ON

    SELECT  cf.fund_no, 
            '', 
            COUNT(distinct cc.description) AS cnt,
            'More than one campaign with the same category associated with this fund' AS reason 
    FROM TX_CAMP_FUND AS cf
         INNER JOIN T_CAMPAIGN AS c ON cf.campaign_no = c.campaign_no
         INNER JOIN TR_CAMPAIGN_CATEGORY AS cc ON c.category = cc.id
    GROUP BY cf.fund_no
    HAVING COUNT(DISTINCT cc.description)> 1

END

--EXECUTE [dbo].[LRP_EXCEPTION_COA_TX_CAMP_FUND]