USE impresario
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_ADV_PROSPECT_MANAGER_PLAN_SUMMARY]') AND type IN (N'P', N'PC'))
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_ADV_PROSPECT_MANAGER_PLAN_SUMMARY] AS' 
GO

EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LRP_ADV_PROSPECT_MANAGER_PLAN_SUMMARY] TO [impusers], [tessitura_app]'
GO

ALTER PROCEDURE [dbo].[LRP_ADV_PROSPECT_MANAGER_PLAN_SUMMARY]
        @fiscal_year INT = 2021,
        @workers_str VARCHAR(4000) = '672463'
AS BEGIN

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;
    
    /* Procedure Variables  */

        --Determine current fiscal year, fiscal start date, fiscal end date, and current date
        DECLARE @fiscal_start_dt DATETIME = NULL, @fiscal_end_dt DATETIME = NULL;
        DECLARE @current_dt DATETIME = GETDATE();

        DECLARE @worker_list TABLE ([worker_no] INT NOT NULL DEFAULT (0));

        IF OBJECT_ID('tempdb..#plan_data') IS NOT NULL DROP TABLE [#plan_data];

        CREATE TABLE [#plan_data] ([plan_no] INT NOT NULL DEFAULT (0),
                                   [pl_customer_no] INT NOT NULL DEFAULT (0),
                                   [pm_customer_no] INT NOT NULL DEFAULT (0),
                                   [pm_name] VARCHAR(80) NOT NULL DEFAULT (''),
                                   [wk_customer_no] INT NOT NULL DEFAULT (0),
                                   [wk_name] VARCHAR(80) NOT NULL DEFAULT (''),
                                   [start_dt] DATETIME NULL,
                                   [complete_by_dt] DATETIME NULL,
                                   [campaign_no] INT NOT NULL DEFAULT (0),
                                   [campaign_description] VARCHAR(50) NOT NULL DEFAULT (''),
                                   [ask_amount] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                   [goal_amount] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                   [plan_amount] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                   [contribution_amount] DECIMAL (18,2) NOT NULL DEFAULT (0.0),
                                   [plan_status_id] INT NOT NULL DEFAULT (0),
                                   [plan_status] VARCHAR(50) NOT NULL DEFAULT (''),
                                   [pmn_counter] INT NOT NULL DEFAULT (0),
                                   [wrk_counter] INT NOT NULL DEFAULT (0),
                                   [tot_counter] INT NOT NULL DEFAULT (0));

    /* Check Parameters  */

        IF ISNULL(@fiscal_year, 0) <= 0 SELECT @fiscal_year = [dbo].[LF_GetFiscalYear](@current_dt)

        SELECT @fiscal_start_dt = [dbo].[LF_GetFiscalYearStartDt](@fiscal_year),
               @fiscal_end_dt = [dbo].[LF_GetFiscalYearEndDt](@fiscal_year)
       
        IF ISNULL(@workers_str,'') = ''
            INSERT INTO @worker_list ([worker_no])
            SELECT ISNULL([worker_customer_no], 0)
            FROM [dbo].[LV_CAMPAIGN_ACTIVITY_WORKERS] 
            WHERE [inactive] = 'N'
              AND ISNULL([worker_customer_no], 0) > 0;
        ELSE
            INSERT INTO @worker_list ([worker_no])
            SELECT ISNULL(TRY_CAST([Element] AS INTEGER),0) 
            FROM [dbo].[FT_SPLIT_LIST](REPLACE(@workers_str,'"',''),',')
            WHERE ISNULL(TRY_CAST([Element] AS INTEGER),0) > 0;

    /*  Get plans where worker is Prospect Manager  */

        WITH [CTE_PROSPECT_MANAGERS] ([pl_customer_no], [pm_customer_no], [pm_name])
        AS (SELECT DISTINCT  kwp.[customer_no],
                             kwc.[customer_no], 
                            (kwc.[lname] + ' ' + kwc.[fname])
            FROM [dbo].[TX_CUST_KEYWORD] AS kwp
                 LEFT OUTER JOIN [dbo].[T_CUSTOMER] AS kwc ON (kwc.[lname] + ' ' + kwc.[fname]) = kwp.[key_value]
                 INNER JOIN @worker_list AS wrk ON wrk.[worker_no] = kwc.[customer_no]
            WHERE kwp.[keyword_no] = 555)
        INSERT INTO [#plan_data] ([plan_no], [pl_customer_no], [pm_customer_no], [pm_name], [wk_customer_no], [wk_name], [start_dt],[complete_by_dt],[campaign_no],
                                  [campaign_description],[ask_amount],[goal_amount],[contribution_amount],[plan_status_id],[plan_status],[pmn_counter],[wrk_counter],[tot_counter])
        SELECT pln.[plan_no],
               pln.[customer_no],
               pmn.[pm_customer_no],
               pmc.[lname] + ' ' + pmc.[fname],
               ISNULL(xpp.[customer_no], 0) AS [wk_customer_no],
               wkc.[lname] + ' ' + pmc.[fname],
               pln.[start_dt], 
               pln.[complete_by_dt],
               pln.[campaign_no],
               cmp.[description],
               ISNULL(pln.[ask_amt], 0.0),
               isnull(pln.[goal_amt], 0.0),
               ISNULL(pln.[cont_amt],0.0),
               pln.[status],
               sta.[description],
               1,
               0,
               1 
        FROM [dbo].[T_PLAN] AS pln
             INNER JOIN [CTE_PROSPECT_MANAGERS] AS pmn ON pmn.[pl_customer_no] = pln.[customer_no]
             LEFT OUTER JOIN [dbo].[T_CAMPAIGN] AS cmp ON cmp.[campaign_no] = pln.[campaign_no]
             LEFT OUTER JOIN [dbo].[TR_PLAN_STATUS] AS sta ON sta.[id] = pln.[status]
             LEFT OUTER JOIN [dbo].[TX_CUST_PLAN] AS xpp ON xpp.[plan_no] = pln.[plan_no] AND xpp.[role_no] = 1
             LEFT OUTER JOIN [dbo].[T_CUSTOMER] AS pmc ON pmc.[customer_no] = pmn.[pm_customer_no]
             LEFT OUTER JOIN [dbo].[T_CUSTOMER] AS wkc ON wkc.[customer_no] = ISNULL(xpp.[customer_no], 0)
        WHERE pln.[complete_by_dt] >= @fiscal_start_dt;
         
    /*  Get plans where worker is the primary worker (excluding plans where they are prospect manager  */

        WITH [CTE_PROSPECT_MANAGERS] ([pl_customer_no], [pm_customer_no], [pm_name])
        AS (SELECT DISTINCT  kwp.[customer_no],
                             kwc.[customer_no], 
                            (kwc.[lname] + ' ' + kwc.[fname])
            FROM [dbo].[TX_CUST_KEYWORD] AS kwp
                 LEFT OUTER JOIN [dbo].[T_CUSTOMER] AS kwc ON (kwc.[lname] + ' ' + kwc.[fname]) = kwp.[key_value]
                 --INNER JOIN @worker_list AS wrk ON wrk.[worker_no] = kwc.[customer_no]
            WHERE kwp.[keyword_no] = 555)
        INSERT INTO [#plan_data] ([plan_no], [pl_customer_no], [pm_customer_no], [pm_name], [wk_customer_no], [wk_name], [start_dt],[complete_by_dt],[campaign_no],
                                  [campaign_description],[ask_amount],[goal_amount],[contribution_amount],[plan_status_id],[plan_status],[pmn_counter],[wrk_counter],[tot_counter])
        SELECT pln.[plan_no],
               pln.[customer_no],
               ISNULL(pmn.[pm_customer_no], 0),
               '',
               pln.[primary_worker_no],
               pln.[primary_worker],
               pln.[start_dt],
               pln.[complete_by_dt],
               pln.[campaign_no],
               pln.[campaign_desc],
               ISNULL(pln.[ask_amt], 0),
               ISNULL(pln.[goal_amt], 0),
               ISNULL(pln.[cont_amt], 0),
               pln.[status],
               pln.[status_desc],
               0,
               1,
               1
        FROM [dbo].[VS_PLAN_WITH_PRIMARY_WORKER] AS pln
             LEFT OUTER JOIN [CTE_PROSPECT_MANAGERS] AS pmn ON pmn.[pl_customer_no] = pln.[customer_no]
        WHERE pln.[complete_by_dt] >= @fiscal_start_dt
          AND pln.[type] = 7
          AND pln.[primary_worker_no] = @workers_str
          AND pln.[customer_no] NOT IN (SELECT [pl_customer_no] FROM [CTE_PROSPECT_MANAGERS] WHERE [pm_customer_no] = @workers_str)

    FINISHED: 
    
        /*  Final data set  */

            SELECT [plan_no],
                   [pl_customer_no],
                   [pm_customer_no],
                   [pm_name],
                   [wk_customer_no],
                   [wk_name],
                   [start_dt],
                   [complete_by_dt],
                   [campaign_no],
                   [campaign_description],
                   [ask_amount],
                   [goal_amount],
                   [plan_amount],
                   [contribution_amount],
                   [plan_status_id],
                   [plan_status],
                   [pmn_counter],
                   [wrk_counter],
                   [tot_counter]
          FROM [#plan_data]
          
            --WHERE [campaign_no] = 2396 --AND [plan_status_id] = 35

    DONE:

        IF OBJECT_ID('tempdb..#plan_data') IS NOT NULL DROP TABLE [#plan_data];

END
GO


EXECUTE [dbo].[LRP_ADV_PROSPECT_MANAGER_PLAN_SUMMARY] @fiscal_year = 2021, @workers_str = '672463'






        --SELECT pln.[plan_no],
        --       pln.[customer_no],
        --       ISNULL(pmn.[pm_customer_no], 0),
        --       ISNULL(pmc.[lname] + ' ' + pmc.[fname], ''),
        --       ISNULL(xpp.[customer_no], 0) AS [wk_customer_no],
        --       wkc.[lname] + ' ' + wkc.[fname],
        --       pln.[start_dt], 
        --       pln.[complete_by_dt],
        --       pln.[campaign_no],
        --       cmp.[description],
        --       ISNULL(pln.[ask_amt], 0.0),
        --       isnull(pln.[goal_amt], 0.0),
        --       ISNULL(pln.[cont_amt],0.0),
        --       pln.[status],
        --       sta.[description],
        --       0,
        --       1,
        --       1
        --FROM [dbo].[T_PLAN] AS pln
        --     LEFT OUTER JOIN [CTE_PROSPECT_MANAGERS] AS pmn ON pmn.[pl_customer_no] = pln.[customer_no]
        --     LEFT OUTER JOIN [dbo].[T_CAMPAIGN] AS cmp ON cmp.[campaign_no] = pln.[campaign_no]
        --     LEFT OUTER JOIN [dbo].[TR_PLAN_STATUS] AS sta ON sta.[id] = pln.[status]
        --     LEFT OUTER JOIN [dbo].[TX_CUST_PLAN] AS xpp ON xpp.[plan_no] = pln.[plan_no] AND xpp.[role_no] = 1
        --     LEFT OUTER JOIN [dbo].[T_CUSTOMER] AS pmc ON pmc.[customer_no] = pmn.[pm_customer_no]
        --     LEFT OUTER JOIN [dbo].[T_CUSTOMER] AS wkc ON wkc.[customer_no] = xpp.[customer_no]
        --WHERE pln.[complete_by_dt] >= @fiscal_start_dt
        --  AND xpp.[customer_no] IN (SELECT worker_no FROM @worker_list)
        --  AND NOT EXISTS (SELECT plan_no FROM [#plan_data] WHERE [#plan_data].[plan_no] = pln.[plan_no])
        








--SELECT pln.[start_dt], 
--       pln.[complete_by_dt],
--       pln.[customer_no] AS [pl_customer_no],
--       xpp.[customer_no] AS [wo_customer_no],
--       pmc.[customer_no] AS [pm_customer_no],
--       pmk.[key_value] AS [primary_worker],
--       (pmc.[lname] + ' ' + pmc.[fname])  AS [prospect_manager]
--FROM [dbo].[T_PLAN] AS pln








--SELECT pln.[start_dt], 
--       pln.[complete_by_dt],
--       pln.[customer_no] AS [pl_customer_no],
--       xpp.[customer_no] AS [wo_customer_no],
--       pmc.[customer_no] AS [pm_customer_no],
--       pmk.[key_value] AS [primary_worker],
--       (pmc.[lname] + ' ' + pmc.[fname])  AS [prospect_manager]
--FROM [dbo].[T_PLAN] AS pln
--     LEFT OUTER JOIN [dbo].[TX_CUST_PLAN] AS xpp ON xpp.[plan_no] = pln.[plan_no] AND xpp.[role_no] = 1
--     LEFT OUTER JOIN [dbo].[T_CUSTOMER] AS pmc ON pmc.[customer_no] = xpp.[customer_no]
--     LEFT OUTER JOIN [dbo].[TX_CUST_KEYWORD] AS pmk ON pmk.[customer_no] = pln.[customer_no] AND [keyword_no] = 555
--WHERE ISNULL(pln.[complete_by_dt], pln.[start_dt]) >= '7-1-2020'
--  AND (xpp.[customer_no] = 672463 OR pmc.[customer_no] = 672463);






--SELECT * FROM [dbo].[T_CUSTOMER] WHERE customer_no = 672463