USE [impresario]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[LRP_ADV_PORTFOLIO_STAGE_MOVEMENT]
(
	@worker_customer_no INT = NULL,
	@start_dt DATETIME,
	@end_dt DATETIME  
)
AS

SET NOCOUNT ON 
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

DECLARE @stgMvmnt  TABLE 
(
	step_no INT NOT NULL,
	plan_no INT NOT NULL,
	step_dt DATETIME NOT NULL,
	step_status VARCHAR(30) NULL,
	worker_customer_no INT NOT NULL,
	primaryWorker VARCHAR(160) NULL
) 

INSERT INTO @stgMvmnt
(
    step_no,
    plan_no,
    step_dt,
    step_status,
    worker_customer_no,
    primaryWorker
)
SELECT st.step_no, 
	st.plan_no,
	st.step_dt,
	psnew.description AS step_status, 
	cp.customer_no AS worker_customer_no,
	wrk.display_name as primaryWorker
FROM dbo.T_STEP st
INNER JOIN TX_CUST_PLAN cp
	ON cp.plan_no = st.plan_no
	AND cp.primary_ind = 'Y'
INNER JOIN dbo.FT_CONSTITUENT_DISPLAY_NAME() wrk 
	ON cp.customer_no = wrk.customer_no
LEFT OUTER JOIN [dbo].TR_PLAN_STATUS psold 
	ON st.old_value = psold.id 
LEFT OUTER JOIN [dbo].TR_PLAN_STATUS psnew 
	ON st.new_value = psnew.id 
WHERE st.step_type = -1 
AND st.step_dt BETWEEN @start_dt AND @end_dt
AND cp.customer_no = @worker_customer_no


SELECT 
	step_no,
	plan_no,
	step_dt,
	step_status,
	worker_customer_no,
	primaryWorker
FROM @stgMvmnt