USE impresario;
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_ADV_STW_PROFILE_SUB_GIVING_SOCIETIES]') AND type in (N'P', N'PC'))
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_ADV_STW_PROFILE_SUB_GIVING_SOCIETIES] AS';
GO

EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LRP_ADV_STW_PROFILE_SUB_GIVING_SOCIETIES] TO [impusers], [tessitura_app]';
GO

ALTER PROCEDURE [dbo].[LRP_ADV_STW_PROFILE_SUB_GIVING_SOCIETIES]
        @customer_no INT = NULL
AS BEGIN

    SET NOCOUNT ON;
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

    /*  Procedure Variables  */

        DECLARE @giving_society_memberships TABLE ([society_sort] CHAR(3) NOT NULL DEFAULT (''),
                                                   [society_type] VARCHAR(100) NOT NULL DEFAULT (''),
                                                   [customer_no] INT NOT NULL DEFAULT (0),
                                                   [memb_level_name] VARCHAR(50) NOT NULL DEFAULT (''),
                                                   [expr_dt] DATETIME NULL,
                                                   [current_status_name] VARCHAR(30) NOT NULL DEFAULT (''))

    /*  Givig Societ Memberships  */

        INSERT INTO @giving_society_memberships ([society_sort],[society_type],[customer_no],[memb_level_name],[expr_dt],[current_status_name])
        SELECT TOP (1) 'AAA',
                       'Giving Society Memberships',
                       [customer_no],
                       [memb_level_name],
                       [expr_dt],
                       [current_status_name]
        FROM (SELECT TOP (1) mem.[cust_memb_no],
                             mem.[customer_no],
                             mem.[memb_org_no],
                             mem.[memb_level],
                             lev.[description] AS [memb_level_name],
                             mem.[init_dt],
                             mem.[expr_dt],
                             CASE WHEN mem.[current_status] IN (1, 9) THEN 0
                                  ELSE mem.[current_status] END AS [current_status],
                             sta.[description] AS [current_status_name]
              FROM [dbo].[TX_CUST_MEMBERSHIP] AS mem
                   LEFT OUTER JOIN [dbo].[T_MEMB_LEVEL] AS lev ON lev.[memb_level] = mem.[memb_level]
                   LEFT OUTER JOIN [dbo].[TR_CURRENT_STATUS] AS sta ON sta.[id] = mem.[current_status]
              WHERE customer_no = @customer_no
                AND mem.[memb_org_no] IN (5, 24)
                AND mem.[current_status] IN (2, 3)
              UNION SELECT TOP (1) mem.[cust_memb_no],
                                   mem.[customer_no],
                                   mem.[memb_org_no],
                                   mem.[memb_level],
                                   lev.[description] AS [memb_level_name],
                                   mem.[init_dt],
                                   mem.[expr_dt],
                                   CASE WHEN mem.[current_status] IN (1, 9) THEN 0
                                        ELSE mem.[current_status] END AS [current_status],
                                   sta.[description] AS [current_status_name]
              FROM [dbo].[TX_CUST_MEMBERSHIP] AS mem
                   LEFT OUTER JOIN [dbo].[T_MEMB_LEVEL] AS lev ON lev.[memb_level] = mem.[memb_level]
                   LEFT OUTER JOIN [dbo].[TR_CURRENT_STATUS] AS sta ON sta.[id] = mem.[current_status]
              WHERE customer_no = @customer_no
                AND mem.[memb_org_no] IN (5, 24, 26)
                AND mem.[current_status] IN (1,9)) AS t
        ORDER BY [current_status] DESC,
                 [expr_dt] DESC

    /*  Innovator Membership  */

        INSERT INTO @giving_society_memberships ([society_sort],[society_type],[customer_no],[memb_level_name],[expr_dt],[current_status_name])
        SELECT TOP (1) 'BBB',
                        'Innovator Membership',
                        [customer_no],
                        CASE WHEN [customer_no] > 0 THEN 'Innovator' 
                             ELSE ' ' END AS [innovator], 
                        CAST([end_dt] AS DATE) AS [end_dt],
                        CASE WHEN CAST([end_dt] AS DATE) < GETDATE() THEN 'Inactive'
                             ELSE 'Active' END AS [current_status_name]
        FROM [dbo].[TX_CONST_CUST]
        WHERE [customer_no] = @customer_no
          AND [constituency] = 11 
          AND CAST ([end_dt] AS DATE) > CAST(getdate() AS DATE)

    /*  Giving Societies  */

        INSERT INTO @giving_society_memberships ([society_sort],[society_type],[customer_no],[memb_level_name],[expr_dt],[current_status_name])        
        SELECT CASE WHEN adt.[type_no] = 5 THEN 'CCC'
                    WHEN adt.[type_no] = 25 THEN 'DDD'
                    WHEN adt.[type_no] = 6 THEN 'EEE'
                    ELSE 'FFF' END,
               atp.[description],
               adt.[customer_no],
               CASE WHEN ISNULL(ds3.[description],'') = '' THEN atp.[description]
                    WHEN ds3.[description] = '(none)' THEN atp.[description]
                    ELSE ds3.[description] END,
               adt.[stop_dt],
               ds1.[description]
        FROM [dbo].[LT_ATTRIBUTE_DETAIL] AS adt
             INNER JOIN [dbo].[LTR_ATTDET_TYPE] AS atp ON atp.[id] = adt.[type_no]
             INNER JOIN [dbo].[LTR_ATTDET_DESCR1] AS ds1 ON ds1.[id] = adt.[descr1_no]
             INNER JOIN [dbo].[LTR_ATTDET_DESCR3] AS ds3 ON ds3.[id] = adt.[descr3_no]
        WHERE [customer_no] = @customer_no
          AND [type_no] IN (5, 25, 6)
          AND [descr1_no] = 2
  
    FINISHED:

        /*  Final Data Selection For Report  */

            SELECT [society_sort],
                   [society_type],
                   [customer_no],
                   [memb_level_name],
                   [expr_dt],
                   LTRIM(RTRIM([current_status_name])) AS [current_status_name]
            FROM @giving_society_memberships 
            ORDER BY [society_sort], [society_type]

END
GO
