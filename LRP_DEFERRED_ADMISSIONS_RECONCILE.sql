
USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_DEFERRED_ADMISSIONS_RECONCILE]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_DEFERRED_ADMISSIONS_RECONCILE] AS' 
END
GO    

ALTER PROCEDURE [dbo].[LRP_DEFERRED_ADMISSIONS_RECONCILE]
            @month_no INT = 0,
            @year_no INT = 0,
            @date_field VARCHAR(25) = 'Payment Date',     --Can be: Payment Date or Batch Post Date
            @title_str VARCHAR(4000) = '',
            @display_gl_number CHAR(1) = 'N'
AS BEGIN

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    SET NOCOUNT ON;

    /*  Procedure Variables  */

        DECLARE @report_start_dt DATETIME, @report_end_dt DATETIME

        DECLARE @title_list TABLE ([title_no] INT)


    /*  Check Parameters  */

        SELECT @month_no = ISNULL(@month_no,DATEPART(MONTH,GETDATE()))
        SELECT @year_no = ISNULL(@year_no,DATEPART(YEAR,GETDATE()))

        SELECT @title_str = REPLACE(ISNULL(@title_str,''),'"','')
        
        IF @title_str <> ''
            INSERT INTO @title_list ([title_no])
            SELECT CONVERT(INT,Element) FROM dbo.FT_SPLIT_LIST(REPLACE(@title_str,'"',''),',')
        ELSE 
            INSERT INTO @title_list ([title_no])
            SELECT inv_no FROM [dbo].[T_INVENTORY] WHERE [type] = 'T' AND [description] <> 'Membership'

    /*  Set start and end date  */

        SELECT @report_start_dt =  DATEFROMPARTS(@year_no,@month_no,1) --First Day of the month

        SELECT @report_end_dt = DATEADD(SECOND,-1,DATEADD(MONTH,1,@report_start_dt))  --Add a month to then subtract a day from @report_start_dt

    /*  Temporary Table  */

        IF OBJECT_ID('tempdb..#deferred_final_table') IS NOT NULL DROP TABLE [#deferred_final_table]

        CREATE TABLE #deferred_final_table ([payment_no] int, [sequence_no] int, [payment_dt] datetime, [payment_date] char(10), [posted_dt] DATETIME, [posted_date] CHAR(10), 
                                            [performance_no] int, [performance_dt] datetime, [performance_date] char(10), [performance_code] VARCHAR(10), [title_name] varchar(30), 
                                            [production_name] VARCHAR(30), [production_name_long] varchar(255), [payment_type_name] varchar(30), [payment_method_name] varchar(30), 
                                            [payment_amt] decimal(18,2), [order_no] int, [check_no] varchar(30), [check_name] varchar(100), [batch_no] int, [customer_no] int, 
                                            [posted_status] char(1), [tckt_tran_flag] char(1), [gl_account] varchar(30), [gl_description] varchar(30), [pmt_notes] varchar(750), 
                                            [customer_name] VARCHAR(160), [same_month_payments] MONEY, [previous_month_payments] MONEY, [total_payments] MONEY, [report_msg] varchar(100))

        IF NOT EXISTS (SELECT * FROM tempdb.sys.indexes WHERE name = N'uq_deferred_reconcile_payment_no_sequence_no')
            CREATE UNIQUE NONCLUSTERED INDEX [uq_deferred_reconcile_payment_no_sequence_no] ON [#deferred_final_table] ([payment_no] ASC, sequence_no ASC)


    /*  Pull all performances for the month  */

        INSERT INTO [#deferred_final_table]
        SELECT pay.[payment_no], 
               pay.[sequence_no], 
               pay.[pmt_dt], 
               pay.[pmt_date],
               pay.[posted_dt],
               pay.[posted_date],
               pay.[perf_no], 
               pay.[perf_dt], 
               pay.[perf_date], 
               pay.[perf_code],
               pay.[title_name], 
               pay.[production_name], 
               pay.[production_name_long], 
               pay.[pmt_type_name], 
               pay.[pmt_method_name], 
               pay.[pmt_amt], 
               pay.[order_no], 
               pay.[check_no], 
               pay.[check_name], 
               pay.[batch_no], 
               pay.[customer_no], 
               pay.[posted_status], 
               pay.[tckt_tran_flag], 
               pay.[default_gl_account], 
               pay.[default_gl_description], 
               pay.[pmt_notes],
               nam.display_name,
               0.0,
               0.0,
               0.0,
               ''
        FROM [dbo].[LV_PERFORMANCE_PAYMENT_INFO] AS pay
              LEFT OUTER JOIN [dbo].[FT_CONSTITUENT_DISPLAY_NAME]() AS nam ON nam.[customer_no] = pay.[customer_no]
        WHERE pay.[perf_dt] between @report_start_dt AND @report_end_dt
          AND pay.[title_no] IN (SELECT title_no FROM @title_list)

    /*  Set payment amounts for same (or later) vs previous months */

        UPDATE [#deferred_final_table]
        SET [same_month_payments] = [payment_amt] 
        WHERE (@date_field = 'Batch Posted Date' AND DATEDIFF(MONTH,performance_dt,posted_dt) >= 0)
          OR  (@date_field <> 'Batch Posted Date' AND DATEDIFF(MONTH,performance_dt,payment_dt) >= 0)

        UPDATE [#deferred_final_table]
        SET [previous_month_payments] = [payment_amt] 
        WHERE (@date_field = 'Batch Posted Date' AND DATEDIFF(MONTH,performance_dt,posted_dt) < 0)
          OR  (@date_field <> 'Batch Posted Date' AND DATEDIFF(MONTH,performance_dt,payment_dt) < 0)

        UPDATE [#deferred_final_table]
        SET [total_payments] = [same_month_payments] + [previous_month_payments]

    /*  If displaying GL number instead of show title, change title to gl */

        IF @display_gl_number = 'Y' BEGIN

            UPDATE [#deferred_final_table] 
            SET [gl_account] = [dbo].[LFS_GET_PERFORMANCE_GL_ACCOUNT] ([performance_no], 0)
            WHERE [gl_account] = 'Unknown Account'
            
            UPDATE [#deferred_final_table]
            SET [production_name] = CASE WHEN ISNULL([gl_account],'') = '' THEN 'Unknown Account' ELSE [gl_account] END

        END
        
    FINISHED:

            SELECT [performance_no] AS 'perf_no',
                   [performance_code] AS 'perf_code', 
                   [performance_dt] AS 'perf_dt', 
                   [title_name], 
                   [production_name], 
                   SUM([same_month_payments]) AS 'same_month_payments', 
                   SUM([previous_month_payments]) AS 'previous_month_payments', 
                   SUM([total_payments]) AS 'total_payments',
                   [report_msg]
            FROM [#deferred_final_table]
            GROUP BY [performance_no], [performance_code], [performance_dt],  [title_name], [production_name], [report_msg]

    DONE:

        /*  Destroy Temp Table  */

            IF OBJECT_ID('tempdb..#deferred_final_table') IS NOT NULL DROP TABLE [#deferred_final_table]
       
END
GO

GRANT EXECUTE ON [dbo].[LRP_DEFERRED_ADMISSIONS_RECONCILE] TO [ImpUsers]
GO

EXECUTE [dbo].[LRP_DEFERRED_ADMISSIONS_RECONCILE] 6, 2019, 'Payment Date', '', 'Production and Date'
GO


--SELECT * FROM dbo.T_INVENTORY WHERE description IN ('Traveling Programs','Overnight Programs') AND type = 'T'
  

