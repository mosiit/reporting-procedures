USE [impresario]
GO

IF  EXISTS (SELECT * FROM [sys].[objects] WHERE [object_id] = OBJECT_ID(N'[dbo].[LP_UPDATE_HISTORY_TRX_STATS]') AND [type] in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LP_UPDATE_HISTORY_TRX_STATS]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/* 
        LP_UPDATE_HISTORY_TRX_STATS             

        Written by: Mark Sherwood June/July, 2017
        
        Procedure to update the LT_HISTORY_TRX_STATS data mart that keeps track of operators and their membership sales and one step percentages
        as well as their add on orders and average venues per order.  This procedure is run each early morning as part of the ticket history
        update and processes a single day (the previous day).

            Parameters: @history_dt = the date to be processed.  If nothing passed to this parameter, procedure defaults to yesterday
                        @create_dt = Usually null, which defaults to current date and time.  If running manually for more than one date
                                     and you want the run date/time to be the same on all dates prcocessed, pass the date and time to
                                     the procedure in this variable.
                        @operator = Usually Null or blank, which means run for all operators.  If need to run for a single operator,
                                    pass the Tessitura user_id to the procedure.
                        @return_or_write = Added so that this procedure can be used for same day for a single operator to be used in the
                                           cashout report.  If 'W' procedure writes data to the data mart table.  If 'R' no data is written
                                           to that table.  Data is returned as a dataset for a report.  If 'B' then it both writes the 
                                           data to the data mart and returns the data as a dataset.
                                           NOTE: Will default to 'R' if @operator is not blank or null.
*/

CREATE PROCEDURE [dbo].[LP_UPDATE_HISTORY_TRX_STATS]
        @history_dt datetime = null,                 --If nothing passed, it will default to yesterday's date.
        @create_dt datetime = NULL,                 --When Calling for Multiple dates from another procedure - so that all dates have the same logged date and time
        @operator VARCHAR(10) = NULL,               --Procedure can be run for a single operator or for one operator (if run for cashout report)
        @machine_locations VARCHAR(4000) = NULL,    --Used only when called for the cashout report
        @return_or_write CHAR(1) = 'W'              --W = Write to data mart table / R = Return data from procedure (to a report) / B = Both
AS BEGIN

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

    SET ANSI_WARNINGS OFF

    /***************************************************************************************************************************************************/

    /*  Procedure Variables  */

        DECLARE @go_live_date CHAR(10) = '2016/05/18'
        DECLARE @add_on_grouping VARCHAR(20) = 'Performance'

        DECLARE @mos_buyout_no INT = (SELECT ISNULL([id],0) FROM [dbo].[TR_MOS] WHERE [description] = 'Buyouts')
        DECLARE @mos_citypass_no INT = (SELECT ISNULL([id],0) FROM [dbo].[TR_MOS] WHERE [description] = 'CityPASS')
        DECLARE @mos_consignment_no INT = (SELECT ISNULL([id],0) FROM [dbo].[TR_MOS] WHERE [description] = 'Consignment')
        DECLARE @mos_internal_no INT = (SELECT ISNULL([id],0) FROM [dbo].[TR_MOS] WHERE [description] = 'Internal Tickets and Event Add')
        DECLARE @mos_school_no INT = (SELECT ISNULL([id],0) FROM [dbo].[TR_MOS] WHERE [description] = 'Schools')
        DECLARE @mos_school_web_no INT = (SELECT ISNULL([id],0) FROM [dbo].[TR_MOS] WHERE [description] = 'Web Sales School')
        DECLARE @mos_thrilla_no INT = (SELECT ISNULL([id],0) FROM [dbo].[TR_MOS] WHERE [description] = 'Thrill Ride Capsule A')
        DECLARE @mos_thrillb_no INT = (SELECT ISNULL([id],0) FROM [dbo].[TR_MOS] WHERE [description] = 'Thrill Ride Capsule B')
        DECLARE @mos_tour_op_no INT = (SELECT ISNULL([id],0) FROM [dbo].[TR_MOS] WHERE [description] = 'Tour Operators')

        DECLARE @location_list TABLE ([location_no_str] VARCHAR(20), [location_no] INT)
        DECLARE @machine_list TABLE ([machine_name] VARCHAR(50))

        DECLARE @order_exclusions TABLE ([order_no] INT, [exclusion_type] VARCHAR(100))

        DECLARE @order_exclusions_memb TABLE ([order_no] INT, [operator] VARCHAR(10), [created_or_touched] CHAR(1), [membership_sale] VARCHAR(30), 
                                              [sale_to_member] CHAR(1), [total_performances] DECIMAL(18,4), [add_on_counter] DECIMAL(18,4))

        DECLARE @stats_table_mem TABLE ([data_pass] INT, [history_dt] DATETIME, [operator] VARCHAR(25), [orders_created] DECIMAL(18,2), 
                                        [orders_touched] DECIMAL(18,2), [membership_sales] DECIMAL(18,2), [basic_2_sales] DECIMAL(18,2),
                                        [basic_5_sales] DECIMAL(18,2), [basic_8_sales] DECIMAL(18,2), [basic_total_sales] DECIMAL(18,2), 
                                        [premier_2_sales] DECIMAL(18,2), [premier_5_sales] DECIMAL(18,2), [premier_8_sales] DECIMAL(18,2),
                                        [premier_total_sales] DECIMAL(18,2), [one_step_basic] DECIMAL(18,2),
                                        [one_step_premier] DECIMAL(18,2), [one_step_total] DECIMAL(18,2), [basic_one_step_recovery] DECIMAL(18,2), 
                                        [premier_one_step_recovery] DECIMAL(18,2), [total_one_step_recovery] DECIMAL(18,2),
                                        [basic_gift_memberships] DECIMAL(18,2), [premier_gift_memberships] DECIMAL(18,2), [total_gift_memberships] DECIMAL(18,2))

        DECLARE @stats_table_add TABLE ([data_pass] INT, [history_dt] DATETIME, [operator] VARCHAR(25), [orders_counted] DECIMAL(18,2), 
                                        [multi_venue_orders] DECIMAL(18,2), [total_venues_sold] DECIMAL(18,2))

        DECLARE @valid_titles TABLE ([title_name] VARCHAR(30))

 
    /*  Check Parameters  */

        SELECT @history_dt = ISNULL(@history_dt,DATEADD(DAY,-1,GETDATE()))
        SELECT @create_dt = ISNULL(@create_dt,GETDATE())

        SELECT @history_dt = CONVERT(DATE,@history_dt)

        SELECT @operator = ISNULL(@operator,'')

        SELECT @return_or_write = ISNULL(@return_or_write,'W')

        IF @operator <> '' SELECT @return_or_write = 'R'
        
        IF @return_or_write NOT IN ('R','B') SELECT @return_or_write = 'W'

        SELECT @machine_locations = ISNULL(@machine_locations,'')

        IF @machine_locations <> '' BEGIN

            INSERT INTO @location_list ([location_no_str]) 
            SELECT [item] FROM [dbo].[LFT_SPLIT_DILEMETED_STRING] (@machine_locations, ',')
            
            DELETE FROM @location_list WHERE [dbo].[FS_isReallyNumeric] ([location_no_str]) = 0

            UPDATE @location_list SET [location_no] = CONVERT(INT,[location_no_str])

            INSERT INTO @machine_list ([machine_name])
            SELECT [machine_name] FROM [dbo].[TX_MACHINE_LOCATION] WHERE [location] IN (SELECT [location_no] FROM @location_list)

        END
        
    /***************************************************************************************************************************************************/

    /*  Temporary Tables  */

        IF OBJECT_ID('tempdb..#order_table') IS NOT NULL DROP TABLE [#order_table]
    
        CREATE TABLE [#order_table] ([order_no] INT, [operator] VARCHAR(10), [create_loc] VARCHAR(50), [created_or_touched] CHAR(1), [membership_sale] VARCHAR(30), 
                                     [mos] INT, [sale_to_member] CHAR(1), [total_performances] DECIMAL(18,4), [add_on_counter] Decimal(18,4))

        CREATE CLUSTERED INDEX [ix_order_table_order_no] ON [#order_table] ([order_no] ASC) ON [PRIMARY]
        CREATE NONCLUSTERED INDEX [ix_order_table_create_loc] ON [#order_table] ([create_loc] ASC) ON [PRIMARY]

    
        IF OBJECT_ID('tempdb..#membership_performances') IS NOT NULL DROP TABLE [#membership_performances]
    
        CREATE TABLE [#membership_performances] ([title_name] VARCHAR(30), [Production_name] VARCHAR(30), [production_season_name] VARCHAR(30),
                                                 [performance_no] INT, [performance_code] VARCHAR(10), [zone_no] INT, [zone_name] VARCHAR(30),
                                                 [performance_date] CHAR(10))

        CREATE CLUSTERED INDEX [ix_membership_performances_performance_no_zone_no] ON [#membership_performances] ([performance_no] ASC, [zone_no] ASC) ON [PRIMARY]
        CREATE NONCLUSTERED INDEX [ix_membership_performances_zone_name] ON [#membership_performances] ([zone_name] ASC) ON [PRIMARY]
                                           
        IF OBJECT_ID('tempdb..#sli_cache') IS NOT NULL DROP TABLE [#sli_cache]

        CREATE TABLE [#sli_cache] ([sli_no] INT, [sli_status] INT, [order_no] INT, [customer_no] INT, [customer_name] VARCHAR(125), [is_member] CHAR(1), 
                                   [MOS] INT, [perf_no] INT, [zone_no] INT, [perf_dt] DATETIME, [perf_date] CHAR(10), [perf_time] VARCHAR(8), 
                                   [performance_type] VARCHAR(30), [title_name] VARCHAR(30), [production_name] VARCHAR(30), [price_type] INT, 
                                   [price_type_name] VARCHAR(30), [comp_code] INT, [comp_code_name] VARCHAR(30), [create_dt] DATETIME, 
                                   [created_by] VARCHAR(30), [last_update_dt] DATETIME, [last_updated_by] VARCHAR(30)) 

        CREATE CLUSTERED INDEX [ix_sli_cache_order_no] ON [#sli_cache] ([order_no] ASC) ON [PRIMARY]
        CREATE NONCLUSTERED INDEX [ix_sli_cache_sli_status] ON [#sli_cache] ([sli_status] ASC)  ON [PRIMARY]
        CREATE NONCLUSTERED INDEX [ix_sli_cache_perf_type]  ON [#sli_cache] ([performance_type] ASC) ON [PRIMARY]
        CREATE NONCLUSTERED INDEX [ix_sli_cache_price_type] ON [#sli_cache] ([price_type] ASC)  ON [PRIMARY]
        CREATE NONCLUSTERED INDEX [ix_sli_cache_price_type_name] ON [#sli_cache] ([price_type_name] ASC)  ON [PRIMARY]
        
                                      
    /***************************************************************************************************************************************************/

    /*  Cache the list of valid titles for add on stats
        To be counted in the add on stats, the order must contain at least one of these titles.  */

        INSERT INTO @valid_titles ([title_name])
        VALUES  ('4-D Theater'),('Adult Offerings'),('Butterfly Garden'),('CityPASS'),('Exhibit Halls'),('Events'),
                ('Hayden Planetarium'), ('Member Only Events'), ('Membership'),('Mugar Omni Theater'),('Special Exhibitions')

    /*  Cache all membership performances after go live for reference */

        INSERT INTO [#membership_performances]
        SELECT [title_name], [production_name], [production_season_name], [performance_no], [performance_code],
               [performance_zone], [performance_zone_text], [performance_date]
        FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE]
        WHERE [title_name] = 'Membership' AND [production_name] <> 'Library' AND [performance_date] > @go_live_date


    /***************************************************************************************************************************************************
    #order_table will keep a list of all orders that were created or touched on the history date being processed, along with the operator who created 
    or modified that order.
    ***************************************************************************************************************************************************/

    /*  First group or orders is flagged as CREATED and contains all the orders created by each operator on the history date being processed. This comes
        from the order table using create_dt and not order_dt because the order_dt can be changed by the user when processing a transaction. */

        INSERT INTO [#order_table] ([order_no], [operator], [create_loc], [created_or_touched], [membership_sale], [MOS], [sale_to_member], 
                                    [total_performances], [add_on_counter])
        SELECT [order_no], ISNULL([created_by],'Unknown'), [create_loc], 'C', '', [MOS], 'N', 0, 0
        FROM [dbo].[T_ORDER] 
        WHERE CONVERT(DATE,[create_dt]) = @history_dt

    /*  There are sometimes orders in the T_ORDER table that have nothing in it.  Since those are not actually orders, they are removed.  */

        DELETE FROM [#order_table] WHERE [order_no] NOT IN (SELECT [order_no] FROM [dbo].[T_LINEITEM])  

    /*  The second group of orders is flagged as ADDED and containes all the orders where an operator added
        something to the order on the history date being processed. This comes from the sublineitem table using
        create_dt.  Some of these orders will also be included in the Created group of orders and that's okay.
        Duplicates are OK in this table.  */
        
        INSERT INTO [#order_table] ([order_no], [operator], [create_loc], [created_or_touched], [membership_sale], [mos],
                                  [sale_to_member], [total_performances], [add_on_counter])
        SELECT DISTINCT sli.[order_no], ISNULL(sli.[created_by],'Unknown'), sli.[create_loc], 'A', '', ord.[mos], 'N', 0, 0
        FROM [dbo].[T_SUB_LINEITEM] AS sli (NOLOCK)
             LEFT OUTER JOIN [dbo].[T_ORDER] AS ord (NOLOCK) ON ord.[order_no] = sli.[order_no]
        WHERE CONVERT(DATE,sli.[create_dt]) = @history_dt-- AND sli.[order_no] NOT IN (SELECT [order_no] FROM [#order_table] (NOLOCK) WHERE [operator] = @operator) 

    /*  NOTE: There used to be a third insert statement that pulled based on last_update_dt, but this was pulling only transactions
              that were only printed (nothing added to the report and those orders should be excluded  */

    /*  If being run for a single operator, delete all the others  */

            IF @operator <> ''
                DELETE FROM [#order_table] WHERE [operator] <> @operator

    /*  If being run for specific machine locations, delete all the others */

            IF EXISTS (SELECT * FROM @machine_list)
                DELETE FROM [#order_table] WHERE [create_loc] NOT IN (SELECT [machine_name] FROM @machine_list)

    /***************************************************************************************************************************************************/

    /*  Cache all the subline item information for the orders in #order_table.
        NOTE: Original version of code did not cache and instead made separate calls back to the T_SUB_LINEITEM table
              Before caching, time to get through exclusions portion of script was almost three minutes.
              After caching, time to get through exlusions portion of script was about 20 seconds.  */

        INSERT INTO [#sli_cache]
        SELECT sli.[sli_no], sli.[sli_status], sli.[order_no], ord.[customer_no], LTRIM(ISNULL(cus.[fname],'') + ' ' + ISNULL(cus.[lname],'')), 'N',
               ord.[MOS], sli.[perf_no], sli.[zone_no], prf.[performance_dt], prf.[performance_date], prf.[performance_time], 
               prf.[performance_type_name], prf.[title_name], prf.[production_name], sli.[price_type], typ.[description], ISNULL(sli.[comp_code],0), 
               ISNULL(cmp.[description],''), sli.[create_dt], sli.[created_by], sli.[last_update_dt], sli.[last_updated_by]
        FROM [dbo].[T_SUB_LINEITEM] AS sli (NOLOCK)
             LEFT OUTER JOIN [dbo].[T_ORDER] AS ord (NOLOCK) ON ord.[order_no] = sli.[order_no]
             LEFT OUTER JOIN [dbo].[T_CUSTOMER] AS cus (NOLOCK) ON cus.[customer_no] = ord.[customer_no]
             LEFT OUTER JOIN [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] AS prf (NOLOCK) ON prf.performance_no = sli.[perf_no] 
                                                                                       AND prf.[performance_zone] = sli.[zone_no]
             LEFT OUTER JOIN [dbo].[TR_PRICE_TYPE] AS typ (NOLOCK) ON typ.[id] = sli.[price_type]
             LEFT OUTER JOIN [dbo].[TR_COMP_CODE] AS cmp (NOLOCK) ON cmp.[id] = sli.[comp_code]
        WHERE sli.[order_no] IN (SELECT order_no FROM [#order_table] (NOLOCK))
          AND sli.[sli_status] IN (3, 6, 12)


    /***************************************************************************************************************************************************/

                            /*  NOTE: If ever rebuilding data mart from scratch, uncomment first statement and comment out second statement below  */

    /*  Flag all orders that are member orders (during initial build of the data mart)
        Looks for records in TX_CUST_MEMBERSHIP for the customers in the cache table where the current date being worked on
        is between the init date and expire date of the membership.  Should give a mostly accurate account if the person
        was a member at the time they came in.  No way to know if membership was canceled or deactivated after init date 
        but before they came in.  */
        
        UPDATE [#sli_cache] SET [is_member] = 'Y' 
        WHERE [customer_no] IN  (SELECT [customer_no] FROM [dbo].[TX_CUST_MEMBERSHIP] 
                                 WHERE [customer_no] IN (SELECT [customer_no] FROM [#sli_cache]) 
                                   AND [memb_org_no] = 4                                         --4 = Household Memberships
                                   AND [memb_level] <> 'L'                                       --L = Library Membership
                                   AND @history_dt BETWEEN [init_dt] AND [expr_dt])


    /*  Flag all orders that are member orders (after initial build of the data mart)
        Looks for records in TX_CUST_MEMBERSHIP for the customers in the cache table that have a current membership
        at the time this date is being processed.  Since after the initial build, the data mart information will be
        processed within 24 hours of the visit, we only need to know if the person is a current member right now.  */

        --UPDATE [#sli_cache] SET [is_member] = 'Y' 
        --WHERE [customer_no] IN (SELECT [customer_no] FROM [dbo].[TX_CUST_MEMBERSHIP] 
        --                        WHERE [customer_no] IN (SELECT [customer_no] FROM [#sli_cache]) 
        --                          AND [memb_org_no] = 4                                         --4 = Household Memberships
        --                          AND [current_status] IN (2,3)                                 --2 = Active / 3 = Pending
        --                          AND [memb_level] <> 'L')                                      --L = Library Membership

        
    /*************************************************************************************************************************************************** 
    @order_exclusions will keep a list of all orders that are to be excluded from the data for various reasons.  
    ***************************************************************************************************************************************************/

    /*  EXCLUDE EMPTY ORDERS:  These are orders that have no sublineitem records with a status of 3 (seated, paid) or 12 (ticketed, paid)  */

        INSERT INTO @order_exclusions ([order_no], [exclusion_type])
        SELECT ord.[order_no], 'Empty/Returned Order'
        FROM [#order_table] AS ord (NOLOCK)
             LEFT OUTER JOIN [#sli_cache] AS sli (NOLOCK) ON ISNULL(sli.[order_no],0) = ord.[order_no] AND ISNULL(sli.[sli_status],0) IN (3,12)
        GROUP BY ord.[order_no] 
        HAVING COUNT(DISTINCT sli.[sli_no]) = 0


    /*  EXCLUDE GROUP ORDERS:  An order is designated as a group sale if it contains any single performance with 15 or more paying visitors attending. 
                               It will still be counted if the tickets were paid for but never printed. 
                               sli_status 3 = Seated, Paid and sli_status 12 = Ticketed, Paid   */
            
        INSERT INTO @order_exclusions ([order_no], [exclusion_type])
        SELECT DISTINCT [order_no], 'Group Order'
        FROM [#sli_cache] (NOLOCK)
        WHERE [sli_status] IN (3,12)
        GROUP BY [order_no], [perf_no]
        HAVING COUNT(*) >= 15


    /*  EXCLUDE SCHOOL ORDERS:  Identified by mode of sale.  Any order that was created in either the Schools or the
                                Web Sales School mode of sale is designated as a school order  */
  
        INSERT INTO @order_exclusions ([order_no], [exclusion_type])
        SELECT DISTINCT [order_no], 'School Order'
        FROM [#sli_cache] (NOLOCK)
        WHERE [MOS] IN (@mos_school_no, @mos_school_web_no)
                 
    /*  EXCLUDE TOUR OPERATOR ORDERS:  Identified by mode of sale. Any order created in the Tour Operators mode of sale is
                                       designated as a Tour Operator order.  */

        INSERT INTO @order_exclusions ([order_no], [exclusion_type])
        SELECT DISTINCT [order_no], 'Tour Operator Order'
        FROM [#sli_cache] (NOLOCK)
        WHERE [mos] = @mos_tour_op_no


    /*  EXCLUDE HOTEL ORDERS:  Identified by mode of sale.  Any order created in the Consignment mode of sale is excluded
                               Note: right now only being used for hotel sales.  Might be used for other sales later but
                                     any sale it would be used for would need to also be excluded.   */

        INSERT INTO @order_exclusions ([order_no], [exclusion_type])
        SELECT DISTINCT [order_no], 'Hotel Order'
        FROM [#sli_cache] (NOLOCK)
        WHERE [MOS] = @mos_consignment_no


    /*  EXCLUDE INTERNAL/EXTERNAL ORDERS:  Identified by mode of sale.  Any orders created in the Internal Tickets and Event Add
                                           mode of sale is designated as an Internal/External group order.  */

        INSERT INTO @order_exclusions ([order_no], [exclusion_type])
        SELECT DISTINCT [order_no], 'Internal/External Order'
        FROM [#sli_cache] (NOLOCK)
        WHERE [mos] = @mos_internal_no


    /*  EXCLUDE BUYOUT ORDERS:  Identified by nide of sale.  Any order created in the Buyout mode of sale is excluded.  */

        INSERT INTO @order_exclusions ([order_no], [exclusion_type])
        SELECT DISTINCT [order_no], 'Buyout Order'
        FROM [#sli_cache] (NOLOCK)
        WHERE [mos] = @mos_buyout_no

    /*  EXCLUDE CITY PASS SALES ORDERS:  Identified by mode of sale.  Any order created in the CityPASS mode of sale is excluded.  */

        INSERT INTO @order_exclusions ([order_no], [exclusion_type])
        SELECT DISTINCT [order_no], 'CityPASS Sale Order'
        FROM [#sli_cache] (NOLOCK)
        WHERE [mos] = @mos_citypass_no

            
    /*  EXCLUDE THRILL RIDE ORDERS: These are Thrill Ride orders processed at the Thrill Ride capsules.  Thrill Ride orders processed at the
                                    Box Office should be counted.  Thrill Ride orders identified by two modes of sale Thrill Ride Capsule A (# 22) 
                                    and Thrill Ride Capsule B (# 32)  */
    
        INSERT INTO @order_exclusions ([order_no], [exclusion_type])
        SELECT DISTINCT [order_no], 'Thrill Ride Order'
        FROM [#sli_cache] (NOLOCK)
        WHERE [MOS] IN (@mos_thrilla_no, @mos_thrillb_no)


    /*  Update orders table with membership types (zone names) for the membership products.  
        Membership types are needed to identify the final two exclusion types  */
            
        UPDATE [#order_table]
           SET [membership_sale] = (SELECT MAX(ISNULL(prf.[zone_name],''))
                                    FROM [#sli_cache] AS sli (NOLOCK)
                                         JOIN [#membership_performances] AS prf ON prf.[performance_no] = sli.[perf_no] 
                                                                               AND prf.[zone_no] = sli.[zone_no]
                                     WHERE sli.[order_no] = [#order_table].[order_no] and sli.[sli_status] IN (3,12) 
                                       AND [prf].[zone_name] <> 'Temporary Membership Card' AND prf.[zone_name] NOT LIKE '%Upgrade%')

        UPDATE [#order_table] SET [membership_sale] = '' WHERE [membership_sale] IS null


    /*  EXCLUDE CERTAIN MEMBERSHIP ORDERS:  non gift sales to active members not in the renewal period 
                                            non gift sales to active members in the renewal period who are enrolled in one step
                                            These order numbers are stored in a separate table because they will be put back
                                            for the second part of the procedure (add-ons)  */
        
            /*  Membership sales that are renewals (NRR_Status = 'RN') and not a gift (benefactor_no = 0) and is outside the
                renewal period, which is the three months prior to the expiration date (create_dt < dateadd(month,-1,prv.[expr_dt])
                                            prv = previous membership */

        INSERT INTO @order_exclusions_memb ([order_no], [operator], [created_or_touched], [membership_sale], [sale_to_member], 
                                            [total_performances], [add_on_counter])
        SELECT ack.[order_no], ord.[operator], ord.[created_or_touched], ord.[membership_sale], ord.[sale_to_member], 
               ord.[total_performances], ord.[add_on_counter]
        FROM dbo.LT_MOS_ACK_MEMBERSHIPS_AS_PRODUCTS AS ack (NOLOCK)
             LEFT OUTER JOIN [dbo].[TX_CUST_MEMBERSHIP] AS mem (NOLOCK) ON mem.[cust_memb_no] = ack.[cust_memb_no]
             LEFT OUTER JOIN [dbo].[TX_CUST_MEMBERSHIP] AS prv (NOLOCK) ON prv.[cust_memb_no] = mem.[parent_no]
             INNER JOIN [#order_table] AS ord (NOLOCK) ON ord.[order_no] = ack.[order_no]
             INNER JOIN [dbo].[T_MEMB_LEVEL] AS plv ON plv.[memb_level] = prv.[memb_level]
        WHERE ack.[order_no] IN (SELECT [order_no] FROM [#order_table] (NOLOCK) WHERE [membership_sale] <> '')
          AND mem.[NRR_status] = 'RN' AND ack.benefactor_no = 0 AND mem.[create_dt] < DATEADD(MONTH,(plv.[renewal] * -1),prv.[expr_dt])
          

    /*  Pulling only records where the member is a part of the one-step program (inner join where constituency = 30)
        Of those, Membership sales that are renewals (NRR_Status = 'RN') and not a gift (benefactor = 0)
        and is within the three month renewal period (create_dt between dateadd(month,-1,prv.[expr_dt] and prv.[expr_dt])  
                                        prv = previous membership */

        INSERT INTO @order_exclusions_memb ([order_no], [operator], [created_or_touched], [membership_sale], [sale_to_member],
                                            [total_performances], [add_on_counter])
        SELECT ack.[order_no], ord.[operator], ord.[created_or_touched], ord.[membership_sale], ord.[sale_to_member],
               ord.[total_performances], ord.[add_on_counter]
        FROM dbo.LT_MOS_ACK_MEMBERSHIPS_AS_PRODUCTS AS ack (NOLOCK)
             LEFT OUTER JOIN [dbo].[TX_CUST_MEMBERSHIP] AS mem (NOLOCK) ON mem.[cust_memb_no] = ack.[cust_memb_no]
             LEFT OUTER JOIN [dbo].[TX_CUST_MEMBERSHIP] AS prv (NOLOCK) ON prv.[cust_memb_no] = mem.[parent_no]
             INNER JOIN [dbo].[TX_CONST_CUST] AS con (NOLOCK) ON con.[customer_no] = mem.[customer_no] AND con.[constituency] = 30
             INNER JOIN [#order_table] AS ord (NOLOCK) ON ord.[order_no] = ack.[order_no]
             INNER JOIN [dbo].[T_MEMB_LEVEL] AS plv ON plv.[memb_level] = prv.[memb_level]
        WHERE ack.[order_no] IN (SELECT [order_no] FROM [#order_table] (NOLOCK) WHERE [membership_sale] <> '')
          AND mem.[NRR_status] = 'RN' AND ack.benefactor_no = 0 AND mem.[create_dt] < DATEADD(MONTH,(plv.[renewal] * -1),prv.[expr_dt])
          AND con.start_dt < mem.[create_dt]

    /*  Now that the exclusions have been identified, delete them from the orders table.  */

            DELETE FROM [#order_table] WHERE [order_no] IN (SELECT [order_no] FROM @order_exclusions)
            DELETE FROM [#order_table] WHERE [order_no] IN (SELECT [order_no] FROM @order_exclusions_memb)

       
    /***************************************************************************************************************************************************
    @stats_table_mem will keep the statistics for each operator based on the list of orders that remains.
    ****************************************************************************************************************************************************/

    /*  Get Total orders created for each operator  */
     
        INSERT INTO @stats_table_mem
        SELECT 1, @history_dt, [operator], COUNT(DISTINCT [order_no]), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        FROM [#order_table] (NOLOCK) 
        WHERE [created_or_touched] = 'C' 
        GROUP BY [operator]


    /*  Get Total orders created or touched for each operator  */

        INSERT INTO @stats_table_mem
        SELECT 1, @history_dt, [operator], 0, COUNT(DISTINCT [order_no]), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        FROM [#order_table] (NOLOCK)  
        GROUP BY [operator]


    /*  Get Total membership orders for each operator  */

        --INSERT INTO @stats_table_mem
        --SELECT 1, @history_dt, [operator], 0, 0, COUNT(DISTINCT [order_no]), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        --FROM [#order_table] (NOLOCK)
        --WHERE [membership_sale] <> '' 
        --GROUP BY Operator
        INSERT INTO @stats_table_mem
        SELECT 1, @history_dt, [membership_operator], 0, 0, COUNT(DISTINCT [order_no]), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        FROM [dbo].[LV_RPT_CASHOUT_MEMBERSHIPS] (NOLOCK)
        WHERE [order_no] IN (SELECT [order_no] FROM [#order_table]) AND [membership_type] IN ('Basic 2','Basic 5','Basic 8','Premier 2','Premier 5','Premier 8')
        GROUP BY [membership_Operator]


    /*  Get Total Basic 2 membership orders for each operator  */

        --INSERT INTO @stats_table_mem
        --SELECT 1, @history_dt, [operator], 0, 0, 0, COUNT(DISTINCT [order_no]), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        --FROM[#order_table] (NOLOCK)
        --WHERE [membership_sale] = 'Basic 2' 
        --GROUP BY Operator
        INSERT INTO @stats_table_mem
        SELECT 1, @history_dt, [membership_operator], 0, 0, 0, COUNT(DISTINCT [order_no]), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        FROM [dbo].[LV_RPT_CASHOUT_MEMBERSHIPS] (NOLOCK)
        WHERE [order_no] IN (SELECT [order_no] FROM [#order_table]) AND [membership_type] = 'Basic 2'
        GROUP BY [membership_Operator]


    /*  Get Total Basic 5 membership orders for each operator  */

        --INSERT INTO @stats_table_mem
        --SELECT 1, @history_dt, [operator], 0, 0, 0, 0, COUNT(DISTINCT [order_no]), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        --FROM [#order_table] (NOLOCK)
        --WHERE [membership_sale] = 'Basic 5' 
        --GROUP BY Operator
        INSERT INTO @stats_table_mem
        SELECT 1, @history_dt, [membership_operator], 0, 0, 0, 0, COUNT(DISTINCT [order_no]), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        FROM [dbo].[LV_RPT_CASHOUT_MEMBERSHIPS] (NOLOCK)
        WHERE [order_no] IN (SELECT [order_no] FROM [#order_table]) AND [membership_type] = 'Basic 5'
        GROUP BY [membership_Operator]


    /*  Get Total Basic 8 membership orders for each operator  */

        --INSERT INTO @stats_table_mem
        --SELECT 1, @history_dt, [operator], 0, 0, 0, 0, 0, COUNT(DISTINCT [order_no]), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        --FROM [#order_table] (NOLOCK)
        --WHERE [membership_sale] = 'Basic 8' 
        --GROUP BY Operator
        INSERT INTO @stats_table_mem
        SELECT 1, @history_dt, [membership_operator], 0, 0, 0, 0, 0, COUNT(DISTINCT [order_no]), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        FROM [dbo].[LV_RPT_CASHOUT_MEMBERSHIPS] (NOLOCK)
        WHERE [order_no] IN (SELECT [order_no] FROM [#order_table]) AND [membership_type] = 'Basic 8'
        GROUP BY [membership_Operator]
        

    /*  Get Total Premier 2 membership orders for each operator  */

        --INSERT INTO @stats_table_mem
        --SELECT 1, @history_dt, [operator], 0, 0, 0, 0, 0, 0, 0, COUNT(DISTINCT [order_no]), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        --FROM [#order_table] (NOLOCK) 
        --WHERE [membership_sale] = 'Premier 2' 
        --GROUP BY Operator
        INSERT INTO @stats_table_mem
        SELECT 1, @history_dt, [membership_operator], 0, 0, 0, 0, 0, 0, 0, COUNT(DISTINCT [order_no]), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        FROM [dbo].[LV_RPT_CASHOUT_MEMBERSHIPS] (NOLOCK)
        WHERE [order_no] IN (SELECT [order_no] FROM [#order_table]) AND [membership_type] = 'Premier 2'
        GROUP BY [membership_Operator]

    /*  Get Total Premier 5 membership orders for each operator  */

        --INSERT INTO @stats_table_mem
        --SELECT 1, @history_dt, [operator], 0, 0, 0, 0, 0, 0, 0, 0, COUNT(DISTINCT [order_no]), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        --FROM [#order_table] (NOLOCK) 
        --WHERE [membership_sale] = 'Premier 5' 
        --GROUP BY Operator
        INSERT INTO @stats_table_mem
        SELECT 1, @history_dt, [membership_operator], 0, 0, 0, 0, 0, 0, 0, 0, COUNT(DISTINCT [order_no]), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        FROM [dbo].[LV_RPT_CASHOUT_MEMBERSHIPS] (NOLOCK)
        WHERE [order_no] IN (SELECT [order_no] FROM [#order_table]) AND [membership_type] = 'Premier 5'
        GROUP BY [membership_Operator]


    /*  Get Total Premier 8 membership orders for each operator  */

        --INSERT INTO @stats_table_mem
        --SELECT 1, @history_dt, [operator], 0, 0, 0, 0, 0, 0, 0, 0, 0, COUNT(DISTINCT [order_no]), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        --FROM [#order_table] (NOLOCK)
        --WHERE [membership_sale] = 'Premier 8' 
        --GROUP BY Operator
        INSERT INTO @stats_table_mem
        SELECT 1, @history_dt, [membership_operator], 0, 0, 0, 0, 0, 0, 0, 0, 0, COUNT(DISTINCT [order_no]), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        FROM [dbo].[LV_RPT_CASHOUT_MEMBERSHIPS] (NOLOCK)
        WHERE [order_no] IN (SELECT [order_no] FROM [#order_table]) AND [membership_type] = 'Premier 8'
        GROUP BY [membership_Operator]


    /*  Get Total Basic Membeship orders with one-step for each operator  */

        INSERT INTO @stats_table_mem
        SELECT 1, @history_dt, [membership_operator], 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, SUM([one_step_count]), 0, 0, 0, 0, 0, 0, 0, 0
        FROM [dbo].[LV_RPT_CASHOUT_MEMBERSHIPS] 
        WHERE [membership_date] = convert(char(10),@history_dt,111) and left(membership_type,5) = 'Basic' AND sli_status_no IN (3, 12)
        GROUP BY [membership_operator]


    /*  Get Total Premier Membeship orders with one-step for each operator  */

        INSERT INTO @stats_table_mem
        SELECT 1, @history_dt, [membership_operator], 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, SUM([one_step_count]), 0, 0, 0, 0, 0, 0, 0
        FROM [dbo].[LV_RPT_CASHOUT_MEMBERSHIPS] 
        WHERE [membership_date] = convert(char(10),@history_dt,111) and left(membership_type,5) = 'Premi' AND sli_status_no IN (3, 12)
        GROUP BY [membership_operator]

    /*  Get Total Basic Membeship one-step failure recovery orders for each operator  */

        INSERT INTO @stats_table_mem
        SELECT 1, @history_dt, [membership_operator], 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, COUNT(DISTINCT [order_no]), 0, 0, 0, 0, 0
        FROM [dbo].[LV_RPT_CASHOUT_MEMBERSHIPS] 
        WHERE [membership_date] = convert(char(10),@history_dt,111) and left(membership_type,5) = 'Basic' AND sli_status_no IN (3, 12) AND [is_one_step_recovery] = 'Y'
        GROUP BY [membership_operator]

    /*  Get Total Premier Membeship one-step failure recovery orders for each operator  */

        INSERT INTO @stats_table_mem
        SELECT 1, @history_dt, [membership_operator], 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, count(DISTINCT [order_no]), 0, 0, 0, 0
        FROM [dbo].[LV_RPT_CASHOUT_MEMBERSHIPS] 
        WHERE [membership_date] = convert(char(10),@history_dt,111) and left(membership_type,5) = 'Premi' AND sli_status_no IN (3, 12) AND [is_one_step_recovery] = 'Y'
        GROUP BY [membership_operator]

    /*  Get Total Basic Membeship that are gift memberships for each operator  */

        INSERT INTO @stats_table_mem
        SELECT 1, @history_dt, [membership_operator], 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, COUNT(DISTINCT [order_no]), 0, 0
        FROM [dbo].[LV_RPT_CASHOUT_MEMBERSHIPS] 
        WHERE [membership_date] = convert(char(10),@history_dt,111) and left(membership_type,5) = 'Basic' AND sli_status_no IN (3, 12) AND [is_gift_membership] = 'Y'
        GROUP BY [membership_operator]

    /*  Get Total Premier Membeship that are gift memberships for each operator  */

        INSERT INTO @stats_table_mem
        SELECT 1, @history_dt, [membership_operator], 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, COUNT(DISTINCT [order_no]), 0
        FROM [dbo].[LV_RPT_CASHOUT_MEMBERSHIPS] 
        WHERE [membership_date] = convert(char(10),@history_dt,111) and left(membership_type,5) = 'Premi' AND sli_status_no IN (3, 12) AND [is_gift_membership] = 'Y'
        GROUP BY [membership_operator]

    /*  Aggregate all the values in a second data pass, then delete the first data pass  */
        
        INSERT INTO @stats_table_mem
        SELECT 2, @history_dt, [operator], SUM([orders_created]), SUM([orders_touched]), SUM(membership_sales), SUM([basic_2_sales]),
               SUM([basic_5_sales]), SUM([basic_8_sales]), 0, SUM([premier_2_sales]), SUM([premier_5_sales]), SUM([premier_8_sales]), 0,
               SUM([one_step_basic]), SUM([one_step_premier]), 0, SUM([basic_one_step_recovery]), SUM([premier_one_step_recovery]), 0,
               SUM([basic_gift_memberships]), SUM([premier_gift_memberships]), 0
        FROM @stats_table_mem 
        WHERE [data_pass] = 1 
        GROUP BY [operator]

        DELETE FROM @stats_table_mem WHERE [data_pass] = 1
        
    /*  Update totals for each type of membership  */

        UPDATE @stats_table_mem SET [basic_total_sales] = ([basic_2_sales] + [basic_5_sales] + [basic_8_sales]),
                                    [premier_total_sales] = ([premier_2_sales] + [premier_5_sales] + [premier_8_sales]),
                                    [one_step_total] = ([one_step_basic] + [one_step_premier]),
                                    [total_one_step_recovery] = ([basic_one_step_recovery] + [premier_one_step_recovery]),
                                    [total_gift_memberships] = ([basic_gift_memberships] + [premier_gift_memberships])

    /***************************************************************************************************************************************************
    @stats_table_add will keep the add on statistics for each operator based on the list of orders that remains.
    ****************************************************************************************************************************************************/

    /*  Re-insert the membership orders that were excluded from the previous part of the procedure. */

    INSERT INTO [#order_table] ([order_no], [operator], [created_or_touched], [membership_sale], [sale_to_member], [total_performances], [add_on_counter])
    SELECT [order_no], [operator], [created_or_touched], [membership_sale], [sale_to_member], [total_performances], [add_on_counter]
    FROM @order_exclusions_memb


    /*  Delete orders that do not have at least one of the designated venues in it to be counted in the add on statistics  */

        DELETE FROM [#order_table] WHERE [order_no] NOT IN (SELECT DISTINCT [order_no] FROM [#sli_cache] (NOLOCK)
                                                            WHERE [order_no] IN (SELECT [order_no] FROM #order_table)
                                                             AND [title_name] IN (SELECT [title_name] FROM @valid_titles))
    
                  
    /*  Delete cache information for orders no longer in the order table  */

    DELETE FROM [#sli_cache] 
    WHERE [order_no] NOT IN (SELECT [order_no] FROM [#order_table] (NOLOCK))

    /*  Flag whether each order was or was not sold to a visitor who was a member at the time of visit  */

        UPDATE [#order_table]
        SET [sale_to_member] = 'Y' 
        WHERE [order_no] IN (SELECT DISTINCT [order_no] FROM [#sli_cache] WHERE [is_member] = 'Y')


    /*  Get the number of performances in each order.  This can be done at the title level, the performance level, or at the performance time level  
        The way it's being done is hard-coded into the @add_on grouping variable set at the start of the procedure  */
    
        --Performance and Time Level - Two Omni shows to same title at different times counted as two
        IF @add_on_grouping = 'Performance and Time'
            UPDATE [#order_table] 
            SET [total_performances] = (SELECT COUNT(DISTINCT CONVERT(VARCHAR(20),[perf_no]) + '_' + CONVERT(VARCHAR(20),[zone_no])) FROM [#sli_cache]
                                        WHERE [#sli_cache].[order_no] = [#order_table].[order_no])

        --Performance Level - Two Omni shows with same title counted as 1/two different titles counted as 2
        ELSE IF @add_on_grouping = 'Performance'
            UPDATE [#order_table] 
            SET [total_performances] = (SELECT COUNT(DISTINCT [perf_no]) FROM [#sli_cache]
                                        WHERE [#sli_cache].[order_no] = [#order_table].[order_no])

        --Title Level - Two or more Omni shows, regardles of titles and times all counted as one
        ELSE
            UPDATE [#order_table] 
            SET [total_performances] = (SELECT COUNT(DISTINCT [title_name]) FROM [#sli_cache]
                                        WHERE [#sli_cache].[order_no] = [#order_table].[order_no])

    /*  Any order with more than 1 performance on it has an add on  */

        UPDATE [#order_table] SET [add_on_counter] = 1 WHERE [total_performances] > 1
        UPDATE [#order_table] SET [add_on_counter] = 0 WHERE [total_performances] <= 1


    /*  Get order add on stats */
                
        INSERT INTO @stats_table_add
        SELECT 1, @history_dt, [operator], COUNT(DISTINCT [order_no]), SUM([add_on_counter]), SUM([total_performances])
        FROM [#order_table]
        GROUP BY [operator]


    /*  If being run for a single operator, delete all the others  */

        IF @operator <> '' BEGIN

            DELETE FROM @stats_table_mem WHERE [operator] <> @operator
        
            DELETE FROM @stats_table_add WHERE [operator] <> @operator

        END


    /*  Return data from procedure or write data to the data mart table  */

    IF @return_or_write IN ('B','W') BEGIN

        /*  If data for this date already exists in the data mart, delete it  */

            DELETE FROM [dbo].[LT_HISTORY_TRX_STATS] WHERE history_dt = @history_dt

        /*  Write new data for this date to the data mart.  */

            INSERT INTO [dbo].[LT_HISTORY_TRX_STATS]
            SELECT    @create_dt
                    , @history_dt
                    , CONVERT(CHAR(10),@history_dt,111)             
                    , ISNULL(mem.[operator],'Unknown')
                    , ISNULL(usr.[fname] + ' ' + usr.[lname],'Unknown Operator')    
                    , ISNULL(usr.[lname] + ', ' + usr.[fname],'zUnknown Operator')
                    , ISNULL(usr.[location],'')                     
                    , ISNULL(mem.[orders_created],0.0)
                    , ISNULL(mem.[orders_touched],0.0)              
                    , ISNULL(mem.[membership_sales],0.0)
                    , ISNULL(mem.[total_one_step_recovery],0.0)    
                    , ISNULL(mem.[total_gift_memberships],0.0)
                    , ISNULL(mem.[basic_2_sales],0.0)               
                    , ISNULL(mem.[basic_5_sales],0.0)               
                    , ISNULL(mem.[basic_8_sales],0.0)               
                    , ISNULL(mem.[basic_total_sales],0.0)           
                    , ISNULL(mem.[basic_one_step_recovery],0.0)
                    , ISNULL(mem.[basic_gift_memberships],0.0)
                    , ISNULL(mem.[one_step_basic],0.0)              
                    , ISNULL(mem.[premier_2_sales],0.0)
                    , ISNULL(mem.[premier_5_sales],0.0)             
                    , ISNULL(mem.[premier_8_sales],0.0)
                    , ISNULL(mem.[premier_total_sales],0.0)         
                    , ISNULL(mem.[premier_one_step_recovery],0.0)
                    , ISNULL(mem.[premier_gift_memberships],0.0)
                    , ISNULL(mem.[one_step_premier],0.0)            
                    , ISNULL(mem.[one_step_total],0.0)              
                    , ISNULL(aon.[orders_counted],0.0)              
                    , ISNULL(aon.[multi_venue_orders],0.0)          
                    , ISNULL(aon.[total_venues_sold],0.0)
            FROM @stats_table_mem AS mem
                    LEFT OUTER JOIN @stats_table_add AS aon ON aon.[operator] = mem.[operator]
                    LEFT OUTER JOIN [dbo].[T_METUSER] AS usr (NOLOCK) ON usr.[userid] = mem.operator 
       
    END

    IF @return_or_write IN ('B','R') BEGIN

        /*  Return new data for this date as a data set (for a report).  */

            SELECT    @create_dt AS 'run_dt'
                    , @history_dt AS 'history_dt'
                    , CONVERT(CHAR(10),@history_dt,111) AS 'history_date'             
                    , ISNULL(mem.[operator],'Unknown') AS 'user_id'
                    , ISNULL(usr.[fname] + ' ' + usr.[lname],'Unknown Operator') AS 'user_name'    
                    , ISNULL(usr.[lname] + ', ' + usr.[fname],'zUnknown Operator') AS 'user_name_sort'
                    , ISNULL(usr.[location],'') AS 'user_location'                    
                    , ISNULL(mem.[orders_created],0.0) AS 'orders_created'
                    , ISNULL(mem.[orders_touched],0.0) AS 'orders_touched'            
                    , ISNULL(mem.[membership_sales],0.0) AS 'membership_sales'
                    , ISNULL(mem.[total_one_step_recovery],0.0) AS 'total_one_step_recovery'
                    , ISNULL(mem.[total_gift_memberships],0.0) AS 'total_gift_memberships'
                    , ISNULL(mem.[basic_2_sales],0.0) AS 'basic_2_sales'              
                    , ISNULL(mem.[basic_5_sales],0.0) AS 'basic_5_sales'
                    , ISNULL(mem.[basic_8_sales],0.0) AS 'basic_8_sales'              
                    , ISNULL(mem.[basic_total_sales],0.0) AS 'basic_total_sales'
                    , ISNULL(mem.[basic_one_step_recovery],0.0) AS 'basic_one_step_recovery'
                    , ISNULL(mem.[basic_gift_memberships],0.0) AS 'basic_gift_memberships'
                    , ISNULL(mem.[one_step_basic],0.0) AS 'one_step_basic'            
                    , ISNULL(mem.[premier_2_sales],0.0) AS 'premier_2_sales'          
                    , ISNULL(mem.[premier_5_sales],0.0) AS 'premier_5_sales'          
                    , ISNULL(mem.[premier_8_sales],0.0) AS 'premier_8_sales'          
                    , ISNULL(mem.[premier_total_sales],0.0) AS 'premier_total_sales'
                    , ISNULL(mem.[premier_one_step_recovery],0.0) AS 'premier_one_step_recovery'
                    , ISNULL(mem.[premier_gift_memberships],0.0) AS 'premier_gift_memberships'  
                    , ISNULL(mem.[one_step_premier],0.0) AS 'one_step_premier'        
                    , ISNULL(mem.[one_step_total],0.0) AS 'one_step_total'            
                    , ISNULL(aon.[orders_counted],0.0) AS 'orders_counted'            
                    , ISNULL(aon.[multi_venue_orders],0.0) AS 'multi_venue_orders'    
                    , ISNULL(aon.[total_venues_sold],0.0) AS 'total_venues_sold'
            FROM @stats_table_mem AS mem
                 LEFT OUTER JOIN @stats_table_add AS aon ON aon.[operator] = mem.[operator]
                 LEFT OUTER JOIN [dbo].[T_METUSER] AS usr (NOLOCK) ON usr.[userid] = mem.operator
            ORDER BY user_name_sort

    END

DONE:

    /*  Destroy Temp Tables  */

        IF OBJECT_ID('tempdb..#sli_cache') IS NOT NULL DROP TABLE [#sli_cache]
        IF OBJECT_ID('tempdb..#membership_performances') IS NOT NULL DROP TABLE [#membership_performances]
        IF OBJECT_ID('tempdb..#order_table') IS NOT NULL DROP TABLE [#order_table]

END
GO

GRANT EXECUTE ON [dbo].[LP_UPDATE_HISTORY_TRX_STATS] TO ImpUsers
GO


--EXECUTE [dbo].[LP_UPDATE_HISTORY_TRX_STATS] @history_dt = '11-10-2017', @create_dt = NULL, @operator = Null, @return_or_write = 'R'


--SELECT * FROM dbo.LT_HISTORY_TRX_STATS WHERE history_date = '2017/11/10'
--SELECT * FROM dbo.T_SUB_LINEITEM WHERE create_dt >= '10-27-2017' AND rule_id = 1218 AND created_by = 'kaltom00'