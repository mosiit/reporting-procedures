USE [impresario]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[LRP_EVENT_RSVP]
(
	@campaign_no INT
)
AS
/*
created 11/3/2016 Brian Graham (Tessitura)
EXEC LRP_EVENT_RSVP @campaign_no = 2048

-- WO 90454 - Event RSVP Report Issue
-- Debbie Jacob, 2/23/18
-- The "individual registrants that are not associated with a Household record" query was not returning any results. Melanie re-wrote this 1 query. 
-- This entire report was created by Tessitura. I did not try to rewrite any of the code. 
*/

if OBJECT_ID('tempdb..#results') is not null begin drop table #results end 
create table #results(Event_Description varchar(30), Event_Name varchar(110), Event_Requests varchar(255), Reg_ID int, Reg_Name varchar(55), reg_sort varchar(55), source_name varchar(50)
, Source_Group varchar(30), Inv_Status varchar(30), Guest_ID int, Guest_Name varchar(55), First_Name varchar(20), Last_Name varchar(55), Guest_SortName varchar(55)
, Reg_Primary_Star_Email varchar(80)
, Guest_Primary_Star_Email varchar(80)
, Familiar_Name varchar(55), Familiar_Salu varchar(55)
, PM_Initial varchar(55)
, PM_Name varchar(20)
, Emp_PM_Initial varchar(55) 
, Emp_PM_Name varchar(20)
, Board varchar(30)
, Washburn_Society varchar(255)
, Washburn_Society_Start_Dt date
, Colby_Society varchar(255)
, Colby_Society_Start_Dt date
, Adv_Comm_Flag char(1)
, Food_Task_Flag char(1)
, Discoverers_SubComm_Flag char(1)
, YngLdrshp_Comm_Flag char(1)
, MOS_Inclination varchar(30)
, Annual_Gift_Likelihood varchar(255)
, Major_Gift_Likelihood varchar(255)
, Planned_Gift_Likelihood varchar(255)
, Capacity_Rating varchar(30)
, Target_Gift_Range varchar(30)
, Lifetime_Contribution money
, Best_Last_Mem varchar(3)
, Event_Research_Briefing varchar(max)
, main_id int
)
insert into #results(Event_Description, Event_Name, Reg_ID, Reg_Name, reg_sort, source_name, Source_Group, Inv_Status, Guest_ID, Guest_Name, First_Name, Last_Name, Guest_SortName, main_id)
/*Pulls individual registrants that are not associated with a Household record*/
SELECT c.description "event_description", ed.eventname, ee.customer_no "reg_id", cs.esal1_desc "reg_name", t.sort_name "reg_sort",
am.source_name, sg.description  "source_group",  s.description "inv_status", 
ee.customer_no "guest_id", cs.esal1_desc "guest_name", t.fname, t.lname, t.sort_name, ee.customer_no "main_id"
FROM TX_EVENT_EXTRACT AS ee (NOLOCK)
INNER JOIN T_CAMPAIGN AS c (NOLOCK) ON ee.campaign_no = c.campaign_no
LEFT OUTER JOIN LTR_EVENT_DETAIL AS ed (NOLOCK) ON ee.campaign_no = ed.campaign_no
INNER JOIN TX_APPEAL_MEDIA_TYPE AS am (NOLOCK) ON ee.source_no = am.source_no 
INNER JOIN TR_SOURCE_GROUP AS sg (NOLOCK) ON am.source_group = sg.id
INNER JOIN TR_INVITATION_STATUS AS s (NOLOCK) ON ee.inv_status = s.id
INNER JOIN T_CUSTOMER AS t (NOLOCK) ON ee.customer_no = t.customer_no
INNER JOIN TX_CUST_SAL AS cs (NOLOCK) ON ee.customer_no = cs.customer_no
WHERE ee.campaign_no = @campaign_no AND
cs.default_ind = 'Y' AND
t.cust_type = 1 AND
ee.customer_no NOT IN (SELECT customer_no from T_CUSTOMER WHERE name_status = 2) AND
ee.customer_no NOT IN (SELECT a.individual_customer_no FROM T_AFFILIATION as a (NOLOCK) INNER JOIN TX_EVENT_EXTRACT AS ee (NOLOCK) ON a.individual_customer_no = ee.customer_no WHERE a.affiliation_type_id = 10002) AND
ee.customer_no NOT IN (SELECT a.individual_customer_no FROM T_AFFILIATION as a (NOLOCK) INNER JOIN TX_EVENT_EXTRACT AS ee (NOLOCK) ON a.group_customer_no = ee.customer_no WHERE a.affiliation_type_id = 10002)

union all 

/*Pulls individual registrants that are associated with a Household record*/
SELECT c.description "event_description", ed.eventname, ee.customer_no "reg_id", cs.esal1_desc "reg_name", t.sort_name "reg_sort",
am.source_name, sg.description  "source_group",  s.description "inv_status", 
ee.customer_no "guest_id", cs.esal1_desc "guest_name", t.fname, t.lname, t.sort_name, a.group_customer_no "main_id"
FROM TX_EVENT_EXTRACT AS ee (NOLOCK)
INNER JOIN T_CAMPAIGN AS c (NOLOCK) ON ee.campaign_no = c.campaign_no
LEFT OUTER JOIN LTR_EVENT_DETAIL AS ed (NOLOCK) ON ee.campaign_no = ed.campaign_no
INNER JOIN TX_APPEAL_MEDIA_TYPE AS am (NOLOCK) ON ee.source_no = am.source_no 
INNER JOIN TR_SOURCE_GROUP AS sg (NOLOCK) ON am.source_group = sg.id
INNER JOIN TR_INVITATION_STATUS AS s (NOLOCK) ON ee.inv_status = s.id
INNER JOIN T_CUSTOMER AS t (NOLOCK) ON ee.customer_no = t.customer_no
INNER JOIN TX_CUST_SAL AS cs (NOLOCK) ON ee.customer_no = cs.customer_no AND cs.default_ind = 'Y'
INNER JOIN T_AFFILIATION AS a (NOLOCK) ON ee.customer_no = a.individual_customer_no AND a.affiliation_type_id = 10002 
WHERE 
ee.campaign_no = @campaign_no  
AND t.cust_type = 1 
AND ISNULL(t.name_status,0) != 2 

union all 

/*Household Reg*/
SELECT c.description "event_description", ed.eventname, ee.customer_no "reg_id", t.lname "reg_name",t.sort_name "reg_sort", 
am.source_name, sg.description "source_group", s.description "inv_status",  
cs2.customer_no "guest_id", cs2.esal1_desc "guest_name", t2.fname, t2.lname, t2.sort_name, ee.customer_no "main_id"
FROM TX_EVENT_EXTRACT AS ee (NOLOCK)
INNER JOIN T_CAMPAIGN AS c (NOLOCK) ON ee.campaign_no = c.campaign_no
LEFT OUTER JOIN LTR_EVENT_DETAIL as ed (NOLOCK) ON ee.campaign_no = ed.campaign_no
INNER JOIN TX_APPEAL_MEDIA_TYPE AS am (NOLOCK) ON ee.source_no = am.source_no
INNER JOIN TR_SOURCE_GROUP AS sg (NOLOCK) ON am.source_group = sg.id
INNER JOIN TR_INVITATION_STATUS AS s (NOLOCK) ON ee.inv_status = s.id
INNER JOIN T_CUSTOMER AS t (NOLOCK) ON ee.customer_no = t.customer_no AND t.cust_type = 7
INNER JOIN TX_CUST_SAL AS cs (NOLOCK) ON ee.customer_no = cs.customer_no AND cs.default_ind = 'Y'
INNER JOIN T_AFFILIATION AS a  (NOLOCK) ON ee.customer_no = a.group_customer_no AND a.affiliation_type_id = 10002 
INNER JOIN TX_CUST_SAL AS cs2 (NOLOCK) ON a.individual_customer_no = cs2.customer_no AND cs2.default_ind = 'Y'
INNER JOIN T_CUSTOMER AS t2 (NOLOCK) ON a.individual_customer_no  = t2.customer_no
WHERE ee.campaign_no = @campaign_no
AND ISNULL(t2.name_status,0) != 2 

--select * from #results
--return


update a set a.Event_Requests = b.notes 
from #results a 
join LVS_EVENTS_SPECIALREQUESTS b on a.Reg_ID = b.customer_no 

update a set a.Reg_Primary_Star_Email = b.address 
from #results a 
join T_EADDRESS AS b (NOLOCK) ON a.Reg_ID = b.customer_no AND b.primary_ind = 'Y' 

update a set a.Guest_Primary_Star_Email = b.address 
from #results a 
join T_EADDRESS AS b (NOLOCK) ON a.Guest_ID = b.customer_no AND b.primary_ind = 'Y' 

update a set a.Familiar_Name = b.esal1_desc 
from #results a 
join LVS_FAMILIAR_SALU AS b (NOLOCK) ON a.Guest_ID = b.customer_no 

update a set a.Familiar_Salu = b.lsal_desc
from #results a 
join LVS_FAMILIAR_SALU AS b (NOLOCK) ON a.Guest_ID = b.customer_no 

update a set a.PM_Initial = b.pm_initial, a.PM_Name = b.pm_name
from #results a 
join LVS_PROSPECT_MANAGER AS b (NOLOCK) ON a.main_id = b.customer_no

update a set a.Emp_PM_Initial = b.pm_initial, a.Emp_PM_Name = b.pm_name
from #results a 
join LVS_EMP_PROSPECT_MANAGER AS b (NOLOCK) ON a.Guest_ID = b.individual_customer_no

update a set a.Board = b.board
from #results a 
join LVS_CURRENT_BOARD AS b (NOLOCK) ON a.Guest_ID = b.individual_customer_no

update a set a.Washburn_Society = b.description1 + ' ' + b.description2,  a.Washburn_Society_Start_Dt = b.start_dt
from #results a 
join LVS_SOCIETY_WASHBURN AS b (NOLOCK) ON a.Reg_ID = b.customer_no

update a set a.Colby_Society = b.description1 + ' ' + b.description2 + ' ' + b.description3,  a.Colby_Society_Start_Dt = b.start_dt
from #results a 
join LVS_SOCIETY_COLBY AS b (NOLOCK) ON a.Reg_ID = b.customer_no

update a set a.Adv_Comm_Flag = case when b.individual_customer_no is not null then 'Y' else 'N' end
from #results a 
LEFT JOIN T_AFFILIATION b on a.Guest_ID = b.individual_customer_no and b.group_customer_no = 2653805 and b.affiliation_type_id = 10096

update a set a.Food_Task_Flag = case when b.individual_customer_no is not null then 'Y' else 'N' end
from #results a 
LEFT JOIN T_AFFILIATION b on a.Guest_ID = b.individual_customer_no and b.group_customer_no = 2653853 and b.affiliation_type_id = 10096

UPDATE a SET a.Discoverers_SubComm_Flag = CASE WHEN b.individual_customer_no IS NOT NULL THEN 'Y' ELSE 'N' END
FROM #results a 
LEFT JOIN T_AFFILIATION b ON a.Guest_ID = b.individual_customer_no AND b.group_customer_no = 2653841 AND b.affiliation_type_id = 10096

UPDATE a SET a.YngLdrshp_Comm_Flag = CASE WHEN b.individual_customer_no IS NOT NULL THEN 'Y' ELSE 'N' END
FROM #results a 
LEFT JOIN T_AFFILIATION b ON a.Guest_ID = b.individual_customer_no AND b.group_customer_no = 2653923 AND b.affiliation_type_id = 10096

UPDATE a SET a.MOS_Inclination = c.description
FROM #results a 
JOIN T_CUST_RESEARCH AS b (NOLOCK) ON a.Guest_ID = b.customer_no AND b.research_type IN(20,21,22,23,24) 
JOIN TR_RESEARCH_TYPE AS c (NOLOCK) ON b.research_type = c.id 

UPDATE a SET a.Capacity_Rating = c.description
FROM #results a 
JOIN T_CUST_RESEARCH AS b (NOLOCK) ON a.Reg_ID = b.customer_no
JOIN TR_RESEARCH_TYPE AS c (NOLOCK) ON b.research_type = c.id  AND b.research_type IN(8,9,10,11,12,13,14,15,16,17,18,19) 

UPDATE a SET a.Target_Gift_Range = c.description
FROM #results a 
JOIN T_CUST_RESEARCH AS b (NOLOCK) ON a.Reg_ID = b.customer_no
JOIN TR_RESEARCH_TYPE AS c (NOLOCK) ON b.research_type = c.id  AND b.research_type IN(25,26,27,28,29,30,31,32,33,34,35,36,37,38,39) 

UPDATE a SET a.Annual_Gift_Likelihood = b.research_source
FROM #results a 
JOIN T_CUST_RESEARCH AS b (NOLOCK) ON a.Reg_ID = b.customer_no AND b.research_type = 70

UPDATE a SET a.Major_Gift_Likelihood = b.research_source
FROM #results a 
JOIN T_CUST_RESEARCH AS b (NOLOCK) ON a.Reg_ID = b.customer_no AND b.research_type = 71

UPDATE a SET a.Planned_Gift_Likelihood = b.research_source
FROM #results a 
JOIN T_CUST_RESEARCH AS b (NOLOCK) ON a.Reg_ID = b.customer_no AND b.research_type = 72

UPDATE a SET a.Lifetime_Contribution = b.value
FROM #results a 
JOIN LT_NIGHTLY_SUMMARY AS b (NOLOCK) ON a.Reg_ID = b.customer_no

UPDATE a SET a.Best_Last_Mem = b.memb_level
FROM #results a 
JOIN LV_ADV_BEST_LAST_MEMBERSHIP AS b (NOLOCK) ON a.Reg_ID = b.customer_no

UPDATE a SET a.Event_Research_Briefing = b.notes
FROM #results a 
JOIN LVS_RESEARCH_NOTES_EVENTS AS b (NOLOCK) ON a.Guest_ID = b.customer_no

SELECT * FROM #results 

RETURN 

GO


