USE [impresario]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LRP_EXCEPTION_FY_TR_BATCH_PERIOD]
AS
-- TR_BATCH_PERIOD Exception reporting
-- DSJ 6/13/2016

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

-----------------------------
-- TR_BATCH_PERIOD discrepancies 
-----------------------------

SELECT id, fyear, period, quarter, start_dt, end_dt,
CASE 
	WHEN fyear IS NULL THEN 'fyear is NULL'
	WHEN period IS NULL THEN 'period is NULL'
	WHEN quarter IS NULL THEN 'quarter is NULL'
	WHEN start_dt IS NULL THEN 'start_dt is NULL'
	WHEN end_dt IS NULL THEN 'end_dt is NULL' 
	ELSE NULL 
END AS reason
FROM dbo.TR_Batch_Period
WHERE fyear IS NULL OR period IS NULL OR quarter IS NULL OR start_dt IS NULL OR end_dt IS NULL 

UNION ALL

SELECT id, fyear, period, quarter, start_dt, end_dt,
CASE 
	WHEN LEN(fyear) <> 4 THEN 'LEN(fyear) <> 4'
	WHEN CONVERT(VARCHAR(12), start_dt, 114) <> '00:00:00:000' THEN 'CONVERT(VARCHAR(12), start_dt, 114) <> ''00:00:00:000'''
	WHEN CONVERT(VARCHAR(12), end_dt, 114) <> '23:59:59:997' THEN 'CONVERT(VARCHAR(12), end_dt, 114) <> ''23:59:59:997'''
	ELSE NULL 
END AS reason
FROM dbo.TR_Batch_Period
WHERE LEN(fyear) <> 4  OR CONVERT(VARCHAR(12), start_dt, 114) <> '00:00:00:000' OR CONVERT(VARCHAR(12), end_dt, 114) <> '23:59:59:997'

UNION ALL 

SELECT id, fyear, period, quarter, start_dt, end_dt,
CASE 
	WHEN period IN (1,2,3) AND quarter <> 1 THEN 'period IN (1,2,3) AND quarter <> 1'
	WHEN period IN (4,5,6) AND quarter <> 2 THEN 'period IN (4,5,6) AND quarter <> 2'
	WHEN period IN (7,8,9) AND quarter <> 3 THEN 'period IN (7,8,9) AND quarter <> 3'
	WHEN period IN (10,11,12) AND quarter <> 4 THEN 'period IN (10,11,12) AND quarter <> 4'
	ELSE NULL 
END AS reason
FROM dbo.TR_Batch_Period
WHERE 
(period IN (1,2,3) AND quarter <> 1)
OR 
(period IN (4,5,6) AND quarter <> 2)
OR 
(period IN (7,8,9) AND quarter <> 3)
OR 
(period IN (10,11,12,13,14) AND quarter <> 4)

UNION ALL

SELECT id, fyear, period, quarter, start_dt, end_dt,
	'DAY(start_dt) <> 1' AS reason
FROM dbo.TR_Batch_Period
WHERE DAY(start_dt) <> 1

UNION ALL

SELECT id, fyear, period, quarter, start_dt, end_dt,
	CASE
		WHEN period = 1 AND MONTH(start_dt) <> 5 THEN 'period = 1 AND MONTH(start_dt) <> 5'
		WHEN period = 2 AND MONTH(start_dt) <> 6 THEN 'period = 2 AND MONTH(start_dt) <> 6'
		WHEN period = 3 AND MONTH(start_dt) <> 7 THEN 'period = 3 AND MONTH(start_dt) <> 7'
		WHEN period = 4 AND MONTH(start_dt) <> 8 THEN 'period = 4 AND MONTH(start_dt) <> 8'
		WHEN period = 5 AND MONTH(start_dt) <> 9 THEN 'period = 5 AND MONTH(start_dt) <> 9'
		WHEN period = 6 AND MONTH(start_dt) <> 10 THEN 'period = 6 AND MONTH(start_dt) <> 10'
		WHEN period = 7 AND MONTH(start_dt) <> 11 THEN 'period = 7 AND MONTH(start_dt) <> 11'
		WHEN period = 8 AND MONTH(start_dt) <> 12 THEN 'period = 8 AND MONTH(start_dt) <> 12'
		WHEN period = 9 AND MONTH(start_dt) <> 1 THEN 'period = 9 AND MONTH(start_dt) <> 1'
		WHEN period = 10 AND MONTH(start_dt) <> 2 THEN 'period = 10 AND MONTH(start_dt) <> 2'
		WHEN period = 11 AND MONTH(start_dt) <> 3 THEN 'period = 11 AND MONTH(start_dt) <> 3'
		WHEN period = 12 AND MONTH(start_dt) <> 4 THEN 'period = 12 AND MONTH(start_dt) <> 4'
		ELSE NULL
	END AS reason
FROM dbo.TR_Batch_Period
WHERE start_dt <'2005/07/01'
AND
(
	(period = 1 AND MONTH(start_dt) <> 5)
	OR (period = 2 AND MONTH(start_dt) <> 6)
	OR (period = 3 AND MONTH(start_dt) <> 7)
	OR (period = 4 AND MONTH(start_dt) <> 8)
	OR (period = 5 AND MONTH(start_dt) <> 9)
	OR (period = 6 AND MONTH(start_dt) <> 10)
	OR (period = 7 AND MONTH(start_dt) <> 11)
	OR (period = 8 AND MONTH(start_dt) <> 12)
	OR (period = 9 AND MONTH(start_dt) <> 1)
	OR (period = 10 AND MONTH(start_dt) <> 2)
	OR (period = 11 AND MONTH(start_dt) <> 3)
	OR (period = 12 AND MONTH(start_dt) <> 4)
)

UNION ALL

SELECT id, fyear, period, quarter, start_dt, end_dt,
	CASE
		WHEN period = 1 AND MONTH(start_dt) <> 7 THEN 'period = 1 AND MONTH(start_dt) <> 7'
		WHEN period = 2 AND MONTH(start_dt) <> 8 THEN 'period = 2 AND MONTH(start_dt) <> 8'
		WHEN period = 3 AND MONTH(start_dt) <> 9 THEN 'period = 3 AND MONTH(start_dt) <> 9'
		WHEN period = 4 AND MONTH(start_dt) <> 10 THEN 'period = 4 AND MONTH(start_dt) <> 10'
		WHEN period = 5 AND MONTH(start_dt) <> 11 THEN 'period = 5 AND MONTH(start_dt) <> 11'
		WHEN period = 6 AND MONTH(start_dt) <> 12 THEN 'period = 6 AND MONTH(start_dt) <> 12'
		WHEN period = 7 AND MONTH(start_dt) <> 1 THEN 'period = 7 AND MONTH(start_dt) <> 1'
		WHEN period = 8 AND MONTH(start_dt) <> 2 THEN 'period = 8 AND MONTH(start_dt) <> 2'
		WHEN period = 9 AND MONTH(start_dt) <> 3 THEN 'period = 9 AND MONTH(start_dt) <> 3'
		WHEN period = 10 AND MONTH(start_dt) <> 4 THEN 'period = 10 AND MONTH(start_dt) <> 4'
		WHEN period = 11 AND MONTH(start_dt) <> 5 THEN 'period = 11 AND MONTH(start_dt) <> 5'
		WHEN period = 12 AND MONTH(start_dt) <> 6 THEN 'period = 12 AND MONTH(start_dt) <> 6'
		ELSE NULL
	END AS reason
FROM dbo.TR_Batch_Period
WHERE start_dt >='2005/07/01'
AND
(
	(period = 1 AND MONTH(start_dt) <> 7)
	OR (period = 2 AND MONTH(start_dt) <> 8)
	OR (period = 3 AND MONTH(start_dt) <> 9)
	OR (period = 4 AND MONTH(start_dt) <> 10)
	OR (period = 5 AND MONTH(start_dt) <> 11)
	OR (period = 6 AND MONTH(start_dt) <> 12)
	OR (period = 7 AND MONTH(start_dt) <> 1)
	OR (period = 8 AND MONTH(start_dt) <> 2)
	OR (period = 9 AND MONTH(start_dt) <> 3)
	OR (period = 10 AND MONTH(start_dt) <> 4)
	OR (period = 11 AND MONTH(start_dt) <> 5)
	OR (period = 12 AND MONTH(start_dt) <> 6)
)

UNION ALL

SELECT id, fyear, period, quarter, start_dt, end_dt,
CASE 
	WHEN MONTH(end_dt) = 1 AND DAY(end_dt) <> 31 THEN 'MONTH(end_dt) = 1 AND DAY(end_dt) <> 31'
	WHEN MONTH(end_dt) = 2 AND 
		( (YEAR(end_dt) % 4 = 0 AND YEAR(end_dt) % 100 <> 0) OR YEAR(end_dt) % 400 = 0)
		AND DAY(end_dt) <> 29 THEN 'MONTH(end_dt) = 2 AND 
		( (YEAR(end_dt) % 4 = 0 AND YEAR(end_dt) % 100 <> 0) OR YEAR(end_dt) % 400 = 0)
		AND DAY(end_dt) <> 29'
	WHEN MONTH(end_dt) = 2 AND 
		NOT ( (YEAR(end_dt) % 4 = 0 AND YEAR(end_dt) % 100 <> 0) OR YEAR(end_dt) % 400 = 0)
		AND DAY(end_dt) <> 28 THEN 'MONTH(end_dt) = 2 AND 
		NOT ( (YEAR(end_dt) % 4 = 0 AND YEAR(end_dt) % 100 <> 0) OR YEAR(end_dt) % 400 = 0)
		AND DAY(end_dt) <> 28'
	WHEN MONTH(end_dt) = 3 AND DAY(end_dt) <> 31 THEN 'MONTH(end_dt) = 3 AND DAY(end_dt) <> 31'
	WHEN MONTH(end_dt) = 4 AND DAY(end_dt) <> 30 THEN 'MONTH(end_dt) = 4 AND DAY(end_dt) <> 30'
	WHEN MONTH(end_dt) = 5 AND DAY(end_dt) <> 31 THEN 'MONTH(end_dt) = 5 AND DAY(end_dt) <> 31'
	WHEN MONTH(end_dt) = 6 AND DAY(end_dt) <> 30 THEN 'MONTH(end_dt) = 6 AND DAY(end_dt) <> 30'
	WHEN MONTH(end_dt) = 7 AND DAY(end_dt) <> 31 THEN 'MONTH(end_dt) = 7 AND DAY(end_dt) <> 31'
	WHEN MONTH(end_dt) = 8 AND DAY(end_dt) <> 31 THEN 'MONTH(end_dt) = 8 AND DAY(end_dt) <> 31'
	WHEN MONTH(end_dt) = 9 AND DAY(end_dt) <> 30 THEN 'MONTH(end_dt) = 9 AND DAY(end_dt) <> 30' 
	WHEN MONTH(end_dt) = 10 AND DAY(end_dt) <> 31 THEN 'MONTH(end_dt) = 10 AND DAY(end_dt) <> 31'
	WHEN MONTH(end_dt) = 11 AND DAY(end_dt) <> 30 THEN 'MONTH(end_dt) = 11 AND DAY(end_dt) <> 30'
	WHEN MONTH(end_dt) = 12 AND DAY(end_dt) <> 31 THEN 'MONTH(end_dt) = 12 AND DAY(end_dt) <> 31' 
	ELSE NULL
	END AS reason
FROM dbo.TR_Batch_Period
WHERE 
(
	(MONTH(end_dt) = 1 AND DAY(end_dt) <> 31)
	OR (MONTH(end_dt) = 2 AND 
		( (YEAR(end_dt) % 4 = 0 AND YEAR(end_dt) % 100 <> 0) OR YEAR(end_dt) % 400 = 0)
		AND DAY(end_dt) <> 29)
	OR (MONTH(end_dt) = 2 AND 
		NOT ( (YEAR(end_dt) % 4 = 0 AND YEAR(end_dt) % 100 <> 0) OR YEAR(end_dt) % 400 = 0)
		AND DAY(end_dt) <> 28)
	OR (MONTH(end_dt) = 3 AND DAY(end_dt) <> 31)
	OR (MONTH(end_dt) = 4 AND DAY(end_dt) <> 30)
	OR (MONTH(end_dt) = 5 AND DAY(end_dt) <> 31)
	OR (MONTH(end_dt) = 6 AND DAY(end_dt) <> 30)
	OR (MONTH(end_dt) = 7 AND DAY(end_dt) <> 31)
	OR (MONTH(end_dt) = 8 AND DAY(end_dt) <> 31)
	OR (MONTH(end_dt) = 9 AND DAY(end_dt) <> 30)
	OR (MONTH(end_dt) = 10 AND DAY(end_dt) <> 31)
	OR (MONTH(end_dt) = 11 AND DAY(end_dt) <> 30)
	OR (MONTH(end_dt) = 12 AND DAY(end_dt) <> 31)
)

UNION ALL 

SELECT id, fyear, period, quarter, start_dt, end_dt,
	CASE
		WHEN period = 1 AND MONTH(end_dt) <> 5 THEN 'period = 1 AND MONTH(end_dt) <> 5'
		WHEN period = 2 AND MONTH(end_dt) <> 6 THEN 'period = 2 AND MONTH(end_dt) <> 6'
		WHEN period = 3 AND MONTH(end_dt) <> 7 THEN 'period = 3 AND MONTH(end_dt) <> 7'
		WHEN period = 4 AND MONTH(end_dt) <> 8 THEN 'period = 4 AND MONTH(end_dt) <> 8'
		WHEN period = 5 AND MONTH(end_dt) <> 9 THEN 'period = 5 AND MONTH(end_dt) <> 9'
		WHEN period = 6 AND MONTH(end_dt) <> 10 THEN 'period = 6 AND MONTH(end_dt) <> 10'
		WHEN period = 7 AND MONTH(end_dt) <> 11 THEN 'period = 7 AND MONTH(end_dt) <> 11'
		WHEN period = 8 AND MONTH(end_dt) <> 12 THEN 'period = 8 AND MONTH(end_dt) <> 12'
		WHEN period = 9 AND MONTH(end_dt) <> 1 THEN 'period = 9 AND MONTH(end_dt) <> 1'
		WHEN period = 10 AND MONTH(end_dt) <> 2 THEN 'period = 10 AND MONTH(end_dt) <> 2'
		WHEN period = 11 AND MONTH(end_dt) <> 3 THEN 'period = 11 AND MONTH(end_dt) <> 3'
		WHEN period = 12 AND MONTH(end_dt) <> 4 THEN 'period = 12 AND MONTH(end_dt) <> 4'
		ELSE NULL
	END AS reason
FROM dbo.TR_Batch_Period
WHERE start_dt <'2005/07/01'
AND
(
	(period = 1 AND MONTH(end_dt) <> 5)
	OR (period = 2 AND MONTH(end_dt) <> 6)
	OR (period = 3 AND MONTH(end_dt) <> 7)
	OR (period = 4 AND MONTH(end_dt) <> 8)
	OR (period = 5 AND MONTH(end_dt) <> 9)
	OR (period = 6 AND MONTH(end_dt) <> 10)
	OR (period = 7 AND MONTH(end_dt) <> 11)
	OR (period = 8 AND MONTH(end_dt) <> 12)
	OR (period = 9 AND MONTH(end_dt) <> 1)
	OR (period = 10 AND MONTH(end_dt) <> 2)
	OR (period = 11 AND MONTH(end_dt) <> 3)
	OR (period = 12 AND MONTH(end_dt) <> 4)
)

UNION ALL 

SELECT id, fyear, period, quarter, start_dt, end_dt,
	CASE
		WHEN period = 1 AND MONTH(end_dt) <> 7 THEN 'period = 1 AND MONTH(end_dt) <> 7'
		WHEN period = 2 AND MONTH(end_dt) <> 8 THEN 'period = 2 AND MONTH(end_dt) <> 8'
		WHEN period = 3 AND MONTH(end_dt) <> 9 THEN 'period = 3 AND MONTH(end_dt) <> 9'
		WHEN period = 4 AND MONTH(end_dt) <> 10 THEN 'period = 4 AND MONTH(end_dt) <> 10'
		WHEN period = 5 AND MONTH(end_dt) <> 11 THEN 'period = 5 AND MONTH(end_dt) <> 11'
		WHEN period = 6 AND MONTH(end_dt) <> 12 THEN 'period = 6 AND MONTH(end_dt) <> 12'
		WHEN period = 7 AND MONTH(end_dt) <> 1 THEN 'period = 7 AND MONTH(end_dt) <> 1'
		WHEN period = 8 AND MONTH(end_dt) <> 2 THEN 'period = 8 AND MONTH(end_dt) <> 2'
		WHEN period = 9 AND MONTH(end_dt) <> 3 THEN 'period = 9 AND MONTH(end_dt) <> 3'
		WHEN period = 10 AND MONTH(end_dt) <> 4 THEN 'period = 10 AND MONTH(end_dt) <> 4'
		WHEN period = 11 AND MONTH(end_dt) <> 5 THEN 'period = 11 AND MONTH(end_dt) <> 5'
		WHEN period = 12 AND MONTH(end_dt) <> 6 THEN 'period = 12 AND MONTH(end_dt) <> 6'
		ELSE NULL
	END AS reason
FROM dbo.TR_Batch_Period
WHERE start_dt >='2005/07/01'
AND
(
	(period = 1 AND MONTH(end_dt) <> 7)
	OR (period = 2 AND MONTH(end_dt) <> 8)
	OR (period = 3 AND MONTH(end_dt) <> 9)
	OR (period = 4 AND MONTH(end_dt) <> 10)
	OR (period = 5 AND MONTH(end_dt) <> 11)
	OR (period = 6 AND MONTH(end_dt) <> 12)
	OR (period = 7 AND MONTH(end_dt) <> 1)
	OR (period = 8 AND MONTH(end_dt) <> 2)
	OR (period = 9 AND MONTH(end_dt) <> 3)
	OR (period = 10 AND MONTH(end_dt) <> 4)
	OR (period = 11 AND MONTH(end_dt) <> 5)
	OR (period = 12 AND MONTH(end_dt) <> 6)
)

UNION ALL

SELECT id, fyear, period, quarter, start_dt, end_dt,
	CASE
		WHEN period BETWEEN 1 AND 8 AND YEAR(start_dt)+ 1 <> fyear THEN 'period BETWEEN 1 AND 8 AND YEAR(start_dt)+ 1 <> fyear'
        WHEN period BETWEEN 9 AND 14 AND YEAR(start_dt) <> fyear THEN 'period BETWEEN 9 AND 14 AND YEAR(start_dt) <> fyear'
	ELSE NULL
	END AS reason
FROM dbo.TR_Batch_Period
WHERE start_dt <'2005/07/01'
AND 
(
	period BETWEEN 1 AND 8 AND YEAR(start_dt)+ 1 <> fyear
	OR period BETWEEN 9 AND 14 AND YEAR(start_dt) <> fyear
)

UNION ALL 

SELECT id, fyear, period, quarter, start_dt, end_dt,
	CASE
		WHEN period BETWEEN 1 AND 6 AND YEAR(start_dt)+ 1 <> fyear THEN 'period BETWEEN 1 AND 6 AND YEAR(start_dt)+ 1 <> fyear'
        WHEN period BETWEEN 7 AND 12 AND YEAR(start_dt) <> fyear THEN 'period BETWEEN 7 AND 12 AND YEAR(start_dt) <> fyear'
	ELSE NULL
	END AS reason
FROM dbo.TR_Batch_Period
WHERE start_dt >= '2005/07/01'
AND 
(
	period BETWEEN 1 AND 6 AND YEAR(start_dt)+ 1 <> fyear
	OR period BETWEEN 7 AND 12 AND YEAR(start_dt) <> fyear
)