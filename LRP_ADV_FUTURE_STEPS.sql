USE impresario
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_ADV_FUTURE_STEPS]') AND type IN (N'P', N'PC'))
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_ADV_FUTURE_STEPS] AS' 
GO

EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LRP_ADV_FUTURE_STEPS] TO [impusers], [tessitura_app]'
GO


ALTER PROCEDURE [dbo].[LRP_ADV_FUTURE_STEPS]
        @fiscal_year INT = NULL,
        @numOfDays INT = NULL,
        @workers_str VARCHAR(4000) = NULL
AS BEGIN

    SET NOCOUNT ON;
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

     /*  Procedure Variables  */

        DECLARE @step_start_dt DATETIME = NULL
        DECLARE @step_end_dt DATETIME = NULL
        DECLARE @fiscal_start_dt DATETIME = NULL
        DECLARE @fiscal_end_dt DATETIME = NULL 
        DECLARE @current_dt DATETIME = GETDATE()
        DECLARE @worker_no INT = 0
        DECLARE @outside_fiscal_year INT = 0

        DECLARE @abbrev_chars INT = 300

    /*  Check Parameters  */

        IF @fiscal_year IS NULL SELECT @fiscal_year = dbo.LF_GetFiscalYear(GETDATE())
        
        SELECT @fiscal_start_dt = [dbo].[LF_GetFiscalYearStartDt](@fiscal_year)

        SELECT @fiscal_end_dt = [dbo].[LF_GetFiscalYearEndDt](@fiscal_year)

        SELECT @numOfDays = ISNULL(@numOfDays, 15)
        
        SELECT @step_start_dt = CAST(DATEADD(DAY, 1, GETDATE()) AS DATE),
               @step_end_dt = FORMAT(DATEADD(DAY,15,GETDATE()),'yyyy/MM/dd') + ' 23:59:59.957'
    
        IF @step_start_dt NOT BETWEEN @fiscal_start_dt AND @fiscal_end_dt SELECT @outside_fiscal_year = 1
        IF @step_end_dt NOT BETWEEN @fiscal_start_dt AND @fiscal_end_dt SELECT @outside_fiscal_year = 1

        SELECT @workers_str = ISNULL(@workers_str, 0)

        SELECT @worker_no = TRY_CONVERT(INT, REPLACE(@workers_str,'"',''))
        SELECT @worker_no = ISNULL(@worker_no, 0)


        SELECT p.[customer_no], 
               cust.[display_name], 
               cust.[sort_name], 
               cam.[description] AS [campaign_desc], 
               cdes.[description] AS [cont_designation_desc],
               stype.[description] AS [step_type_desc], 
               s.[step_dt], 
               s.[description] AS [step_desc], 
               --LEFT(replace(ISNULL(s.[notes],''), CHAR(13) + CHAR(10), ''), @abbrev_chars) AS [step_notes],
               REPLACE(ISNULL(s.[notes],''), CHAR(13) + CHAR(10), '') AS [step_notes],
               s.[worker_customer_no], 
               wkr.[display_name] AS [worker_display_name],
               ISNULL(@outside_fiscal_year, 0) AS [outside_fiscal_year]
            FROM [dbo].[T_PLAN] AS p
                 INNER JOIN [dbo].[T_STEP] AS s ON s.[plan_no] = p.[plan_no]
                 INNER JOIN [dbo].[TR_STEP_TYPE] AS stype ON stype.[id] = s.[step_type]
                 INNER JOIN [dbo].[T_CAMPAIGN] AS cam ON cam.[campaign_no] = p.[campaign_no]
                 --INNER JOIN @camp_categories AS ccat ON ccat.[id] = cam.[category]
                 INNER JOIN [dbo].[TR_CONT_DESIGNATION] AS cdes ON cdes.[id] = p.[cont_designation]
                 INNER JOIN [dbo].[FT_CONSTITUENT_DISPLAY_NAME]() AS cust ON p.[customer_no] = cust.[customer_no]
                 LEFT OUTER JOIN [dbo].[FT_CONSTITUENT_DISPLAY_NAME]() AS wkr ON s.[worker_customer_no] = wkr.[customer_no]
            WHERE s.[step_dt] BETWEEN @step_start_dt AND @step_end_dt
              AND p.[type] = 7 -- Advancement Pipeline
              AND ISNULL(s.[worker_customer_no],0) = @worker_no
            --AND DATEDIFF(d, GETDATE(), s.step_dt) BETWEEN 1 AND @numOfDays -- The step date <> today�s date. 


END
GO

EXECUTE [dbo].[LRP_ADV_FUTURE_STEPS] @fiscal_year = 2021, @numOfDays = 15, @workers_str = '3747019'

    