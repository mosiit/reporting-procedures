USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_ORDER_STATISTICS]') AND type in (N'P', N'PC')) BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_ORDER_STATISTICS] AS' 
END
GO

ALTER PROCEDURE [dbo].[LRP_ORDER_STATISTICS]
        @perf_start_dt_1 DATETIME = NULL,
        @perf_end_dt_1 DATETIME = NULL,
        @order_start_dt_1 DATETIME = NULL,
        @order_end_dt_1 DATETIME = NULL,
        @perf_start_dt_2 DATETIME = NULL,
        @perf_end_dt_2 DATETIME = NULL,
        @order_start_dt_2 DATETIME = NULL,
        @order_end_dt_2 DATETIME = NULL,
        @title_str VARCHAR(4000) = '',
        @mode_of_sale_str VARCHAR(4000) = '',
        @column_detail VARCHAR(30) = 'Anonymous/Known', --Anonymous/Known or Member/NonMember
        @show_mode_of_sale CHAR(1) = 'Y',
        @show_production CHAR(1) = 'Y'
AS BEGIN

        SET NOCOUNT ON;
        SET ANSI_WARNINGS OFF;
        
    /*  Procedure Variables  */      

        
        DECLARE @final_table TABLE ([date_range] VARCHAR(100), [customer_name] VARCHAR(100), [order_dt] DATETIME, [performance_dt] DATETIME, 
                                    [order_mode_of_sale] INT, [order_mode_of_sale_name] VARCHAR(30), [title_name] VARCHAR(30), [production_name] VARCHAR(30), 
                                    [order_count] INT, [ticket_count] INT, [member_nonmember] VARCHAR(15), [total_order_count] INT, [total_ticket_count] INT,
                                    [report_message] VARCHAR(100));

        DECLARE @title_list TABLE ([title_no] INT, [title_name] VARCHAR(30));

        DECLARE @mos_list TABLE ([mos_no] INT);

        DECLARE @date_range VARCHAR(10) = 'First', @rpt_message VARCHAR(100) = ''
        
        DECLARE @dates_1 VARCHAR(100) = '', @dates_2 VARCHAR(100) =  ''

        DECLARE @pStart DATETIME, @pEnd DATETIME, @oStart DATETIME, @oEnd DATETIME

        DECLARE @first_order_count INT = 0, @second_order_count INT = 0
        DECLARE @first_ticket_count INT = 0, @second_ticket_count INT = 0

        IF OBJECT_ID('tempdb..#performance_cache') IS NOT NULL DROP TABLE [#performance_cache]

        CREATE TABLE [#performance_cache] ([date_range] VARCHAR(100), [perf_no] INT, [zone_no] INT, [performance_dt] DATETIME, [performance_date] CHAR(10), 
                                           [performance_time] VARCHAR(8), [title_no] INT, [title_name] VARCHAR(30), [production_name] VARCHAR(30));
        

        IF OBJECT_ID('tempdb..#order_list') IS NOT NULL DROP TABLE [#order_list]

        CREATE TABLE [#order_list] ([date_range] VARCHAR(100), [order_no] INT, [order_dt] DATETIME, [order_source] INT, [order_source_name] VARCHAR(100), 
                                    [order_channel] INT, [order_channel_name] VARCHAR(30), [order_mode_of_sale] INT, [order_mode_of_sale_name] VARCHAR(30), 
                                    [customer_no] INT, [customer_name] VARCHAR(150), [is_anonymous] CHAR(1));

        CREATE CLUSTERED INDEX [uq_order_list_order_no] ON [#order_list] ([order_no] ASC);
        CREATE NONCLUSTERED INDEX [ix_order_list_order_mode_of_sale] ON [#order_list] ([order_mode_of_sale] ASC);
        CREATE NONCLUSTERED INDEX [ix_order_list_order_source_name] ON [#order_list] ([order_source_name] ASC);
        CREATE NONCLUSTERED INDEX [ix_order_list_order_channel_name] ON [#order_list] ([order_channel_name] ASC);

    /*  Check Parameters  */

        --If either @perf_start_dt_1 or @perf_end_dt_1 is set to Null, make sure they are both set to Null

        IF @perf_start_dt_1 IS NULL OR @perf_end_dt_1 IS NULL
            SELECT @perf_start_dt_1 = NULL, @perf_end_dt_1 = NULL;

        --If either @order_start_dt_1 or @order_end_dt_1 is set to Null, make sure they are both set to Null

        IF @order_start_dt_1 IS NULL OR @order_end_dt_1 IS NULL
            SELECT @order_start_dt_1 = NULL, @order_end_dt_1 = NULL;

        --If either @perf_start_dt_2 or @perf_end_dt_2 is set to Null, make sure they are both set to Null

        IF @perf_start_dt_2 IS NULL OR @perf_end_dt_2 IS NULL
            SELECT @perf_start_dt_2 = NULL, @perf_end_dt_2 = NULL;

        --If either @order_start_dt_2 or @order_end_dt_2 is set to Null, make sure they are both set to Null

        IF @order_start_dt_2 IS NULL OR @order_end_dt_2 IS NULL
            SELECT @order_start_dt_2 = NULL, @order_end_dt_2 = NULL;

        --Make sure start dates start at the beginning of the day and end dates end at the end of the day

        IF @perf_start_dt_1 IS NOT NULL SELECT @perf_start_dt_1 = CONVERT(CHAR(10),@perf_start_dt_1,111) + ' 00:00:00.000'
        IF @perf_end_dt_1 IS NOT NULL SELECT @perf_end_dt_1 = CONVERT(CHAR(10),@perf_end_dt_1,111) + ' 23:59:59.957'
        IF @perf_start_dt_2 IS NOT NULL SELECT @perf_start_dt_2 = CONVERT(CHAR(10),@perf_start_dt_2,111) + ' 00:00:00.000'
        IF @perf_end_dt_2 IS NOT NULL SELECT @perf_end_dt_2 = CONVERT(CHAR(10),@perf_end_dt_2,111) + ' 23:59:59.957'

        --If all #1 date variables are null, go to the end
        
        IF @perf_start_dt_1 IS NULL AND @perf_end_dt_1 IS NULL AND @order_start_dt_1 IS NULL AND @order_end_dt_1 IS NULL BEGIN
            SELECT @rpt_message = 'Invalid Dates';
            GOTO FINISHED;
        END;

        --Create Date Range Strings to display on report

        IF @perf_start_dt_1 IS NOT NULL AND @perf_end_dt_1 IS NOT NULL
            SELECT @dates_1 = ' Perfs: ' + CONVERT(VARCHAR(10),@perf_start_dt_1,101) + ' to ' + CONVERT(VARCHAR(10),@perf_end_dt_1,101)
        IF @order_start_dt_1 IS NOT NULL AND @order_end_dt_1 IS NOT NULL 
            SELECT @dates_1 = @dates_1 + CHAR(10) + 'orders: ' + CONVERT(VARCHAR(10),@order_start_dt_1,101) + ' to ' + CONVERT(VARCHAR(10),@order_end_dt_1,101)

        IF @perf_start_dt_2 IS NOT NULL AND @perf_end_dt_2 IS NOT NULL
            SELECT @dates_2 = ' Perfs: ' + CONVERT(VARCHAR(10),@perf_start_dt_2,101) + ' to ' + CONVERT(VARCHAR(10),@perf_end_dt_2,101)
        IF @order_start_dt_2 IS NOT NULL AND @order_end_dt_2 IS NOT NULL 
            SELECT @dates_2 = @dates_2 + CHAR(10) + 'orders: ' + CONVERT(VARCHAR(10),@order_start_dt_2,101) + ' to ' + CONVERT(VARCHAR(10),@order_end_dt_2,101)

        --Check and parse list of titles and modes of sale (if any)
        IF ISNULL(@title_str,'') <> ''
            INSERT INTO @title_list ([title_no], [title_name]) 
                SELECT CAST(lst.[Element] AS INTEGER), inv.[description]
                FROM dbo.FT_SPLIT_LIST(@title_str,',') AS lst
                     INNER JOIN [dbo].[T_INVENTORY] AS inv ON inv.inv_no = CAST(lst.Element AS INTEGER)

        IF ISNULL(@mode_of_sale_str,'') <> ''
            INSERT INTO @mos_list ([mos_no])
                SELECT CAST(Element AS INTEGER) 
                FROM dbo.FT_SPLIT_LIST(@mode_of_sale_str,',')


        --Additional Parameters

        SELECT @column_detail = ISNULL(@column_detail,'Member/NonMember')
        
    /*  Passing performance Dates requires more work, so that is checked first.  All attendance generating performances will be cached,
        If titles are passed to the report, those will filtered out of the performance cache.  A list of orders is generated
        using subline items that contain any of the performances in the performance cache.  If order dates are also passed to
        the report anything with an order date outside of that range is ommitted.  The gathering data process will run twice if
        two sets of dates are passed to the procedure.   */

        --Process First dates
        SELECT @pStart = @perf_start_dt_1, @pEnd = @perf_end_dt_1
        SELECT @oStart = @order_start_dt_1, @oEnd = @order_end_dt_1

    BEGIN_DATA_GATHER:
    
         /*  Create a cache table of all the performances needed for the report  */
        
         IF @pStart IS NOT NULL AND @pEnd IS NOT NULL

            INSERT INTO #performance_cache ([date_range],[perf_no], [zone_no], [performance_dt], [performance_date], [performance_time], 
                                            [title_no], [title_name], [production_name])
            SELECT @date_range, 
                   prf.[performance_no], 
                   prf.[performance_zone], 
                   prf.[performance_dt], 
                   prf.[performance_date], 
                   prf.[performance_time], 
                   prf.[title_no], 
                   prf.[title_name], 
                   prf.[production_name]
            FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] prf
                 INNER JOIN @title_list AS ttl ON ttl.[title_no] = prf.[title_no]
            WHERE prf.[performance_dt] BETWEEN @pstart AND @pEnd
              AND prf.[performance_no] NOT IN (SELECT [perf_no] FROM [#performance_cache]);

         IF @oStart IS NOT NULL AND @oEnd IS NOT NULL

            INSERT INTO #performance_cache ([date_range], [perf_no], [zone_no], [performance_dt], [performance_date], [performance_time], 
                                            [title_no], [title_name], [production_name])
            SELECT @date_range, 
                   prf.[performance_no], 
                   prf.[performance_zone], 
                   prf.[performance_dt], 
                   prf.[performance_date], 
                   prf.[performance_time], 
                   prf.[title_no], 
                   prf.[title_name], 
                   prf.[production_name]
            FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] AS prf
                 INNER JOIN @title_list AS ttl ON ttl.[title_no] = prf.[title_no]
            WHERE prf.[performance_no] IN (SELECT [perf_no] 
                                           FROM [dbo].[T_SUB_LINEITEM] (NOLOCK) 
                                           WHERE order_no IN (SELECT [order_no] 
                                                              FROM [dbo].[T_ORDER] 
                                                              WHERE [order_dt] BETWEEN @order_start_dt_1 AND @order_end_dt_1))
              AND prf.[performance_no] NOT IN (SELECT [perf_no] FROM #performance_cache);

        /*  Get the orders that will be processed  */
         
            IF @pStart IS NOT NULL AND @pEnd IS NOT NULL BEGIN

                /*  Get order list based on performance date range  */

                    INSERT INTO [#order_list] ([date_range], [order_no], [order_dt], [order_source], [order_source_name], [order_channel], [order_channel_name], 
                                               [order_mode_of_sale], [order_mode_of_sale_name], [customer_no], [customer_name], [is_anonymous])
                    SELECT @date_range, ord.[order_no], ord.[order_dt], ord.[source_no], sou.[source_name], ord.[channel], cha.[description], ord.[MOS], mos.[description],
                           ord.[customer_no], LTRIM(ISNULL(cus.[fname],'') + ' ' + ISNULL(cus.lname,'')), 'N'
                    FROM [dbo].[T_ORDER] AS ord (NOLOCK)
                         LEFT OUTER JOIN [dbo].[TR_SALES_CHANNEL] AS cha (NOLOCK) ON cha.[id] = ord.[channel]
                         LEFT OUTER JOIN [dbo].[TX_APPEAL_MEDIA_TYPE] AS sou (NOLOCK) ON sou.[source_no] = ord.[source_no]
                         LEFT OUTER JOIN [dbo].[TR_MOS] AS mos (NOLOCK) ON mos.[id] = ord.[MOS]
                         LEFT OUTER JOIN [dbo].[T_CUSTOMER] AS cus (NOLOCK) ON cus.[customer_no] = ord.[customer_no]
                    WHERE ord.[order_no] IN (SELECT [order_no] FROM [dbo].[T_SUB_LINEITEM] WHERE [perf_no] IN (SELECT [performance_no] FROM dbo.LV_PRODUCTION_ELEMENTS_PERFORMANCE WHERE performance_dt BETWEEN @pStart AND @pEnd)); 
                    
                    --(SELECT [perf_no] FROM #performance_cache WHERE performance_dt BETWEEN @pStart AND @pEnd));

                /*  If both dates are used, delete the orders not within the order date range  */

                    IF @order_start_dt_1 IS NOT NULL AND @order_end_dt_1 IS NOT NULL
                        DELETE FROM [#order_list]
                            WHERE [order_dt] NOT BETWEEN @order_start_dt_1 AND @order_end_dt_1;
                        
            END ELSE BEGIN
        
                /*  Get order list based on order date range */

                   INSERT INTO [#order_list] ([date_range], [order_no], [order_dt], [order_source], [order_source_name], [order_channel], [order_channel_name], 
                                              [order_mode_of_sale], [order_mode_of_sale_name], [customer_no], [customer_name], [is_anonymous])
                    SELECT @date_range, ord.[order_no], ord.[order_dt], ord.[source_no], sou.[source_name], ord.[channel], cha.[description], ord.[MOS], mos.[description],
                           ord.[customer_no], LTRIM(ISNULL(cus.[fname],'') + ' ' + ISNULL(cus.lname,'')), 'N'
                    FROM [dbo].[T_ORDER] AS ord (NOLOCK)
                         LEFT OUTER JOIN [dbo].[TR_SALES_CHANNEL] AS cha (NOLOCK) ON cha.[id] = ord.[channel]
                         LEFT OUTER JOIN [dbo].[TX_APPEAL_MEDIA_TYPE] AS sou (NOLOCK) ON sou.[source_no] = ord.[source_no]
                         LEFT OUTER JOIN [dbo].[TR_MOS] AS mos (NOLOCK) ON mos.[id] = ord.[MOS]
                         LEFT OUTER JOIN [dbo].[T_CUSTOMER] AS cus (NOLOCK) ON cus.[customer_no] = ord.[customer_no]
                         LEFT OUTER JOIN [dbo].[T_EADDRESS] AS eml (NOLOCK) ON eml.[customer_no] = ord.[customer_no] AND eml.[inactive] = 'N' AND eml.[eaddress_type] = 1 
                    WHERE ord.[order_dt] BETWEEN @order_start_dt_1 AND @order_end_dt_1;

            END;

        /*  If two date ranges are used, change the start and end date and return to the start of the data gathering  */

            IF (@date_range = 'First' AND (@perf_start_dt_2 IS NOT NULL OR @order_start_dt_2 IS NOT NULL)) BEGIN

                SELECT @first_order_count = COUNT(DISTINCT [order_no]) FROM [#order_list] WHERE [date_range] = 'First'
               
                SELECT @date_range =  'Second';

                SELECT @pStart = @perf_start_dt_2,
                       @pEnd = @perf_end_dt_2,
                       @oStart = @order_start_dt_2,
                       @oEnd = @order_end_dt_2;

                GOTO BEGIN_DATA_GATHER;

            END;
            
                    /*****  THIS IS AN ATTEMPT TO PARTIALLY MITIGATE BAD DATA ENTRY.
                            SINCE ANY ORDER TAKEN AT THE BOX OFFICE BY DEFAULT IS IN PERSON,
                            ANY ORDER WITH THE BOX OFFICE SOURCE IS CHANGED TO HAVE THE
                            IN PERSON CHANNEL. *****/
    
                            UPDATE [#order_list]
                            SET [order_channel_name] = 'In Person'
                            WHERE [order_source_name] = 'Box Office' AND [order_channel_name] <> 'In Person'

        /*  If limited to specific modes of sale, delete all other modes of sale  */

            IF EXISTS (SELECT * FROM @mos_list)
                DELETE FROM [#order_list] WHERE [order_mode_of_sale] NOT IN (SELECT [mos_no] FROM @mos_list)

        /*  If the customer number is 0 or the customer name is either Guest User or Kiosk Customer, then the order is 
            anonymous and not for a specific person.  Otherwise, it is an order for a known user  */

            UPDATE [#order_list] SET [customer_name] = 'Anonymous', [is_anonymous] = 'Y' WHERE [customer_no] = 0
            UPDATE [#order_list] SET [customer_name] = 'Anonymous', [is_anonymous] = 'Y' WHERE [customer_name] IN ('Guest User','Kiosk Customer')
            UPDATE [#order_list] SET [customer_name] = 'Known' WHERE [is_anonymous] = 'N'
       

        /*  Generate final aggregated data set  */
                   
            INSERT INTO @final_table([date_range], [customer_name], [performance_dt], [order_mode_of_sale], [order_mode_of_sale_name], [title_name], 
                                     [production_name], [order_count], [ticket_count], [member_nonmember], [total_order_count], [total_ticket_count], [report_message])
            SELECT ord.[date_range], ord.[customer_name], prf.[performance_dt], [ord].[order_mode_of_sale], ord.[order_mode_of_sale_name], prf.[title_name], prf.[production_name],
                   COUNT(DISTINCT ord.[order_no]), COUNT(DISTINCT sli.[sli_no]), CASE WHEN ISNULL(MAX(mem.cust_memb_no),0) > 0 THEN 'Member' ELSE 'Non-Member' END, 0, 0,  ''
            FROM [#order_list] AS ord (NOLOCK)
                 INNER JOIN [dbo].[T_SUB_LINEITEM] AS sli (NOLOCK) ON sli.[order_no] = ord.[order_no]
                 INNER JOIN #performance_cache AS prf ON prf.[perf_no] = sli.[perf_no] AND prf.[zone_no] = sli.[zone_no]
                 LEFT OUTER JOIN [dbo].[TX_CUST_MEMBERSHIP] AS mem (NOLOCK) ON mem.[customer_no] = ord.[customer_no] AND prf.[performance_dt] BETWEEN mem.init_dt AND mem.expr_dt
                                                                           AND mem.[current_status] NOT IN (5, 6, 8, 9)
            WHERE sli.[order_no] IN (SELECT [order_no] FROM [#order_list])
              AND sli.[sli_status] IN (3, 12)
            GROUP BY ord.[date_range], ord.[customer_name], prf.[performance_dt], ord.[order_mode_of_sale], ord.[order_mode_of_sale_name], prf.[title_name], prf.[production_name]

        /*  If there is no data in the final table, jump to the end  */

            IF NOT EXISTS (SELECT * FROM @final_table) GOTO FINISHED

        /*  If Anonymous/Known breakdown not wanted, reset customer name field  */

            IF @column_detail <> 'Anonymous/Known'
                UPDATE @final_table SET [customer_name] = [member_nonmember]

            IF @show_mode_of_sale = 'N'
                UPDATE @final_table SET [order_mode_of_sale_name] = 'All Modes of Sale'

            IF @show_production = 'N'
                UPDATE @final_table SET [production_name] = [title_name]

        /*  Set Totals */

            SELECT @first_order_count = ISNULL(SUM([order_count]),0) FROM @final_table WHERE [date_range] = 'First'
            SELECT @second_order_count = ISNULL(SUM([order_count]),0) FROM @final_table WHERE [date_range] = 'Second'

            SELECT @first_ticket_count = ISNULL(SUM([ticket_count]),0) FROM @final_table WHERE [date_range] = 'First'
            SELECT @second_ticket_count = ISNULL(SUM([ticket_count]),0) FROM @final_table WHERE [date_range] = 'Second'

            UPDATE @final_table 
            SET [total_order_count] = @first_order_count, [total_ticket_count] = @first_ticket_count
            WHERE [date_range] = 'First';

            UPDATE @final_table 
            SET [total_order_count] = @second_order_count, [total_ticket_count] = @second_ticket_count
            WHERE [date_range] = 'Second'

        /*  If specific titles were past, delete all the others
            Note: this is done at the end so that the percentages are based on all orders and not just orders for the selected titles  */

            IF EXISTS (SELECT * FROM @title_list)
                DELETE FROM @final_table WHERE [title_name] NOT IN (SELECT [title_name] FROM @title_list);
                --DELETE FROM @final_table WHERE [title_name] NOT IN (SELECT [title_name] FROM [dbo].[LV_SS_PRODUCTION_ELEMENTS_PERFORMANCE] (NOLOCK) WHERE [title_no] in(SELECT [title_no] FROM @title_table));
          
    FINISHED:

            /*  Reset Date Range Labels  */

            IF @dates_1 <> '' UPDATE @final_table SET [date_range] = @dates_1 WHERE [date_range] = 'First'
            IF @dates_2 <> '' UPDATE @final_table SET [date_range] = @dates_2 WHERE [date_range] = 'Second'


        /*  If nothing in the final data table, add a single record with a "no records found" message  */

            IF NOT EXISTS (SELECT * FROM @final_table) BEGIN
                IF @rpt_message = '' SELECT @rpt_message = 'No data found for the criteria entered.'
                INSERT INTO @final_table ([date_range], [customer_name], [order_dt], [performance_dt], [order_mode_of_sale], [order_mode_of_sale_name], [title_name], 
                                          [production_name], [order_count], [ticket_count], [total_order_count], [total_ticket_count], [report_message])
                VALUES ('', '', NULL, NULL, 0, '', '', '', 0, 0, 0, 0, @rpt_message)
            END

        /*  Return final data set back to the report file  */

            SELECT [date_range]
                 , [customer_name]
                 , [performance_dt]
                 , [order_mode_of_sale]
                 , [order_mode_of_sale_name]
                 , [title_name]
                 , [production_name]
                 , [order_count]
                 , [ticket_count]
                 , [total_order_count]
                 , [total_ticket_count]
                 , [report_message]
            FROM @final_table
    
    DONE:

END
GO

GRANT EXECUTE ON [dbo].[LRP_ORDER_STATISTICS] TO ImpUsers
GO

--EXECUTE [dbo].[LRP_ORDER_STATISTICS]
--        @perf_start_dt_1 = '6-15-2019', 
--        @perf_end_dt_1 = '1-6-2020',
--        @order_start_dt_1 = '5-14-2019', 
--        @order_end_dt_1 = '1-6-2020',
--        @perf_start_dt_2 = NULL, 
--        @perf_end_dt_2 = NULL,
--        @order_start_dt_2= NULL, 
--        @order_end_dt_2 = NULL,
--        @title_str = '37',
--        @mode_of_sale_str = '',--"3","7","11","12"',
--        --@channel_str = '',--'"2","6"',
--        --@column_detail = 'Member/NonMember',
--        @column_detail = 'Anonymous/Known',
--        @show_mode_of_sale = 'Y',
--        @show_production = 'Y'
