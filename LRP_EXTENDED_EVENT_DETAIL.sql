USE [impresario]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[LRP_EXTENDED_EVENT_DETAIL]
(
	@campaign_no INT
)
AS

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

WITH guestCTE (campaign_no, seat_no, cnt)
AS 
(
	SELECT ext.campaign_no, ISNULL(guest.seat_no,'') AS seat_no, COUNT(*) AS cnt
	FROM TX_EVENT_GUEST AS guest
	INNER JOIN TX_EVENT_EXTRACT AS ext
		ON guest.evex_no = ext.evex_no
	WHERE  guest.campaign_no = @campaign_no 
		AND ext.inv_status = 3
	GROUP BY ext.campaign_no, ISNULL(guest.seat_no, '')
)
SELECT det.id_key,
	det.campaign_no,
	det.eventcode,
	det.eventname,
	det.evntlocat,
	det.evnttype,
	LTRIM(RTRIM(typ.description)) as eventType,
	cam.event_dt,
	(SELECT COUNT(DISTINCT ex.customer_no) FROM TX_EVENT_EXTRACT ex WHERE ex.campaign_no = det.campaign_no) AS NumOfRegistrants,
	(SELECT SUM(cnt) FROM guestCTE g WHERE g.campaign_no = det.campaign_no) AS NumOfGuests,
	(SELECT cnt FROM guestCTE gu WHERE gu.seat_no = '' AND gu.campaign_no = det.campaign_no) AS NumOfAttendees
FROM LTR_EVENT_DETAIL det
LEFT JOIN LTR_EVENT_TYPE typ
	ON det.evnttype = typ.id
INNER JOIN dbo.T_CAMPAIGN cam
	ON det.campaign_no = cam.campaign_no
WHERE det.campaign_no = @campaign_no
ORDER BY cam.event_dt


