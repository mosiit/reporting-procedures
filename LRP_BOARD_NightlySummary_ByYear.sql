USE [impresario]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[LRP_BOARD_NightlySummary_ByYear]
(
	@customer_no INT
)    
AS

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

-- IF any more NIGHTLY SUMMARIES are ever created that are associated with a YEAR, 
-- the script will need to be checked to make sure they are being handled correctly. 

DECLARE @FYcurrent INT,
	@startDt DATETIME,
	@endDt DATETIME

EXEC [LRP_GetCurrentFY] @FYcurrent OUTPUT, @startDt OUTPUT, @endDt OUTPUT

SELECT customer_no, section, value,
	CASE 
		WHEN section LIKE 'FY%' THEN SUBSTRING(section,3,4)
		WHEN section LIKE '%FY' THEN ''
		WHEN section LIKE '%FY%' THEN 2000 + SUBSTRING(section, CHARINDEX('FY',section)+2, 2)
		WHEN section LIKE '%CY%' THEN 2000 + SUBSTRING(section, CHARINDEX('CY',section)+2, 2)
		ELSE ''
	END AS yr
FROM dbo.LT_NIGHTLY_SUMMARY 
	WHERE customer_no IN (SELECT expanded_customer_no FROM dbo.V_CUSTOMER_WITH_PRIMARY_GROUP WHERE customer_no = @customer_no)
		AND (section <> 'Outstanding AF Pledge Balance Current FY'
		AND section LIKE '%FY%' OR section LIKE '%CY%'
		)
