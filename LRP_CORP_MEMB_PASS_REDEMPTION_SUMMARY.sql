USE impresario
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_CORP_MEMB_PASS_REDEMPTION_SUMMARY]') AND type IN (N'P', N'PC'))
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_CORP_MEMB_PASS_REDEMPTION_SUMMARY] AS' 
GO

EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LRP_CORP_MEMB_PASS_REDEMPTION_SUMMARY] TO [impusers], [tessitura_app]'
GO

/*
    [LRP_CORP_MEMB_PASS_REDEMPTION_SUMMARY]
    Most reports are designated either as "Attendance" (past dates) or "Sales" (today and future dates).
    This report can be both, so the data is gathered in two steps.  
        The first step uses the history table to pull data for any past dates
        The second step uses the live sales tables to pull any data for any current or future dates


*/

ALTER PROCEDURE [dbo].[LRP_CORP_MEMB_PASS_REDEMPTION_SUMMARY]
        @report_start_dt DATETIME = NULL,
        @report_end_dt DATETIME = NULL,
        @title_str VARCHAR(4000) = '',
        @production_str VARCHAR(4000) = '',
        @source_str VARCHAR(4000) = '',
        @source_group_str VARCHAR(4000) = ''
WITH RECOMPILE AS BEGIN

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;
  
    /*  Procedure Variables and Temp Tables  */

        DECLARE @past_start_dt DATETIME = NULL,     @past_end_dt DATETIME = NULL, 
                @future_start_dt DATETIME = NULL,   @future_end_dt DATETIME = NULL;

        DECLARE @today DATETIME = CAST(GETDATE() AS DATE);

        DECLARE @title_list TABLE ([title_no] INT NOT NULL DEFAULT (0));
        DECLARE @production_list TABLE ([production_no] INT NOT NULL DEFAULT (0));
        DECLARE @source_list TABLE ([source_no] INT NOT NULL DEFAULT (0));
        DECLARE @source_group_list TABLE ([source_group_no] INT NOT NULL DEFAULT (0));

        IF OBJECT_ID('tempdb..#corp_pass_redemption') IS NOT NULL DROP TABLE [#corp_pass_redemption];

        CREATE TABLE [#corp_pass_redemption_summary] ([order_no] INT NOT NULL DEFAULT (0),
                                              [appeal_no] INT NOT NULL DEFAULT (0),
                                              [appeal_name] VARCHAR(30) NOT NULL DEFAULT (''),
                                              [appeal_campaign] INT NOT NULL DEFAULT (0),
                                              [source_no] INT NOT NULL DEFAULT (0),
                                              [source_name] VARCHAR(100) NOT NULL DEFAULT (''),
                                              [source_customer_no] VARCHAR(30) NOT NULL DEFAULT (''),
                                              [street1] VARCHAR(100) NOT NULL DEFAULT (''),
                                              [street2] VARCHAR(100) NOT NULL DEFAULT (''),
                                              [street3] VARCHAR(100) NOT NULL DEFAULT (''),
                                              [city] VARCHAR(50) NOT NULL DEFAULT (''), 
                                              [state] VARCHAR(30) NOT NULL DEFAULT (''),
                                              [postal_code] VARCHAR(30) NOT NULL DEFAULT (''),
                                              [source_group] INT NOT NULL DEFAULT (0),
                                              [source_group_name] VARCHAR(30) NOT NULL DEFAULT (''),
                                              [sli_no] INT NOT NULL DEFAULT (0),
                                              [price_type] INT NOT NULL DEFAULT (0),
                                              [price_type_name] VARCHAR(30) NOT NULL DEFAULT (''),
                                              [due_amt] DECIMAL (18,2) NOT NULL DEFAULT (0.0),
                                              [paid_amt] DECIMAL (18,2) NOT NULL DEFAULT (0.0),
                                              [ticket_no] INT NOT NULL DEFAULT (0),
                                              [sli_status] INT NOT NULL DEFAULT (0),
                                              [sli_status_name] VARCHAR(30) NOT NULL DEFAULT (''),
                                              [perf_no] INT NOT NULL DEFAULT (0),
                                              [zone_no] INT NOT NULL DEFAULT (0),
                                              [performance_dt] DATETIME NULL,
                                              [performance_time] VARCHAR(10) NOT NULL DEFAULT (''),
                                              [performance_time_display] VARCHAR(15) NOT NULL DEFAULT (''),
                                              [title_no] INT NOT NULL DEFAULT (0),
                                              [title_name] VARCHAR(30) NOT NULL DEFAULT (''),
                                              [production_no] INT NOT NULL DEFAULT (0),
                                              [production_name] VARCHAR(30) NOT NULL DEFAULT (''),
                                              [production_name_long] VARCHAR(100) NOT NULL DEFAULT (''),
                                              [rule_ind] CHAR(1) NOT NULL DEFAULT (''),
                                              [rule_id] INT NOT NULL DEFAULT (0),
                                              [rule_name] VARCHAR(30) NOT NULL DEFAULT (''),
                                              [customer_no] INT NOT NULL DEFAULT (0),
                                              [customer_name] VARCHAR(100) NOT NULL DEFAULT (''),
                                              [customer_sort] VARCHAR(100) NOT NULL DEFAULT (''),
                                              [cust_memb_no] INT NOT NULL DEFAULT (0),
                                              [memb_level] VARCHAR(20) NOT NULL DEFAULT (''),
                                              [memb_level_name] VARCHAR(50) NOT NULL DEFAULT (''),
                                              [memb_init_dt] DATETIME NULL,
                                              [memb_expire_dt] DATETIME NULL,
                                              [memb_fiscal_year] INT NOT NULL DEFAULT (0),
                                              [seat_counter] INT NOT NULL DEFAULT (0));

    /*  Check Parameters  */
    
        SELECT @report_start_dt = CAST(@report_start_dt AS DATE),
               @report_end_dt = CONVERT(CHAR(10),@report_end_dt,111) + ' 23:59:59.957';

        IF @report_end_dt >= @report_start_dt BEGIN

            IF CAST(@report_start_dt AS DATE) < @today AND CAST(@report_end_dt AS DATE) < @today
                SELECT @past_start_dt = @report_start_dt, @past_end_dt = @report_end_dt;

            ELSE IF CAST(@report_start_dt AS DATE) >= @today AND CAST(@report_end_dt AS DATE) >= @today
                SELECT @future_start_dt = @report_start_dt, @future_end_dt = @report_end_dt;

            ELSE IF CAST(@report_start_dt AS DATE) < @today AND CAST(@report_end_dt AS DATE) >= @today
                SELECT @past_start_dt = @report_start_dt, @past_end_dt = DATEADD(SECOND,-1,@today),
                       @future_start_dt = @today, @future_end_dt = @report_end_dt;

        END

        IF ISNULL(@title_str, '') = '' INSERT INTO @title_list ([title_no]) SELECT [title_no] FROM [dbo].[T_TITLE]
        ELSE INSERT INTO @title_list ([title_no]) SELECT [Element] FROM [dbo].[FT_SPLIT_LIST](REPLACE(@title_str,'"',''),',');
        
        IF ISNULL(@production_str, '') = '' INSERT INTO @production_list ([production_no]) SELECT [prod_no] FROM [dbo].[T_PRODUCTION]
        ELSE INSERT INTO @production_list ([production_no]) SELECT [Element] FROM [dbo].[FT_SPLIT_LIST](REPLACE(@production_str,'"',''),',');
        
        IF ISNULL(@source_str, '') = '' INSERT INTO @source_list ([source_no])  SELECT [source_no] FROM [dbo].[LV_ACTIVE_CORPORATE_MEMBERS]
        ELSE INSERT INTO @source_list ([source_no]) SELECT [Element] FROM [dbo].[FT_SPLIT_LIST](REPLACE(@source_str,'"',''),',');

        IF ISNULL(@source_group_str, '') = '' INSERT INTO @source_group_list ([source_group_no]) SELECT DISTINCT [source_group] FROM [dbo].[LV_ACTIVE_CORPORATE_MEMBERS] WHERE ISNULL([source_group],0) > 0
        ELSE INSERT INTO @source_group_list ([source_group_no]) SELECT [Element] FROM [dbo].[FT_SPLIT_LIST](REPLACE(@source_group_str,'"',''),',');

    /*  Get past data from the history table  */

        INSERT INTO [#corp_pass_redemption_summary] ([order_no], [appeal_no], [appeal_name], [appeal_campaign], [source_group], [source_group_name], [source_no], [source_customer_no], 
                                                     [source_name], [street1], [street2], [street3], [city], [state], [postal_code], [cust_memb_no], [memb_level], [memb_level_name],
                                                     [memb_init_dt], [memb_expire_dt], [memb_fiscal_year], [sli_no], [price_type], [price_type_name], [due_amt], [paid_amt], [ticket_no],
                                                     [sli_status], [sli_status_name], [perf_no], [zone_no], [performance_dt], [performance_time], [performance_time_display], [title_no],
                                                     [title_name], [production_no], [production_name], [production_name_long], [rule_ind], [rule_id], [rule_name], [customer_no],
                                                     [customer_name], [customer_sort], [seat_counter])
        EXECUTE [dbo].[LRP_CORP_MEMB_PASS_REDEMPTION] @report_start_dt = @report_start_dt, 
                                                      @report_end_dt = @report_end_dt, 
                                                      @title_str = @title_str, 
                                                      @production_str = @production_str, 
                                                      @source_str = @source_str, 
                                                      @source_group_str = @source_group_str

    

        


    FINISHED:



            SELECT pvt.[source_name], 
                   pvt.[source_group_name], 
                   pvt.[memb_expire_dt], 
                   ISNULL(pvt.[Exhibit Halls], 0) AS [Exhibit Halls], 
                   ISNULL([Special Exhibitions], 0) AS [Special Exhibitions],
                   ISNULL([Mugar Omni Theater], 0) AS [Omni Theater], 
                   ISNULL([Hayden Planetarium], 0) AS [Planetarium],
                   ISNULL([4-D Theater], 0) AS [4D Theater],
                   ISNULL([Butterfly Garden], 0) AS [Butterfly],
                   ISNULL(pvt.[Exhibit Halls], 0) + ISNULL([Special Exhibitions], 0) 
                                                  + ISNULL([Mugar Omni Theater], 0) 
                                                  + ISNULL([Hayden Planetarium], 0) 
                                                  + ISNULL([4-D Theater], 0) 
                                                  + ISNULL([Butterfly Garden], 0) AS [Total]
            FROM   (SELECT Source_name,
                           [source_group_name],
                           [memb_expire_dt],
                           [title_name],
                           [seat_counter]
                    FROM [#corp_pass_redemption_summary]) p  
            PIVOT (SUM ([seat_counter])  
                   FOR [title_name] IN  ([Exhibit Halls], [Special Exhibitions], [Mugar Omni Theater], [Hayden Planetarium], [4-D Theater], [Butterfly Garden])) AS pvt  
            ORDER BY pvt.[source_name];  

    DONE:

END
GO

EXECUTE [dbo].[LRP_CORP_MEMB_PASS_REDEMPTION_SUMMARY] @report_start_dt = '7-1-2020', @report_end_dt = '6-30-2021', @title_str = '', @production_str = '', @source_str = '', @source_group_str = ''
GO

