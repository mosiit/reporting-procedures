USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_SALES_PERCENTAGES_REPORT]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LRP_SALES_PERCENTAGES_REPORT]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LRP_SALES_PERCENTAGES_REPORT]
        @report_start_dt DATETIME = Null,
        @report_end_dt DATETIME = NULL,
        @report_users VARCHAR(4000) = NULL,
        @user_locations varchar(4000) = NULL,
        @include_operator_detail CHAR(1) = NULL,
        @include_daily_detail CHAR(1) = NULL,
        @exclusion_accounts varchar(4000) = Null
AS BEGIN

 /*  Report Variables  */
            
        DECLARE @divide_total VARCHAR(20) = 'touched'
        DECLARE @user_list TABLE ([userid] VARCHAR(50))
        DECLARE @location_list TABLE ([user_location] VARCHAR(30))
        DECLARE @exclusions_list TABLE ([userid] VARCHAR(50))
        DECLARE @user_count INT = 0

    /* Check Parameters  */
            
        SELECT @report_start_dt = ISNULL(@report_start_dt,DATEADD(DAY,-1,GETDATE())),
               @report_end_dt = ISNULL(@report_end_dt,DATEADD(DAY,-1,GETDATE()))

        SELECT @include_operator_detail = ISNULL(@include_operator_detail,'Y'),
               @include_daily_detail = ISNULL(@include_daily_detail,'N')

        SELECT @report_start_dt = CONVERT(DATE,@report_start_dt)
        SELECT @report_end_dt = CONVERT(DATE,@report_end_dt)

        SELECT @report_users = ISNULL(@report_users,'')
        IF @report_users = 'All' OR @report_users = 'All Users' SELECT @report_users = ''
        SELECT @report_users = REPLACE(@report_users,'"','')

        SELECT @user_locations = ISNULL(@user_locations,'')
        IF @user_locations = 'All' OR @user_locations = 'All Locations' SELECT @user_locations = ''
        SELECT @user_locations = REPLACE(@user_locations,'"','')

        SELECT @include_operator_detail = ISNULL(@include_operator_detail,'Y')
        IF @include_operator_detail <> 'N' SELECT @include_operator_detail = 'Y'

        SELECT @exclusion_accounts = ISNULL(@exclusion_accounts,'')
        SELECT @exclusion_accounts = REPLACE(@exclusion_accounts,'"','')

    /*  Parse Out Users and Locations */

        IF @report_users <> ''
            INSERT INTO @user_list SELECT LTRIM(RTRIM(Item)) FROM [dbo].[LFT_SPLIT_DILEMETED_STRING] (@report_users,',')

        IF @user_locations <> ''
            INSERT INTO @location_list SELECT LTRIM(RTRIM(Item)) FROM [dbo].[LFT_SPLIT_DILEMETED_STRING] (@user_locations,',')

        IF @exclusion_accounts <> ''
            INSERT INTO @exclusions_list SELECT LTRIM(RTRIM(Item)) FROM [dbo].[LFT_SPLIT_DILEMETED_STRING] (@exclusion_accounts,',')
            
        SELECT @user_count = COUNT(*) FROM @user_list
        SELECT @user_count = ISNULL(@user_count,0)

    /*  Temporary Table */
    
            IF OBJECT_ID('tempdb..#trx_percentages') IS NOT NULL DROP TABLE [#trx_percentages]

            CREATE TABLE [#trx_percentages] ([user_id] VARCHAR(10), [sale_date] CHAR(10), [user_name] VARCHAR(100), [user_name_sort] VARCHAR(100), [user_location] VARCHAR(30), 
                                             [orders_created] DECIMAL(18,4), [orders_touched] DECIMAL(18,4), [membership_sales] DECIMAL(18,4),
                                             [membership_percent] DECIMAL(18,4), [total_onestep_recovery] DECIMAL(18,4), [total_gift_memberships] DECIMAL(18,4),
                                             [total_onestep_eligible] DECIMAL(18,4), [basic_2_sales] DECIMAL(18,4), [basic_2_percent] DECIMAL(18,4), [basic_5_sales] DECIMAL(18,4), 
                                             [basic_5_percent] DECIMAL(18,4), [basic_8_sales] DECIMAL(18,4), [basic_8_percent] DECIMAL(18,4), [basic_total_sales] DECIMAL(18,4), 
                                             [basic_total_percent] DECIMAL(18,4), [basic_onestep_recovery] DECIMAL(18,2), [basic_gift_memberships] DECIMAL(18,4),
                                             [basic_one_step_eligible] DECIMAL(18,4), [onestep_basic] DECIMAL(18,4), [onestep_basic_percent] DECIMAL(18,4), 
                                             [premier_2_sales] DECIMAL(18,4), [premire_2_percent] DECIMAL(18,4), [premier_5_sales] DECIMAL(18,4), [premier_5_percent] DECIMAL(18,4),
                                             [premier_8_sales] DECIMAL(18,4), [premier_8_percent] DECIMAL(18,4), [premier_total_sales] DECIMAL(18,4),
                                             [premier_total_percent] DECIMAL(18,4), [premier_onestep_recovery] DECIMAL(18,4), [premier_gift_memberships] DECIMAL(18,2),
                                             [premier_onestep_eligible] DECIMAL(18,4), [onestep_premier] DECIMAL(18,4), [onestep_premier_percent] DECIMAL(18,4),
                                             [onestep_total] DECIMAL(18, 4), [onestep_total_percent] DECIMAL(18,4), [orders_counted] DECIMAL(18,4),
                                             [multi_performances_orders] DECIMAL(18, 4), [multi_performance_percent] DECIMAL(18,4), [performances_sold] DECIMAL(18,4),
                                             [average_perfs_per_order] DECIMAL(18,4), [user_count] INT, [rpt_message] VARCHAR(100))

            IF @include_daily_detail = 'Y'
                INSERT INTO [#trx_percentages]
                SELECT [user_id], [history_date], [user_name], [user_name_sort], [user_location], 
                       [orders_created], [orders_touched], [membership_sales],  0, [total_onestep_recovery], total_gift_memberships, 0,
                       [basic_2_sales], 0, [basic_5_sales], 0, [basic_8_sales], 0, [basic_total_sales], 0, [basic_onestep_recovery], [basic_gift_memberships],
                       0,  [onestep_basic], 0,  [premier_2_sales], 0, [premier_5_sales], 0, [premier_8_sales], 0, [premier_total_sales], 0, 
                       [premier_onestep_recovery], [premier_gift_memberships], 0, [onestep_premier], 0,
                       [onestep_total],  0, [orders_counted], [multi_performances_orders], 0, [performances_sold], 0, @user_count, ''
                FROM [dbo].[LT_HISTORY_TRX_STATS]
                WHERE [history_dt] BETWEEN @report_start_dt AND @report_end_dt
            ELSE
                INSERT INTO [#trx_percentages]
                SELECT [user_id], '', [user_name], [user_name_sort], [user_location], 
                       SUM([orders_created]), SUM([orders_touched]), SUM([membership_sales]),  0, SUM([total_onestep_recovery]), SUM(total_gift_memberships), 0,
                       SUM([basic_2_sales]), 0, SUM([basic_5_sales]), 0, SUM([basic_8_sales]), 0, SUM([basic_total_sales]), 0, SUM([basic_onestep_recovery]), SUM([basic_gift_memberships]),
                       0,  SUM([onestep_basic]), 0,  SUM([premier_2_sales]), 0, SUM([premier_5_sales]), 0, SUM([premier_8_sales]), 0, SUM([premier_total_sales]), 0, 
                       SUM([premier_onestep_recovery]), SUM([premier_gift_memberships]), 0, SUM([onestep_premier]), 0,
                       SUM([onestep_total]),  0, SUM([orders_counted]), SUM([multi_performances_orders]), 0, SUM([performances_sold]), 0, @user_count, ''
                FROM [dbo].[LT_HISTORY_TRX_STATS]
                WHERE [history_dt] BETWEEN @report_start_dt AND @report_end_dt
                GROUP BY [user_id], [user_name], [user_name_sort], [user_location]

    /*  Filter out unwanted records based on the parameters*/

        IF EXISTS (SELECT * FROM @user_list)
            DELETE FROM [#trx_percentages] WHERE [user_id] NOT IN (SELECT [userid] FROM @user_list)
        ELSE IF EXISTS (SELECT * FROM @location_list)
            DELETE FROM [#trx_percentages] WHERE [user_location] NOT IN (SELECT [user_location] FROM @location_list)

        IF EXISTS (SELECT * FROM @exclusions_list)
            DELETE FROM [#trx_percentages] WHERE [user_id] IN (SELECT [userid] FROM @exclusions_list)

    /*  Operator totals  */
    
        IF @include_daily_detail = 'Y' AND EXISTS (SELECT * FROM @user_list)
            INSERT INTO [#trx_percentages]
            SELECT '', '', [user_name] + ' Total', 'ZZA_' + [user_name], [user_name], 
                   SUM([orders_created]), SUM([orders_touched]), SUM([membership_sales]), 0, SUM([total_onestep_recovery]), SUM([total_gift_memberships]), 0,
                   SUM([basic_2_sales]), 0, SUM([basic_5_sales]), 0, SUM([basic_8_sales]), 0, SUM([basic_total_sales]), 0, SUM([basic_onestep_recovery]), 
                   SUM([basic_gift_memberships]), 0, SUM([onestep_basic]), 0,  SUM([premier_2_sales]), 0, SUM([premier_5_sales]), 0, SUM([premier_8_sales]), 0, 
                   SUM([premier_total_sales]), 0, SUM([premier_onestep_recovery]), SUM([premier_gift_memberships]), 0, SUM([onestep_premier]), 0,
                   SUM([onestep_total]),  0, SUM([orders_counted]), SUM([multi_performances_orders]), 0, SUM([performances_sold]), SUM([average_perfs_per_order]),
                   @user_count, ''
            FROM [#trx_percentages]
            GROUP BY [user_name]
            HAVING SUM([orders_touched]) > 0


    /*  Insert deparmental totals  */
    
            INSERT INTO [#trx_percentages]
            SELECT '', '', [user_location] + ' Total', 'ZZB_' + [user_location], [user_location], 
                   SUM([orders_created]), SUM([orders_touched]), SUM([membership_sales]), 0, SUM([total_onestep_recovery]), SUM([total_gift_memberships]), 0,
                   SUM([basic_2_sales]), 0, SUM([basic_5_sales]), 0, SUM([basic_8_sales]), 0, SUM([basic_total_sales]), 0, SUM([basic_onestep_recovery]), 
                   SUM([basic_gift_memberships]), 0, SUM([onestep_basic]), 0,  SUM([premier_2_sales]), 0, SUM([premier_5_sales]), 0, SUM([premier_8_sales]), 0, 
                   SUM([premier_total_sales]), 0, SUM([premier_onestep_recovery]), SUM([premier_gift_memberships]), 0, SUM([onestep_premier]), 0,
                   SUM([onestep_total]),  0, SUM([orders_counted]), SUM([multi_performances_orders]), 0, SUM([performances_sold]), SUM([average_perfs_per_order]),
                   @user_count, ''
            FROM [#trx_percentages]
            GROUP BY [user_location]
            HAVING COUNT(DISTINCT [user_id]) > 1
            
        /*  Insert Grand Total  */

            IF EXISTS (SELECT * FROM [#trx_percentages]) AND (SELECT COUNT(DISTINCT [user_location]) FROM [#trx_percentages] WHERE [user_id] <> '') > 1
                INSERT INTO [#trx_percentages]
                SELECT '', '', 'Grand Total', 'ZZC_Grand Total', 'ZZC_Grand Total', 
                       SUM([orders_created]), SUM([orders_touched]), SUM([membership_sales]), 0, SUM([total_onestep_recovery]), SUM([total_gift_memberships]), 0,
                       SUM([basic_2_sales]), 0, SUM([basic_5_sales]), 0, SUM([basic_8_sales]), 0, SUM([basic_total_sales]), 0, SUM([basic_onestep_recovery]),
                       SUM([basic_gift_memberships]), 0, SUM([onestep_basic]), 0,  SUM([premier_2_sales]), 0, SUM([premier_5_sales]), 0, SUM([premier_8_sales]), 0, 
                       SUM([premier_total_sales]), 0, SUM([premier_onestep_recovery]), SUM([premier_gift_memberships]), 0, SUM([onestep_premier]), 0,
                       SUM([onestep_total]),  0, SUM([orders_counted]), SUM([multi_performances_orders]), 0, SUM([performances_sold]), SUM([average_perfs_per_order]),
                       @user_count, ''
                FROM [#trx_percentages]
                WHERE [user_id] <> ''

          
        /*  Delete fine detail if that's what asked for  */

            IF @include_operator_detail = 'N'
                DELETE FROM [#trx_percentages] WHERE [user_id] <> ''

        /*  Do the Math  */

            UPDATE [#trx_percentages] SET [total_onestep_eligible] = ([membership_sales] - [total_onestep_recovery] - [total_gift_memberships]),
                                          [basic_one_step_eligible] = ([basic_total_sales] - [basic_onestep_recovery] - [basic_gift_memberships]),
                                          [premier_onestep_eligible] = ([premier_total_sales] - [premier_onestep_recovery] - [premier_gift_memberships])

            IF @divide_total = 'touched' UPDATE [#trx_percentages] SET [membership_percent] = ([membership_sales] / [orders_touched]) WHERE [orders_touched] <> 0
            ELSE                         UPDATE [#trx_percentages] SET [membership_percent] = ([membership_sales] / [orders_created]) WHERE [orders_created] <> 0

            UPDATE [#trx_percentages] SET [basic_2_percent] = ([basic_2_sales] / [membership_sales]) WHERE [membership_sales] <> 0
            UPDATE [#trx_percentages] SET [basic_5_percent] = ([basic_5_sales] / [membership_sales]) WHERE [membership_sales] <> 0
            UPDATE [#trx_percentages] SET [basic_8_percent] = ([basic_8_sales] / [membership_sales]) WHERE [membership_sales] <> 0
            UPDATE [#trx_percentages] SET [basic_total_sales] = ([basic_total_sales] / [membership_sales]) WHERE [membership_sales] <> 0
            UPDATE [#trx_percentages] SET [onestep_basic_percent] = ([onestep_basic] / [basic_one_step_eligible]) WHERE [basic_one_step_eligible] <> 0
            UPDATE [#trx_percentages] SET [premire_2_percent] = ([premier_2_sales] / [membership_sales]) WHERE [membership_sales] <> 0
            UPDATE [#trx_percentages] SET [premier_5_percent] = ([premier_5_sales] / [membership_sales]) WHERE [membership_sales] <> 0
            UPDATE [#trx_percentages] SET [premier_8_percent] = ([premier_8_sales] / [membership_sales]) WHERE [membership_sales] <> 0
            UPDATE [#trx_percentages] SET [premier_total_percent] = ([premier_total_sales] / [membership_sales]) WHERE [membership_sales] <> 0
            UPDATE [#trx_percentages] SET [onestep_premier_percent] = ([onestep_premier] / [premier_onestep_eligible]) WHERE [premier_onestep_eligible] <> 0
            UPDATE [#trx_percentages] SET [onestep_total_percent] = ([onestep_total] / [total_onestep_eligible]) WHERE [total_onestep_eligible] <> 0
            UPDATE [#trx_percentages] SET [multi_performance_percent] = ([multi_performances_orders] / [orders_counted]) WHERE [orders_counted] <> 0
            UPDATE [#trx_percentages] SET [average_perfs_per_order] = ([performances_sold] / [orders_counted]) WHERE [orders_counted] <> 0

            UPDATE [#trx_percentages] SET [user_name_sort] = [user_location] + ' ' + [user_name_sort]

    FINISHED:

            IF NOT EXISTS (SELECT * FROM [#trx_percentages])
                INSERT INTO [#trx_percentages] ([rpt_message])  VALUES ('No records found for the criteria you entered.')
            ELSE
                INSERT INTO [#trx_percentages]
                VALUES ('', '', CONVERT(CHAR(10),@report_end_dt,111), '', CONVERT(CHAR(10),@report_start_dt,111), 
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0, 0, 0, 0, 0, 0, @user_count, '')
            

            SELECT [user_id], [sale_date], [user_name], [user_name_sort], [user_location], [orders_created], [orders_touched], [membership_sales], [membership_percent], 
                   [total_onestep_recovery], [total_gift_memberships], [total_onestep_eligible], [basic_2_sales], [basic_2_percent], [basic_5_sales], 
                   [basic_5_percent], [basic_8_sales], [basic_8_percent], [basic_total_sales], [basic_total_percent], [onestep_basic], [onestep_basic_percent], 
                   [premier_2_sales], [premire_2_percent], [premier_5_sales], [premier_5_percent], [premier_8_sales], [premier_8_percent], [premier_total_sales], 
                   [premier_total_percent], [onestep_premier], [onestep_premier_percent], [onestep_total], [onestep_total_percent], [orders_counted], 
                   [multi_performances_orders], [multi_performance_percent], [performances_sold], [average_perfs_per_order], @user_count AS 'user_count', [rpt_message]
            FROM [#trx_percentages]
            ORDER BY [user_name_sort], [sale_date]

    DONE:

        IF OBJECT_ID('tempdb..#trx_percentages') IS NOT NULL DROP TABLE [#trx_percentages]

END
GO


GRANT EXECUTE ON [dbo].[LRP_SALES_PERCENTAGES_REPORT] TO ImpUsers
GO


--EXECUTE [dbo].[LRP_SALES_PERCENTAGES_REPORT] 
--        @report_start_dt = '11-1-2017', @report_end_dt = '11-30-2017',@report_users = '', @user_locations = '',
--        @include_operator_detail = 'Y', @include_daily_detail = 'N'

--EXECUTE [dbo].[LRP_SALES_PERCENTAGES_REPORT] @report_start_dt = '11-1-2017', @report_end_dt = '11-30-2017',@report_users = '"sluorn00', @user_locations = '"Science Central"',@include_operator_detail = 'Y', @exclusion_accounts = '"agerma00"'