USE [impresario]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LRP_BOARD_EventAttendanceHistory]
(
	@customer_no INT
)
AS

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

---- Get all Events that the customer ever Attended ("RSVP_Reg") 

SELECT 
	ex.customer_no, det.eventname, camp.start_dt AS eventDate
FROM dbo.TX_EVENT_EXTRACT ex
	INNER JOIN dbo.TR_INVITATION_STATUS stat
		ON ex.inv_status = stat.id
	INNER JOIN LTR_EVENT_DETAIL det
		ON ex.campaign_no = det.campaign_no
	INNER JOIN dbo.T_CAMPAIGN camp
		ON ex.campaign_no = camp.campaign_no
WHERE stat.description = 'RSVP_Reg'
	AND ex.customer_no IN (SELECT expanded_customer_no FROM dbo.V_CUSTOMER_WITH_PRIMARY_GROUP WHERE customer_no = @customer_no)
ORDER BY camp.start_dt DESC 