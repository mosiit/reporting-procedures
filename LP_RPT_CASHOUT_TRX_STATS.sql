USE impresario
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LP_RPT_CASHOUT_TRX_STATS]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LP_RPT_CASHOUT_TRX_STATS]
GO

CREATE PROCEDURE [dbo].[LP_RPT_CASHOUT_TRX_STATS]
        @rpt_dt datetime,
        @rpt_operator varchar(8)
AS BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

    DECLARE @rpt_date char(10), @rpt_message varchar(100), @report_start datetime

    DECLARE @trx_stat_table table ([rpt_date] char(10), [trx_started] decimal(18,2), [trx_touched] decimal(18,2), 
                                   [trx_addon] decimal(18,2), [trx_addon_percent_Started] decimal(18,2), [trx_addon_percent_touched] decimal(18,2), 
                                   [trx_memb] int, [trx_memb_percent_started] decimal(18,2), [trx_memb_percent_touched] decimal(18,2),
                                   [trx_memb_basic] decimal(18,2), [trx_memb_basic_percent_started] decimal(18,2), [trx_memb_basic_percent_touched] decimal(18,2), [trx_memb_basic_percent_memb] decimal(18,2), 
                                   [trx_memb_premier] decimal(18,2), [trx_memb_premier_percent_started] decimal (18,2), [trx_memb_premier_percent_touched] decimal (18,2), [trx_memb_premier_percent_memb] decimal (18,2),
                                   [trx_memb_onestep] decimal(18,2), [trx_memb_onestep_percent_started] decimal (18,2), [trx_memb_onestep_percent_touched] decimal (18,2), [trx_memb_onestep_percent_memb] decimal(18,2),
                                   [elapsed_time] int, [rpt_message] varchar(100))

    SELECT @report_start = getdate()

    SELECT @rpt_message = '', @rpt_date = ''

    SELECT @rpt_operator = IsNull(@rpt_operator,'')
    
    IF @rpt_dt is null
        SELECT @rpt_message = 'error: invalid date passed to report.'
    ELSE BEGIN

        SELECT @rpt_date = convert(char(10),@rpt_dt,111)
    
        IF @rpt_operator = '' or not exists (SELECT * FROM T_METUSER WHERE [userid] = @rpt_operator)
            SELECT @rpt_message = 'error: invalid operator id passed to report (' + @rpt_operator + ').'
    END
        
    IF @rpt_message <> '' GOTO FINISHED
          
    --Total Transactions (started)
    INSERT INTO @trx_stat_table VALUES (@rpt_date, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '')

    UPDATE @trx_stat_table SET [trx_started] = (SELECT count(DISTINCT [order_no]) FROM T_SUB_LINEITEM WHERE convert(char(10),[create_dt],111) = @rpt_date and created_by = @rpt_operator),
                               [trx_touched] = (SELECT count(DISTINCT [order_no]) FROM T_SUB_LINEITEM WHERE (convert(char(10),[create_dt],111) = @rpt_date and created_by = @rpt_operator) or
                                                                                                            (convert(char(10),[last_update_dt],111) = @rpt_date and last_updated_by = @rpt_operator)),
                               [trx_addon] = (SELECT count(DISTINCT [order_no]) FROM [dbo].[LV_RPT_CASHOUT_ADD_ONS] WHERE [product_date] = @rpt_date and [product_operator] = @rpt_operator),
                                  [trx_memb] = (SELECT count(*) FROM [dbo].[LV_RPT_CASHOUT_MEMBERSHIPS] WHERE [membership_date] = @rpt_date and [membership_operator] = @rpt_operator),
                            [trx_memb_basic] = (SELECT count(*) FROM [dbo].[LV_RPT_CASHOUT_MEMBERSHIPS] WHERE [membership_date] = @rpt_date and [membership_operator] = @rpt_operator and [membership_type] like 'Basic%'),
                            [trx_memb_premier] = (SELECT count(*) FROM [dbo].[LV_RPT_CASHOUT_MEMBERSHIPS] WHERE [membership_date] = @rpt_date and [membership_operator] = @rpt_operator and [membership_type] like 'Premier%'),
                            [trx_memb_onestep] = (SELECT sum([one_step_count]) FROM [dbo].[LV_RPT_CASHOUT_MEMBERSHIPS] WHERE [membership_date] = @rpt_date and [membership_operator] = @rpt_operator)


    --UPDATE @trx_stat_table SET [trx_addon_percent] = ([trx_addon] / [trx_total]) WHERE [trx_total] > 0
   
    UPDATE @trx_stat_table SET [trx_addon_percent_Started] = ([trx_addon] / [trx_started]),
                               [trx_memb_percent_started] = ([trx_memb] / [trx_started]),
                               [trx_memb_basic_percent_started] = ([trx_memb_basic] / [trx_started]),
                               [trx_memb_premier_percent_started] = ([trx_memb_premier] / [trx_started]) ,
                               [trx_memb_onestep_percent_started] = ([trx_memb_onestep] / [trx_started]) 
    WHERE [trx_started] > 0


    UPDATE @trx_stat_table SET [trx_addon_percent_touched] = ([trx_addon] / [trx_touched]),
                               [trx_memb_percent_touched] = ([trx_memb_basic] / [trx_touched]),
                               [trx_memb_basic_percent_touched] = ([trx_memb_basic] / [trx_touched]),
                               [trx_memb_premier_percent_touched] = ([trx_memb_premier] / [trx_touched]) ,
                               [trx_memb_onestep_percent_touched] = ([trx_memb_onestep] / [trx_touched]) 
    WHERE [trx_touched] > 0


    UPDATE @trx_stat_table SET [trx_memb_basic_percent_memb] = ([trx_memb_basic] / [trx_memb]), 
                               [trx_memb_premier_percent_memb] = ([trx_memb_premier] / [trx_memb]), 
                               [trx_memb_onestep_percent_memb] = ([trx_memb_onestep] / [trx_memb]) 
    WHERE [trx_memb] > 0

    FINISHED:

        DELETE FROM @trx_stat_table WHERE [trx_started] = 0.00 and [trx_touched] = 0.00

        IF not exists (SELECT * FROM @trx_stat_table) BEGIN
            IF @rpt_message = '' SELECT @rpt_message = 'No transaction statistics found for ' + @rpt_operator + ' on ' + @rpt_date +  '.'
            INSERT INTO @trx_stat_table (rpt_date, rpt_message) VALUES (@rpt_date, @rpt_message)
        END

         UPDATE @trx_stat_table SET [elapsed_time] = datediff(second,@report_start,getdate())

         SELECT [rpt_date],
                ISNULL([trx_started], 0) AS [trx_started],
                ISNULL([trx_touched], 0) AS [trx_touched],
                ISNULL([trx_addon], 0) AS [trx_addon],
                ISNULL([trx_addon_percent_started], 0) AS [trx_addon_percent_started],
                ISNULL([trx_addon_percent_touched], 0) AS [trx_addon_percent_touched],
                ISNULL([trx_memb], 0) AS [trx_memb],
                ISNULL([trx_memb_percent_started], 0) AS [trx_memb_percent_started],
                ISNULL([trx_memb_percent_touched], 0) AS [trx_memb_percent_touched],
                ISNULL([trx_memb_basic], 0) AS [trx_memb_basic],
                ISNULL([trx_memb_basic_percent_started], 0) AS [trx_memb_basic_percent_started],
                ISNULL([trx_memb_basic_percent_touched], 0) AS [trx_memb_basic_percent_touched],
                ISNULL([trx_memb_basic_percent_memb], 0) AS [trx_memb_basic_percent_memb],
                ISNULL([trx_memb_premier], 0) AS [trx_memb_premier],
                ISNULL([trx_memb_premier_percent_started], 0) AS [trx_memb_premier_percent_started],
                ISNULL( [trx_memb_premier_percent_touched], 0) AS [trx_memb_premier_percent_touched],
                ISNULL([trx_memb_premier_percent_memb], 0) AS [trx_memb_premier_percent_memb],
                ISNULL([trx_memb_onestep], 0) AS [trx_memb_onestep],
                ISNULL([trx_memb_onestep_percent_started], 0) AS [trx_memb_onestep_percent_started],
                ISNULL([trx_memb_onestep_percent_touched], 0) AS [trx_memb_onestep_percent_touched],
                ISNULL([trx_memb_onestep_percent_memb], 0) AS [trx_memb_onestep_percent_memb],
                [elapsed_time],
                [rpt_message]
         FROM   @trx_stat_table

END
GO

GRANT EXECUTE ON [dbo].[LP_RPT_CASHOUT_TRX_STATS] to impusers
GO


/*

trx_started = # of transactions the cashier started
trx_touched = # of transactions the cashier both started and updated
trx_addon = # of transactions with one of the add-on price types
trx_addon_percent_Started = percentage of started transactions that contain an add-on price type
trx_addon_percent_touched = percentage of touched transactions that contain an add-on price type
trx_memb = # of transactions that contain a membership sold by the cashier
trx_memb_percent_started = percentage of started transactions that contain a membership
trx_memb_percent_touched = percentage of touched transactions that contain a membership
trx_memb_basic = # of basic membership transactions
trx_memb_basic_percent_started = percentage of started transactions that contain a basic membership
trx_memb_basic_percent_touched = percentage of touched transactions that contain a basic membership
trx_memb_basic_percent_memb = percentage of membership transactions that contain a basic membership
trx_memb_premier = # of premier membership transactions
trx_memb_premier_percent_started = percentage of started transactions that contain a premier membership
trx_memb_premier_percent_touched = percentage of touched transactions that contain a premier membership
trx_memb_premier_percent_memb = percentage of memberhship transactions that contain a premier membership
trx_memb_onestep = # of membership transactions processed by the cashier who have asked to be on one-step
trx_memb_onestep_percent_started = percentage of started transactions that contain a membership with one-step
trx_memb_onestep_percent_touched = percentage of touched transactions that contain a membership with one-step
trx_memb_onestep_percent_memb = percentage of started transactions that contain  one-step

*/
