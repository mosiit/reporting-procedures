USE impresario;
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_ATTEND_MONTH_TO_DATE]') AND type in (N'P', N'PC')) BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_ATTEND_MONTH_TO_DATE] AS';
    EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LRP_ATTEND_MONTH_TO_DATE] TO impusers';
END;
GO

ALTER PROCEDURE [dbo].[LRP_ATTEND_MONTH_TO_DATE]
        @week_ending DATETIME = NULL
AS BEGIN

    SET NOCOUNT ON;
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;    

    /*  Procedure Variables  */

        DECLARE @fiscal_year INT = 0
        DECLARE @week_start_dt DATETIME, @week_end_dt DATETIME;
        DECLARE @month_start_dt DATETIME, @month_end_dt DATETIME;

        DECLARE @prev_fiscal_year INT = 0
        DECLARE @prev_week_start_dt DATETIME, @prev_week_end_dt DATETIME; 
        DECLARE @prev_month_start_dt DATETIME, @prev_month_end_dt DATETIME;

        DECLARE @first_monday INT = 0;

        --These are the title numbers for the various venues
        DECLARE @gross_attend_no INT = 0,           @exhibit_halls_no INT = 27,         @special_exhibitions_no INT = 37,   
                @butterfly_garden_no INT = 157,     @mugar_omni_theater_no INT = 161,   @4d_theater_no INT = 173,
                @hayden_planetarium_no INT = 1132,  @thrill_ride_no INT = 1343,         @entertainment_no INT = 25;

        DECLARE @special_exhibitions_name VARCHAR(30) = ''

        DECLARE @non_school_exhibit_production INT = 32

        DECLARE @entertainment_title_no INT = 25;        --Assigned an invenyory number not being used as a place holder
                                                         --this number is not used anywhere else but the Attendance Reports

        DECLARE @report_titles TABLE ([title_no] INT NOT NULL DEFAULT (0),
                                      [title_name] VARCHAR(30) NOT NULL DEFAULT (''))

    /*  Temporary Table  */    

        IF OBJECT_ID('tempdb..#attendance_table') IS NOT NULL DROP TABLE [#attendance_table];    
    
        CREATE TABLE [#attendance_table] ([title_group] VARCHAR(30) NOT NULL DEFAULT(''),
                                          [title_group_order] CHAR(1) NOT NULL DEFAULT(''),
                                          [title_group_color] VARCHAR(50) NOT NULL DEFAULT ('White'),
                                          [title_no] INT NOT NULL DEFAULT(0),
                                          [title_name] VARCHAR(30) DEFAULT(0),
                                          [title_order] CHAR(1) NOT NULL DEFAULT(''),
                                          [title_color] VARCHAR(50) NOT NULL DEFAULT ('White'),
                                          [production_no] INT NOT NULL DEFAULT(0),
                                          [production_name] VARCHAR(30) NOT NULL DEFAULT(''),
                                          [fiscal_year] INT NOT NULL DEFAULT(0),
                                          [visit_dt] DATETIME NULL,
                                          [visit_week] INT NOT NULL DEFAULT(0),
                                          [past_report_date] CHAR(1) NOT NULL DEFAULT('N'),
                                          [tickets_sold] INT NOT NULL DEFAULT(0));
          
    /*  Check Parameters  */

        SELECT @week_ending = CAST(ISNULL(@week_ending,GETDATE()) AS DATE)
    
    /*  Get date values from LFT_ATTENDANCE_WEEK_DATES function  */
          
        SELECT @fiscal_year = [cur_fiscal_year], 
               @week_start_dt = [cur_week_start_dt], 
               @week_end_dt = [cur_week_end_dt], 
               @month_start_dt = [cur_month_start_dt], 
               @month_end_dt = [cur_month_end_dt], 
               @prev_fiscal_year = [prv_fiscal], 
               @prev_week_start_dt = [prv_week_start_dt], 
               @prev_week_end_dt = [prv_week_end_dt], 
               @prev_month_start_dt = [prv_month_start_dt], 
               @prev_month_end_dt = [prv_month_end_dt]
        FROM [dbo].[LFT_ATTENDANCE_WEEK_DATES] (@week_ending, 0, 'Y');


        SELECT @fiscal_year AS [cur_fiscal_year], 
               @week_start_dt AS [cur_week_start_dt], 
               @week_end_dt AS [cur_week_end_dt], 
               @month_start_dt AS [cur_month_start_dt], 
               @month_end_dt AS [cur_month_end_dt], 
               @prev_fiscal_year AS [prv_fiscal], 
               @prev_week_start_dt AS [prv_week_start_dt], 
               @prev_week_end_dt AS [prv_week_end_dt], 
               @prev_month_start_dt AS [prv_month_start_dt], 
               @prev_month_end_dt AS [prv_month_end_dt]


    /*  For weeks that cross over months, change start and end dates to the month based on week ending date  */

    --IF @week_end_dt > @month_end_dt
    --    SELECT @month_start_dt = DATEADD(MONTH,1,@month_start_dt),
    --           @month_end_dt = DATEADD(MONTH,1,@month_end_dt),
    --           @prev_month_start_dt = DATEADD(MONTH,1,@prev_month_start_dt),
    --           @prev_month_end_dt = DATEADD(MONTH,1,@prev_month_end_dt)

                --If want to base on start date of the week instead of end date of the week, 
                --these are the only two variables that need to be changed.
                --SELECT @week_end_dt = @month_end_dt,
                --       @prev_week_end_dt = @prev_month_end_dt

    /*  Get the day on which the first Monday of the month falls  */

        --SELECT @first_monday = DATEPART(DAY,DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(DAY, (6 - DATEPART(DAY, @week_ending)), @week_ending)), 0));
        SELECT @first_monday = DATEPART(DAY,DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(DAY, (6 - DATEPART(DAY, @month_start_dt)), @month_start_dt)), 0));

        INSERT INTO @report_titles ([title_no])
        VALUES (@exhibit_halls_no),         (@butterfly_garden_no),     (@special_exhibitions_no),      (@mugar_omni_theater_no),    
               (@4d_theater_no),            (@thrill_ride_no),          (@hayden_planetarium_no),       (@entertainment_no) 
       
        UPDATE ttl
        SET ttl.[title_name] = CASE WHEN ttl.[title_no] = @entertainment_no THEN 'Entertainment'
                                    ELSE ISNULL([description],'') END
        FROM @report_titles AS ttl
             LEFT OUTER JOIN [dbo].[T_INVENTORY] AS inv ON inv.[inv_no] = ttl.[title_no]

    /*  Get Gross Attendance numbers  */    
    
        INSERT INTO [#attendance_table] ([title_group],[title_group_order],[title_no],[title_name], [production_no], 
                                         [production_name], [fiscal_year],[visit_dt],[tickets_sold])
        SELECT 'Entire Museum',
               'A',
               0,
               'Gross Attendance',
               0,
               '',
               [dbo].[LF_GetFiscalYear]([history_dt]), 
               CAST([history_dt] AS DATE), 
               SUM([visit_count])
        FROM [dbo].[LT_HISTORY_VISIT_COUNT]
        WHERE [history_dt] BETWEEN @month_start_dt AND @week_end_dt
           OR [history_dt] BETWEEN @prev_month_start_dt AND @prev_week_end_dt
        GROUP BY [dbo].[LF_GetFiscalYear]([history_dt]), CAST([history_dt] AS DATE);


    /*  Get Ticketed Venue Numbers  */

        INSERT INTO [#attendance_table] ([title_group],[title_group_order],[title_no],[title_name], [title_order], 
                                         [production_no], [production_name], [fiscal_year],[visit_dt],[tickets_sold])
        SELECT '',
               '',
               tik.[title_no],
               tik.[title_name],
               '',
               tik.[production_no],
               tik.[production_name_short],
               [dbo].[LF_GetFiscalYear](tik.[performance_date]),
               tik.[performance_date],
               SUM(tik.[sale_total])
        FROM [dbo].[LT_HISTORY_TICKET] AS tik
             INNER JOIN [dbo].[T_ORDER] AS ord ON ord.[order_no] = tik.[order_no]
        WHERE (tik.[performance_date] BETWEEN CONVERT(CHAR(10),@month_start_dt,111) AND CONVERT(CHAR(10),@week_end_dt,111)
               OR tik.[performance_date] BETWEEN CONVERT(CHAR(10),@prev_month_start_dt,111) AND CONVERT(CHAR(10),@prev_week_end_dt,111))
          AND tik.[title_no] IN (@exhibit_halls_no, @butterfly_garden_no, @special_exhibitions_no, @mugar_omni_theater_no,
                                 @4d_theater_no, @thrill_ride_no, @hayden_planetarium_no)
          AND ord.[MOS] <> 9
          AND tik.[production_name] NOT LIKE '%Buyout%'
        GROUP BY tik.[title_no], tik.[title_name], tik.[production_no], tik.[production_name_short],
                 [dbo].[LF_GetFiscalYear](tik.[performance_date]), tik.[performance_date];

        --SELECT * FROM [#attendance_table] WHERE title_no = 1132 AND CAST([visit_dt] AS DATE) = '10-3-2018';

       /*  Add Member Scan, Show and Go, Show and Collected, and GoBoston counts to the Exhibit Hall Attendance numbers */
                    
        WITH CTE_EXTRA_EXHIBIT_HALL_ATTENDANCE ([fiscal_year], [history_dt], [extra_exh_attendance])
        AS (                 
            SELECT  dbo.LF_GetFiscalYear(history_dt), [history_dt], SUM([visit_count]) 
            FROM [dbo].[LT_HISTORY_VISIT_COUNT] 
            WHERE ([history_dt] BETWEEN @month_start_dt AND @week_end_dt
                    OR [history_dt] BETWEEN @prev_month_start_dt AND @prev_week_end_dt)
                 AND [attendance_type] <> 'Ticketed Events' 
                 GROUP BY dbo.LF_GetFiscalYear(history_dt), history_dt
           )
         UPDATE att
         SET att.[tickets_sold] += ISNULL(exh.[extra_exh_attendance],0)
         FROM [#attendance_table] AS att
              LEFT OUTER JOIN [CTE_EXTRA_EXHIBIT_HALL_ATTENDANCE] AS exh ON exh.[fiscal_year] = att.[fiscal_year]
                                                                        AND exh.[history_dt] = att.[visit_dt]
         WHERE att.[title_no] = @exhibit_halls_no AND att.[production_no] = @non_school_exhibit_production;
           

    /*  Identify the shows that are entertainment shows vs educational shows in the Planetarium  */

        WITH CTE_ENTERTAINMENT_SHOWS (prod_no, prod_name, cont_value)
        AS (
            SELECT pro.[prod_no], inv.[description], con.[value]
            FROM [dbo].[T_PRODUCTION] AS pro
                 INNER JOIN [dbo].[T_INVENTORY] AS inv ON inv.[inv_no] = pro.[prod_no]
                 INNER JOIN [dbo].[TX_INV_CONTENT] AS con ON con.[inv_no] = pro.[prod_no] 
                                                         AND con.[content_type] = 37
            WHERE pro.[title_no] = @hayden_planetarium_no
              AND con.[value] = 'Entertainment'
           )
        UPDATE att
        SET [title_no] = @entertainment_title_no,
            [title_name] = 'Entertainment'
        FROM [#attendance_table] AS att
             INNER JOIN [CTE_ENTERTAINMENT_SHOWS] AS cte ON cte.[prod_no] = att.[production_no]
                
     /*  Add In Missing Dates with zero sold  */

        INSERT INTO [#attendance_table] ([title_group],[title_group_order],[title_no],[title_name], [title_order], 
                                         [production_no], [production_name], [fiscal_year],[visit_dt],[tickets_sold])
        SELECT '',
               '',
               tbl.[title_no],
               tbl.[title_name],
               '',
               0,
               '',
               @fiscal_year,
               CONVERT(CHAR(10),dte.[return_dt],111),
               0
        FROM [dbo].[LFT_INDIVIDUAL_DATES](@month_start_dt, @week_end_dt) AS dte
             CROSS JOIN @report_titles AS tbl
             LEFT OUTER JOIN [#attendance_table] AS att ON CAST(att.[visit_dt] AS DATE) = CAST(dte.[return_dt] AS DATE)
                                                       AND att.[title_no] = tbl.[title_no]
                                                       AND att.[fiscal_year] = @fiscal_year
         WHERE ISNULL(att.[title_no],0) = 0;

        INSERT INTO [#attendance_table] ([title_group],[title_group_order],[title_no],[title_name], [title_order], 
                                         [production_no], [production_name], [fiscal_year],[visit_dt],[tickets_sold])
        SELECT '',
               '',
               tbl.[title_no],
               tbl.[title_name],
               '',
               0,
               '',
               @prev_fiscal_year,
               CONVERT(CHAR(10),dte.[return_dt],111),
               0
        FROM [dbo].[LFT_INDIVIDUAL_DATES](@prev_month_start_dt, @prev_week_end_dt) AS dte
             CROSS JOIN @report_titles AS tbl
             LEFT OUTER JOIN [#attendance_table] AS att ON CAST(att.[visit_dt] AS DATE) = CAST(dte.[return_dt] AS DATE)
                                                       AND att.[title_no] = tbl.[title_no]
                                                       AND att.[fiscal_year] = @fiscal_year
         WHERE ISNULL(att.[title_no],0) = 0;



    /*  Set the Title Group, Title Group Order, and Title Order for the selected data  */
        
        UPDATE [#attendance_table]
        SET [title_group] =  CASE WHEN [title_no] = @gross_attend_no THEN 'Entire Museum'
                                  WHEN [title_no] IN (@exhibit_halls_no, @butterfly_garden_no, @special_exhibitions_no) THEN 'Exhibits'
                                  WHEN [title_no] IN (@4d_theater_no, @mugar_omni_theater_no, @thrill_ride_no) THEN 'Theaters'
                                  WHEN [title_no] IN (@hayden_planetarium_no, @entertainment_no) THEN 'Planetarium'
                                  ELSE '' END,
            [title_group_order] =  CASE WHEN [title_no] = @gross_attend_no THEN 'A'
                                        WHEN [title_no] IN (@exhibit_halls_no, @butterfly_garden_no, @special_exhibitions_no) THEN 'B'
                                        WHEN [title_no] IN (@4d_theater_no, @mugar_omni_theater_no, @thrill_ride_no) THEN 'C'
                                        WHEN [title_no] IN (@hayden_planetarium_no, @entertainment_no) THEN 'D'
                                        ELSE 'Z' END,
             [title_order] = CASE [title_no]
                             WHEN @gross_attend_no THEN 'A'
                             WHEN @exhibit_halls_no THEN 'B'
                             WHEN @special_exhibitions_no THEN 'C'
                             WHEN @butterfly_garden_no THEN 'D'
                             WHEN @mugar_omni_theater_no THEN 'E'
                             WHEN @4d_theater_no THEN 'F'
                             WHEN @thrill_ride_no THEN 'G'
                             WHEN @hayden_planetarium_no THEN 'H'
                             WHEN @entertainment_no THEN 'I'
                             ELSE 'Z' END

    /*  Shorten Title Names - To Save Space  */

        UPDATE [#attendance_table]
        SET [title_name] =  CASE WHEN [title_no] = @mugar_omni_theater_no THEN 'Omni Theater'
                                 WHEN [title_no] = @hayden_planetarium_no THEN 'Planetarium'
                                 ELSE [title_name] END


    /*  Delete anything not in one of the designated title groups  */

        DELETE FROM [#attendance_table] 
        WHERE [title_group] = '';    

    /*  Make sure there are records in the table for every day of the month  */


        WITH CTE_ALL_DATES (return_dt)
        AS (SELECT return_dt 
            FROM [dbo].[LFT_INDIVIDUAL_DATES](@month_start_dt,@month_end_dt))
        INSERT INTO [#attendance_table] ([title_group],[title_group_order], [title_group_color], [title_no],
                                         [title_name], [title_order], [title_color], [production_no], [production_name], [fiscal_year],[visit_dt], [tickets_sold])
        SELECT DISTINCT att.[title_group],
                        att.[title_group_order],
                        att.[title_group_color],
                        att.[title_no],
                        att.[title_name],
                        att.[title_order],
                        att.[title_color],
                        0,
                        '',
                        @fiscal_year,
                        cte.[return_dt],
                        0
        FROM [CTE_ALL_DATES] AS cte
             CROSS JOIN [#attendance_table] AS att
        WHERE cte.[return_dt] NOT IN (SELECT [visit_dt] FROM [#attendance_table]);

    /*  Assign colors to each title Group  */

        --UPDATE [#attendance_table]
        --SET [title_group_color] = CASE WHEN [title_group] = 'Entire Museum' THEN 'LightSteelBlue'
        --                               WHEN [title_group] = 'Exhibits' THEN 'SeaGreen'
        --                               WHEN [title_group] = 'Theaters' THEN 'LightSteelBlue'              /* Changed to be two shades of blue */
        --                               WHEN [title_group] = 'Planetarium' THEN 'SandyBrown'
        --                               ELSE 'White' END

        UPDATE [#attendance_table]
        SET [title_group_color] = CASE WHEN [title_group] = 'Entire Museum' THEN 'LightSteelBlue'
                                       WHEN [title_group] = 'Exhibits' THEN 'LightSkyBlue'
                                       WHEN [title_group] = 'Theaters' THEN 'LightSteelBlue'
                                       WHEN [title_group] = 'Planetarium' THEN 'LightSkyBlue'
                                       ELSE 'White' END

    /*  Assign specific colors to each title  */

        --UPDATE [#attendance_table]
        --SET [title_color] = CASE [title_no] 
        --                    WHEN @gross_attend_no THEN 'LightSteelBlue'
        --                    WHEN @exhibit_halls_no THEN 'OliveDrab'
        --                    WHEN @butterfly_garden_no THEN 'MediumSeaGreen'
        --                    WHEN @special_exhibitions_no THEN 'PaleGreen'
        --                    WHEN @mugar_omni_theater_no THEN 'LightSteelBlue'              /* Changed to be two shades of blue */
        --                    WHEN @4d_theater_no THEN 'LightSkyBlue'
        --                    WHEN @thrill_ride_no THEN 'Gainsboro'
        --                    WHEN @hayden_planetarium_no THEN 'LightCoral'
        --                    WHEN @entertainment_no THEN 'Salmon'
        --                    ELSE 'White' END

        UPDATE [#attendance_table]
        SET [title_color] = CASE [title_no] 
                            WHEN @gross_attend_no THEN 'LightSteelBlue'
                            WHEN @exhibit_halls_no THEN 'LightSkyBlue'
                            WHEN @butterfly_garden_no THEN 'LightSkyBlue'
                            WHEN @special_exhibitions_no THEN 'LightSkyBlue'
                            WHEN @mugar_omni_theater_no THEN 'LightSteelBlue'
                            WHEN @4d_theater_no THEN 'LightSteelBlue'
                            WHEN @thrill_ride_no THEN 'LightSteelBlue'
                            WHEN @hayden_planetarium_no THEN 'LightSkyBlue'
                            WHEN @entertainment_no THEN 'LightSkyBlue'
                            ELSE 'White' END

    /*  Change the previous fiscal year's dates to that they line up with the proper current fiscal year date in the matrix */

        --ADD A YEAR
        UPDATE [#attendance_table]
        SET [visit_dt] = DATEADD(YEAR,1,[visit_dt])
        WHERE [fiscal_year] = @prev_fiscal_year;

        --THE DATEDIFF IN THIS COMMAND SHOULD RESULT IN A NEGATIVE NUMBER SO THAT NUMBER OF DAYS ARE SUBTRACTED
        UPDATE [#attendance_table]
        SET [visit_dt] = DATEADD(DAY,DATEDIFF(DAY,DATEADD(YEAR,1,@prev_month_start_dt),@month_start_dt),[visit_dt])
        WHERE [fiscal_year] = @prev_fiscal_year;

    /*  Identify dates that are past the degignated week ending date  */

        UPDATE [#attendance_table]
        SET [past_report_date] = 'Y'
        WHERE CAST([visit_dt] AS DATE) > CAST(@week_ending AS DATE)

    /*  Set anything past the week ending date to zero  */

        UPDATE [#attendance_table] 
        SET [tickets_sold] = 0.00
        WHERE [visit_dt] > @week_end_dt
          AND [fiscal_year] = @fiscal_year
          AND [tickets_sold] > 0

    /*  Identify Week Numbers Within the Month  */   
    
        DECLARE @first_month INT = 0,
                @second_month INT = 0

        SELECT @first_month = DATEPART(MONTH,@month_start_dt)
        SELECT @second_month = DATEPART(MONTH,@week_end_dt)

        UPDATE [#attendance_table]
        SET [visit_week] = CASE WHEN DATEPART(MONTH,[visit_dt]) = DATEPART(MONTH,@month_start_dt) AND DATEPART(DAY,[visit_dt]) < @first_monday THEN 0
                                WHEN DATEPART(MONTH,[visit_dt]) = DATEPART(MONTH,@month_start_dt) AND DATEPART(DAY,[visit_dt]) BETWEEN @first_monday AND (@first_monday + 6) THEN 1
                                WHEN DATEPART(MONTH,[visit_dt]) = DATEPART(MONTH,@month_start_dt) AND DATEPART(DAY,[visit_dt]) BETWEEN (@first_monday + 7) AND (@first_monday + 13) THEN 2
                                WHEN DATEPART(MONTH,[visit_dt]) = DATEPART(MONTH,@month_start_dt) AND DATEPART(DAY,[visit_dt]) BETWEEN (@first_monday + 14) AND (@first_monday + 20) THEN 3
                                WHEN DATEPART(MONTH,[visit_dt]) = DATEPART(MONTH,@month_start_dt) AND DATEPART(DAY,[visit_dt]) BETWEEN (@first_monday + 21) AND (@first_monday + 27) THEN 4
                                ELSE 5 END;


    /*  Adjust week number when the date range spans into two different months  */

        IF @second_month <> @first_month AND DATEPART(WEEKDAY,@month_end_dt) <> 1  BEGIN

            UPDATE [#attendance_table]
            SET [visit_week] = (SELECT TOP 1 [visit_week] 
                                FROM [#attendance_table] 
                                WHERE [visit_dt] = @month_end_dt 
                                  AND [fiscal_year] = @fiscal_year
                                ORDER BY [visit_week])
            WHERE [visit_dt] > @month_end_dt
                                  
        END
        

    /* Set title name to production name for special exhibits  */

        IF EXISTS (SELECT * FROM [#attendance_table] WHERE [title_no] = @special_exhibitions_no) BEGIN

            IF @month_start_dt BETWEEN '6-15-2019' AND '1-10-2020' OR @month_end_dt BETWEEN '6-15-2019' AND '1-10-2020'
                SELECT @special_exhibitions_name = 'BdyWld'
            ELSE
                SELECT @special_exhibitions_name = ISNULL(MAX([production_name]),'') 
                                                   FROM [#attendance_table] 
                                                   WHERE [title_no] = @special_exhibitions_no

            IF ISNULL(@special_exhibitions_name,'') <> ''
                UPDATE [#attendance_table]
                SET [title_name] = @special_exhibitions_name
                WHERE [title_no] = @special_exhibitions_no
        
        END
               
    /*  If no tickets sold for either year to any title, delete that title  */
        DELETE FROM [#attendance_table]
        WHERE [title_no] IN (SELECT [title_no] 
                             FROM [#attendance_table] 
                             GROUP BY title_no 
                             HAVING SUM([tickets_sold]) = 0)



    FINISHED:

        /*  Final Data Set  */

            SELECT [title_group],
                   [title_group_order],
                   [title_group_color],
                   [title_no],
                   [title_name],
                   [title_order],
                   [title_color],
                   [fiscal_year],
                   [visit_dt],
                   DATENAME(WEEKDAY,[visit_dt]) AS [visit_day],
                   [visit_week],
                   LEFT(DATENAME(WEEKDAY,[visit_dt]),3) AS [visit_day_abbrev],
                   [past_report_date],
                   [tickets_sold],
                   FORMAT([visit_dt],'MM/dd') AS sort_day
            FROM [#attendance_table] 

            --WHERE DATEPART(DAY,[visit_dt]) = 8 AND [title_no] = @entertainment_no
            WHERE title_no = @exhibit_halls_no AND [tickets_sold] > 0

            ORDER BY [fiscal_year] DESC, [visit_dt], [title_name]
           
    DONE:

END
GO

EXECUTE [dbo].[LRP_ATTEND_MONTH_TO_DATE] '2-28-2021'
--EXECUTE [dbo].[LRP_ATTEND_MONTH_TO_DATE] '9-8-2019'



--SELECT @fiscal_year AS [cur_fiscal_year], @week_start_dt AS [cur_week_start_dt], @week_end_dt AS [cur_week_end_dt], @month_start_dt AS [cur_month_start_dt], 
--       @month_end_dt AS [cur_month_end_dt], @prev_fiscal_year AS [prv_fiscal], @prev_week_start_dt AS [prv_week_start_dt], @prev_week_end_dt AS [prv_week_end_dt], 
--       @prev_month_start_dt AS [prv_month_start_dt], @prev_month_end_dt AS [prv_month_end_dt], @first_monday AS [first_monday]


