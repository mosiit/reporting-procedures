USE [impresario]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[LRP_BOARD_NightlySummary_WithoutYearlyCalculations]
(
	@customer_no INT
)    
AS

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

-- IF any more NIGHTLY SUMMARIES are ever created that are associated with a YEAR, they will need be manually added to this script. 

SELECT customer_no, 
	[Gift excl GIK First],
	[Outstanding Pledge Balance],
	[GIK Largest],
	[I3 Comprehensive Campaign Total],
	[GIK Latest],
	[Lifetime Innovator Dues],
	[Pledge First],
	[GIK First],
	[Gift excl GIK Latest],
	[Gift excl GIK Largest],
	[Pledge Latest],
	[Lifetime Corp Membership Dues],
	[Comprehensive Lifetime Total],
	[Pledge Largest],
	[Outstanding AF Pledge Balance Current FY]
FROM
(
	SELECT customer_no, section, value
	FROM dbo.LT_NIGHTLY_SUMMARY 
	WHERE customer_no IN (SELECT expanded_customer_no FROM dbo.V_CUSTOMER_WITH_PRIMARY_GROUP WHERE customer_no = @customer_no)
	AND 
	(section = 'Outstanding AF Pledge Balance Current FY'
	OR (section NOT LIKE '%FY%' AND section NOT LIKE '%CY%')
	)
)  AS tbl2Pivot
PIVOT (MAX(value) FOR section IN (
	[Gift excl GIK First],
	[Outstanding Pledge Balance],
	[GIK Largest],
	[I3 Comprehensive Campaign Total],
	[GIK Latest],
	[Lifetime Innovator Dues],
	[Pledge First],
	[GIK First],
	[Gift excl GIK Latest],
	[Gift excl GIK Largest],
	[Pledge Latest],
	[Lifetime Corp Membership Dues],
	[Comprehensive Lifetime Total],
	[Pledge Largest],
	[Outstanding AF Pledge Balance Current FY]
) ) AS SummaryPivot