USE [impresario]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LRP_OVERNIGHT_ORDER_PAYMENTS]
(
	@order_no INT
)

AS
SET NOCOUNT ON 
/*
Created by: D. Jacob 10/30/2015
Purpose: Called by LRP_OVERNIGHT_ORDER_ACK (SP) and used by Overnight_Invoice report to calculate payments made and what 
the remaining balance is.
*/
SELECT pay.pmt_dt, pay.pmt_amt, 
	CASE pm.description
		WHEN 'Ovrnite: Scholarships Clrg' THEN 'Scholarship - ' + pay.notes
		ELSE pm.description
	END AS pay_description, pay.check_no, t.order_no, pay.notes
FROM dbo.T_TRANSACTION t
	INNER JOIN dbo.T_PAYMENT pay
		ON t.transaction_no = pay.transaction_no
	INNER JOIN dbo.TR_PAYMENT_METHOD pm
		ON pay.pmt_method = pm.id
WHERE t.order_no = @order_no

GO