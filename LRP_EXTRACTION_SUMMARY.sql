USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LRP_EXTRACTION_SUMMARY]    Script Date: 2/19/2019 8:45:08 AM ******/
DROP PROCEDURE [dbo].[LRP_EXTRACTION_SUMMARY]
GO

/****** Object:  StoredProcedure [dbo].[LRP_EXTRACTION_SUMMARY]    Script Date: 2/19/2019 8:45:08 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[LRP_EXTRACTION_SUMMARY]
	--@StartDate DATETIME,
	--@EndDate DATETIME,
	@TimePeriodMins INT
AS
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

DECLARE @max INT = 0,
		@cnt INT = 0;

DECLARE @StartExt	 INT,
		@EndExt		 INT,
		@DLNbr		 INT,
		@ExtractNbr	 INT,
		@DateStamp	 DATE,
		@Status		 VARCHAR(12),
		@Description VARCHAR(50);

DECLARE @tblExts TABLE (RowNbr INT,
						uid NUMERIC(10, 0),
						dl_no INT,
						process_segment VARCHAR(20),
						comment VARCHAR(255),
						ExtractNbr INT,
						StartOfNextExtract INT,
						EndOfNextExtract INT,
						ExtDateTime DATETIME,
						DateStamp VARCHAR(10));

DECLARE @tblResults TABLE (DateRun DATE,
						   StartTime DATETIME,
						   EndTime DATETIME,
						   Duration INT,
						   ExtractNbr INT,
						   ExtractDescription VARCHAR(50),
						   DLNbr INT,
						   RunStatus VARCHAR(12),
						   LastUpdatedBy VARCHAR(10));

INSERT INTO @tblExts
			SELECT ROW_NUMBER () OVER (ORDER BY uid) AS 'RowNbr',
				   uid,
				   dl_no,
				   process_segment,
				   comment,
				   CONVERT (
					   INT,
					   CASE
							WHEN PATINDEX ('%: Final Resul', el.process_segment) = 0 THEN
								REPLACE (REPLACE (el.process_segment, ': Final Result', ''), 'DL ', '')
							ELSE REPLACE (REPLACE (el.process_segment, ': Final Resul', ''), 'DL ', '')
					   END
				   ) AS 'ExtractNbr',
				   uid + 1 AS 'Start of Next Extract',
				   LEAD (uid, 1, 0) OVER (ORDER BY uid) AS 'End of Next Extract',
				   [cur_datetime],
				   CAST([cur_datetime] AS DATE)
			FROM   [BIExtract].[TS_EVENTLOG] el
			WHERE  el.process_segment LIKE 'DL%Final%'
			--AND	   (
			--	 el.cur_datetime >= @StartDate
			--	AND el.cur_datetime <=  DATEADD(dd, 1, @EndDate)
			--)
			AND	   DATEDIFF (mi, el.cur_datetime, GETDATE ()) <= @TimePeriodMins;

--SELECT * FROM @tblExts

SELECT @max = MAX (RowNbr)
FROM   @tblExts;
SELECT @cnt = 1;

WHILE @cnt < @max
	BEGIN
		SELECT @StartExt = StartOfNextExtract,
			   @EndExt = EndOfNextExtract,
			   @DateStamp = DateStamp,
			   @Status = CASE
							  WHEN comment LIKE 'Successful%' THEN 'Success'
							  WHEN comment LIKE 'Error%' THEN 'Failed'
							  ELSE 'Unknown'
						 END
		FROM   @tblExts
		WHERE  RowNbr = @cnt;

		SELECT @DLNbr = dl_no,
			   @ExtractNbr = ExtractNbr
		FROM   @tblExts
		WHERE  RowNbr = @cnt + 1;

		INSERT INTO @tblResults
					SELECT	 @DateStamp AS 'Date Run',
							 MIN (el.cur_datetime) AS 'Start Time',
							 MAX (el.cur_datetime) AS 'End Time',
							 DATEDIFF (MINUTE, MIN (el.cur_datetime), MAX (el.cur_datetime)) AS 'Duration (Mins)',
							 @ExtractNbr AS 'Extract Number',
							 hdr.[description] AS 'Description',
							 el.dl_no AS 'DL Number',
							 @Status AS 'Status',
							 hdr.last_updated_by AS 'Last Updated By'
					FROM	 [BIExtract].[TS_EVENTLOG] el
							 JOIN [BIExtract].[T_KA_HEADER] hdr
							   ON el.dl_no = hdr.ka_no
					WHERE	 el.uid BETWEEN @StartExt AND @EndExt
					AND		 el.dl_no = @DLNbr
					AND		 DATEDIFF (DAY, el.cur_datetime, @DateStamp) = 0
					GROUP BY el.dl_no,
							 hdr.last_updated_by,
							 hdr.[description];

		SELECT @cnt = @cnt + 1;
	END;

SELECT	 StartTime,
		 EndTime,
		 DATEDIFF (SECOND, [StartTime], [EndTime]) AS 'Duration',
		 ExtractNbr AS 'Extract Number',
		 ExtractDescription AS 'Description',
		 DLNbr AS 'DL Number',
		 RunStatus AS 'Status',
		 CASE
			  WHEN DATEPART (HOUR, StartTime) = 12 THEN CONVERT (VARCHAR(2), DATEPART (HOUR, StartTime)) + ' PM'
			  WHEN DATEPART (HOUR, StartTime) < 12 THEN CONVERT (VARCHAR(2), DATEPART (HOUR, StartTime)) + ' AM'
			  WHEN DATEPART (HOUR, StartTime) >= 12 THEN CONVERT (VARCHAR(2), DATEPART (HOUR, StartTime) - 12) + ' PM'
		 END AS 'Time Slot',
		 LastUpdatedBy AS 'Last Updated By'
FROM	 @tblResults
ORDER BY DateRun DESC;


GO


