USE [impresario]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[LRP_BOARD_ResearchNotes]
(
	@customer_no INT,
	@NoteType VARCHAR(30)
)
AS

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

--DESCRIPTION values
--ConfidentialNominatingBriefing
--Event Research Note
--Non Confidential Briefing

SELECT cust.customer_no, cust.cust_notes_no,  
	CASE 
		WHEN typ.description = 'ConfidentialNominatingBriefing' THEN 'CONFIDENTIAL Nominating Briefing'
		ELSE typ.description
	END notesType, 
	dbo.LFS_BOARD_GetResearchNotes(cust.cust_notes_no) notes
FROM dbo.TX_CUST_NOTES cust
	INNER JOIN [TR_CUST_NOTES_TYPE] typ
		ON cust.note_type = typ.id
WHERE cust.customer_no = @customer_no
AND typ.description = @NoteType

