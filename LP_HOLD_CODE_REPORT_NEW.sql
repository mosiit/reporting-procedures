USE [impresario]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER  PROCEDURE [dbo].[LP_HOLD_CODE_REPORT_NEW]
	(
	@season INT,
	@prod_season_no INT = NULL,
	@perf_no INT = NULL,
	@perf_start_dt DATETIME = NULL,
	@perf_end_dt DATETIME = NULL,
	@hc_start_dt DATETIME = NULL,
	@hc_end_dt DATETIME = NULL,
	@hc_str VARCHAR(4000) = NULL,
	@show_avail_only CHAR(1) = 'Y'
	)
AS 

/***************************************************************************************
-- COPIED FROM RP_HOLD_CODE_REPORT_NEW	
-- WK 
-- 09/21/2016

PROC:		RP_HOLD_CODE_REPORT_NEW														  
BY:		CWR																	  
DATE:		7/18/2005
INPUT:		Season, Performance Number, Perf Date Range Start, Perf Date Range End,  
			Mode
RETURNS:	total held and current held per performance

Modified CWR 7/22/2005 -- join to #end_seats in last select was not quite right,
	so that some seats were being missed
Modified CWR 12/2/2005 -- when only perf date range was entered, season parameter
	was not being used
Modified 1/20/2006 -- previous fix was incorrect; now using new season column
	in vs_perf
Modified CWR 1/21/2006 -- added owner name to table references
MOdified RWC 5/27/2008 FP568.  -- increased hc_str to 4000 characters
Modified RWC 9/20/2009 FP1361. -- commented out portion that adds end of day time to perf_end_dt
	as this is already handled in the report setup.
Modified WK 9/20/2016 #WK920 -- Conversion from DATETIME to SMALLDATETIME not reflect the number of seconds and milliseonds 
Modified WK 9/21/2016 #WK921 -- Added a join table LV_PRODUCTION_ELEMENTS_PERFORMANCE_STATUS and put where clause to select Active Zones
rp_hold_code_report_new @season = 1, @prod_season_no = null, @perf_no = null,
	@perf_start_dt = '1900-1-1', @perf_end_dt = '2999-12-31'

exec RP_HOLD_CODE_REPORT_NEW 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y'

exec dbo.RP_HOLD_CODE_REPORT_NEW 2, 69, NULL, NULL, NULL, NULL, NULL, NULL, 'Y'

****************************************************************************************/

SET NOCOUNT ON

---CREATE TEMP TABLE

--select	@perf_start_dt = '2005-1-1', @perf_end_dt = '2005-2-15', @perf_no = null, @show_avail_only = 'N'

CREATE TABLE #tperf (
		perf_no INT,
		perf_code VARCHAR(10),
		perf_dt DATETIME)

---GET DESIRED PERFORMANCES
---If we have a prod_season_no, then we're doing it for all performances of a particular event
IF @prod_season_no is not null and @prod_season_no > 0
  BEGIN
	INSERT #tperf(perf_no, perf_code, perf_dt) 
		SELECT	perf_no, perf_code, perf_dt
		FROM	[dbo].vs_perf 
		WHERE	prod_season_no = @prod_season_no
	
	GOTO ReturnResults
  END

---If we're doing this for only one performance, just put that perf in the temp table
IF ISNUMERIC(@perf_no) <> 0 AND @perf_no > 0  --We're doing this for a single perf
  BEGIN	
	INSERT #tperf(perf_no, perf_code, perf_dt)
		SELECT	perf_no, perf_code, perf_dt
		FROM	[dbo].vs_perf 
		WHERE	perf_no = @perf_no
	
	GOTO ReturnResults
  END
  
--_If we have a date range, then select the perfs for that range
IF ISDATE(@perf_start_dt) <> 0 AND ISDATE(@perf_end_dt) <> 0 --We're doing this for a range of performances
  BEGIN
	--Fix @perf_end_dt so that it goes to 11:59:59 pm on ending date so we include perfs on that date  --FP1361.  Handled in report setup
--	SET @perf_end_dt = DATEADD(hour, 23, @perf_end_dt)
--	SET @perf_end_dt = DATEADD(minute, 59, @perf_end_dt)
--	SET @perf_end_dt = DATEADD(second, 59, @perf_end_dt)
	
	INSERT #tperf(perf_no, perf_code, perf_dt)
		SELECT	perf_no, perf_code, perf_dt
		FROM	[dbo].vs_perf 
		WHERE	perf_dt BETWEEN @perf_start_dt AND @perf_end_dt
			and season = @season
	
	GOTO ReturnResults
  END

IF IsNull(@season, 0) > 0 --We're doing this for a whole season
  BEGIN
	
	INSERT #tperf(perf_no, perf_code, perf_dt)
		SELECT	perf_no, perf_code, perf_dt
		FROM	[dbo].vs_perf 
		WHERE	season = @season

	
	GOTO ReturnResults
  END


---If we've gotten this far, no option is taken and we just clean up and end
--GOTO EndProc

---RETURN RESULTSET
ReturnResults:

--select * from #tperf

-- all seats in a temp table
select	a.perf_no, a.zone_no, a.zmap_no, c.hc_no, avail = Case When a.seat_status in (4, 6) Then 'Y' Else 'N' End, b.*, 
c.start_dt AS hold_start_dt, c.end_dt AS hold_end_dt
INTO #cwr1
from	[dbo].tx_perf_seat a
	JOIN [dbo].t_seat b ON a.seat_no = b.seat_no
	JOIN [dbo].tx_perf_hc c ON a.seat_no = c.seat_no and a.perf_no = c.perf_no 
	JOIN [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE_STATUS] p ON a.perf_no = p.performance_no and a.zmap_no = p.zone_map_no and a.zone_no = p.zone_no  -- #WK921
	and total_active_prices > 0 -- #WK921 
	JOIN #tperf d ON a.perf_no = d.perf_no 
where	c.priority = 1 
	and (IsNull(@show_avail_only, 'Y') = 'N' OR a.seat_status in (4, 6))
	and (charindex(',' + convert(varchar, c.hc_no) + ',' , ',' + @hc_str + ',') > 0 or ISNULL(@hc_str, '') = '')
	and c.start_dt between IsNull(@hc_start_dt, '1900-1-1') and IsNull(@hc_end_dt, '2999-12-31')



-- temp table 1 (a) finds the start seat of each block (where there's no lower logical_seat_num in the same logical_seat_row
select section, seat_row, start_seat = seat_num, logical_seat_row, start_logical = logical_seat_num, zmap_no, zone_no, perf_no, hc_no, avail, a.hold_start_dt, a.hold_end_dt
		into #start_seats
		from	#cwr1 a 
			JOIN [dbo].tr_section c ON a.section = c.id 
		where	not exists 
				(select * from #cwr1 where perf_no = a.perf_no and avail = a.avail and section = a.section and zmap_no = a.zmap_no and zone_no = a.zone_no and hc_no = a.hc_no and seat_row = a.seat_row 
				and logical_seat_row = a.logical_seat_row and logical_seat_num + 1 = a.logical_seat_num AND hold_start_dt = a.hold_start_dt AND hold_end_dt = a.hold_end_dt ) 



-- derived table 2 finds the end seat of each block (where there's no higher logical_seat_num in the same logical_seat_row
SELECT	section, seat_row, end_seat = seat_num, logical_seat_row, end_logical = logical_seat_num, a.zmap_no, a.zone_no, perf_no, a.avail, a.hold_start_dt, a.hold_end_dt
		INTO #end_seats
		FROM	#cwr1 a 
			JOIN [dbo].tr_section c ON a.section = c.id
		WHERE	NOT EXISTS (SELECT * FROM #cwr1 WHERE perf_no = a.perf_no AND avail = a.avail AND section = a.section AND zmap_no = a.zmap_no AND zone_no = a.zone_no AND hc_no = a.hc_no AND seat_row = a.seat_row 
				AND logical_seat_row = a.logical_seat_row AND logical_seat_num - 1 = a.logical_seat_num AND hold_start_dt = a.hold_start_dt AND hold_end_dt = a.hold_end_dt) 

CREATE CLUSTERED INDEX i1 ON #start_seats(perf_no, section, logical_seat_row)
CREATE CLUSTERED INDEX i2 ON #end_seats(perf_no, section, logical_seat_row);

-- here's the output
WITH HoldCodeCTE 
AS 
(
	SELECT 	a.perf_no,
		d.perf_code,
		d.perf_dt,
		perf_desc = e.description,
		'section' = c.description,  
		'row' = a.seat_row, 
		'start_seat' = a.start_seat, 
		'end_seat' = b.end_seat, 
		zone = g.description,
		seat_count = (SELECT COUNT(*) FROM #cwr1 WHERE perf_no = a.perf_no AND avail = a.avail AND 
				section = a.section AND zmap_no = a.zmap_no AND zone_no = a.zone_no AND logical_seat_row = a.logical_seat_row AND logical_seat_num BETWEEN
				a.start_logical AND b.end_logical),
		f.hc_legend, 
		hc_desc = f.description,
		f.hc_no,
		a.avail,
		blackout_ind = ISNULL(f.blackout_ind, 'N') ,
		a.hold_start_dt,
		a.hold_end_dt
		FROM	#start_seats a
		JOIN #end_seats b ON a.perf_no = b.perf_no AND a.avail = b.avail AND a.section = b.section AND a.zmap_no = b.zmap_no AND a.zone_no = b.zone_no AND a.logical_seat_row = b.logical_seat_row
				AND b.hold_start_dt = a.hold_start_dt AND ISNULL(b.hold_end_dt, 0) = ISNULL(a.hold_end_dt, 0)
				AND b.end_logical = (SELECT MIN(end_logical) FROM #end_seats 
					WHERE perf_no = a.perf_no AND avail = b.avail AND zmap_no = a.zmap_no AND zone_no = a.zone_no 
					AND section = a.section AND logical_seat_row = a.logical_seat_row AND seat_row = a.seat_row AND end_logical >= a.start_logical)
		JOIN 	[dbo].tr_section c ON a.section = c.id
		JOIN 	[dbo].t_perf d ON a.perf_no = d.perf_no
		JOIN 	[dbo].t_inventory e ON a.perf_no = e.inv_no
		JOIN 	[dbo].t_hc f ON a.hc_no = f.hc_no
		JOIN [dbo].t_zone g ON a.zone_no = g.zone_no AND d.zmap_no = g.zmap_no
	--order by a.perf_no, f.hc_legend, c.print_sequence, c.description, a.seat_row, a.start_seat
)
SELECT 	perf_no,
		perf_code,
		perf_dt,
		perf_desc,
		zone,
		hc_legend, 
		hc_desc,
		hc_no,
		avail,
		blackout_ind,
		CAST(hold_start_dt AS DATE) hold_start_dt,
		CAST(hold_end_dt AS DATE) AS hold_end_dt,
		SUM(seat_count) AS seat_count
FROM HoldCodeCTE
GROUP BY perf_no,
		perf_code,
		perf_dt,
		perf_desc,
		zone,
		hc_legend, 
		hc_desc,
		hc_no,
		avail,
		blackout_ind,
		CAST(hold_start_dt AS DATE),
		CAST(hold_end_dt AS DATE)

RETURN
GO

