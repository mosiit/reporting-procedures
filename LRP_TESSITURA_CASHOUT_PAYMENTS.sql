USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_TESSITURA_CASHOUT_PAYMENTS]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LRP_TESSITURA_CASHOUT_PAYMENTS]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LRP_TESSITURA_CASHOUT_PAYMENTS]
        @report_dt datetime,
        @report_operator varchar(10),
        @machine_locations varchar(4000) = '' 
AS BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

    /*  Procedure variables  */

        DECLARE @report_msg varchar(100)        SELECT @report_msg = ''
        DECLARE @location_list TABLE ([location_no_str] VARCHAR(20), [location_no] INT)
        DECLARE @machine_list TABLE ([machine_name] VARCHAR(50))

    /*  Check Parameters  - Null Date = Today, Null Operator = All  */

        SELECT @report_dt = ISNULL(@report_dt, GETDATE())
        SELECT @report_operator = ISNULL(@report_operator,'All')
    
        SELECT @machine_locations = ISNULL(@machine_locations,'')

    /*  @tblPaymentsRaw: Table to hold all the raw payment data for the specific data and operator id passed to the procedure.  */

        DECLARE @tblPaymentsRaw TABLE ([transaction_no] int, [customer_no] int, [payment_date] char(10), [payment_time] varchar(8), [payment_amount] decimal(18,2), 
                                       [payment_method_name] varchar(30), [payment_type_name] varchar(30), [account_no] VARCHAR(30), [check_no] VARCHAR(30), [check_name] varchar(100), 
                                       [card_expiry_dt] varchar(30), [auth_no] varchar(50), [payment_operator] varchar(10), [payment_operator_first_name] varchar(50), 
                                       [payment_operator_last_name] varchar(50), [payment_operator_location] varchar(30), [batch_no] INT, [transaction_machine] VARCHAR(50))

    /* @tblPaymentsRecorded: Table to hold all recorded payment information for the specific date and operator id passed to the procedure.
                             This table contains data that is aggregated from the data in the @tblPaymentsRaw temp table  */

        DECLARE @tblPaymentsRecorded TABLE ([payment_date] char(10), [payment_operator] varchar(10), [payment_type_name] varchar(30), [payment_amount_Recorded] decimal(18,2))

    /* @tblPaymentsActual: Table to all all the actual payment information for the specific date and operator id passed to the procedure.
                           This table contains data from LT_CASH_OUT_DATA local table populated with the Cashout Data Entry report  */

        DECLARE @tblPaymentsActual TABLE ([payment_date] char(10), [payment_operator] varchar(10), [payment_type_name] varchar(30), [payment_amount_actual] decimal(18,2))

    /*  @tblPaymentsFinal: Table to contain the final payment information for the specific date and operator id passed to the procedure.
                           By the time the procedure is done with the data in this table, it has been cleaned up and is ready to be pulled into the report.  */

        DECLARE @tblPaymentsFinal TABLE ([payment_date] char(10), [payment_operator] varchar(10), [payment_header_no] int, [payment_header_name] varchar(30), [payment_group_no] int, 
                                         [payment_group_name] varchar(30), [payment_type_name] varchar(30), [payment_amount_recorded] decimal(18,2), [payment_amount_actual] decimal(18,2), 
                                         [payment_amount_variance] decimal(18,2), [report_msg] varchar(100))

    /*  Parse machine_locations parameter  */

            INSERT INTO @location_list ([location_no_str]) 
            SELECT [item] FROM [dbo].[LFT_SPLIT_DILEMETED_STRING] (@machine_locations, ',')
            
            DELETE FROM @location_list WHERE [dbo].[FS_isReallyNumeric] ([location_no_str]) = 0
            
            UPDATE @location_list SET [location_no] = CONVERT(INT,[location_no_str])
            
            INSERT INTO @machine_list ([machine_name])
            SELECT [machine_name] FROM [dbo].[TX_MACHINE_LOCATION] WHERE [location] IN (SELECT [location_no] FROM @location_list)

    /*  The raw data is pulled directly from the LV_RPT_CASHOUT_PAYMENTS view created specifically for this report.
        A single day's data for a single operator is pulled and used throughout the report.  */

        INSERT INTO @tblPaymentsRaw        
        SELECT [transaction_no], [customer_no], [payment_date], [payment_time], sum([payment_amount]), [payment_method_name], [payment_type_name], [account_no], [check_no], [check_name], 
               [card_expiry_dt], [auth_no], [payment_operator], [payment_operator_first_name], [payment_operator_last_name], [payment_operator_location], [batch_no], [transaction_machine]
        FROM [dbo].[LV_RPT_CASHOUT_PAYMENTS]
        WHERE [payment_date] = convert(char(10),@report_dt,111)
        GROUP BY [transaction_no], [customer_no], [payment_date], [payment_time], [payment_method_name], [payment_type_name], [account_no], 
                 [check_no], [check_name], [card_expiry_dt], [auth_no], [payment_operator], [payment_operator_first_name], [payment_operator_last_name], [payment_operator_location], 
                 [batch_no], [transaction_machine]

        IF @report_operator <> '' DELETE FROM @tblPaymentsRaw WHERE [payment_operator] <> @report_operator

    /*  If @machine_list contains values, delete from @tblPaymentsRaw that are not from the machines in @machine_list  */

        DELETE FROM @tblPaymentsRaw WHERE [transaction_machine] NOT IN (SELECT [machine_name] FROM @machine_list)

    /*  If no raw data found, jump to the end of the report */

        IF not exists (SELECT * FROM @tblPaymentsRaw) GOTO FINISHED

    /*  Hard-coded data manipulation: Certain "other" payment type names are changed to specific payment types based on their payment method name.
        This allows things like CityPASS, invoices, and Interdepartmental Transfers to be separated out into their own types on the report.  */

        UPDATE @tblPaymentsRaw SET [payment_type_name] = 'Gift Certificate' WHERE [payment_method_name] like '%Gift Cert%'

        UPDATE @tblPaymentsRaw SET [payment_type_name] = 'Invoice' WHERE [payment_type_name] = 'Other' and [payment_method_name] like '%Invoice%'

        UPDATE @tblPaymentsRaw SET [payment_type_name] = 'School and Group Refunds' WHERE [payment_method_name] = 'School and Group Refunds'

        UPDATE @tblPaymentsRaw SET [payment_type_name] = 'Interdepartmental Transfer' WHERE [payment_method_name] = 'Interdepartmental Transfer'

        UPDATE @tblPaymentsRaw SET [payment_type_name] = 'Manual Credit Card' WHERE [payment_type_name] = 'Other' and [payment_method_name] like 'zManual Card%'

        UPDATE @tblPaymentsRaw SET [payment_type_name] = [payment_method_name] WHERE [payment_type_name] = 'Other' --and [payment_method_name] like '%CityPASS%'

    /*  The recorded payments from the @tblPaymentsRaw temp table are aggregated into their specific payment types and stored in the @tblPaymentsRecorded temp table.  */

        INSERT INTO @tblPaymentsRecorded ([payment_date], [payment_operator], [payment_type_name], [payment_amount_Recorded])
                    SELECT [payment_date], [payment_operator], [payment_type_name], sum([payment_amount]) 
                    FROM @tblPaymentsRaw
                    GROUP BY [payment_date], [payment_operator], [payment_type_name]

    /*  The actual payments from the LT_CASH_OUT_DATA local table are aggregated and placed into the @tblPaymentsActual temp table 
        for the specific date and operator being reported on.  */

        INSERT INTO @tblPaymentsActual ([payment_date], [payment_operator], [payment_type_name], [payment_amount_actual])
                SELECT [payment_date], [payment_operator], [payment_type_name], sum([payment_amount])
                FROM [dbo].[LT_CASH_OUT_DATA] 
                WHERE [payment_date] = convert(char(10),@report_dt,111) and [payment_operator] = @report_operator
                GROUP BY [payment_date], [payment_operator], [payment_type_name]
     
     /*  The data from @tblPaymentsRecorded and @tblPaymentsActual are combined into the @tblPaymentsFinal temp table.  */

        INSERT INTO @tblPaymentsFinal
        SELECT act.[payment_date], act.[payment_operator], 0, '', 0, '', act.[payment_type_name], IsNull(rec.[payment_amount_recorded], 0.00), IsNull(act.[payment_amount_actual], 0.00), 0.00, ''
        FROM @tblPaymentsActual as act
             LEFT OUTER JOIN @tblPaymentsRecorded as rec ON rec.[payment_type_name] = act.[payment_type_name]

    /*  Nothing to cashout for credit cards anymore so the recorded is always the actual  */

        UPDATE @tblPaymentsFinal SET [payment_amount_actual] = [payment_amount_recorded] WHERE [payment_type_name] = 'Credit Card'

    /*  Retrieve Refund total from the Raw Data These are refunds that are not school and group refund check request.  They are actual money given back out of the drawer.  */

        UPDATE @tblPaymentsFinal 
        SET [payment_amount_recorded] = (SELECT ISNULL(SUM([payment_amount]), 0.00)
                                         FROM @tblPaymentsRaw 
                                         WHERE [payment_method_name] NOT IN ('School and Group Refunds', 'On Acct - Scholarship') 
                                           AND [payment_amount] < 0.00)
        WHERE [payment_type_name] = 'Refunds'

    /*  Calculate the variances (if any)  */

        UPDATE @tblPaymentsFinal SET [payment_amount_variance] = ([payment_amount_actual] - [payment_amount_recorded])

    /*  If there are any records in the @tblPaymentsRecorded table that did not make it over to the @tblPaymentsFinal table becuase they did not have a 
        corresponding record in @tblPaymentsActual, those payments are now moved over with 0.00 for the actual amount.  These are payments that need to 
        be on the report but are not accounted for with the amount entered through the data entry report.  */

        INSERT INTO @tblPaymentsFinal
        SELECT [payment_date], [payment_operator], 0, '', 0, '', [payment_type_name], [payment_amount_recorded], 0.00, 0.00, ''
        FROM @tblPaymentsRecorded 
        WHERE [payment_type_name] not in (SELECT [payment_type_name] FROM @tblPaymentsFinal)

    /*  CityPASS should not be counted in with the payments  */

        DELETE FROM @tblPaymentsFinal WHERE [payment_type_name] like '%CityPASS%'

    /*  Payments are separated into three different groups that determine how and in what order they appear on the report.
            1. Deposit: Cash, checks, and gift certificates that are a part of the nightly deposit
            2. Other Reconcile: Other payment types accounted for with amounts entered through the data entry report.
            3. Manual Reconcile (Everything else): Same group as #4 and includes everything remaining that has not been assigned a group  */

        UPDATE @tblPaymentsFinal SET [payment_group_no] = 1, [payment_group_name] = 'Deposit' WHERE [payment_type_name] in ('Cash','Check','Gift Certificate')
        UPDATE @tblPaymentsFinal SET [payment_group_no] = 2, [payment_group_name] = 'Other Reconcile' WHERE [payment_type_name] in ('Credit Card','Invoice', 'Other', 'Refunds')
        UPDATE @tblPaymentsFinal SET [payment_group_no] = 3, [payment_group_name] = 'Manual Reconcile' WHERE [payment_group_no] = 0

    /*  Payments are separated into two different headers that determine how and in what order they appear on the report
            1. Drawer
            2. Other  */

        UPDATE @tblPaymentsFinal SET [payment_header_no] = 1, [payment_header_name] = 'Drawer' WHERE [payment_group_name] in ('Deposit','Other Reconcile')
        UPDATE @tblPaymentsFinal SET [payment_header_no] = 2, [payment_header_name] = 'Other' WHERE [payment_header_no] = 0

    FINISHED:

        IF NOT EXISTS (SELECT * FROM @tblPaymentsFinal)
            INSERT INTO @tblPaymentsFinal VALUES  (CONVERT(CHAR(10),@report_dt,111), @report_operator, 0, '', 0, '', '', 0.00, 0.00, 0.00, 'No payments found')
       
        SELECT [payment_date], [payment_operator], [payment_header_no], [payment_header_name], [payment_group_no], [payment_group_name], [payment_type_name], 
               [payment_amount_recorded], [payment_amount_actual], [payment_amount_variance], [report_msg]
        FROM @tblPaymentsFinal 
        ORDER BY [payment_operator], [payment_group_no], [payment_group_name], [payment_type_name]

END
GO

GRANT EXECUTE ON [dbo].[LRP_TESSITURA_CASHOUT_PAYMENTS] TO impusers
GO

--EXECUTE [dbo].[LRP_TESSITURA_CASHOUT_PAYMENTS] @report_dt = '12-12-2018', @report_operator = 'schals00', @machine_locations = '15,16'

