USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_ADV_CAMPAIGN_ACTIVITY_STAFF]') AND type IN (N'P', N'PC'))
BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_ADV_CAMPAIGN_ACTIVITY_STAFF] AS' 
    EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LRP_ADV_CAMPAIGN_ACTIVITY_STAFF] TO impusers'
END
GO

/*      LRP_ADV_CAMPAIGN_ACTIVITY_STAFF
        Pulls information for goals, numbers, and YTD percentages for all step activity, vists, ask/yield amounts, and actual contributions taken in.

        NOTE: Contribution amounts are based on what's in The Plan record under cont_amount

*/
ALTER PROCEDURE [dbo].[LRP_ADV_CAMPAIGN_ACTIVITY_STAFF]
WITH RECOMPILE AS BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

    /* Procedure Variables  */

        --These variables change how the data is processed.  
        --If decisions change as to which data to include, change these and the code itself does not need to be changed
        DECLARE @use_ask_amount CHAR(1) = 'N'           --Change to Y to use ask amount instead of goal amount
        DECLARE @include_zero_asks CHAR(1) = 'N'        --Change to Y to include plans with a contribution amount but no ask or goal amount

        --Hard coded for the campaign (may change)
        DECLARE @overall_campaign_no INT = 1, @overall_campaign_name VARCHAR(30) = 'Campaign16';
        
        --Determine current fiscal year, fiscal start date, fiscal end date, and current date
        DECLARE @fiscal_year INT = 2020
        --DECLARE @fiscal_year INT = dbo.LF_GetFiscalYear(GETDATE())
        DECLARE @fiscal_start_dt DATETIME = [dbo].[LF_GetFiscalYearStartDt](@fiscal_year)
        DECLARE @fiscal_end_dt DATETIME = [dbo].[LF_GetFiscalYearEndDt](@fiscal_year)
        DECLARE @fiscal_end_dt_formatted VARCHAR(50) = FORMAT(@fiscal_end_dt,'MMMM d, yyyy','en-US')
        DECLARE @current_dt DATETIME = GETDATE()
        IF @current_dt > @fiscal_end_dt SELECT @current_dt = @fiscal_end_dt

        DECLARE @visit_goal_kw_no INT = -1,
                @contact_goal_kw_no INT = -2,       --NOTE KEYWORD NUMBERS WILL CHANGE WHEN
                @ask_goal_kw_no INT = -3,           --THEY ARE ACTUALLY CREATED IN THE T_KEYWORD_TABLE
                @actual_goal_kw_no INT = -4
                
                /*  THESE ARE HERE FOR TESTING ONLY  */        
                DECLARE @hard_coded_contact_goal INT = 500, @hard_coded_visit_goal INT = 100
                DECLARE @hard_coded_ask_goal DECIMAL(18,2) = 10000000.00, @hard_coded_actual_goal DECIMAL(18,2) = 3000000.00

        DECLARE @step_type_list TABLE ([step_type] INT NOT NULL DEFAULT (0),
                                       [step_type_name] VARCHAR(30) NOT NULL DEFAULT (''))
        
        DECLARE @worker_list TABLE ([worker_customer_no] INT NOT NULL DEFAULT (0),
                                    [tessitura_login_id] VARCHAR(25) NOT NULL DEFAULT (''),
                                    [tessitura_location] VARCHAR(50) NOT NULL DEFAULT (''),
                                    [worker_full_name] VARCHAR(75) NOT NULL DEFAULT (''),
                                    [worker_initials] VARCHAR(75) NOT NULL DEFAULT (''),
                                    [visit_goal_count] INT NOT NULL DEFAULT (0),
                                    [contact_goal_count] INT NOT NULL DEFAULT (0),
                                    [ask_goal_amount] DECIMAL(18,2) NOT NULL DEFAULT (0),
                                    [actual_goal_amount] DECIMAL(18,2) NOT NULL DEFAULT (0))

    /*  Temporary Tables  */
                                    
        IF OBJECT_ID('tempdb..#step_raw_data') IS NOT NULL DROP TABLE [#step_raw_data]

        CREATE TABLE [#step_raw_data]  ([step_no] INT NOT NULL DEFAULT (0), 
                                        [plan_no] INT NOT NULL DEFAULT (0), 
                                        [step_dt] DATETIME NULL, 
                                        [completed_on_dt] DATETIME NULL,
                                        [month_num] INT NOT NULL DEFAULT (0), 
                                        [month_abbrev] VARCHAR(30) NOT NULL DEFAULT (''),
                                        [month_sort] VARCHAR(7) NOT NULL DEFAULT (''),
                                        [month_year] VARCHAR(4) NOT NULL DEFAULT (''),
                                        [fiscal_year] INT NOT NULL DEFAULT (0),
                                        [is_current_fiscal] INT NOT NULL DEFAULT (0),
                                        [step_type] INT NOT NULL DEFAULT (0), 
                                        [step_type_name] VARCHAR(30) NOT NULL DEFAULT (''), 
                                        [step_description] VARCHAR(30) NOT NULL DEFAULT(''), 
                                        [associate_no] INT NOT NULL DEFAULT (0),
                                        [priority] INT NOT NULL DEFAULT (0), 
                                        [worker_customer_no] INT NOT NULL DEFAULT (0),
                                        [worker_name] VARCHAR(75) NOT NULL DEFAULT (''),
                                        [worker_initials] VARCHAR(10) NOT NULL DEFAULT (''),
                                        [is_visit] CHAR(1) NOT NULL DEFAULT ('N')) 
    
        IF OBJECT_ID('tempdb..#worker_activity') IS NOT NULL DROP TABLE [#worker_activity]

        CREATE TABLE [#worker_activity] ([worker_customer_no] INT NOT NULL DEFAULT (0),
                                         [worker_name] VARCHAR(75) NOT NULL DEFAULT (''),
                                         [worker_initials] VARCHAR(75) NOT NULL DEFAULT (''),
                                         [visit_goal_count] DECIMAL(18,4) NOT NULL DEFAULT (0.0),
                                         [visit_count] DECIMAL(18,4) NOT NULL DEFAULT (0.0),
                                         [contact_goal_count] DECIMAL(18,4) NOT NULL DEFAULT (0.0),
                                         [contact_count] DECIMAL(18,4) NOT NULL DEFAULT (0.0),
                                         [ask_goal_amount] DECIMAL(18,4) NOT NULL DEFAULT (0.0),
                                         [ask_amount] DECIMAL (18,4) NOT NULL DEFAULT (0.0),
                                         [actual_goal_amount] DECIMAL(18,4) NOT NULL DEFAULT (0.0),
                                         [actual_amount] DECIMAL(18,4) NOT NULL DEFAULT (0.0))

        IF OBJECT_ID('tempdb..#plan_data') IS NOT NULL DROP TABLE [#plan_data]

        CREATE TABLE [#plan_data] ([plan_no] INT NOT NULL DEFAULT (0),
                                   [worker_customer_no] INT NOT NULL DEFAULT (0),
                                   [worker_name] VARCHAR(75) NOT NULL DEFAULT (0),
                                   [worker_initials] VARCHAR(75) NOT NULL DEFAULT (0),
                                   [ask_amount] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                   [goal_amount] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                   [plan_amount] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                   [contribution_amount] DECIMAL (18,2) NOT NULL DEFAULT (0.0))

    /*  Genrate a list of all active step types  */
 
        INSERT INTO @step_type_list ([step_type], [step_type_name])
        SELECT [id], 
               [description] 
        FROM [dbo].[TR_STEP_TYPE] 
        WHERE [inactive] = 'N'

        /*  Generate a list of valid workers with their goal amounts */

            INSERT INTO @worker_list ([worker_customer_no], [tessitura_login_id], [tessitura_location], [worker_full_name], [worker_initials], 
                                      [visit_goal_count], [contact_goal_count], [ask_goal_amount], [actual_goal_amount])
            SELECT usr.[worker_customer_no],
                   usr.[userid], 
                   usr.[location], 
                   cus.[fname],
                   cus.[lname],
                   CAST (ISNULL(kw2.[key_value],'0') AS INT) AS [visit_goal_count],
                   CAST (ISNULL(kw2.[key_value],'0') AS INT) AS [contact_goal_count],
                   CAST (ISNULL(kw3.[key_value],'0.0') AS DECIMAL(18,2)) AS [ask_goal_amount],
                   CAST (ISNULL(kw4.[key_value],'0.0') AS DECIMAL(18,2))  AS [actual_goal_amount]
            FROM [dbo].[T_METUSER] AS usr
                 INNER JOIN [dbo].[T_CUSTOMER] AS cus ON cus.[customer_no] = usr.[worker_customer_no]
                 LEFT OUTER JOIN [dbo].[TX_CUST_KEYWORD] AS kw1 ON kw1.[customer_no] = usr.[worker_customer_no] AND kw1.keyword_no = @visit_goal_kw_no
                 LEFT OUTER JOIN [dbo].[TX_CUST_KEYWORD] AS kw2 ON kw2.[customer_no] = usr.[worker_customer_no] AND kw2.keyword_no = @contact_goal_kw_no
                 LEFT OUTER JOIN [dbo].[TX_CUST_KEYWORD] AS kw3 ON kw3.[customer_no] = usr.[worker_customer_no] AND kw3.keyword_no = @ask_goal_kw_no
                 LEFT OUTER JOIN [dbo].[TX_CUST_KEYWORD] AS kw4 ON kw4.[customer_no] = usr.[worker_customer_no] AND kw4.keyword_no = @actual_goal_kw_no
            WHERE ISNULL(usr.[worker_customer_no],0)  > 0
        

                /***********************************************************************************************************************/
                /*************************** FOR TESTING ONLY  - REMOVE WHEN DONE TESTING **********************************************/
                UPDATE @worker_list SET [visit_goal_count] = @hard_coded_visit_goal,    [contact_goal_count] = @hard_coded_contact_goal,
                                        [ask_goal_amount] = @hard_coded_ask_goal,       [actual_goal_amount] = @hard_coded_actual_goal
                

    /*  Pulls a list of all visits that were completed in the past twelve months  */
    
        INSERT INTO [#step_raw_data] ([step_no], [plan_no], [step_dt], [completed_on_dt], [month_num], [month_abbrev], [month_sort], [month_year], 
                                      [fiscal_year], [is_current_fiscal], [step_type], [step_type_name], [step_description], [associate_no], 
                                      [priority], [worker_customer_no], [worker_name], [worker_initials], [is_visit])
        SELECT stp.[step_no],
               stp.[plan_no], 
               stp.[step_dt], 
               stp.[completed_on_dt],
               DATEPART(MONTH,stp.[completed_on_dt]),
               LEFT(DATENAME(MONTH,stp.[completed_on_dt]),3),
               LEFT(CONVERT(VARCHAR(10),stp.[completed_on_dt],111),7),
               DATENAME(YEAR,stp.[completed_on_dt]),
               CASE WHEN  stp.[completed_on_dt] < @fiscal_start_dt THEN (@fiscal_year - 1) ELSE @fiscal_year END,
               CASE WHEN stp.[completed_on_dt] < @fiscal_start_dt THEN 0 ELSE 1 END,
               stp.[step_type], 
               typ.[step_type_name], 
               stp.[description], 
               ISNULL(stp.[associate_no],0),
               stp.[priority], 
               stp.[worker_customer_no],
               cus.[fname],
               cus.[lname],
               CASE WHEN typ.[step_type_name] LIKE '%visit%' THEN 'Y' ELSE 'N' END
        FROM [dbo].[T_STEP] AS stp
             INNER JOIN @worker_list AS usr ON usr.[worker_customer_no] = stp.[worker_customer_no]
             INNER JOIN @step_type_list AS typ ON typ.[step_type] = stp.[step_type]
             INNER JOIN [dbo].[T_CUSTOMER] AS cus ON cus.[customer_no] = stp.[worker_customer_no]
        WHERE stp.[completed_on_dt] BETWEEN @fiscal_start_dt AND @current_dt
        
    /*  Pull the plan data to be used  */

        INSERT INTO [#plan_data] ([plan_no], [worker_customer_no], [worker_name], [worker_initials], [ask_amount], [goal_amount], [plan_amount], [contribution_amount])
        SELECT stp.[plan_no],
               stp.[worker_customer_no],
               stp.[worker_name],
               stp.[worker_initials],
               SUM(ISNULL(pln.[ask_amt],0.0)),
               SUM(ISNULL(pln.[goal_amt],0.0)),
               CASE WHEN @use_ask_amount = 'Y' THEN SUM(ISNULL(pln.[ask_amt],0.0)) ELSE SUM(ISNULL(pln.[goal_amt],0.0)) END,
               SUM(ISNULL(pln.[cont_amt],0.0))
        FROM [#step_raw_data] AS stp
             INNER JOIN [dbo].[T_PLAN] AS pln ON pln.[plan_no] = stp.[plan_no]
        GROUP BY stp.[plan_no], stp.[worker_customer_no], stp.[worker_name], stp.[worker_initials] 

        IF @include_zero_asks <> 'Y'
            DELETE FROM [#plan_data] WHERE [plan_amount] <= 0.0

    /*  Seed the #worker_activity table with each worker and their goals  */

        INSERT INTO [#worker_activity] ([worker_customer_no], [worker_name], [worker_initials], [visit_goal_count], [contact_goal_count], [ask_goal_amount], [actual_goal_amount])
        SELECT [worker_customer_no],
               [worker_full_name],
               [worker_initials],
               [visit_goal_count],
               [contact_goal_count],
               [ask_goal_amount], 
               [actual_goal_amount]
        FROM @worker_list

    /*  Insert the count of actual visits for each solicitor  */

        INSERT INTO [#worker_activity] ([worker_customer_no], [worker_name], [worker_initials], [visit_count])
        SELECT [worker_customer_no],
               [worker_name],
               [worker_initials], 
               COUNT([step_no])
        FROM [#step_raw_data]
        WHERE [completed_on_dt] BETWEEN @fiscal_start_dt AND @current_dt AND [is_visit] = 'Y'
        GROUP BY [worker_customer_no], [worker_name], [worker_initials]

    /*  Insert the count of all steps for each solicitor  */

        INSERT INTO [#worker_activity] ([worker_customer_no], [worker_name], [worker_initials], [contact_count])
        SELECT [worker_customer_no],
               [worker_name],
               [worker_initials], 
               COUNT([step_no])
        FROM [#step_raw_data]
        WHERE [completed_on_dt] BETWEEN @fiscal_start_dt AND @current_dt
        GROUP BY [worker_customer_no], [worker_name], [worker_initials]

    /*  Insert the total expected amount (based on ask or goal amount depending on how the variable is set at the start of the procedure  */

        INSERT INTO [#worker_activity] ([worker_customer_no], [worker_name], [worker_initials], [ask_amount])
        SELECT [worker_customer_no],
               [worker_name],
               [worker_initials],
               SUM([plan_amount])
        FROM [#plan_data]
        GROUP BY [worker_customer_no], [worker_name], [worker_initials]

    /*  Insert the total actual amount (contributions)
        --This may or may not include plans with no ask amount depending on how the variable is set at the start of the procedure  */

        INSERT INTO [#worker_activity] ([worker_customer_no], [worker_name], [worker_initials], [actual_amount])
        SELECT [worker_customer_no],
               [worker_name],
               [worker_initials],
               SUM([contribution_amount])
        FROM [#plan_data]
        GROUP BY [worker_customer_no], [worker_name], [worker_initials]

    FINISHED:

        SELECT  [worker_customer_no],
                [worker_name],
                [worker_initials],
                SUM([visit_goal_count]) AS [visit_goal_count],
                SUM([visit_count]) AS [visit_count],
                CASE WHEN SUM([visit_goal_count]) = 0.0 THEN 0.0 ELSE (SUM([visit_count]) / SUM([visit_goal_count])) END AS [visit_count_percent],
                SUM([contact_goal_count]) AS [contact_goal_count],
                SUM([contact_count]) AS [contact_count],
                CASE WHEN SUM([contact_goal_count]) = 0.0 THEN 0.0 ELSE (SUM([contact_count]) / SUM([contact_goal_count])) END AS [contact_count_percent],
                SUM([ask_goal_amount]) AS [ask_goal_amount],
                SUM([ask_amount]) AS [ask_amount],
                CASE WHEN SUM([ask_goal_amount]) = 0.0 THEN 0.0 ELSE (SUM([ask_amount]) / SUM([ask_goal_amount])) END AS [ask_amount_percent],
                SUM([actual_goal_amount]) [actual_goal_amount],
                SUM([actual_amount]) AS [actual_amount],
                CASE WHEN SUM([actual_goal_amount]) = 0.0 THEN 0.0 ELSE (SUM([actual_amount]) / SUM([actual_goal_amount])) END AS [actual_amount_percent]
        FROM [#worker_activity]
        GROUP BY [worker_customer_no], [worker_name], [worker_initials]

    DONE:

END
GO

EXECUTE [dbo].[LRP_ADV_CAMPAIGN_ACTIVITY_STAFF]

