USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_TESSITURA_CASHOUT_TRX_STATS_ALL]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LRP_TESSITURA_CASHOUT_TRX_STATS_ALL]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LRP_TESSITURA_CASHOUT_TRX_STATS_ALL]
        @report_start_dt DATETIME = Null,
        @report_end_dt DATETIME = NULL,
        @report_users VARCHAR(4000) = NULL,
        @user_locations varchar(4000) = NULL,
        @include_operator_detail CHAR(1) = NULL
AS BEGIN

    /*  Report Variables  */
            
        DECLARE @rpt_message VARCHAR(100)       SELECT @rpt_message = ''
        DECLARE @user_list TABLE ([userid] VARCHAR(50))
        DECLARE @location_list TABLE ([user_location] VARCHAR(30))

    /* Check Parameters  */
            
        SELECT @report_start_dt = ISNULL(@report_start_dt,DATEADD(DAY,-1,GETDATE())),
               @report_end_dt = ISNULL(@report_end_dt,DATEADD(DAY,-1,GETDATE()))

        SELECT @report_start_dt = CONVERT(DATE,@report_start_dt)
        SELECT @report_end_dt = CONVERT(DATE,@report_end_dt)

        SELECT @report_users = ISNULL(@report_users,'')
        IF @report_users = 'All' OR @report_users = 'All Users' SELECT @report_users = ''
        SELECT @report_users = REPLACE(@report_users,'"','')

        SELECT @user_locations = ISNULL(@user_locations,'')
        IF @user_locations = 'All' OR @user_locations = 'All Locations' SELECT @user_locations = ''
        SELECT @user_locations = REPLACE(@user_locations,'"','')

        SELECT @include_operator_detail = ISNULL(@include_operator_detail,'Y')
        IF @include_operator_detail <> 'N' SELECT @include_operator_detail = 'Y'


    /*  Parse Out Users and Locations */

        IF @report_users <> ''
            INSERT INTO @user_list SELECT LTRIM(RTRIM(Item)) FROM [dbo].[LFT_SPLIT_DILEMETED_STRING] (@report_users,',')

        IF @user_locations <> ''
            INSERT INTO @location_list SELECT LTRIM(RTRIM(Item)) FROM [dbo].[LFT_SPLIT_DILEMETED_STRING] (@user_locations,',')

    /*  Create Temporary Table for order information  */

        IF OBJECT_ID('tempdb..#trx_stat_table') IS NOT NULL DROP TABLE [#trx_stat_table]

        CREATE TABLE [#trx_stat_table] ([rpt_operator] varchar(10), [rpt_operator_name] VARCHAR(50), [rpt_operator_name_sort] VARCHAR(50), [rpt_operator_location] VARCHAR(30),
                                        [trx_started] decimal(18,4), [trx_touched] decimal(18,4), [trx_memb_basic] decimal(18,4), [trx_memb_premier] decimal(18,4), [trx_memb_total] decimal(18,4),
                                        [trx_memb_basic_percent_started] decimal(18,4), [trx_memb_basic_percent_touched] decimal(18,4), [trx_memb_basic_percent_memberships] decimal(18,4), 
                                        [trx_memb_premier_percent_started] decimal(18,4), [trx_memb_premier_percent_touched] decimal(18,4), [trx_memb_premier_percent_memberships] DECIMAL(18,4), 
                                        [trx_memb_total_percent_started] decimal(18,4), [trx_memb_total_percent_touched] decimal(18,4), [trx_memb_basic_onestep] decimal(18,4), 
                                        [trx_memb_basic_onestep_percent_started] decimal(18,4), [trx_memb_basic_onestep_percent_touched] decimal(18,4), [trx_memb_basic_onestep_percent_Basic] decimal(18,4), 
                                        [trx_memb_basic_onestep_percent_Memberships] decimal(18,4), [trx_memb_premier_onestep] decimal(18,4), [trx_memb_premier_onestep_percent_started] decimal(18,4), 
                                        [trx_memb_premier_onestep_percent_touched] decimal(18,4), [trx_memb_premier_onestep_percent_premier] decimal(18,4), [trx_memb_premier_onestep_percent_memberships] decimal(18,4), 
                                        [trx_memb_total_onestep] decimal(18,4), [trx_memb_total_onestep_percent_started] decimal(18,4), [trx_memb_total_onestep_percent_memberships] decimal(18,4), 
                                        [trx_memb_total_onestep_percent_touched] decimal(18,4), [memb_level] VARCHAR(30), [total_orders_counted] INT, [total_performances_sold] DECIMAL(18,4), 
                                        [average_performances_per_order] DECIMAL(18,4), [orders_with_add_on] DECIMAL(18,4), [percentage_of_orders_with_add_on] DECIMAL(18,4), [rpt_message] VARCHAR(100))

        CREATE CLUSTERED INDEX [ix_trx_stat_table_rpt_operatr] ON #trx_stat_table ([rpt_operator] ASC) ON [PRIMARY]
    
    /*  Retrieve data set from the history table  */

        INSERT INTO [#trx_stat_table]
        SELECT   [user_id]
               , [user_name]
               , [user_name_sort]
               , [user_location]
               , SUM([trx_started])
               , SUM([trx_touched])
               , SUM([trx_memb_basic])
               , SUM([trx_memb_premier])
               , SUM([trx_memb_total])
               , 0--[trx_memb_basic_percent_started]
               , 0--[trx_memb_basic_percent_touched]
               , 0--[trx_memb_basic_percent_memberships]
               , 0--[trx_memb_premier_percent_started]
               , 0--[trx_memb_premier_percent_touched]
               , 0--[trx_memb_premier_percent_memberships]
               , 0--[trx_memb_total_percent_started]
               , 0--[trx_memb_total_percent_touched]
               , SUM([trx_memb_basic_onestep])
               , 0--[trx_memb_basic_onestep_percent_started]
               , 0--[trx_memb_basic_onestep_percent_touched]
               , 0--[trx_memb_basic_onestep_percent_Basic]
               , 0--[trx_memb_basic_onestep_percent_Memberships]
               , SUM([trx_memb_premier_onestep])
               , 0--[trx_memb_premier_onestep_percent_started]
               , 0--[trx_memb_premier_onestep_percent_touched]
               , 0--[trx_memb_premier_onestep_percent_premier]
               , 0--[trx_memb_premier_onestep_percent_memberships]
               , SUM([trx_memb_total_onestep])
               , 0--[trx_memb_total_onestep_percent_started]
               , 0--[trx_memb_total_onestep_percent_memberships]
               , 0--[trx_memb_total_onestep_percent_touched]
               , [is_member]
               , SUM([total_orders_counted])
               , SUM([total_performances_sold])
               , 0--[average_performances_per_order]
               , SUM([orders_with_add_on])
               , 0--[percentage_of_orders_with_add_on]
               , ''
        FROM [dbo].[LT_HISTORY_TRX_STATS]
        WHERE [history_dt] BETWEEN @report_start_dt AND @report_end_dt
        GROUP BY [user_id], [user_name], [user_name_sort], [user_location], [is_member]

    /*  Filter out unwanted records based on the parameters*/

        IF EXISTS (SELECT * FROM @user_list)
            DELETE FROM [#trx_stat_table] WHERE [rpt_operator] NOT IN (SELECT [userid] FROM @user_list)
        ELSE IF EXISTS (SELECT * FROM @location_list)
            DELETE FROM [#trx_stat_table] WHERE [rpt_operator_location] NOT IN (SELECT [user_location] FROM @location_list)

    /*  Insert deparmntal totals  */
    
        INSERT INTO [#trx_stat_table]
        SELECT '', [rpt_operator_location] + ' Total', 'ZZZ_' + [rpt_operator_location], [rpt_operator_location], SUM([trx_started]), SUM([trx_touched]), SUM([trx_memb_basic]), SUM([trx_memb_premier]), SUM([trx_memb_total]), 
                   0, 0, 0, 0, 0, 0, 0, 0, SUM([trx_memb_basic_onestep]), 0, 0, 0, 0, SUM([trx_memb_premier_onestep]), 0, 0, 0, 0, SUM([trx_memb_total_onestep]), 0, 0, 0, '', SUM([total_orders_counted]), 
                   SUM([total_performances_sold]), 0, SUM([orders_with_add_on]), 0, ''
        FROM [#trx_stat_table]
        GROUP BY [rpt_operator_location]

    /*  INSERT GRAND TOTAL  */

        INSERT INTO [#trx_stat_table]
        SELECT '', 'GRAND TOTAL', 'ZZZ_GRAND TOTAL', 'ZZZ_GRAND TOTAL', SUM([trx_started]), SUM([trx_touched]), SUM([trx_memb_basic]), SUM([trx_memb_premier]), SUM([trx_memb_total]), 
                   0, 0, 0, 0, 0, 0, 0, 0, SUM([trx_memb_basic_onestep]), 0, 0, 0, 0, SUM([trx_memb_premier_onestep]), 0, 0, 0, 0, SUM([trx_memb_total_onestep]), 0, 0, 0, '', SUM([total_orders_counted]), 
                   SUM([total_performances_sold]), 0, SUM([orders_with_add_on]), 0, ''
        FROM [#trx_stat_table]
        
    /*  Delete fine detail if that's what asked for  */

        IF @include_operator_detail = 'N'
            DELETE FROM [#trx_stat_table] WHERE rpt_operator <> ''

    /*  Figure out percentages and averages  */

        UPDATE [#trx_stat_table] SET [trx_memb_basic_percent_started] = ([trx_memb_basic] / [trx_started]) WHERE [trx_started] <> 0
        UPDATE [#trx_stat_table] SET [trx_memb_basic_percent_touched] = ([trx_memb_basic] / [trx_touched]) WHERE [trx_touched] <> 0
        UPDATE [#trx_stat_table] SET [trx_memb_basic_percent_memberships] = ([trx_memb_basic] / [trx_memb_total]) WHERE [trx_memb_total] <> 0
        UPDATE [#trx_stat_table] SET [trx_memb_premier_percent_started] = ([trx_memb_premier] / [trx_started]) WHERE [trx_started] <> 0
        UPDATE [#trx_stat_table] SET [trx_memb_premier_percent_touched] = ([trx_memb_premier] / [trx_touched]) WHERE [trx_touched] <> 0
        UPDATE [#trx_stat_table] SET [trx_memb_premier_percent_memberships] = ([trx_memb_premier] / [trx_memb_total]) WHERE [trx_memb_total] <> 0
        UPDATE [#trx_stat_table] SET [trx_memb_total_percent_started] = (trx_memb_total) / [trx_started] WHERE [trx_started] <> 0
        UPDATE [#trx_stat_table] SET [trx_memb_total_percent_touched] = (trx_memb_total) / [trx_touched] WHERE [trx_started] <> 0
        UPDATE [#trx_stat_table] SET [trx_memb_basic_onestep_percent_started] = ([trx_memb_basic_onestep] / [trx_started]) WHERE [trx_started] <> 0
        UPDATE [#trx_stat_table] SET [trx_memb_basic_onestep_percent_touched] = ([trx_memb_basic_onestep] / [trx_touched]) WHERE [trx_touched] <> 0
        UPDATE [#trx_stat_table] SET [trx_memb_basic_onestep_percent_Basic] = ([trx_memb_basic_onestep] / [trx_memb_basic]) WHERE [trx_memb_basic] <> 0
        UPDATE [#trx_stat_table] SET [trx_memb_basic_onestep_percent_Memberships] = ([trx_memb_basic_onestep] / [trx_memb_total]) WHERE [trx_memb_total] <> 0
        UPDATE [#trx_stat_table] SET [trx_memb_premier_onestep_percent_started] = ([trx_memb_premier_onestep] / [trx_started]) WHERE [trx_started] <> 0
        UPDATE [#trx_stat_table] SET [trx_memb_premier_onestep_percent_touched] = ([trx_memb_premier_onestep] / [trx_touched]) WHERE [trx_touched] <> 0
        UPDATE [#trx_stat_table] SET [trx_memb_premier_onestep_percent_premier] = ([trx_memb_premier_onestep] / [trx_memb_premier]) WHERE [trx_memb_premier] <> 0
        UPDATE [#trx_stat_table] SET [trx_memb_premier_onestep_percent_memberships] = ([trx_memb_premier_onestep] / [trx_memb_total]) WHERE [trx_memb_total] <> 0
        UPDATE [#trx_stat_table] SET [trx_memb_total_onestep_percent_started] = ([trx_memb_total_onestep] / [trx_started]) WHERE [trx_started] <> 0
        UPDATE [#trx_stat_table] SET [trx_memb_total_onestep_percent_touched] = ([trx_memb_total_onestep] / [trx_touched]) WHERE [trx_touched] <> 0
        UPDATE [#trx_stat_table] SET [trx_memb_total_onestep_percent_memberships] = ([trx_memb_total_onestep] / [trx_memb_total]) WHERE [trx_memb_total] <> 0
        UPDATE [#trx_stat_table] SET [average_performances_per_order] = ([total_performances_sold] / [total_orders_counted]) WHERE [total_orders_counted] <> 0
        UPDATE [#trx_stat_table] SET [percentage_of_orders_with_add_on] = ([orders_with_add_on] / [total_orders_counted]) WHERE [total_orders_counted] <> 0

    FINISHED:

        /*  If nothing found, put a single record in with a message saying so.  */

            IF NOT EXISTS(SELECT * FROM [#trx_stat_table]) BEGIN
                IF @rpt_message = '' SELECT @rpt_message = 'No records found for the criteria you entered.'

                INSERT INTO [#trx_stat_table]
                VALUES  ('' , '' , '' , '' , 0 , 0 , 0 , 0 , 0, 0 , 0, 0, 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , '' , 0 , 0 , 0 , 0 , 0 , @rpt_message)
            END

        /*  Select the final data set that will be passed to the report  */

            SELECT [rpt_operator], [rpt_operator_name], [rpt_operator_name_sort], [rpt_operator_location], [trx_started], [trx_touched], [trx_memb_basic], [trx_memb_premier], [trx_memb_total], 
                   [trx_memb_basic_percent_started], [trx_memb_basic_percent_touched], [trx_memb_basic_percent_memberships], [trx_memb_premier_percent_started], [trx_memb_premier_percent_touched], 
                   [trx_memb_premier_percent_memberships], [trx_memb_total_percent_started], [trx_memb_total_percent_touched], [trx_memb_basic_onestep], [trx_memb_basic_onestep_percent_started], 
                   [trx_memb_basic_onestep_percent_touched], [trx_memb_basic_onestep_percent_Basic], [trx_memb_basic_onestep_percent_Memberships], [trx_memb_premier_onestep], 
                   [trx_memb_premier_onestep_percent_started], [trx_memb_premier_onestep_percent_touched], [trx_memb_premier_onestep_percent_premier], [trx_memb_premier_onestep_percent_memberships], 
                   [trx_memb_total_onestep], [trx_memb_total_onestep_percent_started], [trx_memb_total_onestep_percent_memberships], [trx_memb_total_onestep_percent_touched], [memb_level], 
                   [total_orders_counted], [total_performances_sold], [average_performances_per_order], [orders_with_add_on], [percentage_of_orders_with_add_on], [rpt_message]
            FROM [#trx_stat_table]
            ORDER BY [rpt_operator_location], [rpt_operator_name_sort]

    DONE:

        /*  Clean up  */

            IF OBJECT_ID('tempdb..#trx_stat_table') IS NOT NULL DROP TABLE [#trx_stat_table]

END
GO

GRANT EXECUTE ON [dbo].[LRP_TESSITURA_CASHOUT_TRX_STATS_ALL] TO ImpUsers
GO

EXECUTE [dbo].[LRP_TESSITURA_CASHOUT_TRX_STATS_ALL] '1-1-2017', '1-31-2017', Null, 'Box Office,Membership,Science Central', 'Y'

--EXECUTE [dbo].[LRP_TESSITURA_CASHOUT_TRX_STATS_ALL] '12-3-2016', '12-3-2016', 'candre00'

--DELETE FROM TemporaryData.dbo.report_parameter_check WHERE report_name = 'LRP_TESSITURA_CASHOUT_TRX_STATS_ALL'
--SELECT * FROM TemporaryData.dbo.report_parameter_check
--INSERT INTO TemporaryData.dbo.report_parameter_check
--VALUES (GETDATE(),'LRP_TESSITURA_CASHOUT_TRX_STATS_ALL','@user_locations',@user_locations)
