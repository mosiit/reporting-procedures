USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_OVERNIGHT_SNACK_TIMES]') AND type IN (N'P', N'PC'))
BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_OVERNIGHT_SNACK_TIMES] AS' 
    EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LRP_OVERNIGHT_SNACK_TIMES] TO impusers'
END
GO

/*      LRP_OVERNIGHT_SNACK_TIMES
   


*/
ALTER PROCEDURE [dbo].[LRP_OVERNIGHT_SNACK_TIMES]
        @report_dt DATETIME = NULL
AS BEGIN

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

    /*  Procedure Variables  */

        DECLARE @temp_table TABLE ([snack_no] INT NOT NULL IDENTITY (1,1),
                                   [booking_no] INT NOT NULL DEFAULT (0),
                                   [overnight_dt] DATETIME NULL,
                                   [snack_time] VARCHAR(50) NOT NULL DEFAULT (''),
                                   [snack_column] VARCHAR(50) NOT NULL DEFAULT (''));

        DECLARE @snack_table TABLE ([booking_no] INT NOT NULL DEFAULT (0),
                                    [overnight_dt] DATETIME NULL,
                                    [snack_1] VARCHAR(25),
                                    [snack_2] VARCHAR(25),
                                    [snack_3] VARCHAR(25));

    /*  Retrieve the snack information from the database  */
        
        INSERT INTO @temp_table ([booking_no], [overnight_dt], [snack_time])
        SELECT TOP 3 [booking_no], [overnight_dt], [assigned_resource] 
        FROM [dbo].[LV_OVERNIGHT_RESOURCE_ASSIGNMENTS]
        WHERE [overnight_dt] = @report_dt and [resource_type] = 'Snack Time' AND ISDATE([assigned_resource]) = 1
        ORDER BY CONVERT(DATETIME,[assigned_resource])

    /*  Set the column names based on row identity  */

        UPDATE @temp_table SET snack_column = 'snack_' + CONVERT(CHAR(1),[snack_no])

    /*  Pivot the data into a single row  */
            
        INSERT INTO @snack_table ([booking_no], [overnight_dt], [snack_1], [snack_2], [snack_3])
        SELECT [booking_no], [overnight_dt], [snack_1], [snack_2], [snack_3]
        FROM (SELECT [booking_no], [overnight_dt], [snack_time], [snack_column] FROM @temp_table) d
              PIVOT (MAX(snack_time)
                     FOR snack_column in (snack_1, snack_2, snack_3)
              ) piv;

    FINISHED:

        /*  Select the single tow and pass it back to the report  */
            
            SELECT [booking_no],
                   [overnight_dt],
                   [snack_1],
                   [snack_2],
                   [snack_3]
            FROM @snack_table

END
GO

    EXECUTE [dbo].[LRP_OVERNIGHT_SNACK_TIMES] '10-18-2018'

