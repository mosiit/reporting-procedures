USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LRP_SchoolChaperoneLabels]    Script Date: 8/11/2016 11:26:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[LRP_SchoolChaperoneLabels]
(
	@perf_start_dt DATETIME,
	@perf_end_dt DATETIME,
	@mos_str VARCHAR(MAX),
	@facility_str VARCHAR(MAX)
)
AS
-- Print labels for Chaperones of School Groups
-- Using output from Alex's SP - LRP_MOS_GROUP_ARRIVAL
-- Used cool CROSS JOIN trick to create 1 row/label for each chaperone - http://www.sqlservercentral.com/Forums/Topic911157-338-1.aspx?Update=1
-- DSJ 6/16/2016

--EXEC dbo.LRP_SchoolChaperoneLabels @perf_start_dt DATETIME = '6/1/2016',
--	@perf_end_dt DATETIME = '6/8/2016',
--	@mos_str VARCHAR(MAX) = '12',
--	@facility_str VARCHAR(MAX) = ''

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET NOCOUNT ON;


DECLARE @results TABLE
(
	order_no INT NOT NULL,
	customer_no INT NULL,
	owner VARCHAR(255) NULL,
	initiator VARCHAR(255) NULL,
    recipient VARCHAR(255) NULL,
	sort_name VARCHAR(255) NULL,
	total_due MONEY NULL,
	total_paid MONEY NULL,
	order_notes VARCHAR(255) NULL,
	perf_no INT, 
	perf_desc VARCHAR(30) NULL,
	facility VARCHAR(30) NULL,
	zone VARCHAR(30) NULL,
	price_type VARCHAR(30) NULL,
	perf_dt DATETIME NULL,
	master_perf_dt DATETIME NULL,
	num_seats INT NULL,
	group_count INT, 
	arrival VARCHAR(255) NULL,
	departure VARCHAR(255) NULL,
	grade VARCHAR(255) NULL,
	transportation VARCHAR(255) NULL,
	lunch VARCHAR(255) NULL,
	po_number VARCHAR(255) NULL,
    master_daily_count int NULL
)


DECLARE @LabelResults TABLE
(
	chaperoneStr VARCHAR(10),
	order_no INT,
	owner VARCHAR(255),
	perf_dt DATETIME
)

INSERT INTO @results
        (order_no,
         customer_no,
         owner,
         initiator,
         recipient,
         sort_name,
         total_due,
         total_paid,
         order_notes,
		 perf_no,
         perf_desc,
         facility,
         zone,
         price_type,
         perf_dt,
         master_perf_dt,
         num_seats,
		 group_count,
         arrival,
         departure,
         grade,
         transportation,
         lunch,
         po_number,
         master_daily_count)
EXEC LRP_MOS_GROUP_ARRIVAL
	@order_start_dt = '01/01/1900',
	@order_end_dt = '12/31/2999',
	@perf_start_dt = @perf_start_dt,
	@perf_end_dt = @perf_end_dt,
	@mos_str = @mos_str,
	@facility_str = @facility_str

--1st. Get all School Chaperones and School Teachers who are booked for Exhibit Halls
INSERT INTO @LabelResults
(
	chaperoneStr,
	order_no,
	owner,
	perf_dt
)
SELECT 'CHAPERONE', order_no, owner, perf_dt
FROM @results
CROSS JOIN
(SELECT TOP 100 ROW_NUMBER() OVER(ORDER BY (SELECT 1)) FROM sys.columns)D(n)
WHERE D.n <= num_seats
	AND facility IN ('Exhibit Halls','Exhibit Halls, Timed')  
	AND price_type IN ('School Chaperone','School Teacher')

-- 2nd. Get All School Chaperones and School Teachers who are ONLY booked for Omni Theater or Planetarium
INSERT INTO @LabelResults
(
	chaperoneStr,
	order_no,
	owner,
	perf_dt
)
SELECT 'CHAPERONE', order_no, owner, perf_dt
FROM @results
CROSS JOIN
(SELECT TOP 100 ROW_NUMBER() OVER(ORDER BY (SELECT 1)) FROM sys.columns)D(n)
WHERE D.n <= num_seats
	AND facility IN ('Omni Theater', 'Planetarium')
	AND price_type IN ('School Chaperone','School Teacher')
	AND order_no NOT IN (SELECT DISTINCT order_no FROM @LabelResults)

SELECT chaperoneStr, order_no, owner, perf_dt
FROM @LabelResults
ORDER BY perf_dt, order_no
GO


