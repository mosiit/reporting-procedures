USE [impresario]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[LP_CONTRIBUTION_IMPORT](
		@session_id int,
		@run_mode int,				-- 1=Import, 2=Review, 3=Reprint Session Output
		@mode char(1),				-- (P)re-process, (B)efore, (A)fter
		@ref_no int
	)
	AS
	/*	This procedure is used for local processing during contribution Import. 

		The Run Mode and Session ID are always passed in.

		This procedures is called at various points during the import process, indicated by @mode:

		(P)re-process - This mode indicates that the procedure is being called before any error checking
						occurs on the imported data.  The data is in TW_CONTRIBUTION_IMPORT marked with the passed
						in session_no.

		(B)efore	  - This mode indicates that the procedure is being called for a specific contribution (@ref_no)
						before beginning to insert it, inside the transaction. Only called when run_mode = 1 (inserting)

		(A)fter		  - This mode indicates that the procedure is being called for a specific contribution (@ref_no)
						after it has been inserted, inside the transaction. Only called when run_mode = 1 (inserting)
	*/

	-- You likely don't want to do anything in reprint session output mode.
	If @run_mode = 3
		RETURN

	-- Local code goes here:
	-- https://community.tessituranetwork.com/local_tessitura_user_groups/austin_tessitura_users/f/austin_tessitura_users-forum/16797/importing-basics
	IF @run_mode = 1 AND @mode LIKE 'A'
	BEGIN

		UPDATE tmc
		SET tmc.worker_customer_no = tci.local_use0
		FROM TM_CONTRIBUTION tmc
		JOIN TW_CONTRIBUTION_IMPORT tci
			ON tmc.ref_no = tci.ref_no
		WHERE tci.local_use0 IS NOT NULL;

	END;

	RETURN
GO


