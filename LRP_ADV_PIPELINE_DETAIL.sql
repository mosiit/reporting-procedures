USE [impresario]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[LRP_ADV_PIPELINE_DETAIL]
(
	@campaign_str VARCHAR(MAX) = NULL, 
	@plan_status_str VARCHAR(MAX) = NULL,
	@close_dt_x_months_out INT = NULL,
	@worker_customer_no INT = NULL  
)
AS

-- Used in 3 reports
-- ADV Pipeline Detail
-- ADV Pipeline Summary
-- ADV Pipeline Upcoming Close report

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

-- TABLE TO HOLD CAMPAIGN PARAMETERS
DECLARE	@campaignTbl TABLE (campaignNo INT NOT NULL, fyear INT)

IF ISNULL(@campaign_str,'') <> ''
	INSERT INTO @campaignTbl (campaignNo, fyear)
	SELECT Element, c.fyear
	FROM dbo.FT_SPLIT_LIST(@campaign_str,',') x
		INNER JOIN dbo.T_CAMPAIGN c
		ON x.Element = c.campaign_no
ELSE
	INSERT INTO @campaignTbl (campaignNo, fyear)
	SELECT campaign_no, fyear
	FROM dbo.T_CAMPAIGN

-- TABLE TO HOLD PLAN STATUS
DECLARE @planStatusTbl TABLE (plan_status_id INT NOT NULL)

IF ISNULL(@plan_status_str,'') <> ''
	INSERT INTO @planStatusTbl (plan_status_id)
	SELECT Element From dbo.FT_SPLIT_LIST(@plan_status_str,',')
ELSE
	INSERT INTO @planStatusTbl (plan_status_id)
	SELECT id
	FROM dbo.TR_PLAN_STATUS
	WHERE inactive = 'N';

WITH primaryWorker
AS 
(
-- CTE code taken from VS_PLAN_WITH_PRIMARY_WORKER
SELECT plan_no, 
	dn.display_name as primaryWorker, 
	dn.customer_no as primaryWorkerNo, 
	dn.display_name_tiny as primaryWorkerTiny
FROM TX_CUST_PLAN cp
INNER JOIN dbo.FT_CONSTITUENT_DISPLAY_NAME() dn 
	ON cp.customer_no = dn.customer_no
WHERE cp.primary_ind = 'Y'
)
SELECT DISTINCT 
	bus.Business_Unit AS businessUnit,
	ccat.description AS campaignCat,
	camp.fyear camp_fyear,
	p.customer_no,
	custname.display_name AS cust_display_name,
	custname.sort_name AS cust_sort_name,
	[dbo].[LFS_BOARD_GetProspectManagerInitials](p.customer_no) AS ProspectMgr,
	p.complete_by_dt,
	bp.fyear complete_by_fyear,
	bp.quarter complete_by_quarter,
	'FY' + RIGHT(bp.fyear,2) + ' Q' + CAST(bp.quarter AS VARCHAR(1)) AS Timeperiod,
	p.ask_amt,
	p.start_dt AS askDate,
	p.complete_by_dt AS closeDate,
	p.probability,
	p.goal_amt,
	stausDesc.description AS planStatus,
	CASE p.status
		WHEN 1 THEN 'Open'
		WHEN 2 THEN 'Open'
		WHEN 4 THEN 'Open'
		WHEN 22 THEN 'Solicitation'
		WHEN 23 THEN 'Solicitation'
		ELSE NULL
	END AS planStatusType, 
	desi.description designation,
	worker.primaryWorker,
	secondWorker.display_name AS secondWorker,
	p.notes
FROM T_PLAN p
INNER JOIN @planStatusTbl pst
	ON pst.plan_status_id = p.status
INNER JOIN dbo.TR_PLAN_STATUS stausDesc
	ON stausDesc.id = p.status
INNER JOIN dbo.FT_CONSTITUENT_DISPLAY_NAME() AS custname 
		ON p.customer_no = custname.customer_no
INNER JOIN dbo.T_CAMPAIGN camp
	ON camp.campaign_no = p.campaign_no
INNER JOIN @campaignTbl ct
	ON ct.campaignNo = camp.campaign_no
INNER JOIN dbo.TR_CAMPAIGN_CATEGORY ccat
	 ON camp.category = ccat.id
LEFT JOIN dbo.TR_CONT_DESIGNATION desi
	ON desi.id = p.cont_designation
LEFT JOIN dbo.LTR_CAMPAIGN_BUSINESS_UNIT bus
	ON bus.campaign_no = camp.campaign_no
LEFT JOIN dbo.TR_Batch_Period bp
	ON p.complete_by_dt BETWEEN bp.start_dt AND bp.end_dt
LEFT JOIN primaryWorker worker
	ON worker.plan_no = p.plan_no
LEFT JOIN dbo.TX_CUST_PLAN cpl
	ON cpl.plan_no = p.plan_no
	AND cpl.role_no = 11 -- Secondary Solicitor
LEFT JOIN dbo.FT_CONSTITUENT_DISPLAY_NAME() AS secondWorker 
		ON cpl.customer_no = secondWorker.customer_no
WHERE 
(   -- if @worker_customer_no is not null, only want to pull records where the primary or secondary worker = @worker_customer_no
	worker.primaryWorkerNo = ISNULL(@worker_customer_no, worker.primaryWorkerNo)
	OR secondWorker.customer_no = ISNULL(@worker_customer_no, secondWorker.customer_no)
)
AND
(
	(@close_dt_x_months_out IS NOT NULL AND DATEDIFF(MONTH,GETDATE(),p.complete_by_dt) BETWEEN 0 AND @close_dt_x_months_out )
	OR 
	(@close_dt_x_months_out IS NULL) 
)