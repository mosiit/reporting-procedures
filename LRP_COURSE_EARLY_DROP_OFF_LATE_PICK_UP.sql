USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_COURSE_EARLY_DROP_OFF_LATE_PICK_UP]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LRP_COURSE_EARLY_DROP_OFF_LATE_PICK_UP]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LRP_COURSE_EARLY_DROP_OFF_LATE_PICK_UP]
    @report_start_dt DATETIME,
    @report_end_dt DATETIME
AS BEGIN

    /*  Procedure Variables  */

        DECLARE @rpt_message VARCHAR(100) = '', @extras_order_no INT, @extras_date CHAR(10), @extras_type VARCHAR(30), @extras_participant_name VARCHAR(100), 
                @course_title VARCHAR(30), @course_title_long VARCHAR(100)

        CREATE TABLE  #early_late_stay ([extras_order_no] INT, [extras_date] CHAR(10), [extras_time] varchar(8), [extras_type] VARCHAR(20), [extras_title] varchar(30), 
                                        [extras_title_long] VARCHAR(100), [extras_participant_id] INT, [extras_participant_name] VARCHAR(150), [extras_participant_sort_name] VARCHAR(150),
                                        [course_order_no] INT, [course_date] CHAR(10), [course_time] varchar(8), [course_type] VARCHAR(20), [course_title] varchar(30),
                                        [course_title_long] VARCHAR(100), [course_participant_id] INT, [course_participant_name] VARCHAR(150), [rpt_message] VARCHAR(100))

        CREATE NONCLUSTERED INDEX [ix_early_late_stay_order] ON [#early_late_stay] ([course_order_no] ASC) ON [PRIMARY]
        CREATE NONCLUSTERED INDEX [ix_early_late_stay_date] ON [#early_late_stay] ([extras_date] ASC) ON [PRIMARY]
        CREATE NONCLUSTERED INDEX [ix_early_late_stay_type] ON [#early_late_stay] ([extras_type] ASC) ON [PRIMARY]
        CREATE NONCLUSTERED INDEX [ix_early_late_stay_participant] ON [#early_late_stay] ([extras_participant_name] ASC) ON [PRIMARY]
        

    /*  Null start date = tomorrow  */

        SELECT @report_start_dt = ISNULL(@report_start_dt,DATEADD(DAY,1,GETDATE()))
        SELECT @report_end_dt = ISNULL(@report_end_dt ,@report_start_dt)

        SELECT @report_start_dt = CONVERT(DATE,@report_start_dt),
               @report_end_dt =  CONVERT(DATE,@report_end_dt)
        
    /*  Get all early drop off / late stay participants and temporarily store in #early_late_stay table  */


        INSERT INTO #early_late_stay
        SELECT ext.[order_no], ext.[course_date], ext.[course_time], ext.[course_type], ext.[extras_title], ext.[extras_title_long], ext.[participant_id], ext.[participant_name], 
               ext.[participant_sort_name], ISNULL(amp.[order_no],''), ISNULL(amp.[course_date],''), ISNULL(amp.[course_time],''), ISNULL(amp.[course_type],''), ISNULL(amp.[course_title],''), 
               ISNULL(amp.[course_title_long],''), ISNULL(amp.[participant_id],''), ISNULL(amp.[participant_name],''), ''
        FROM [dbo].[LV_COURSE_PARTICIPANT_EXTRAS] AS ext (NOLOCK)
             LEFT OUTER JOIN [dbo].[LV_COURSE_PARTICIPANTS] AS amp (NOLOCK) ON amp.[order_no] = ext.[order_no] AND amp.[course_date] = ext.[course_date] 
                                                                           AND amp.[participant_name] = ext.[participant_name] AND amp.[course_type] IN ('1/2 DAY AM','Full Day')
        WHERE CONVERT(DATE,ext.[course_date]) between @report_start_dt AND @report_end_dt AND ext.[extras_title] = 'Early Drop-Off'

        INSERT INTO #early_late_stay
        SELECT ext.[order_no], ext.[course_date], ext.[course_time], ext.[course_type], ext.[extras_title], ext.[extras_title_long], ext.[participant_id], ext.[participant_name], 
               ext.[participant_sort_name], ISNULL(pmp.[order_no],''), ISNULL(pmp.[course_date],''), ISNULL(pmp.[course_time],''), ISNULL(pmp.[course_type],''), ISNULL(pmp.[course_title],''), 
               ISNULL(pmp.[course_title_long],''), ISNULL(pmp.[participant_id],''), ISNULL(pmp.[participant_name],''), ''
        FROM [dbo].[LV_COURSE_PARTICIPANT_EXTRAS] AS ext (NOLOCK)
             LEFT OUTER JOIN [dbo].[LV_COURSE_PARTICIPANTS] AS pmp (NOLOCK) ON pmp.[order_no] = ext.[order_no] AND pmp.[course_date] = ext.[course_date] 
                                                                           AND pmp.[participant_name] = ext.[participant_name] AND pmp.[course_type] IN ('1/2 DAY PM', 'Full Day')
        WHERE CONVERT(DATE,ext.[course_date]) between @report_start_dt AND @report_end_dt AND ext.[extras_title] = 'Late Stay'


    /*  Update Values  */

        UPDATE #early_late_stay SET [extras_title_long] = 'AM Early Drop-Off' WHERE [extras_title] = 'Early Drop-Off'
        
        UPDATE #early_late_stay SET [extras_title_long] = 'PM Late Pick Up' WHERE [extras_title] = 'Late Stay'

        UPDATE #early_late_stay SET [extras_participant_id] = 0 WHERE [extras_participant_id] IS NULL
        
        UPDATE #early_late_stay 
        SET [course_date] = [extras_date], [course_time] = '??:??', [course_type] = '1/2 Day AM', [course_title] = 'NO MORNING COURSE FOUND', [course_title_long] = 'NO MORNING COURSE FOUND' 
        WHERE [course_order_no] = 0 AND [extras_type] = 'EARLY'
        
        UPDATE #early_late_stay 
        SET [course_date] = [extras_date], [course_time] = '??:??', [course_type] = '1/2 Day PM', [course_title] = 'NO AFTERNOON COURSE FOUND', [course_title_long] = 'NO AFTERNOON COURSE FOUND' 
        WHERE [course_order_no] = 0 AND [extras_type] = 'LATE'

        UPDATE #early_late_stay SET [extras_participant_name] = 'UNKNOWN PARTICIPANT' WHERE [extras_participant_id] = 0

        DECLARE double_check_cursor INSENSITIVE CURSOR FOR
        SELECT [extras_order_no], [extras_date], [extras_type], [extras_participant_name] FROM #early_late_stay WHERE course_order_no = 0 AND extras_participant_id > 0
        OPEN double_check_cursor
        BEGIN_DOUBLE_CHECK_LOOP:

            FETCH NEXT FROM double_check_cursor INTO @extras_order_no, @extras_date, @extras_type, @extras_participant_name
            IF @@FETCH_STATUS = -1 GOTO END_DOUBLE_CHECK_LOOP

            IF @extras_type = 'EARLY'
                SELECT @course_title = MAX([course_title]), @course_title_long = MAX([course_title_long]) FROM [dbo].[LV_COURSE_PARTICIPANTS] 
                WHERE [course_date] = @extras_date AND [course_type] IN ('1/2 DAY AM', 'FULL DAY') AND [participant_name] = @extras_participant_name
            ELSE
                SELECT @course_title = MAX([course_title]), @course_title_long = MAX([course_title_long]) FROM [dbo].[LV_COURSE_PARTICIPANTS] 
                WHERE [course_date] = @extras_date AND [course_type] IN ('1/2 DAY PM', 'FULL DAY') AND [participant_name] = @extras_participant_name

            SELECT @course_title = ISNULL(@course_title,'')
            SELECT @course_title_long = ISNULL(@course_title_long, @course_title)

            IF @course_title = '' AND @extras_type = 'EARLY' SELECT @course_title = 'NO MORNING COURSE FOUND', @course_title_long = 'NO MORNING COURSE FOUND'

            IF @course_title = '' AND @extras_type = 'LATE' SELECT @course_title = 'NO AFTERNOON COURSE FOUND', @course_title_long = 'NO AFTERNOON COURSE FOUND'

            UPDATE #early_late_stay 
            SET [course_title] = @course_title, [course_title_long] = @course_title_long
            WHERE [extras_order_no] = @extras_order_no AND [extras_date] = @extras_date AND [extras_type] = @extras_type 
              AND [extras_participant_name] = @extras_participant_name AND [course_order_no] = 0

            GOTO BEGIN_DOUBLE_CHECK_LOOP

        END_DOUBLE_CHECK_LOOP:
        CLOSE double_check_cursor
        DEALLOCATE double_check_cursor
                

    FINISHED:

        /*  If no records found, insert single record with message  */

            IF NOT EXISTS (SELECT * FROM #early_late_stay) BEGIN
                IF @rpt_message = '' SELECT @rpt_message = 'No records found for the criteria you entered.'
                INSERT INTO #early_late_stay VALUES  (0, '', '', '', '', '', 0, '', '', 0, '', '', '', '', '', 0, '', @rpt_message) 
            END

        /*  Select final data set to pass to the report  */

            SELECT [extras_order_no], [extras_date], [extras_time], [extras_type], [extras_title], [extras_title_long], [extras_participant_id], 
                   [extras_participant_name], [extras_participant_sort_name], [course_order_no], [course_date], [course_time], [course_type], 
                   [course_title], [course_title_long], [course_participant_id], [course_participant_name], [rpt_message]
            FROM #early_late_stay
            ORDER BY extras_date, extras_participant_name

END
GO

GRANT EXECUTE ON [dbo].[LRP_COURSE_EARLY_DROP_OFF_LATE_PICK_UP] TO ImpUsers
GO

--EXECUTE [dbo].[LRP_COURSE_EARLY_DROP_OFF_LATE_PICK_UP] '7-10-2017', '7-10-2017'





        --SELECT * FROM dbo.LV_PRODUCTION_ELEMENTS_PERFORMANCE WHERE performance_no IN (SELECT perf_no FROM dbo.T_SUB_LINEITEM WHERE Order_no = 503645 AND recipient_no = 3268516 )
        --(SELECT perf_no, recipient_no, sli_status FROM dbo.T_SUB_LINEITEM WHERE Order_no = 503645 AND recipient_no = 3268516 )


