USE [impresario]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[LRP_PLEDGE_PAYMENTS]
	@customer_no INT, 
	@printable_fund VARCHAR(80),
	@contrib_dt DATE -- not datetime    
AS
BEGIN

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
SET NOCOUNT ON;

--EXEC [LRP_PLEDGE_PAYMENTS] 90009, 'Blue Wing Founders Fund', '12/13/2016'

/*pledge payment - hard credit*/
SELECT 
	c.ref_no, 
	i.customer_no, 
	b.best_name, 
	tt.description type, 
	tt.description type_cred, 
	c.custom_1 soft_credit_type, 
	(SELECT fyear FROM TR_Batch_Period WHERE CAST(p.pmt_dt AS DATE) >= start_dt and CAST(p.pmt_dt AS DATE) <= end_dt) fyear, 
	CAST(p.pmt_dt AS DATE) activity_date, 
	CASE 
		WHEN CAST(c.custom_6 AS DATE) = '1900-01-01' THEN NULL 
		ELSE CAST(c.custom_6 AS DATE) 
	END AS agr_date,
	CASE 
		WHEN KG_xfer_dt > '1900-01-01 00:00:00.000' THEN 'Y' 
		ELSE 'N' 
	END gik_flag, 
	CAST (c.KG_xfer_dt AS DATE) gik_date,
	p.pmt_amt amount_received, 
	p.pmt_amt amount,
	p.pmt_amt-p.pmt_amt balance,
	f.description fund_description,  
	fd.printable_fund,
	cc.description cmcategory, 
	cd.description designation, 
	ag1.description acctgoal, 
	ag2.description acctgrp, 
	CASE 
		WHEN fd.acct_goal_no = 3 THEN 'Unrestricted' 
		WHEN fd.acct_goal_no = 5 THEN 'Skip' 
		WHEN fd.acct_goal_no IN (1,4,3) AND fd.acct_grp_no = 10 THEN 'Unrestricted' 
		ELSE 'Restricted' 
	END AS unr_rest,
	c.custom_3 cont_detail1, 
	c.custom_4 cont_detail2, 
	c.custom_5 anonymous,
	i.sort_name,
	REPLACE(p.notes, char(13) + char(10), ' ') notes
FROM T_TRANSACTION AS t
INNER JOIN T_PAYMENT AS p
	ON t.sequence_no = p.sequence_no
INNER JOIN TR_TRANSACTION_TYPE AS tt
	ON t.trn_type = tt.id
INNER JOIN T_CONTRIBUTION AS c
	ON t.ref_no = c.ref_no
INNER JOIN LTR_FUND_DETAIL AS fd
	ON c.fund_no = fd.fund_no
INNER JOIN TR_CONT_DESIGNATION AS cd
	ON fd.designation = cd.id
LEFT OUTER JOIN LTR_ACCT_GOAL AS ag1
	ON fd.acct_goal_no = ag1.id
LEFT OUTER JOIN LTR_ACCT_GRP AS ag2
	ON fd.acct_grp_no = ag2.id
INNER JOIN T_CAMPAIGN AS g
	ON c.campaign_no = g.campaign_no
INNER JOIN TR_CAMPAIGN_CATEGORY AS cc
	ON g.category = cc.id
INNER JOIN T_FUND AS f
	ON c.fund_no = f.fund_no
INNER JOIN T_CUSTOMER AS i
	ON c.customer_no = i.customer_no
INNER JOIN LV_ADV_BEST_NAME AS b
	ON c.customer_no = b.customer_no
WHERE t.trn_type IN (3,6)
	AND c.cont_type = 'P'
	AND c.custom_1 in ('(none)', 'Matching Gift Credit')
	AND c.cont_amt > 0 
	AND fd.printable_fund = @printable_fund
	AND DATEDIFF(d,c.cont_dt,@contrib_dt) = 0 
	AND i.customer_no = @customer_no

UNION ALL 

/*pledge payment - soft credit*/
SELECT 
	c.ref_no,
	i.customer_no, 
	b.best_name, 
	tt.description AS type, 
	r.descriptiuon AS type_cred,  
	c.custom_1 AS soft_credit_type, 
	(SELECT fyear FROM TR_Batch_Period WHERE CAST(p.pmt_dt AS DATE) >= start_dt and CAST(p.pmt_dt AS DATE) <= end_dt) fyear, 
	CAST(p.pmt_dt AS DATE) activity_date, 
	CASE 
		WHEN CAST(c.custom_6 AS DATE) = '1900-01-01' 
		THEN NULL 
		ELSE CAST(c.custom_6 AS DATE) 
	END AS agr_date,
	CASE 
		WHEN KG_xfer_dt > '1900-01-01 00:00:00.000' THEN 'Y' 
		ELSE 'N' 
	END AS gik_flag, 
	CAST(c.KG_xfer_dt AS DATE) gik_date,
	p.pmt_amt AS amount_received,
	p.pmt_amt AS amount, 
	p.pmt_amt - p.pmt_amt AS balance,
	f.description fund_description, 
	fd.printable_fund,
	cc.description cmcategory, 
	cd.description designation, 
	ag1.description acctgoal, 
	ag2.description acctgrp,
	CASE 
		WHEN fd.acct_goal_no = 3 THEN 'Unrestricted' 
		WHEN fd.acct_goal_no = 5 THEN 'Skip' 
		WHEN fd.acct_goal_no IN (1,4,3) AND fd.acct_grp_no = 10 THEN 'Unrestricted' 
		ELSE 'Restricted'
	END AS unr_rest,
	c.custom_3 cont_detail1, 
	c.custom_4 cont_detail2, 
	c.custom_5 anonymous, 
	i.sort_name,
	REPLACE(p.notes, char(13) + char(10), ' ') notes
FROM T_TRANSACTION AS t
INNER JOIN T_PAYMENT AS p
	ON t.sequence_no = p.sequence_no
INNER JOIN TR_TRANSACTION_TYPE AS tt
	ON t.trn_type = tt.id
INNER JOIN T_CONTRIBUTION AS c
	ON t.ref_no = c.ref_no
INNER JOIN T_CREDITEE AS cr
	ON c.ref_no=  cr.ref_no
INNER JOIN TR_CREDITEE_TYPE AS r
	ON cr.creditee_type = r.id
INNER JOIN LTR_FUND_DETAIL AS fd
	ON c.fund_no = fd.fund_no
INNER JOIN TR_CONT_DESIGNATION AS cd
	ON fd.designation = cd.id
LEFT OUTER JOIN LTR_ACCT_GOAL AS ag1
	ON fd.acct_goal_no = ag1.id
LEFT OUTER JOIN LTR_ACCT_GRP AS ag2
	ON fd.acct_grp_no = ag2.id
INNER JOIN T_CUSTOMER AS i
	ON cr.creditee_no = i.customer_no
INNER JOIN T_CAMPAIGN AS g
	ON c.campaign_no = g.campaign_no
INNER JOIN TR_CAMPAIGN_CATEGORY AS cc
	ON g.category = cc.id
INNER JOIN T_FUND AS f
	ON c.fund_no = f.fund_no
INNER JOIN LV_ADV_BEST_NAME AS b
	ON i.customer_no = b.customer_no
WHERE t.trn_type IN (3, 6) 
	AND c.cont_type = 'P'
	AND c.custom_1 IN ('Primary Soft Credit', 'Stewardship Soft Credit', 'Matching Gift Credit')
	AND cr.creditee_type in ('12', '15', '5', '10', '16', '14', '1')
	AND c.cont_amt > 0 
	AND fd.printable_fund = @printable_fund
	AND DATEDIFF(d,c.cont_dt,@contrib_dt) = 0 
	AND i.customer_no = @customer_no

END 