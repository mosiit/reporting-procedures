USE [impresario]; -- NOTES add count of title and delete sales info from rows 2+?  OR rownum and break into venue 1, venue 2 etc.
GO

SET ANSI_NULLS ON;
GO

SET QUOTED_IDENTIFIER ON;
GO
--DROP PROCEDURE [dbo].[LRP_WEB_CART_DATA_DUMP]
GO
--ALTER PROCEDURE [dbo].[LRP_WEB_CART_DATA_DUMP]
CREATE PROCEDURE [dbo].[LRP_WEB_CART_DATA_DUMP]
(
    @order_start_dt DATETIME,
    @order_end_dt DATETIME
)
AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
SET NOCOUNT ON;
-- =============================================
-- Author: Aileen Duffy-Brown
-- Create date: 5/31/2018
-- TrackIt! Work Order #92731
-- Description:	Website cart data dump to excel
-- based on order date with date range parameters.
-- =============================================
BEGIN

    --Check Parameters
    SELECT @order_start_dt = ISNULL(@order_start_dt, '1/1/1900');
    SELECT @order_end_dt = ISNULL(@order_end_dt, '12/31/2999');

    CREATE TABLE #orders
    (
        order_no INT,
        customer_no INT,
        mode_of_sale VARCHAR(30),
        order_date DATETIME,
        total_ticket_cost MONEY,
        contribution_total MONEY,
        total_cart MONEY
    );

    INSERT INTO #orders
    (
        order_no,
        customer_no,
        mode_of_sale,
        order_date,
        total_ticket_cost,
        contribution_total,
        total_cart
    )
    SELECT o.order_no,
           o.customer_no,
           m.description AS 'mode_of_sale',
           FORMAT(o.order_dt, 'd', 'en') AS 'order_date',
           SUM(o.tot_ticket_purch_amt + o.tot_ticket_return_amt) AS 'total_ticket_cost',
           o.tot_contribution_paid_amt AS 'contribution_total',
           o.tot_paid_amt AS 'total_cart'
    FROM dbo.T_ORDER AS o
        LEFT OUTER JOIN dbo.TR_MOS AS m
            ON m.id = o.MOS
        LEFT OUTER JOIN dbo.TR_SALES_CHANNEL AS s
            ON s.id = o.channel
    WHERE o.order_dt
          BETWEEN @order_start_dt AND @order_end_dt
          AND m.description = 'web sales'
          AND s.description = 'web'
    GROUP BY o.order_no,
             o.customer_no,
             m.description,
             o.order_dt,
             o.tot_contribution_paid_amt,
             o.tot_paid_amt;

    --SELECT * FROM #orders;


    CREATE TABLE #order_detail
    (
        inv_no INT,
        show_title VARCHAR(30),
        order_no INT,
        perf_no INT,
        sli_status INT,
        ticket_count INT
    );

    INSERT INTO #order_detail
    (
        inv_no,
        show_title,
        order_no,
        perf_no,
        sli_status,
        ticket_count
    )
    SELECT i.inv_no,
           i.description AS show_title,
           d.order_no,
           d.perf_no,
           d.sli_status,
           1 AS ticket_count
    FROM dbo.T_SUB_LINEITEM AS d
        LEFT OUTER JOIN dbo.T_INVENTORY AS i
            ON i.inv_no = d.perf_no
    WHERE d.order_no IN
          (
              SELECT order_no FROM #orders
          )
          AND d.sli_status IN ( 3, 12, 2, 6 ); -- Seated, Paid, Ticketed, Unpaid, unseated_paid

    --SELECT * FROM #order_detail;

    --final select
    SELECT ord.order_no,
           ord.customer_no,
           ord.mode_of_sale,
           ord.order_date,
           ord.total_ticket_cost,
           ord.contribution_total,
           ord.total_cart,
           det.show_title,
           det.perf_no,
           det.sli_status,
           SUM(det.ticket_count) AS ticket_count
    FROM #orders AS ord
        JOIN #order_detail AS det
            ON det.order_no = ord.order_no
    GROUP BY ord.order_no,
             det.perf_no,
             ord.customer_no,
             ord.order_date,
             ord.mode_of_sale,
             ord.total_ticket_cost,
             ord.contribution_total,
             ord.total_cart,
             det.show_title,
             det.sli_status;

    DROP TABLE #orders;
    DROP TABLE #order_detail;
END;

GO
GRANT EXECUTE ON [dbo].[LRP_WEB_CART_DATA_DUMP] TO ImpUsers;



