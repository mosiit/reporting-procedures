USE impresario;
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_ATTEND_BUDGET_VS_ACTUAL]') AND type in (N'P', N'PC')) BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_ATTEND_BUDGET_VS_ACTUAL] AS';
    EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LRP_ATTEND_BUDGET_VS_ACTUAL] TO impusers';
END;
GO

ALTER PROCEDURE [dbo].[LRP_ATTEND_BUDGET_VS_ACTUAL]
        @week_ending DATETIME = NULL
AS BEGIN

    SET NOCOUNT ON;
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

    /*  Procedure Variables  */

        DECLARE @fiscal_year INT = 0
        DECLARE @week_start_dt DATETIME, @week_end_dt DATETIME;
        DECLARE @month_start_dt DATETIME, @month_end_dt DATETIME;

        DECLARE @prev_fiscal_year INT = 0
        DECLARE @prev_week_start_dt DATETIME, @prev_week_end_dt DATETIME; 
        DECLARE @prev_month_start_dt DATETIME, @prev_month_end_dt DATETIME;

        DECLARE @start_dt DATETIME, @end_dt DATETIME, @end_dt_budget DATETIME, @prev_end_dt_budget DATETIME
        DECLARE @prev_start_dt DATETIME, @prev_end_dt DATETIME
   
        DECLARE @gross_attend INT = 0, @prev_gross_attend INT = 0;

        --These are the title numbers for the various venues
        DECLARE @gross_attend_no INT = 0,           @exhibit_halls_no INT = 27,         @special_exhibitions_no INT = 37,   
                @butterfly_garden_no INT = 157,     @mugar_omni_theater_no INT = 161,   @4d_theater_no INT = 173,
                @hayden_planetarium_no INT = 1132,  @thrill_ride_no INT = 1343,         @entertainment_no INT = 25;
        
        DECLARE @entertainment_title_no INT = 25;       --Assigned an invenyory number not being used as a place holder
                                                        --this number is not used anywhere else but the Attendance Reports

        DECLARE @exhibit_prod_no INT = 32

        DECLARE @final_table TABLE ([fiscal_year] INT,
                                    [title_no] INT,
                                    [title_name] VARCHAR(30),
                                    [title_order] CHAR(1),
                                    [monthly_budget] INT,
                                    [month_to_date_budget] INT,
                                    [current_actual] INT,
                                    [previous_actual] INT,
                                    [month_start_dt] DATE,
                                    [month_end_dt] DATE,
                                    [week_start_dt] DATE,
                                    [week_end_dt] DATE)
           
    /*  Tepmporary Tables  */

        IF OBJECT_ID('tempdb..#budget_table') IS NOT NULL DROP TABLE [#budget_table];

        CREATE TABLE [#budget_table] ([fiscal_year] INT NULL DEFAULT (0),
                                      [title_no] INT NULL DEFAULT(0),
                                      [title_group] VARCHAR(30) NULL DEFAULT (''),
                                      [title_name] VARCHAR(30) NULL DEFAULT (''),
                                      [title_order] CHAR(1) NULL DEFAULT ('Z'),
                                      [monthly_budget] INT NULL DEFAULT (0),
                                      [month_to_date_budget] INT NULL DEFAULT (0));
        
        IF OBJECT_ID('tempdb..#attendance_history_data') IS NOT NULL DROP TABLE [#attendance_history_data];

        CREATE TABLE [#attendance_history_data] ([fiscal_year] INT NOT NULL DEFAULT(0),
                                                 [title_no] INT NOT NULL DEFAULT (0), 
                                                 [title_name] VARCHAR(50) NOT NULL DEFAULT (''),
                                                 [production_no] INT NOT NULL DEFAULT (0),
                                                 [production_name] VARCHAR(50) NOT NULL DEFAULT (''),
                                                 [sale_total] INT NOT NULL DEFAULT (0),
                                                 [scan_total] INT NOT NULL DEFAULT (0))

    /*  Check Parameters  */

        IF @week_ending IS NULL GOTO FINISHED

        SELECT @week_ending = CAST(ISNULL(@week_ending,GETDATE()) AS DATE)
    
    /*  Get date values from LFT_ATTENDANCE_WEEK_DATES function  */
            
        SELECT @fiscal_year = [cur_fiscal_year],
               @week_start_dt = [cur_week_start_dt], 
               @week_end_dt = [cur_week_end_dt], 
               @month_start_dt = [cur_month_start_dt], 
               @month_end_dt = [cur_month_end_dt], 
               @prev_fiscal_year = [prv_fiscal], 
               @prev_week_start_dt = [prv_week_start_dt], 
               @prev_week_end_dt = [prv_week_end_dt], 
               @prev_month_start_dt = [prv_month_start_dt], 
               @prev_month_end_dt = [prv_month_end_dt]
        FROM [dbo].[LFT_ATTENDANCE_WEEK_DATES] (@week_ending, 0, 'N');

        /*  Set the start and end dates for the month to date numbers - based on week ending date  */

        IF DATEPART(MONTH,@month_start_dt) < DATEPART(MONTH,@week_end_dt)
            SELECT @start_dt = DATEADD(MONTH,1,@month_start_dt),
                   @prev_start_dt = DATEADD(MONTH,1,@prev_month_start_dt)
        ELSE
            SELECT @start_dt = @month_start_dt,
                   @prev_start_dt = @prev_month_start_dt;        

        SELECT @end_dt = @week_end_dt,
               @prev_end_dt = @prev_week_end_dt;

        SELECT @end_dt_budget = EOMONTH(@start_dt),
               @prev_end_dt_budget = EOMONTH(@prev_start_dt);
        
      /*  Gather Budget information
          CTE is total monthly budget for entire month.
          Main body of statement selects budget for month to date (based on @week_ending parameter)  */
        WITH CTE_TOTAL_MONTH_BUDGET ([fiscal_year], [title_no], [title_group], [title_name], [monthly_budget])
           AS (
               SELECT CASE WHEN bud.[perf_dt] BETWEEN @prev_start_dt AND @prev_end_dt_budget THEN @prev_fiscal_year
                           ELSE @fiscal_year END,
                      bud.[title_no], 
                      bud.[title_group],
                      CASE WHEN bud.[title_no] = 0 AND bud.[title_group] = 'Entire Museum' THEN 'Gross Attendance'
                           WHEN bud.[title_no] = 25 AND bud.[title_group] = 'Planetarium' THEN 'Entertainment'
                           ELSE ISNULL(inv.[description],'') END,
                      SUM(bud.[budget])
               FROM [dbo].[LTR_ATTENDANCE_BUDGET_DATA] AS bud
                    LEFT OUTER JOIN [dbo].[T_INVENTORY] AS inv ON inv.[inv_no] = bud.[title_no]
               WHERE bud.[perf_dt] BETWEEN @start_dt AND @end_dt_budget
                     OR bud.[perf_dt] BETWEEN @prev_start_dt AND @prev_end_dt_budget
               GROUP BY CASE WHEN bud.[perf_dt] BETWEEN @prev_start_dt AND @prev_end_dt_budget THEN @prev_fiscal_year
                           ELSE @fiscal_year END, 
                        bud.[title_no], 
                        bud.[title_group], 
                        CASE WHEN bud.[title_no] = 0 AND bud.[title_group] = 'Entire Museum' THEN 'Gross Attendance'
                             WHEN bud.[title_no] = 25 AND bud.[title_group] = 'Planetarium' THEN 'Entertainment'
                             ELSE ISNULL(inv.[description],'') END
              )
            INSERT INTO [#budget_table] ([fiscal_year], [title_no], [title_group], [title_name], [monthly_budget], [month_to_date_budget])
            SELECT dbo.LF_GetFiscalYear(bud.perf_dt),
                   bud.[title_no], 
                   bud.[title_group],
                   CASE WHEN bud.[title_no] = 0 AND bud.[title_group] = 'Entire Museum' THEN 'Gross Attendance'
                        WHEN bud.[title_no] = 25 AND bud.[title_group] = 'Planetarium' THEN 'Entertainment'
                        ELSE ISNULL(inv.[description],'') END AS [title],
                   tot.[monthly_budget],
                   SUM(bud.[budget]) AS [month_to_date_budget]
            FROM [dbo].[LTR_ATTENDANCE_BUDGET_DATA] AS bud
                 LEFT OUTER JOIN [dbo].[T_INVENTORY] AS inv ON inv.inv_no = bud.title_no
                 LEFT OUTER JOIN [CTE_TOTAL_MONTH_BUDGET] AS tot ON tot.[title_no] = bud.[title_no] AND tot.[title_group] = bud.[title_group] AND tot.[fiscal_year] = dbo.LF_GetFiscalYear(bud.perf_dt)
            WHERE bud.[perf_dt] BETWEEN @start_dt AND @end_dt
               OR bud.[perf_dt] BETWEEN @prev_start_dt AND @prev_end_dt
            GROUP BY dbo.LF_GetFiscalYear(bud.perf_dt),
                     bud.[title_no], 
                     bud.[title_group], 
                     CASE WHEN bud.[title_no] = 0 AND bud.[title_group] = 'Entire Museum' THEN 'Gross Attendance'
                          WHEN bud.[title_no] = 25 AND bud.[title_group] = 'Planetarium' THEN 'Entertainment'
                          ELSE ISNULL(inv.[description],'') END, 
                     tot.[monthly_budget]
            ORDER BY bud.title_group
         
         
        IF NOT EXISTS (SELECT * FROM [#budget_table] WHERE fiscal_year = @fiscal_year)
           AND EXISTS (SELECT * FROM [#budget_table] WHERE fiscal_year = @prev_fiscal_year)
                INSERT INTO [#budget_table] ([fiscal_year], [title_no], [title_group], [title_name], [title_order], [monthly_budget], [month_to_date_budget])
                SELECT @fiscal_year, 
                       [title_no],
                       [title_group],
                       [title_name],
                       'Z',
                       0,
                       0
                FROM [#budget_table]
                WHERE [fiscal_year] = @prev_fiscal_year


                INSERT INTO [#budget_table] ([fiscal_year],[title_no],[title_group],[title_name],[title_order],[monthly_budget],[month_to_date_budget])
                VALUES (2019, 37, 'Exhibits', 'Special Exhibitions', 'Z', 0, 0)

       
    /* Set Title Order so venues are displayed in the correct order on the report  */

        UPDATE [#budget_table]
        SET [title_order] = CASE WHEN [title_no] = @gross_attend_no THEN 'A'
                                 WHEN [title_no] = @exhibit_halls_no THEN 'B'
                                 WHEN [title_no] = @special_exhibitions_no THEN 'C'
                                 WHEN [title_no] = @butterfly_garden_no THEN 'D'
                                 WHEN [title_no] = @mugar_omni_theater_no THEN 'E'
                                 WHEN [title_no] = @4d_theater_no THEN 'F'
                                 WHEN [title_no] = @thrill_ride_no THEN 'G'
                                 WHEN [title_no] = @hayden_planetarium_no THEN 'H'
                                 WHEN [title_no] = @entertainment_no THEN 'I'
                                 ELSE 'Z' END;

    /*  Shorten Title Names - To Save Space  */

        UPDATE [#budget_table]
        SET [title_name] =  CASE WHEN [title_no] = @mugar_omni_theater_no THEN 'Omni Theater'
                                 WHEN [title_no] = @hayden_planetarium_no THEN 'Planetarium'
                                 ELSE [title_name] END;

    /*  Get History Data  */

        INSERT INTO [#attendance_history_data] ([fiscal_year], [title_no], [title_name], [production_no], [production_name], [sale_total], [scan_total])
        SELECT CASE WHEN TRY_CONVERT(DATETIME,[performance_date]) BETWEEN @prev_start_dt AND @prev_end_dt THEN @prev_fiscal_year
                    ELSE @fiscal_year END,
               [title_no],
               [title_name],
               [production_no],
               [production_name],
               SUM([sale_total]),
               SUM([scan_admission_total])
        FROM [dbo].[LT_HISTORY_TICKET]
        WHERE (TRY_CONVERT(DATE,[performance_date]) BETWEEN @start_dt AND @end_dt
               OR TRY_CONVERT(DATE,[performance_date]) BETWEEN @prev_start_dt AND @prev_end_dt)
          AND LEFT([attendance_type],6) = 'Gate A'
        GROUP BY CASE WHEN TRY_CONVERT(DATETIME,[performance_date]) BETWEEN @prev_start_dt AND @prev_end_dt THEN @prev_fiscal_year
                      ELSE @fiscal_year END,
                 [title_no], 
                 [title_name], 
                 [production_no], 
                 [production_name];


    /*  Add Member Scan, Show and Go, Show and Collected, and GoBoston counts to the Exhibit Hall Attendance numbers */
                 
        WITH CTE_EXTRA_EXHIBIT_HALL_ATTENDANCE ([fiscal_year], [extra_exh_attendance])
        AS (                 
            SELECT  [dbo].[LF_GetFiscalYear](history_dt), SUM([visit_count]) 
            FROM [dbo].[LT_HISTORY_VISIT_COUNT] 
            WHERE ([history_dt] BETWEEN @start_dt AND @end_dt
                    OR [history_dt] BETWEEN @prev_start_dt AND @prev_end_dt)
                 AND [attendance_type] <> 'Ticketed Events' 
                 GROUP BY dbo.LF_GetFiscalYear(history_dt)
           )
         UPDATE att
         SET att.[sale_total] += exh.[extra_exh_attendance],
             att.[scan_total] += exh.[extra_exh_attendance]
         FROM [#attendance_history_data] AS att
              LEFT OUTER JOIN [CTE_EXTRA_EXHIBIT_HALL_ATTENDANCE] AS exh ON exh.fiscal_year = att.fiscal_year 
         WHERE att.title_no = @exhibit_halls_no 
           AND att.[production_no] = @exhibit_prod_no;

    /*  Determine which are entertainment shows (Planetarium)
        CTE pulls list of all entertainment shows (shows that contain the proper content record and value)
        UPDATE sets title_no to 25 and title_name to 'Entertainment'  on all entertainment shows  */

        WITH [CTE_ENTERTAINMENT_SHOWS] ([prod_no], [prod_name], [cont_value])
        AS (
            SELECT pro.[prod_no], inv.[description], con.[value]
            FROM [dbo].[T_PRODUCTION] AS pro
                 INNER JOIN [dbo].[T_INVENTORY] AS inv ON inv.[inv_no] = pro.[prod_no]
                 INNER JOIN [dbo].[TX_INV_CONTENT] AS con ON con.[inv_no] = pro.[prod_no] 
                                                         AND con.[content_type] = 37
            WHERE pro.[title_no] = 1132
              AND con.[value] = 'Entertainment'
           )
        UPDATE [#attendance_history_data]
        SET [title_no] =  25,
            [title_name] = 'Entertainment'
        WHERE [production_no] IN (SELECT [prod_no] 
                                  FROM [CTE_ENTERTAINMENT_SHOWS])

    /*  Get month to date gross attendance (visit count)  */

        

        SELECT @gross_attend = SUM(ISNULL([visit_count],0)) 
                               FROM [dbo].[LT_HISTORY_VISIT_COUNT] 
                               WHERE [history_dt] BETWEEN @start_dt AND @end_dt;
        
        SELECT @prev_gross_attend = SUM(ISNULL([visit_count],0)) 
                                    FROM [dbo].[LT_HISTORY_VISIT_COUNT] 
                                    WHERE [history_dt] BETWEEN @prev_start_dt AND @prev_end_dt;

        WITH [CTE_TOTALS_BY_TITLE] ([fiscal_year], [title_no], [title_name], [sale_total])
            AS (SELECT [fiscal_year],
                       [title_no],
                       [title_name],
                       SUM([sale_total])
                FROM [#attendance_history_data]
                GROUP BY [fiscal_year], [title_no], [title_name])                            
        INSERT INTO @final_table ([fiscal_year],[title_no],[title_name],[title_order],[monthly_budget],[month_to_date_budget],
                                  [current_actual],[previous_actual],[month_start_dt],[month_end_dt],[week_start_dt],[week_end_dt])
        SELECT bud.[fiscal_year],
                   bud.[title_no],
                   bud.[title_name],
                   bud.[title_order],
                   bud.monthly_budget,
                   bud.[month_to_date_budget],
                   CASE WHEN bud.[title_no] = 0 THEN @gross_attend
                        ELSE SUM(ISNULL(act.[sale_total],0)) END AS [current_actual],
                   CASE WHEN bud.[title_no] = 0 THEN @prev_gross_attend
                        ELSE SUM(ISNULL(prv.[sale_total],0)) END AS [previous_actual],
                   CAST(@start_dt AS date) AS [month_start_dt],
                   CAST(EOMONTH(@start_dt) AS DATE) AS [month_end_dt],
                   CAST(@week_start_dt AS DATE) AS [week_start_dt],
                   CAST(@week_end_dt AS DATE) AS [week_end_dt]
                   --CAST(@prev_month_start_dt AS DATE) AS [prev_month_start_dt],
                   --CAST(@prev_month_end_dt AS DATE) AS [prev_month_end_dt]
            FROM [#budget_table] AS bud
                 LEFT OUTER JOIN [CTE_TOTALS_BY_TITLE] AS act ON act.[title_no] = bud.[title_no] AND act.[fiscal_year] = @fiscal_year
                 LEFT OUTER JOIN [CTE_TOTALS_BY_TITLE] AS prv ON prv.[title_no] = bud.[title_no] AND prv.[fiscal_year] = @prev_fiscal_year
            WHERE bud.[fiscal_year] = @fiscal_year
            GROUP BY bud.[fiscal_year],
                     bud.[title_no],
                     bud.[title_name], 
                     bud.[title_order], 
                     bud.monthly_budget, 
                     bud.[month_to_date_budget]
            ORDER BY bud.[fiscal_year], 
                     bud.[title_order]

    /*  Delete records with no budget  */

        DELETE FROM @final_table WHERE [monthly_budget] = 0 AND [month_to_date_budget] = 0 AND [current_actual] = 0;

    FINISHED:
       
        /*  Get final data set  */
        SELECT [fiscal_year],
               [title_no],
               [title_name],
               [title_order],
               [monthly_budget],
               [month_to_date_budget],
               [current_actual],
               [previous_actual],
               [month_start_dt],
               [month_end_dt],
               [week_start_dt],
               [week_end_dt]
        FROM @final_table

    DONE:

        IF OBJECT_ID('tempdb..#attendance_history_data') IS NOT NULL DROP TABLE [#attendance_history_data];
        IF OBJECT_ID('tempdb..#budget_table') IS NOT NULL DROP TABLE [#budget_table];

END
GO

EXECUTE [dbo].[LRP_ATTEND_BUDGET_VS_ACTUAL] @week_ending = '3-7-2021'
