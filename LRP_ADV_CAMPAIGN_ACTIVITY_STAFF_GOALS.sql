--USE impresario
--GO

--SET ANSI_NULLS ON
--SET QUOTED_IDENTIFIER ON
--GO

--IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_ADV_CAMPAIGN_ACTIVITY_STAFF_GOALS]') AND type IN (N'P', N'PC'))
--BEGIN
--    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_ADV_CAMPAIGN_ACTIVITY_STAFF_GOALS] AS' 
--    EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LRP_ADV_CAMPAIGN_ACTIVITY_STAFF_GOALS] TO impusers'
--END
--GO

--/*      LRP_ADV_CAMPAIGN_ACTIVITY_STAFF_GOALS
--        Pulls information for goals, numbers, and YTD percentages for all step activity, vists, ask/yield amounts, and actual contributions taken in.

--        NOTE: Contribution amounts are based on what's in The Plan record under cont_amount

--*/
--ALTER PROCEDURE dbo.LRP_ADV_CAMPAIGN_ACTIVITY_STAFF_GOALS
DECLARE        @fiscal_year INT = NULL
--WITH RECOMPILE AS BEGIN

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

    /*  Check Fiscal Year Parameter - If none passed, set to current  */
        IF @fiscal_year IS NULL SELECT @fiscal_year = dbo.LF_GetFiscalYear(GETDATE())

    /* Procedure Variables  */

        --Determine current fiscal year, fiscal start date, fiscal end date, and current date
        DECLARE @fiscal_start_dt DATETIME = dbo.LF_GetFiscalYearStartDt(@fiscal_year)
        DECLARE @fiscal_end_dt DATETIME = dbo.LF_GetFiscalYearEndDt(@fiscal_year)
        DECLARE @current_dt DATETIME = GETDATE()
        IF @current_dt > @fiscal_end_dt SELECT @current_dt = @fiscal_end_dt

    /*  Temporary Tables  */
                                    
        IF OBJECT_ID('tempdb..#step_raw_data') IS NOT NULL DROP TABLE #step_raw_data

        CREATE TABLE #step_raw_data  ([step_no] INT NOT NULL DEFAULT (0), 
                                      [plan_no] INT NOT NULL DEFAULT (0), 
                                      [step_dt] DATETIME NULL, 
                                      [completed_on_dt] DATETIME NULL,
                                      [month_num] INT NOT NULL DEFAULT (0), 
                                      [month_abbrev] VARCHAR(30) NOT NULL DEFAULT (''),
                                      [month_sort] VARCHAR(7) NOT NULL DEFAULT (''),
                                      [month_year] VARCHAR(4) NOT NULL DEFAULT (''),
                                      [fiscal_year] INT NOT NULL DEFAULT (0),
                                      [is_current_fiscal] INT NOT NULL DEFAULT (0),
                                      [step_type] INT NOT NULL DEFAULT (0), 
                                      [step_type_name] VARCHAR(30) NOT NULL DEFAULT (''), 
                                      [step_description] VARCHAR(30) NOT NULL DEFAULT(''), 
                                      [associate_no] INT NOT NULL DEFAULT (0),
                                      [priority] INT NOT NULL DEFAULT (0), 
                                      [worker_customer_no] INT NOT NULL DEFAULT (0),
                                      [worker_name] VARCHAR(75) NOT NULL DEFAULT (''),
                                      [worker_initials] VARCHAR(10) NOT NULL DEFAULT (''),
                                      [worker_inactive] CHAR(1) NOT NULL DEFAULT ('N'),
                                      [is_visit] CHAR(1) NOT NULL DEFAULT ('N')) 

        IF OBJECT_ID('tempdb..#plan_data') IS NOT NULL DROP TABLE #plan_data

        CREATE TABLE #plan_data ([plan_no] INT NOT NULL DEFAULT (0),
                                 [worker_customer_no] INT NOT NULL DEFAULT (0),
                                 [worker_name] VARCHAR(75) NOT NULL DEFAULT (0),
                                 [worker_initials] VARCHAR(75) NOT NULL DEFAULT (0),
                                 [worker_inactive] CHAR(1) NOT NULL DEFAULT ('N'),
                                 [complete_by_dt] DATETIME NULL,
                                 [ask_amount] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                 [goal_amount] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                 [plan_amount] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                 [contribution_amount] DECIMAL (18,2) NOT NULL DEFAULT (0.0))
    
        IF OBJECT_ID('tempdb..#worker_activity') IS NOT NULL DROP TABLE #worker_activity

        CREATE TABLE #worker_activity ([worker_customer_no] INT NOT NULL DEFAULT (0),
                                       [worker_name] VARCHAR(75) NOT NULL DEFAULT (''),
                                       [worker_initials] VARCHAR(75) NOT NULL DEFAULT (''),
                                       [worker_inactive] CHAR(1) NOT NULL DEFAULT ('N'),
                                       [goal_visits] DECIMAL(18,4) NOT NULL DEFAULT (0.0),
                                       [actual_visits] DECIMAL(18,4) NOT NULL DEFAULT (0.0),
                                       [goal_hard_contacts] DECIMAL(18,4) NOT NULL DEFAULT (0.0),
                                       [actual_hard_contacts] DECIMAL(18,4) NOT NULL DEFAULT (0.0),
                                       [goal_soft_contacts] DECIMAL(18,4) NOT NULL DEFAULT (0.0),
                                       [actual_soft_contacts] DECIMAL(18,4) NOT NULL DEFAULT (0.0),
                                       [goal_ask_amount] DECIMAL(18,4) NOT NULL DEFAULT (0.0),
                                       [actual_ask_amount] DECIMAL (18,4) NOT NULL DEFAULT (0.0),
                                       [goal_expected_amount] DECIMAL(18,4) NOT NULL DEFAULT (0.0),
                                       [actual_expected_amount] DECIMAL(18,4) NOT NULL DEFAULT (0.0),
                                       [goal_prim_sol_amount] DECIMAL(18,4) NOT NULL DEFAULT (0.0),
                                       [actual_prim_sol_amount] DECIMAL(18,4) NOT NULL DEFAULT (0.0))

    /*  Pulls a list of all visits that were completed in the past twelve months  */
    
        INSERT INTO [#step_raw_data] ([step_no], [plan_no], [step_dt], [completed_on_dt], [month_num], [month_abbrev], [month_sort], [month_year], 
                                      [fiscal_year], [is_current_fiscal], [step_type], [step_type_name], [step_description], [associate_no], 
                                      [priority], [worker_customer_no], [worker_name], [worker_initials], [worker_inactive], [is_visit])
        SELECT stp.[step_no],
               stp.[plan_no], 
               stp.[step_dt], 
               stp.[completed_on_dt],
               DATEPART(MONTH,stp.[completed_on_dt]),
               LEFT(DATENAME(MONTH,stp.[completed_on_dt]),3),
               LEFT(CONVERT(VARCHAR(10),stp.[completed_on_dt],111),7),
               DATENAME(YEAR,stp.[completed_on_dt]),
               CASE WHEN  stp.[completed_on_dt] < @fiscal_start_dt THEN (@fiscal_year - 1) ELSE @fiscal_year END,
               CASE WHEN stp.[completed_on_dt] < @fiscal_start_dt THEN 0 ELSE 1 END,
               stp.[step_type], 
               typ.[description], 
               stp.[description], 
               ISNULL(stp.[associate_no],0),
               stp.[priority], 
               stp.[worker_customer_no],
               wrk.[worker_name],
               wrk.[worker_initials],
               wrk.[inactive],
               CASE WHEN typ.[id] = 29 THEN 'Y' ELSE 'N' END
        FROM [dbo].[T_STEP] AS stp
             INNER JOIN [dbo].[LV_CAMPAIGN_ACTIVITY_WORKERS] AS wrk ON wrk.[worker_customer_no] = stp.[worker_customer_no] AND wrk.[inactive] = 'N'
             INNER JOIN [dbo].[TR_STEP_TYPE] AS typ ON typ.[id] = stp.[step_type]
        WHERE stp.[completed_on_dt] BETWEEN @fiscal_start_dt AND @current_dt
        
    /*  Pull the plan data to be used  */

        INSERT INTO [#plan_data] ([plan_no], [worker_customer_no], [worker_name], [worker_initials], [worker_inactive], [complete_by_dt], [ask_amount], [goal_amount], [contribution_amount])
        SELECT pln.[plan_no],
               xpp.[customer_no],
               wrk.[worker_name],
               wrk.[worker_initials],
               wrk.[inactive],
               pln.[complete_by_dt],
               ISNULL(pln.[ask_amt],0.0),
               ISNULL(pln.[goal_amt],0.0),
               ISNULL(pln.[cont_amt],0.0)
        FROM [dbo].[T_PLAN] AS pln
             LEFT OUTER JOIN [dbo].[TX_CUST_PLAN] AS xpp ON xpp.[plan_no] = pln.[plan_no] AND xpp.[role_no] = 1
             INNER JOIN [dbo].[LV_CAMPAIGN_ACTIVITY_WORKERS] AS wrk ON wrk.[worker_customer_no] = xpp.[customer_no]
        WHERE pln.[complete_by_dt] BETWEEN @fiscal_start_dt AND @fiscal_end_dt
          AND pln.[ask_amt] > 0
        
    /*  Seed the #worker_activity table with each worker and their goals  */

        INSERT INTO [#worker_activity] ([worker_customer_no], [worker_name], [worker_initials], [worker_inactive], [goal_visits], [goal_hard_contacts], 
                                        [goal_soft_contacts], [goal_ask_amount], [goal_expected_amount], [goal_prim_sol_amount])
        SELECT [worker_customer_no],
               [worker_name],
               [worker_initials],
               [inactive],
               [goal_visits],
               [goal_hard_contacts],
               [goal_soft_contacts],
               [goal_ask_total],
               [goal_expected_total],
               [goal_prim_sol_rev_total]
        FROM [dbo].[LV_CAMPAIGN_ACTIVITY_WORKERS]
        WHERE [inactive] = 'N'

    /*  ***Visits*** */

        INSERT INTO [#worker_activity] ([worker_customer_no], [worker_name], [worker_initials], [worker_inactive], [actual_visits])
        SELECT [worker_customer_no],
               [worker_name],
               [worker_initials],
               [worker_inactive],
               COUNT([step_no])
        FROM [#step_raw_data]
        WHERE [completed_on_dt] BETWEEN @fiscal_start_dt AND @current_dt 
          AND is_visit = 'Y'
          AND [worker_inactive] = 'N'
        GROUP BY [worker_customer_no], [worker_name], [worker_initials], [worker_inactive]

    /*  ***Hard Contacts*** */

        INSERT INTO [#worker_activity] ([worker_customer_no], [worker_name], [worker_initials], [worker_inactive], [actual_hard_contacts])
        SELECT [worker_customer_no],
               [worker_name],
               [worker_initials],
               [worker_inactive], 
               COUNT([step_no])
        FROM [#step_raw_data]
        WHERE [completed_on_dt] BETWEEN @fiscal_start_dt AND @current_dt
          AND [step_type] IN (29, 30, 70, 71)
          AND [worker_inactive] = 'N'
        GROUP BY [worker_customer_no], [worker_name], [worker_initials], [worker_inactive]

    /*  ***Soft Contacts*** */

        INSERT INTO [#worker_activity] ([worker_customer_no], [worker_name], [worker_initials], [worker_inactive], [actual_soft_contacts])
        SELECT [worker_customer_no],
               [worker_name],
               [worker_initials], 
               [worker_inactive],
               COUNT([step_no])
        FROM [#step_raw_data]
        WHERE [completed_on_dt] BETWEEN @fiscal_start_dt AND @current_dt 
          AND [step_type] NOT IN (29, 30, 70, 71) 
          AND [step_type_name] LIKE 'contact_%'
          AND [worker_inactive] = 'N'
        GROUP BY [worker_customer_no], [worker_name], [worker_initials], [worker_inactive]

    /*  ***Ask Amount and Exptected Amount*** */
        
        INSERT INTO [#worker_activity] ([worker_customer_no], [worker_name], [worker_initials], [worker_inactive], [actual_ask_amount], [actual_expected_amount])
        SELECT [worker_customer_no],
               [worker_name],
               [worker_initials], 
               [worker_inactive],
               SUM(ISNULL([ask_amount],0)),
               SUM(ISNULL([goal_amount],0))
        FROM [#plan_data]
        WHERE [complete_by_dt] BETWEEN @fiscal_start_dt AND @fiscal_end_dt
          AND [worker_inactive] = 'N'
        GROUP BY [worker_customer_no], [worker_name], [worker_initials], [worker_inactive]

    /*  ***Primary Solicitor Amount*** */

        INSERT INTO [#worker_activity] ([worker_customer_no], [worker_name], [worker_initials], [worker_inactive], [actual_prim_sol_amount])
        SELECT con.[solicitor_number],
               wrk.[worker_name],
               wrk.[worker_initials],
               wrk.[inactive],
               SUM(ISNULL(con.[contribution_amount],0))
        FROM [dbo].[LV_CAMPAIGN_ACTIVITY_CONTRIBUTIONS] AS con
             INNER JOIN [dbo].[LV_CAMPAIGN_ACTIVITY_WORKERS] AS wrk ON wrk.[worker_customer_no] = con.[solicitor_number]
        WHERE con.[contribution_date] BETWEEN @fiscal_start_dt AND @fiscal_end_dt AND ISNULL(con.[solicitor_number],0) > 0
          AND wrk.[inactive] = 'N'
        GROUP BY con.[solicitor_number], wrk.[worker_name], wrk.[worker_initials], wrk.[inactive]

    FINISHED:

        /*  Aggregate everything into a final list, one row per worker  */

            SELECT  [worker_customer_no],
                    [worker_name],
                    [worker_initials],
                    [worker_inactive] AS [inactive],
                    SUM([goal_visits]) AS [goal_visits],
                    SUM([actual_visits]) AS [actual_visits],
                    CASE WHEN SUM([goal_visits]) = 0.0 THEN 0.0 ELSE (SUM([actual_visits]) / SUM([goal_visits])) END AS [percent_visits],
                    SUM([goal_hard_contacts]) AS [goal_hard_contacts],
                    SUM([actual_hard_contacts]) AS [actual_hard_contacts],
                    CASE WHEN SUM([goal_hard_contacts]) = 0.0 THEN 0.0 ELSE (SUM([actual_hard_contacts]) / SUM([goal_hard_contacts])) END AS [percent_hard_contacts],
                    SUM([goal_soft_contacts]) AS [goal_soft_contacts],
                    SUM([actual_soft_contacts]) AS [actual_soft_contacts],
                    CASE WHEN SUM([goal_soft_contacts]) = 0.0 THEN 0.0 ELSE (SUM([actual_soft_contacts]) / SUM([goal_soft_contacts])) END AS [percent_soft_contacts],
                    SUM([goal_ask_amount]) AS [goal_ask_amount],
                    SUM([actual_ask_amount]) AS [actual_ask_amount],
                    CASE WHEN SUM([goal_ask_amount]) = 0.0 THEN 0.0 ELSE (SUM([actual_ask_amount]) / SUM([goal_ask_amount])) END AS [percent_ask_amount],
                    SUM([goal_expected_amount]) AS [goal_expected_amount],
                    SUM([actual_expected_amount]) AS [actual_expected_amount],
                    CASE WHEN SUM([goal_expected_amount]) = 0.0 THEN 0.0 ELSE (SUM([actual_expected_amount]) / SUM([goal_expected_amount])) END AS [percent_expectred_amount],
                    SUM([goal_prim_sol_amount]) AS [goal_prim_sol_amount],
                    SUM([actual_prim_sol_amount]) AS [actual_prim_sol_amount],
                    CASE WHEN SUM([goal_prim_sol_amount]) = 0.0 THEN 0.0 ELSE (SUM([actual_prim_sol_amount]) / SUM([goal_prim_sol_amount])) END AS [percent_prim_sol_amount]
            FROM [#worker_activity]  --WHERE worker_initials = 'LLU'
            GROUP BY [worker_customer_no], [worker_name], [worker_initials], [worker_inactive]
            ORDER BY worker_name
                
    DONE:

        --IF OBJECT_ID('tempdb..#worker_activity') IS NOT NULL DROP TABLE #worker_activity
        --IF OBJECT_ID('tempdb..#plan_data') IS NOT NULL DROP TABLE #plan_data
        --IF OBJECT_ID('tempdb..#step_raw_data') IS NOT NULL DROP TABLE #step_raw_data

--END
--GO

--EXECUTE dbo.LRP_ADV_CAMPAIGN_ACTIVITY_STAFF_GOALS @fiscal_year = 2020



SELECT [worker_customer_no],                --DATA
       [worker_name]                        --DISPLAY
FROM  [dbo].[LV_CAMPAIGN_ACTIVITY_WORKERS]  --TABLE
WHERE [login_inactive] = 'N'                --WHERE CLAUSE
ORDER BY [worker_sort]                      --SORT

