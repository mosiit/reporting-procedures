USE [impresario]
GO

--IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LP_RPT_ATTENDANCE_BY_VENUE]') AND type in (N'P', N'PC'))
--    DROP PROCEDURE [dbo].[LP_RPT_ATTENDANCE_BY_VENUE]
--GO


SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LP_RPT_ATTENDANCE_BY_VENUE]
       @report_start_dt DATETIME,
       @report_end_dt DATETIME,
       @title_names VARCHAR(4000)
AS BEGIN

    /*  Procedure Variables  */

    DECLARE @title_list TABLE ([title_name] VARCHAR(30))


    /* Check Parameters - If dates are null, run for yesterday */
            
        SELECT @report_start_dt = ISNULL(@report_start_dt,DATEADD(DAY,-1,GETDATE())),
               @report_end_dt = ISNULL(@report_end_dt,DATEADD(DAY,-1,GETDATE()))

        SELECT @title_names = ISNULL(@title_names,'')
        IF @title_names = 'All' OR @title_names = 'All Titles' SELECT @title_names = ''
        SELECT @title_names = REPLACE(@title_names,'"','')


  /*  Parse Out Users and Locations */

        IF @title_names <> ''
            INSERT INTO @title_list SELECT LTRIM(RTRIM(Item)) FROM [dbo].[LFT_SPLIT_DILEMETED_STRING] (@title_names,',')

    /* Temporary Table to hold the data  */


        IF OBJECT_ID('tempdb..#attendance_table') IS NOT NULL DROP TABLE [#attendance_table]

        CREATE TABLE [#attendance_table] ([attendance_type] varchar(50), [performance_type] varchar(50), [title_name] VARCHAR(30), [production_name] VARCHAR(50), [production_name_short] VARCHAR(50), 
                                          [production_name_long] VARCHAR(150), [comp_code_name] VARCHAR(50), [order_payment_status] VARCHAR(50), [sale_total] INT, [scan_admission_total] INT,
                                          [due_amt] DECIMAL(18,2), [paid_amt] DECIMAL(18,2), [p_report_start_dt] DATETIME, [p_report_end_date] DATETIME, [p_include_partial_paid] CHAR(1))

        CREATE CLUSTERED INDEX [ix_attendance_table_title_name] ON [#attendance_table] ([title_name] ASC) ON [PRIMARY]


    /*  Get original data set by running the basic attendance report  */

        INSERT INTO [#attendance_table] ([attendance_type],[performance_type],[title_name],[production_name],[production_name_short],[production_name_long],[comp_code_name],[order_payment_status],[sale_total],
                                         [scan_admission_total],[due_amt],[paid_amt],[p_report_start_dt],[p_report_end_date],[p_include_partial_paid])
        EXECUTE [dbo].[LP_RPT_ATTENDANCE] @report_start_dt, @report_end_dt, 'Y'

    
    /*  If specific venues passed to the report, delete all but those venues  */

        IF EXISTS (SELECT * FROM @title_list)
            DELETE FROM [#attendance_table] WHERE [title_name] NOT IN (SELECT [title_name] FROM @title_list)


    FINISHED:

        IF NOT EXISTS (SELECT * FROM [#attendance_table])
            INSERT INTO [#attendance_table] ([attendance_type],[performance_type],[title_name],[production_name],[production_name_short],[production_name_long],[comp_code_name],[order_payment_status],[sale_total],
                                             [scan_admission_total],[due_amt],[paid_amt],[p_report_start_dt],[p_report_end_date],[p_include_partial_paid])
            VALUES ('No rows found.','','','','','','','',0,0,0.00,0.00,@report_start_dt, @report_end_dt, 'Y')                



        SELECT [attendance_type],[performance_type],[title_name],[production_name],[production_name_short],[production_name_long],[comp_code_name],[order_payment_status],[sale_total],
               [scan_admission_total],[due_amt],[paid_amt],[p_report_start_dt],[p_report_end_date],[p_include_partial_paid] 
        FROM [#attendance_table]

    DONE:

        --IF OBJECT_ID('tempdb..#attendance_table') IS NOT NULL DROP TABLE [#attendance_table]

END
GO


GRANT EXECUTE ON [dbo].[LP_RPT_ATTENDANCE_BY_VENUE] TO ImpUsers
GO


EXECUTE [dbo].[LP_RPT_ATTENDANCE_BY_VENUE] '9-1-2017', '9-30-2017', '"Mugar Omni Theater"'




--SELECT [description] FROM [dbo].[T_INVENTORY] WHERE type = 'T' ORDER BY description

--SELECT DISTINCT [title_name] FROM dbo.LV_ORDER_DETAIL WHERE performance_dt BETWEEN '9-1-2017' AND '9-30-2017 23:59:59' ORDER BY [title_name]

