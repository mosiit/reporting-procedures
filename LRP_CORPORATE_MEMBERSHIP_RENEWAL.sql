USE [impresario]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Debbie Jacob
-- Create date: September 9, 2016
-- Description:	Corporate Membership Renewal Report 
-- =============================================
ALTER PROCEDURE [dbo].[LRP_CORPORATE_MEMBERSHIP_RENEWAL]
	@period INT,
	@Dt datetime  
AS
BEGIN

SET NOCOUNT ON;

DECLARE @curr_start_dt DATETIME,
	@curr_end_dt DATETIME,
	@prev_start_dt DATETIME,
	@prev_end_dt DATETIME, 
	@fyear INT 

-- NOTE, the only options are 1 and 3. But, if a value in not entered. I don't want the query to fail. So, the default will be Months. 
IF @period = 1 -- Fiscal Year
BEGIN
	SELECT @fyear = fyear
	FROM dbo.TR_BATCH_Period
	WHERE @Dt BETWEEN start_dt AND end_dt	

	SELECT @curr_start_dt = MIN(start_dt), @curr_end_dt = MAX(end_dt)
	FROM TR_BATCH_PERIOD 
	WHERE inactive = 'n' AND fyear = @fyear
END 
ELSE -- IF @period = 3 -- Months 
BEGIN
	SELECT @curr_start_dt = DATEADD(month, DATEDIFF(month, 0, @Dt), 0)
	SELECT @curr_end_dt = DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@Dt)+1,0))
END 

SELECT @prev_start_dt = DATEADD(yy, -1, @curr_start_dt), @prev_end_dt = DATEADD(yy, -1, @curr_end_dt)

--SELECT @fyear, @dt, @curr_start_dt, @curr_end_dt, @prev_start_dt, @prev_end_dt

SELECT 
CASE mem.NRR_status
	WHEN 'NE' THEN 'New'
	WHEN 'RN' THEN 'Renewed ' + 
		CASE mt1.[description]
			WHEN 'Same' THEN ''
			ELSE mt1.[description]
	END
	WHEN 'RI' THEN 'Reinstated ' +
		CASE mt1.[description]
			WHEN 'Same' THEN ''
			ELSE mt1.[description]
		END
END AS memb_status,
CASE 
	WHEN mem.init_dt BETWEEN @curr_start_dt AND @curr_end_dt THEN @curr_end_dt
	ELSE @prev_end_dt
END AS end_dt, COUNT(*) AS cnt
FROM dbo.TX_CUST_MEMBERSHIP mem
	INNER JOIN T_MEMB_LEVEL ml1 
		ON mem.memb_org_no = ml1.memb_org_no AND mem.memb_level = ml1.memb_level
	INNER JOIN dbo.TR_CONSTITUENCY trc
		ON ml1.constituency = trc.id
		AND trc.description = 'Corporate Member'
	INNER JOIN TR_MEMB_TREND mt1 
		ON mem.memb_trend = mt1.id
WHERE (mem.init_dt BETWEEN @curr_start_dt AND @curr_end_dt) OR (mem.init_dt BETWEEN @prev_start_dt AND @prev_end_dt)
GROUP BY 
	CASE mem.NRR_status
		WHEN 'NE' THEN 'New'
		WHEN 'RN' THEN 'Renewed ' + 
			CASE mt1.[description]
				WHEN 'Same' THEN ''
				ELSE mt1.[description]
			END
		WHEN 'RI' THEN 'Reinstated ' +
			CASE mt1.[description]
				WHEN 'Same' THEN ''
				ELSE mt1.[description]
			END
	END ,CASE 
		WHEN mem.init_dt BETWEEN @curr_start_dt AND @curr_end_dt THEN @curr_end_dt
		ELSE @prev_end_dt
	END
ORDER BY memb_status,end_dt

END 
