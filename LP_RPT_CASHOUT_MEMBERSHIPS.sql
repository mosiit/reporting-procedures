USE impresario
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LP_RPT_CASHOUT_MEMBERSHIPS]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LP_RPT_CASHOUT_MEMBERSHIPS]
GO

CREATE PROCEDURE [dbo].[LP_RPT_CASHOUT_MEMBERSHIPS]
        @rpt_dt datetime,
        @rpt_operator varchar(8)
AS BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

    DECLARE @rpt_date char(10), @rpt_message varchar(100), @report_start datetime
    DECLARE @membership_table table ([membership_date] char(10), [membership_type] varchar(30), [total_memberships] int, [total_with_one_step] int, [membership_operator] varchar(8),
                                     [operator_first] varchar(50), [operator_last] varchar(50), [operator_location] varchar(50), [elapsed_time] int, [rpt_message] varchar(100))

    SELECT @report_start = getdate()

    SELECT @rpt_message = '', @rpt_date = ''

    SELECT @rpt_operator = IsNull(@rpt_operator,'')
    
    IF @rpt_dt is null
        SELECT @rpt_message = 'error: invalid date passed to report.'
    ELSE BEGIN

        SELECT @rpt_date = convert(char(10),@rpt_dt,111)
    
        IF @rpt_operator = '' or not exists (SELECT * FROM T_METUSER WHERE [userid] = @rpt_operator)
            SELECT @rpt_message = 'error: invalid operator id passed to report (' + @rpt_operator + ').'
        ELSE IF not exists (SELECT * FROM [dbo].[LV_RPT_CASHOUT_membershipS] WHERE membership_date = @rpt_date and membership_operator = @rpt_operator)
            SELECT @rpt_message = 'No memberships found for ' + @rpt_operator + ' on ' + @rpt_date +  '.'

    END
        
    IF @rpt_message <> '' GOTO FINISHED

    INSERT INTO @membership_table 
    SELECT [membership_date], [membership_type], count(*), sum([one_step_count]), [membership_operator], [membership_operator_first_name], [membership_operator_last_name], [membership_operator_location], 0, @rpt_message
    FROM [LV_RPT_CASHOUT_MEMBERSHIPS] WHERE membership_date = @rpt_date and membership_operator = @rpt_operator
    GROUP BY [membership_date], [membership_type], [membership_operator], [membership_operator_first_name], [membership_operator_last_name], [membership_operator_location]

        /********************************************************************************/
        /*    IF ANY ADDITIONAL DATA MANIPULATION NEEDS TO BE ADDED, IT WILL GO HERE    */
        /********************************************************************************/

    FINISHED:

        IF not exists (SELECT * FROM @membership_table) BEGIN
            IF @rpt_message = '' SELECT @rpt_message = 'No memberships found for ' + @rpt_operator + ' on ' + @rpt_date +  '.'
            INSERT INTO @membership_table VALUES (@rpt_date, '', 0, 0, @rpt_operator, '', '', '', 0, @rpt_message)
        END

        UPDATE @membership_table SET [elapsed_time] = datediff(second,@report_start,getdate())

        SELECT [membership_date], [membership_operator], [operator_first], [operator_last], [operator_location], [membership_type], [total_memberships], [total_with_one_step], [elapsed_time], [rpt_message] 
        FROM @membership_table

END
GO

GRANT EXECUTE ON [dbo].[LP_RPT_CASHOUT_MEMBERSHIPS] TO impusers
GO


