USE [impresario]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[LRP_ADV_WKLY_GIFT_NOTIFICATION]
(
	@numOfDays INT 
)
AS

SET NOCOUNT ON  
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED 

-- Code is taken from LRP_GIFT_NOTIFICATION_OUTSIDE_DMS
-- This calls AP_NEW_CONT_REPORT which needs a start and an end date.
-- @end_dt is today and make the @start_dt @numOfDays before 

SET @numOfDays = -1*@numOfDays

DECLARE @start_dt DATETIME,
	@end_dt DATETIME

SELECT @end_dt = DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0) + ' 23:59:59.99'
SELECT @start_dt = DATEDIFF(dd, 0, DATEADD(d, @numOfDays, @end_dt))

DECLARE @work1 TABLE 
(
	customer_no INT,
	ref_no INT,
	cust_name VARCHAR(160),
	cont_dt DATETIME,
	cont_type CHAR(1),
	cont_amt MONEY,
	cont_fund VARCHAR(30),
	designation VARCHAR(30) NULL,
	plan_primary_worker CHAR(8) NULL,
	cont_worker CHAR(8) NULL,
	state VARCHAR(20) NULL,
	memb_level CHAR(3) NULL,
	memb_trend VARCHAR(30) NULL,
	nrr_status CHAR(2) NULL,
	letter_desc VARCHAR(30) NULL,
	expr_dt DATETIME NULL,
	sort_name VARCHAR(55),
	initiator_no INT NULL,
	initiator_name VARCHAR(160) NULL,
	creditee_no INT NULL,
	creditee_name VARCHAR(160) NULL,
	credit_amt MONEY,
	credit_type_desc VARCHAR(30)
)

INSERT INTO @work1
EXEC dbo.AP_NEW_CONT_REPORT 
	@start_dt = @start_dt, 
	@end_dt = @end_dt, 
	@fund_str = NULL, 
	@type_str = NULL, 
	@list_no = 0, 
	@list_includes_owner_ind = 'N',
	@list_includes_initiator_ind = 'N',
	@list_includes_creditee_ind = 'N',
	@creditee_str = '1,5,8,11,12,14,15'

SELECT 
	w.customer_no,
	w.ref_no,
	w.cust_name,
	w.cont_dt,
	w.cont_type,
	w.cont_amt,
	w.cont_fund,
	w.designation,
	w.plan_primary_worker,
	w.cont_worker,
	w.state,
	w.memb_level,
	w.memb_trend,
	w.nrr_status,
	w.letter_desc,
	w.expr_dt,
	w.sort_name,
	w.initiator_no,
	w.initiator_name,
	w.creditee_no,
	w.creditee_name,
	w.credit_amt,
	w.credit_type_desc
FROM @work1 w
	INNER JOIN V_CUSTOMER_WITH_PRIMARY_GROUP vc
		ON w.customer_no = vc.customer_no
	INNER JOIN VS_CONTRIBUTION_WITH_INITIATOR a1
		ON vc.expanded_customer_no = a1.customer_no
		AND w.ref_no = a1.ref_no
WHERE 
	a1.cont_amt >= 25.00
	AND w.letter_desc <> 'Adjustment/Reentry' -- omitting Reentires.
	AND vc.customer_no <> 2653093 -- Stewardship Soft Credit
	AND ISNULL(vc.inactive, 1) = 1
ORDER BY w.cust_name

