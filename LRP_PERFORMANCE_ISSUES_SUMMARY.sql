USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LRP_PERFORMANCE_ISSUES_SUMMARY]    Script Date: 10/17/2019 9:08:39 AM ******/
DROP PROCEDURE [dbo].[LRP_PERFORMANCE_ISSUES_SUMMARY]
GO

/****** Object:  StoredProcedure [dbo].[LRP_PERFORMANCE_ISSUES_SUMMARY]    Script Date: 10/17/2019 9:08:39 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






CREATE PROCEDURE [dbo].[LRP_PERFORMANCE_ISSUES_SUMMARY]
	@TimePeriodMins INT,
	@ExcludeScreenLookups CHAR(1) = 'Y'
AS

SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

-- If no value passed in for past time view, then assume everything for the last 30 days
IF @TimePeriodMins = 0
	SELECT @TimePeriodMins = 43200;

CREATE TABLE #ExtResults (	StartTime DATETIME,
							EndTime DATETIME,
							Duration VARCHAR(15),
							ExtractNbr INT,
							ExtractDescription VARCHAR(50),
							DLNbr INT,
							RunStatus VARCHAR(20),
							TimeSlot VARCHAR(10),
							LastUpdatedBy VARCHAR(10));

CREATE TABLE #RptResults (	[Full Report Path] VARCHAR(MAX),
							[Report Name] VARCHAR(MAX),
							[Report Parameters] VARCHAR(MAX),
							Duration VARCHAR(30),
							RptType VARCHAR(60),
							RunStatus VARCHAR(100),
							StartTime DATETIME,
							EndTime DATETIME,
							LastRunBy VARCHAR(60),
							CreatedBy VARCHAR(60),
							Scheduled CHAR(1),
							RecordCount VARCHAR(10))

CREATE TABLE #DeadLockResults (	[DeadlockID] [bigint] NULL,
								[TransactionTime] [datetime] NULL,
								[DeadlockGraph] [xml] NULL,
								[DeadlockObjects] [nvarchar](max) NULL,
								[Victim] [int] NOT NULL,
								[SPID] [int] NULL,
								[ProcedureName] [varchar](200) NULL,
								[LockMode] [char](1) NULL,
								[Code] [varchar](8000) NULL,
								[ClientApp] [nvarchar](245) NULL,
								[HostName] [varchar](20) NULL,
								[LoginName] [varchar](20) NULL,
								[InputBuffer] [varchar](8000) NULL)

CREATE TABLE #AllResults	(
							Tool					VARCHAR(30),
							[Start Time]			DATETIME,
							[End Time]				DATETIME,
							Duration				VARCHAR(30),
							[Description]			VARCHAR(MAX),
							[Parameters]			VARCHAR(MAX),
							[Run Status]			VARCHAR(100),
							[Last Run By]			VARCHAR(60),
							[Created By]			VARCHAR(60),
							Scheduled				CHAR(1)
							)

INSERT INTO #ExtResults
	EXEC [impresario].[dbo].[LRP_EXTRACTION_SUMMARY] @TimePeriodMins = @TimePeriodMins;

INSERT INTO #RptResults
	EXEC [impresario].[dbo].[LRP_REPORT_SUMMARY] @TimePeriodMins = @TimePeriodMins;

INSERT INTO #DeadLockResults 
	EXEC [impresario].[dbo].[LRP_DEADLOCK_SUMMARY] @TimePeriodMins = @TimePeriodMins;

INSERT INTO #AllResults
	SELECT	'Extract' AS 'Tool',
			StartTime,
			EndTime,
			Duration,
			ExtractDescription AS 'Description',
			'' AS [Parameters],
			RunStatus,
			LastUpdatedBy,
			'' AS 'CreatedBy',
			'' AS 'Scheduled'
	FROM	#ExtResults

	UNION

	SELECT	RptType AS 'Tool',
			StartTime,
			EndTime,
			Duration,
			--REPLACE(REPLACE(REPLACE(REPLACE([Full Report Path], '/Tessitura/CustomReports/', ''), '/Tessitura/ScheduledReports/', ''), '/Tessitura/Reports/', ''), '/', ' > ') AS 'Description', -- /Tessitura/CustomReports/Advancement/Pipeline
			--REPLACE([Full Report Path], '/Tessitura/', '') AS 'Description', -- /Tessitura/CustomReports/Advancement/Pipeline
			CASE 
				WHEN RptType = 'Screen' AND PATINDEX('%,%', [Report Parameters]) > 0 THEN REPLACE(REPLACE([Full Report Path], '/Tessitura/CustomReports/ConstituentCustomScreens/', '') + ' (' + SUBSTRING([Report Parameters], 1, PATINDEX('%,%', [Report Parameters]) - 1) + ')', 'customer_no=', '#')
				WHEN RptType = 'Screen' AND PATINDEX('%,%', [Report Parameters]) = 0 THEN REPLACE(REPLACE([Full Report Path], '/Tessitura/CustomReports/ConstituentCustomScreens/', '') + ' (' + [Report Parameters] + ')', 'customer_no=', '#')
				ELSE [Report Name] 
			END AS 'Description',
			[Report Parameters] AS 'Parameters',
			RunStatus,
			LastRunBy,
			CreatedBy,
			Scheduled
	FROM   #RptResults
	--LEFT JOIN [TR_CUSTOM_TAB] ct
	--ON		[Report Name] = REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(ct.d_object, '%/Tessitura/CustomReports/ConstituentCustomScreens/', ''), '%/Tessitura/CustomReports/EventsCustomScreens/', ''), '%/Tessitura/CustomReports/CampaignCustomScreens/', ''), '&customer_no=<<key>>', ''), '&campaign_no=<<key>>', ''), '&UserGroup=<<ug>>&UserId=<<user>>', '')
	
	UNION

	SELECT	'Deadlock' AS 'Tool',
			[TransactionTime],
			[TransactionTime],
			'',
			[DeadlockObjects] AS 'Description',
			[ProcedureName] AS 'Parameters',
			CASE WHEN [Victim] = 1 THEN 'Victim' ELSE 'Not Victim' END,
			ISNULL([LoginName], ''),
			[ClientApp],
			''
	FROM   #DeadLockResults

IF @ExcludeScreenLookups = 'Y'
	SELECT	Tool,
			[Start Time],
			[End Time],
			-- 20190430, H. Sheridan - return duration as a number, not a string
			CONVERT(INT, Duration) AS 'Duration (Secs)',
			[Description],
			[Parameters],
			[Run Status],
			ISNULL([Last Run By], '') AS 'User',
			ISNULL([Created By], '') AS 'Group',
			Scheduled
	FROM	#AllResults
	WHERE	Tool <> 'Screen'
	ORDER BY [Start Time] DESC;
ELSE
	SELECT	Tool,
			[Start Time],
			[End Time],
			-- 20190430, H. Sheridan - return duration as a number, not a string
			CONVERT(INT, Duration) AS 'Duration (Secs)',
			[Description],
			[Parameters],
			[Run Status],
			ISNULL([Last Run By], '') AS 'User',
			ISNULL([Created By], '') AS 'Group',
			Scheduled
	FROM	#AllResults
	ORDER BY [Start Time] DESC;

DROP TABLE #ExtResults
DROP TABLE #DeadLockResults
DROP TABLE #RptResults
DROP TABLE #AllResults




GO


