USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LP_RPT_ATTENDANCE_VISIT_COUNT]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LP_RPT_ATTENDANCE_VISIT_COUNT]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LP_RPT_ATTENDANCE_VISIT_COUNT]
        @report_start_date datetime,
        @report_end_date datetime,
        @include_partial_paid char(1) = 'Y'
AS BEGIN

    SET NOCOUNT ON;
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

    /* 6/19/2019:  All the commented code below moved into a procedure called LP_DAILY_VISIT_COUNT with the added ability to include
                   or not include collected passes and negative counts in the visit count.  The new procedure is now called from here 
                   without including collected passes or negative counts.  The job that updates the data mart uses the same procedure 
                   only including collected passes and negative counts.  */

     IF OBJECT_ID('tempdb..#visit_count_data') is not null DROP TABLE [#visit_count_data]

     CREATE TABLE [#visit_count_data] ([attendance_type] VARCHAR (30) NOT NULL DEFAULT (''), 
                                       [transaction_count] INT NOT NULL DEFAULT (0), 
                                       [visit_count] INT NOT NULL DEFAULT (0), 
                                       [visit_count_scan] INT NOT NULL DEFAULT (0))

                                      
      INSERT INTO [#visit_count_data] ([attendance_type], [transaction_count], [visit_count], [visit_count_scan])
          EXECUTE [dbo].[LP_DAILY_VISIT_COUNT]
                  @report_start_date = @report_start_date, 
                  @report_end_date = @report_end_date, 
                  @include_collected_passes = 'Y',
                  @include_negative_counts = 'N'

        FINISHED:

             SELECT [attendance_type], 
                    [transaction_count], 
                    [visit_count],
                    [visit_count_scan],
                    @report_start_date as [p_report_start_date], 
                    @report_end_date as [p_report_end_date], 
                    @include_partial_paid as [p_include_partial_paid]  
            FROM [#visit_count_data] 

    DONE:

        IF OBJECT_ID('tempdb..#visit_count_data') is not null DROP TABLE [#visit_count_data]

             
END
GO

GRANT EXECUTE ON [dbo].[LP_RPT_ATTENDANCE_VISIT_COUNT] to impusers
GO


--EXECUTE [dbo].[LP_RPT_ATTENDANCE_VISIT_COUNT] @report_start_date = '7-5-2019', @report_end_date = '7-5-2019', @include_partial_paid = 'Y'
--EXECUTE [dbo].[LP_RPT_ATTENDANCE_VISIT_COUNT] '9-2-2019', '9-8-2019', 'Y'



--/*  Procedure Parameters  */

    --    DECLARE @start_date char(10), @end_date char(10)
    
    --    DECLARE @mos_buyout_no INT = (SELECT ISNULL([id],0) FROM [dbo].[TR_MOS] WHERE [description] = 'Buyouts')

    --/*  Check Parameters  */

    --    SELECT @start_date = convert(char(10),@report_start_date,111), @end_date = convert(char(10),@report_end_date,111)
        
    --/* Make sure the dates passed to the procedure include the entire day (from midnight on the start date to 11:59 PM on the end date)  */

    --    SELECT @report_start_date = convert(char(10),@report_start_date,111) + ' 00:00:00',
    --           @report_end_date = convert(char(10),@report_end_date,111) + ' 23:59:59'

    --/*  Create temp tables needed for this report  */
        
    --    IF OBJECT_ID('tempdb..#visit_count_raw_data') is not null DROP TABLE [#visit_count_raw_data]
    --    IF OBJECT_ID('tempdb..#visit_count_final_data') is not null DROP TABLE [#visit_count_final_data]

    --    CREATE TABLE [#visit_count_raw_data] ([attend_type] varchar(30), [order_no] int, [perf_no] int, [zone_no] int, [prod_name] varchar(30), [sale_total] int, [scan_admission_total] int)

        
    --/*  Retrieve the Visit Count data for ticketed events from the database  */
            
    --    INSERT INTO [#visit_count_raw_data]
    --    SELECT 'ticketed', [order_no], [perf_no], [zone_no], [production_name], sum([sale_total]), sum([scan_admission_total]) 
    --    FROM [dbo].[LT_HISTORY_TICKET]
    --    WHERE [performance_date] between @start_date and @end_date and 
    --            ([title_name] in (SELECT [title_name] FROM [dbo].[LV_SS_PRODUCTION_ELEMENTS_TITLE] WHERE [visit_count_title] = 'Y')
    --          OR [production_name] IN (SELECT [production_name] FROM [dbo].[LV_PRODUCTION_ELEMENTS_PRODUCTION] WHERE [visit_count_production] = 'Y'))
    --    GROUP BY [order_no], [perf_no], [zone_no], [production_name] ORDER BY [order_no]

    --    DELETE FROM [#visit_count_raw_data] WHERE prod_name like '%Buyout%'

    --/*  Added 2/7/2018:  Hard Coded exception added for the Exhibit Halls Special production, which is in the 
    --                     Exhibit Halls title but should not be counted in with the visit count.  */
         
    --     DELETE FROM [#visit_count_raw_data] WHERE [prod_name] = 'Exhibit Halls Special'


    --    --SELECT * FROM [#visit_count_raw_data] WHERE prod_name LIKE '%Special%'


    --/*  Added 10/26/2017:  Deletes orders from the visit count that were created in the buyouts mode of sale regardless of what the production name is.
    --                       @mos_buyout_no is defined above and gets the mode of sale number from the TR_MOS table.  
    --                       It must find and if number > 0 for this to happen.  */

    --        DELETE FROM [#visit_count_raw_data]
    --        WHERE [order_no] IN (SELECT [order_no] FROM [dbo].[T_ORDER] WHERE [order_no] IN (SELECT [order_no] FROM [#visit_count_raw_data]) AND [MOS] = @mos_buyout_no)

    --    INSERT INTO [#visit_count_final_data] SELECT 'Ticketed Events', [order_no], max([sale_total]), max([scan_admission_total]) FROM [#visit_count_raw_data] GROUP By [Order_no]

    --/*  Retrieve the Visit Count data for gate scans from the database  */

    --    INSERT INTO [#visit_count_final_data] 
    --    SELECT 'Gate Scan', [sale_total], [scan_admission_total], 0 FROM [dbo].[LT_HISTORY_TICKET]
    --    WHERE [performance_date] between @start_date and @end_date and [title_name] = 'Gate Scan'
              
             
    ----/*  Retrieve the Visit Count data for show and go scans from the database  */

    ----    INSERT INTO [#visit_count_final_data] 
    ----    SELECT 'Show and Go', [sale_total], [scan_admission_total], 0 FROM [dbo].[LT_HISTORY_TICKET] 
    ----    WHERE [performance_date] between @start_date and @end_date and [title_name] = 'Show and Go'
    ----      AND [customer_no] NOT IN (SELECT [customer_no] FROM dbo.TX_CUST_SAL WHERE esal2_desc LIKE '%NO VISIT COUNT%')

    --/*  Retrieve the Visit Count data for show and go scans from the database  */

    --    INSERT INTO [#visit_count_final_data] 
    --    SELECT [title_name], [sale_total], [scan_admission_total], 0 FROM [dbo].[LT_HISTORY_TICKET] 
    --    WHERE [performance_date] between @start_date and @end_date and [title_name] = 'Show and Go'  --IN ('Show and Collected','Show and Go') --DO NOT WANT COLLECTED ON REPORTS
    --      AND [customer_no] NOT IN (SELECT [customer_no] FROM dbo.TX_CUST_SAL WHERE esal2_desc LIKE '%NO VISIT COUNT%')

    --    UPDATE [#visit_count_final_data] SET [attendance_type] = 'Membership Gate Scan' WHERE [attendance_type] = 'Gate Scan'
         
    --DONE:

    --   /* Select aggregated date into the report  */

    --    SELECT [attendance_type], count([order_no]) as 'transaction_count', sum([sale_total]) as 'visit_count', sum([scan_admission_total]) as 'visit_count_scan', 
    --           @report_start_date as 'p_report_start_date', @report_end_date as 'p_report_end_date', @include_partial_paid as 'p_include_partial_paid'  
    --    FROM [#visit_count_final_data] GROUP BY [attendance_type] ORDER BY [attendance_type]

    --CLEAN_UP:

    --    /*  Not really necessary, but the programmer in me wants to clean up  */

    --    IF OBJECT_ID('tempdb..#visit_count_raw_data') is not null DROP TABLE [#visit_count_raw_data]
    --    IF OBJECT_ID('tempdb..#visit_count_final_data') is not null DROP TABLE [#visit_count_final_data]