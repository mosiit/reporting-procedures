USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_ADV_WKLY_STAGE_MOVEMENT]') AND type IN (N'P', N'PC'))
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_ADV_WKLY_STAGE_MOVEMENT] AS'
GO

EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LRP_ADV_WKLY_STAGE_MOVEMENT] TO [impusers], [tessitura_app]'
GO

ALTER PROCEDURE [dbo].[LRP_ADV_WKLY_STAGE_MOVEMENT]
    	@numOfDays INT = 15,
        @workers_str VARCHAR(4000)
AS BEGIN

    SET NOCOUNT ON;
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

    /*  Procedure Variables  */

        DECLARE @start_dt DATETIME = NULL, @end_dt DATETIME = NULL
    
        DECLARE @worker_list TABLE ([worker_no] INT NOT NULL DEFAULT (0))
        DECLARE	@camp_categories TABLE ([id] INT NOT NULL DEFAULT (0))

    /*  Check Parameters  */
    
        SELECT @numOfDays = ISNULL(@numOfDays, 7)

        SET @numOfDays = -1 * @numOfDays
        
        SELECT @end_dt = DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0) + ' 23:59:59.99'
        SELECT @start_dt = DATEDIFF(dd, 0, DATEADD(d, @numOfDays, @end_dt))

        IF ISNULL(@workers_str,'') = ''
            INSERT INTO @worker_list ([worker_no])
            SELECT ISNULL([worker_customer_no], 0)
            FROM [dbo].[LV_CAMPAIGN_ACTIVITY_WORKERS] 
            WHERE [inactive] = 'N'
              AND ISNULL([worker_customer_no], 0) > 0
        ELSE
            INSERT INTO @worker_list ([worker_no])
            SELECT ISNULL(TRY_CAST([Element] AS INTEGER),0) 
            FROM [dbo].[FT_SPLIT_LIST](REPLACE(@workers_str,'"',''),',')
            WHERE ISNULL(TRY_CAST([Element] AS INTEGER),0) > 0;

    /*  Get Data  */

        -- CTE code taken from VS_PLAN_WITH_PRIMARY_WORKER
        WITH [CTE_PRIMARYWORKER] ([plan_no], [primaryWorker], [primaryWorkerNo], [primaryWorkerTiny])
        AS (SELECT cp.[plan_no], 
		           dn.[display_name], 
		           dn.[customer_no], 
		           dn.[display_name_tiny] 
	        FROM [dbo].[TX_CUST_PLAN] cp
                 INNER JOIN @worker_list AS lis ON lis.[worker_no] = cp.[customer_no]
	             JOIN [dbo].[FT_CONSTITUENT_DISPLAY_NAME]() dn ON cp.[customer_no] = dn.[customer_no]
	        WHERE cp.[primary_ind] = 'Y')
        SELECT  p.[customer_no], 
                cust.[display_name], 
                cust.[sort_name], 
                cam.[description] AS [campaign], 
                cdes.[description] AS [designation],
        	    p.[ask_amt], 
	            COALESCE(psold.[description], cold.[description]) AS [old_value_desc], 
	            COALESCE(psnew.[description], cnew.[description]) AS [new_value_desc],
                wkr.[primaryWorkerNo],
	            wkr.[primaryWorker],
	            s.[step_dt]
            FROM [dbo].[T_PLAN] AS p
                 INNER JOIN [dbo].[t_step] AS s ON s.[plan_no] = p.[plan_no]
                 INNER JOIN [dbo].[T_CAMPAIGN] AS cam ON cam.[campaign_no] = p.[campaign_no]
                 --INNER JOIN @camp_categories ccat ON ccat.[id] = cam.[category]
                 INNER JOIN [dbo].[FT_CONSTITUENT_DISPLAY_NAME]() AS cust ON p.[customer_no] = cust.[customer_no]
                 LEFT OUTER JOIN [dbo].[TR_CONT_DESIGNATION] AS cdes ON cdes.[id] = p.[cont_designation]
                 INNER JOIN [CTE_PRIMARYWORKER] AS wkr ON wkr.[plan_no] = p.[plan_no]
                 LEFT OUTER JOIN [dbo].[TR_PLAN_STATUS] AS psold ON s.[old_value] = psold.[id] AND s.[step_type] = -1
                 LEFT OUTER JOIN [dbo].[TR_PLAN_STATUS] AS psnew ON s.[new_value] = psnew.[id] AND s.[step_type] = -1
                 LEFT OUTER JOIN [dbo].[T_CAMPAIGN] AS cold ON s.[old_value] = cold.[campaign_no] AND s.[step_type] = -2
                 LEFT OUTER JOIN [dbo].[T_CAMPAIGN] AS cnew ON s.[new_value] = cnew.[campaign_no] AND s.[step_type] = -2
            WHERE p.[type] = 7 -- Advancement Pipeline
              AND s.[step_dt] BETWEEN @start_dt AND @end_dt
	          AND s.[step_type] IN (-1, -2) -- Status Change, Campaign Change

    DONE:

END
GO


EXECUTE [dbo].[LRP_ADV_WKLY_STAGE_MOVEMENT] @numOfDays = 15, @workers_str = ''
--EXECUTE [dbo].[LRP_ADV_WKLY_STAGE_MOVEMENT] @numOfDays = 15, @workers_str = '4009961,817730,672463'

