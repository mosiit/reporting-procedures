USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- CHART OF ACCOUNTS Exception reporting on T_GL_ACCOUNT
-- DSJ 05/24/2016
-- MES 01/20/2021 Updated per SCTASK0001778
--                Changed code to update each exception individually rather than in one big case statement
--                This will make it much easier to add and/or remove exceptions from the report when needed.
ALTER PROCEDURE [dbo].[LRP_EXCEPTION_COA_T_GL_ACCOUNT]
AS BEGIN

    /*  Procedure Variables  */

        DECLARE @results TABLE ([id] INT, 
                                [gl_account_no] VARCHAR (30), 
                                [gl_description] VARCHAR (30), 
                                [inactive] CHAR (1), 
                                [created_by] VARCHAR (10), 
                                [create_dt] DATETIME, 
                                [last_updated_by] VARCHAR (10), 
                                [last_update_dt] DATETIME,
                                [reason] VARCHAR(255))

    /*  Get GL Account Info  */

        INSERT INTO @results ([id], [gl_account_no], [gl_description], [inactive], [created_by], [create_dt], [last_updated_by], [last_update_dt], [reason])
        SELECT [id], 
               [gl_account_no], 
               [gl_description], 
               [inactive], 
               [created_by], 
               [create_dt], 
               [last_updated_by], 
               [last_update_dt],
               ''
        FROM [dbo].[T_GL_ACCOUNT]
        WHERE LEFT([gl_account_no], 3) in ('001', '435', '440', '450', '460', '461', '462', '465', '470', '480') 
          AND CAST([last_update_dt] AS DATE) >= '2020-07-24'
          AND SUBSTRING([gl_account_no], 20, 1) <> '9'  

    /*  Identify Exceptions one at a time  */

        UPDATE @results 
        SET [reason] = 'LEN(gl_account_no) <> 20 AND id <> 2224' 
        WHERE LEN([gl_account_no]) <> 20 
          AND [id] <> 2224
        
        UPDATE @results 
        SET [reason] = 'LEFT(gl_account_no, 3) = ''001'' AND LEFT(gl_account_no, 8) not in (''001-1298'', ''001-1510'') AND DATEDIFF(d,''2016-05-17'',create_dt) > 0' 
        WHERE LEFT([gl_account_no], 3) = '001' 
          AND LEFT([gl_account_no], 8) NOT IN ('001-1298', '001-1510') 
          AND DATEDIFF(d,'2016-05-17',[create_dt]) > 0

        UPDATE @results 
        SET [reason] = 'left(gl_account_no, 8) = ''001-1298'' and left(gl_description, 7) <> ''ST Pldg''' 
        WHERE LEFT([gl_account_no], 8) = '001-1298' 
          AND LEFT([gl_description], 7) <> 'ST Pldg'

        UPDATE @results 
        SET [reason] = 'left(gl_account_no, 8) = ''001-1510'' and left(gl_description, 7) <> ''LT Pldg''' 
        WHERE LEFT([gl_account_no], 8) = '001-1510' 
          AND LEFT([gl_description], 7) <> 'LT Pldg'

        UPDATE @results 
        SET [reason] = 'left(gl_account_no, 3) = ''435'' and left(gl_description, 5) <> ''AdvEv'' and id not in (1259, 1260) AND DATEDIFF(d,''2020-07-01'',create_dt) > 0'
        WHERE LEFT([gl_account_no], 3) = '435' 
          AND LEFT([gl_description], 5) <> 'AdvEv' 
          AND [id] NOT IN (1259, 1260) 
          AND DATEDIFF(d,'2020-07-01', [create_dt]) > 0

        UPDATE @results 
        SET [reason] = 'left(gl_account_no, 3) = ''440'' and left(gl_description, 4) <> ''DonRel'' AND DATEDIFF(d,''2020-07-01'',create_dt) > 0'
        WHERE LEFT([gl_account_no], 3) = '440' 
          AND LEFT([gl_description], 6) <> 'DonRel' 
          AND DATEDIFF(d,'2020-07-01',[create_dt]) > 0

        UPDATE @results 
        SET [reason] = 'left([gl_account_no], 3) = ''460'' and left(gl_description, 2) <> ''CF'' AND  DATEDIFF(d,''2020-07-01'',create_dt) > 0'
        WHERE LEFT([gl_account_no], 3) = '460' 
        AND LEFT([gl_description], 2) <> 'CF' 
        AND  DATEDIFF(d,'2020-07-01',[create_dt]) > 0

        UPDATE @results 
        SET [reason] = 'left(gl_account_no, 3) = ''470'' and left(gl_description, 3) <> ''LPG'' and id not in (13, 2199, 2200)'
        WHERE LEFT([gl_account_no], 3) = '470' 
        AND LEFT([gl_description], 3) <> 'LPG' 
        AND [id] NOT IN (13, 2199, 2200)

        UPDATE @results 
        SET [reason] = 'left(gl_account_no, 3) = ''480'' and left(gl_description, 6) <> ''AnnGiv'' and id <> 12'
        WHERE LEFT([gl_account_no], 3) = '480' 
          AND LEFT([gl_description], 6) <> 'AnnGiv' 
          AND [id] <> 12

        UPDATE @results 
        SET [reason] = 'SUBSTRING(gl_account_no, 5,4) not in (''1298'', ''1510'', ''4010'', ''4020'', ''4030'', ''4040'', ''4050'', ''4060'', ''4070'', ''4720'', ''4760'', ''4761'', 4780'', ''4781'', ''4799'',  ''4998'', ''4999'', ''5953'') AND DATEDIFF(d,''2016-05-17'',create_dt) > 0'
        WHERE SUBSTRING([gl_account_no], 5,4) NOT IN ('1298', '1510', '4010', '4020', '4030', '4040', '4050', '4060', '4070', '4720', '4760', '4761', '4780', '4781', '4799', '4998', '4999', '5953') 
          AND DATEDIFF(d,'2016-05-17',[create_dt]) > 0

        UPDATE @results 
        SET [reason] = 'SUBSTRING(gl_account_no, 5,4) = ''4010'' and gl_description not like ''%Ind Gift%'' and id not in (12,13)'
        WHERE SUBSTRING([gl_account_no], 5,4) = '4010' 
          AND [gl_description] NOT LIKE '%Ind Gift%' 
          AND [id] NOT IN (12,13)

        UPDATE @results 
        SET [reason] = 'SUBSTRING(gl_account_no, 5,4) = ''4020'' and gl_description  not like ''%Crp Gift%'''
        WHERE SUBSTRING([gl_account_no], 5,4) = '4020' 
          AND [gl_description] NOT LIKE '%Crp Gift%'

        UPDATE @results 
        SET [reason] = 'SUBSTRING(gl_account_no, 5,4) = ''4030'' and gl_description not like ''%Fdn Gift%'''
        WHERE SUBSTRING([gl_account_no], 5,4) = '4030' 
          AND [gl_description] NOT LIKE '%Fdn Gift%'

        UPDATE @results 
        SET [reason] = 'SUBSTRING(gl_account_no, 5,4) = ''4040'' and gl_description not like ''%Fed Grt%'''
        WHERE SUBSTRING([gl_account_no],5,4) = '4040' 
          AND [gl_description] NOT LIKE '%Fed Grt%'

        UPDATE @results 
        SET [reason] = 'SUBSTRING(gl_account_no, 5,4) = ''4050'' and gl_description not like ''%Sta Grt%'''
        WHERE SUBSTRING([gl_account_no],5,4) = '4050' 
          AND [gl_description] NOT LIKE '%Sta Grt%'

        UPDATE @results 
        SET [reason] = 'SUBSTRING(gl_account_no, 5,4) = ''4060'' and gl_description not like ''%Loc Grt%'''
        WHERE SUBSTRING([gl_account_no],5,4) = '4060' 
          AND [gl_description] not like '%Loc Grt%'

        UPDATE @results 
        SET [reason] = 'SUBSTRING(gl_account_no, 5,4) = ''4760'' and gl_description not like ''%Spnsrsp%'')'
        WHERE SUBSTRING([gl_account_no],5,4) = '4760' 
          AND [gl_description] NOT LIKE '%Spnsrsp%'

        UPDATE @results 
        SET [reason] = 'SUBSTRING(gl_account_no, 5,4) = ''4781'' and gl_description not like ''%Tix%'''
        WHERE SUBSTRING([gl_account_no],5,4) = '4781' 
          AND [gl_description] NOT LIKE '%Tix%'

        UPDATE @results 
        SET [reason] = 'SUBSTRING(gl_account_no, 5,4) = ''4998'' and gl_description not like ''%Cntrb Svcs%'''
        WHERE SUBSTRING([gl_account_no],5,4) = '4998' 
          AND [gl_description] NOT LIKE '%Cntrb Svcs%'

        UPDATE @results 
        SET [reason] = 'SUBSTRING(gl_account_no, 5,4) = ''4999'' and gl_description not like ''%GIK%'''
        WHERE SUBSTRING([gl_account_no],5,4) = '4999' 
          AND [gl_description] NOT LIKE '%GIK%'

        --UPDATE @results 
        --SET [reason] = 'SUBSTRING(gl_account_no, 5,4) = ''4997'' and gl_description not like ''%Misc Inc%'''
        --WHERE SUBSTRING([gl_account_no],5,4) = '4997' 
        --  AND [gl_description] NOT LIKE '%Misc Inc%'

        UPDATE @results 
        SET [reason] = 'SUBSTRING(gl_account_no, 5,4) = ''4761'' and gl_description not like ''%Spons Act%'''
        WHERE SUBSTRING([gl_account_no],5,4) = '4761' 
          AND [gl_description] NOT LIKE '%Spons Act%'

        UPDATE @results 
        SET [reason] = 'LEFT(gl_account_no, 3) = ''450'' AND LEFT(gl_description, 6) <> ''IndGiv'' and [id] <> 12 AND DATEDIFF(d,''2020-07-01'',create_dt) > 0'
        WHERE LEFT([gl_account_no], 3) = '450' 
          AND LEFT([gl_description], 6) <> 'IndGiv' 
          AND [id] <> 12 
          AND DATEDIFF(d,'2020-07-01',[create_dt]) > 0

        UPDATE @results 
        SET [reason] = 'LEFT(gl_account_no, 3) = ''461'' AND DATEDIFF(d,''2020-07-01'',create_dt) > 0'
        WHERE LEFT([gl_account_no], 3) = '461' 
          AND DATEDIFF(d,'2020-07-01',[create_dt]) > 0

        UPDATE @results 
        SET [reason] = 'LEFT(gl_account_no, 3) = ''462'' AND LEFT(gl_description, 3) <> ''EiE'' and [id] <> 12 AND DATEDIFF(d,''2020-07-01'',create_dt] > 0'
        WHERE LEFT([gl_account_no], 3) = '462' 
          AND LEFT([gl_description], 3) <> 'EiE' 
          AND [id] <> 12 
          AND DATEDIFF(d,'2020-07-01',[create_dt]) > 0

        UPDATE @results 
        SET [reason] = 'LEFT(gl_account_no, 3) = ''465'' AND LEFT(gl_description, 4) <> ''Govt'' and id <> 12 AND DATEDIFF(d,''2020-07-01'',create_dt) > 0)'
        WHERE LEFT([gl_account_no], 3) = '465' 
          AND LEFT([gl_description], 4) <> 'Govt' 
          AND [id] <> 12 AND DATEDIFF(d,'2020-07-01',[create_dt]) > 0

        UPDATE @results 
        SET [reason] = 'SUBSTRING(gl_account_no, 5,4) = ''4070'' AND gl_description NOT LIKE ''%Intl Grt%'' and id <> 12'
        WHERE SUBSTRING([gl_account_no], 5,4) = '4070' 
        AND [gl_description] NOT LIKE '%Intl Grt%' 
        AND [id] <> 12

    /*  Delete records with no exception  */

        DELETE FROM @results 
        WHERE [reason] = ''

    FINISHED:

        /* Final Data Set For Report  */

            SELECT  [id],
                    [gl_account_no],
                    [gl_description],
                    [inactive],
                    [created_by],
                    [create_dt],
                    [last_updated_by],
                    [last_update_dt],
                    [reason]
            FROM @results
                           
END
GO

--EXECUTE [dbo].[LRP_EXCEPTION_COA_T_GL_ACCOUNT]

