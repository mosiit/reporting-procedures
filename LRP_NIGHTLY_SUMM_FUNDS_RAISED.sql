USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LRP_NIGHTLY_SUMM_FUNDS_RAISED]    Script Date: 12/14/2020 2:25:24 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



ALTER PROCEDURE [dbo].[LRP_NIGHTLY_SUMM_FUNDS_RAISED]
	@section VARCHAR(300),
	@sortOrder INT,
	@runBy	VARCHAR(300),
	@fy VARCHAR(4),
	@fystart DATETIME,
	@fyend DATETIME
AS

BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE @tblPassOne	TABLE	(
								customer_no	INT,
								value			MONEY
								)

	DECLARE @tblPassTwo TABLE	(
								customer_no		INT,
								value			MONEY
								)

	DECLARE @tblPassBoth TABLE	(
								customer_no		INT,
								value			MONEY
								)

	DECLARE @tbl_ns TABLE ([customer_no] [int] NOT NULL,
						   [section] [varchar](300) NOT NULL,
						   [value] [money] NULL,
						   [apply_date] [varchar](20) NULL,
						   [sort_order] [int] NULL,
						   [created_on] [datetime] NULL,
						   [created_by] [varchar](300) NULL)

	INSERT INTO @tblPassOne
        SELECT  c.customer_no,
                ISNULL(SUM(c.cont_amt), 0)
        FROM    T_CONTRIBUTION AS c
        INNER JOIN LTR_FUND_DETAIL AS f ON c.fund_no = f.fund_no
		--2020/12/14, H. Sheridan.  Service Now ticket SCTASK0001710 - join to campaign table
		JOIN	T_CAMPAIGN cam
		ON		cam.campaign_no = c.campaign_no
        WHERE   c.cont_type IN ('G', 'P')
                AND c.custom_1 IN ('(none)', 'Matching Gift Credit')
                AND CAST(c.cont_dt AS DATE) >= @fystart
                AND CAST(c.cont_dt AS DATE) <= @fyend
  				--2020/12/14, H. Sheridan.  Service Now ticket SCTASK0001710 - add criteria for campaign category
				AND	cam.category NOT IN (8,9)
				--2020/12/14, H. Sheridan.  Service Now ticket SCTASK0001710 - remove criteria for acct_grp_no
                --AND f.acct_grp_no <> '3'
        GROUP BY c.customer_no

	--SELECT 'debug - tblPassOne', @fy, @fystart, @fyend, * FROM @tblPassOne WHERE customer_no = 126287

	INSERT INTO @tblPassTwo
        SELECT  i.customer_no,
                ISNULL(SUM(c.cont_amt), 0)
        FROM    T_CONTRIBUTION AS c
        INNER JOIN T_CREDITEE AS t ON c.ref_no = t.ref_no
        INNER JOIN LTR_FUND_DETAIL AS f ON c.fund_no = f.fund_no
        INNER JOIN T_CUSTOMER AS i ON t.creditee_no = i.customer_no
		--2020/12/14, H. Sheridan.  Service Now ticket SCTASK0001710 - join to campaign table
		JOIN	T_CAMPAIGN cam
		ON		cam.campaign_no = c.campaign_no
        WHERE   c.cont_type IN ('G', 'P')
                AND c.custom_1 IN ('Primary Soft Credit', 'Matching Gift Credit', 'Stewardship Soft Credit')
                AND t.creditee_type IN ('12', '15', '5', '10', '16', '1', '14')
                AND CAST(c.cont_dt AS DATE) >= @fystart
                AND CAST(c.cont_dt AS DATE) <= @fyend
				--2020/12/14, H. Sheridan.  Service Now ticket SCTASK0001710 - add criteria for campaign category
				AND	cam.category NOT IN (8,9)
				--2020/12/14, H. Sheridan.  Service Now ticket SCTASK0001710 - remove criteria for acct_grp_no
                --AND f.acct_grp_no <> '3'
        GROUP BY i.customer_no

	--SELECT 'debug - tblPassTwo', @fy, @fystart, @fyend, * FROM @tblPassTwo WHERE customer_no = 126287

	INSERT INTO @tblPassBoth
		SELECT * FROM @tblPassOne

	INSERT INTO @tblPassBoth
		SELECT * FROM @tblPassTwo

	--SELECT 'debug - tblPassBoth', @fy, @fystart, @fyend, * FROM @tblPassBoth WHERE customer_no = 126287

	INSERT INTO @tbl_ns
		SELECT  t1.customer_no,
				@section,
				ISNULL(t1.value, 0),
				'',
				@sortOrder,
				GETDATE(),
				@runBy
		FROM	@tblPassBoth t1

	--SELECT 'debug - tbl_ns', @fy, @fystart, @fyend, * FROM @tbl_ns WHERE customer_no = 126287

	INSERT INTO LT_NIGHTLY_SUMMARY
		SELECT	DISTINCT [customer_no],
				[section],
				SUM(ISNULL(value, 0)),
				[apply_date],
				[sort_order],
				[created_on],
				[created_by] 
		FROM    @tbl_ns
		GROUP BY [customer_no], [section], [apply_date], [sort_order], [created_on], [created_by] 

	--SELECT 'debug - LT_NIGHTLY_SUMMARY', @fy, @fystart, @fyend, * FROM LT_NIGHTLY_SUMMARY WHERE customer_no = 126287

END


GO


