USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_SCHOOL_EXHIBIT_HALLS_PRICE_BREAKDOWN]') AND type in (N'P', N'PC')) BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_SCHOOL_EXHIBIT_HALLS_PRICE_BREAKDOWN] AS' 
END
GO

ALTER PROCEDURE [dbo].[LRP_SCHOOL_EXHIBIT_HALLS_PRICE_BREAKDOWN]
        @report_start_dt DATETIME = NULL,
        @report_end_dt DATETIME = NULL,
        @student_teacher_chaperone VARCHAR(100) = ''
AS BEGIN

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    SET NOCOUNT ON

    /*  Procedure Variables  */

        DECLARE @type_list TABLE ([price_type_name] varchar(30))

        DECLARE @data_table TABLE ([order_no] INT, [customer_no] INT, [customer_name] VARCHAR(150), [title_name] VARCHAR(30), [production_name] VARCHAR(30), [performance_date] CHAR(10),
                                   [performance_time] VARCHAR(8), [price_type] INT, [price_type_name] VARCHAR(30), [per_ticket_amount] MONEY, [rule_ind] CHAR(1), [rule_id] INT, 
                                   [rule_name] VARCHAR(30), [total_tickets] INT, [total_amount] MONEY, [report_message] VARCHAR(100))

    /*  Check Parameters  */
        
        --If Null Dates, run for Yesterday
        SELECT @report_start_dt = ISNULL(@report_start_dt, DATEADD(DAY,-1,GETDATE()));
        SELECT @report_end_dt = ISNULL(@report_end_dt, DATEADD(DAY,-1,GETDATE()));

        --Make sure start date is at beginning of day and end date is at end of day
        SELECT @report_start_dt = CONVERT(CHAR(10),@report_start_dt,111) + ' 00:00:00',
               @report_end_dt = CONVERT(CHAR(10),@report_end_dt, 111) + ' 23:59:59.957';

        --Parse out school/teacher/chaperone
        IF ISNULL(@student_teacher_chaperone,'') <> '' BEGIN

            INSERT INTO @type_list ([price_type_name])
            SELECT [item] FROM [dbo].[LFT_SPLIT_DILEMETED_STRING](REPLACE(@student_teacher_chaperone,'"',''),',')

            --If the "Other" option is chosen, get all price types that were used in the date range that
            --are not School Chaperone, School Student, or School Teacher
            IF EXISTS (SELECT * FROM @type_list WHERE [price_type_name] = 'Other')
                INSERT INTO @type_list ([price_type_name])
                SELECT DISTINCT [price_type_name] FROM [dbo].[LV_SCHOOL_EXHIBIT_ORDER_PRICING]
                WHERE [performance_dt] BETWEEN @report_start_dt AND @report_end_dt
                      AND [price_type_name] NOT IN ('School Chaperone', 'School Student','School Teacher')

            --Since "Other" is not an actual price type, delete it if it exists in the table
            DELETE FROM @type_list WHERE [price_type_name] = 'Other'

        END


    /*  Get the initial list of records using a view called LV_SCHOOL_EXHIBIT_ORDER_PRICING
        This view gives a pricing breakdown for the School Exhibit Halls product
        Note: I created the view because the exact same logic needed to be used twice in the procedure, once above
              when parsing out the "other" option in the @student_teacher_chapterone parameter, and a second time 
              for this insert statement.
          */

        INSERT INTO @data_table ([order_no], [customer_no], [customer_name], [title_name], [production_name], [performance_date], [performance_time], [price_type], [price_type_name],
                                 [per_ticket_amount], [rule_ind], [rule_id], [rule_name], [total_tickets], [total_amount], [report_message])
        SELECT [order_no], 
               [customer_no],
               [dbo].[LFS_GetCustomerFullName] ([customer_no]),
               [title_name],
               [production_name],
               [performance_date],
               [performance_time],
               [price_type],
               [price_type_name],
               [paid_amt],
               [rule_ind],
               [rule_id],
               [rule_name],
               [ticket_count],
               [paid_amount],
               ''
        FROM [dbo].[LV_SCHOOL_EXHIBIT_ORDER_PRICING]
        WHERE [performance_dt] BETWEEN @report_start_dt AND @report_end_dt

    /*  If specific price types were passed to the report, delete those not in the list  */

        IF EXISTS (SELECT * FROM @type_list)
            DELETE FROM @data_table
            WHERE [price_type_name] NOT IN (SELECT [price_type_name] FROM @type_list)

    /*  Remove the rule name from the qualifying lines
        Only want to see it on lines where the actual price was changed  */

        UPDATE @data_table
        SET [rule_name] = ''
        WHERE [rule_ind] = 'Q'

    /*  Indicate No Rule Where no rule name is present  */

        UPDATE @data_table
        SET [rule_name] = 'No Rule'
        WHERE [rule_name] = ''

    /*  Update some of the data to make the strings shorter)  */
    
        UPDATE @data_table
        SET [rule_name] = '$5 Disc Month'
        WHERE [rule_name] LIKE '$5 Discount Months%'

        UPDATE @data_table
        SET [price_type_name] = 'Exhibit Hall Pass'
        WHERE [price_type_name] LIKE '%Pass%'

        UPDATE @data_table
        SET [price_type_name] = 'Tour Operator'
        WHERE [price_type_name] LIKE '%Tour Operator%'
        
    FINISHED:

        /* Check to make sure there's data to return.  If not add a single record with a message  */

            IF NOT EXISTS (SELECT * FROM @data_table)
                INSERT INTO @data_table ([report_message])
                VALUES ('No data was found for the criteria you entered.')


        /*  Pull final data set to pass to the report  */

            SELECT [order_no], 
                   [customer_no], 
                   [customer_name],
                   [title_name], 
                   [production_name], 
                   [performance_date], 
                   [performance_time], 
                   [price_type], 
                   [price_type_name], 
                   [per_ticket_amount], 
                   [rule_ind], 
                   [rule_id], 
                   [rule_name], 
                   [total_tickets], 
                   [total_amount], 
                   [report_message]
            FROM @data_table

END
GO

GRANT EXECUTE ON [dbo].[LRP_SCHOOL_EXHIBIT_HALLS_PRICE_BREAKDOWN] TO ImpUsers
GO

--EXECUTE [dbo].[LRP_SCHOOL_EXHIBIT_HALLS_PRICE_BREAKDOWN] @report_start_dt = '10-1-2017', @report_end_dt = '10-31-2017 23:59:59', @student_teacher_chaperone = ''
--SELECT @report_start_dt = '10-1-2017', @report_end_dt = '10-31-2017 23:59:59', @student_teacher_chaperone = '"School Student","Other","School Chaperone"'
--SELECT * FROM dbo.T_PRICING_RULE WHERE description LIKE '%Discount Months%'
