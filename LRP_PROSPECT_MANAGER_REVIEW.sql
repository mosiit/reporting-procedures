
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[LRP_PROSPECT_MANAGER_REVIEW]
	@PM INT = NULL
	,@List_no INT = NULL

AS BEGIN

    SET NOCOUNT ON

SELECT DISTINCT
	pm.customer_no
	,total_cust = DENSE_RANK() OVER(PARTITION BY pm.pm_id ORDER BY pm.customer_no) + DENSE_RANK() OVER(PARTITION BY pm.pm_id ORDER BY pm.customer_no desc) - 1
	,cust_full_name = 
		CASE
			WHEN LTRIM(c.fname) IS NULL OR c.fname = '' THEN LTRIM(c.lname)
			ELSE LTRIM(c.fname) + ' ' + LTRIM(c.lname)
		END
	,cust_sort_name = c.sort_name
	,pm.pm_initial
	,pm.pm_name
	,pm.pm_id
	,RIGHT(RTRIM(pm.pm_initial), 1) AS lni
	,LEFT(pm.pm_initial, 1) AS fni
	,Dept = ''
	,cn.cust_notes_no
	,last_update_dt = COALESCE(CONVERT(VARCHAR(10),cn.last_update_dt,101),'N/A')
	,last_update = 
		CASE
			WHEN cn.last_update_dt IS NULL THEN 0
			ELSE 1
		END
	,cn.note_type 
	,cn.note_type_desc
	,PlanNotes = cn.notes 
	,p.plancampaign 
	,p.designation
	,p.status
	,p.askdate
	,p.closedate
	,ask_amt = COALESCE(p.ask_amt,0)
	,goal_amt = COALESCE(p.goal_amt,0)
	,cont_amt = COALESCE(p.cont_amt,0)
	,p.plan_no
	,probability = COALESCE(p.probability,0)
	,PPW = COALESCE(pw.primaryWorker,'N/A')
	,p.notes
FROM
	dbo.LVS_PROSPECT_MANAGER AS pm WITH (NOLOCK)
	INNER JOIN dbo.T_CUSTOMER c (NOLOCK) ON c.customer_no = pm.customer_no
	INNER JOIN dbo.TX_CUST_KEYWORD ky (NOLOCK) ON ky.customer_no = c.customer_no
	--INNER JOIN dbo.T_LIST_CONTENTS LC (NOLOCK) ON LC.customer_no = c.customer_no
	OUTER APPLY(
		SELECT
			cn.cust_notes_no
			,cn.last_update_dt
			,note_type = cn.note_type_no
			,cn.note_type_desc
			,notes = LTRIM(RTRIM(cn.note))
		FROM
			bi.VT_CUST_NOTE cn (NOLOCK) 
		WHERE
			cn.note_type_no IN(9,10,11)
			AND cn.customer_no = pm.customer_no
			) cn 
	OUTER APPLY(
		SELECT
			[plancampaign] = g.description	 
		   ,[designation] = d.description	 
		   ,[status] = ps.description	 
		   ,[askdate] = CAST(p.start_dt	AS DATE)
		   ,[closedate] = CAST(p.complete_by_dt AS DATE)
		   ,p.ask_amt
		   ,p.goal_amt
		   ,p.cont_amt
		   ,p.plan_no
		   ,p.probability
		   ,[notes] = LTRIM(RTRIM(p.notes))
		FROM
			dbo.T_PLAN p WITH(NOLOCK) 
			INNER JOIN dbo.T_CAMPAIGN g WITH(NOLOCK) ON p.campaign_no = g.campaign_no
			LEFT OUTER JOIN dbo.TR_PLAN_STATUS ps WITH(NOLOCK) ON p.status = ps.id
			LEFT OUTER JOIN dbo.TR_CONT_DESIGNATION d WITH(NOLOCK) ON p.cont_designation = d.id
		WHERE
			p.type = 7
			AND g.fyear >=
				(
					SELECT
						fyear
					FROM
						dbo.TR_BATCH_PERIOD WITH(NOLOCK) 
					WHERE
						CAST(GETDATE() AS DATE) >= start_dt
						AND CAST(GETDATE() AS DATE) <= end_dt
			AND c.customer_no = p.customer_no
				)) P
	OUTER APPLY(
		SELECT
			pl.plan_no
		   ,cp.primary_ind
		   ,primaryWorker = 
				CASE
					WHEN c.fname IS NULL THEN c.sort_name
					ELSE c.lname + ' ' + c.fname
				END
		FROM
			dbo.T_PLAN (NOLOCK) AS pl
			INNER JOIN dbo.TX_CUST_PLAN (NOLOCK) AS cp ON p.plan_no = cp.plan_no
			INNER JOIN dbo.T_CUSTOMER (NOLOCK) AS c ON cp.customer_no = c.customer_no
			INNER JOIN dbo.T_CAMPAIGN (NOLOCK) AS g ON pl.campaign_no = g.campaign_no
		WHERE
			cp.primary_ind = 'Y'
			AND pl.plan_no = p.plan_no) PW
WHERE 
	CAST(pm.pm_id AS VARCHAR(20)) IN(SELECT DISTINCT Element FROM dbo.FT_SPLIT_LIST (@PM,',')) 
	AND (COALESCE(@list_no, 0) = 0 OR c.customer_no IN (SELECT customer_no FROM dbo.T_LIST_CONTENTS WHERE list_no = @list_no))
	AND ky.keyword_no = 555

END

GO


