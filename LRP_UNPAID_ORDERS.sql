USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_UNPAID_ORDERS]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LRP_UNPAID_ORDERS]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LRP_UNPAID_ORDERS]
         @report_start_dt DATETIME = NULL,
         @report_end_dt DATETIME = NULL,
         @exclude_partially_paid CHAR(1) = 'N'
AS BEGIN

    /*  Procedure Variables  */

        DECLARE @report_msg VARCHAR(100) = ''

        DECLARE @order_table TABLE ([order_no] INT, [performance_dt] DATETIME, [total_due_amount] MONEY, [total_paid_amount] MONEY, [balance_due] money)

        DECLARE @order_info TABLE ([order_no] INT, [sli_no] INT, [li_seq_no] INT, [customer_no] INT, [sli_status_no] INT, [sli_status_name] VARCHAR(30), [title_no] INT, [title_name] VARCHAR(30), 
                                   [production_no] INT, [production_name] VARCHAR(30), [perf_no] INT, [zone_no] INT, [zone_name] VARCHAR(30), [performance_date] CHAR(10), [performance_time] CHAR(8),
                                   [price_type_no] INT, [price_type_name] VARCHAR(30), [price_type_name_short] VARCHAR(20), [comp_code_no] int, [comp_code_name] VARCHAR(30), 
                                   [comp_code_name_short] VARCHAR(20), [sli_counter] INT, [due_amount] MONEY, [current_price] MONEY, [returned_amount] MONEY, [total_amount] MONEY, [paid_amount] MONEY,
                                   [returned_payment] MONEY, [total_paid] MONEY, [fee_amt] MONEY, [fee_parent_sli_no] INT, [seat_no] INT, [ticket_no] INT, [batch_no] INT, [recipient_no] INT, 
                                   [recipient_name] VARCHAR(150), [customer_name] VARCHAR(150), [record_type] VARCHAR(30), [report_msg] VARCHAR(100))

    /*  Check Parameters  - If Nulls passed to dates, run for yesterday  */

        SELECT @report_start_dt = IsNull(@report_start_dt, dateadd(day, -1, getdate())),
               @report_end_dt = IsNull(@report_end_dt, dateadd(day, -1, getdate()))

        SELECT @exclude_partially_paid = ISNULL(@exclude_partially_paid,'N')

    /*  Make sure the date parameters include the full dates, starting at 00:00 and ending at 23:59  */

        SELECT @report_start_dt = convert(char(10),@report_start_dt,111) + ' 00:00:00',
               @report_end_dt = convert(char(10), @report_end_dt,111) + ' 23:59:59'


    /*  Generate a list of all valid orders for visits within the date range  */
    
        INSERT INTO @order_table
        SELECT DISTINCT sli.[order_no], prf.[performance_dt], ord.[tot_due_amt], ord.[tot_paid_amt], ord.[tot_due_amt] - ord.[tot_paid_amt]
        FROM [dbo].[T_SUB_LINEITEM] AS sli (NOLOCK)
             LEFT OUTER JOIN [dbo].[T_ORDER] AS ord (NOLOCK) ON ord.[order_no] = sli.[order_no]
             LEFT OUTER JOIN [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] AS prf (NOLOCK) ON prf.[performance_no] = sli.[perf_no] AND prf.[performance_zone] = sli.[zone_no]
        WHERE prf.[performance_dt] BETWEEN @report_start_dt AND @report_end_dt AND sli.[sli_status] IN (3, 6, 12)

    /*  Delete fully paid for orders  */

        DELETE FROM @order_table WHERE [balance_due] = 0.00

    /*  If indicated in the parameters, delete partially paid for orders  */

        IF @exclude_partially_paid = 'Y'
            DELETE FROM @order_table WHERE total_paid_amount > 0.00

    /*  Get the order detail for the orders being displayed on the report  */

        INSERT INTO @order_info
        SELECT det.[order_no], det.[sli_no], det.[li_seq_no], det.[customer_no], det.[sli_status_no], det.[sli_status_name], det.[title_no], det.[title_name], det.[production_no], det.[production_name], 
               det.[perf_no], det.[zone_no], det.[zone_name], det.[performance_date], det.[performance_time], det.[price_type_no], det.[price_type_name], det.[price_type_name_short], 
               det.[comp_code_no], det.[comp_code_name], det.[comp_code_name_short], 1, det.[due_amount], det.[current_price], det.[returned_amount], det.[total_amount], 0.00, 
               det.[returned_payment], 0.00, det.[fee_amt], det.[fee_parent_sli_no], det.[seat_no], det.[ticket_no], det.[batch_no], det.[recipient_no], det.[recipient_name],
               LTRIM(ISNULL(cus.[fname],'') + ' ' + CASE WHEN ISNULL(cus.[mname],'') = '' THEN '' ELSE cus.[mname] + ' ' END + ISNULL(cus.[lname],'')), 'Detail', ''
        FROM [dbo].[LV_ORDER_DETAIL] AS det (NOLOCK)
             LEFT OUTER JOIN [dbo].[T_CUSTOMER] AS cus (NOLOCK) ON cus.[customer_no] = det.[customer_no]
        WHERE det.[order_no] IN (SELECT order_no FROM @order_table)

    /*  Get the payment detail for the partially paid for orders on the report  */

        INSERT INTO @order_info
        SELECT pay.[order_no], pay.[transaction_no], pay.[transaction_sequence_no], pay.[payment_customer_no], 0, '', 0, 'Payment', pay.[transaction_type_no], pay.[payment_method_name],
               pay.[payment_no], pay.[payment_sequence_no], '', CONVERT(CHAR(10),pay.[payment_dt],111), LEFT(CONVERT(CHAR(8),pay.[payment_dt],108),5), pay.[payment_method_no], 
               pay.[payment_method_name], '', 0, '', '', 0, 0.00, 0.00, 0.00, pay.[payment_amount], pay.[payment_amount],
               0.00, pay.[payment_amount], 0.00, 0, 0, 0, pay.[transaction_batch_no], 0, '', pay.[payment_customer_name], 'Payments', ''
        FROM [dbo].[LV_ORDER_PAYMENTS] AS pay
        WHERE pay.[order_no] IN (SELECT [order_no] FROM @order_table) AND [pay].[payment_amount] <> 0.00

    /*  Insert a single "No Payments" record into the table for orders where no payments were taken  */

        INSERT INTO @order_info
        SELECT DISTINCT [order_no], 0,  0, [customer_no], 0, '', 0, 'Payment', 0, 0, 0, 0, '', CONVERT(CHAR(10),GETDATE(),111), CONVERT(CHAR(8),GETDATE(),108), 0, 'No Payments For This Order', '',
               0, '', '', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, [batch_no], 0, '', [customer_name], 'Payments', ''
        FROM @order_info WHERE [record_type] = 'Detail' AND order_no NOT IN (SELECT order_no FROM @order_info WHERE [record_type] = 'Payments')

    FINISHED:

        /*  Check to make sure that data was collected and if nothing in the table add a single record with a nothing found message  */

            IF NOT EXISTS (SELECT * FROM @order_info) BEGIN

                IF @report_msg = '' SELECT @report_msg = 'No data found for the criteria you entered.'

                INSERT INTO @order_info
                VALUES (0, 0,  0, 0, 0, '', 0, '', 0, 0, 0, 0, '', '', '', 0, '', '', 0, '', '', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, '', '', 'Detail', @report_msg)

           END

        /*  Select final data set to be passed to the report  */

            SELECT [order_no], [sli_no], [li_seq_no], [customer_no], [sli_status_no], [sli_status_name], [title_no], [title_name], [production_no], [production_name], [perf_no], 
                   [zone_no], [zone_name], [performance_date], [performance_time], [price_type_no], [price_type_name], [price_type_name_short], [comp_code_no], [comp_code_name], 
                   [comp_code_name_short], [sli_counter], [due_amount], [current_price], [returned_amount], [total_amount], [paid_amount], [returned_payment], [total_paid], [fee_amt], 
                   [fee_parent_sli_no], [seat_no], [ticket_no], [batch_no], [recipient_no], [recipient_name], [customer_name], [record_type], [report_msg]
            FROM @order_info  --WHERE record_type = 'Detail'
            ORDER BY [order_no], [record_type], [title_name], [performance_time]

END
GO

GRANT EXECUTE ON [dbo].[LRP_UNPAID_ORDERS] TO ImpUsers
GO


--EXECUTE  [dbo].[LRP_UNPAID_ORDERS] '1-1-2017', '1-31-2017', 'N'

