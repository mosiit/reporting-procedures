USE [impresario]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[LRP_ADV_WKLY_VERIFIED_BDAYS]
(@numOfDays INT = 15) 
AS

SET NOCOUNT ON 
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

-- Pulling folks who have a birthday in the next @numOfDays who 1) are either a Board Member or 2) have a PM

SELECT bday.customer_no, cust.display_name, cust.sort_name, bday.birthDate, bday.VerifiedStatus, brd.board,
	[dbo].[LFS_BOARD_GetProspectManager](bday.customer_no) AS PM_full_name 
FROM [LV_CONSTITUENT_BIRTHDAY_VERIFICATION] bday
INNER JOIN dbo.FT_CONSTITUENT_DISPLAY_NAME() AS cust 
	ON bday.customer_no = cust.customer_no
LEFT JOIN [LVS_CURRENT_BOARD] brd
	ON brd.individual_customer_no = bday.customer_no
WHERE (DATEDIFF(dd, getdate(), DATEADD(yyyy, DATEDIFF(yyyy, bday.birthDate, getdate()), bday.birthDate)) + 365) % 365 <= @numOfDays
	AND bday.verifiedStatus LIKE 'Verified%'
	AND (board IS NOT NULL OR [dbo].[LFS_BOARD_GetProspectManager](bday.customer_no) <> '')
ORDER BY board DESC, cust.sort_name

