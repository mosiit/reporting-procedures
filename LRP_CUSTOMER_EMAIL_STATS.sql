USE [impresario]
GO

--DROP PROCEDURE [dbo].[LRP_CUSTOMER_EMAIL_STATS]


SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_CUSTOMER_EMAIL_STATS]') AND type IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_CUSTOMER_EMAIL_STATS] AS' 
END
GO

ALTER PROCEDURE [dbo].[LRP_CUSTOMER_EMAIL_STATS]
        @order_start_dt DATETIME = Null,
        @order_end_dt DATETIME = Null,
        @perf_start_dt DATETIME = Null,
        @perf_end_dt DATETIME = Null,
        @title_str varchar(4000) = '',  --Ignored if no visit dates passed
        @mode_of_sale_str VARCHAR(4000),
        @channel_str VARCHAR(4000),
        @customer_type VARCHAR(50),  --Everyone/Members/Non-Members
        @output_type VARCHAR(25) = '',
        @opt_in_rule VARCHAR(25) = 'Loose'
WITH RECOMPILE AS BEGIN

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    SET NOCOUNT ON

    /*  Procedure Variables  */
    
        DECLARE @title_list TABLE ([title_no] INT);

        DECLARE @mos_list TABLE ([mos_no] INT);

        DECLARE @channel_list TABLE ([channel_no] INT)

    /*  Temporary Tables  */

        IF OBJECT_ID('tempdb..#order_list') IS NOT NULL DROP TABLE [#order_list]

        CREATE TABLE [#order_list] ([order_no] INT, [order_dt] DATETIME, [order_source] INT, [order_source_name] VARCHAR(100), [order_channel] INT, 
                                    [order_channel_name] VARCHAR(30), [order_mode_of_sale] INT, [order_mode_of_sale_name] VARCHAR(30), 
                                    [order_customer_no] INT, [order_customer_name] VARCHAR(255), [has_primary_email] TINYINT, [no_primary_email] TINYINT, 
                                    [opt_in] TINYINT, [not_opt_in] TINYINT, [is_member] CHAR(1), [output_type] VARCHAR(30), [rpt_message] VARCHAR(100));

        IF OBJECT_ID('tempdb..#performance_cache') IS NOT NULL DROP TABLE [#performance_cache]

        CREATE TABLE [#performance_cache] ([perf_no] INT, [zone_no] INT, [performance_dt] DATETIME, [performance_date] CHAR(10), 
                                           [performance_time] VARCHAR(8), [title_no] INT, [title_name] VARCHAR(30), [production_name] VARCHAR(30));

        IF OBJECT_ID('tempdb..#email_address_customers') IS NOT NULL DROP TABLE [#email_address_customers]

        CREATE TABLE [#email_address_customers] ([row_num] INT, [customer_no] INT, [list_no] INT, [email_address] VARCHAR(100), [email_create_dt] DATETIME, [list_create_dt] DATETIME);


    /*  Check Parameters  */

        --If either @perf_start_dt or @perf_end_dt is set to Null, make sure they are both set to Null
        IF @perf_start_dt IS NULL OR @perf_end_dt IS NULL
            SELECT @perf_start_dt = NULL, @perf_end_dt = NULL;

        --If either @order_start_dt or @order_end_dt is set to Null, make sure they are both set to Null
        IF @order_start_dt IS NULL OR @order_end_dt IS NULL
            SELECT @order_start_dt = NULL, @order_end_dt = NULL;

        --If all date variables are null, go to the end
        IF @perf_start_dt IS NULL AND @perf_end_dt IS NULL AND @order_start_dt IS NULL AND @order_end_dt IS NULL GOTO FINISHED;

        --Set default values for output type and opt in rule

        IF ISNULL(@output_type,'') <> 'Mode of Sale' SELECT @output_type = 'Sales Channel';

    /*  Parse title, mode of sale, and channel lists  */

        IF ISNULL(@title_str,'') <> ''
            INSERT INTO @title_list 
            SELECT CONVERT(INT,Element) 
            FROM dbo.FT_SPLIT_LIST(REPLACE(@title_str,'"',''),',');
        ELSE
            INSERT INTO @title_list 
            SELECT [inv_no] FROM [dbo].[T_INVENTORY] WHERE [type] = 'T';

        IF ISNULL(@mode_of_sale_str,'') <> ''
            INSERT INTO @mos_list 
            SELECT CONVERT(INT,[Element]) FROM [dbo].[FT_SPLIT_LIST] (REPLACE(@mode_of_sale_str,'"',''),',');
        ELSE
            INSERT INTO @mos_list 
            SELECT [id] FROM [dbo].[TR_MOS];

        IF ISNULL(@channel_str,'') <> ''
            INSERT INTO @channel_list 
            SELECT CONVERT(INT,[Element]) FROM [dbo].[FT_SPLIT_LIST] (REPLACE(@channel_str,'"',''),',');
        ELSE
            INSERT INTO @channel_list
            SELECT [id] FROM [dbo].[TR_SALES_CHANNEL];

    /*  Set order dates to what is essentally all dates if they are null  */

    SELECT @order_start_dt = ISNULL(@order_start_dt,'1-1-1900'), 
           @order_end_dt = ISNULL(@order_end_dt,'12-31-2999 23:59:59');

    /*  Passing Visit Dates requires more work, so that is checked first.  All attendance generating performances are cached,
        A list of orders is generated using subline items that contain any of the performances in the performance cache.  
        If order dates are also passed to the report, anything with an order date outside of that range is ommitted.
        If just order dates are passed and not visit dates, a list of orders is generated using the order date field in the T_ORDER table.  */

        IF @perf_start_dt IS NOT NULL AND @perf_end_dt IS NOT NULL BEGIN

            INSERT INTO [#performance_cache] ([perf_no], [zone_no], [performance_dt], [performance_date], [performance_time], [title_no], 
                                              [title_name], [production_name])
            SELECT prf.[performance_no], 
                   prf.[performance_zone], 
                   prf.[performance_dt], 
                   prf.[performance_date], 
                   prf.[performance_time], 
                   prf.[title_no], 
                   prf.[title_name], 
                   prf.[production_name]
            FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] AS prf
                 INNER JOIN @title_list AS ttl ON ttl.[title_no] = prf.[title_no]
            WHERE prf.[performance_dt] BETWEEN @perf_start_dt AND @perf_end_dt;

          INSERT INTO [#order_list] ([order_no], [order_dt], [order_source], [order_source_name], [order_channel], [order_channel_name], 
                                     [order_mode_of_sale], [order_mode_of_sale_name], [order_customer_no], [order_customer_name], 
                                     [has_primary_email], [no_primary_email], [opt_in], [not_opt_in], [is_member], [output_type], [rpt_message])
            SELECT ord.[order_no], 
                   ord.[order_dt], 
                   ord.[source_no], 
                   sou.[source_name], 
                   ord.[channel], 
                   cha.[description], 
                   ord.[MOS], 
                   mos.[description], 
                   ord.[customer_no], 
                   con.[display_name],
                   0, 
                   0, 
                   0, 
                   0, 
                   [dbo].[LFS_ActiveMemberOnDate] (ord.[customer_no], 4, ord.[order_dt], 'N'),
                   '', 
                   ''
            FROM [dbo].[T_ORDER] AS ord
                 LEFT OUTER JOIN [dbo].[TR_SALES_CHANNEL] AS cha ON cha.[id] = ord.[channel]
                 LEFT OUTER JOIN [dbo].[TX_APPEAL_MEDIA_TYPE] AS sou ON sou.[source_no] = ord.[source_no]
                 LEFT OUTER JOIN [dbo].[TR_MOS] AS mos ON mos.[id] = ord.[MOS]
                 LEFT OUTER JOIN [dbo].[T_EADDRESS] AS eml ON eml.[customer_no] = ord.[customer_no] AND eml.[inactive] = 'N' AND eml.primary_ind = 'Y' --eml.[eaddress_type] = 1
                 LEFT OUTER JOIN [dbo].[T_CUSTOMER] AS cus ON cus.[customer_no] = ord.[customer_no]
                 LEFT OUTER JOIN [dbo].[FT_CONSTITUENT_DISPLAY_NAME]() AS con ON con.[customer_no] = ord.[customer_no]
            WHERE ord.[order_no] IN (SELECT [order_no] FROM [dbo].[T_SUB_LINEITEM] WHERE [perf_no] IN (SELECT [perf_no] FROM [#performance_cache]))
              AND ord.[order_dt] BETWEEN @order_start_dt AND @order_end_dt;

        END ELSE BEGIN
        
            INSERT INTO [#order_list] ([order_no], [order_dt], [order_source], [order_source_name], [order_channel], [order_channel_name], 
                                       [order_mode_of_sale], [order_mode_of_sale_name], [order_customer_no], [order_customer_name], 
                                       [has_primary_email], [no_primary_email], [opt_in], [not_opt_in], [is_member], [output_type], [rpt_message])
            SELECT ord.[order_no], 
                   ord.[order_dt], 
                   ord.[source_no], 
                   sou.[source_name], 
                   ord.[channel], 
                   cha.[description], 
                   ord.[MOS], 
                   mos.[description], 
                   ord.[customer_no], 
                   con.[display_name],
                   0, 
                   0, 
                   0, 
                   0, 
                   [dbo].[LFS_ActiveMemberOnDate] (ord.[customer_no], 4, ord.[order_dt], 'N'),
                   '', 
                   ''
            FROM [dbo].[T_ORDER] AS ord
                 LEFT OUTER JOIN [dbo].[TR_SALES_CHANNEL] AS cha ON cha.[id] = ord.[channel]
                 LEFT OUTER JOIN [dbo].[TX_APPEAL_MEDIA_TYPE] AS sou ON sou.[source_no] = ord.[source_no]
                 LEFT OUTER JOIN [dbo].[TR_MOS] AS mos ON mos.[id] = ord.[MOS]
                 LEFT OUTER JOIN [dbo].[T_EADDRESS] AS eml ON eml.[customer_no] = ord.[customer_no] 
                                                              AND eml.[inactive] = 'N' AND eml.[primary_ind] = 'Y' -- eml.[eaddress_type] = 1
                 LEFT OUTER JOIN [dbo].[T_CUSTOMER] AS cus ON cus.[customer_no] = ord.[customer_no]
                 LEFT OUTER JOIN [dbo].[FT_CONSTITUENT_DISPLAY_NAME]() AS con ON con.[customer_no] = ord.[customer_no]
                 INNER JOIN @mos_list AS msl ON msl.[mos_no] = ord.[MOS]
                 INNER JOIN @channel_list AS chl ON chl.[channel_no] = ord.[channel]
            WHERE ord.[order_dt] BETWEEN @order_start_dt AND @order_end_dt
              
        END;

    /*  If a specific customer type was chosen, remove the others  */

        IF @customer_type = 'Members'
            DELETE FROM [#order_list] WHERE [is_member] = 'N'
        ELSE IF @customer_type = 'Non-Members'
            DELETE FROM [#order_list] WHERE [is_member] = 'Y'
                
                /*****  THIS IS AN ATTEMPT TO PARTIALLY MITIGATE BAD DATA ENTRY.
                        SINCE ANY ORDER TAKEN AT THE BOX OFFICE BY DEFAULT IS IN PERSON,
                        ANY ORDER WITH THE BOX OFFICE SOURCE IS CHANGED TO HAVE THE
                        IN PERSON CHANNEL. *****/
    
                        UPDATE [#order_list]
                        SET [order_channel_name] = 'In Person'
                        WHERE [order_source_name] = 'Box Office';

    /*  Combine all the separate kiosk channels into a single channel called Kiosk  */

        UPDATE [#order_list]
        SET [order_channel_name] = 'Kiosk' 
        WHERE [order_channel_name] LIKE '%Kiosk%';


    /*  Get a list of all active primary email addresses for customers with a customer number greater than zero  */
    
        INSERT INTO [#email_address_customers] ([row_num], [customer_no], [list_no], [email_address], 
                                                [email_create_dt], [list_create_dt])
        SELECT ROW_NUMBER() OVER (PARTITION BY adr.[customer_no] ORDER BY adr.[customer_no], ISNULL(lis.[m2_list_no],0) DESC),
               adr.[customer_no], 
               ISNULL(lis.[m2_list_no],0), 
               adr.[address],
               adr.[create_dt], 
               lis.[create_dt]
        FROM [dbo].[T_EADDRESS] AS adr
             LEFT OUTER JOIN [dbo].[LTX_M2_ELISTS_EADDRESS] AS lis ON lis.[address] = adr.[address] 
                                                               AND lis.[m2_list_no] IN (SELECT [id] 
                                                                                        FROM [dbo].[LTR_M2_ELISTS] 
                                                                                        WHERE active = 'Y' AND list_type = 'Public')
        WHERE adr.[customer_no] > 0 and adr.[customer_no] IN (SELECT [order_customer_no] FROM [#order_list]) 
          AND adr.[inactive] = 'N' 
          AND adr.[eaddress_type] IN (SELECT id FROM [dbo].[TR_EADDRESS_TYPE] WHERE email_ind = 'Y')
          AND adr.[address] NOT LIKE '%kiosk%@mos.org';

    /*  Delete duplicates so that there is one row per customer */

        DELETE FROM [#email_address_customers] WHERE row_num > 1;
   
    /*  Set the no_primary_email counter to 1 on any record where the has_primary_email is not set to 1  */

            UPDATE [#order_list] 
            SET [has_primary_email] = 1
            WHERE [order_customer_no] IN (SELECT [customer_no] FROM [#email_address_customers]);

            UPDATE [#order_list]
            SET [no_primary_email] = 1
            WHERE [has_primary_email] = 0;

    /*  Set the opt_in counter to 1 for any record where the customer number is in the [#email_address_customers] table and list_no > 0  */
        
            UPDATE [#order_list] 
            SET [opt_in] = 1
            WHERE [order_customer_no] IN (SELECT [customer_no] FROM [#email_address_customers] WHERE list_no > 0)
              AND [has_primary_email] = 1;

    /*  Set the not_opt_in counter to 1 on any record with an email address where the opt_in is not set to 1  */

        UPDATE [#order_list]
        SET [not_opt_in] = 1
        WHERE [has_primary_email] = 1 and [opt_in] = 0;

    /*  If the customer name is blank or "Guest User" or "Kiosk Customer", change the customer name to Anonymous.
        On all other records, change the customer name to Known  */

        UPDATE [#order_list]
        SET [order_customer_name] = CASE WHEN ISNULL([order_customer_name],'') IN ('','Guest User','Kiosk Customer') THEN 'Anonymous'
                                         ELSE 'Known' END;

    /*  Set Output Type - the ouput type determines how the grid is set up in the SSRS report file  */

        IF @output_type = 'Mode of Sale' UPDATE [#order_list] SET [output_type] = [order_mode_of_sale_name]
        ELSE UPDATE [#order_list] SET output_type = [order_channel_name];

    FINISHED:

        /*  Pull final aggregated data set for the report*/

            SELECT [order_source_name]
                 , [order_channel_name]
                 , [order_mode_of_sale_name]
                 , [order_customer_name]
                 , COUNT(DISTINCT [order_no]) AS 'order_count'
                 , SUM([has_primary_email]) AS 'has_primary_email'
                 , SUM([no_primary_email]) AS 'no_primary_email'
                 , SUM([opt_in]) AS 'opt_in'
                 , SUM([not_opt_in]) AS 'not_opt_in'
                 , [output_type]
                 , [rpt_message]
            FROM [#order_list]
            GROUP BY [order_source_name], [order_channel_name], [order_mode_of_sale_name], 
                     [order_customer_name], [output_type], [rpt_message];

    DONE:

        IF OBJECT_ID('tempdb..#email_address_customers') IS NOT NULL DROP TABLE [#email_address_customers]
        IF OBJECT_ID('tempdb..#performance_cache') IS NOT NULL DROP TABLE [#performance_cache]
        IF OBJECT_ID('tempdb..#order_list') IS NOT NULL DROP TABLE [#order_list]

END
GO

GRANT EXECUTE ON [dbo].[LRP_CUSTOMER_EMAIL_STATS] TO ImpUsers;
GO


/*
EXECUTE [dbo].[LRP_CUSTOMER_EMAIL_STATS] 
    @order_start_dt = '3-1-2018', 
    @order_end_dt = '3-31-2018 23:59:59.957', 
    @perf_start_dt = Null, 
    @perf_end_dt = Null, 
    @customer_type = 'Non-Members',
    @title_str = '',
    @mode_of_sale_str = '',
    @channel_str = '',
    @output_type = 'Mode of Sale'
*/
