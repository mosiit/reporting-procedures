USE [impresario]
GO
/****** Object:  StoredProcedure [dbo].[LP_UPDATE_HISTORY_CONTRACT_ANALYSIS]    Script Date: 10/1/2021 11:57:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[LP_UPDATE_HISTORY_CONTRACT_ANALYSIS]
        @contract_id INT = NULL
AS BEGIN

    SET NOCOUNT ON;
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

    /*  Procedure Variables  */

        DECLARE @title_no INT = 0, 
                @title_name VARCHAR(30) = ''
        
        DECLARE @pub_prod_no INT = 0, 
                @sch_prod_no INT = 0, 
                @prod_no INT = 0, 
                @prod_name VARCHAR(30) = ''

        DECLARE @start_dt DATE, 
                @end_dt DATE, 
                @day_count INT = 0, 
                @lowest_trigger_no INT = 0, 
                @highest_trigger_no INT = 0

        DECLARE @sch_guarantee_days INT = 0, 
                @show_count_prod DECIMAL(18,4) = 0.0, 
                @show_count_total DECIMAL(18,4) = 0.0, 
                @show_percentage DECIMAL(18,4) = 0.0,
                @average_tik_Price DECIMAL(18,2) = 0.00

        DECLARE @license_admission INT = 1, 
                @license_revenue INT = 2, 
                @license_greater INT = 3
        DECLARE @license_type INT

        DECLARE @update_attendance_matrix CHAR(1) = 'N'
        
    /*  Temporary Tables  */

        IF OBJECT_ID('tempdb..#analysis_contract_info') is not null DROP TABLE [#analysis_contract_info]

        CREATE TABLE [#analysis_contract_info] ([contract_id] INT NOT NULL DEFAULT (0),
                                       [contract_name] VARCHAR(100) NOT NULL DEFAULT (''),
                                       [vendor_no] INT NOT NULL DEFAULT (0),
                                       [vendor_name] VARCHAR(100) NOT NULL DEFAULT (''),
                                       [vendor_address_1] VARCHAR(100) NOT NULL DEFAULT (''),
                                       [vendor_Address_2] VARCHAR(100) NOT NULL DEFAULT (''),
                                       [vendor_city] VARCHAR(50) NOT NULL DEFAULT (''),
                                       [vendor_state] VARCHAR(25) NOT NULL DEFAULT (''),
                                       [vendor_zip_code] VARCHAR(25) NOT NULL DEFAULT (''),
                                       [public_prod_no] INT NOT NULL DEFAULT (0),
                                       [public_prod_name] VARCHAR(30) NOT NULL DEFAULT (''),
                                       [school_prod_no] INT NOT NULL DEFAULT (0),
                                       [school_prod_name] VARCHAR(30) NOT NULL DEFAULT (''),
                                       [reporting_type] VARCHAR(30) NOT NULL DEFAULT (''),
                                       [license_type] INT NOT NULL DEFAULT (0),
                                       [license_type_name] VARCHAR(30) NOT NULL DEFAULT (''),
                                       [start_dt] DATETIME NULL,
                                       [end_dt] DATETIME NULL,
                                       [contract_comp_percentage] DECIMAL(18,4) NOT NULL DEFAULT (0.0),
                                       [minmum_shows] INT NOT NULL DEFAULT (0),
                                       [minimum_shows_achieved] DATE NULL,
                                       [attendance_guarantee] INT NOT NULL DEFAULT (0),
                                       [attendance_guarantee_achieved] DATE NULL,
                                       [schedule_guarantee_percentage] DECIMAL (18,4) NOT NULL DEFAULT (0.0),
                                       [schedule_guarantee_days] INT NOT NULL DEFAULT (0),
                                       [schedule_actual_percentage] DECIMAL(18,4) NOT NULL DEFAULT (0.0),
                                       [schedule_guarantee_achieved] CHAR(1) NOT NULL DEFAULT ('N'),
                                       [total_contract_payments] DECIMAL(18,2) NOT NULL DEFAULT (0.0))

        IF OBJECT_ID('tempdb..#analysis_triggers') is not null DROP TABLE [#analysis_triggers]

        CREATE TABLE [#analysis_triggers] ([rec_no] INT NOT NULL IDENTITY (1,1),
                                           [contract_id] INT  NULL DEFAULT (0),
                                           [trigger_no] INT  NULL DEFAULT (0),
                                           [first_trigger] CHAR(1)  NULL DEFAULT ('N'),
                                           [trigger_type] VARCHAR(30)  NULL DEFAULT (''),
                                           [trigger_range_low] INT  NULL DEFAULT (0),
                                           [trigger_range_high] INT  NULL DEFAULT (0),
                                           [trigger_description] VARCHAR(50)  NULL DEFAULT (''),
                                           [trigger_start_date] DATE NULL,
                                           [trigger_end_date] DATE NULL,
                                           [trigger_achieved] CHAR(1)  NULL DEFAULT ('N'),
                                           [trigger_status] VARCHAR(50)  NULL DEFAULT ('Pending'),
                                           [show_count] INT  NULL DEFAULT (0),
                                           [comp_attendance] INT  NULL DEFAULT (0),
                                           [comp_attendance_percentage] DECIMAL(18,4) NOT NULL DEFAULT (0.0),
                                           [comp_allowed] INT  NULL DEFAULT(0),
                                           [comp_difference] INT  NULL DEFAULT (0),
                                           [public_attendance] INT  NULL DEFAULT (0),
                                           [public_revenue] DECIMAL(18,2)  NULL DEFAULT (0.0),
                                           [public_owed_per]  DECIMAL(18,2)  NULL DEFAULT (0.0),
                                           [public_owed_rev]  DECIMAL(18,2)  NULL DEFAULT (0.0),
                                           [public_owed] DECIMAL(18,2)  NULL DEFAULT (0.0),
                                           [public_net] DECIMAL(18,2)  NULL DEFAULT (0.0),
                                           [school_attendance] INT  NULL DEFAULT (0),
                                           [school_revenue] DECIMAL(18,2)  NULL DEFAULT (0.0),
                                           [school_owed_per]  DECIMAL(18,2)  NULL DEFAULT (0.0),
                                           [school_owed_rev]  DECIMAL(18,2)  NULL DEFAULT (0.0),
                                           [school_owed] DECIMAL(18,2)  NULL DEFAULT (0.0),
                                           [school_net] DECIMAL(18,2)  NULL DEFAULT (0.0),
                                           [buyout_attendance] INT  NULL DEFAULT (0),
                                           [buyout_revenue] DECIMAL(18,2)  NULL DEFAULT (0.0),
                                           [buyout_owed_per]  DECIMAL(18,2)  NULL DEFAULT (0.0),
                                           [buyout_owed_rev]  DECIMAL(18,2)  NULL DEFAULT (0.0),
                                           [buyout_owed] DECIMAL(18,2)  NULL DEFAULT (0.0),
                                           [buyout_net] DECIMAL(18,2)  NULL DEFAULT (0.0),
                                           [comp_owed_per] DECIMAL(18,2)  NULL DEFAULT (0.0),
                                           [comp_owed_rev] DECIMAL(18,2)  NULL DEFAULT (0.0),
                                           [comp_owed] DECIMAL(18,2)  NULL DEFAULT (0.0),
                                           [total_attendance] INT  NULL DEFAULT (0),
                                           [total_revenue] DECIMAL(18,2)  NULL DEFAULT (0.0),
                                           [total_owed] DECIMAL(18,2)   NULL DEFAULT (0.0),
                                           [total_net] DECIMAL(18,2)  NULL DEFAULT (0.0),
                                           [comp_percentage_allowed] DECIMAL(18,4)  NULL DEFAULT (0.0),
                                           [per_admission_public] DECIMAL(18,2)  NULL DEFAULT (0.0), 
                                           [per_admission_school] DECIMAL(18,2)  NULL DEFAULT (0.0),
                                           [per_admission_private] DECIMAL(18,2)  NULL DEFAULT (0.0),
                                           [per_admission_comp] DECIMAL(18,2)  NULL DEFAULT (0.0),
                                           [revenue_percent_public] DECIMAL(18,4)  NULL DEFAULT (0.0),
                                           [revenue_percent_school] DECIMAL(18,4)  NULL DEFAULT (0.0),
                                           [revenue_percent_private] DECIMAL(18,4)  NULL DEFAULT (0.0),
                                           [revenue_amount_comp] DECIMAL(18,2)  NULL DEFAULT (0.0),
                                           [free_screenings_private] INT NULL DEFAULT (0),
                                           [per_screening_private] DECIMAL(18,2)  NULL DEFAULT (0.0))

        IF OBJECT_ID('tempdb..#analysis_comp') is not null DROP TABLE [#analysis_comp]

        CREATE TABLE [#analysis_comp] ([comp_year] INT NOT NULL DEFAULT (0),
                                        [comp_month] INT NOT NULL DEFAULT (0),
                                        [comp_month_id] VARCHAR (10) NOT NULL DEFAULT (''),
                                        [comp_per_admission_amount] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                        [comp_percent_allowed] DECIMAL (18,4) NOT NULL DEFAULT (0.0),
                                        [month_total_attendance] DECIMAL (18,4) NOT NULL DEFAULT (0.0),
                                        [month_comp_allowed] DECIMAL (18,4) NOT NULL DEFAULT (0.0),
                                        [month_comp_attendance] DECIMAL (18,4) NOT NULL DEFAULT (0.0),
                                        [month_comp_difference] DECIMAL (18,4) NOT NULL DEFAULT (0.0),
                                        [month_comp_owed] DECIMAL (18,2) NOT NULL DEFAULT (0.0))


    /**************************************************
        Check Parameters
        If no vaid contract id number is passed to the report, it cannot continue.
     **************************************************/

        SELECT @contract_id = ISNULL(@contract_id, 0)
        IF @contract_id <= 0 BEGIN

            THROW 51000, 'Invalid Contract Id Number.', 1;  

            GOTO FINISHED

        END

        --If the attendance matrix does not already exist or it's more then 12 hours old, the procedure will force the matrix to be rebuilt.
        IF NOT EXISTS (SELECT * FROM [dbo].[LT_HISTORY_ATTENDANCE_MATRIX] WHERE contract_id = @contract_id)
            SELECT @update_attendance_matrix = 'Y'
        ELSE IF (SELECT DATEDIFF(HOUR, (SELECT MAX(data_dt) FROM [dbo].[LT_HISTORY_ATTENDANCE_MATRIX] WHERE contract_id = 3), GETDATE())) >= 22 
            SELECT @update_attendance_matrix = 'Y'

    /**************************************************
        Get Contract Information  
        Pull the information from LTR_CONTRACT_TERMS for the contract
        There should only be a single row in this table
            Using Temp Table rather than table variable because temp table better for joining
     **************************************************/
     
        INSERT INTO [#analysis_contract_info] ([contract_id], [contract_name], [vendor_no], [vendor_name], [vendor_address_1], [vendor_Address_2],
                                      [vendor_city], [vendor_state], [vendor_zip_code], [public_prod_no], [public_prod_name], [school_prod_no],
                                      [school_prod_name], [reporting_type], [license_type], [license_type_name], [start_dt], [end_dt], 
                                      [contract_comp_percentage], [minmum_shows], [attendance_guarantee], [schedule_guarantee_percentage], 
                                      [schedule_guarantee_days])
        SELECT trm.[id],
               trm.[contract_name],
               trm.[vendor],
               ISNULL(nam.[display_name],''),
               ISNULL(adr.[street1],''),
               ISNULL(adr.[street2],''),
               ISNULL(adr.[city],''),
               ISNULL(adr.[state],''),
               ISNULL(adr.[postal_code],''),
               trm.[public_production],
               ISNULL(prp.[production_name],''),
               trm.[school_production],
               ISNULL(prs.[production_name],''),
               trm.[Reporting_type],
               trm.[license_type],
               ftp.[description],
               trm.[start_date],
               trm.[end_date],
               trm.[comp_percentage],
               trm.[minimum_shows],
               trm.[attendance_guarantee],
               trm.[schedule_guarantee_percentage],
               trm.[schedule_guarantee_days]
        FROM [dbo].[LTR_CONTRACT_TERMS] AS trm
             LEFT OUTER JOIN [dbo].[FT_CONSTITUENT_DISPLAY_NAME]() AS nam ON nam.[customer_no] = trm.[vendor]
             LEFT OUTER JOIN [dbo].[FT_GET_PRIMARY_ADDRESS]() AS adr ON adr.[customer_no] = trm.[vendor]
             LEFT OUTER JOIN [dbo].[LV_PRODUCTION_ELEMENTS_PRODUCTION] AS prp ON prp.[production_no] = trm.[public_production]
             LEFT OUTER JOIN [dbo].[LV_PRODUCTION_ELEMENTS_PRODUCTION] AS prs ON prs.[production_no] = trm.[school_production]
             LEFT OUTER JOIN [dbo].[LTR_LICENSE_FEE_TYPE] AS ftp ON ftp.[id] = trm.[license_type]
        WHERE trm.[id] = @contract_id;

    /**************************************************
        Set contract percentages to decimals
        Need to be in decimal form when using as a multiplier
            If percentages are > 1 then they need to be divided by 100 
            If they are already <= 1 then leave them alone  
        Doing this check here allows users to enter the percentages however they want to
        25% can be entered into the table as 25 or 0.25 and both will work.
     **************************************************/

        UPDATE [#analysis_contract_info]
        SET [schedule_guarantee_percentage] = ([schedule_guarantee_percentage] / 100)
        WHERE [schedule_guarantee_percentage] > 1


    /**************************************************
        Calculate total payments 
        This is an extra field in [#contact_info]
            Total of all payments taken for this contract
     **************************************************/

        UPDATE [#analysis_contract_info]
        SET [total_contract_payments] =  (SELECT ISNULL(SUM([payment_amount]), 0.0)
                                          FROM [dbo].[LTR_CONTRACT_PAYMENTS] AS pmt
                                          WHERE pmt.[contract_id] = @contract_id
                                            AND pmt.[inactive] = 'N')

    /**************************************************
        Determine earliest and latest date reporting on, as well as the production name to use
        These values are used throughout the procedure.  [#analysis_contract_info] *should* only have
        a single row, but used MIN and MAX anyway, just in case.
     **************************************************/

        SELECT @start_dt = MIN(start_dt), 
               @end_dt = MAX(end_dt),
               @license_type = MAX([license_type])
        FROM [#analysis_contract_info]


    /**************************************************
        Extract public and school production numbers from [#analysis_contract_info]
        Procedure picks one to use as the title for everything.
        It will choose the public title, unless there isn't one
        If no public title, it uses the school title.
     **************************************************/

        SELECT @pub_prod_no = ISNULL(MAX([public_prod_no]), 0),
               @sch_prod_no = ISNULL(MAX([school_prod_no]), 0) 
        FROM [#analysis_contract_info] 
        
        IF @pub_prod_no > 0 SELECT @prod_no = @pub_prod_no
        ELSE SELECT @prod_no = @sch_prod_no

        SELECT @prod_name = ISNULL([description], 'Unknown Title') 
                            FROM [dbo].[T_INVENTORY] 
                            WHERE [inv_no] = @prod_no
  
        SELECT @title_no = ISNULL([title_no],0)
        FROM [dbo].[T_PRODUCTION]
        WHERE [prod_no] = @prod_no

        SELECT @title_name = ISNULL([description],'')
        FROM [dbo].[T_INVENTORY]
        WHERE [inv_no] = @title_no

    
    /**************************************************
        Generate the Attendance Matrix with running totals 
        This is a stored procedure written specifically for this report but there is a version
        of it that may be used for other reports as well.  This versions is passed only a
        contract number from which it generates a date range and production numbers.
        It generates a grid of attendance data day by day with running totals so that the total 
        attendance can be pulled for any given day within the run of the movie.  It uses two 
        productions in case there is a public and a school production for the same show.
        The end results are put into a local table that is then used by a second procedure
        run from within the report.
     **************************************************/

        IF @update_attendance_matrix = 'Y'
            EXECUTE [dbo].[LRP_CREATE_ATTENDANCE_MATRIX_FOR_CONTRACT] @contract_id


    /**************************************************
        If a revenue % contract and no $$ amount set for extra comps, the average price paid is needed
        This looks at entire matrix, takes the total attendance, subtracts the comp attendance
        and divides that into the total paid amount to get an average ticket price over the course of the run 
     **************************************************/

        SELECT @average_tik_Price = CASE WHEN CAST(SUM([tot_sale_total]) - SUM([cmp_sale_total]) AS DECIMAL(18,2)) = 0 THEN 0.0
                                         ELSE SUM([tot_paid_amount]) / CAST(SUM([tot_sale_total]) - SUM([cmp_sale_total]) AS DECIMAL(18,2)) END
                                    FROM [dbo].[LT_HISTORY_ATTENDANCE_MATRIX]
                                    WHERE [contract_id] = @contract_id


    /**************************************************
        Some contracts have a minumum show requirement
        We need to calculate the date when we hit the  minimum number of shows
     **************************************************/

        UPDATE [#analysis_contract_info]
        SET [minimum_shows_achieved] = (SELECT MIN(perf_date) 
                                        FROM [dbo].[LT_HISTORY_ATTENDANCE_MATRIX]
                                        WHERE [contract_id] = @contract_id
                                          AND [show_count_running] >= [#analysis_contract_info].[minmum_shows])
        FROM [#analysis_contract_info]


    /**************************************************        
        Some contracts have a guaranteed number of viewers (attendance).  
        We need to calculate the date when we hit the minimum number of viewers.
     **************************************************/

        UPDATE [#analysis_contract_info]
        SET [attendance_guarantee_achieved] = (SELECT MIN(perf_date) 
                                               FROM [dbo].[LT_HISTORY_ATTENDANCE_MATRIX]
                                               WHERE [contract_id] = @contract_id
                                                 AND [tot_sale_total_running] >= [#analysis_contract_info].[attendance_guarantee])
         FROM [#analysis_contract_info]


    /**************************************************
        Some contracts have a guarantee that the show will be a certain percentage of our entire schedule
        for a set number of days after opening.  We need to determine whether or not if we reached that
        goal.  A function called LFS_GET_NUMBER_OF_SHOWS was created to count shows specifically for this.
        Pass it a start date, number of days, and a title number (no productions) and it will count all shows
        for that title.  Pass it one or two production numbers (no title) and it will count the number of shows
        for those specific titles.  Title number overrides production number(s),
     **************************************************/

        SELECT @sch_guarantee_days = ISNULL(MAX([schedule_guarantee_days]), 1) FROM [#analysis_contract_info]
        
        SELECT @show_count_prod = [dbo].[LFS_GET_NUMBER_OF_SHOWS] (@start_dt, @sch_guarantee_days, 0, @pub_prod_no, @sch_prod_no)
        
        SELECT @show_count_total = [dbo].[LFS_GET_NUMBER_OF_SHOWS] (@start_dt, @sch_guarantee_days, @title_no, 0, 0)

        SELECT @show_percentage = CASE WHEN @show_count_total = 0.0 THEN 0.0 ELSE (@show_count_prod / @show_count_total) END

        UPDATE [#analysis_contract_info]
        SET [schedule_actual_percentage] = @show_percentage

        UPDATE [#analysis_contract_info]
        SET [schedule_guarantee_achieved] = 'Y'
        WHERE [schedule_actual_percentage] >= [schedule_guarantee_percentage]
 

    /**************************************************
        Each contract will have a number of triggers, called that because each one triggers a change in terms.
        In the actual report, these are labeled as milestones.  Triggers can be based on the number of shows
        that have happened or the number of visitors that have viewed the show.  Each trigger can have a per person
        rate or a percentage of revenue rate depending on what type of contract it is.  Each trigger is numbered.
        The numbers do not have to be sequential, but each will be processed and calculated in numerical order.
     **************************************************/
        
        INSERT INTO [#analysis_triggers] ([contract_id], [trigger_no], [trigger_type], [trigger_range_low], [trigger_range_high], 
                                          [comp_percentage_allowed], [per_admission_public], [per_admission_school], [per_admission_private], 
                                          [revenue_percent_public], [per_admission_comp], [revenue_percent_school], [revenue_percent_private], 
                                          [revenue_amount_comp], [free_screenings_private], [per_screening_private])
        SELECT  trg.[contract_id],
                trg.[trigger_no],
                trg.[trigger_type],
                trg.[trigger_range_low],
                trg.[trigger_range_high],
                CASE WHEN trg.[comp_percentage] <= 0.0 THEN con.[contract_comp_percentage]      --If no comp percentage added to the trigger,
                     ELSE trg.[comp_percentage] END,                                            --use comp percentage from contract
                trg.[per_admission_public], 
                trg.[per_admission_school],
                trg.[per_admission_private],
                trg.[revenue_percent_public],
                trg.[per_admission_comp],
                trg.[revenue_percent_school],
                trg.[revenue_percent_private],
                trg.[revenue_amount_comp],
                trg.[free_screenings_private],
                trg.[per_screening_private]
         FROM [dbo].[LTR_CONTRACT_TRIGGERS] AS trg
              INNER JOIN [#analysis_contract_info] AS con ON con.[contract_id] = trg.[contract_id]
         WHERE trg.[contract_id] = @contract_id
         ORDER BY trg.[trigger_no]


    /**************************************************
        If this is a revenue percentage contract and no dollar amount has been set for extra comp tickets,
        Use the average ticket price as generated above from the attendance matrix.
     **************************************************/

        IF @license_type = @license_revenue OR @license_type = @license_greater
            UPDATE [#analysis_triggers]
            SET [revenue_amount_comp] = @average_tik_Price
            WHERE [revenue_amount_comp] = 0.0

    /**************************************************
        The report needs to know which trigger is the first (the one with the lowest number).
        The contract data appears on every row of the report but we only want it to be displayed
        once.  Telling the report to display that data only from the first trigger was the 
        easiest way to do that.  it also needs to know which trigger number is the last so that it
        can process end dates and statuses appropriately.
     **************************************************/
 
        SELECT @lowest_trigger_no = ISNULL(MIN([trigger_no]), -1),
               @highest_trigger_no = ISNULL(MAX([trigger_no]), -1)        
        FROM [#analysis_triggers]
        UPDATE [#analysis_triggers] SET [first_trigger] = 'Y' WHERE [trigger_no] = @lowest_trigger_no


    /**************************************************
        Calculating the display description for each trigger here makes things easier later.
        The description is basically the range of values and what type they are
            example: 1 to 5000 Viewers or 101 to 200 Shows
        On the final trigger, the end of the range (which is usually set to a rediculously high number)
        is removed and a plus sign is put in its place.
            exmaple:  25,001 + Viewers
     **************************************************/

        UPDATE [#analysis_triggers]
        SET [trigger_description] = FORMAT([trigger_range_low],'#,##0') 
                                  + CASE WHEN [trigger_no] = @highest_trigger_no THEN ' +'
                                         ELSE ' to ' + FORMAT([trigger_range_high],'#,##0') END
                                  + ' ' + [trigger_type]


    /**************************************************
        Like we did for the contract terms percentages, we need to set the
        trigger percentages to decimals so that they can be used to do the math.
            If percentages are > 1 then they need to be divided by 100 
            If they are already <= 1 then leave them alone
        Doing this check here allows users to enter the percentages however they want to
        25% can be entered into the table as 25 or 0.25 and both will work.
     **************************************************/

        UPDATE [#analysis_triggers]
        SET [revenue_percent_public] = ([revenue_percent_public] / 100)
        WHERE [revenue_percent_public] > 1

        UPDATE [#analysis_triggers]
        SET [revenue_percent_school] = ([revenue_percent_school] / 100)
        WHERE [revenue_percent_school] > 1

        UPDATE [#analysis_triggers]
        SET [revenue_percent_private] = ([revenue_percent_private] / 100)
        WHERE [revenue_percent_private] > 1

        UPDATE [#analysis_triggers]
        SET [comp_percentage_allowed] = ([comp_percentage_allowed] / 100)
        WHERE [comp_percentage_allowed] > 1


    /**************************************************
        Using the Attendance Matix that was created earlier and it's running totals, 
        We can determine the date on which we reched the top of that trigger's range.
        It's done slightly differently depending on whether the trigger is based on
        the number of viewers vs the number of shows.  These help establish date
        ranges for each specific trigger.  
     **************************************************/

        UPDATE [#analysis_triggers]
         SET [trigger_end_date] = (SELECT MIN(perf_date) 
                                   FROM [dbo].[LT_HISTORY_ATTENDANCE_MATRIX]
                                   WHERE [contract_id] = @contract_id
                                     AND [tot_sale_total_running] >= [#analysis_triggers].[trigger_range_high])
         FROM [#analysis_triggers]
         WHERE trigger_type = 'Viewers'

        UPDATE [#analysis_triggers]
         SET [trigger_end_date] = (SELECT MIN(perf_date) 
                                   FROM [dbo].[LT_HISTORY_ATTENDANCE_MATRIX]
                                   WHERE [contract_id] = @contract_id
                                     AND [show_count_running] >= [#analysis_triggers].[trigger_range_high])
         FROM [#analysis_triggers]
         WHERE trigger_type = 'Shows'

    /**************************************************
        Set the achieved flag to Yes on any trigger that has an end date.
        This does not include the trigger that is currently in effect because the milestone
        has not been reached yet.
     **************************************************/
      
         UPDATE [#analysis_triggers]
         SET [trigger_achieved] = 'Y',
             [trigger_status] = 'Achieved'
         WHERE [trigger_end_date] IS NOT NULL;


    /**************************************************
        The first trigger without an end date is the trigger currently "in progress"
        The end date of that trigger is the end date of the contract
     **************************************************/
    
         UPDATE [#analysis_triggers]
         SET [trigger_end_date] = @end_dt,
             [trigger_status] = 'Current'
         WHERE [trigger_no] = (SELECT ISNULL(MIN([trigger_no]), 1)
                               FROM [#analysis_triggers] AS trg
                                WHERE trg.[trigger_end_date] IS NULL)

                               
    /**************************************************
        The start date on the first trigger is the start date of the contract
     **************************************************/

        UPDATE [#analysis_triggers]
        SET [trigger_start_date] = @start_dt
        WHERE trigger_no = 1


    /**************************************************    
        The start date on trigger other than the first is the end date of the previous record plus 1 day
        To help calculate this, the [#analysis_triggers] table was created with an identity field and 
        the insert statement has an ORDER BY clause to make sure the records are inserted in the correct
        order.  This makes it easier to identify which record in the table is the one before any other
        specific record.
     **************************************************/

        UPDATE trc
        SET trc.[trigger_start_date] = DATEADD(DAY,1,trp.[trigger_end_date])
        FROM [#analysis_triggers] AS trc
             LEFT OUTER JOIN [#analysis_triggers] AS trp ON trp.[rec_no] = (trc.[rec_no] - 1)
        WHERE trc.[trigger_end_date] IS NOT NULL AND trc.[trigger_start_date] IS NULL


    /**************************************************
        Using the Attendance Matix that was created earlier and it's running totals, 
        We can determine the total number of shows that occured with the specific
        date ranges of each trigger.
     **************************************************/

        UPDATE [#analysis_triggers]
        SET [show_count] = (SELECT ISNULL(SUM([show_count]), 0)
                            FROM [dbo].[LT_HISTORY_ATTENDANCE_MATRIX]
                            WHERE [contract_id] = @contract_id
                              AND [trigger_no] = [#analysis_triggers].[trigger_no])
                              --AND [perf_date] between [#analysis_triggers].[trigger_start_date]
                              --                    AND [#analysis_triggers].[trigger_end_date])

    /**************************************************
        Using the Attendance Matix that was created earlier and it's running totals, 
        We can calculate the comp attendance, as well as the public attendance and revenue.
     **************************************************/

--SELECT [trigger_no], [show_count], [public_attendance], [school_attendance], [buyout_attendance], [total_attendance] FROM [#analysis_triggers];

        UPDATE [#analysis_triggers]
        SET [comp_attendance] = (SELECT SUM(mtx.[cmp_sale_total])
                                FROM [dbo].[LT_HISTORY_ATTENDANCE_MATRIX] AS mtx
                                WHERE mtx.[contract_id] = @contract_id
                                  AND mtx.[trigger_no] = [#analysis_triggers].[trigger_no]),
                                   
            [public_attendance] =  (SELECT SUM(mtx.[pub_sale_total])
                                    FROM [dbo].[LT_HISTORY_ATTENDANCE_MATRIX] AS mtx
                                    WHERE mtx.[contract_id] = @contract_id
                                      AND mtx.[trigger_no] = [#analysis_triggers].[trigger_no]),

                                     
            [public_revenue] = (SELECT SUM(mtx.[pub_paid_amount])
                                FROM [dbo].[LT_HISTORY_ATTENDANCE_MATRIX] AS mtx
                                WHERE mtx.[contract_id] = @contract_id
                                  AND mtx.[trigger_no] = [#analysis_triggers].[trigger_no])

        FROM [#analysis_triggers]
        WHERE [trigger_start_date] IS NOT NULL  

    /**************************************************
        Using the Attendance Matix that was created earlier and it's running totals, 
        We can calculate the school attendance and revenue.
      **************************************************/

        UPDATE [#analysis_triggers]
        SET [school_attendance] = (SELECT SUM([sch_sale_total])
                                   FROM [dbo].[LT_HISTORY_ATTENDANCE_MATRIX]
                                   WHERE [contract_id] = @contract_id
                                     AND [trigger_no] = [#analysis_triggers].[trigger_no]),

            [school_revenue] = (SELECT SUM([sch_paid_amount])
                                FROM [dbo].[LT_HISTORY_ATTENDANCE_MATRIX]
                                WHERE [contract_id] = @contract_id
                                  AND [trigger_no] = [#analysis_triggers].[trigger_no])
        FROM [#analysis_triggers]
        WHERE [trigger_start_date] IS NOT NULL  


    /**************************************************
        Using the Attendance Matix that was created earlier and it's running totals, 
        We can calculate the buyout attendance and revenue.
      **************************************************/

        UPDATE [#analysis_triggers]
        SET [buyout_attendance] = (SELECT SUM(ISNULL([buy_sale_total],0))
                                   FROM [dbo].[LT_HISTORY_ATTENDANCE_MATRIX]
                                   WHERE [contract_id] = @contract_id
                                     AND [trigger_no] = [#analysis_triggers].[trigger_no]),

            [buyout_revenue] = (SELECT SUM(ISNULL([buy_paid_amount],0))
                                FROM [dbo].[LT_HISTORY_ATTENDANCE_MATRIX]
                                WHERE [contract_id] = @contract_id
                                  AND [trigger_no] = [#analysis_triggers].[trigger_no])
        FROM [#analysis_triggers]
        WHERE [trigger_start_date] IS NOT NULL  



    /**************************************************
        Add Public, School, and Buyout attendance and revenue together to get total attendance and revenue
     **************************************************/
    
        UPDATE [#analysis_triggers]
        SET [Total_attendance] = ([public_attendance] + [school_attendance] + [buyout_attendance]),
            [total_revenue] = ([public_revenue] + [school_revenue] + [buyout_revenue])


    /**************************************************
        Determine the percent of comp tickets in each of the date ranges.
        Allowed comps takes the total attendance and multiplies it by the percent allowed.
        The actual comp percentage is the comp attendance divided by the total attedance.
    **************************************************/
    
        UPDATE [#analysis_triggers]
        SET [comp_allowed] = (CAST(total_attendance AS DECIMAL(18,4)) * [comp_percentage_allowed])

        UPDATE [#analysis_triggers]
        SET [comp_attendance_percentage] = (CAST([comp_attendance] AS DECIMAL(18,4)) / CAST([total_attendance] AS DECIMAL(18,4)))
        WHERE [Total_attendance] <> 0

        UPDATE [#analysis_triggers]
        SET [comp_difference] = ([comp_attendance] - [comp_allowed])

    /**************************************************
        Calculate what is owed by the museum based on the contract terms of each trigger
        Both the per_admission and the revenue rates are calculated because one of the license types
        is the greater of the two.  After the values are calculated, one is chosen based on
        what the license type is for the contract.
     **************************************************/
       
       UPDATE [#analysis_triggers]
       SET [public_owed_rev] = ([public_revenue] * [revenue_percent_public]),
           [school_owed_rev] = ([school_revenue] * [revenue_percent_school]),
           [buyout_owed_rev] = ([buyout_revenue] * [revenue_percent_private]),
           [comp_owed_rev] = CASE WHEN [comp_difference] < 0 THEN 0.0 
                                  ELSE (([comp_difference] * [revenue_amount_comp]) * [revenue_percent_public]) END
       
        UPDATE [#analysis_triggers]
        SET [public_owed_per] = CAST(([public_attendance] - [comp_attendance]) AS DECIMAL(18,2)) * [per_admission_public],
            [school_owed_per] = CAST([school_attendance] AS DECIMAL(18,2)) * [per_admission_school],
            [buyout_owed_per] = CAST([buyout_attendance] AS DECIMAL(18,2)) * [per_admission_private],
            [comp_owed_per] = CASE WHEN [comp_difference] <= 0 THEN 0.0 
                                   ELSE ([comp_difference] * [per_admission_comp]) END

       
        IF @license_type = @license_greater 

            UPDATE [#analysis_triggers]
            SET [public_owed] = CASE WHEN [public_owed_rev] > [public_owed_per] THEN [public_owed_rev]
                                     ELSE [public_owed_per] END,
                [school_owed] = CASE WHEN [school_owed_rev] > [school_owed_per] THEN [school_owed_rev]
                                     ELSE [school_owed_per] END,
                [buyout_owed] = CASE WHEN [buyout_owed_rev] > [buyout_owed_per] THEN [buyout_owed_rev]
                                     ELSE [buyout_owed_per] END,
                [comp_owed] =  [comp_owed_per]
        
        ELSE IF @license_type = @license_revenue 
        
            UPDATE [#analysis_triggers]
            SET [public_owed] = [public_owed_rev],
                [school_owed] = [school_owed_rev],
                [buyout_owed] = [buyout_owed_rev],
                [comp_owed] = [comp_owed_per]
        
        ELSE

            UPDATE [#analysis_triggers]
            SET [public_owed] = [public_owed_per],
                [school_owed] = [school_owed_per],
                [buyout_owed] = [buyout_owed_per],
                [comp_owed] = [comp_owed_per]
            WHERE [total_revenue] <> 0.0

        --/****************************************
        -- DETERMINE MONTHLY COMP ATTENDANCE
        --****************************************/

        INSERT INTO [#analysis_comp] ([comp_year], [comp_month], [comp_month_id], [comp_per_admission_amount], 
                                       [comp_percent_allowed], [month_total_attendance], [month_comp_attendance])
        SELECT [comp_year],
               [comp_month],
               [comp_month_id],
               [comp_per_admission_amount],
               [comp_percent_allowed],
               [month_total_attendance],
               [month_comp_attendance]
        FROM [dbo].[LT_HISTORY_OMNI_CONTRACT_COMPS]
        WHERE [contract_id] = @contract_id
        

        /****************************
        STILL NEED TO FIGURE OUT HOW TO UPDATE ACCORDINGLY FOR COMPS
        *****************************/


    /**************************************************
        Total up the amount owed, then calculate the net difference betweeen
        what came in and what we are paying out.
     **************************************************/

        UPDATE [#analysis_triggers]
        SET [total_owed] = ([public_owed] + [school_owed] + [buyout_owed] + [comp_owed])

        UPDATE [#analysis_triggers]
        SET [public_net] = ([public_revenue] - [public_owed] - [comp_owed]),
            [school_net] = ([school_revenue] - [school_owed]),
            [buyout_net] = ([buyout_revenue] - [buyout_owed]),
            [total_net] = ([total_revenue] - [total_owed])

    FINISHED:            
       
       /**************************************************
            Select final data set.  
            There will be one row for each trigger.
            Each row will have the data for the contract in it, but one should be flagged
            as the First Trigger so that the report will know to only show the contract
            data once (from the first trigger).
        **************************************************/

        DELETE FROM [dbo].[LT_HISTORY_OMNI_CONTRACT] WHERE contract_id = @contract_id

        INSERT INTO [dbo].[LT_HISTORY_OMNI_CONTRACT] ([contract_id], [contract_name], [vendor_no], [vendor_name], [vendor_address_1], [vendor_Address_2], [vendor_city],
                            [vendor_state], [vendor_zip_code], [public_prod_no], [public_prod_name], [school_prod_no], [school_prod_name], [reporting_type], [license_type],
                            [license_type_name], [start_dt], [end_dt], [contract_comp_percentage], [minmum_shows], [minimum_shows_achieved], [attendance_guarantee],
                            [attendance_guarantee_achieved], [schedule_guarantee_percentage], [schedule_guarantee_days], [schedule_actual_percentage], [schedule_guarantee_achieved],
                            [total_contract_payments], [first_trigger], [trigger_no], [trigger_type], [trigger_range_low], [trigger_range_high], [trigger_description],
                            [trigger_start_date], [trigger_end_date], [trigger_achieved], [trigger_status], [show_count], [comp_attendance], [comp_attendance_percentage], 
                            [comp_difference], [public_attendance], [public_revenue], [public_owed], [public_net], [school_attendance], [school_revenue], [school_owed],
                            [school_net], [buyout_attendance], [buyout_revenue], [buyout_owed], [buyout_net], [comp_owed], [total_attendance], [total_revenue], [total_owed],
                            [total_net], [comp_percentage_allowed], [comp_allowed], [per_admission_public], [per_admission_school], [per_admission_private], [per_admission_comp],
                            [revenue_percent_public], [revenue_percent_school], [revenue_percent_private], [revenue_amount_comp], [free_screenings_private], [per_screening_private])
           SELECT con.[contract_id],
                  con.[contract_name],
                  con.[vendor_no],
                  con.[vendor_name],
                  con.[vendor_address_1],
                  con.[vendor_Address_2],
                  con.[vendor_city],
                  con.[vendor_state],
                  con.[vendor_zip_code],
                  con.[public_prod_no],
                  con.[public_prod_name],
                  con.[school_prod_no],
                  con.[school_prod_name],
                  con.[reporting_type],
                  con.[license_type],
                  con.[license_type_name],
                  con.[start_dt],
                  con.[end_dt],
                  con.[contract_comp_percentage],
                  con.[minmum_shows],
                  con.[minimum_shows_achieved],
                  con.[attendance_guarantee],
                  con.[attendance_guarantee_achieved],
                  con.[schedule_guarantee_percentage],
                  con.[schedule_guarantee_days],
                  con.[schedule_actual_percentage],
                  con.[schedule_guarantee_achieved],
                  CASE WHEN [trg].[trigger_no] = @lowest_trigger_no THEN con.[total_contract_payments]
                       ELSE 0.0 END AS [total_contract_payments],
                  trg.[first_trigger],
                  trg.[trigger_no],
                  trg.[trigger_type],
                  trg.[trigger_range_low],
                  trg.[trigger_range_high],
                  trg.[trigger_description],
                  trg.[trigger_start_date],
                  CASE WHEN [trg].[trigger_achieved] = 'Y' THEN trg.[trigger_end_date]
                       ELSE NULL END AS [trigger_end_date],
                  trg.[trigger_achieved],
                  trg.[trigger_status],
                  trg.[show_count],
                  trg.[comp_attendance],
                  trg.[comp_attendance_percentage],
                  trg.[comp_difference],
                  trg.[public_attendance],
                  trg.[public_revenue],
                  trg.[public_owed],
                  trg.[public_net],
                  trg.[school_attendance],
                  trg.[school_revenue],
                  trg.[school_owed],
                  trg.[school_net],
                  trg.[buyout_attendance],
                  trg.[buyout_revenue],
                  trg.[buyout_owed],
                  trg.[buyout_net],
                  trg.[comp_owed],
                  trg.[total_attendance],
                  trg.[total_revenue],
                  trg.[total_owed],
                  trg.[total_net],
                  trg.[comp_percentage_allowed],
                  trg.[comp_allowed],
                  trg.[per_admission_public], 
                  trg.[per_admission_school],
                  trg.[per_admission_private],
                  trg.[per_admission_comp],
                  trg.[revenue_percent_public],
                  trg.[revenue_percent_school],
                  trg.[revenue_percent_private],
                  trg.[revenue_amount_comp],
                  trg.[free_screenings_private],
                  trg.[per_screening_private]
           FROM [#analysis_contract_info] AS con
                INNER JOIN [#analysis_triggers] AS trg ON trg.[contract_id] = con.[contract_id]
           ORDER BY trg.[trigger_no]

    DONE:

        IF OBJECT_ID('tempdb..#analysis_contract_info') is not null DROP TABLE [#analysis_contract_info]
        IF OBJECT_ID('tempdb..#analysis_triggers') is not null DROP TABLE [#analysis_triggers]


END
GO

