USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_GROUP_ORDERS_DETAILS]') AND type in (N'P', N'PC')) BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_GROUP_ORDERS_DETAILS] AS' 
    EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LRP_GROUP_ORDERS_DETAILS] TO impusers' 
END
GO

ALTER PROCEDURE [dbo].[LRP_GROUP_ORDERS_DETAILS]
        @report_start_dt DATETIME = NULL,
        @report_end_dt DATETIME = NULL,
        @date_type VARCHAR(25) = 'Visit Date',
        @mos_str VARCHAR(4000) = '',
        @order_categories VARCHAR(4000) = '',
        @exclude_unpaid_past_orders CHAR(1) = 'Y'
WITH RECOMPILE AS BEGIN

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    SET NOCOUNT ON;


    /*  Report Variables and tables  */

        DECLARE @fiscal_year INT = [dbo].[LF_GetFiscalYear](GETDATE());

        DECLARE @order_category_list TABLE  ([order_category] VARCHAR(50));

        DECLARE @title_list TABLE ([title_no] INT, [title_name] VARCHAR(30));

        DECLARE @mos_list TABLE ([mos_no] INT, [mos_name] VARCHAR(30))

        IF OBJECT_ID('tempdb..#order_list') IS NOT NULL DROP TABLE [#order_list];
            
                CREATE TABLE [#order_list] ([order_no] INT NOT NULL DEFAULT(0), 
                                            [order_dt] DATETIME NULL,
                                            [customer_no] INT NOT NULL DEFAULT(0), 
                                            [customer_type] INT NOT NULL DEFAULT(0), 
                                            [customer_type_name] VARCHAR(30) NOT NULL DEFAULT(''), 
                                            [order_category] INT NOT NULL DEFAULT(0), 
                                            [order_category_name] VARCHAR(30) NOT NULL DEFAULT(''), 
                                            [time_flag] VARCHAR(20) NOT NULL DEFAULT(''), 
                                            [create_dt] DATETIME NULL, 
                                            [mode_of_sale] INT NOT NULL DEFAULT(0),
                                            [mode_of_sale_name] VARCHAR(30) NOT NULL DEFAULT(''),
                                            [perf_dt] DATETIME NULL,
                                            [amount_due] DECIMAL(18,2) NOT NULL DEFAULT(0.0), 
                                            [amount_paid] DECIMAL(18,2) NOT NULL DEFAULT(0.0));

                CREATE UNIQUE CLUSTERED INDEX [ix_order_list_order_no_perf_dt] ON [#order_list] ([order_no] ASC, [perf_dt] ASC) ON [PRIMARY];

        IF OBJECT_ID('tempdb..#order_info') IS NOT NULL DROP TABLE [#order_info];
            
                CREATE TABLE [#order_info] ([order_no] INT NOT NULL DEFAULT(0), 
                                            [perf_dt] DATETIME NULL,
                                            [exhibits] INT NOT NULL DEFAULT(0), 
                                            [omni] INT NOT NULL DEFAULT(0), 
                                            [planetarium] INT NOT NULL DEFAULT(0), 
                                            [4d] INT NOT NULL DEFAULT(0), 
                                            [butterfly] INT NOT NULL DEFAULT(0),
                                            [special] INT NOT NULL DEFAULT(0));
                                             
                CREATE UNIQUE CLUSTERED INDEX [ix_order_info_order_no_perf_dt] ON [#order_info] ([order_no] ASC, [perf_dt] ASC) ON [PRIMARY];


    /*  Check Parameters - If null is passed to either date, use start and/or end of current fiscal year  */

        IF @report_start_dt IS NULL or @report_end_dt IS NULL BEGIN

            SELECT @report_start_dt = ISNULL(@report_start_dt,[dbo].[LF_GetFiscalYearStartDt](@fiscal_year));

            SELECT @report_end_dt = ISNULL(@report_end_dt,[dbo].[LF_GetFiscalYearEndDt](@fiscal_year));

        END;

        --SET START DATE TO 0:00 and end date to 23:59
        SELECT @report_start_dt = CAST(@report_start_dt AS DATE),
               @report_end_dt = CONVERT(CHAR(10),@report_end_dt,111) + ' 23:59:59.957';

        IF ISNULL(@order_categories,'') = ''
            INSERT INTO @order_category_list ([order_category]) 
            SELECT [description] 
            FROM [dbo].[TR_GOOESOFT_DROPDOWN] 
            WHERE [code] = '1494' AND [inactive] = 'N'  --code 1494 is the code that gets categories       
        ELSE
            INSERT INTO @order_category_list ([order_category]) 
            SELECT [item] 
            FROM [dbo].[LFT_SPLIT_DILEMETED_STRING] (REPLACE(@order_categories,'"',''), ',');

        IF ISNULL(@mos_str,'') = ''
            INSERT INTO @mos_list ([mos_no],[mos_name])
            SELECT [id],
                   [description]
            FROM [dbo].[TR_MOS]
        ELSE
            INSERT INTO @mos_list ([mos_no],[mos_name])
            SELECT CAST(lst.[element] AS INT), 
                   mos.[description]
            FROM [dbo].[FT_SPLIT_LIST](REPLACE(@mos_str,'"',''),',') AS lst
                 INNER JOIN [dbo].[TR_MOS] AS mos ON mos.[id] = CAST(lst.[Element] AS INT);


        SELECT @exclude_unpaid_past_orders = ISNULL(@exclude_unpaid_past_orders,'Y')

    /*  Create list of titles to look at --  if additional titles are needed, they have to be added here */

        INSERT INTO @title_list ([title_no], [title_name])
        SELECT [inv_no], 
               [description]
        FROM [dbo].[T_INVENTORY]
        WHERE [type] = 'T' 
          AND [description] IN ('Exhibit Halls', 'Mugar Omni Theater', 'Hayden Planetarium', 
                                '4-D Theater', 'Butterfly Garden', 'Special Exhibitions');

    /*  Pull data when based on *ORDER* date  */

        IF @date_type = 'Order Date' BEGIN

            --Get a list of all orders and information about each
            WITH CTE_PERF_NO ([perf_no], [perf_dt])
            AS (
                SELECT DISTINCT prf.[performance_no], 
                                prf.[performance_dt]
                FROM dbo.T_SUB_LINEITEM AS sli
                     INNER JOIN dbo.T_ORDER AS ord ON ord.order_no = sli.order_no
                     INNER JOIN dbo.LV_PRODUCTION_ELEMENTS_PERFORMANCE AS prf ON prf.[performance_no] = sli.[perf_no] 
                                                                             AND prf.[performance_zone] = sli.[zone_no]
                     INNER JOIN @title_list AS ttl ON ttl.[title_no] = prf.[title_no]
                WHERE ord.[order_dt] BETWEEN @report_start_dt AND @report_end_dt
               )
                    INSERT INTO [#order_list] ([order_no], [order_dt], [customer_no], [customer_type], [customer_type_name], [order_category], 
                                               [order_category_name], [create_dt], [mode_of_sale], [mode_of_sale_name], [perf_dt], [amount_due], [amount_paid])
                    SELECT  sli.[order_no], 
                            ord.[order_dt],
                            ord.[customer_no], 
                            cus.[cust_type], 
                            typ.[description], 
                            ord.[class], 
                            cat.[description], 
                            ord.[create_dt], 
                            ord.[MOS],
                            mos.[description],
                            prf.[perf_dt],
                            SUM(sli.[due_amt]),
                            SUM(sli.[paid_amt])
                    FROM [dbo].[T_SUB_LINEITEM] AS sli
                         INNER JOIN [dbo].[T_ORDER] AS ord ON ord.[order_no] = sli.[order_no]
                         INNER JOIN @mos_list AS msl ON msl.[mos_no] = ord.[mos]
                         INNER JOIN [dbo].[T_CUSTOMER] AS cus ON cus.[customer_no] = ord.[customer_no]
                         INNER JOIN [dbo].[TR_CUST_TYPE] AS typ ON typ.[id] = cus.[cust_type]
                         INNER JOIN [dbo].[TR_ORDER_CATEGORY] AS cat ON cat.[id] = ord.[class]
                         INNER JOIN @order_category_list AS cls ON cls.[order_category] = cat.[description]
                         INNER JOIN [dbo].[TR_MOS] AS mos ON mos.[id] = ord.[MOS]
                         INNER JOIN [CTE_PERF_NO] AS prf ON prf.[perf_no] = sli.perf_no
                    WHERE ord.[order_dt] BETWEEN @report_start_dt AND @report_end_dt 
                      AND sli.[sli_status] IN (2, 3, 12)
                    GROUP BY sli.[order_no], ord.[order_dt], ord.[customer_no],  cus.[cust_type],  typ.[description],  ord.[class], 
                             cat.[description], ord.[create_dt], ord.[MOS], mos.[description], prf.[perf_dt]

        END 
        
    /*  Pull data when based on *VISIT* date  */
        
        ELSE BEGIN

            WITH CTE_PERF_NO ([perf_no], [perf_dt])
            AS (SELECT DISTINCT prf.[performance_no], 
                                prf.[performance_dt] 
                FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] AS prf
                WHERE [performance_dt] BETWEEN @report_start_dt AND @report_end_dt
                  AND [prf].[title_no] IN (SELECT [title_no] FROM @title_list)
                )
                    INSERT INTO [#order_list] ([order_no], [order_dt], [customer_no], [customer_type], [customer_type_name], [order_category], 
                                               [order_category_name], [create_dt], [mode_of_sale], [mode_of_sale_name], [perf_dt], [amount_due], [amount_paid])
                    SELECT  sli.[order_no], 
                            ord.[order_dt],
                            ord.[customer_no], 
                            cus.[cust_type], 
                            typ.[description], 
                            ord.[class], 
                            cat.[description], 
                            ord.[create_dt], 
                            ord.[MOS],
                            mos.[description],
                            prf.[perf_dt],
                            SUM(sli.[due_amt]), -- ord.[tot_due_amt], 
                            SUM(sli.[paid_amt]) -- ord.[tot_paid_amt]
                    FROM [dbo].[T_SUB_LINEITEM] AS sli
                         INNER JOIN CTE_PERF_NO AS prf ON prf.[perf_no] = sli.[perf_no]
                         INNER JOIN [dbo].[T_ORDER] AS ord ON ord.[order_no] = sli.[order_no]
                         INNER JOIN @mos_list AS msl ON msl.[mos_no] = ord.[mos]
                         INNER JOIN [dbo].[T_CUSTOMER] AS cus ON cus.[customer_no] = ord.[customer_no]
                         INNER JOIN [dbo].[TR_CUST_TYPE] AS typ ON typ.[id] = cus.[cust_type]
                         INNER JOIN [dbo].[TR_ORDER_CATEGORY] AS cat ON cat.[id] = ord.[class]
                         INNER JOIN @order_category_list AS cls ON cls.[order_category] = cat.[description]
                         INNER JOIN [dbo].[TR_MOS] AS mos ON mos.[id] = ord.[MOS]
                    WHERE sli.[sli_status] IN (2, 3, 12)
                    GROUP BY sli.[order_no], ord.[order_dt], ord.[customer_no],  cus.[cust_type],  typ.[description], ord.[class], 
                             cat.[description], ord.[create_dt], ord.[MOS],mos.[description], prf.[perf_dt];

        END;
            
    /*  Get a list of how many tickets to each venue for each order  */

        WITH CTE_PERF_NO ([perf_no])
        AS (SELECT DISTINCT prf.[performance_no] 
            FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] AS prf
                 INNER JOIN @title_list AS ttl ON ttl.[title_no] = prf.[title_no]
            WHERE [performance_dt] BETWEEN @report_start_dt AND @report_end_dt
            )
                INSERT INTO [#order_info] ([order_no], [perf_dt], [exhibits], [omni], [planetarium], [4d], [butterfly], [special])
                SELECT [order_no], 
                       [performance_dt], 
                       [Exhibit Halls], 
                       [Mugar Omni Theater], 
                       [Hayden Planetarium], 
                       [4-D Theater], 
                       [Butterfly Garden],
                       [Special Exhibitions]
                FROM (SELECT sli.[sli_no], 
                             sli.[order_no], 
                             prf.[performance_dt], 
                             prf.[title_name]
                      FROM [dbo].[T_SUB_LINEITEM] AS sli
                           INNER JOIN [#order_list] AS ord ON ord.[order_no] = sli.[order_no]
                           LEFT OUTER JOIN [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] AS prf ON prf.[performance_no] = sli.[perf_no] 
                                                                                     AND prf.[performance_zone] = sli.[zone_no]
                           INNER JOIN CTE_PERF_NO AS ctp ON ctp.[perf_no] = prf.[performance_no]
                      WHERE sli.[sli_status] IN (2, 3, 12)) AS D
                PIVOT (COUNT(sli_no) FOR [title_name] IN ([Exhibit Halls], 
                                                          [Mugar Omni Theater], 
                                                          [Hayden Planetarium], 
                                                          [4-D Theater], 
                                                          [Butterfly Garden], 
                                                          [Special Exhibitions])) AS pvt;


        /*  Delete order information where no tickets were purchased for the disignated venues  */

            DELETE FROM [#order_info] 
            WHERE ([exhibits] + [omni] + [planetarium] + [4d] + [butterfly] + [special]) = 0;
           
           
            IF @exclude_unpaid_past_orders = 'Y' BEGIN

                SET ANSI_WARNINGS OFF;
           

                WITH CTE_ZERO_PAYMENTS ([order_no], [perf_dt], [due_amt], [paid_amt])
                AS (
                    SELECT inf.[order_no], MAX(prf.[performance_date]), SUM(ISNULL(sli.[due_amt],0)), SUM(ISNULL(sli.[paid_amt],0))
                    FROM [#order_info] AS inf
                         LEFT OUTER JOIN [dbo].[T_SUB_LINEITEM] AS sli ON sli.[order_no] = inf.[order_no] AND sli.[sli_status] IN (2, 3, 12)
                         LEFT OUTER JOIN [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] AS prf ON prf.[performance_no] = sli.perf_no AND prf.[performance_zone] = sli.[zone_no]
                    GROUP BY inf.[order_no]
                    HAVING MAX(prf.performance_dt) < GETDATE() 
                       AND SUM(ISNULL(sli.due_amt,0)) > 0.00 
                       AND SUM(ISNULL(sli.paid_amt,0)) = 0.00
                   )
                DELETE inf 
                FROM [#order_info] AS inf
                     INNER JOIN [CTE_ZERO_PAYMENTS] AS zer ON zer.[order_no] = inf.[order_no]
            
            END
 
    FINISHED:

        /*  Get final data set for report  */

            SELECT lst.[customer_no], 
                   LTRIM(ISNULL(nam.[display_name],'')) AS [customer_name],
                   LTRIM(ISNULL(nam.[display_name],'')) + '_' + CAST(lst.[customer_no] AS VARCHAR(50)) AS [customer_id],
                   ISNULL(adr.[city] + ', ' + adr.[state],'') AS [city_state],
                   ISNULL(LEFT(adr.[postal_code],5),'') AS [postal_code],
                   lst.[order_category_name],
                   lst.[mode_of_sale_name],
                   lst.[order_no],
                   lst.[order_dt],
                   inf.[perf_dt],
                   DATEDIFF(DAY,lst.[order_dt],inf.[perf_dt]) AS [advance_days],
                   ISNULL(inf.[exhibits],0) AS [exhibits],
                   ISNULL(inf.[omni],0) AS [omni],
                   ISNULL(inf.[planetarium],0) AS [planetarium],
                   ISNULL(inf.[4d],0) AS [4d],
                   ISNULL(inf.[butterfly],0) AS [butterfly],
                   ISNULL(inf.[special],0) AS [special],
                   lst.[amount_due],
                   lst.[amount_paid],
                   lst.[amount_due] - lst.[amount_paid] AS [balance_due]
            FROM [#order_list] AS lst
                 LEFT OUTER JOIN [dbo].[FT_CONSTITUENT_DISPLAY_NAME]() AS nam ON nam.[customer_no] =  lst.[customer_no]
                 LEFT OUTER JOIN [dbo].[T_ADDRESS] AS adr ON adr.[customer_no] = lst.[customer_no] 
                                                         AND adr.[primary_ind] = 'Y'
                 INNER JOIN [#order_info] AS inf ON inf.[order_no] = lst.[order_no] 
                                                AND inf.[perf_dt] = lst.[perf_dt]

    DONE:

END
GO

    --EXECUTE [dbo].[LRP_GROUP_ORDERS_DETAILS] @report_start_dt = '7-1-2018', @report_end_dt = '6-30-2019', @date_type = 'Visit Date', @mos_str = '', @order_categories = ''
    --EXECUTE [dbo].[LRP_GROUP_ORDERS_DETAILS] @report_start_dt = '10-1-2018', @report_end_dt = '11-30-2018', @date_type = 'Visit Date', @mos_str = '', @order_categories = '', @exclude_unpaid_past_orders = 'N'
