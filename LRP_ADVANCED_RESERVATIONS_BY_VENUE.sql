USE [impresario]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*  4/12/2019 - The Advanced Reservations by Venue and the Overnight Schedule By Date reports both use this procedure.
                I made some significant changes to accomodate a change request on the Overnight Schedule report.  These
                particular changes should not adversely affect the Advanced Reservations report but at some point 
                (particularly if more changes come in for one and not the other) the procedures should probably be 
                separated so that each report has its own. */

ALTER PROCEDURE [dbo].[LRP_ADVANCED_RESERVATIONS_BY_VENUE]
(
	@order_start_dt DATETIME = NULL,
	@order_end_dt DATETIME = NULL,
	@visit_start_dt DATETIME = NULL,
	@visit_end_dt DATETIME = NULL, 
	@title_str VARCHAR(MAX),
    @mos_str VARCHAR(4000),
	@include_disabled_zones CHAR(1) = 'N',
	@list_no INT = 0
)
AS BEGIN

    -- 2 reports use this SP: ADVANCED RESERVATIONS BY VENUE and OVERNIGHT SCHEDULE BY DATE

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    SET NOCOUNT ON;

    /*  Procedure Variables and tables  */

        DECLARE @overnight_only CHAR(1) = 'N';

        DECLARE @title TABLE (title_no INT);
        DECLARE @mos TABLE ([mos_no] INT);

        DECLARE @overnight_title_no INT = 61;
        DECLARE @member_event_title_no INT = 7051;
        DECLARE @member_overnight_prod_no INT = 7173;

                /*  MS 4-12-2019 - Changed table variable to temp table because of the number of rows it needs to store.
                    When I added the CTE to the final select statement below, the performance with the table
                    variable went way down (taking minutes instead of seconds).  Once it was changed to a 
                    table variable, the run time for a month dropped from 2 or 3 minutes to 14 seconds.   */

                /*  MS 9-26-2019 - added order_category to table  */

                CREATE TABLE #results ([title_no] INT NOT NULL DEFAULT (0),
	                           [title_name] VARCHAR(30) NOT NULL DEFAULT(''),
    	                       [order_no] INT NOT NULL DEFAULT (0),
                               [order_mos] INT NOT NULL DEFAULT (0),
                               [order_category] VARCHAR(30) NOT NULL DEFAULT(''),
                               [customer_no] INT NOT NULL DEFAULT (0), 
	                           [consituentName] VARCHAR(100) NOT NULL DEFAULT (''),
	                           [sort_name] VARCHAR(55) NOT NULL DEFAULT (''),
	                           [city] VARCHAR(30) NOT NULL DEFAULT (''),
	                           [state] VARCHAR(20) NOT NULL DEFAULT (''),
                               [postal_code] VARCHAR(25) NOT NULL DEFAULT (''),
	                           [initiator_cust_no] INT NOT NULL DEFAULT (0), 
	                           [initiatorName] VARCHAR(100) NOT NULL DEFAULT (''),
	                           [grades] VARCHAR(255) NOT NULL DEFAULT (''),
	                           [arrivalTime] VARCHAR(30) NOT NULL DEFAULT (''),
	                           [production_name] VARCHAR(30) NOT NULL DEFAULT (''),
	                               [visitDate] DATETIME NULL,
	                           [price_type_name] VARCHAR(30) NOT NULL DEFAULT (0),
	                           [order_notes] VARCHAR(255) NOT NULL DEFAULT (''),
	                           [zone_enabled] CHAR(1) NOT NULL DEFAULT (''),
	                           [cnt] INT NOT NULL DEFAULT (0));

    /*  Check Variables  */

        --If all four date parameters are null send to the end.  No dates entered will overwhelm the database
        IF (@order_start_dt IS NULL AND @order_end_dt IS NULL AND @visit_start_dt IS NULL and @visit_end_dt IS NULL)
	        GOTO FINISHED 

        IF @order_start_dt IS NOT NULL SELECT @order_start_dt = CAST(@order_start_dt AS DATE)
        IF @order_end_dt IS NOT NULL SELECT @order_end_dt = CONVERT(CHAR(10),@order_end_dt,111) + ' 23:59:59.957'

        IF @visit_start_dt IS NOT NULL SELECT @visit_start_dt = CAST(@visit_start_dt AS DATE)
        IF @visit_end_dt IS NOT NULL SELECT @visit_end_dt = CONVERT(CHAR(10),@visit_end_dt,111) + ' 23:59:59.957'


        IF ISNULL(@title_str,'') IN ('61','"61"')
            SELECT @overnight_only = 'Y'

        IF ISNULL(@title_str,'') <> ''
	        INSERT INTO @title ([title_no])
    	    SELECT CONVERT(int,[Element]) FROM [dbo].[FT_SPLIT_LIST](REPLACE(@title_str,'"',''),',')
        ELSE
	        INSERT INTO @title ([title_no])
	        SELECT [inv_no] FROM [dbo].[T_INVENTORY] WHERE type = 'T'  --T=Title

        IF ISNULL(@mos_str,'') <> ''
	        INSERT INTO @mos ([mos_no])
	        SELECT CONVERT(int,[Element]) FROM [dbo].[FT_SPLIT_LIST](REPLACE(@mos_str,'"',''),',')
        ELSE
	        INSERT INTO @mos ([mos_no])
	        SELECT [id] FROM [dbo].[TR_MOS]

    /*  Generate results data  */

        INSERT INTO #results ([title_no], [title_name], [order_no], [order_mos], [order_category], [customer_no], [consituentName], [sort_name], 
                              [city], [state], [postal_code], [initiator_cust_no], [initiatorName], [grades], [arrivalTime], 
                              [production_name], [visitDate], [price_type_name], [order_notes], [zone_enabled], [cnt])
        SELECT ISNULL(inv.[inv_no],0),
               ISNULL(inv.[description],''),
	           ISNULL(ord.[order_no],0),
               ISNULL(ord.[mos], 0),
	           ISNULL(cat.[description],''),
               ISNULL(ord.[customer_no],0),
               ISNULL(cust.[display_name],''),
	           ISNULL(cust.[sort_name],''),
	           ISNULL(addr.[city],''),
	           ISNULL(addr.[state],''),
               ISNULL(addr.[postal_code],''),
	           isnull(ord.[initiator_no],0),
	           ISNULL(initi.[display_name], ''), 
	           ISNULL(ord.[custom_5], ''), 
	           ISNULL(SUBSTRING(LTRIM(RTRIM(ord.[custom_3])),1,30),''),
	           ISNULL(inv2.[description],''),
	           CASE WHEN pty.[description] IN ('Drop-In Activity', 'Live Presentation') THEN p.[perf_dt]
                    WHEN ISDATE(Z.[description]) = 0 THEN p.[perf_dt]
                    ELSE DATEADD(dd, DATEDIFF(dd, 0, p.perf_dt), 0) + CAST(Z.[description] AS DATETIME) END,
	           ISNULL(pty.[description],''),
	           ISNULL(ord.[notes],''),
	           ISNULL(pp.start_enabled,''),
	           ISNULL(COUNT(*),0) AS cnt
        FROM [dbo].[T_ORDER] AS ord
                INNER JOIN @mos AS mos ON mos.[mos_no] = ord.[MOS]

                INNER JOIN [dbo].[T_SUB_LINEITEM] AS sli 
                           ON ord.[order_no] = sli.[order_no]

                INNER JOIN [dbo].[t_sli_detail] AS slidet 
                           ON sli.[sli_no] = slidet.[sli_no]

                INNER JOIN [dbo].[T_PERF] AS p 
                           ON p.[perf_no] = sli.[perf_no]

                LEFT JOIN [dbo].[T_PERF_PRICE_TYPE] AS PPT 
                          ON PPT.[id] = slidet.[pmap_no] 
                         AND PPT.[perf_no] = p.[perf_no]

                LEFT JOIN [dbo].[T_PERF_PRICE] AS pp 
                          ON pp.[perf_no] = p.[perf_no] 
                         AND pp.[zone_no] = sli.[zone_no] 
                         AND pp.[perf_price_type] = PPT.[id]

                LEFT JOIN [dbo].[TR_PRICE_TYPE] as pty 
                          ON pty.[id] = sli.[price_type]
                         AND pty.[inactive] = 'N'

                LEFT JOIN [dbo].[T_PROD_SEASON] AS psea 
                          ON p.[prod_season_no] = psea.[prod_season_no] 

                LEFT JOIN [dbo].[T_PRODUCTION] AS prod 
                          ON prod.[prod_no] = psea.[prod_no] 

                LEFT JOIN [dbo].[T_INVENTORY] AS inv 
                          ON inv.[inv_no] = prod.[title_no] 
                         AND inv.[type] = 'T' -- Title

                INNER JOIN @title AS tlist 
                           ON inv.[inv_no] = tlist.[title_no]

                LEFT JOIN [dbo].[T_INVENTORY] AS inv2 
                          ON inv2.[inv_no] = prod.[prod_no] 
                         AND inv2.[type] = 'P' -- Production

                LEFT JOIN [dbo].[T_ZMAP] AS ZM 
                          ON P.[zmap_no] = ZM.[zmap_no]

                LEFT JOIN [dbo].[T_ZONE] AS Z 
                          ON SLI.[zone_no] = Z.[zone_no] 
                         AND ZM.[zmap_no] = Z.[zmap_no]

                LEFT JOIN [dbo].FT_CONSTITUENT_DISPLAY_NAME() AS cust 
                          ON ord.[customer_no] = cust.[customer_no]

                LEFT JOIN [dbo].[T_ADDRESS] AS addr 
                          ON addr.[customer_no] = ord.[customer_no] 
                         AND addr.[primary_ind] = 'Y' 

                LEFT JOIN [dbo].[FT_CONSTITUENT_DISPLAY_NAME]() AS initi 
                          ON ord.[initiator_no] = initi.[customer_no]

                LEFT JOIN [dbo].[TR_ORDER_CATEGORY] AS cat 
                          ON cat.[id] = ord.[class]

        WHERE ord.[order_dt] BETWEEN ISNULL(@order_start_dt, '1900-1-1') and ISNULL(@order_end_dt, '2999-12-31')
	      AND p.[perf_dt] between ISNULL(@visit_start_dt, '1900-1-1') and ISNULL(@visit_end_dt, '2999-12-31')
	      AND sli.sli_status IN (2,3,11,12) -- Seated, Unpaid|Seated, Paid|Upgraded|Ticketed, Paid
        GROUP BY inv.[inv_no], inv.[description], ord.[order_no], ord.[MOS], ISNULL(cat.[description],''), ord.[customer_no], cust.[display_name], cust.[sort_name], 
	             addr.[city], addr.[state], addr.[postal_code],ord.[initiator_no], ISNULL(initi.display_name, ''), ISNULL(ord.custom_5, ''), 
                 ISNULL(SUBSTRING(LTRIM(RTRIM(ord.custom_3)),1,30),''), inv2.[description], p.perf_dt, Z.[description],pty.[description],ISNULL(ord.notes,''),
	             pp.start_enabled;

    /*  If Overnight Only, Check for Member Overnights in the Member Only Events title  */

    IF @overnight_only = 'Y' BEGIN

        INSERT INTO #results ([title_no], [title_name], [consituentName], [sort_name], [production_name], [visitDate], [zone_enabled], [cnt])
        SELECT @overnight_title_no, 
               inv.[description], 
	           'Museum Members', 
	           'Museum Members', 
	           inv2.[description], 
	           p.[perf_dt],
               'Y',
	           COUNT(*) AS cnt
        FROM [dbo].[T_ORDER] AS ord
             INNER JOIN @mos AS mos ON mos.[mos_no] = ord.[MOS]
             INNER JOIN [dbo].[T_SUB_LINEITEM] AS sli ON ord.[order_no] = sli.[order_no]
             INNER JOIN [dbo].[t_sli_detail] AS slidet ON sli.[sli_no] = slidet.[sli_no]
             INNER JOIN [dbo].[T_PERF] AS p ON p.[perf_no] = sli.[perf_no]
             LEFT JOIN [dbo].[T_PROD_SEASON] AS psea ON p.[prod_season_no] = psea.[prod_season_no] 
             LEFT JOIN [dbo].[T_PRODUCTION] AS prod ON prod.[prod_no] = psea.[prod_no] 
             LEFT JOIN [dbo].[T_INVENTORY] AS inv ON inv.[inv_no] = prod.[title_no] AND inv.[type] = 'T' -- Title
             LEFT JOIN [dbo].[T_INVENTORY] AS inv2 ON inv2.[inv_no] = prod.[prod_no] AND inv2.[type] = 'P' -- Production
        WHERE ord.[order_dt] BETWEEN ISNULL(@order_start_dt, '1900-1-1') and ISNULL(@order_end_dt, '2999-12-31')
	      AND p.[perf_dt] between ISNULL(@visit_start_dt, '1900-1-1') and ISNULL(@visit_end_dt, '2999-12-31')
	      AND sli.sli_status IN (2,3,11,12) -- Seated, Unpaid|Seated, Paid|Upgraded|Ticketed, Paid
          AND inv.[inv_no] = @member_event_title_no
          AND prod.[prod_no] = @member_overnight_prod_no
        GROUP BY inv.[description], inv2.[description], p.perf_dt;

    END


    /*  Changed this to simplify the final statement
        Took out the join and perform a delete from the temp table instead  */

        IF @list_no > 0
            DELETE FROM #results
            WHERE [customer_no] NOT IN (SELECT [customer_no] FROM [dbo].[T_LIST_CONTENTS]
                                        WHERE list_no = @list_no);

    /*  If this is an overnight report only, the price type doesn't matter and leaving it in was causing issues with the
        export on orders with multiple price types.  An overnight order is defined as a report run for only the overnight
        title (#61).  If that is the case, the price type name is changed to Visitor  */

        IF @overnight_only = 'Y'
            UPDATE #results
            SET price_type_name = 'Visitor';

    FINISHED:

        /*  Final select changed to a single statement with a grouping to accomodate the Overnight Schedule by Date report  
            Had issues with ON orders with multiple price types producing multiple lines in the csv output.  
            Doing this (and changing the price type name above) fixes that issue when run only for overnights  */
    
        /*  initiator email was added using a CTE.  We've had issues in past with people having more than one primary email.
            This will mitigate that if it happens.  Also prevents having to add yet another join to previous statement.
            If it finds more than one primary email address, it will use the most recent address entered. */
            
        WITH CTE_EmailAddresses (row_no, customer_no, email_address)
        AS (
            SELECT ROW_NUMBER() OVER(PARTITION BY eaddr.[customer_no] ORDER BY eaddr.[create_dt] DESC),
                   eaddr.[customer_no], 
                   eaddr.[address]
            FROM [dbo].[T_EADDRESS] AS eaddr
                 INNER JOIN #results AS res ON res.[initiator_cust_no] = eaddr.[customer_no]
            WHERE eaddr.[inactive] = 'N' and eaddr.[primary_ind] = 'Y'
           )
	    SELECT 
		    r.[title_no],
		    r.[title_name],
		    r.[order_no],
            r.[order_mos],
            r.[order_category],
		    r.[customer_no],
		    r.[consituentName],
		    r.[sort_name], 
		    r.[city],
		    r.[state],
            CASE WHEN LEN(r.[postal_code]) = 9 THEN LEFT(r.[postal_code],5) + '-' + RIGHT(r.[postal_code],4)
                                               ELSE r.[postal_code] END AS [postal_code],
		    r.[initiator_cust_no],
		    r.[initiatorName],
            ISNULL(e.[email_address],'') AS [InitiatorEmail],
		    r.[grades],
		    r.[arrivalTime],
		    r.[production_name],
		    r.[visitDate],
		    r.[price_type_name],
		    r.[order_notes],
		    r.[zone_enabled],
		    SUM(r.[cnt]) AS [cnt]
	    FROM [#results] r
	    INNER JOIN @title t
		           ON t.title_no = r.title_no
        LEFT OUTER JOIN [CTE_EmailAddresses] AS e 
                        ON e.customer_no = r.initiator_cust_no AND e.row_no = 1
	    WHERE (@include_disabled_zones = 'N' AND r.zone_enabled = 'Y')
		    OR @include_disabled_zones = 'Y'
        GROUP BY
            r.[title_no], r.[title_name], r.[order_no], r.[order_mos], r.[order_category], r.[customer_no], r.[consituentName], r.[sort_name],  r.[city], r.[state],
            CASE WHEN LEN(r.[postal_code]) = 9 THEN LEFT(r.[postal_code],5) + '-' + RIGHT(r.[postal_code],4)
                                               ELSE r.[postal_code] END,
		    r.[initiator_cust_no], r.[initiatorName], ISNULL(e.[email_address],''), r.[grades], r.[arrivalTime], r.[production_name], r.[visitDate],
		    r.[price_type_name], r.[order_notes], r.[zone_enabled]; 

END
GO


--EXECUTE [dbo].[LRP_ADVANCED_RESERVATIONS_BY_VENUE]
--    	@order_start_dt = NULL,
--	    @order_end_dt = NULL,
--	    @visit_start_dt = '10-1-2019',
--	    @visit_end_dt = '10-10-2019', 
--	    @title_str = '',
--        @mos_str = '11,12',
--	    @include_disabled_zones = 'N',
--	    @list_no = 0;




