USE impresario;
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_ATTEND_BUDGET_VS_ACTUAL_YTD_WITH_INCREMENTAL]') AND type in (N'P', N'PC')) BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_ATTEND_BUDGET_VS_ACTUAL_YTD_WITH_INCREMENTAL] AS';
    EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LRP_ATTEND_BUDGET_VS_ACTUAL_YTD_WITH_INCREMENTAL] TO impusers';
END;
GO

ALTER PROCEDURE [dbo].[LRP_ATTEND_BUDGET_VS_ACTUAL_YTD_WITH_INCREMENTAL]
        @week_ending DATETIME = NULL
AS BEGIN

    SET NOCOUNT ON;
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

    /*  Procedure Variables  */

        DECLARE @fiscal_year INT = 0;
        DECLARE @week_start_dt DATETIME; 
        DECLARE @week_end_dt DATETIME;
        DECLARE @year_start_dt DATETIME;
        DECLARE @year_end_dt DATETIME;

        DECLARE @prev_fiscal_year INT = 0;
        DECLARE @prev_week_start_dt DATETIME;
        DECLARE @prev_week_end_dt DATETIME; 
        DECLARE @prev_year_start_dt DATETIME;
        DECLARE @prev_year_end_dt DATETIME;
   
        DECLARE @gross_attend_no INT = 0, 
                @incremental_attend_no INT = -1;

        DECLARE @ytd_budget INT = 0, 
                @ytd_budget_base INT = 0, 
                @ytd_budget_incr INT = 0;

        DECLARE @ytd_actual INT = 0, 
                @ytd_actual_base INT = 0, 
                @ytd_actual_incr INT = 0;

        DECLARE @prev_ytd_actual INT = 0, 
                @prev_ytd_actual_base INT = 0, 
                @prev_ytd_actual_incr INT = 0;
        
        DECLARE @num_of_days INT = 0,
                @total_days INT = 365;

        DECLARE @month_data TABLE ([month_id] CHAR(7) NOT NULL DEFAULT (''), 
                                   [month_base] INT NOT NULL DEFAULT (0), 
                                   [month_incr] INT NOT NULL DEFAULT (0),
                                   [month_total] INT NOT NULL DEFAULT (0),
                                   [month_actual_base] INT NOT NULL DEFAULT (0),
                                   [month_actual_incr] INT NOT NULL DEFAULT (0),
                                   [month_actual_total] INT NOT NULL DEFAULT (0),
                                   [prev_month_base] INT NOT NULL DEFAULT (0), 
                                   [prev_month_incr] INT NOT NULL DEFAULT (0),
                                   [prev_month_total] INT NOT NULL DEFAULT (0),
                                   [prev_month_actual_base] INT NOT NULL DEFAULT (0),
                                   [prev_month_actual_incr] INT NOT NULL DEFAULT (0),
                                   [prev_month_actual_total] INT NOT NULL DEFAULT (0))

        DECLARE @ytd_final_table TABLE ([attendance_category] VARCHAR(30) NOT NULL DEFAULT (''),
                                        [attendance_category_sort] CHAR(3) NOT NULL DEFAULT ('ZZZ'),
                                        [fiscal_year] INT NOT NULL DEFAULT (0),
                                        [ytd_budget] INT NOT NULL DEFAULT (0),
                                        [ytd_actual] INT NOT NULL DEFAULT (0),
                                        [ytd_variance] INT NOT NULL DEFAULT (0),
                                        [ytd_percentage] DECIMAL(18,4) NOT NULL DEFAULT (0.0),
                                        [prev_fiscal_year] INT NOT NULL DEFAULT (0),
                                        [prev_ytd_budget] INT NOT NULL DEFAULT (0),
                                        [prev_ytd_actual] INT NOT NULL DEFAULT (0),
                                        [prev_ytd_variance] INT NOT NULL DEFAULT (0),
                                        [prev_ytd_percentage] DECIMAL(18,4) NOT NULL DEFAULT (0.0),
                                        [number_of_days] INT NOT NULL DEFAULT(0),
                                        [total_days] INT NOT NULL DEFAULT (0))

    /*  Check Parameters  */

        SELECT @week_ending = CAST(ISNULL(@week_ending,GETDATE()) AS DATE);

    /*  Get date values from LFT_ATTENDANCE_WEEK_DATES function  */
        
        SELECT @fiscal_year = [cur_fiscal_year], 
               @week_start_dt = [cur_week_start_dt], 
               @week_end_dt = [cur_week_end_dt],
               @year_start_dt = [cur_fiscal_start_dt], 
               @year_end_dt = [cur_fiscal_end_dt], 
               @prev_fiscal_year = [prv_fiscal], 
               @prev_week_start_dt = [prv_week_start_dt], 
               @prev_week_end_dt = [prv_week_end_dt], 
               @prev_year_start_dt = [prv_fiscal_start_dt],
               @prev_year_end_dt = [prv_fiscal_end_dt]
        FROM [dbo].[LFT_ATTENDANCE_WEEK_DATES] (@week_ending, 0, 'N');
        
    /*  Get number of days between start of fiscal year and run date of the report  */

        SELECT @num_of_days = (DATEDIFF(DAY,@year_start_dt, @week_end_dt) + 1)

        IF [dbo].[LFS_IsLeapYear] (@fiscal_year) = 'Y'
            SELECT @total_days += 1;     --366 instead of 365

    /*  Get Month Data  */
    
        WITH CTE_VISIT_COUNT ([month_id], [visit_count])
        AS (SELECT FORMAT(CAST([history_date] AS date),'yyyy/MM'),
                   SUM(ISNULL([visit_count],0))
            FROM [dbo].[LT_HISTORY_VISIT_COUNT]
            WHERE CAST([history_date] AS DATE) BETWEEN @year_start_dt AND @week_end_dt
            GROUP BY FORMAT(CAST([history_date] AS date),'yyyy/MM'))
        INSERT INTO @month_data ([month_id], [month_base], [month_incr], [month_total], [month_actual_base], [month_actual_incr], [month_actual_total])
        SELECT FORMAT(att.[perf_dt],'yyyy/MM'),
               SUM((att.[budget] - att.[incremental])),
               SUM(att.[incremental]),
               SUM(att.[budget]),
               vis.[visit_count],
               0,
               vis.[visit_count]
        FROM [dbo].[LTR_ATTENDANCE_BUDGET_DATA] AS att
             INNER JOIN [CTE_VISIT_COUNT] AS vis ON vis.[month_id] = FORMAT(perf_dt,'yyyy/MM')
        WHERE att.[perf_dt] BETWEEN @year_start_dt AND @week_end_dt
          AND att.[title_no] = @gross_attend_no
        GROUP BY FORMAT(att.[perf_dt],'yyyy/MM'), vis.[visit_count]

        UPDATE @month_data
        SET [month_actual_base] = [month_base],
            [month_actual_incr] = ([month_actual_base] - [month_base])
        WHERE [month_actual_base] > [month_base] AND [month_incr] > 0;


        WITH CTE_VISIT_COUNT ([month_id], [visit_count])
        AS (SELECT FORMAT(CAST([history_date] AS date),'yyyy/MM'),
                   SUM(ISNULL([visit_count],0))
            FROM [dbo].[LT_HISTORY_VISIT_COUNT]
            WHERE CAST([history_date] AS DATE) BETWEEN @prev_year_start_dt AND @prev_week_end_dt
            GROUP BY FORMAT(CAST([history_date] AS date),'yyyy/MM'))
        INSERT INTO @month_data ([month_id], [prev_month_base], [prev_month_incr], [prev_month_total], [prev_month_actual_base], [prev_month_actual_incr], [prev_month_actual_total])
        SELECT FORMAT(att.[perf_dt],'yyyy/MM'),
               SUM((att.[budget] - att.[incremental])),
               SUM(att.[incremental]),
               SUM(att.[budget]),
               vis.[visit_count],
               0,
               vis.[visit_count]
        FROM [dbo].[LTR_ATTENDANCE_BUDGET_DATA] AS att
             INNER JOIN [CTE_VISIT_COUNT] AS vis ON vis.[month_id] = FORMAT(att.[perf_dt],'yyyy/MM')
        WHERE att.[perf_dt] BETWEEN @prev_year_start_dt AND @prev_week_end_dt
          AND att.[title_no] = @gross_attend_no
        GROUP BY FORMAT(att.[perf_dt],'yyyy/MM'), vis.[visit_count]

        UPDATE @month_data
        SET [prev_month_actual_base] = [prev_month_base],
            [prev_month_actual_incr] = ([prev_month_actual_base] - [prev_month_base])
        WHERE [prev_month_actual_base] > [prev_month_base] AND [prev_month_incr] > 0

    /*  Create Final Aggregated Data  */

        --Base Attendance
        INSERT INTO @ytd_final_table ([attendance_category], [attendance_category_sort], [fiscal_year],[ytd_budget],[ytd_actual],[ytd_variance],[ytd_percentage],
                                      [prev_fiscal_year], [prev_ytd_budget],[prev_ytd_actual],[prev_ytd_variance],[prev_ytd_percentage], [number_of_days], [total_days])
        SELECT  'Museum Base', 
                'AAA',
                @fiscal_year,
                SUM(month_base), 
                SUM(month_actual_base), 
                (SUM(month_actual_base) - SUM(month_base)), 
                0.0000,
                @prev_fiscal_year,
                SUM(prev_month_base), 
                SUM(prev_month_actual_base), 
                (SUM(month_actual_base) - SUM(prev_month_actual_base)),
                0.0000,
                @num_of_days,
                @total_days
        FROM @month_data
        
        --Incremental Attendance
        INSERT INTO @ytd_final_table ([attendance_category], [attendance_category_sort], [fiscal_year],[ytd_budget],[ytd_actual],[ytd_variance],[ytd_percentage],
                                      [prev_fiscal_year], [prev_ytd_budget],[prev_ytd_actual],[prev_ytd_variance],[prev_ytd_percentage])
        SELECT  'Museum Incremental', 
                'BBB',
                @fiscal_year,
                SUM([month_incr]), 
                SUM([month_actual_incr]), 
                (SUM(month_actual_incr) - SUM(month_incr)), 
                CASE WHEN SUM([month_incr]) = 0 THEN 0.0000
                     ELSE ((CAST(SUM(month_actual_incr) AS decimal(18,2)) - CAST(SUM(month_incr) AS DECIMAL(18,4))) / CAST(SUM(month_incr) AS DECIMAL(18,4))) END,
                @prev_fiscal_year,
                SUM(prev_month_incr), 
                SUM(prev_month_actual_incr), 
                (SUM(prev_month_actual_incr) - SUM(prev_month_incr)),
                CASE WHEN SUM([prev_month_incr]) = 0 THEN 0.0000
                     ELSE ((CAST(SUM(prev_month_actual_incr) AS decimal(18,2)) - CAST(SUM(prev_month_incr) AS DECIMAL(18,4))) / CAST(SUM(prev_month_incr) AS DECIMAL(18,4))) END
        FROM @month_data

        --Total Attendance
        INSERT INTO @ytd_final_table ([attendance_category], [attendance_category_sort], [fiscal_year],[ytd_budget],[ytd_actual],[ytd_variance],[ytd_percentage],
                                      [prev_fiscal_year], [prev_ytd_budget],[prev_ytd_actual],[prev_ytd_variance],[prev_ytd_percentage])
        SELECT  'Total Gross Gate Attendance', 
                'CCC',
                @fiscal_year,
                SUM([month_total]), 
                SUM(month_actual_total), 
                (SUM(month_actual_total) - SUM(month_total)), 
                CASE WHEN SUM([month_total]) = 0 THEN 0.0000
                     ELSE ((CAST(SUM([month_actual_total]) AS decimal(18,4)) - CAST(SUM([month_total]) AS DECIMAL(18,4))) / CAST(SUM([month_total]) AS DECIMAL(18,4))) END,
                @prev_fiscal_year,
                SUM(prev_month_total), 
                SUM(prev_month_actual_total), 
                (SUM(month_actual_total) - SUM(prev_month_actual_total)),
                CASE WHEN SUM([prev_month_total]) = 0 THEN 0.0000
                     ELSE ((CAST(SUM(prev_month_actual_total) AS decimal(18,2)) - CAST(SUM([prev_month_total]) AS DECIMAL(18,4))) / CAST(SUM([prev_month_total]) AS DECIMAL(18,4))) END
        FROM @month_data

        UPDATE @ytd_final_table
        SET [ytd_percentage] = CASE WHEN [ytd_budget] = 0 AND [ytd_actual] = 0 THEN 0.000
                                    WHEN [ytd_budget] > [ytd_actual] THEN CAST([ytd_variance] AS DECIMAL(18,4)) / CAST([ytd_budget] AS DECIMAL(18,4))
                                    ELSE CAST([ytd_variance] AS DECIMAL(18,4)) / CAST([ytd_actual] AS DECIMAL(18,4)) END,
            [prev_ytd_percentage] = CASE WHEN [ytd_actual] = 0 AND [prev_ytd_actual] = 0 THEN 0.0000
                                         WHEN [ytd_actual] > [prev_ytd_actual] THEN CAST([prev_ytd_variance] AS DECIMAL(18,4)) / CAST([ytd_actual] AS DECIMAL(18,4))
                                         ELSE CAST([prev_ytd_variance] AS DECIMAL(18,4)) / CAST([prev_ytd_actual] AS DECIMAL(18,4)) END

        

    FINISHED:

        /*  Final data set is a single row per category  */

            SELECT  [attendance_category_sort],
                    [attendance_category],
                    [fiscal_year],
                    [ytd_budget],
                    [ytd_actual],
                    [ytd_variance],
                    [ytd_percentage],
                    [prev_fiscal_year],
                    [prev_ytd_budget],
                    [prev_ytd_actual],
                    [prev_ytd_variance],
                    [prev_ytd_percentage],
                    [number_of_days],
                    [total_days],
                    @year_start_dt AS [start_dt],
                    @week_end_dt AS [end_dt]
            FROM @ytd_final_table
            ORDER BY [attendance_category_sort]

            
    DONE:

END
GO

--EXECUTE [dbo].[LRP_ATTEND_BUDGET_VS_ACTUAL_YTD_WITH_INCREMENTAL] @week_ending = '11-3-2019'

