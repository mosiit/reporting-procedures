USE [impresario];
GO

/****** Object:  StoredProcedure [dbo].[LRP_DELETE_CREDIT_CARD]    Script Date: 6/25/2021 4:10:12 PM ******/
DROP PROCEDURE [dbo].[LRP_DELETE_CREDIT_CARD];
GO

/****** Object:  StoredProcedure [dbo].[LRP_DELETE_CREDIT_CARD]    Script Date: 6/25/2021 4:10:12 PM ******/
SET ANSI_NULLS ON;
GO

SET QUOTED_IDENTIFIER ON;
GO


CREATE PROCEDURE [dbo].[LRP_DELETE_CREDIT_CARD]
    @customer_no VARCHAR(30),
    @act_no_four VARCHAR(4),
    @card_expiry_dt DATETIME,
    @id INT,
    @review_or_update CHAR(6) = 'Review'
AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

DECLARE @tblDel TABLE
(
    [id] [INT] NOT NULL,
    [customer_no] [INT] NOT NULL,
    [act_name] [VARCHAR](30) NULL,
    [act_no_four] [CHAR](4) NOT NULL,
    [act_no_six] [CHAR](6) NOT NULL,
    [card_expiry_dt] [DATETIME] NULL
);

BEGIN

	-- The day of the month varies but it's always on the first or last day.  Taking apart the month and year parts to compare to in later commands.
    DECLARE @expiry_month VARCHAR(2),
            @expiry_year VARCHAR(4);

    SELECT @expiry_month = CASE
              WHEN LEN(DATEPART(MONTH, @card_expiry_dt)) = 1 THEN
                  '0' + CONVERT(VARCHAR(2), DATEPART(MONTH, @card_expiry_dt))
              ELSE CONVERT(VARCHAR(2), DATEPART(MONTH, @card_expiry_dt))
          END;

    SELECT @expiry_year = CONVERT(VARCHAR(4), DATEPART(YEAR, @card_expiry_dt));

    IF @review_or_update = 'Review'
        SELECT id,
               customer_no,
               act_name,
               act_no_four,
               act_no_six,
               card_expiry_dt,
               'Selected' AS 'Action'
        FROM dbo.T_ACCOUNT_DATA
        WHERE customer_no = @customer_no
              AND act_no_four = @act_no_four
              AND CASE
                      WHEN LEN(DATEPART(MONTH, card_expiry_dt)) = 1 THEN
                          '0' + CONVERT(VARCHAR(2), DATEPART(MONTH, card_expiry_dt))
                      ELSE CONVERT(VARCHAR(2), DATEPART(MONTH, card_expiry_dt))
                  END = @expiry_month
              AND CONVERT(VARCHAR(4), DATEPART(YEAR, card_expiry_dt)) = @expiry_year
              AND id = @id;

    -- I know an ELSE would work here but I want to make sure that a mistake in entering 'Review' won't accidentally delete anything.
    IF @review_or_update = 'Delete'
    BEGIN

        INSERT INTO @tblDel
			SELECT id,
				   customer_no,
				   act_name,
				   act_no_four,
				   act_no_six,
				   card_expiry_dt
			FROM dbo.T_ACCOUNT_DATA
			WHERE customer_no = @customer_no
				  AND act_no_four = @act_no_four
				  AND CASE
						  WHEN LEN(DATEPART(MONTH, card_expiry_dt)) = 1 THEN
							  '0' + CONVERT(VARCHAR(2), DATEPART(MONTH, card_expiry_dt))
						  ELSE CONVERT(VARCHAR(2), DATEPART(MONTH, card_expiry_dt))
					  END = @expiry_month
				  AND CONVERT(VARCHAR(4), DATEPART(YEAR, card_expiry_dt)) = @expiry_year
				  AND id = @id;

        DELETE FROM dbo.T_ACCOUNT_DATA
        WHERE customer_no = @customer_no
              AND act_no_four = @act_no_four
              AND CASE
                      WHEN LEN(DATEPART(MONTH, card_expiry_dt)) = 1 THEN
                          '0' + CONVERT(VARCHAR(2), DATEPART(MONTH, card_expiry_dt))
                      ELSE CONVERT(VARCHAR(2), DATEPART(MONTH, card_expiry_dt))
                  END = @expiry_month
              AND CONVERT(VARCHAR(4), DATEPART(YEAR, card_expiry_dt)) = @expiry_year
              AND id = @id;

        IF NOT EXISTS
        (
            SELECT id
            FROM dbo.T_ACCOUNT_DATA
            WHERE customer_no = @customer_no
                  AND act_no_four = @act_no_four
                  AND CASE
                          WHEN LEN(DATEPART(MONTH, card_expiry_dt)) = 1 THEN
                              '0' + CONVERT(VARCHAR(2), DATEPART(MONTH, card_expiry_dt))
                          ELSE CONVERT(VARCHAR(2), DATEPART(MONTH, card_expiry_dt))
                      END = @expiry_month
                  AND CONVERT(VARCHAR(4), DATEPART(YEAR, card_expiry_dt)) = @expiry_year
                  AND id = @id
        )
            SELECT id,
                   customer_no,
                   act_name,
                   act_no_four,
                   act_no_six,
                   card_expiry_dt,
                   'Deleted' AS 'Action'
            FROM @tblDel;
        ELSE
            SELECT id,
                   customer_no,
                   act_name,
                   act_no_four,
                   act_no_six,
                   card_expiry_dt,
                   'Unable to delete' AS 'Action'
            FROM dbo.T_ACCOUNT_DATA
            WHERE customer_no = @customer_no
                  AND act_no_four = @act_no_four
                  AND CASE
                          WHEN LEN(DATEPART(MONTH, card_expiry_dt)) = 1 THEN
                              '0' + CONVERT(VARCHAR(2), DATEPART(MONTH, card_expiry_dt))
                          ELSE CONVERT(VARCHAR(2), DATEPART(MONTH, card_expiry_dt))
                      END = @expiry_month
                  AND CONVERT(VARCHAR(4), DATEPART(YEAR, card_expiry_dt)) = @expiry_year
                  AND id = @id;

    END;

END;


GO


