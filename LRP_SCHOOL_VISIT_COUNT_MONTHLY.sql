USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_SCHOOL_VISIT_COUNT_MONTHLY]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LRP_SCHOOL_VISIT_COUNT_MONTHLY]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LRP_SCHOOL_VISIT_COUNT_MONTHLY]
        @report_month VARCHAR(10) = NULL,
        @report_year int = NULL,
        @include_unpaid CHAR(1) = Null
AS BEGIN

    /*  Procedure Variables  */

        DECLARE @current_month INT = DATEPART(MONTH,GETDATE()), @previous_month INT = 0
        DECLARE @current_year INT = DATEPART(YEAR,GETDATE()), @previous_year INT = 0
        DECLARE @report_Start_dt DATETIME, @report_end_dt DATETIME, @seed_dt DATETIME
        DECLARE @report_month_num INT,  @rpt_msg varchar(100)       

        DECLARE @mos_buyout_no INT = (SELECT ISNULL([id],0) FROM [dbo].[TR_MOS] WHERE [description] = 'Buyouts')

        SELECT @rpt_msg = ''

    /*  Null Parameters = Previous Month  */

        SELECT @report_month = ISNULL(@report_month,''),
               @report_year = ISNULL(@report_year,0)

       IF @report_month = '' OR @report_year = 0 BEGIN

            IF @current_month = 1 SELECT @previous_month = 12, @previous_year = (@current_year - 1) 
            ELSE SELECT @previous_month = (@current_month - 1), @previous_year = @current_year
            
            SELECT @report_month = DateName(mm,DATEADD(mm,@previous_month - 1,0)),
                   @report_year = @previous_year
                
        END

        SELECT @report_month_num = DATEPART(MONTH, @report_month + ' 1, 1966')

        SELECT @include_unpaid = ISNULL(@include_unpaid,'N')

    /* Convert month and year into a start and end date...   */

        SELECT @report_start_dt =  CONVERT(DATETIME,CAST(@report_year AS VARCHAR(4)) + '/' + CASE WHEN @report_month_num < 10 THEN '0' ELSE '' END + CONVERT(VARCHAR(2),@report_month_num) + '/01')
        SELECT @report_end_dt = DATEADD(DAY,-1,DATEADD(MONTH,1,@report_Start_dt))


    /* pull back start date to the previous Sunday and extend end date to the following Saturday so that full weeks are pulled
       In the report any dates outside the month and year of the report are suppressed but having full weeks to start makes it easier to format the calendar.  */

        WHILE DATEPART(weekday,@report_Start_dt) > 1
            SELECT @report_Start_dt = DATEADD(DAY,-1,@report_Start_dt)

        WHILE DATEPART(weekday,@report_end_dt) < 7
            SELECT @report_end_dt = DATEADD(DAY,1,@report_end_dt)

    /* Make sure the dates passed to the procedure include the entire day (from midnight on the start date to 11:59 PM on the end date) and that parameters have values  */

        SELECT @report_start_dt = convert(char(10),@report_start_dt,111) + ' 00:00:00',
               @report_end_dt = convert(char(10),@report_end_dt,111) + ' 23:59:59.995'

        -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        
        IF OBJECT_ID('tempdb..#sub_line_items') IS NOT NULL DROP TABLE [#sub_line_items]

        CREATE TABLE [#sub_line_items] ([sli_no] int NOT NULL, [order_no] int, [perf_no] int, [zone_no] INT, [due_amt] decimal (18,2), [paid_amt] decimal(18,2), [sli_status] int, [price_type] INT,
                                               [perf_type] INT, [mode_of_sale] INT)

        CREATE CLUSTERED INDEX [sli_sli_no] ON [#sub_line_items] ([sli_no] ASC) ON [PRIMARY]
        CREATE NONCLUSTERED INDEX [sli_perf_no] ON [#sub_line_items] ([perf_no] ASC) ON [PRIMARY]
        CREATE NONCLUSTERED INDEX [sli_price_type] ON [#sub_line_items] ([price_Type] ASC) ON [PRIMARY]

        -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
        IF OBJECT_ID('tempdb..#visit_count_raw_data') is not null DROP TABLE [#visit_count_raw_data]
        
        CREATE TABLE [#visit_count_raw_data] ([attend_type] varchar(30), [order_no] int, [perf_date] CHAR(10), [perf_no] int, [zone_no] int, [title_name] VARCHAR(30), [prod_name] varchar(30), 
                                              [sale_total] int, [scan_admission_total] int)

        CREATE CLUSTERED INDEX [vcr_perf_date] ON [#visit_count_raw_data] ([perf_date] ASC) ON [PRIMARY]  
 
        -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

        IF OBJECT_ID('tempdb..#visit_count_final_data') is not null DROP TABLE [#visit_count_final_data]

        CREATE TABLE [#visit_count_final_data] ([attendance_type] varchar(30), [order_count] int, [perf_date] CHAR(10), [week_number] INT, [day_of_week] INT, [sale_total] int, 
                                                [scan_admission_total] INT, [report_message] VARCHAR(100))

        CREATE CLUSTERED INDEX [vcf_perf_date] ON [#visit_count_final_data] ([perf_date] ASC) ON [PRIMARY]
        
        -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /*  Get Sublineitem raw data - Using Exhibit Hall Admission rather than visit count  */

        INSERT INTO [#sub_line_items]
        SELECT [sli_no], sli.[order_no], sli.[perf_no], sli.[zone_no], sli.[due_amt], sli.[paid_amt], sli.[sli_status], sli.[price_type], prf.[perf_type], ord.[MOS]
        FROM [dbo].[T_SUB_LINEITEM] as sli (NOLOCK)
             INNER JOIN [dbo].[T_PERF] AS prf (NOLOCK) ON prf.[perf_no] = sli.[perf_no]
             INNER JOIN [dbo].[T_ORDER] AS ord (NOLOCK) ON ord.[order_no] = sli.[order_no]
        WHERE sli.[perf_no] in (SELECT [performance_no] FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] WHERE performance_dt BETWEEN @report_Start_dt AND @report_end_dt AND [title_name] = 'Exhibit Halls')

        DELETE FROM [#sub_line_items] WHERE [sli_status] not in (2, 3, 12) --SLI Status 2 = Seated, Unpaid, SLI Status 3 = Seated, Paid, SLU Status 12 = Ticketed, Paid

        IF @include_unpaid <> 'Y'
            DELETE FROM [#sub_line_items] WHERE [sli_status] = 2 --SLI Status 2 = Seated, Unpaid

        DELETE FROM [#sub_line_items] WHERE [perf_type] <> 3 AND [mode_of_sale] NOT IN (12,13)  --perf type 3 = School Only/mos 12 = Schools/mos 13 = Web Sales School)

    /*  Retrieve the Visit Count data for ticketed events from the database  */
            
        INSERT INTO [#visit_count_raw_data]
        SELECT 'ticketed', sli.[order_no], prf.[performance_date], sli.[perf_no], sli.[zone_no], prf.[title_name], prf.[production_name], count(sli.[sli_no]), COUNT(sli.[sli_no])
        FROM [#sub_line_items] AS sli
             LEFT OUTER JOIN [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] AS prf (NOLOCK) ON prf.performance_no = sli.[perf_no] AND prf.performance_zone = sli.[zone_no]
        --WHERE prf.[visit_count_title] = 'Y'
        GROUP BY sli.[order_no], prf.[performance_date], sli.[perf_no], sli.[zone_no], prf.[title_name], prf.[production_name] 

        DELETE FROM [#visit_count_raw_data] WHERE [prod_name] like '%Buyout%'

    /*  Added 2/7/2018:  Hard Coded exception added for the Exhibit Halls Special production, which is in the 
                         Exhibit Halls title but should not be counted in with the visit count.  */
         
         DELETE FROM [#visit_count_raw_data] WHERE [prod_name] = 'Exhibit Halls Special'


    /*  Added 10/26/2017:  Deletes orders from the visit count that were created in the buyouts mode of sale regardless of what the production name is.
                           @mos_buyout_no is defined above and gets the mode of sale number from the TR_MOS table.  
                          It must find and if number > 0 for this to happen.  */

            DELETE FROM [#visit_count_raw_data]
            WHERE [order_no] IN (SELECT [order_no] FROM [dbo].[T_ORDER] WHERE [order_no] IN (SELECT [order_no] FROM [#visit_count_raw_data]) AND [MOS] = @mos_buyout_no)

    /*  Retrieve Final Visit Count Data For Each School Order*/
        
        INSERT INTO [#visit_count_final_data] SELECT 'Ticketed Events', COUNT(DISTINCT [order_no]), [perf_date], DATEPART(WEEK,[perf_date]), DATEPART(WEEKDAY,[perf_date]), sum([sale_total]), sum([scan_admission_total]), ''
        FROM [#visit_count_raw_data] 
        GROUP By [perf_date]

        DELETE FROM [#visit_count_final_data] WHERE [perf_date] < CONVERT(CHAR(10),@report_Start_dt,111) OR [perf_date] > CONVERT(CHAR(10),@report_end_dt,111)

        UPDATE [#visit_count_final_data] SET [sale_total] = 0, [scan_admission_total] = 0 WHERE DATEPART(MONTH,[perf_date]) <> @report_month_num

    FINISHED:

        /*  If nothing found, set the proper report message  */

        IF NOT EXISTS (SELECT * FROM [#visit_count_final_data]) BEGIN
            IF @rpt_msg = '' SELECT @rpt_msg = 'No records found for the criteria you entered'
        END        

        /*  Make sure there is at least one record for every date in the range (otherwise the calendar will not format properly  */

            SELECT @seed_dt = @report_Start_dt
            WHILE @seed_dt < @report_end_dt BEGIN

                IF NOT EXISTS (SELECT * FROM [#visit_count_final_data] WHERE perf_date = CONVERT(CHAR(10),@seed_dt,111))
                    INSERT INTO [#visit_count_final_data]
                    VALUES  ('Ticketed Events', 0, CONVERT(CHAR(10),@seed_dt,111), DATEPART(WEEK,@seed_dt), DATEPART(WEEKDAY,@seed_dt), 0, 0, @rpt_msg)

                SELECT @seed_dt = DATEADD(DAY,1,@seed_dt)

            END

        /* Select aggregated date into the report  */

            SELECT [attendance_type], DATENAME(WEEKDAY,[perf_date]), [perf_date], DATENAME(MONTH,[perf_date]) AS 'month_name', [week_number], [day_of_week],
                   [order_count] as 'transaction_count', sum([sale_total]) as 'visit_count', sum([scan_admission_total]) as 'visit_count_scan', [report_message]
            FROM [#visit_count_final_data]
            GROUP BY [attendance_type], DATENAME(WEEKDAY,[perf_date]), [perf_date], DATENAME(MONTH,[perf_date]), [week_number], [day_of_week], [order_count], [report_message]
            ORDER BY [perf_date]

    CLEAN_UP:

        /*  Not really necessary, but the programmer in me wants to clean up  */

        IF OBJECT_ID('tempdb..#visit_count_raw_data') is not null DROP TABLE [#visit_count_raw_data]
        IF OBJECT_ID('tempdb..#visit_count_final_data') is not null DROP TABLE [#visit_count_final_data]
        IF OBJECT_ID('tempdb..#sub_line_items') IS NOT NULL DROP TABLE [#sub_line_items]

    DONE:

END
GO

GRANT EXECUTE ON [dbo].[LRP_SCHOOL_VISIT_COUNT_MONTHLY] to impusers
GO

--EXECUTE [dbo].[LRP_SCHOOL_VISIT_COUNT_MONTHLY] 'November', 2016, 'N'
--EXECUTE [dbo].[LRP_SCHOOL_VISIT_COUNT_MONTHLY] Null, Null, Null


--SELECT * FROM TemporaryData.dbo.[param_values]
--DELETE FROM TemporaryData.dbo.param_values
--INSERT INTO TemporaryData.dbo.param_values VALUES ('@report_month',@report_month)
--INSERT INTO TemporaryData.dbo.param_values VALUES ('@report_year',CONVERT(VARCHAR(50),@report_year))
--INSERT INTO TemporaryData.dbo.param_values VALUES ('@include_unpaid',@include_unpaid)