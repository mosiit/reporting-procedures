USE impresario;
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_ADV_STW_PROFILE_RESEARCH_NOTES]') AND type in (N'P', N'PC'))
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_ADV_STW_PROFILE_RESEARCH_NOTES] AS';
GO

EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LRP_ADV_STW_PROFILE_RESEARCH_NOTES] TO [impusers], [tessitura_app]';
GO

ALTER PROCEDURE [dbo].[LRP_ADV_STW_PROFILE_RESEARCH_NOTES]
         @customer_no INT = NULL,
         @note_type VARCHAR(30) = NULL
AS BEGIN

    SET NOCOUNT ON;
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    
    /*  Procedute Variables and Temp Tables  */

        DECLARE @cust_type INT = 0, @household_no INT = 0

        DECLARE @note_table TABLE ([customer_no] INT NOT NULL DEFAULT (0),
                                   [cust_type] INT NOT NULL DEFAULT (0),
                                   [expanded_customer_no] INT NOT NULL DEFAULT (0),
                                   [name_ind] INT NOT NULL DEFAULT (0),
                                   [name_ind_str] VARCHAR (30) NOT NULL DEFAULT (''),
                                   [customer_name] VARCHAR(100) NOT NULL DEFAULT (''),
                                   [note_type] INT NOT NULL DEFAULT (0),
                                   [note_type_descrip] VARCHAR(30) NOT NULL DEFAULT (''),
                                   [description] VARCHAR (30) NOT NULL DEFAULT (''),
                                   [cust_notes_no] INT NOT NULL DEFAULT (0),
                                   [create_dt] DATETIME NULL,
                                   [created_by] VARCHAR (30) NOT NULL DEFAULT (''),
                                   [create_loc] VARCHAR (30) NOT NULL DEFAULT (''),
                                   [last_update_dt] DATETIME NULL,
                                   [last_updated_by] VARCHAR (30) NOT NULL DEFAULT (''),
                                   [notes_length] INT NOT NULL DEFAULT (0),
                                   [notes] VARCHAR (MAX) NOT NULL DEFAULT (''))

    /*  Check Parameters */

        SELECT @customer_no = ISNULL(@customer_no, 0)

        SELECT @note_type = ISNULL(@note_type, 'All')

        IF @note_type NOT IN ('Strategy','Research') SELECT @note_type = 'All'
        

    /*  Get Notes  */

        SELECT @cust_type = cust_type FROM [dbo].[T_CUSTOMER] WHERE [customer_no] = @customer_no

        IF @cust_type = 7 SELECT @household_no = @customer_no 
        ELSE SELECT @household_no = ISNULL(MAX([household_no]),0)  FROM [dbo].[LV_CUSTOMER_WITH_HOUSEHOLD_INFO] WHERE [customer_no] = @customer_no
    
        IF @note_type IN ('All', 'Research') BEGIN

            INSERT INTO @note_table ([customer_no], [cust_type], [expanded_customer_no], [name_ind], [name_ind_str], [customer_name], [note_type], [note_type_descrip],
                                     [description], [cust_notes_no], [create_dt], [created_by], [create_loc], [last_update_dt], [last_updated_by], [notes_length], [notes])
            SELECT aff.[customer_no],
                   aff.[cust_type],
                   Aff.[expanded_customer_no],
                   aff.[name_ind],
                   CASE aff.[name_ind] WHEN -1 THEN 'A1'
                                       WHEN -2 THEN 'A2'
                                       ELSE '' END AS [name_ind_str],
                   nam.[best_name],
                   nte.[note_type],
                   'Research',
                   typ.[description],
                   nte.[cust_notes_no],
                   nte.[create_dt],
                   nte.[created_by],
                   nte.[create_loc],
                   nte.[last_update_dt],
                   nte.[last_updated_by],
                   ISNULL(LEN([dbo].[LFS_BOARD_GetResearchNotes] (nte.[cust_notes_no])), 0) AS [notes_length],
                   ISNULL([dbo].[LFS_BOARD_GetResearchNotes](nte.[cust_notes_no]), '') 
            FROM V_CUSTOMER_WITH_PRIMARY_AFFILIATES AS aff 
                 INNER JOIN TX_CUST_NOTES AS nte ON nte.[customer_no] = aff.[expanded_customer_no]
                 LEFT OUTER JOIN [dbo].[TR_CUST_NOTES_TYPE] AS typ ON typ.[id] = nte.[note_type]
                 LEFT OUTER JOIN [dbo].[LV_ADV_BEST_NAME] AS nam ON nam.[customer_no] = aff.[expanded_customer_no]
            WHERE aff.[customer_no] = @customer_no
              AND aff.[name_ind] IN(-2, -1, 0) 
              AND nte.[note_type] IN (3, 4)

        END ELSE IF @note_type IN ('All', 'Strategy') BEGIN

            INSERT INTO @note_table ([customer_no], [cust_type], [expanded_customer_no], [name_ind], [name_ind_str], [customer_name], [note_type], [note_type_descrip],
                                      [description], [cust_notes_no], [create_dt], [created_by], [create_loc], [last_update_dt], [last_updated_by], [notes_length], [notes])
            SELECT aff.[customer_no],
                   aff.[cust_type],
                   Aff.[expanded_customer_no],
                   aff.[name_ind],
                   CASE aff.[name_ind] WHEN -1 THEN 'A1'
                                       WHEN -2 THEN 'A2'
                                       ELSE '' END AS [name_ind_str],
                   nam.[best_name],
                   nte.[note_type],
                   'Strategy',
                   typ.[description],
                   nte.[cust_notes_no],
                   nte.[create_dt],
                   nte.[created_by],
                   nte.[create_loc],
                   nte.[last_update_dt],
                   nte.[last_updated_by],
                   ISNULL(LEN([dbo].[LFS_BOARD_GetResearchNotes] (nte.[cust_notes_no])),0) AS [notes_length],
                   ISNULL([dbo].[LFS_BOARD_GetResearchNotes](nte.[cust_notes_no]), '') 
            FROM V_CUSTOMER_WITH_PRIMARY_AFFILIATES AS aff 
                 INNER JOIN TX_CUST_NOTES AS nte ON nte.[customer_no] = aff.[expanded_customer_no]
                 LEFT OUTER JOIN [dbo].[TR_CUST_NOTES_TYPE] AS typ ON typ.[id] = nte.[note_type]
                 LEFT OUTER JOIN [dbo].[LV_ADV_BEST_NAME] AS nam ON nam.[customer_no] = aff.[expanded_customer_no]
            WHERE aff.[customer_no] = @household_no
              AND aff.[name_ind] IN(-1, 0) 
              AND nte.[note_type] = 11

        END


    FINISHED:

        /*  Get Final Data Set For The Report  */    

            SELECT [customer_no],
                   [cust_type],
                   [expanded_customer_no],
                   [name_ind],
                   [name_ind_str],
                   [customer_name],
                   [note_type],
                   [note_type_descrip],
                   [description],
                   [cust_notes_no],
                   [create_dt],
                   [created_by],
                   [create_loc],
                   CAST([last_update_dt] AS DATE) AS [last_update_dt],
                   [last_update_dt] AS [last_update_dttm],
                   [last_updated_by],
                   [notes_length],
                   [notes]
            FROM @note_table
        

END
GO
