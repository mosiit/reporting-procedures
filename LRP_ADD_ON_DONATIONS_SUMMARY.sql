USE [impresario];
GO

SET ANSI_NULLS ON;
GO

SET QUOTED_IDENTIFIER ON;
GO
DROP PROCEDURE [dbo].[LRP_ADD_ON_DONATIONS_SUMMARY];
GO

CREATE PROCEDURE [dbo].[LRP_ADD_ON_DONATIONS_SUMMARY]
(
    @order_start_dt DATETIME,
    @order_end_dt DATETIME,
    @anon_vs_known VARCHAR(4000),
    @member_vs_nonmemb VARCHAR(4000),
    @mode_of_sale VARCHAR(4000),
    @fund_no VARCHAR(4000),
    @max_contribution_amt MONEY
)
AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
SET NOCOUNT ON;
-- =============================================
-- Author:		Aileen Duffy-Brown
-- Create date: 5/15/2018
-- Description:	add on donations.
-- What percentage of orders fall into each category?
-- Total Tickets $ by Total Add-On Donation $
-- Parameters for Order Date Start and End	
-- Parameter for Member vs Non-Member at time of donation 
-- (or all) (Probably not possible, but that's the ideal)
-- Parameter for Known vs Anonymous (or all)
-- Parameter for Web vs Kiosk (or all)	
-- 5/8/18 Adding parameters for fund number for A/B testing
-- "variant ID" per Annie Petroff.
-- Including if record has an email address and % of orders 
-- with one per Heather Calvin.
-- 5/11/2018 Adding in counts of donations and Amounts in 
-- each column per Heather Calvin.
-- TrackIt! Work Order #90813
-- =============================================
BEGIN
    --Check Parameters
    SELECT @order_start_dt = ISNULL(@order_start_dt, '1/1/1900');
    SELECT @order_end_dt = ISNULL(@order_end_dt, '12/31/2999');
    SELECT @anon_vs_known = ISNULL(@anon_vs_known, '');
    SELECT @member_vs_nonmemb = ISNULL(@member_vs_nonmemb, '');
    SELECT @mode_of_sale = ISNULL(@mode_of_sale, '');
    SELECT @fund_no = ISNULL(@fund_no, '');
    SELECT @max_contribution_amt = ISNULL(@max_contribution_amt, '');

    --Mode of Sale 
    DECLARE @mos TABLE
    (
        mode_of_sale VARCHAR(30) NOT NULL
    );

    --IF @mos_str is NULL, search ALL MOS
    IF @mode_of_sale <> ''
    BEGIN
        INSERT INTO @mos
        SELECT Element
        FROM dbo.FT_SPLIT_LIST(REPLACE(@mode_of_sale, '"', ''), ',');
    END;
    ELSE
    BEGIN
        INSERT INTO @mos
        SELECT description
        FROM dbo.TR_MOS;

    END;


    --FUND # parameter.  This is being built for future growth using the gooesoft_dropdown table.  
    --This way additional funds can be entered into GOOESOFT_DROPDOWN by the CRM or DMS teams without having to edit this report.
    DECLARE @fund TABLE
    (
        fund_no VARCHAR(50) NOT NULL
    );

    --IF ISNULL(@fund_no, '') <> ''
    IF @fund_no <> ''
    BEGIN
        INSERT INTO @fund
        SELECT Element --CONVERT(VARCHAR(50), Element)
        FROM dbo.FT_SPLIT_LIST(REPLACE(@fund_no, '"', ''), ',');
    END;
    ELSE
    BEGIN
        INSERT INTO @fund
        SELECT SUBSTRING(description, 1, 4)
        FROM dbo.TR_GOOESOFT_DROPDOWN
        WHERE code = 1509;
    --WHERE fund_no IN ( 1388, 2303 );

    END;

    --
    CREATE TABLE #email
    (
        customer_no INT,
        [address] VARCHAR(80),
        inactive CHAR(1)
    );

    INSERT INTO #email
    (
        customer_no,
        address,
        inactive
    )
    SELECT DISTINCT
           customer_no,
           address,
           inactive
    FROM dbo.T_EADDRESS
    WHERE address LIKE '%@%'
          AND address NOT LIKE '%kiosk%@mos.org'
          AND inactive = 'N';

    --Find all donations to the selected funds between the order dates selected.  
	--When donation is present, is there also an email present?
    CREATE TABLE #donations_with_email
    (
        ref_no INT,
        customer_no INT,
        fund_no VARCHAR(50),
        donation_count INT,
        recd_amt MONEY,
        cont_dt DATETIME,
        batch_no INT,
        create_dt DATETIME,
        solicitor CHAR(8),
        donation_range VARCHAR(30),
        channel SMALLINT,
        e_address VARCHAR(80),
        has_email INT,
        inactive CHAR(1)
    );

    INSERT INTO #donations_with_email
    (
        ref_no,
        customer_no,
        fund_no,
        donation_count,
        recd_amt,
        cont_dt,
        batch_no,
        create_dt,
        solicitor,
        donation_range,
        channel,
        e_address,
        has_email,
        inactive
    )
    SELECT co.ref_no,
           co.customer_no,
           co.fund_no,
           1 AS 'donation_count',
           co.recd_amt,
           co.cont_dt,
           co.batch_no,
           co.create_dt,
           co.solicitor,
           CASE
               WHEN co.recd_amt = 0 THEN
                   'A. No Donation'
               WHEN co.recd_amt
                    BETWEEN 0.01 AND 1.99 THEN
                   'B. $0.01 - $1.99'
               WHEN co.recd_amt
                    BETWEEN 2.00 AND 2.99 THEN
                   'C. $2.00 - $2.99'
               WHEN co.recd_amt
                    BETWEEN 3.00 AND 4.99 THEN
                   'D. $3.00 - $4.99'
               WHEN co.recd_amt
                    BETWEEN 5.00 AND 9.99 THEN
                   'E. $5.00 - $9.99'
               WHEN co.recd_amt
                    BETWEEN 10.00 AND 14.99 THEN
                   'F. $10.00 - $14.99'
               WHEN co.recd_amt
                    BETWEEN 15.00 AND 19.99 THEN
                   'G. $15.00 - $19.99'
               WHEN co.recd_amt
                    BETWEEN 20.00 AND 49.99 THEN
                   'H. $20.00 - $49.99'
               WHEN co.recd_amt
                    BETWEEN 50.00 AND 299.99 THEN
                   'I. $50+'
               ELSE
                   'J. Other'
           END AS donation_range,
           co.channel,
           em.[address],
           CASE
               WHEN em.[address] IS NULL THEN
                   0
               ELSE
                   1
           END AS 'has_email',
           em.inactive
    FROM T_CONTRIBUTION AS co
        LEFT OUTER JOIN #email AS em
            ON em.customer_no = co.customer_no
    WHERE co.create_dt
          BETWEEN @order_start_dt AND @order_end_dt
          AND (co.fund_no IN
               (
                   SELECT fund_no FROM @fund
               )
              )
          OR ( -- this has been added to compensate for buggy data that occured prior to 2/23/2018
                 co.fund_no = 1388
                 AND co.channel IN
                     (
                         SELECT id FROM dbo.TR_SALES_CHANNEL WHERE description LIKE '%kiosk%'
                     )
                 AND co.cont_dt < '2018/02/23' --date after fix installed.
             )
             AND co.recd_amt < @max_contribution_amt
             AND co.recd_amt > 0;

    -- SELECT * FROM @donations_with_email;

    --strip out duplicates caused by duplicate emails on the account


    CREATE TABLE #donations
    (
        ref_no INT,
        customer_no INT,
        fund_no VARCHAR(50),
        donation_count INT,
        recd_amt MONEY,
        cont_dt DATETIME,
        batch_no INT,
        create_dt DATETIME,
        solicitor CHAR(8),
        donation_range VARCHAR(30),
        channel SMALLINT,
        e_address VARCHAR(80),
        has_email INT,
        inactive CHAR(1)
    );

    INSERT INTO #donations
    (
        ref_no,
        customer_no,
        fund_no,
        donation_count,
        recd_amt,
        cont_dt,
        batch_no,
        create_dt,
        solicitor,
        donation_range,
        channel,
        e_address,
        has_email,
        inactive
    )
    SELECT DISTINCT
           ref_no,
           customer_no,
           fund_no,
           donation_count,
           recd_amt,
           cont_dt,
           batch_no,
           create_dt,
           solicitor,
           donation_range,
           channel,
           e_address,
           has_email,
           inactive
    FROM #donations_with_email;

    --SELECT *
    --FROM @donations;

    --Find *all* Ticket orders between the order dates selected that happened 
	--in the modes of sale that were selected.
    CREATE TABLE #ticket_orders1
    (
        order_no INT,
        customer_no INT,
        order_count INT,
        mos_no SMALLINT,
        mode_of_sale VARCHAR(30),
        batch_no INT,
        order_dt DATETIME,
        total_paid_tix MONEY,
        solicitor CHAR(8)
    );

    INSERT INTO #ticket_orders1
    (
        order_no,
        customer_no,
        order_count,
        mos_no,
        mode_of_sale,
        batch_no,
        order_dt,
        total_paid_tix,
        solicitor
    )
    SELECT DISTINCT
           od.order_no,
           od.customer_no,
           1 AS 'order_count',
           od.MOS AS mos_no,
           mo.description AS mode_of_sale,
           od.batch_no,
           od.order_dt,
           tx.total_paid_tix,
           od.solicitor
    FROM T_ORDER AS od
        LEFT OUTER JOIN TR_MOS AS mo
            ON od.MOS = mo.id
        INNER JOIN dbo.T_SUB_LINEITEM AS sl
            ON sl.order_no = od.order_no
        CROSS APPLY
    (
        SELECT ABS(od.tot_ticket_purch_amt) + (od.tot_ticket_return_amt)
    ) tx(total_paid_tix)
    WHERE od.order_dt
          BETWEEN @order_start_dt AND @order_end_dt
          AND
          (
              @mode_of_sale = 'ALL'
              OR mo.description IN
                 (
                     SELECT mode_of_sale FROM @mos
                 )
          )
          AND
          (
              sl.sli_status IN ( 3, 12, 2, 6 ) -- Seated, Paid, Ticketed, Unpaid, unseated_paid
              AND od.tot_paid_amt > 0 -- no donation and no passes.  Likely a returned order.
          )
    GROUP BY od.order_no,
             od.customer_no,
             od.MOS,
             mo.description,
             od.batch_no,
             od.order_dt,
             tx.total_paid_tix,
             od.solicitor;
    --SELECT * FROM @ticket_orders1


    CREATE TABLE #membership_history
    (
        customer_no INT,
        order_no INT,
        order_dt DATETIME,
        initiation_date DATETIME,
        expiration_date DATETIME,
        memb_level_code CHAR(3)
    );

    INSERT INTO #membership_history
    (
        customer_no,
        order_no,
        order_dt,
        initiation_date,
        expiration_date,
        memb_level_code
    )
    SELECT od.customer_no,
           od.order_no,
           od.order_dt,
           lv.initiation_date,
           lv.expiration_date,
           lv.memb_level_code
    FROM LV_PAST_MEMBERSHIP_INFO AS lv
        INNER JOIN #ticket_orders1 AS od
            ON od.customer_no = lv.customer_no
               AND od.order_dt
               BETWEEN lv.initiation_date AND lv.expiration_date;
    --SELECT * FROM @membership_history;



    CREATE TABLE #ticket_orders
    (
        customer_no INT,
        order_no INT,
        order_count INT,
        mos_no SMALLINT,
        mode_of_sale VARCHAR(30),
        batch_no INT,
        order_dt DATETIME,
        total_paid_tix MONEY,
        ticket_range VARCHAR(30),
        anon_vs_known VARCHAR(30),
        memb_vs_nonmemb VARCHAR(30),
        memb_level_code CHAR(3),
        initiation_date DATETIME,
        expiration_date DATETIME,
        display_name VARCHAR(200),
        solicitor CHAR(8)
    );

    INSERT INTO #ticket_orders
    (
        customer_no,
        order_no,
        order_count,
        mos_no,
        mode_of_sale,
        batch_no,
        order_dt,
        total_paid_tix,
        ticket_range,
        anon_vs_known,
        memb_vs_nonmemb,
        memb_level_code,
        initiation_date,
        expiration_date,
        display_name,
        solicitor
    )
    SELECT od1.customer_no,
           od1.order_no,
           od1.order_count,
           od1.mos_no,
           od1.mode_of_sale,
           od1.batch_no,
           od1.order_dt,
           od1.total_paid_tix,
           CASE
               WHEN od1.total_paid_tix = 0 THEN
                   'A. $0.00'
               WHEN od1.total_paid_tix
                    BETWEEN 0.01 AND 24.99 THEN
                   'B. $0.01 - $24.99'
               WHEN od1.total_paid_tix
                    BETWEEN 25.00 AND 49.99 THEN
                   'C. $25.00 - $49.99'
               WHEN od1.total_paid_tix
                    BETWEEN 50.00 AND 74.99 THEN
                   'D. $50.00 - $74.99'
               WHEN od1.total_paid_tix
                    BETWEEN 75.00 AND 99.99 THEN
                   'E. $75.00 - $99.99'
               WHEN od1.total_paid_tix
                    BETWEEN 100.00 AND 124.99 THEN
                   'F. $100.00 - $124.99'
               WHEN od1.total_paid_tix
                    BETWEEN 125.00 AND 149.99 THEN
                   'G. $125.00 - $149.99'
               WHEN od1.total_paid_tix
                    BETWEEN 150.00 AND 174.99 THEN
                   'H. $150.00 - $174.99'
               WHEN od1.total_paid_tix
                    BETWEEN 175.00 AND 199.99 THEN
                   'I. $175.00 - $199.99'
               WHEN od1.total_paid_tix
                    BETWEEN 200.00 AND 999999.99 THEN
                   'J. $200+'
               ELSE
                   'K. Other'
           END AS ticket_range,
           CASE
               WHEN od1.customer_no = 0 THEN
                   'Anonymous'
               WHEN cu.display_name = 'Kiosk Customer' THEN
                   'Anonymous'
               WHEN cu.display_name = 'Guest User' THEN
                   'Anonymous'
               WHEN cu.display_name = 'Kiosk Anon User Do Not Remove' THEN
                   'Anonymous'
               ELSE
                   'Known'
           END AS anon_vs_known,
           CASE
               WHEN me.memb_level_code IS NULL THEN
                   'Nonmember'
               ELSE
                   'Member'
           END AS memb_vs_nonmemb,
           me.memb_level_code,
           me.initiation_date,
           me.expiration_date,
           cu.display_name,
           od1.solicitor
    FROM #ticket_orders1 AS od1
        LEFT OUTER JOIN [dbo].[FT_CONSTITUENT_DISPLAY_NAME]() AS cu
            ON od1.customer_no = cu.customer_no
        LEFT OUTER JOIN #membership_history AS me
            ON me.customer_no = od1.customer_no
               AND od1.order_dt
               BETWEEN me.initiation_date AND me.expiration_date;

    --SELECT * FROM @ticket_orders

    --Final select from
    SELECT DISTINCT
           co.ref_no,
           od.customer_no,
           ISNULL(od.order_count, 0) AS 'order_count',
           ISNULL(co.donation_count, 0) AS 'donation_count',
           od.order_no,
           od.mode_of_sale,
           od.order_dt,
           od.total_paid_tix,
           od.ticket_range,
           od.anon_vs_known,
           od.memb_vs_nonmemb,
           co.fund_no,
           co.recd_amt,
           CASE
               WHEN donation_range IS NULL THEN
                   'A. No Donation'
               ELSE
                   co.donation_range
           END AS donation_range,
           SUM(od.total_paid_tix + co.recd_amt) AS paid_amt,
           co.has_email
    FROM #ticket_orders AS od
        LEFT OUTER JOIN #donations AS co
            ON od.customer_no = co.customer_no
               AND DATEDIFF(d, od.order_dt, co.cont_dt) = 0
               AND od.batch_no = co.batch_no
        LEFT OUTER JOIN #membership_history AS mh
            ON mh.customer_no = od.customer_no
               AND mh.order_dt = od.order_dt
    WHERE od.order_dt
          BETWEEN @order_start_dt AND @order_end_dt
          AND
          (
              @member_vs_nonmemb = 'ALL'
              OR od.memb_vs_nonmemb = @member_vs_nonmemb
          )
          AND
          (
              @anon_vs_known = 'ALL'
              OR od.anon_vs_known = @anon_vs_known
          )
    GROUP BY co.ref_no,
             od.order_no,
             od.order_dt,
             od.customer_no,
             od.ticket_range,
             co.donation_range,
             od.mode_of_sale,
             od.total_paid_tix,
             od.memb_vs_nonmemb,
             od.anon_vs_known,
             co.fund_no,
             co.recd_amt,
             od.order_count,
             co.donation_count,
             co.has_email;
    DROP TABLE #donations;
    DROP TABLE #donations_with_email;
    DROP TABLE #email;
    DROP TABLE #membership_history;
    DROP TABLE #ticket_orders;
    DROP TABLE #ticket_orders1;
END;

GO
GRANT EXECUTE ON [dbo].[LRP_ADD_ON_DONATIONS_SUMMARY] TO ImpUsers;

GO
