USE [impresario];
GO

SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO

ALTER PROCEDURE [dbo].[LRP_BUYOUTS]
(
    @perf_start_dt DATETIME,
    @perf_end_dt DATETIME
)
AS

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
SET NOCOUNT ON;

--EXEC [LRP_BUYOUTS] '12/1/2018', '12/31/2018'

BEGIN

-- QUERY COPIED FROM LRP_ADVANCED_RESERVATIONS_BY_VENUE
SELECT 
	inv.inv_no AS title_no, 
	inv.description AS title_name, 
	
	ord.order_no, 
	ord.customer_no, 
	cust.display_name AS consituentName, 
	cust.sort_name, 
	addr.city, 
	addr.state, 
	ord.initiator_no, 
	ISNULL(initi.display_name, '') AS initiatorName, 
	inv2.description AS production_name, 
	CASE 
		WHEN pty.[description] IN ('Drop-In Activity', 'Live Presentation') THEN p.[perf_dt]
		WHEN ISDATE(Z.[description]) = 0 THEN p.[perf_dt]
		ELSE DATEADD(dd, DATEDIFF(dd, 0, p.perf_dt), 0) + CAST(Z.[description] AS DATETIME)
	END AS visitDate,
	pty.description AS price_type_name,
	ord.notes AS order_notes,
	pp.start_enabled,
	COUNT(*) AS cnt
FROM T_ORDER ord
INNER JOIN dbo.T_SUB_LINEITEM sli
	ON ord.order_no = sli.order_no
INNER JOIN [dbo].t_sli_detail slidet
	ON sli.sli_no = slidet.sli_no
INNER JOIN T_PERF p
	ON p.perf_no = sli.perf_no
LEFT JOIN dbo.T_PERF_PRICE_TYPE PPT
	ON PPT.id = slidet.pmap_no
	AND PPT.perf_no = p.perf_no
LEFT JOIN dbo.T_PERF_PRICE pp
	ON pp.perf_no = p.perf_no
	AND pp.zone_no = sli.zone_no
	AND pp.perf_price_type = PPT.id
LEFT JOIN [dbo].[TR_PRICE_TYPE] as pty
	ON pty.[id] = sli.[price_type]
	AND pty.inactive = 'N'
LEFT JOIN dbo.T_PROD_SEASON psea
	ON p.prod_season_no = psea.prod_season_no
LEFT JOIN dbo.T_PRODUCTION prod
	ON prod.prod_no = psea.prod_no
LEFT JOIN dbo.T_INVENTORY inv
	ON inv.inv_no = prod.title_no
	AND inv.type = 'T' -- Title
LEFT JOIN dbo.T_INVENTORY inv2
	ON inv2.inv_no = prod.prod_no
	AND inv2.type = 'P' -- Production
LEFT JOIN dbo.T_ZMAP ZM 
	ON P.zmap_no = ZM.zmap_no
LEFT JOIN dbo.T_ZONE Z 
	ON SLI.zone_no = Z.zone_no 
	AND ZM.zmap_no = Z.zmap_no
LEFT JOIN dbo.FT_CONSTITUENT_DISPLAY_NAME() AS cust 
	ON ord.customer_no = cust.customer_no
LEFT JOIN dbo.T_ADDRESS addr
	ON addr.customer_no = ord.customer_no
	AND addr.primary_ind = 'Y' 
LEFT JOIN dbo.FT_CONSTITUENT_DISPLAY_NAME() AS initi 
	ON ord.initiator_no = initi.customer_no
WHERE p.perf_type = 5 -- Buyouts
	AND sli.sli_status IN (2,3,11,12) -- Seated, Unpaid|Seated, Paid|Upgraded|Ticketed, Paid
	AND CASE 
			WHEN pty.[description] IN ('Drop-In Activity', 'Live Presentation') THEN p.[perf_dt]
			WHEN ISDATE(Z.[description]) = 0 THEN p.[perf_dt]
			ELSE DATEADD(dd, DATEDIFF(dd, 0, p.perf_dt), 0) + CAST(Z.[description] AS DATETIME)
		END BETWEEN @perf_start_dt AND @perf_end_dt
GROUP BY 
	inv.inv_no, 
	inv.description, 
	ord.order_no, 
	ord.customer_no, 
	cust.display_name, 
	cust.sort_name, 
	addr.city, 
	addr.state, 
	ord.initiator_no, 
	ISNULL(initi.display_name, ''), 
	inv2.description, 
	CASE 
			WHEN pty.[description] IN ('Drop-In Activity', 'Live Presentation') THEN p.[perf_dt]
			WHEN ISDATE(Z.[description]) = 0 THEN p.[perf_dt]
			ELSE DATEADD(dd, DATEDIFF(dd, 0, p.perf_dt), 0) + CAST(Z.[description] AS DATETIME)
		END, 
	Z.description,
	pty.description,
	ord.notes,
	pp.start_enabled
ORDER BY ord.order_no

END 
