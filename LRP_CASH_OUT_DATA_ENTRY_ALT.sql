USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LRP_CASH_OUT_DATA_ENTRY_ALT]    Script Date: 4/15/2016 11:50:42 AM ******/
DROP PROCEDURE [dbo].[LRP_CASH_OUT_DATA_ENTRY_ALT]
GO

/****** Object:  StoredProcedure [dbo].[LRP_CASH_OUT_DATA_ENTRY_ALT]    Script Date: 4/15/2016 11:50:42 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[LRP_CASH_OUT_DATA_ENTRY_ALT]
	@payment_operator_location VARCHAR(30),
	@payment_operator VARCHAR(8),
	@payment_date VARCHAR(10),
	@cash_amt MONEY,
	@check_amt MONEY,
	@credit_card_amt MONEY,
	@gift_certificate_amt MONEY,
	@invoice_amt MONEY,
	@refunds_amt MONEY,
	@other_amt MONEY,
	@adult_city_pass_voucher_one_step_amt MONEY,
	@child_city_pass_voucher_one_step_amt MONEY,
    @userid VARCHAR(8),
	@OverrideCode VARCHAR(25) = null
AS

BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE	@cnt INT,
			@max INT

	DECLARE	@payment_type_name VARCHAR(30)

	DECLARE	@payment_amt MONEY

    DECLARE @code_to_override VARCHAR(25) = @payment_date + ' ' + @payment_Operator
    SELECT @OverrideCode = ISNULL(@OverrideCode,'')

	DECLARE	@tblPays TABLE	(
							row_id				INT IDENTITY(1,1),
							payment_type_name	VARCHAR(30),
							payment_amt			MONEY,
                            payment_msg         VARCHAR(100)
							)

    IF EXISTS (SELECT * FROM [dbo].[LT_CASH_OUT_DATA] WHERE [payment_date] = @payment_date AND [payment_operator] = @payment_operator AND @OverrideCode <> @code_to_override) BEGIN

        SELECT  0 AS 'id',
			    '' AS 'payment_operator_location',
			    @payment_operator AS 'payment_operator',
			    '' AS 'payment_operator_full_name',
			    @payment_date AS 'payment_date',
			    '' AS 'payment_type_name',
			    0.00 AS 'payment_amount',
			    'N' AS 'inactive',
			    USER_ID() AS 'created_by',
			    GETDATE() AS 'created_dt',
			    '' AS 'created_loc',
			    USER_ID() AS 'last_updated_by',
			    GETDATE() AS 'last_updated_dt',
                'DATA ALREADY ENTERED FOR ' + UPPER(@payment_operator) + ' ON ' + @payment_date + CHAR(10) + 'To overwrite existing data enter the proper override code' AS 'payment_msg'

            GOTO DONE

    END


	INSERT INTO @tblPays VALUES
		('Cash', @cash_amt, ''),
		('Check', @check_amt, ''),
		('Credit Card', @credit_card_amt, ''),
		('Gift Certificate', @gift_certificate_amt, ''),
		('Invoice', @invoice_amt, ''),
		('Refunds', @refunds_amt, ''),
		('Other', @other_amt, ''),
		('CityPass Prepaid Voucher Adult', @adult_city_pass_voucher_one_step_amt, ''),
		('CityPass Prepaid Voucher Child', @child_city_pass_voucher_one_step_amt, '')

	--SELECT * FROM @tblPays

	SELECT @cnt = 1
	SELECT @max = COUNT(row_id) FROM @tblPays

	--SELECT @cnt, @max

	WHILE @cnt <= @max
		BEGIN

			SELECT	@payment_type_name = payment_type_name,
					@payment_amt = payment_amt	
			FROM	@tblPays
			WHERE	row_id = @cnt

			--SELECT	@payment_type_name, @payment_amt

			MERGE	[dbo].[LT_CASH_OUT_DATA] AS target
			USING	(SELECT	@payment_operator, 
							usr.fname,
							usr.lname,
							@payment_operator_location, 
							@payment_date,
							@payment_type_name, 
							@payment_amt,
							@UserId
						FROM	[dbo].[T_METUSER] AS usr 
						WHERE	@payment_operator = usr.[Userid]
						) AS source (pop, fn, ln, popl, pd, ptn, pta, cb)
			ON		(target.payment_operator_location = source.popl
			AND		 target.payment_operator = source.pop
			AND		 target.payment_date = source.pd
			AND		 target.payment_type_name = source.ptn)
			WHEN MATCHED THEN
			UPDATE	SET target.payment_amount = source.pta,
						target.created_by = source.cb,
						target.last_updated_by = source.cb
			WHEN NOT MATCHED THEN
			INSERT ([payment_operator_location], [payment_operator], [payment_operator_first_name], [payment_operator_last_name], [payment_date], [payment_type_name], [payment_amount], [created_by], [last_updated_by])
				VALUES (source.popl, source.pop, source.fn, source.ln, source.pd, source.ptn, source.pta, source.cb, source.cb);

			SELECT @cnt = @cnt + 1

		END

    SELECT  [id],
			[payment_operator_location],
			[payment_operator],
			[payment_operator_last_name] + ', ' + [payment_operator_first_name] AS [payment_operator_full_name],
			[payment_date],
			[payment_type_name],
			[payment_amount],
			[inactive],
			[created_by],
			[created_dt],
			[created_loc],
			[last_updated_by],
			[last_updated_dt],
            '' AS 'payment_msg'
    FROM    [dbo].[LT_CASH_OUT_DATA]
	WHERE	[payment_operator] = @payment_operator
	AND		[payment_operator_location] = @payment_operator_location
	AND		[payment_date] = @payment_date

    DONE:

END

GO

GRANT EXECUTE ON [dbo].[LRP_CASH_OUT_DATA_ENTRY_ALT] TO impusers
GO


