USE [impresario];
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM [sys].[objects] WHERE [object_id] = OBJECT_ID(N'[dbo].[LRP_MEMBER_VISIT_STATS]') AND [type] in (N'P', N'PC')) BEGIN
    EXEC [dbo].[sp_executesql] @statement = N'CREATE PROCEDURE [dbo].[LRP_MEMBER_VISIT_STATS] AS';
    EXEC [dbo].[sp_executesql] @statement = N'GRANT EXECUTE ON [dbo].[LRP_MEMBER_VISIT_STATS] TO impusers';
    EXEC [dbo].[sp_executesql] @statement = N'GRANT EXECUTE ON [dbo].[LRP_MEMBER_VISIT_STATS] TO tessitura_app';
END;
GO

ALTER PROCEDURE [dbo].[LRP_MEMBER_VISIT_STATS]
        @report_start_dt DATE = '6-1-2019',
        @report_end_dt DATE = '8-31-2019',
        @membership_org_str varchar(4000) = '"4","13"',
        @include_member_output CHAR(1) = 'Y'
AS BEGIN

    SET NOCOUNT ON;
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
       
    /*  Procedure Variables and Temporary Tables  */

        DECLARE @total_memberships DECIMAL(18,2) = 0.0,
                @Total_households DECIMAL(18,2) = 0.0,
                @Total_Visits DECIMAL(18,2) = 0.0,
                @zero_visits INT = 0
    
        DECLARE @mem_org_list TABLE ([memb_org_no] INT NOT NULL DEFAULT (0))

        IF OBJECT_ID('tempdb..#all_active_members') IS NOT NULL DROP TABLE [#all_active_members];

        CREATE TABLE [#all_active_members] ([cust_memb_no] INT NOT NULL DEFAULT(0),
                                            [customer_no] INT NOT NULL DEFAULT (0),
                                            [memb_org_no] INT NOT NULL DEFAULT (0),
                                            [memb_level] VARCHAR(10) NOT NULL DEFAULT (''),
                                            [init_dt] DATETIME NULL,
                                            [expr_dt] DATETIME NULL);

        IF OBJECT_ID('tempdb..#member_visit_raw_data') IS NOT NULL DROP TABLE [#member_visit_raw_data];

        CREATE TABLE [#member_visit_raw_data] ([perf_date] DATE NULL,
                                               [attendance_type] VARCHAR(30) NOT NULL DEFAULT (''),
                                               [customer_no] INT NOT NULL DEFAULT (0),
                                               [active_membership] CHAR(1) NOT NULL DEFAULT (''),
                                               [active_membership_no] INT NOT NULL DEFAULT (0));

        IF OBJECT_ID('tempdb..#member_visit_data') IS NOT NULL DROP TABLE [#member_visit_data];

        CREATE TABLE [#member_visit_data] ([perf_date] DATE NULL,
                                               [attendance_type] VARCHAR(30) NOT NULL DEFAULT (''),
                                               [customer_no] INT NOT NULL DEFAULT (0),
                                               [customer_name] VARCHAR(100) NOT NULL DEFAULT (''),
                                               [active_membership_no] INT NOT NULL DEFAULT (0),
                                               [memb_org_no] INT NOT NULL DEFAULT (0),
                                               [memb_level_no] INT NOT NULL DEFAULT (0),
                                               [memb_level] VARCHAR (5) NOT NULL DEFAULT (''),
                                               [memb_level_group] VARCHAR(30) NOT NULL DEFAULT (''),
                                               [memb_level_name] VARCHAR(30) NOT NULL DEFAULT (''));

        IF OBJECT_ID('tempdb..#member_visit_stats') IS NOT NULL DROP TABLE [#member_visit_stats];

        CREATE TABLE [#member_visit_stats] ([stat_id] INT NOT NULL IDENTITY (1,1),
                                            [stat_order] INT NOT NULL DEFAULT (0),
                                            [stat_category] VARCHAR(30) NOT NULL DEFAULT (''),
                                            [stat_subcategory] VARCHAR(100) NOT NULL DEFAULT (''),
                                            [stat_sort_value] VARCHAR(100) NOT NULL DEFAULT (''),
                                            [stat_value] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                            [stat_metric] VARCHAR(50) NOT NULL DEFAULT (''),
                                            [stat_display] VARCHAR(100) NOT NULL DEFAULT (''))


    /*  Check Parameters  */

        SELECT @report_start_dt = ISNULL(@report_start_dt, DATEADD(DAY, -31, GETDATE()))
        SELECT @report_end_dt = ISNULL(@report_end_dt, DATEADD(DAY, -1, GETDATE()))

        SELECT @report_start_dt = CAST(@report_start_dt AS DATE)
        SELECT @report_end_dt = CONVERT(CHAR(10),@report_end_dt,111) + ' 23:59:59.957'

        IF ISNULL(@membership_org_str,'') = ''
            INSERT INTO @mem_org_list ([memb_org_no])
            SELECT [memb_org_no] FROM [dbo].[T_MEMB_ORG]
        ELSE
            INSERT INTO @mem_org_list ([memb_org_no])
            SELECT [element] FROM [dbo].[FT_SPLIT_LIST](REPLACE(@membership_org_str, '"', ''), ',')

    /*  Get the list of any customer who had an active membership at any point within the date range  */

        INSERT INTO [#all_active_members] ([cust_memb_no], [customer_no], [memb_org_no], [memb_level], [init_dt], [expr_dt])
        SELECT [mem].[cust_memb_no],
               [mem].[customer_no],
               [mem].[memb_org_no],
               [mem].[memb_level],
               [mem].[init_dt],
               [mem].[expr_dt]
        FROM [dbo].[TX_CUST_MEMBERSHIP] AS [mem]
             INNER JOIN @mem_org_list AS [org] ON [org].[memb_org_no] = [mem].[memb_org_no]
        WHERE CAST([mem].[init_dt] AS DATE) <= CAST(@report_end_dt AS DATE) AND CAST([mem].[expr_dt] AS DATE) >= CAST(@report_start_dt AS DATE)
          AND [mem].[current_status] IN (1, 2)
        
        --Total # of Distinct Households Who Had Memberships
        SELECT @total_memberships = COUNT(DISTINCT [Customer_no]) FROM [#all_active_members]

    /*  Get all customers who bought a ticket to anything within the designated time frame  */

        INSERT INTO [#member_visit_raw_data] ([perf_date], [customer_no], [attendance_type])
        SELECT DISTINCT CAST([performance_date] AS DATE),
                             [customer_no],
                             'Ticket'
        FROM [dbo].[LT_HISTORY_TICKET]
        WHERE CAST([performance_date] AS DATE) BETWEEN @report_start_dt AND @report_end_dt;
                
    /*  Update the records to indicate which had an active membership on the date of their visit
            Note: accessing this function as part of the above select statement was causing
                  performance issues, so it was removed and done as a separate step.  */

        UPDATE [#member_visit_raw_data]
        SET [active_membership] = [dbo].[LFS_ActiveMemberOnDate]([customer_no], 0, CAST([perf_date] AS DATE),'N');

    /*  Delete any non-member records  */

        DELETE FROM [#member_visit_raw_data] 
        WHERE [active_membership] = 'N';

    /*  Get the membership gate scans for the designated date range  */

        INSERT INTO [#member_visit_raw_data] ([perf_date], [customer_no], [attendance_type], [active_membership], [active_membership_no])
        SELECT DISTINCT CAST([prf].[performance_date] AS DATE), 
                        [att].[customer_no],
                        'Gate Scan',
                        'Y',
                        [att].[cust_memb_no]
                        
        FROM [dbo].[T_ATTENDANCE] AS [att]                                                                 --236=primary zone # for public Exhibit Halls
             LEFT OUTER JOIN [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] AS [prf] ON [prf].[performance_no] = [att].[perf_no] AND [prf].[performance_zone] = 236 
             LEFT OUTER JOIN [dbo].[T_MEMB_LEVEL] AS [lev] ON [lev].[memb_level_no] = [att].[memb_level_no]                        
        WHERE ISNULL([att].[ticket_no], 0) = 0 
          AND CAST([att].[attend_dt] AS DATE) BETWEEN @report_start_dt AND @report_end_dt 
          AND [att].[area_no] IS NULL;

    /*  Get the active membership number on the date of the visit (if not already in the data)  */

        UPDATE [#member_visit_raw_data]
        SET [active_membership_no] = [dbo].[LFS_ActiveMemberNumberOnDate]([customer_no], 0, CAST([perf_date] AS DATE))
        WHERE ISNULL([active_membership_no], 0) = 0;

        
    /*  If a member both bought a ticket and scanned their membership card at the gate on a given date, 
        remove the ticket sale and keep the gate scan so that they are only counted once.  */

        DELETE [rd1]
        FROM [#member_visit_raw_data] AS [rd1]
             INNER JOIN [#member_visit_raw_data] AS [rd2] ON [rd2].[perf_date] = [rd1].[perf_date] 
                                                       AND [rd2].[customer_no] = [rd1].[customer_no] 
                                                       AND [rd2].[attendance_type] = 'Gate Scan'
        WHERE [rd1].[attendance_type] = 'Ticket';

    /*  Using the list of members remaining, get the information about each visit and membership  */
   
        INSERT INTO [#member_visit_data] ([perf_date], [attendance_type], [customer_no], [customer_name], [active_membership_no], 
                                          [memb_org_no], [memb_level_no], [memb_level], [memb_level_group], [memb_level_name])
        SELECT [rd1].[perf_date],
               [rd1].[attendance_type],
               [rd1].[customer_no],
               LTRIM([cus].[display_name]),
               [rd1].[active_membership_no],
               ISNULL([mem].[memb_org_no], 0),
               ISNULL([lev].[memb_level_no], 1000),
               ISNULL([mem].[memb_level], 'Unk'),
               CASE WHEN ISNULL([lev].[description], 'Unknown') LIKE 'Basic%' THEN 'Basic'
                    WHEN ISNULL([lev].[description], 'Unknown') LIKE 'Premier%' THEN 'Premier'
                    ELSE 'Unknown' END,
               ISNULL(REPLACE([lev].[description],' Membership', ''), 'Unknown Level')
        FROM [#member_visit_raw_data] [rd1]
             INNER JOIN [dbo].[FT_CONSTITUENT_DISPLAY_NAME]() AS [cus] ON [cus].[customer_no] = [rd1].[customer_no]
             LEFT OUTER JOIN [dbo].[TX_CUST_MEMBERSHIP] AS [mem] ON [mem].[cust_memb_no] = [rd1].[active_membership_no]
             LEFT OUTER JOIN [dbo].[T_MEMB_LEVEL] AS [lev] ON [lev].[memb_level] = [mem].[memb_level]
             INNER JOIN @mem_org_list AS [org] ON [org].[memb_org_no] = [mem].[memb_org_no]

        --Total # of Visits
        SELECT @Total_Visits = COUNT(*) FROM [#member_visit_data]

        --Total # of Distinct Households Who Had Memberships AND visited
        SELECT @Total_households = COUNT(DISTINCT [customer_no]) FROM [#member_visit_data]
                           
    /*****************************************************************************************************************************************/
    
    /* Statistic # 1: Total Active Memberships and Total number of membership Visits */

        INSERT INTO [#member_visit_stats] ([stat_order], [stat_category], [stat_subcategory], [stat_sort_value], 
                                       [stat_value], [stat_metric], [stat_display])
        SELECT 1, 
               'Grand Total',
               'Memberships',
               '000000000000000',
               @total_memberships,
               'Members',
               FORMAT(@total_memberships,'###,##0') + ' Members'
              
        INSERT INTO [#member_visit_stats] ([stat_order], [stat_category], [stat_subcategory], [stat_sort_value], 
                                           [stat_value], [stat_metric], [stat_display])
        SELECT 1, 
               'Grand Total',
               'Member Visits',
               '000000000000001',
               @Total_Visits,
               'Visits',
               FORMAT(@Total_Visits,'###,##0') + ' Visits'
        
        INSERT INTO [#member_visit_stats] ([stat_order], [stat_category], [stat_subcategory], [stat_sort_value], 
                                           [stat_value], [stat_metric], [stat_display])
        SELECT 1, 
               'Grand Total',
               'Households',
               '000000000000002',
               @Total_households,
               'Members',
               FORMAT(@Total_households,'###,##0') + ' Members'

        INSERT INTO [#member_visit_stats] ([stat_order], [stat_category], [stat_subcategory], [stat_sort_value], 
                                           [stat_value], [stat_metric], [stat_display])
        SELECT 1, 
               'Grand Total',
               'Avg/Household',
               '000000000000003',
               @total_visits / @Total_households, --CONVERT(DECIMAL(18,2), (COUNT(*) / COUNT(DISTINCT [customer_no]))),
               'Visits',
               FORMAT((@total_visits / @Total_households),'###,##0') + ' Visits'
               --FORMAT(CONVERT(DECIMAL(18,2), (COUNT(*) / COUNT(DISTINCT [customer_no]))),'###,##0') + ' Visits'
        --FROM [#member_visit_data]

        INSERT INTO [#member_visit_stats] ([stat_order], [stat_category], [stat_subcategory], [stat_sort_value], 
                                           [stat_value], [stat_metric], [stat_display])
        SELECT 1, 
               'Grand Total',
               'Avg/Household*',
               '000000000000004',
               (@Total_Visits / @total_memberships), --  (CONVERT(DECIMAL(18,2), COUNT(*)) / @total_memberships),
               CASE WHEN @Total_Visits / @total_memberships <= 1 THEN 'Visit' ELSE 'Visits' END,
               FORMAT((@Total_Visits / @total_memberships),'###,##0') + CASE WHEN (@Total_Visits / @total_memberships) <= 1 THEN ' Visit' ELSE ' Visits' END
               --FORMAT((CONVERT(DECIMAL(18,2), COUNT(*)) / @total_members),'###,##0') + CASE WHEN COUNT(*) = 1 THEN ' Visit' ELSE ' Visits' END
        --FROM [#member_visit_data]
              
        
    /* Statistic # 2: Most and Fewest Visits */
      
        INSERT INTO [#member_visit_stats] ([stat_order], [stat_category], [stat_subcategory], [stat_sort_value], 
                                           [stat_value], [stat_metric], [stat_display])
        SELECT TOP (1) 2,
                       'Total Visits', 
                       'Fewest', 
                        FORMAT(COUNT(DISTINCT [perf_date]),'000000000000000'),
                        COUNT(DISTINCT [perf_date]),
                        CASE WHEN COUNT(DISTINCT [perf_date]) = 1 THEN ' Visit'
                                ELSE 'Visits' END,
                        FORMAT(COUNT(DISTINCT [perf_date]),'###,##0') 
                             + CASE WHEN COUNT(DISTINCT [perf_date]) = 1 THEN ' Visit'
                                ELSE 'Visits' END
        FROM [#member_visit_data]
        GROUP BY [customer_no]
        ORDER BY COUNT(DISTINCT [perf_date])

        INSERT INTO [#member_visit_stats] ([stat_order], [stat_category], [stat_subcategory], [stat_sort_value], 
                                           [stat_value], [stat_metric], [stat_display])
        SELECT TOP (1) 2,
                       'Total Visits', 
                       'Most', 
                        FORMAT(COUNT(DISTINCT [perf_date]),'000000000000000'),
                        COUNT(DISTINCT [perf_date]),
                        CASE WHEN COUNT(DISTINCT [perf_date]) = 1 THEN ' Visit'
                                    ELSE ' Visits' END,
                        FORMAT(COUNT(DISTINCT [perf_date]),'###,##0')
                                  + CASE WHEN COUNT(DISTINCT [perf_date]) = 1 THEN ' Visit'
                                    ELSE ' Visits' END
        FROM [#member_visit_data]
        GROUP BY [customer_no]
        ORDER BY COUNT(DISTINCT [perf_date]) DESC;


    /*  Statistic # 3: Individual Membership Visit Counts  */

        WITH [CTE_VISIT_COUNT] ([customer_no], [range_low], [stat_display])
        AS (SELECT [customer_no],
                   CASE WHEN COUNT(DISTINCT [perf_date]) = 1 THEN 1
                        WHEN COUNT(DISTINCT [perf_date]) = 2 THEN 2
                        WHEN COUNT(DISTINCT [perf_date]) = 3 THEN 3
                        WHEN COUNT(DISTINCT [perf_date]) = 4 THEN 4
                        WHEN COUNT(DISTINCT [perf_date]) = 5 THEN 5
                        WHEN COUNT(DISTINCT [perf_date]) BETWEEN 6 AND 10 THEN 6
                        WHEN COUNT(DISTINCT [perf_date]) BETWEEN 11 AND 15 THEN 11
                        WHEN COUNT(DISTINCT [perf_date]) BETWEEN 16 AND 20 THEN 16
                        ELSE 21 END,
                   CASE WHEN COUNT(DISTINCT [perf_date]) = 1 THEN 'One Visit'
                        WHEN COUNT(DISTINCT [perf_date]) = 2 THEN 'Two Visits'
                        WHEN COUNT(DISTINCT [perf_date]) = 3 THEN 'Three Visits'
                        WHEN COUNT(DISTINCT [perf_date]) = 4 THEN 'Four Visits'
                        WHEN COUNT(DISTINCT [perf_date]) = 5 THEN 'Five Visits'
                        WHEN COUNT(DISTINCT [perf_date]) BETWEEN 6 AND 10 THEN '6 to 10 Visits'
                        WHEN COUNT(DISTINCT [perf_date]) BETWEEN 11 AND 15 THEN '11 to 15 Visits'
                        WHEN COUNT(DISTINCT [perf_date]) BETWEEN 16 AND 20 THEN '16 to 20 Visits'
                        ELSE 'More than 20 Visits' END
            FROM [#member_visit_data]
            GROUP BY [customer_no])

                INSERT INTO [#member_visit_stats] ([stat_order], [stat_category], [stat_subcategory], [stat_sort_value], 
                                                   [stat_value], [stat_metric], [stat_display])
                SELECT 3,
                       'Number of Visits',
                       [stat_display],
                       FORMAT([range_low],'000000000000000'),
                       COUNT(*),
                       CASE WHEN COUNT(*) = 1 THEN ' Member'
                                   ELSE ' Members' END,
                       FORMAT(COUNT(*),'###,##0')
                            + CASE WHEN COUNT(*) = 1 THEN ' Member'
                                   ELSE ' Members' END
                FROM [CTE_VISIT_COUNT]
                GROUP BY [stat_display], [range_low]

                 --Determine what members did not visit in the designated time frame and insert a "No Visits" Row for Stat # 3
           
                SELECT @zero_visits = COUNT(DISTINCT [mem].[customer_no])
                FROM [#all_active_members] AS [mem]
                     LEFT OUTER JOIN [#member_visit_data] AS [vis] ON [vis].[customer_no] = [mem].[customer_no]
                WHERE [vis].[customer_no] IS NULL

                INSERT INTO [#member_visit_stats] ([stat_order], [stat_category], [stat_subcategory], [stat_sort_value], 
                                                   [stat_value], [stat_metric], [stat_display])
                SELECT 3,
                       'Number of Visits',
                       'No Visits',
                       '000000000000000',
                       @zero_visits,
                       CASE WHEN @zero_visits = 1 THEN ' Member'
                                   ELSE ' Members' END,
                       FORMAT(@zero_visits,'###,##0')
                            + CASE WHEN @zero_visits = 1 THEN ' Member'
                                   ELSE ' Members' END

    /* Statistic # 4: Visit Counts by Membership Level  */               

       INSERT INTO [#member_visit_stats] ([stat_order], [stat_category], [stat_subcategory], [stat_sort_value], 
                                          [stat_value], [stat_metric], [stat_display])
       SELECT 4,
              'Visits by Type',
              [memb_Level_name],
              FORMAT([memb_level_no],'000000000000000'),
              COUNT(*),
              CASE WHEN COUNT(*) = 1 THEN ' Visit'
                                   ELSE ' Visits' END,
              FORMAT(COUNT(*),'###,##0')
                            + CASE WHEN COUNT(*) = 1 THEN ' Visit'
                                   ELSE ' Visits' END
       FROM [#member_visit_data]
       GROUP BY [memb_level_name], [memb_level_no]
        

    /*  Statistic # 5: Visit Counts by Membership Level  */               

        IF @include_member_output = 'Y'
            INSERT INTO [#member_visit_stats] ([stat_order], [stat_category], [stat_subcategory], [stat_sort_value], 
                                               [stat_value], [stat_metric], [stat_display])
            SELECT 5,
                   'Visits by Member',
                   CAST([customer_no] AS VARCHAR(10)),
                   [customer_name],
                   COUNT(*),
                   CASE WHEN COUNT(*) = 1 THEN ' Visit'
                                 ELSE ' Visits' END,
                   FORMAT(COUNT(*),'###,##0')
                          + CASE WHEN COUNT(*) = 1 THEN ' Visit'
                                 ELSE ' Visits' END
            FROM [#member_visit_data]
            GROUP BY [customer_no], [customer_name]
            ORDER BY COUNT(*) DESC


    FINISHED:
    
        /*  Return Final Statistical Information  */
        SELECT [stat_id],
               [stat_order],
               [stat_category],
               [stat_subcategory],
               [stat_sort_value],
               [stat_value],
               [stat_metric],
               [stat_display]
        FROM [#member_visit_stats]
        ORDER BY [stat_order], [stat_sort_value], [stat_subcategory]
        
    DONE:

END
GO
 
--FOR TESTING
--EXECUTE [dbo].[LRP_MEMBER_VISIT_STATS] @report_start_dt = '7-1-2019', @report_end_dt = '12-31-2019', @membership_org_str  = '"4","13"', @include_member_output = 'N'
    