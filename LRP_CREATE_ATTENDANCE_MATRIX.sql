USE [impresario]
GO
/****** Object:  StoredProcedure [dbo].[LRP_CREATE_ATTENDANCE_MATRIX]    Script Date: 10/1/2021 10:09:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[LRP_CREATE_ATTENDANCE_MATRIX]
        @report_start_dt DATETIME,
        @report_end_dt DATETIME,
        @public_prod_no INT,
        @school_prod_no INT
AS BEGIN

        SET NOCOUNT ON;
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
       
    /*  Procedure Variables  */
    
        DECLARE @title_no INT = 0, @title_name VARCHAR(30) = '', @prod_no INT = 0, @prod_name VARCHAR(30) = ''
        DECLARE @start_dt DATE, @end_dt DATE, @day_count INT = 0, @omni_buyout_prod_no INT = 0

        DECLARE @perf_type_buyout INT = 5   --From [TR_PERF_TYPE] table

    /*  Temporary Tables  */

        IF OBJECT_ID('tempdb..#raw_attend_data') is not null DROP TABLE [#raw_attend_data]

        CREATE TABLE [#raw_attend_data] ([prod_no] INT NOT NULL DEFAULT (0),
                                         [prod_name] VARCHAR(50) NOT NULL DEFAULT (0),
                                         [perf_date] DATE NULL,
                                         [perf_time] VARCHAR(10) NOT NULL DEFAULT (''),
                                         [perf_dt] DATETIME NULL,
                                         --[perform_type] VARCHAR(30) NOT NULL DEFAULT (''),
                                         [attendance_type] VARCHAR(30) NOT NULL DEFAULT ('Public'),
                                         [sale_total] INT NOT NULL DEFAULT (1),
                                         [paid_amount] DECIMAL (18,2) NOT NULL DEFAULT (0.0))

        IF OBJECT_ID('tempdb..#attend_data') is not null DROP TABLE [#attend_data]

        CREATE TABLE [#attend_data] ([title_no] INT NOT NULL DEFAULT (0), 
                                     [title_name] VARCHAR(30) NOT NULL DEFAULT (''), 
                                     [attendance_type] VARCHAR(30) NOT NULL DEFAULT (''),
                                     [prod_no] INT NOT NULL DEFAULT (0), 
                                     [prod_name] VARCHAR(30) NOT NULL DEFAULT (''), 
                                     [perf_date] DATE NULL, 
                                     [show_count] INT NOT NULL DEFAULT (0), 
                                     [show_count_running] INT NOT NULL DEFAULT (0),
                                     [sale_total] INT NOT NULL DEFAULT (0),
                                     [sale_total_running] INT NOT NULL DEFAULT (0),
                                     [paid_amount] DECIMAL (18,2) NOT NULL DEFAULT (0.0),
                                     [paid_amount_running] DECIMAL (18,2) NOT NULL DEFAULT (0.0))

        IF OBJECT_ID('tempdb..#attend_final') is not null DROP TABLE [#attend_final]

         CREATE TABLE [#attend_final] ([rec_no] INT NOT NULL IDENTITY (1,1),
                                       [title_no] INT NOT NULL DEFAULT (0), 
                                       [title_name] VARCHAR(30) NOT NULL DEFAULT (''), 
                                       [prod_no] INT NOT NULL DEFAULT (0), 
                                       [prod_name] VARCHAR(30) NOT NULL DEFAULT (''), 
                                       [perf_date] DATE NULL, 
                                       [perf_day] VARCHAR(30) NOT NULL DEFAULT (''),
                                       [show_count] INT NOT NULL DEFAULT (0), 
                                       [show_count_running] INT NOT NULL DEFAULT (0),
                                       [cmp_sale_total] INT NOT NULL DEFAULT (0),
                                       [cmp_sale_total_running] INT NOT NULL DEFAULT (0),
                                       [pub_sale_total] INT NOT NULL DEFAULT (0),
                                       [pub_sale_total_running] INT NOT NULL DEFAULT (0),
                                       [pub_paid_amount] DECIMAL (18,2) NOT NULL DEFAULT (0.0),
                                       [pub_paid_amount_running] DECIMAL (18,2) NOT NULL DEFAULT (0.0),
                                       [sch_show_count] INT NOT NULL DEFAULT (0), 
                                       [sch_show_count_running] INT NOT NULL DEFAULT (0),
                                       [sch_sale_total] INT NOT NULL DEFAULT (0),
                                       [sch_sale_total_running] INT NOT NULL DEFAULT (0),
                                       [sch_paid_amount] DECIMAL (18,2) NOT NULL DEFAULT (0.0),
                                       [sch_paid_amount_running] DECIMAL (18,2) NOT NULL DEFAULT (0.0),
                                       [buy_show_count] INT NOT NULL DEFAULT (0),
                                       [buy_show_count_running] INT NOT NULL DEFAULT (0),
                                       [buy_sale_total] INT NOT NULL DEFAULT (0),
                                       [buy_sale_total_running] INT NOT NULL DEFAULT (0),
                                       [buy_paid_amount] DECIMAL (18,2) NOT NULL DEFAULT (0.0),
                                       [buy_paid_amount_running] DECIMAL (18,2) NOT NULL DEFAULT (0.0),                                       
                                       [tot_sale_total] INT NOT NULL DEFAULT (0),
                                       [tot_sale_total_running] INT NOT NULL DEFAULT (0),
                                       [tot_paid_amount] DECIMAL (18,2) NOT NULL DEFAULT (0.0),
                                       [tot_paid_amount_running] DECIMAL (18,2) NOT NULL DEFAULT (0.0));

    /*  Check Parameters  */

        IF @public_prod_no > 0 SELECT @prod_no = @public_prod_no
        ELSE SELECT @prod_no = @school_prod_no

        SELECT @prod_name = ISNULL([description], 'Unknown Title') FROM [dbo].[T_INVENTORY] WHERE [inv_no] = @prod_no

        SELECT @title_no = ISNULL([title_no],0) FROM [dbo].[T_PRODUCTION] WHERE [prod_no] = @prod_no
        SELECT @title_name = ISNULL([description], '') FROM [dbo].[T_INVENTORY] WHERE [inv_no] = @title_no

        SELECT @start_dt = CAST(@report_start_dt AS DATE),
               @end_dt = CAST(@report_end_dt AS DATE)

        SELECT @omni_buyout_prod_no = ISNULL(MAX([inv_no]), 0) FROM [dbo].[T_INVENTORY] WHERE [type] = 'P' AND [description] = 'Omni Buyout'

    /*  Get All the Raw Data  */

        --Public Attendance
        IF @public_prod_no > 0
            INSERT INTO [#raw_attend_data] ([prod_no], [prod_name], [perf_date], [perf_time], [perf_dt], [attendance_type], [sale_total], [paid_amount])
            SELECT @prod_no,
                   @prod_name,
                   his.[performance_date],
                   his.[perf_time],
                   his.[performance_date] + ' ' + his.[perf_time],
                   CASE WHEN his.[performance_type] = 'Buyout' THEN 'Buyout'
                    WHEN mos.[description] LIKE '%school%' THEN 'School'
                    ELSE 'Public' END,
                   his.[sale_total],
                   his.[paid_amt]
                 FROM [dbo].[LT_HISTORY_TICKET] AS his
                      INNER JOIN [dbo].[T_ORDER] AS ord ON ord.[order_no] = his.[order_no]
                      INNER JOIN [dbo].[TR_MOS] AS mos ON mos.[id] = ord.[mos]
            WHERE his.[performance_date] BETWEEN CONVERT(CHAR(10),@start_dt,111) AND CONVERT(CHAR(10),@end_dt,111)
              AND his.[production_no] = @public_prod_no;

            --Omni Buyout Attendance
            --Added specifically for the Omni Contract Report...Using a Mapping table, this will convert
            --Performances in the "Omni Buyout" production to actual attendance for the show that was shown
            --It does not do this for any other venue.
                      
            WITH [CTE_BUYOUTS] AS (SELECT CONVERT(CHAR(10),[performance_dt],111) AS [performance_date], 
                                          CONVERT(CHAR(5),[performance_dt],108) AS [performance_time], 
                                          [production_no] 
                                   FROM [dbo].[LTR_CONTRACT_BUYOUT_MAP]
                                   WHERE [performance_dt] BETWEEN @report_start_dt AND @report_end_dt)
            INSERT INTO [#raw_attend_data] ([prod_no], [prod_name], [perf_date], [perf_time], [perf_dt], [attendance_type], [sale_total], [paid_amount])
            SELECT @prod_no,
                   @prod_name,
                   his.[performance_date],
                   his.[perf_time],
                   his.[performance_date] + ' ' + his.[perf_time],
                   'Buyout',
                   his.[sale_total],
                   his.[paid_amt]
                 FROM [dbo].[LT_HISTORY_TICKET] AS his
                      INNER JOIN [CTE_BUYOUTS] AS buy ON buy.[performance_date] = his.[perf_date] 
                                                     AND buy.[performance_time] = his.[perf_time]
                                                     AND buy.[production_no] = @prod_no
            WHERE his.[performance_date] BETWEEN CONVERT(CHAR(10),@start_dt,111) AND CONVERT(CHAR(10),@end_dt,111)
              AND his.[production_no] = @omni_buyout_prod_no;


        --School Attendance
        IF @school_prod_no > 0
            INSERT INTO [#raw_attend_data] ([prod_no], [prod_name], [perf_date], [perf_time], [perf_dt], [attendance_type], [sale_total], [paid_amount])
            SELECT @prod_no,
                   @prod_name,
                   his.[performance_date],
                   his.[perf_time],
                   his.[performance_date] + ' ' + his.[perf_time],
                   CASE WHEN his.[performance_type] = 'Buyout' THEN 'Buyout'
                    WHEN mos.[description] LIKE '%school%' THEN 'School'
                    ELSE 'Public' END,
                   his.[sale_total],
                   his.[paid_amt]
                 FROM [dbo].[LT_HISTORY_TICKET] AS his
                      INNER JOIN [dbo].[T_ORDER] AS ord ON ord.[order_no] = his.[order_no]
                      INNER JOIN [dbo].[TR_MOS] AS mos ON mos.[id] = ord.[mos]
            WHERE his.[performance_date] BETWEEN CONVERT(CHAR(10),@start_dt,111) AND CONVERT(CHAR(10),@end_dt,111)
              AND his.[production_no] = @school_prod_no;

    /*  Aggregate data with running totals - separate data sets for Public and Schools and Complimentary
        NOTE: School attendance is determined based on mode of sale on the oder - not school only productions  */

        --Public Attendance
        WITH CTE_ATTEND_PUBLIC ([prod_no], [prod_name], [perf_date], [show_count], [sale_total], [paid_amount]) AS 
                                (SELECT @prod_no,
                                        @prod_name,
                                        [perf_date],
                                        COUNT(DISTINCT [perf_dt]),
                                        SUM([sale_total]),
                                        SUM([paid_amount])
                                FROM [#raw_attend_data]   
                                 WHERE [attendance_type] = 'Public'
                                 GROUP BY [perf_date])
        INSERT INTO [#attend_data] ([title_no], [title_name], [attendance_type], [prod_no], [prod_name], [perf_date], [show_count], 
                                    [show_count_running], [sale_total], [sale_total_running], [paid_amount], [paid_amount_running])
        SELECT @title_no,
               @title_name,
               'Public',
               @prod_no,
               @prod_name,
               cte.[perf_date],
               cte.[show_count],
               SUM(ctr.[show_count]) AS [show_count_running],
               cte.[sale_total],
               SUM(ctr.[sale_total]) AS [sale_total_running],
               cte.[paid_amount],
               SUM(ctr.[paid_amount]) AS [paid_amoun_running]
        FROM [CTE_ATTEND_PUBLIC] AS cte
             INNER JOIN [CTE_ATTEND_PUBLIC] AS ctr ON ctr.[perf_date] <= cte.[perf_date]
        GROUP BY cte.[perf_date], cte.[show_count], cte.[sale_total], cte.[paid_amount]
        ORDER BY cte.[perf_date];
        
        --School Attendance   
        WITH [CTE_ATTEND_SCHOOL] ([prod_no], [prod_name], [perf_date], [show_count], [sale_total], [paid_amount]) AS
                                  (SELECT @prod_no,
                                          @prod_name,
                                          [perf_date],
                                          COUNT(DISTINCT [perf_dt]),
                                          SUM([sale_total]),
                                          SUM([paid_amount])
                                   FROM [#raw_attend_data]
                                   WHERE [attendance_type] = 'School'
                                   GROUP BY [perf_date])
        INSERT INTO [#attend_data] ([title_no], [title_name], [attendance_type], [prod_no], [prod_name], [perf_date], [show_count], 
                                    [show_count_running], [sale_total], [sale_total_running], [paid_amount], [paid_amount_running])
        SELECT @title_no,
               @title_name,
               'School',
               @prod_no,
               @prod_name,
               cte.[perf_date],
               cte.[show_count],
               SUM(ctr.[show_count]) AS [show_count_running],
               cte.[sale_total],
               SUM(ctr.[sale_total]) AS [sale_total_running],
               cte.[paid_amount],
               SUM(ctr.[paid_amount]) AS [paid_amoun_running]
        FROM [CTE_ATTEND_SCHOOL] AS cte
             INNER JOIN [CTE_ATTEND_SCHOOL] AS ctr ON ctr.[perf_date] <= cte.[perf_date]
        GROUP BY cte.[perf_date], cte.[show_count], cte.[sale_total], cte.[paid_amount]
        ORDER BY cte.[perf_date];

        --Buyout Attendance
        WITH CTE_ATTEND_BUYOUT ([prod_no], [prod_name], [perf_date], [show_count], [sale_total], [paid_amount]) AS 
                                (SELECT @prod_no,
                                        @prod_name,
                                        [perf_date],
                                        COUNT(DISTINCT [perf_dt]),
                                        SUM([sale_total]),
                                        SUM([paid_amount])
                                FROM [#raw_attend_data]   
                                 WHERE [attendance_type] = 'Buyout'
                                 GROUP BY [perf_date])
        INSERT INTO [#attend_data] ([title_no], [title_name], [attendance_type], [prod_no], [prod_name], [perf_date], [show_count], 
                                    [show_count_running], [sale_total], [sale_total_running], [paid_amount], [paid_amount_running])
        SELECT @title_no,
               @title_name,
               'Buyout',
               @prod_no,
               @prod_name,
               cte.[perf_date],
               cte.[show_count],
               SUM(ctr.[show_count]) AS [show_count_running],
               cte.[sale_total],
               SUM(ctr.[sale_total]) AS [sale_total_running],
               cte.[paid_amount],
               SUM(ctr.[paid_amount]) AS [paid_amoun_running]
        FROM [CTE_ATTEND_BUYOUT] AS cte
             INNER JOIN [CTE_ATTEND_BUYOUT] AS ctr ON ctr.[perf_date] <= cte.[perf_date]
        GROUP BY cte.[perf_date], cte.[show_count], cte.[sale_total], cte.[paid_amount]
        ORDER BY cte.[perf_date];

        --Comp Attendance   
        WITH [CTE_ATTEND_COMP] ([prod_no], [prod_name], [perf_date], [show_count], [sale_total], [paid_amount]) AS
                                (SELECT @prod_no,
                                        @prod_name,
                                        [perf_date],
                                        COUNT(DISTINCT [perf_dt]),
                                        SUM([sale_total]),
                                        SUM([paid_amount])
                                 FROM [#raw_attend_data]
                                 WHERE [paid_amount] = 0.0
                                 GROUP BY [perf_date])
        INSERT INTO [#attend_data] ([title_no], [title_name], [attendance_type], [prod_no], [prod_name], [perf_date], [show_count], 
                                    [show_count_running], [sale_total], [sale_total_running], [paid_amount], [paid_amount_running])
        SELECT @title_no,
               @title_name,
               'Comp',
               @prod_no,
               @prod_name,
               cte.[perf_date],
               cte.[show_count],
               SUM(ctr.[show_count]) AS [show_count_running],
               cte.[sale_total],
               SUM(ctr.[sale_total]) AS [sale_total_running],
               cte.[paid_amount],
               SUM(ctr.[paid_amount]) AS [paid_amoun_running]
        FROM [CTE_ATTEND_COMP] AS cte
             INNER JOIN [CTE_ATTEND_COMP] AS ctr ON ctr.[perf_date] <= cte.[perf_date]
        GROUP BY cte.[perf_date], cte.[show_count], cte.[sale_total], cte.[paid_amount]
        ORDER BY cte.[perf_date]
        
        
    /*  Combine public and school attendance into a single line for each date
        The LFT_INDIVIDUAL_DATES function makes sure there is a line for every date
        in the range, even if there is no data for that date  */

        INSERT INTO [#attend_final] ([title_no], [title_name], [prod_no], [prod_name], [perf_date], [perf_day], [show_count], [show_count_running], 
                                     [cmp_sale_total], [cmp_sale_total_running], [pub_sale_total], [pub_sale_total_running], [pub_paid_amount], 
                                     [pub_paid_amount_running], [sch_show_count], [sch_show_count_running], [sch_sale_total], [sch_sale_total_running], 
                                     [sch_paid_amount], [sch_paid_amount_running],[buy_show_count], [buy_show_count_running], [buy_sale_total], 
                                     [buy_sale_total_running], [buy_paid_amount], [buy_paid_amount_running])
        SELECT @title_no,
               @title_name,
               @prod_no,
               @prod_name,
               CAST(dts.return_dt AS DATE) AS [perf_date],
               dts.[day_of_week] AS [perf_Day],
               ISNULL(pub.[show_count], 0),
               ISNULL(pub.[show_count_running], 0),
               ISNULL(cmp.[sale_total], 0),
               ISNULL(cmp.[sale_total_running], 0.0),
               ISNULL(pub.[sale_total], 0),
               ISNULL(pub.[sale_total_running], 0),
               ISNULL(pub.[paid_amount], 0),
               ISNULL(pub.[paid_amount_running], 0),
               ISNULL(sch.[show_count],0),
               ISNULL(sch.[show_count_running],0),
               ISNULL(sch.[sale_total], 0),
               ISNULL(sch.[sale_total_running], 0),
               ISNULL(sch.[paid_amount], 0),
               ISNULL(sch.[paid_amount_running], 0),
               ISNULL(buy.[show_count],0),
               ISNULL(buy.[show_count_running],0),
               ISNULL(buy.[sale_total], 0),
               ISNULL(buy.[sale_total_running], 0),
               ISNULL(buy.[paid_amount], 0),
               ISNULL(buy.[paid_amount_running], 0)
        FROM [dbo].[LFT_INDIVIDUAL_DATES](@start_dt, @end_dt) AS dts 
             LEFT OUTER JOIN [#attend_data] AS pub ON CAST(pub.[perf_date] AS DATE) = CAST(dts.[return_dt] AS DATE) AND pub.[attendance_type] = 'Public'
             LEFT OUTER JOIN [#attend_data] AS sch ON CAST(sch.[perf_date] AS DATE) = CAST(dts.[return_dt] AS DATE) AND sch.[attendance_type] = 'School'
             LEFT OUTER JOIN [#attend_data] AS buy ON CAST(buy.[perf_date] AS DATE) = CAST(dts.[return_dt] AS DATE) AND buy.[attendance_type] = 'Buyout'
             LEFT OUTER JOIN [#attend_data] AS cmp ON CAST(cmp.[perf_date] AS DATE) = CAST(dts.[return_dt] AS DATE) AND cmp.[attendance_type] = 'Comp'
        ORDER BY  CAST(dts.return_dt AS DATE)

    /*  If there is no data for a given day, the running totals on those days come back at zero
        All the insert statements above have ORDER BY clauses to that the data always gets inserted in date order
        The final table has a unique sequential id number.  This series of updates will populate a zero running total
        with whatever value is in the previous record so that the running total carries over even if nothing is added to it.
        Using a counter makes sure the code can never get stuck in any kind of infinite loop.  Once the update statement
        has been run a number of times equal to the number of days in the date range, it stops even if it still things
        there is data to update.
    */

        --Update the show count running total
        SELECT @day_count = DATEDIFF(DAY, @start_dt, @end_dt)

        WHILE EXISTS (SELECT * FROM [#attend_final] WHERE [show_count_running] = 0) AND @day_count > 0 BEGIN

            UPDATE afc
            SET afc.[show_count_running] = ISNULL(afp.[show_count_running],0)
            FROM [#attend_final] AS afc
                 LEFT OUTER JOIN [#attend_final] AS afp ON afp.[rec_no] = (afc.[rec_no] - 1)
            WHERE afc.[show_count_running] = 0
                       
            SELECT @day_count -= 1

        END

        --Update the comp attendance running total
        SELECT @day_count = DATEDIFF(DAY, @start_dt, @end_dt)

        WHILE EXISTS (SELECT * FROM [#attend_final] WHERE [cmp_sale_total_running] = 0) AND @day_count > 0 BEGIN

            UPDATE afc
            SET afc.[cmp_sale_total_running] = ISNULL(afp.[cmp_sale_total_running], 0)
            FROM [#attend_final] AS afc
                 LEFT OUTER JOIN [#attend_final] AS afp ON afp.[rec_no] = (afc.[rec_no] - 1)
            WHERE afc.[cmp_sale_total_running] = 0
                       
            SELECT @day_count -= 1

        END

        --Update the public attendance and revenue running totals
        SELECT @day_count = DATEDIFF(DAY, @start_dt, @end_dt)

        WHILE EXISTS (SELECT * FROM [#attend_final] WHERE [pub_sale_total_running] = 0) AND @day_count > 0 BEGIN

            UPDATE afc
            SET afc.[pub_sale_total_running] = ISNULL(afp.[pub_sale_total_running], 0),
                afc.[pub_paid_amount_running] = ISNULL(afp.[pub_paid_amount_running], 0.0)
            FROM [#attend_final] AS afc
                 LEFT OUTER JOIN [#attend_final] AS afp ON afp.[rec_no] = (afc.[rec_no] - 1)
            WHERE afc.[pub_sale_total_running] = 0

            SELECT @day_count -= 1

        END

        --Update the school attendance and revenue running totals.
        SELECT @day_count = DATEDIFF(DAY, @start_dt, @end_dt)

        WHILE EXISTS (SELECT * FROM [#attend_final] WHERE [sch_sale_total_running] = 0) AND @day_count > 0 BEGIN

            UPDATE afc
            SET afc.[sch_sale_total_running] = ISNULL(afp.[sch_sale_total_running], 0),
                afc.[sch_paid_amount_running] = ISNULL(afp.[sch_paid_amount_running], 0.0)
            FROM [#attend_final] AS afc
                 LEFT OUTER JOIN [#attend_final] AS afp ON afp.[rec_no] = (afc.[rec_no] - 1)
            WHERE afc.[sch_sale_total_running] = 0

            SELECT @day_count -= 1

        END

        --Update the buyout attendance and revenue running totals.
        SELECT @day_count = DATEDIFF(DAY, @start_dt, @end_dt)

        WHILE EXISTS (SELECT * FROM [#attend_final] WHERE [buy_sale_total_running] = 0) AND @day_count > 0 BEGIN

            UPDATE afc
            SET afc.[buy_sale_total_running] = ISNULL(afp.[buy_sale_total_running], 0),
                afc.[buy_paid_amount_running] = ISNULL(afp.[buy_paid_amount_running], 0.0)
            FROM [#attend_final] AS afc
                 LEFT OUTER JOIN [#attend_final] AS afp ON afp.[rec_no] = (afc.[rec_no] - 1)
            WHERE afc.[buy_sale_total_running] = 0

            SELECT @day_count -= 1

        END

    /*  Update the total (public + school) columns  */

        UPDATE [#attend_final]
        SET [tot_sale_total] = ([pub_sale_total] + [sch_sale_total] + [buy_sale_total]),
            [tot_sale_total_running] = ([pub_sale_total_running] + [sch_sale_total_running] + [buy_sale_total_running]),
            [tot_paid_amount] = ([pub_paid_amount] + [sch_paid_amount] + [buy_paid_amount]),
            [tot_paid_amount_running] = ([pub_paid_amount_running] + [sch_paid_amount_running] + [buy_paid_amount_running])

            --NOTE: There is no tot_show_count column because it is always the same as the show_count column
            --      sch_show_count is a subset of the show_count and does not get added to the show count.
            --      If there is a show count of 4 and a sch_show_count of 3, that means that of the 4 shows 
            --      that ran that day, three of them contained school attendance.
            
    FINISHED:

        /*  Produce the final data set  */

            SELECT [title_no], 
                   [title_name], 
                   [prod_no], 
                   [prod_name], 
                   [perf_date], 
                   [perf_day],
                   [show_count], 
                   [show_count_running],
                   [cmp_sale_total],
                   [cmp_sale_total_running],
                   [pub_sale_total],
                   [pub_sale_total_running],
                   [pub_paid_amount],
                   [pub_paid_amount_running],
                   [sch_show_count], 
                   [sch_show_count_running],
                   [sch_sale_total],
                   [sch_sale_total_running],
                   [sch_paid_amount],
                   [sch_paid_amount_running],
                   [buy_show_count], 
                   [buy_show_count_running],
                   [buy_sale_total],
                   [buy_sale_total_running],
                   [buy_paid_amount],
                   [buy_paid_amount_running],
                   [tot_sale_total],
                   [tot_sale_total_running],
                   [tot_paid_amount],
                   [tot_paid_amount_running]
            FROM [#attend_final]
            ORDER BY [perf_date]

    DONE:

        /*  Clean up  */

            IF OBJECT_ID('tempdb..#attend_final') is not null DROP TABLE [#attend_final]
            IF OBJECT_ID('tempdb..#attend_data') is not null DROP TABLE [#attend_data]
            IF OBJECT_ID('tempdb..#raw_attend_data') is not null DROP TABLE [#raw_attend_data]        

END
