USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_AVAILABLE_CAPACITY_SUMMARY]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LRP_AVAILABLE_CAPACITY_SUMMARY]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/*  7-5-2019:  Updated procedure to include title parameter
               Also rewrote a little of the code to no longer need a cursor
*/

CREATE PROCEDURE [dbo].[LRP_AVAILABLE_CAPACITY_SUMMARY]
        @report_start_dt datetime = NULL,
        @report_end_dt datetime = NULL,
        @include_thrill_ride char(1) = 'N',
        @days_included varchar (25) = 'All Days',
        @title_str VARCHAR(4000) = ''
AS BEGIN

    SET NOCOUNT ON;
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    
    /*  Procedure Variables  */

        DECLARE @perf_no int, @perf_time varchar(8), @zone_no int, @report_run_dt datetime
        DECLARE @total_cap int, @hold_cap int, @soft_cap int, @sold_cap int, @avail_cap int, @perf_ina char(1)
    
        DECLARE @title_list TABLE ([title_no] INT NOT NULL DEFAULT(0))

        SELECT @report_run_dt = getdate()


    /*  Temporary Tables  */

        IF OBJECT_ID('tempdb..#work_table') IS NOT NULL DROP TABLE [#work_table]

        CREATE TABLE [#work_table] ([title_no] INT NOT NULL DEFAULT (0),
                                    [title_name] VARCHAR(30) NOT NULL DEFAULT (''), 
                                    [perf_no] INT NOT NULL DEFAULT (0), 
                                    [perf_code] varchar(10) NOT NULL DEFAULT (''), 
                                    [perf_dt] DATETIME NULL, 
                                    [perf_day] VARCHAR(30) NOT NULL DEFAULT (''), 
                                    [perf_date] CHAR(10) NOT NULL DEFAULT (''), 
                                    [perf_time] CHAR(8) NOT NULL DEFAULT (''), 
                                    [prod_name] VARCHAR(30) NOT NULL DEFAULT (''), 
                                    [prod_name_long] VARCHAR(150) NOT NULL DEFAULT (''), 
                                    [zone_map_no] INT NOT NULL DEFAULT (0), 
                                    [zone_map_name] VARCHAR(30) NOT NULL DEFAULT (''), 
                                    [zone_no] INT NOT NULL DEFAULT (0),
                                    [total_capacity] INT NOT NULL DEFAULT (0), 
                                    [held_seats] INT NOT NULL DEFAULT (0), 
                                    [soft_capacity] INT NOT NULL DEFAULT (0), 
                                    [tix_sold] INT NOT NULL DEFAULT (0), 
                                    [tix_avail] INT NOT NULL DEFAULT (0), 
                                    [inactive] CHAR(1) NOT NULL DEFAULT (''))


        IF OBJECT_ID('tempdb..#results_table') IS NOT NULL DROP TABLE [#results_table]

        CREATE TABLE [#results_table] ([report_run_dt] DATETIME NULL, 
                                       [report_message] VARCHAR(100) NOT NULL DEFAULT (''), 
                                       [title_no] INT NOT NULL DEFAULT (0),
                                       [title_name] VARCHAR(30) NOT NULL DEFAULT (''), 
                                       [prod_name] VARCHAR(200) NOT NULL DEFAULT (''), 
                                       [perf_date] VARCHAR(10) NOT NULL DEFAULT (''), 
                                       [perf_time] VARCHAR(8) NOT NULL DEFAULT (''), 
                                       [title_order] INT NOT NULL DEFAULT (0), 
                                       [hard_capacity] INT NOT NULL DEFAULT (0), 
                                       [soft_capacity] INT NOT NULL DEFAULT (0), 
                                       [tix_sold] INT NOT NULL DEFAULT (0), 
                                       [percent_sold] DECIMAL(18,2) NOT NULL DEFAULT (0.0), 
                                       [tickets_available] INT NOT NULL DEFAULT (0), 
                                       [record_count] INT NOT NULL DEFAULT (0))

    /*  Check Parameters  */

        --If Null Values passed to date parameter, set to today
        SELECT @report_start_dt = ISNULL(@report_start_dt, GETDATE())
        SELECT @report_end_dt = ISNULL(@report_end_dt, @report_start_dt)
        
        --Make sure start date starts at 00:00:00 and end date ends at 23:59:59.997
        SELECT @report_start_dt = CAST(@report_start_dt AS DATE),
               @report_end_dt = CONVERT(CHAR(10),@report_end_dt,111) + ' 23:59:59.997'

        --If Any Value other than 'Y' is passed to include Thrill Ride parameter, set to N
        IF ISNULL(@include_thrill_ride,'') <> 'Y' SELECT @include_thrill_ride = 'N'

        --If Null Value passed to days included parameter, set to All Days
        SELECT @days_included = ISNULL(@days_included,'All Days')

        --Parse Title List if any
        IF ISNULL(@title_str,'') <> ''
            INSERT INTO @title_list ([title_no])
            SELECT TRY_CONVERT(INT,Element) 
            FROM [dbo].[FT_SPLIT_LIST](REPLACE(@title_str,'"',''),',')
        ELSE
            INSERT INTO @title_list ([title_no])
            SELECT [title_no]
            FROM [dbo].[T_TITLE] 
            WHERE [title_no] NOT IN (42, 61, 1126, 1395, 1398, 5541, 7179, 7314, 53191)

        --Delete any zero values
        DELETE FROM @title_list WHERE [title_no] = 0

        --Delete or add thrill ride based on @include_thrill_ride parameter
        IF @include_thrill_ride = 'N' 
            DELETE FROM @title_list WHERE [title_no] = 1343

        ELSE IF NOT EXISTS (SELECT * FROM @title_list WHERE [title_no] = 1343)
            INSERT INTO @title_list ([title_no]) VALUES (1343)

    /*  Insert data into the work table  */
 
        INSERT INTO #work_table ([title_no], [title_name], [perf_no], [perf_code], [perf_dt], [perf_day], [perf_date], [perf_time], [prod_name], 
                                 [prod_name_long], [zone_map_no], [zone_map_name], [zone_no], [total_capacity], [held_seats], [tix_sold])
        SELECT prf.[title_no],
               prf.[title_name], 
               prf.[performance_no], 
               prf.[performance_code], 
               prf.[performance_dt], 
               DATENAME(weekday,prf.[performance_dt]), 
               prf.[performance_date], 
               prf.[performance_time], 
               prf.[production_name], 
               prf.[production_name_long], 
               prf.[performance_zone_map], 
               prf.[performance_zone_map_name], 
               prf.[performance_zone],
               ISNULL([dbo].[LF_GetCapacity] (prf.[performance_no], prf.[performance_zone], 'total'),0),
               ISNULL([dbo].[LF_GetCapacity] (prf.[performance_no], prf.[performance_zone], 'hold'),0),
               ISNULL([dbo].[LF_GetCapacity] (prf.[performance_no], prf.[performance_zone], 'sold'),0)
        FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] AS prf
             INNER JOIN @title_list AS ttl ON ttl.[title_no] = prf.[title_no]
        WHERE prf.[performance_dt] between @report_start_dt and @report_end_dt
          AND ISNULL(prf.[performance_zone],0) > 0

    /*  Delete unwanted days from the work table */

             IF @days_included = 'Weekdays Only' DELETE FROM #work_table WHERE perf_day IN ('Saturday','Sunday')
        ELSE IF @days_included = 'Weekends Only' DELETE FROM #work_table WHERE perf_day NOT IN ('Saturday','Sunday')

    /*  Update the status of each performance  */

        UPDATE wrk
        SET wrk.inactive = sta.[inactive]
        FROM [#work_table] AS wrk
             LEFT OUTER JOIN [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE_STATUS] AS sta ON sta.[performance_no] = wrk.perf_no 
                                                                                     AND sta.[zone_no] = wrk.[zone_no]

    /*  Delete inactive performances  */
    
        DELETE FROM [#work_table]
        WHERE [inactive] = 'Y'

    /*  Aggregate Capacity  */

        UPDATE [#work_table]
        SET [soft_capacity] = ([total_capacity] - [held_seats])

        UPDATE [#work_table]
        SET [tix_avail] = ([soft_capacity] - [tix_sold])

    /*  Aggregate titles  */

        INSERT INTO [#results_table] ([report_run_dt], [report_message], [title_no], [title_name], [prod_name], [perf_date], [perf_time], [title_order],
                                      [hard_capacity], [soft_capacity], [tix_sold], [percent_sold], [tickets_available], [record_count])
        SELECT @report_run_dt, 
               '', 
               [title_no],
               [title_name], 
               [prod_name], 
               [perf_date], 
               [perf_time], 
               0, 
               SUM([total_capacity]), 
               SUM([soft_capacity]), 
               SUM([tix_sold]),  
               CASE WHEN sum([soft_capacity]) = 0 THEN 0.00
                    ELSE (sum([tix_sold]) / sum([soft_capacity])) END,
               (SUM([soft_capacity]) - SUM([tix_sold])), 
               COUNT([prod_name])
        FROM [#work_table]
        GROUP BY [title_no], [title_name], [prod_name], [perf_date], [perf_time]
    
    /*  Delete buyouts from the results table  */

        DELETE FROM #results_table WHERE prod_name like '%Buyout%'

    /*  Assign each main Title a primary sequential number in the order we want them to appear on the report.
        Titles not specifically listed will be assigned 99 and sort alphabetically at the end of the report.  */

        UPDATE [#results_table] SET [title_order] = CASE [title_name]
                                                    WHEN 'Exhibit Halls' THEN 1
                                                    WHEN 'Mugar Omni Theater' THEN 2
                                                    WHEN 'Hayden Planetarium' THEN 3
                                                    WHEN '4-D Theater' THEN 4
                                                    WHEN 'Butterfly Garden' THEN 5
                                                    WHEN 'Thrill Ride 360' THEN 6
                                                    WHEN 'School Lunch' THEN 7
                                                    WHEN 'Cahners Theater' THEN 8
                                                    WHEN 'Current Science & Technology' THEN 9
                                                    WHEN 'Adult Offerings' THEN 10
                                                    ELSE 99 END
    
    DONE:

        /*  If no records exist in the results table, add a single record with a "No Records Found Message  */

            IF not exists (SELECT * FROM #results_table) 
                INSERT INTO [#results_table] ([report_run_dt], [report_message], [title_no], [title_name], [prod_name], [perf_date], [perf_time], [title_order],
                                              [hard_capacity], [soft_capacity], [tix_sold], [percent_sold], [tickets_available], [record_count])
                VALUES (@report_run_dt, 
                        'No records found for criteria entered.', 
                        0,
                        '', 
                        '', 
                        '', 
                        '', 
                        0, 
                        0, 
                        0, 
                        0, 
                        0, 
                        0, 
                        0)
            
        /*  Final selection from the results table to be displayed on the report  */

            SELECT [report_run_dt], 
                   [report_message], 
                   [perf_date], 
                   [title_no],
                   [title_name],  
                   [perf_time], 
                   [prod_name], 
                   [title_order], 
                   [soft_capacity] AS [total_capacity],
                   [tix_sold], 
                   [percent_sold], 
                   [tickets_available], 
                   [record_count]
            FROM #results_table 
            ORDER BY [perf_date], [title_order], [title_name], [perf_time], [prod_name]

    CLEAN_UP:

        /*  Delete the work and results tables after we are done using them  */

            IF OBJECT_ID('tempdb..#results_table') IS NOT NULL DROP TABLE [#results_table]

            IF OBJECT_ID('tempdb..#work_table') IS NOT NULL DROP TABLE [#work_table]

END
GO

GRANT EXECUTE ON [dbo].[LRP_AVAILABLE_CAPACITY_SUMMARY] TO impusers
GO

EXECUTE [dbo].[LRP_AVAILABLE_CAPACITY_SUMMARY] '7-6-2019', '7-6-2019', 'Y', 'All Days', '27,161'



--SELECT * FROM dbo.T_INVENTORY WHERE type = 'T' AND inv_no NOT IN (42, 61, 1126, 1395, 1398, 5541, 7179, 7314, 53191)