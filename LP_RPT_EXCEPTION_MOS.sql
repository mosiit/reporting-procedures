USE impresario
GO

DECLARE @performance_dt datetime,
        @title_name varchar(30)
        
    --SELECT @title_name = 'Mugar Omni Theater'
    --SELECT @title_name = 'Hayden Planetarium'
    --SELECT @title_name = '4-D Theater'

    DECLARE @ttl_nm varchar(30)
    DECLARE @exceptions_table_mos table ([exception_no] int IDENTITY (1,1), [exception_level] varchar(30), [exception_type] varchar(30), [exception_notes] varchar(255), [title_no] int, [title_name] varchar(30), 
                                         [production_name] varchar(30), [performance_no] int, [performance_dt] datetime, [performance_code] varchar(10), [performance_type_name] varchar(30), [mode_of_sale_no] int, 
                                         [mode_of_sale_name] varchar(30), [start_dt] datetime, [start_dt_should_be] datetime, [end_dt] datetime, [end_dt_should_be] datetime)

    SELECT @title_name = IsNull(@title_name, 'All')

    IF @title_name = 'All' BEGIN

        DECLARE TitleCursor INSENSITIVE CURSOR FOR
        SELECT [description] FROM [dbo].[T_Inventory] WHERE [type] = 'T' and [inv_no] in (SELECT [tessitura_title] FROM [dbo].[LTR_TITLE_MOS_MAP]) ORDER BY [description]
        OPEN TitleCursor    
        BEGIN_TITLE_LOOP:

            FETCH NEXT FROM TitleCursor INTO @ttl_nm 
            IF @@FETCH_STATUS = -1 GOTO END_TITLE_LOOP

            INSERT INTO @exceptions_table_mos EXECUTE [dbo].[LP_RPT_EXCEPTION_MOS_SINGLE_VENUE] @title_name = @ttl_nm

            GOTO BEGIN_TITLE_LOOP

        END_TITLE_LOOP:
        CLOSE TitleCursor
        DEALLOCATE TitleCursor

    END ELSE BEGIN

        INSERT INTO @exceptions_table_mos EXECUTE [dbo].[LP_RPT_EXCEPTION_MOS_SINGLE_VENUE] @title_name = @title_name

    END



    --SELECT [performance_no], [mode_of_sale_no], end_dt, end_dt_should_be FROM @exceptions_table_mos  WHERE exception_type = 'Incorrect End Date'

    --SELECT * FROM TX_PERF_PKG_MOS 
        
    -- SELECT performance_dt, production_name, performance_code, mode_of_sale_name, end_dt, end_dt_should_be, datediff(day,end_dt,end_dt_should_be) FROM @exceptions_table_mos WHERE exception_type = 'Incorrect End Date'

    --SELECT FROM @exceptions_table_mos

    DELETE FROM @exceptions_table_mos WHERE mode_of_sale_name in ('Web Members')


    --SELECT DISTINCT title_name, production_name, performance_dt, mode_of_sale_name, exception_type FROM @exceptions_table_mos

    SELECT exception_type, title_name, production_name, mode_of_sale_name, count(DISTINCT performance_code) as performances FROM @exceptions_table_mos
    GROUP BY exception_type, title_name, production_name, mode_of_sale_name ORDER BY exception_type, title_name, production_name, mode_of_sale_name

    --SELECT title_name, exception_type, performance_dt, production_name, performance_code, mode_of_sale_name,
    --   CASE WHEN start_dt is null THEN '' WHEN start_dt = start_dt_should_be THEN '' ELSE convert(varchar(25),start_dt) END as 'start_dt', 
    --   CASE WHEN start_dt_should_be is null THEN '' WHEN start_dt = start_dt_should_be THEN '' ELSE convert(varchar(25),start_dt_should_be) END as 'start_dt_should_be',
    --   CASE WHEN end_dt is null THEN '' WHEN end_dt = end_dt_should_be THEN '' ELSE convert(varchar(25),end_dt) END as 'end_dt', 
    --   CASE WHEN end_dt_should_be is null THEN '' WHEN end_dt = end_dt_should_be THEN '' ELSE convert(varchar(25),end_dt_should_be) END as 'end_dt_should_be',
    --   exception_notes
    --FROM @exceptions_table_mos 
    ----WHERE (exception_type = 'Incorrect Start and End Date' and (datediff(day,start_dt_should_be,start_dt) between -1 and 1 and datediff(day,end_dt_should_be,end_dt) between -1 and 1))
    ----    or (exception_type = 'Incorrect Start Date' and datediff(day,start_dt_should_be,start_dt) between -1 and 1)
    ----    or (exception_type = 'Incorrect End Date' and datediff(day,end_dt_should_be,end_dt) between -1 and 1)
    ----    --or (exception_type = 'Invalid Mode of Sale' and mode_of_sale_no = 15)
    --ORDER BY title_name, exception_level, exception_type, performance_dt


    --DROP TABLE [TemporaryData].[dbo].[rpt_exceptions_mode_of_sale]
    
    --SELECT * INTO [TemporaryData].[dbo].[rpt_exceptions_mode_of_sale] FROM @exceptions_table_mos 

    --SELECT * FROM [TemporaryData].[dbo].[rpt_exceptions_mode_of_sale]

    --SELECT count(*) FROM @exceptions_table_mos
    --SELECT count(*) FROM @exceptions_table_mos WHERE exception_type = 'Invalid mode of sale' and mode_of_sale_no = 15
    --SELECT * INTO [TemporaryData].[dbo].[rpt_exceptions_mode_of_sale] FROM @exceptions_table_mos 
    --SELECT * FROM @exceptions_table_mos 
    --INSERT INTO @exceptions_table_mos EXECUTE LP_RPT_EXCEPTION_MOS_SINGLE_VENUE @title_name = '4-D Theater'
    --INSERT INTO @exceptions_table_mos EXECUTE LP_RPT_EXCEPTION_MOS_SINGLE_VENUE @title_name = 'Butterfly Garden'
    --INSERT INTO @exceptions_table_mos EXECUTE LP_RPT_EXCEPTION_MOS_SINGLE_VENUE @title_name = 'Hayden Planetarium'
    --INSERT INTO @exceptions_table_mos EXECUTE LP_RPT_EXCEPTION_MOS_SINGLE_VENUE @title_name = 'Mugar Omni Theater'


    