USE impresario
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LP_RPT_CASHOUT_PAYMENTS]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LP_RPT_CASHOUT_PAYMENTS]
GO

CREATE PROCEDURE [dbo].[LP_RPT_CASHOUT_PAYMENTS]
        @rpt_dt datetime,
        @rpt_operator varchar(8)
AS BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

    DECLARE @rpt_date char(10), @rpt_message varchar(100), @report_Start datetime
    DECLARE @pay_table table ([transaction_no] int, [customer_no] int, [payment_date] char(10), [payment_time] char(8), [payment_amount] decimal(18,2), 
                              [payment_method_name] varchar(30), [payment_type_name] varchar(30), [account_no] int, [check_no] int, [check_name] varchar(30), 
                              [card_expiry_dt] datetime, [auth_no] varchar(30), [payment_operator] varchar(8), [operator_first] varchar(50), [operator_last] varchar(50),
                              [operator_location] varchar(50), [batch_no] int, [elapsed_time] int, [rpt_message] varchar(100))

    SELECT @report_start = getdate()
    SELECT @rpt_message = '', @rpt_date = ''

    SELECT @rpt_operator = IsNull(@rpt_operator,'')
    
    IF @rpt_dt is null
        SELECT @rpt_message = 'error: invalid date passed to report.'
    ELSE BEGIN

        SELECT @rpt_date = convert(char(10),@rpt_dt,111)
    
        IF @rpt_operator = '' or not exists (SELECT * FROM [dbo].[T_METUSER] WHERE [userid] = @rpt_operator)
            SELECT @rpt_message = 'error: invalid operator id passed to report (' + @rpt_operator + ').'

    END
        
    IF @rpt_message <> '' GOTO FINISHED

    INSERT INTO @pay_table 
    SELECT [transaction_no], [customer_no], [payment_date], [payment_time], sum([payment_amount]), [payment_method_name], [payment_type_name], [account_no], [check_no], [check_name], 
           [card_expiry_dt], [auth_no], [payment_operator], [payment_operator_first_name], [payment_operator_last_name], [payment_operator_location], [batch_no], 0,  @rpt_message
    FROM [dbo].[LV_RPT_CASHOUT_PAYMENTS] WHERE payment_date = convert(char(10),@rpt_dt,111) and payment_operator = @rpt_operator
    GROUP BY [transaction_no], [customer_no], [payment_date], [payment_time], [payment_method_name], [payment_type_name], [account_no], 
             [check_no], [check_name], [card_expiry_dt], [auth_no], [payment_operator], [payment_operator_first_name], [payment_operator_last_name], [payment_operator_location], [batch_no]


        /********************************************************************************/
        /*    IF ANY ADDITIONAL DATA MANIPULATION NEEDS TO BE ADDED, IT WILL GO HERE    */
        /********************************************************************************/

    
    FINISHED:

        IF not exists (SELECT * FROM @pay_table) BEGIN
            IF @rpt_message = '' SELECT @rpt_message = 'No payments found for ' + @rpt_operator + ' on ' + @rpt_date +  '.'
            INSERT INTO @pay_table VALUES (0, 0, @rpt_date, '', 0.00, '', '', 0, 0, '', null, '', @rpt_operator, '', '', '', 0, 0, @rpt_message)
        END
        
        UPDATE @pay_table SET [elapsed_time] = datediff(second,@report_start,getdate())

        SELECT [payment_date], [payment_operator], [operator_first], [operator_last], [operator_location], [payment_method_name], [payment_type_name], count(*) as 'payment_count', 
               sum([payment_amount]) as 'payment_amount', [elapsed_time], [rpt_message]
        FROM @pay_table
        GROUP BY [payment_date], [payment_operator], [operator_first], [operator_last], [operator_location], [payment_method_name], [payment_type_name], [elapsed_time], [rpt_message]
 
END
GO

GRANT EXECUTE ON [dbo].[LP_RPT_CASHOUT_PAYMENTS] TO impusers
GO

