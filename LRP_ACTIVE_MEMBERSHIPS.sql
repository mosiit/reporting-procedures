USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LRP_ACTIVE_MEMBERSHIPS]    Script Date: 9/19/2016 1:15:03 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[LRP_ACTIVE_MEMBERSHIPS]
	@MembershipActiveDate DATETIME,
	@category_str VARCHAR(MAX)  
AS

BEGIN 

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	-- DSJ 9/12/2016
	-- GET ACTIVE MEMBERSHIPS AS OF A DATE
	-- NEED TO LOOK IN BOTH VIEWS - LV_CURRENT_MEMBERSHIP_INFO AND LV_PAST_MEMBERSHIP_INFO
    -- MS 1/11/2018
    --    Changed selection date from inception date to initialize date
    --    Separated out Union selection into two separate statements to make it easier to exclude duplicates
    --    changed @ActiveMembershipsTbl table variable to #ActiveMembershipsTbl temp table with an index on customer_no
    --          NOTE: when table variable changed to temp table run time dropped from roughly 2:00 to 0:25
    

    DECLARE @MembCategory TABLE
        (
	        category VARCHAR(30) NOT NULL
        )

    -- IF @category_str is NULL, search ALL TR_MEMB_LEVEL_CATEGORY
    IF ISNULL(@category_str, '') <> '' BEGIN
	    INSERT INTO @MembCategory
	    SELECT REPLACE(CONVERT(VARCHAR(30),Element), CHAR(34), '') FROM dbo.FT_SPLIT_LIST (@category_str,',')
    END ELSE BEGIN
	    INSERT @MembCategory
    	SELECT description FROM TR_MEMB_LEVEL_CATEGORY
    END 

    IF OBJECT_ID('tempdb..#ActiveMembershipsTbl') IS NOT NULL DROP TABLE [#ActiveMembershipsTbl]

	CREATE TABLE #ActiveMembershipsTbl
	(
		 customer_no INT,
		 customer_display_name VARCHAR(200),
		 memb_level_category_description VARCHAR(30), 
		 memb_level_description VARCHAR(30), 
		 initiation_date DATETIME, 
		 expiration_date DATETIME, 
		 current_status_desc VARCHAR(30) 
	)

    CREATE CLUSTERED INDEX [ActiveMembershipsTbl_customer_no] ON [#ActiveMembershipsTbl] ([customer_no] ASC) ON [PRIMARY]

	INSERT INTO #ActiveMembershipsTbl
	(
		customer_no,
		customer_display_name,
		memb_level_category_description,
		memb_level_description,
		initiation_date,
		expiration_date,
		current_status_desc
	)
	SELECT customer_no, customer_display_name,memb_level_category_description, memb_level_description, initiation_date, expiration_date, 'Active' AS current_status_desc
	FROM dbo.LV_CURRENT_MEMBERSHIP_INFO info
		INNER JOIN @MembCategory cat ON info.memb_level_category_description = cat.category
	WHERE @MembershipActiveDate BETWEEN info.initiation_date AND expiration_date
	

    INSERT INTO #ActiveMembershipsTbl
	(
		customer_no,
		customer_display_name,
		memb_level_category_description,
		memb_level_description,
		initiation_date,
		expiration_date,
		current_status_desc
	)
    -- Don't want to double count any non-current memberships if they are already ACTIVE.
	SELECT DISTINCT customer_no, customer_display_name,memb_level_category_description, memb_level_description, initiation_date, expiration_date, current_status_desc
	FROM dbo.LV_PAST_MEMBERSHIP_INFO info
		INNER JOIN @MembCategory cat ON info.memb_level_category_description = cat.category
	WHERE @MembershipActiveDate BETWEEN initiation_date AND expiration_date
    AND customer_no NOT IN (SELECT customer_no FROM #ActiveMembershipsTbl) 

    
	-- Get counts of all Active MOS Members by category and level
	SELECT memb_level_category_description, memb_level_description, COUNT(*) AS cnt
	FROM #ActiveMembershipsTbl
	GROUP BY memb_level_category_description, memb_level_description
	ORDER BY memb_level_category_description, memb_level_description

    IF OBJECT_ID('tempdb..#ActiveMembershipsTbl') IS NOT NULL DROP TABLE [#ActiveMembershipsTbl]

END 
GO

 EXECUTE [dbo].[LRP_ACTIVE_MEMBERSHIPS] @MembershipActiveDate = '12-31-2020', @category_str = 'Household'  

