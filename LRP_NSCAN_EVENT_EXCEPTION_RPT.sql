USE [impresario];
GO

SET ANSI_NULLS ON;
GO

SET QUOTED_IDENTIFIER ON;

----GO
----DROP PROCEDURE [dbo].[LRP_EXCEPTION_NSCAN_EVENT_RPT] 

GO

CREATE PROCEDURE [dbo].[LRP_EXCEPTION_NSCAN_EVENT_RPT]
(
    @scan_dt_from DATETIME,
    @scan_dt_to DATETIME
)
AS
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

-- =============================================
-- Author: Aileen Duffy-Brown
-- Create date: 4/11/2018
-- Description:	Customized report that is based off of the 
-- current NSCAN event exception report in Tessitura used to
-- track down scanning issues for further follow up or investigation.
-- Copied code from NP_EVENT_EXCEPTION_RPT and LRP_MEMBER_VISIT_DTL_SCREEN
-- Custom version of the NSCAN event exception 
-- report to add additional information and remove others.
-- TrackIt! Work Order # 90547
-- =============================================


-- Find members who should be displaying attendance on custom tab.  
-- Copied code from the custom screen that displays this information.
DECLARE @member_attendance TABLE
(
    customer_no INT NOT NULL,
    attend_dt DATETIME NOT NULL,
    no_attended INT NOT NULL,
    mem_attend VARCHAR(1) NOT NULL
);

INSERT INTO @member_attendance
(
    customer_no,
    attend_dt,
    no_attended,
    mem_attend
)
SELECT DISTINCT
       att.customer_no,
       FORMAT(att.attend_dt, 'd', 'en-US') AS attend_dt,
       SUM(admission_adult) + SUM(admission_child) + SUM(admission_other) AS no_attended,
       mem_attend = CASE
                        WHEN SUM(admission_adult) + SUM(admission_child) + SUM(admission_other) > 0 THEN
                            'Y'
                        ELSE
                            'N'
                    END
FROM dbo.LV_PAST_MEMBERSHIP_INFO pmi
    INNER JOIN dbo.T_ATTENDANCE att
        ON att.cust_memb_no = pmi.customer_memb_no
           AND att.attend_dt
           BETWEEN pmi.initiation_date AND pmi.expiration_date
           AND att.cust_memb_no IS NOT NULL
WHERE pmi.customer_no = att.customer_no
      AND att.attend_dt >= @scan_dt_from
      AND att.attend_dt <= @scan_dt_to
GROUP BY att.attend_dt,
         att.customer_no;

--SELECT *
--FROM @member_attendance;

SELECT nsc.customer_no,
       nsc.update_dt AS scan_dt,
       cus.display_name AS customer,
       nsc.scan_str,
       sli.order_no,
       nsc.perf_no,
       per.perf_code,
       inv.description AS perf_name,
       per.perf_dt,
       nsc.ticket_msg,
       nsc.user_id,
       mem_attend = CASE
                        WHEN mem.no_attended > 0 THEN
                            'Y'
                        ELSE
                            'N'
                    END,
       mem.attend_dt,
       mem.no_attended
FROM T_NSCAN_EVENT_CONTROL AS nsc
    LEFT OUTER JOIN @member_attendance AS mem
        ON mem.customer_no = nsc.customer_no
           AND DATEDIFF(d, nsc.update_dt, mem.attend_dt) = 0
    LEFT OUTER JOIN [dbo].[FT_CONSTITUENT_DISPLAY_NAME]() AS cus
        ON nsc.customer_no = cus.customer_no
    LEFT OUTER JOIN dbo.T_ATTENDANCE AS att
        ON nsc.cust_memb_no = att.cust_memb_no
    LEFT OUTER JOIN T_PERF AS per
        ON nsc.perf_no = per.perf_no
    LEFT OUTER JOIN T_INVENTORY AS inv
        ON per.perf_no = inv.inv_no
    LEFT OUTER JOIN T_SEAT AS sea
        ON nsc.seat_no = sea.seat_no
    LEFT OUTER JOIN TR_SECTION AS sec
        ON sea.section = sec.id
    LEFT OUTER JOIN T_SUB_LINEITEM AS sli
        ON nsc.sli_no = sli.sli_no
WHERE nsc.ticket_msg NOT IN ( 'OK - Ticket already recorded', 'Ticket already recorded',
                              'Event not authorized on device'
                            )
      AND nsc.ticket_ok <> 'Y'
      AND nsc.update_mode = 'Y'
      AND nsc.update_dt >= @scan_dt_from
      AND nsc.update_dt <= @scan_dt_to;

--SELECT * FROM @nscan_scans

GO
GRANT EXECUTE ON [dbo].[LRP_EXCEPTION_NSCAN_EVENT_RPT] TO ImpUsers;
GO


