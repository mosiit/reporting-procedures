USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LRP_ATTEND_AVAILABLE_EXHIBITS_AND_SHOWS]    Script Date: 12/20/2019 2:36:11 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[LRP_ATTEND_AVAILABLE_EXHIBITS_AND_SHOWS]
	@weekly_end_dt DATE, -- keep as DATE type,
	@WhichYear VARCHAR(4) -- This or Last 

AS 
BEGIN

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

-- EXEC LRP_ATTEND_AVAILABLE_EXHIBITS_AND_SHOWS @weekly_end_dt = '3/24/2019', @WhichYear = 'This'

DECLARE @reportDt DATE
IF @WhichYear = 'Last'
	SELECT @reportDt = [prv_week_end_dt]
    FROM [dbo].[LFT_ATTENDANCE_WEEK_DATES] (@weekly_end_dt, 1, 'Y')
ELSE 
	SELECT @reportDt = @weekly_end_dt
 
-- GET TEMPORARY EXHIBIT INFORMATION FROM LOCAL TABLE
SELECT 
	id,
	exhibit_name,
	'Exhibit' AS venue,
	@reportDt report_date
	--,NULL as perf_dt
FROM LTR_TEMPORARY_EXHIBITS
WHERE DATEADD(DAY, -6, @reportDt) >= opening_date AND   
	@reportDt <= ISNULL(closing_date,GETDATE())

UNION

-- GET ALL OMNI, PLANETARIUM AND 4D FROM TESSITURA PRODUCTIONS & PERFORMANCE SCHEDULES
SELECT DISTINCT 
	inv2.inv_no AS title_no,
	CASE 
		WHEN inv2.description LIKE '4D: %' THEN SUBSTRING(inv2.description, 5, 30)
		ELSE inv2.description
	END AS exhibit_name,
	CASE 
		WHEN con.value = 'Entertainment' THEN 'Entertainment'
		WHEN inv.short_name = 'Adult Offerings' THEN 'Entertainment'
		WHEN inv.short_name = 'PLANETARIUM' THEN LEFT(inv.short_name,1) + LOWER(SUBSTRING(inv.short_name,2,10)) 
		ELSE inv.short_name
	END AS venue,
	@reportDt report_date
	--,p.perf_dt 
FROM T_PERF p
INNER JOIN dbo.T_PROD_SEASON psea
	ON p.prod_season_no = psea.prod_season_no
INNER JOIN dbo.T_PRODUCTION prod
	ON prod.prod_no = psea.prod_no
INNER JOIN dbo.T_INVENTORY inv
	ON inv.inv_no = prod.title_no
	AND inv.type = 'T' -- Title
	AND 
	(
		inv_no IN (161, 173, 1132) -- Mugar Omni Theater, 4-D Theater, Hayden Planetarium
		OR 
		inv_no = 5542 -- Adult Offerings
	)
INNER JOIN dbo.T_INVENTORY inv2
	ON inv2.inv_no = prod.prod_no
	AND inv2.type = 'P' -- Production
	AND inv2.inv_no <> 235 -- Omni Show
LEFT JOIN dbo.T_ZMAP ZM 
	ON P.zmap_no = ZM.zmap_no
LEFT JOIN [dbo].[TX_INV_CONTENT] AS con 
	ON con.[inv_no] = prod.[prod_no] 
	AND con.[content_type] = 37 -- Production Type
	AND prod.[title_no] = 1132 -- Hayden Planetarium
	AND con.[value] = 'Entertainment'
WHERE p.perf_type NOT IN (3, 5) -- School Only, Buyouts 
	AND CAST(perf_dt AS DATE) BETWEEN DATEADD(DAY, -6, @reportDt) AND @reportDt

END 

GO


