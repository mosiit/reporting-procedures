USE impresario;
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_ADV_STW_PROFILE_SUB_FUND_CONTR]') AND type in (N'P', N'PC'))
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_ADV_STW_PROFILE_SUB_FUND_CONTR] AS';
GO

EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LRP_ADV_STW_PROFILE_SUB_FUND_CONTR] TO [impusers], [tessitura_app]';
GO

ALTER PROCEDURE [dbo].[LRP_ADV_STW_PROFILE_SUB_FUND_CONTR]
        @customer_no INT = NULL
AS BEGIN

    SET NOCOUNT ON;
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

    /*  Procedure Variables and Temp Tables  */

        CREATE TABLE [#gifts] ([reference_no] INT,                      [customer_no] INT,                   [name] VARCHAR (100),
                               [type] CHAR(1),                          [creditee_type] VARCHAR (100),       [cont_date] DATETIME,
                               [fiscal_year] INT,                       [fiscal_quarter] INT,                [fiscal_period] INT,
                               [contribution_amount] DECIMAL (18,2),    [received_amount] DECIMAL (18,2),    [anonymous] CHAR(1),
                               [soft_credit_type] VARCHAR (100),        [fund_description] VARCHAR (100),    [printable_fund] VARCHAR (100),
                               [designation] VARCHAR (100),             [overall_campaign] VARCHAR (100),    [campaign] VARCHAR (100),
                               [campaign_category] VARCHAR (100),       [account_goal] VARCHAR (100),        [account_group] VARCHAR (100),
                               [appeal] VARCHAR (100),                  [media] VARCHAR (100),               [source] VARCHAR (100),
                               [notes] VARCHAR (255),                   [cancel] CHAR(1),                    [pledge_status_desc] VARCHAR (100),
                               [billing_type] VARCHAR (100),            [batch] INT,                         [KG_xfer_dt] DATETIME,
                               [KGift_desc] VARCHAR (100),              [stock_ticker] VARCHAR (100),        [contribution_detail1] VARCHAR (100),
                               [contribution_detail2] VARCHAR (100),    [agreement_date] VARCHAR (100),      [challenge_earned] VARCHAR (100),
                               [pg_instrument] VARCHAR (100),           [solicitation] VARCHAR (100),        [solicitor_number] INT,
                               [solicitor_name] VARCHAR (100),          [cpf_flag] CHAR(1),                  [exh_prog_cat] VARCHAR (100),
                               [full_fund_name] VARCHAR (100),          [cm_category1] VARCHAR (100),        [cm_intermediate1] VARCHAR (100),
                               [cm_pillar1] VARCHAR (100),              [cm_category2] VARCHAR (100),        [cm_intermediate2] VARCHAR (100),
                               [cm_category3] VARCHAR (100),            [cm_intermediate3] VARCHAR (100),    [custom_2] VARCHAR (100),
                               [custom_9] VARCHAR (100),                [main_customer_type] VARCHAR (100),  [original_customer_type] VARCHAR (100),
                               [sort_name] VARCHAR (100),               [channel] VARCHAR (100),             [max_date] DATE)

    /*  Run the Advancement Contribution Stewardship Detail report and place the results in the temp table  */

        INSERT INTO [#gifts] ([reference_no],[customer_no],[name],[type],[creditee_type],[cont_date],[fiscal_year],[fiscal_quarter],[fiscal_period],
                              [contribution_amount],[received_amount],[anonymous],[soft_credit_type],[fund_description],[printable_fund],[designation],
                              [overall_campaign],[campaign],[campaign_category],[account_goal],[account_group],[appeal],[media],[source],[notes],
                              [cancel],[pledge_status_desc],[billing_type],[batch],[KG_xfer_dt],[KGift_desc],[stock_ticker],[contribution_detail1],
                              [contribution_detail2],[agreement_date],[challenge_earned],[pg_instrument],[solicitation],[solicitor_number],
                              [solicitor_name],[cpf_flag],[exh_prog_cat],[full_fund_name],[cm_category1],[cm_intermediate1],[cm_pillar1],[cm_category2],
                              [cm_intermediate2],[cm_category3],[cm_intermediate3],[custom_2],[custom_9],[main_customer_type],[original_customer_type],
                              [sort_name],[channel])
        EXECUTE [dbo].[LRP_MOS_ADVANCEMENT_CONTRIBUTION_STEW_DETAIL] @customer_no = @customer_no;

    FINISHED:

                   
                   SELECT [customer_no],
                               [printable_fund],
                               SUM([contribution_amount]) AS [contribution_amt],
                               MAX(CAST([cont_date] AS DATE)) AS [latest_contriburion]
                        FROM [#gifts]
                        GROUP BY [customer_no], [printable_fund]
                        ORDER BY MAX(CAST([cont_date] AS DATE)) DESC, [printable_fund]
            --SELECT [reference_no],
            --    [customer_no],
            --    [name],
            --    [type],
            --    [creditee_type],
            --    [cont_date],
            --    [fiscal_year],
            --    [fiscal_quarter],
            --    [fiscal_period],
            --    [contribution_amount],
            --    [received_amount],
            --    [anonymous],
            --    [soft_credit_type],
            --    [fund_description],
            --    [printable_fund],
            --    [designation],
            --    [overall_campaign],
            --    [campaign],
            --    [campaign_category],
            --    [account_goal],
            --    [account_group],
            --    [appeal],
            --    [media],
            --    [source],
            --    [notes],
            --    [cancel],
            --    [pledge_status_desc],
            --    [billing_type],
            --    [batch],
            --    [KG_xfer_dt],
            --    [KGift_desc],
            --    [stock_ticker],
            --    [contribution_detail1],
            --    [contribution_detail2],
            --    [agreement_date],
            --    [challenge_earned],
            --    [pg_instrument],
            --    [solicitation],
            --    [solicitor_number],
            --    [solicitor_name],
            --    [cpf_flag],
            --    [exh_prog_cat],
            --    [full_fund_name],
            --    [cm_category1],
            --    [cm_intermediate1],
            --    [cm_pillar1],
            --    [cm_category2],
            --    [cm_intermediate2],
            --    [cm_category3],
            --    [cm_intermediate3],
            --    [custom_2],
            --    [custom_9],
            --    [main_customer_type],
            --    [original_customer_type],
            --    [sort_name],
            --    [channel]
                
            --FROM [#gifts]            
            


    DONE:

        DROP TABLE [#gifts] 

END
GO

EXECUTE [dbo].[LRP_ADV_STW_PROFILE_SUB_FUND_CONTR] @customer_no = 11964






















    --CREATE TABLE [#gifts2] ([customer_no] INT, [ref_no] INT, fund VARCHAR(100), cont_amt DECIMAL(18,2), cont_dt DATETIME)

    --INSERT INTO [#gifts2] ([customer_no],[ref_no],[fund],[cont_amt],[cont_dt])
    --SELECT ctr.[customer_no], 
    --       ctr.[ref_no],
    --       ISNULL(fnd.[printable_fund], 'Fund # ' + CAST(ctr.[fund_no] AS varchar(10))),
    --       ctr.[cont_amt],
    --       CAST (ctr.[cont_dt] AS DATE)
    --FROM [dbo].[T_CONTRIBUTION] AS ctr
    --     LEFT OUTER JOIN [dbo].[LTR_FUND_DETAIL] AS fnd ON fnd.[fund_no] = ctr.[fund_no]
    --WHERE ctr.[customer_no] = @customer_no
    

    -- SELECT [customer_no], 
    --        [fund],
    --       SUM([cont_amt]) AS [total_contributions],
    --       CAST(MAX([cont_dt]) AS DATE) AS [most_recent]
    --FROM [#gifts2]
    --WHERE [customer_no] = @customer_no
    --GROUP BY [customer_no], [fund]
    


    --SELECT ctr.[customer_no], 
    --       ISNULL(fnd.[printable_fund], 'Fund # ' + CAST(ctr.[fund_no] AS varchar(10))) AS [fund_name],
    --       SUM(ctr.[cont_amt]) AS [total_contributions],
    --       CAST(MAX(ctr.[cont_dt]) AS DATE) AS [most_recent]
    --FROM [dbo].[T_CONTRIBUTION] AS ctr
    --     LEFT OUTER JOIN [dbo].[LTR_FUND_DETAIL] AS fnd ON fnd.[fund_no] = ctr.[fund_no]
    --WHERE ctr.[customer_no] = @customer_no
    --GROUP BY ctr.[customer_no], ISNULL(fnd.[printable_fund], 'Fund # ' + CAST(ctr.[fund_no] AS varchar(10)))

    --SELECT * FROM [#gifts]


    --SELECT * FROM [#gifts] WHERE [reference_no] NOT IN (SELECT ref_no FROM [#gifts2])
    --SELECT * FROM [#gifts2] WHERE [ref_no] NOT IN (SELECT reference_no FROM [#gifts])

    --SELECT * FROM [dbo].[T_CONTRIBUTION] WHERE [ref_no] IN (1726843,2283817)
    --SELECT * FROM [dbo].[T_CONTRIBUTION] WHERE [ref_no] = 2252803

    
    --SELECT * FROM LV_MOS_CHART_OF_ACCOUNTS WHERE [fund_no] = 2800 AND [campaign_no] = 2063
    --SELECT * FROM [dbo].[LTR_FUND_DETAIL] WHERE [fund_no] IN (1316,1374,1388,2330)
    --SELECT * FROM [dbo].[T_CAMPAIGN] WHERE [campaign_no] IN (1, 4, 11)
