USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_ATTENDANCE_SCHOOL]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LRP_ATTENDANCE_SCHOOL]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LRP_ATTENDANCE_SCHOOL]
        @report_start_dt datetime,
        @report_end_dt datetime
AS BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

        /*  Procedure Variables  */

        DECLARE @rpt_message VARCHAR(100) = ''

        DECLARE @go_live_date CHAR(10) = '2016/05/18'

        DECLARE @rpt_start_date CHAR(10), @rpt_end_date CHAR(10),
                @his_start_date CHAR(10), @his_end_date CHAR(10),
                @arc_start_date CHAR(10), @arc_end_date CHAR(10)

    /*  Temporary Table  */

        IF OBJECT_ID('tempdb..#attendance') IS NOT NULL DROP TABLE [#attendance]

        CREATE TABLE [#attendance] ([attendance_type] VARCHAR(50), [performance_type] VARCHAR(30), [title_name] VARCHAR(30), [production_name] VARCHAR(30), 
                                    [production_name_short] VARCHAR(30), [production_name_long] VARCHAR(150), [comp_code_name] VARCHAR(30), [order_payment_status] VARCHAR(30), 
                                    [sale_total] INT, [scan_admission_total] INT, [due_amt] DECIMAL(18,2), [paid_amt] decimal(18,2), [report_message] VARCHAR(100))

    /*  Check Parameters  */

        SELECT @report_start_dt = ISNULL(@report_start_dt,DATEADD(DAY, -1, GETDATE())),
               @report_end_dt = ISNULL(@report_end_dt,DATEADD(DAY,-1,GETDATE()))

        IF @report_end_dt < @report_start_dt BEGIN
            SELECT @rpt_message = 'Invalid dates - End Date Before Start date.'
            GOTO FINISHED
        END

        SELECT @rpt_start_date = CONVERT(CHAR(10),@report_start_dt,111),
               @rpt_end_date = CONVERT(CHAR(10),@report_end_dt,111)

        IF @rpt_start_date < @go_live_date AND @rpt_end_date < @go_live_date
            SELECT @his_start_date = '',
                   @his_end_date = '',
                   @arc_start_date = CONVERT(CHAR(10), @report_start_dt,111),
                   @arc_end_date = CONVERT(CHAR(10),@report_end_dt,111)
        ELSE IF @rpt_start_date > @go_live_date AND @rpt_end_date > @go_live_date
            SELECT @his_start_date = CONVERT(CHAR(10), @report_start_dt,111),
                   @his_end_date = CONVERT(CHAR(10),@report_end_dt,111),
                   @arc_start_date = '',
                   @arc_end_date = ''
        ELSE
           SELECT @his_start_date = @go_live_date,
                  @his_end_date = CONVERT(CHAR(10), @report_end_dt,111),
                  @arc_start_date = CONVERT(CHAR(10), @report_start_dt,111),
                  @arc_end_date = CONVERT(CHAR(10), DATEADD(DAY,-1,CONVERT(DATETIME, @go_live_date)),111)


    /*  Get attendance data for dates on or after go live  */

        IF ISDATE(@his_start_date) = 1 AND ISDATE(@his_end_date) = 1
            INSERT INTO [#attendance]
	        SELECT [attendance_type], [performance_type], [title_name], [production_name], [production_name], [production_name], 
                   [comp_code_name], [order_payment_status], SUM([sale_total]) AS 'sale_total', SUM([scan_admission_total]) AS 'scan_admission_total', 
                   SUM([due_amt]) AS 'due_amt', SUM([paid_amt]) AS 'paid_amt', ''
            FROM [dbo].[LT_HISTORY_TICKET] (NOLOCK)
            WHERE [order_no] IN (SELECT [order_no] FROM [dbo].[LT_HISTORY_TICKET] WHERE [performance_date] BETWEEN @his_start_date AND @his_end_date
                                                                               AND ([performance_type] = 'School Only' OR [price_type_name] LIKE '%school%'))
              AND performance_date BETWEEN @his_start_date AND @his_end_date 
              AND [price_type_name] <> 'Internal Group'
            GROUP BY [attendance_type], [performance_type], [title_name], [production_name], [production_name], [comp_code_name], [order_payment_status]

    /*  Get attendance date for dates on or after go live  */

        IF ISDATE(@arc_start_date) = 1 AND ISDATE(@arc_end_date) = 1
            INSERT INTO [#attendance]
	        SELECT [attendance_type], [performance_type], [title_name], [production_name], [production_name], [production_name], 
               [comp_code_name], [order_payment_status], SUM([sale_total]) AS 'sale_total', SUM([scan_admission_total]) AS 'scan_admission_total', 
               SUM([due_amt]) AS 'due_amt', SUM([paid_amt]) AS 'paid_amt', ''
            FROM [dbo].[LT_HISTORY_TICKET_ARCHIVE] (NOLOCK)
            WHERE [order_no] IN (SELECT [order_no] FROM [dbo].[LT_HISTORY_TICKET_ARCHIVE] WHERE [performance_date] BETWEEN @arc_start_date AND @arc_end_date
                                                                                            AND ([performance_type] = 'School Only' OR [price_type_name] LIKE '%school%'))
              AND [performance_date] BETWEEN @arc_start_date AND @arc_end_date 
              AND [price_type_name] <> 'Internal Group'
            GROUP BY [attendance_type], [performance_type], [title_name], [production_name], [comp_code_name], [order_payment_status]
            HAVING SUM([sale_total]) <> 0


        UPDATE [#attendance] SET [performance_type] = 'School Attendance'

    FINISHED:

        IF NOT EXISTS (SELECT * FROM [#attendance])
            INSERT INTO [#attendance]
            VALUES  ('', '', '', '', '', '', '', '', 0, 0, 0.0, 0.0, 'No Data Found')


        SELECT [attendance_type], [report_message], [performance_type], [title_name], [production_name], [production_name_short], [production_name_long], [comp_code_name],
               '' AS 'price_type_name', [order_payment_status], SUM([sale_total]) AS 'sale_total', SUM([scan_admission_total]) AS 'scan_admission_total', 
               SUM([due_amt]) AS 'due_amt', SUM([paid_amt]) AS 'paid_amt', '' AS 'order_mode_of_sale'
        FROM [#attendance]
        GROUP BY [attendance_type], [report_message], [performance_type], [title_name], [production_name], [production_name_short], [production_name_long], [comp_code_name], [order_payment_status]
        ORDER BY title_name, production_name

END
GO

GRANT EXECUTE ON [dbo].[LRP_ATTENDANCE_SCHOOL] TO impusers
GO


