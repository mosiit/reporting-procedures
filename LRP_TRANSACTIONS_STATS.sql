--USE [impresario]
--GO

--IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_TRANSACTION_STATS]') AND type in (N'P', N'PC'))
--    DROP PROCEDURE [dbo].[LRP_TRANSACTION_STATS]
--GO

--SET ANSI_NULLS ON
--SET QUOTED_IDENTIFIER ON
--GO

--CREATE PROCEDURE [dbo].[LRP_TRANSACTION_STATS]
DECLARE @report_start_dt DATETIME = NULL,
        @report_end_dt DATETIME = NULL,
        @order_start_dt DATETIME = NULL,
        @order_end_dt DATETIME = NULL,
        @title_str VARCHAR(4000) = NULL,
        @production_str VARCHAR(4000) = NULL,
        @mode_of_sale_str varchar(4000) = NULL,
        @exclude_canceled_orders CHAR(1) = 'N'
--AS BEGIN

    /*  Title, Production, and Mode of Sale ID Tables  */

        DECLARE @title_table TABLE ([title_id] int)
        DECLARE @production_table TABLE ([production_id] INT)
        DECLARE @mode_of_sale_table TABLE ([mode_of_sale_id] INT)


    /*  Create table for the statistics
        NOTE: Tried to do this with a table variable.  Changing it to a temp table with an index shortened the run time from 51 seconds to 4.  */

        IF OBJECT_ID('tempdb..#trx_stat_table') IS NOT NULL DROP TABLE [#trx_stat_table]

        CREATE TABLE #trx_stat_table  ([order_no] int, [order_appeal_no] int, [order_appeal_name] VARCHAR(30), [order_customer_no] int, [order_customer_name] VARCHAR(100), [order_initiator_no] INT,
                                       [order_initiator_name] VARCHAR(100), [order_solicitor] VARCHAR(10), [order_solicitor_name] VARCHAR(100), [order_created_by] VARCHAR(10), [order_created_by_name] VARCHAR(100),
                                       [order_create_dt] DATETIME, [order_create_date] CHAR(10), [order_create_time] CHAR(8), [order_mode_of_sale] INT, [order_mode_of_sale_name] VARCHAR(30),
                                       [order_channel] INT, [order_channel_name] VARCHAR(30), [order_hold_until_dt] DATETIME, [order_hold_until_date] CHAR(10), [order_hold_until_time] CHAR(8),
                                       [order_is_annonymous] CHAR(1), [order_annonymous_indicator] VARCHAR(30), [order_counter_annonymous] INT, [order_counter_known] INT, [order_counter] INT,
                                       [order_total_due] DECIMAL (18,2), [order_total_paid] DECIMAL(18,2), [order_balance_due] DECIMAL(18,2), [order_transaction_number] INT,
                                       [order_sub_lineitems] INT, [order_notes] VARCHAR(255), customer_is_member CHAR(1))

        CREATE CLUSTERED INDEX [ix_trx_stat_table_order_annonymous_indicator] ON #trx_stat_table ([order_annonymous_indicator] ASC) ON [PRIMARY]

    /*  Check Parameters - If nulls passed to date parameters, run for yesterday.  */

        SELECT @report_start_dt = IsNull(@report_start_dt, dateadd(day, -1, getdate())),
               @report_end_dt = IsNull(@report_end_dt, dateadd(day, -1, getdate()))

        SELECT @title_str = ISNULL(@title_str,'')

        SELECT @production_str = ISNULL(@production_str,'')

        SELECT @mode_of_sale_str = ISNULL(@mode_of_sale_str,'')

        SELECT @exclude_canceled_orders = ISNULL(@exclude_canceled_orders,'N')

    /*  Make sure the date parameters include the full dates, starting at 00:00 and ending at 23:59  */

        SELECT @report_start_dt = convert(char(10),@report_start_dt,111) + ' 00:00:00',
               @report_end_dt = convert(char(10), @report_end_dt,111) + ' 23:59:59'

    /*  Parse out table, production, and mode of sale id numbers  */



    /*  Get order information  */

        INSERT INTO #trx_stat_table
        SELECT [order_no], [order_appeal_no], [order_appeal_name], [order_customer_no], [order_customer_name], [order_initiator_no], [order_initiator_name], [order_solicitor],
               [order_solicitor_name], [order_created_by], [order_created_by_name], [order_create_dt], [order_create_date], [order_create_time], [order_mode_of_sale], [order_mode_of_sale_name],
               [order_channel], [order_channel_name], [order_hold_until_dt], [order_hold_until_date], [order_hold_until_time], [order_is_annonymous], [order_annonymous_indicator], [order_counter_annonymous],
               [order_counter_known], [order_counter], [order_total_due], [order_total_paid], [order_balance_due], [order_transaction_number], 0, [order_notes], 'N'
        FROM [dbo].[LV_ORDER_INFORMATION]
        WHERE [order_create_dt] BETWEEN @report_start_dt AND @report_end_dt

    /*  If Order Dates are passed - Remove everything else  */

        IF @order_start_dt IS NOT NULL AND @order_end_dt IS NOT NULL
            DELETE FROM #trx_stat_table WHERE CONVERT(DATE,[order_create_dt]) < @order_start_dt OR CONVERT(DATE,[order_create_dt]) > @order_end_dt 


    /*  Combine all the various kiosks channels into one to shorten the report.  */

        UPDATE #trx_stat_table SET [order_channel_name] = 'Kiosk'
        WHERE [order_channel_name] like '%Kiosk%'

    /* Change order to Annonymous if either of the generic accounts are attached to the order (guest user for web sales and kiosk customer for kiosk orders)  */

        UPDATE #trx_stat_table
        SET [order_is_annonymous] = 'Y', [order_annonymous_indicator] = 'Annonymous', [order_counter_annonymous] = 1, [order_counter_known] = 0
        WHERE order_customer_name IN ('guest user','Kiosk Customer')

    /*  Count the subline items in the order that have one of the "paid" status codes (0 = canceled order)  */

        UPDATE #trx_stat_table
            SET [order_sub_lineitems] = (SELECT COUNT(DISTINCT [sli_no]) FROM [dbo].[T_SUB_LINEITEM] AS sli
                                         WHERE sli.[order_no] = [#trx_stat_table].order_no AND sli.[sli_status] IN (3, 6, 12))

        IF @exclude_canceled_orders = 'Y' DELETE FROM #trx_stat_table WHERE [order_sub_lineitems] = 0

    /*  Check the TX_CUST_MEMBERSHIPS TO SEE WHO HAD AN ACTIVE MEMBERSHIP ON THE DATE THE ORDER WAS CREATED  */

        UPDATE #trx_stat_table SET customer_is_member = 'Y', [order_annonymous_indicator] = 'Member'
        WHERE [order_customer_no] IN 
                (SELECT [order_customer_no]
                        FROM #trx_stat_table AS trx
                        INNER JOIN [dbo].[TX_CUST_MEMBERSHIP] AS mem (NOLOCK) ON mem.[customer_no] = trx.[order_customer_no] AND trx.[order_create_dt] BETWEEN mem.[init_dt] AND mem.[expr_dt])
        

    /*  Any order that is not annonymous or member is nonmember  */

        UPDATE #trx_stat_table SET [order_annonymous_indicator] = 'Nonmember' WHERE [order_annonymous_indicator] = 'Known'
        
    FINISHED:

        /*  Select final data set to be passed back to the report  */
    
            SELECT [order_no], [order_appeal_no], [order_appeal_name], [order_customer_no], [order_customer_name], [order_initiator_no], [order_initiator_name], [order_solicitor],
               [order_solicitor_name], [order_created_by], [order_created_by_name], [order_create_dt], [order_create_date], [order_create_time], [order_mode_of_sale], [order_mode_of_sale_name],
               [order_channel], [order_channel_name], [order_hold_until_dt], [order_hold_until_date], [order_hold_until_time], [order_is_annonymous], [order_annonymous_indicator], [order_counter_annonymous],
               [order_counter_known], [order_counter], [order_total_due], [order_total_paid], [order_balance_due], [order_transaction_number], [order_sub_lineitems], [order_notes], [customer_is_member]
            FROM #trx_stat_table

        
    DONE:

        /*  Cleanup  */

            IF OBJECT_ID('tempdb..#trx_stat_table') IS NOT NULL DROP TABLE [#trx_stat_table]

END
GO

GRANT EXECUTE ON [dbo].[LRP_TRANSACTION_STATS] TO ImpUsers
GO


--EXECUTE [dbo].[LRP_TRANSACTION_STATS] '12-1-2016', '12-31-2016', 'N'


        