USE impresario;
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_ADV_PM_ASSIGNMENT_SUMMARY]') AND type in (N'P', N'PC'))
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_ADV_PM_ASSIGNMENT_SUMMARY] AS';
GO

EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LRP_ADV_PM_ASSIGNMENT_SUMMARY] TO [impusers], [tessitura_app]';
GO

ALTER PROCEDURE [dbo].[LRP_ADV_PM_ASSIGNMENT_SUMMARY]
        @fiscal_year INT = 2021,
        @workers_str VARCHAR(4000) = 672463
AS BEGIN

    SET NOCOUNT ON;
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

    /*  Procedure Variables and Temp Tables  */

        DECLARE @assigned_prospects DECIMAL(18,2) = 0.0;
        DECLARE @worker_name VARCHAR(100) = ''
        DECLARE @worker_initials VARCHAR(50) = '';

        DECLARE @final_table TABLE ([row_num] INT NOT NULL DEFAULT (0),
                                    [worker_customer_no] INT NOT NULL DEFAULT (0),
                                    [worker_initials] VARCHAR(50) NOT NULL DEFAULT (''),
                                    [worker_name] varchar(100) NOT NULL DEFAULT (''),
                                    [row_description] VARCHAR(100) NOT NULL DEFAULT (''),
                                    [row_value] INT NOT NULL DEFAULT (8),
                                    [row_percent] DECIMAL (18,4) NOT NULL DEFAULT (0.0));


        IF OBJECT_ID('tempdb..#worker_plan_info') IS NOT NULL DROP TABLE [#worker_plan_info];

        CREATE TABLE [#worker_plan_info] ([plan_no] INT NOT NULL DEFAULT (0),
                                          [campaign_no] INT NOT NULL DEFAULT (0),
                                          [campaign_name] VARCHAR(30) NOT NULL DEFAULT (''),
                                          [campaign_category] INT NOT NULL DEFAULT (0),
                                          [campaign_category_name] VARCHAR(30) NOT NULL DEFAULT (''),
                                          [campaign_fiscal_year] INT NOT NULL DEFAULT (0),
                                          [customer_no] INT NOT NULL DEFAULT (0),
                                          [customer_name] VARCHAR(125) NOT NULL DEFAULT (''),
                                          [cont_designation] INT NOT NULL DEFAULT (0),
                                          [cont_designation_name] VARCHAR(30) NOT NULL DEFAULT (''),
                                          [status] INT NOT NULL DEFAULT (0),
                                          [status_name] VARCHAR(30) NOT NULL DEFAULT (''),
                                          [type] INT NOT NULL DEFAULT (0),
                                          [plan_type_name] VARCHAR(30) NOT NULL DEFAULT (''),
                                          [ask_dt] DATETIME NULL,
                                          [ask_dt_fiscal_year] INT NOT NULL DEFAULT (0));

    /*  Get Constants  */

        SELECT @assigned_prospects = COUNT(DISTINCT kvl.[customer_no])
                                           FROM [dbo].[TX_CUST_WORKER_TYPE] AS wtp
                                                INNER JOIN [dbo].[T_CUSTOMER] AS cus ON cus.[customer_no] = wtp.[worker_customer_no] 
                                                INNER JOIN [dbo].[TX_CUST_KEYWORD] AS kvl ON kvl.key_value = (cus.[lname] + ' ' + cus.[fname])
                                           WHERE wtp.[worker_type] = 7 
                                             AND kvl.[keyword_no] = 555
                                             AND wtp.[worker_customer_no] = @workers_str;

        SELECT @worker_name = ISNULL([fname], ''),
               @worker_initials = ISNULL([lname], '')
        FROM [dbo].[T_CUSTOMER] 
        WHERE [customer_no] = @workers_str;

    /*  Get all the plan information for the designated Prospect Manager (worker)  */
    
        WITH [CTE_PROSPECT_MANAGERS] ([customer_no], [worker_customer_no], [worker_initials], [worker_name], [key_value])
                                      AS (SELECT DISTINCT kvl.[customer_no],
                                                          wtp.[worker_customer_no],
                                                          cus.[sort_name], 
                                                          cus.[fname], 
                                                          kvl.[key_value]
                                          FROM [dbo].[TX_CUST_WORKER_TYPE] AS wtp
                                               INNER JOIN [dbo].[T_CUSTOMER] AS cus ON cus.[customer_no] = wtp.[worker_customer_no] 
                                               INNER JOIN [dbo].[TX_CUST_KEYWORD] AS kvl ON kvl.key_value = (cus.[lname] + ' ' + cus.[fname])
                                          WHERE wtp.[worker_type] = 7 
                                            AND kvl.[keyword_no] = 555)
        INSERT INTO [#worker_plan_info] ([plan_no],[campaign_no],[campaign_name],[campaign_category],[campaign_category_name],[campaign_fiscal_year],[customer_no],
                                         [customer_name],[cont_designation],[cont_designation_name],[status],[status_name],[type],[plan_type_name],[ask_dt],[ask_dt_fiscal_year])
        SELECT pln.[plan_no],
               pln.[campaign_no],
               cmp.[description] AS [campaign_name],
               cmp.[category] AS [campaign_category],
               cmc.[description] AS [campaign_category_name],
               COALESCE(cmp.[fyear], [dbo].[LF_GetFiscalYear](pln.[start_dt]), 0) AS [campaign_fiscal_year],
               pln.[customer_no],
               nam.[display_name] AS [customer_name],
               pln.[cont_designation],
               dsg.[description] AS [cont_designation_name],
               pln.[status],
               sta.[description] AS [status_name],
               pln.[type],
               typ.[description] AS [plan_type_name],
               pln.[start_dt] AS [ask_dt],
               [dbo].[LF_GetFiscalYear](pln.[start_dt]) AS [ask_dt_fiscal_year]
        FROM [dbo].[T_PLAN] AS pln
             INNER JOIN [CTE_PROSPECT_MANAGERS] AS cte ON cte.[customer_no] = pln.[customer_no]
             INNER JOIN [dbo].[T_CAMPAIGN] AS cmp ON cmp.[campaign_no] = pln.[campaign_no]
             INNER JOIN [dbo].[TR_CAMPAIGN_CATEGORY] AS cmc ON cmc.[id] = cmp.[category]
             INNER JOIN [dbo].[FT_CONSTITUENT_DISPLAY_NAME]() AS nam ON nam.[customer_no] = pln.[customer_no]
             INNER JOIN [dbo].[TR_CONT_DESIGNATION] AS dsg ON dsg.[id] = pln.[cont_designation]
             INNER JOIN [dbo].[TR_PLAN_STATUS] AS sta ON sta.[id] = pln.[status]
             INNER JOIN [dbo].[TR_PLAN_TYPE] AS typ ON typ.[id] = pln.[type]
        WHERE cte.[worker_customer_no] = @workers_str;

    /*  Delete plan information where the fiscal year ofd the ask date is not the fiscal year passed to the report  */

        DELETE FROM [#worker_plan_info] WHERE [ask_dt_fiscal_year] <> @fiscal_year;
       
    /*  Insert row for total assigned prospects  */

        INSERT INTO @final_table ([row_num], [worker_customer_no], [worker_initials], [worker_name], [row_description], [row_value], [row_percent])
        SELECT 1,
               @workers_str,
               @worker_initials,
               @worker_name,
               'Assigned Prospects',
                @assigned_prospects,
                0;
        
    /*  Insert Annual Fund and Corporate Membership solicitation count  */

        INSERT INTO @final_table ([row_num], [worker_customer_no], [worker_initials], [worker_name], [row_description], [row_value], [row_percent])
        SELECT 2,
               @workers_str,
               @worker_initials,
               @worker_name,
               'Assigned Prospects solicited for Annual Fund or Corporate Membership in ' + CAST(@fiscal_year AS VARCHAR(10)), 
               COUNT(DISTINCT [customer_no]),
               CASE WHEN @assigned_prospects = 0 THEN 0
                    ELSE (COUNT(DISTINCT [customer_no]) / @assigned_prospects) END
        FROM [#worker_plan_info]
        WHERE [status] IN (23, 26, 35, 38)
          AND ([cont_designation] IN (2, 116, 117) OR [campaign_category] = 31);
        
    /*  Insert Annual Fund and Corporate Membership contribution count  */

        INSERT INTO @final_table ([row_num], [worker_customer_no], [worker_initials], [worker_name], [row_description], [row_value], [row_percent])
        SELECT 3,
               @workers_str,
               @worker_initials,
               @worker_name,
               'Assigned Prospects have made their Annual Fund or Corporate Membership gift for ' + CAST(@fiscal_year AS VARCHAR(10)), 
               COUNT(DISTINCT kwd.[customer_no]),
               CASE WHEN @assigned_prospects = 0 THEN 0
                          ELSE (COUNT(DISTINCT kwd.[customer_no]) / @assigned_prospects) END
        FROM [dbo].[TX_CUST_WORKER_TYPE] AS wtp
             INNER JOIN [dbo].[T_CUSTOMER] AS cus ON cus.[customer_no] = wtp.[worker_customer_no]
             INNER JOIN [dbo].[TX_CUST_KEYWORD] AS kwd ON kwd.[key_value] = (cus.[lname] + ' ' + cus.[fname])
             INNER JOIN [dbo].[T_PLAN] AS pln ON kwd.[customer_no] = pln.[customer_no]
             INNER JOIN [dbo].[T_CONTRIBUTION] AS con ON con.[plan_no] =  pln.[plan_no]
             INNER JOIN [dbo].[T_CAMPAIGN] AS cmp ON con.[campaign_no] = cmp.[campaign_no]
        WHERE wtp.[worker_type] = 7 
          AND kwd.[keyword_no] = 555
          AND wtp.[worker_customer_no] = @workers_str
          AND pln.[status] = 35
          AND con.[cont_amt] > 0 
          AND (con.[cont_designation] = 2 OR cmp.[category] = 31) 
          AND cmp.[fyear] = @fiscal_year;

    /*  Insert LD/PG/CFG solicitation count  */

        INSERT INTO @final_table ([row_num], [worker_customer_no], [worker_initials], [worker_name], [row_description], [row_value], [row_percent])
        SELECT 4,
               @workers_str,
               @worker_initials,
               @worker_name,
               'Assigned Prospects solicited for LD/PG/CFG in ' + CAST(@fiscal_year AS VARCHAR(10)), 
               COUNT(DISTINCT [customer_no]),
               CASE WHEN @assigned_prospects = 0 THEN 0
                    ELSE (COUNT(DISTINCT [customer_no]) / @assigned_prospects) END
        FROM [#worker_plan_info]
        WHERE [status] IN (23, 26, 35, 38)
          AND [campaign_category] IN (10, 12, 13, 29, 40);

    /*  Insert LD/PG/CFG contribution count  */

        WITH [CTE_CUSTOMERS] ([customer_no]) 
                              AS (SELECT DISTINCT [customer_no]
                                  FROM [#worker_plan_info]
                                  WHERE [status] IN (23, 26, 35, 38)
                                    AND [campaign_category] IN (10, 12, 13, 29, 40))
        INSERT INTO @final_table ([row_num], [worker_customer_no], [worker_initials], [worker_name], [row_description], [row_value], [row_percent])
        SELECT 5,
               @workers_str,
               @worker_initials,
               @worker_name,
               'Assigned Prospects have made their LD/PG/CFG gift for ' + CAST(@fiscal_year AS VARCHAR(10)), 
               COUNT(DISTINCT kwd.[customer_no]),
               CASE WHEN @assigned_prospects = 0 THEN 0
                          ELSE (COUNT(DISTINCT kwd.[customer_no]) / @assigned_prospects) END
        FROM [dbo].[TX_CUST_WORKER_TYPE] AS wtp
             INNER JOIN [dbo].[T_CUSTOMER] AS cus ON cus.[customer_no] = wtp.[worker_customer_no]
             INNER JOIN [dbo].[TX_CUST_KEYWORD] AS kwd ON kwd.[key_value] = (cus.[lname] + ' ' + cus.[fname])
             INNER JOIN [CTE_CUSTOMERS] AS cte ON cte.[customer_no] = kwd.[customer_no] 
             INNER JOIN [dbo].[T_PLAN] AS pln ON kwd.[customer_no] = pln.[customer_no]
             INNER JOIN [dbo].[T_CONTRIBUTION] AS con ON con.[plan_no] =  pln.[plan_no]
             INNER JOIN [dbo].[T_CAMPAIGN] AS cmp ON con.[campaign_no] = cmp.[campaign_no]
        WHERE wtp.[worker_type] = 7 
          AND kwd.[keyword_no] = 555
          AND wtp.[worker_customer_no] = @workers_str
          AND pln.[status] = 35
          AND con.[cont_amt] > 0 
          AND cmp.category IN (40,29,10,12,13)
          AND cmp.[fyear] = @fiscal_year;

    FINISHED:
    
          SELECT [row_num], 
                 [worker_customer_no], 
                 [worker_initials], 
                 [worker_name], 
                 [row_description], 
                 [row_value], 
                 [row_percent]
          FROM @final_table ORDER BY [row_num]

    DONE:


END
GO


EXECUTE [dbo].[LRP_ADV_PM_ASSIGNMENT_SUMMARY] @fiscal_year = 2021, @workers_str = '672463'



 
  --SELECT * FROM [dbo].[T_PLAN] WHERE [plan_no] = 28637

 --SELECT [plan_no], [campaign_name], [campaign_fiscal_year], [ask_dt], [ask_dt_fiscal_year] FROM [#worker_plan_info] WHERE [campaign_fiscal_year] = 2021 AND [ask_dt_fiscal_year] <> 2021
 --SELECT [plan_no], [campaign_name], [campaign_fiscal_year], [ask_dt], [ask_dt_fiscal_year] FROM [#worker_plan_info] WHERE [campaign_fiscal_year] <> 2021 AND [ask_dt_fiscal_year] = 2021


 --SELECT * FROM [dbo].[TR_PLAN_STATUS] WHERE id IN (23, 26, 35, 38)
--SELECT * FROM [dbo].[TR_CONT_DESIGNATION] WHERE id IN (2, 116, 117)
--SELECT * FROM [dbo].[TR_CAMPAIGN_CATEGORY] WHERE id = 31
--SELECT * FROM [dbo].[TR_CAMPAIGN_CATEGORY] WHERE id IN (10, 12, 13, 29, 40)
