USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_CURRENT_RECOMMENDATIONS]') AND type in (N'P', N'PC')) BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_CURRENT_RECOMMENDATIONS] AS' 
END
GO

ALTER PROCEDURE [dbo].[LRP_CURRENT_RECOMMENDATIONS]
        @title_str VARCHAR(4000) = '',
        @type_str VARCHAR(4000) = '',
        @update_first CHAR(1) = 'N'
AS BEGIN

    SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

    /*  Procedure Variables and Temp Tables  */

        DECLARE @title_no INT, @prod_no INT, @rec_weight INT
        DECLARE @title_name VARCHAR(30), @prod_name VARCHAR(30), @rec_type VARCHAR(30), @prod_season_list varchar(255)
        DECLARE @update_msg VARCHAR(100) = ''

        DECLARE @title_list TABLE ([title_no] INT NOT NULL DEFAULT (0))
        DECLARE @type_list TABLE ([rec_type] VARCHAR(30) NOT NULL DEFAULT (''))

        DECLARE @data_table TABLE([title_no] INT NOT NULL DEFAULT (0), 
                                  [title_name] VARCHAR(30) NOT NULL DEFAULT (''), 
                                  [production_no] INT NOT NULL DEFAULT (0), 
                                  [production_name] VARCHAR(30) NOT NULL DEFAULT (''), 
                                  [recommendation_weight] INT NOT NULL DEFAULT (0), 
                                  [rec_type] VARCHAR(30) NOT NULL DEFAULT (''), 
                                  [production_season_list] VARCHAR(255) NOT NULL DEFAULT (''))

        IF OBJECT_ID('tempdb..#prod_season_table') IS NOT NULL DROP TABLE [#prod_season_table]

        CREATE TABLE [#prod_season_table] ([id] INT IDENTITY(1,1), 
                                           [title_no] INT , 
                                           [title_name] VARCHAR(30),
                                           [production_no] INT,
                                           [production_name] VARCHAR(30), 
                                           [recommendation_weight] VARCHAR(50), 
                                           [rec_type] VARCHAR(50), 
                                           [production_season_list] VARCHAR(255),
                                           [rec_title_no] INT,
                                           [rec_title_name] VARCHAR(30),
                                           [rec_prod_no] INT, 
                                           [rec_prod_name] VARCHAR(30),
                                           [rec_prod_season_no] INT, 
                                           [rec_prod_season_name] VARCHAR(30),
                                           [next_perf_dt] DATETIME,
                                           [no_future_perfs] CHAR(1))

    /*  Check Parameters  */

        IF ISNULL(@title_str,'') = ''
            INSERT INTO @title_list ([title_no])
                SELECT [title_no] 
                FROM [dbo].[T_TITLE]
        ELSE
            INSERT INTO @title_list ([title_no])
                SELECT CAST([element] AS INT) 
                FROM [dbo].[FT_SPLIT_LIST](REPLACE(@title_str,'"',''),',')


        IF ISNULL(@type_str,'') = ''
            INSERT INTO @type_list ([rec_type])
                SELECT [description] 
                FROM [dbo].[TR_GOOESOFT_DROPDOWN]
                WHERE [code] = 1517
        ELSE
            INSERT INTO @type_list ([rec_type])
                SELECT [element] 
                FROM [dbo].[FT_SPLIT_LIST](REPLACE(@type_str,'"',''),',')


        SELECT @update_first = ISNULL(@update_first,'N')
        

    /*  If an update is requested*/


        IF @update_first = 'Y' BEGIN

            EXECUTE [dbo].[LP_UPDATE_PRODUCT_RECOMMENDATIONS]
                    @delete_blank_recommendations = 'Y',
                    @delete_nonexistent_recommendations = 'Y'

            SELECT @update_msg = 'Data Updated: ' + CONVERT(VARCHAR(25),GETDATE(),120)

        END


    /*  Gather Production Element Data  */

        INSERT INTO @data_table 

            SELECT pro.[title_no], 
                   pro.[title_name], 
                   wei.[inv_no], 
                   pro.[production_name], 
                   CONVERT(INT,wei.[value]), 
                   'public', 
                   ISNULL(LTRIM(RTRIM(pub.[value])),'')
            FROM [dbo].[TX_INV_CONTENT] AS wei
                 LEFT OUTER JOIN [dbo].[LV_PRODUCTION_ELEMENTS_PRODUCTION] AS pro ON pro.[production_no] = wei.[inv_no]
                 LEFT OUTER JOIN [dbo].[TX_INV_CONTENT] AS pub ON pub.[inv_no] = wei.inv_no 
                                                              AND pub.[content_type] = (SELECT [id] 
                                                                                        FROM [dbo].[TR_INV_CONTENT] 
                                                                                        WHERE [description] = 'Recommended Events (Public)')
                 INNER JOIN @title_list AS ttl ON ttl.[title_no] = pro.[title_no]
            WHERE wei.[content_type] = (SELECT [id] FROM [dbo].[TR_INV_CONTENT] WHERE [description] = 'Recommendation Weight')
                  AND ISNULL(pro.[production_name],'') <> ''

        UNION ALL

            SELECT pro.[title_no], 
                   pro.[title_name], 
                   wei.[inv_no], 
                   pro.[production_name], 
                   CONVERT(INT,wei.[value]), 
                   'Members', 
                   ISNULL(LTRIM(RTRIM(mem.[value])),'')
            FROM [dbo].[TX_INV_CONTENT] AS wei
                 LEFT OUTER JOIN [dbo].[LV_PRODUCTION_ELEMENTS_PRODUCTION] AS pro ON pro.[production_no] = wei.[inv_no]
                 LEFT OUTER JOIN [dbo].[TX_INV_CONTENT] AS mem ON mem.[inv_no] = wei.inv_no 
                                                              AND mem.[content_type] = (SELECT [id] 
                                                                                        FROM [dbo].[TR_INV_CONTENT] 
                                                                                        WHERE [description] = 'Recommended Events (Members)')
                 INNER JOIN @title_list AS ttl ON ttl.[title_no] = pro.[title_no]
            WHERE wei.[content_type] = (SELECT [id] FROM [dbo].[TR_INV_CONTENT] WHERE [description] = 'Recommendation Weight')
                  AND ISNULL(pro.[production_name],'') <> ''

        UNION ALL

            SELECT pro.[title_no], 
                   pro.[title_name], 
                   wei.[inv_no], 
                   pro.[production_name], 
                   CONVERT(INT,wei.[value]), 
                   'Kiosk', 
                   ISNULL(LTRIM(RTRIM(kio.[value])),'')
            FROM [dbo].[TX_INV_CONTENT] AS wei
                 LEFT OUTER JOIN [dbo].[LV_PRODUCTION_ELEMENTS_PRODUCTION] AS pro ON pro.[production_no] = wei.[inv_no]
                 LEFT OUTER JOIN [dbo].[TX_INV_CONTENT] AS kio ON kio.[inv_no] = wei.inv_no 
                                                              AND kio.[content_type] = (SELECT [id] 
                                                                                        FROM [dbo].[TR_INV_CONTENT] 
                                                                                        WHERE [description] = 'Recommended Events (Kiosk)')
                INNER JOIN @title_list AS ttl ON ttl.[title_no] = pro.[title_no]
            WHERE wei.[content_type] = (SELECT [id] FROM [dbo].[TR_INV_CONTENT] WHERE [description] = 'Recommendation Weight')
              AND ISNULL(pro.[production_name],'') <> ''

    /*  Parse out the recomendation lists one production at a time  */

        DECLARE production_cursor INSENSITIVE CURSOR FOR
        SELECT [title_no], 
               [title_name],
               [production_no],
               [production_name], 
               [recommendation_weight], 
               [rec_type], 
               [production_season_list]
        FROM @data_table 
        OPEN production_cursor
        BEGIN_PRODUCTION_LOOP:

            FETCH NEXT FROM production_cursor INTO @title_no, @title_name, @prod_no, @prod_name, @rec_weight, @rec_type, @prod_season_list
            IF @@FETCH_STATUS = -1 GOTO END_PRODUCTION_LOOP

            IF NOT EXISTS (SELECT * FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] WHERE [performance_dt] >= CONVERT(DATE,GETDATE()) AND [production_no] = @prod_no)
                GOTO BEGIN_PRODUCTION_LOOP
    
            INSERT INTO [#prod_season_table] ([title_no], [title_name], [production_no], [production_name], [recommendation_weight], [rec_type], [production_season_list],
                                              [rec_title_no], [rec_title_name], [rec_prod_no], [rec_prod_name], [rec_prod_season_no], [rec_prod_season_name],
                                              [next_perf_dt], [no_future_perfs])
            SELECT @title_no,
                   @title_name,
                   @prod_no,
                   @prod_name,
                   @rec_weight,
                   @rec_type,
                   @prod_season_list,
                   sea.[title_no],
                   sea.[title_name],
                   ISNULL(sea.[production_no], 0), 
                   ISNULL(sea.production_name,''),
                   CAST(LTRIM(RTRIM(fnc.[Element])) AS INT), 
                   ISNULL(sea.[production_season_name],''),
                   NULL,
                   'N'
            FROM [dbo].[FT_SPLIT_LIST](@prod_season_list,',') AS fnc
                 LEFT OUTER JOIN [dbo].[LV_PRODUCTION_ELEMENTS_SEASON] AS sea ON sea.[production_season_no] = fnc.[Element]
     
            GOTO BEGIN_PRODUCTION_LOOP

        END_PRODUCTION_LOOP:
        CLOSE production_cursor
        DEALLOCATE production_cursor;

        WITH [CTE_NEXT_PERF_DT] ([prod_season_no], [next_perf_dt])
        AS (
            SELECT [production_season_no],
                   MIN([performance_dt])
            FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE]
            WHERE [performance_dt] > GETDATE()
            GROUP BY [production_season_no]
            )
        UPDATE ps
        SET ps.[next_perf_dt] = ISNULL(prf.[next_perf_dt],'5-18-2016')
        FROM [#prod_season_table] AS ps
             INNER JOIN [CTE_NEXT_PERF_DT] AS prf ON prf.[prod_season_no] = ps.[rec_prod_season_no]

        UPDATE [#prod_season_table]
        SET [no_future_perfs] = 'Y'
        WHERE [rec_prod_season_no] > 0 AND [next_perf_dt] IS NULL
        
    FINISHED:

        /*  Final Data Set Returned to the Report  */

            SELECT sea.[title_no], 
                   sea.[title_name], 
                   sea.[production_no], 
                   sea.[production_name], 
                   sea.[recommendation_weight], 
                   sea.[rec_type], 
                   sea.[production_season_list], 
                   ISNULL(sea.[rec_title_no],0) AS [rec_title_no],
                   ISNULL(sea.[rec_title_name],'') AS [rec_title_name],
                   ISNULL(sea.[rec_prod_no],'') AS [rec_prod_no],
                   ISNULL(sea.[rec_prod_name],'') AS [rec_prod_name],
                   ISNULL(sea.[rec_prod_season_no],0) AS [rec_prod_season_no], 
                   CASE WHEN ISNULL(sea.[rec_prod_season_name],'') = '' THEN 'Unknown Production Season'
                                                                        ELSE sea.[rec_prod_season_name] END AS [rec_prod_season_name],
                   ISNULL([sea].[no_future_perfs],'N') AS [no_future_perfs],
                   @update_msg AS [update_message]
            FROM [#prod_season_table] AS sea
                 INNER JOIN @type_list AS typ ON typ.[rec_type] = sea.[rec_type]
            WHERE ISNULL(sea.[rec_title_no],0) > 0

    DONE:

        IF OBJECT_ID('tempdb..#prod_season_table') IS NOT NULL DROP TABLE [#prod_season_table]

END
GO

GRANT EXECUTE ON [dbo].[LRP_CURRENT_RECOMMENDATIONS] TO ImpUsers
GO





