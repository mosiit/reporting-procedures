USE impresario;
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_MEMBER_SALES_SCAN_CHECK]') AND type in (N'P', N'PC'))
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_MEMBER_SALES_SCAN_CHECK] AS';
GO

EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LRP_MEMBER_SALES_SCAN_CHECK] TO impusers, tessitura_app';
GO


ALTER PROCEDURE [dbo].[LRP_MEMBER_SALES_SCAN_CHECK]
        @report_start_dt DATETIME = NULL,
        @report_end_dt DATETIME = NULL,
        @remove_no_shows CHAR(1) = 'N',
        @only_potential_problems CHAR(1) = 'N'
AS BEGIN

    SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

    /*  Report Variables and Temp Tables  */

        DECLARE @exh_title_no INT = 27;
        
        IF OBJECT_ID('tempdb..#raw_data') is not null DROP TABLE [#raw_data]

        CREATE TABLE [#raw_data] ([cust_memb_no] INT NOT NULL DEFAULT (0),
                                  [customer_no] INT NOT NULL DEFAULT (0),
                                  --[memb_level] VARCHAR(30) NOT NULL DEFAULT (''),
                                  [perf_date] DATE NULL,
                                  [order_no] INT NOT NULL DEFAULT (0),
                                  [ticket_purchase] INT NOT NULL DEFAULT (0),
                                  [ticket_scan] INT NOT NULL DEFAULT (0),
                                  [card_scan] INT NOT NULL DEFAULT (0))

        IF OBJECT_ID('tempdb..#final_table') is not null DROP TABLE [#final_table]

        CREATE TABLE [#final_table] ([perf_date] DATE NOT NULL,
                                     [customer_no] INT NOT NULL DEFAULT (0),
                                     [display_name] VARCHAR(125) NOT NULL DEFAULT (''),
                                     [sort_name] VARCHAR(125) NOT NULL DEFAULT (''),
                                     [cust_memb_no] INT NOT NULL DEFAULT (0),
                                     [memb_level] VARCHAR(30) NOT NULL DEFAULT (''),
                                     [expr_dt] DATETIME NULL,
                                     [ticket_purchase] INT NOT NULL DEFAULT (0),
                                     [ticket_scan] INT NOT NULL DEFAULT (0),
                                     [card_scan] INT NOT NULL DEFAULT (0),
                                     [scan_message] VARCHAR(50) NOT NULL DEFAULT (''),
                                     [potential_problem_ind] CHAR(1) NOT NULL DEFAULT ('N'))

    /*  Check Parameters  */

        SELECT @report_start_dt = CAST(ISNULL(@report_start_dt, GETDATE()) AS DATE)
        SELECT @report_end_dt = CONVERT(CHAR(10),ISNULL(@report_end_dt,GETDATE()),111) + ' 23:59:59.957'

        IF ISNULL(@remove_no_shows,'') <> 'Y' SELECT @remove_no_shows = 'N'

        IF ISNULL(@only_potential_problems,'') <> 'Y' SELECT @only_potential_problems = 'N'
        
    /*  Get Raw Data About Membership Card Scans  */

        INSERT INTO [#raw_data] ([cust_memb_no], [customer_no], [perf_date], [order_no], [ticket_purchase], [ticket_scan], [card_scan])
        SELECT att.[cust_memb_no],
               att.[customer_no],
               CAST(att.[attend_dt] AS DATE),
               0,
               0,
               0,
               SUM((ISNULL(att.[admission_adult], 0) + ISNULL(att.[admission_child], 0) + ISNULL(att.[admission_other], 0))) AS [scan_total]
        FROM [dbo].[T_ATTENDANCE] AS att                                                                 --236=primary zone # for public Exhibit Halls
        WHERE att.[attend_dt] BETWEEN @report_start_dt AND @report_end_dt 
          AND ISNULL(att.[ticket_no], 0) = 0 
          AND ISNULL(att.[customer_no], 0) > 0
          AND att.[area_no] IS NULL
        GROUP BY att.[cust_memb_no], att.[customer_no], CAST(att.[attend_dt] AS DATE);
       
                       ----*****BAD DATA PURPOSELY ADDED TO REPORT FOR TESTING (Remove or comment out when report goes live)
                       ----*****Data is specific to one date
                       --IF '2021-09-18' BETWEEN @report_start_dt AND @report_end_dt
                       --    INSERT INTO [#raw_data] ([cust_memb_no], [customer_no], [perf_date], [order_no], [ticket_purchase], [ticket_scan], [card_scan])
                       --    VALUES (1303590, 4150977, '2021-09-18', 0, 0, 0, 4),
                       --           (1309131, 3252185, '2021-09-18', 0, 0, 0, 2),
                       --           (1309175, 4181411, '2021-09-18', 0, 0, 0, 8);

    /*  Get Raw Data About Membership Card Sales  */

        WITH CTE_PERFS AS
        (       SELECT [performance_no],
                       [performance_zone],
                       [performance_dt],
                       [performance_date],
                       [title_no]
                FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE]
                WHERE [performance_dt] BETWEEN @report_start_dt AND @report_end_dt
                  AND [title_no] = @exh_title_no
        ),
        CTE_ORDERS AS
        (       SELECT DISTINCT ord.[order_no],
                                ord.[customer_no],
                                [dbo].[LFS_ActiveMemberNumberOnDate](ord.[customer_no], 0, prf.[performance_dt]) AS [cust_memb_no]
                FROM [dbo].[T_SUB_LINEITEM] AS sli
                     INNER JOIN [dbo].[T_ORDER] AS ord ON ord.[order_no] = sli.[order_no]
                     INNER JOIN [CTE_PERFS] AS prf ON prf.[performance_no] = sli.[perf_no] AND prf.[performance_zone] = sli.[zone_no]
                     --INNER JOIN [dbo].[TX_CUST_MEMBERSHIP] AS mem ON mem.[cust_memb_no] = dbo.[LFS_ActiveMemberNumberOnDate](ord.[customer_no], 0, prf.[performance_dt])
                WHERE sli.[price_type] IN (125, 128)
                   OR (sli.[price_type] IN (3, 32, 33) AND ISNULL(rule_id, 0) in (1400, 1401, 1402, 1403))
        )
        INSERT INTO [#raw_data] ([cust_memb_no], [customer_no], [perf_date], [order_no], [ticket_purchase], [ticket_scan], [card_scan])
        SELECT ord.[cust_memb_no],
               ord.[customer_no],
               CAST(prf.[performance_dt] AS DATE),
               0,
               COUNT(sli.[sli_no]),
               SUM(ISNULL(att.[admission_adult], 0) + ISNULL(att.[admission_child], 0) + ISNULL(att.[admission_other], 0)),
               0
        FROM [dbo].[T_SUB_LINEITEM] AS sli
             INNER JOIN [CTE_ORDERS] AS ord ON ord.[order_no] = sli.[order_no]
             INNER JOIN [CTE_PERFS] AS prf ON prf.[performance_no] = sli.[perf_no] AND prf.[performance_zone] = sli.[zone_no]
             LEFT OUTER JOIN [dbo].[T_ATTENDANCE] as att ON att.[ticket_no] = sli.[ticket_no]
        WHERE sli.[sli_status] IN (3, 12)
          AND sli.[paid_amt] = 0.0
        GROUP BY ord.[cust_memb_no], ord.[customer_no], CAST(prf.[performance_dt] AS DATE);

    /*  Create Final Data Set  */

        WITH CTE_RAW_DATA AS
        (       SELECT [perf_date], 
                        [customer_no], 
                        [cust_memb_no], 
                        [order_no],
                        SUM([ticket_purchase]) AS [ticket_purchase], 
                        SUM([ticket_scan]) AS [ticket_scan], 
                        SUM([card_scan]) AS [card_scan]
                FROM [#raw_data]
                GROUP BY [perf_date], [customer_no], [cust_memb_no], [order_no]
        )
        INSERT INTO [#final_table] ([perf_date],[customer_no],[display_name],[sort_name],[cust_memb_no],[memb_level],[expr_dt],
                                    [ticket_purchase],[ticket_scan],[card_scan],[scan_message],[potential_problem_ind])
        SELECT dat.[perf_date],
                dat.[customer_no],
                nam.[display_name],
                nam.[sort_name],
                dat.[cust_memb_no],
                lev.[description],
                mem.[expr_dt],
                dat.[ticket_purchase],
                dat.[ticket_scan],
                dat.[card_scan],
                CASE WHEN dat.[ticket_scan] <> 0 AND dat.[card_scan] <> 0 AND dat.[ticket_scan] = dat.[card_scan] THEN 'Ticket(s) AND Membership Card Same Scan'
                    WHEN dat.[ticket_scan] <> 0 AND dat.[card_scan] <> 0 AND dat.[ticket_scan] <> dat.[card_scan] THEN 'Ticket(s) AND Membership Card Different Scan'
                    WHEN dat.[ticket_scan] <> 0 THEN 'Ticket(s) Scanned'
                    WHEN dat.[card_scan] <> 0 THEN 'Membership Card Scanned'
                    ELSE 'Nothing Scanned' END AS [scan_message],
                CASE WHEN dat.[ticket_scan] <> 0 AND dat.[card_scan] <> 0 AND dat.[ticket_scan] = dat.[card_scan] THEN 'Y'
                        WHEN dat.[ticket_scan] <> 0 AND dat.[card_scan] <> 0 AND dat.[ticket_scan] <> dat.[card_scan] THEN 'Y'
                        ELSE 'N' END AS [potential_problem_ind]
        FROM [CTE_RAW_DATA] AS dat
                INNER JOIN [dbo].[FT_CONSTITUENT_DISPLAY_NAME]() AS nam ON nam.[customer_no] = dat.[customer_no]
                INNER JOIN [dbo].[TX_CUST_MEMBERSHIP] AS mem ON mem.[cust_memb_no] = dat.[cust_memb_no]
                INNER JOIN [dbo].[T_MEMB_LEVEL] AS lev ON lev.[memb_level] = mem.[memb_level]
        ORDER BY dat.[perf_date], nam.[display_name], dat.[customer_no]

    /*  Remove non Problems  */

        IF @only_potential_problems = 'Y'
            DELETE FROM [#final_table]
            WHERE [potential_problem_ind] = 'N'
        ELSE IF @remove_no_shows = 'Y'
            DELETE FROM [#final_table]
            WHERE [ticket_scan] = 0 AND [card_scan] = 0

    FINISHED:

        /*  Return Final Data Set  */

            SELECT [perf_date],
                   [customer_no],
                   [display_name],
                   [sort_name],
                   [cust_memb_no],
                   [memb_level],
                   [expr_dt],
                   [ticket_purchase],
                   [ticket_scan],
                   [card_scan],
                   [scan_message],
                   [potential_problem_ind]
            FROM [#final_table]



    DONE:

        IF OBJECT_ID('tempdb..#final_table') is not null DROP TABLE [#final_table]

        IF OBJECT_ID('tempdb..#raw_data') is not null DROP TABLE [#raw_data]

END
GO

--EXECUTE [dbo].[LRP_MEMBER_SALES_SCAN_CHECK] @report_start_dt = '9-18-2021', @report_end_dt = '9-19-2021', @only_potential_problems = 'N', @remove_no_shows = 'Y'
--EXECUTE [dbo].[LRP_MEMBER_SALES_SCAN_CHECK] @remove_no_shows = 'Y'
--EXECUTE [dbo].[LRP_MEMBER_SALES_SCAN_CHECK] @report_start_dt = '10-6-2021', @report_end_dt = '10-6-2021', @remove_no_shows = 'N', @only_potential_problems = 'N'

