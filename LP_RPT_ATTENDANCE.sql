USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LP_RPT_ATTENDANCE]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LP_RPT_ATTENDANCE]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LP_RPT_ATTENDANCE]
        @report_start_date datetime,
        @report_end_date datetime,
        @include_partial_paid char(1) = 'Y'
AS BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

    /*  Performance Dates in the History table are stored in yyyy/MM/dd format.  These variables are used for searching that table.  */

    DECLARE @start_date char(10), @end_date char(10)

    SELECT @start_date = convert(char(10),@report_start_date,111), 
           @end_date = convert(char(10),@report_end_date,111)

    /* Select aggregated date into the report  */
  
	-- 2016/05/15, H. Sheridan - Add value to field to indicate no rows returned
	IF (SELECT COUNT([attendance_type]) FROM [dbo].[LT_HISTORY_TICKET] (NOLOCK)) = 0
		SELECT 'No rows found' AS [attendance_type], 
               '' AS [performance_type], 
               '' AS [title_name], 
               '' AS [production_name], 
               '' AS [production_name_short], 
               '' AS [production_name_long], 
               '' AS [comp_code_name], 
               '' AS [order_payment_status], 
               0 as 'sale_total', 
               0 as 'scan_admission_total', 
               0 as 'due_amt', 
               0 as 'paid_amt', 
               @report_start_date as 'p_report_start_date', 
               @report_end_date as 'p_report_end_date', 
               @include_partial_paid as 'p_include_partial_paid'
    ELSE
	    SELECT [attendance_type], 
               [performance_type], 
               [title_name], 
               [production_name], 
               [production_name_short], 
               [production_name_long], 
               [comp_code_name], 
               [order_payment_status], 
               SUM([sale_total]) as 'sale_total', 
               SUM([scan_admission_total]) as 'scan_admission_total', 
               SUM([due_amt]) as 'due_amt', 
               SUM([paid_amt]) as 'paid_amt', 
               @report_start_date as 'p_report_start_date', 
               @report_end_date as 'p_report_end_date', 
               @include_partial_paid as 'p_include_partial_paid'
        FROM [dbo].[LT_HISTORY_TICKET] (NOLOCK)
        WHERE performance_date between @start_date and @end_date
          --AND title_name <> 'Show and Collected'
        GROUP BY [attendance_type], 
                 [performance_type], 
                 [title_name], 
                 [production_name], 
                 [production_name_short], 
                 [production_name_long], 
                 [comp_code_name], 
                 [order_payment_status]
        ORDER BY [attendance_type], 
                 [performance_type], 
                 [title_name], 
                 [production_name], 
                 [production_name_short], 
                 [production_name_long], 
                 [comp_code_name], 
                 [order_payment_status]

END
GO

GRANT EXECUTE ON [dbo].[LP_RPT_ATTENDANCE] to impusers
GO


--EXECUTE [dbo].[LP_RPT_ATTENDANCE] '6-16-2020', '7-5-2020', 'Y'


/*

        ---------------------------------------------------------------------------------------------------------------------------------------------------
        ALL THIS CODE WAS MOVED TO A PROCEDURE CALLED LP_UPDATE_HISTORY_TICKET AND TWEAKED TO MAINTAIN A TABLE CALLED LTR_HISTORY_TICKET WHERE ALL THIS
        INFORMATION IS NOW PRE-COMPILED AND STORED FOR REPORTING PURPOSES.  A NIGHTLY JOB UPDATES THE TABLE WITH THE DATA FOR THE PRIOR DAY AS WELL AS
        ANY PREVIOUS DATES THAT HAVE CHANGED (REFUNDS) SINCE THE LAST TIME THE UPDATE PROCESS WAS RUN
        ---------------------------------------------------------------------------------------------------------------------------------------------------

    /* Make sure the dates passed to the procedure include the entire day (from midnight on the start date to 11:59 PM on the end date)  */

        SELECT @report_start_date = convert(char(10),@report_start_date,111) + ' 00:00:00',
               @report_end_date = convert(char(10),@report_end_date,111) + ' 23:59:59'

        SELECT @include_partial_paid = IsNull(@include_partial_paid, 'Y')
        IF @include_partial_paid <> 'N' SELECT @include_partial_paid = 'Y'

    /*  Create temp tables needed for this report  */

        IF OBJECT_ID('tempdb..#sli_temp_table') is not null DROP TABLE [#sli_temp_table]
                    
        CREATE TABLE [#sli_temp_table] ([order_no] int, [sli_no] int, [perf_no] int, [zone_no] int, [due_amt] decimal (18,2), [paid_amt] decimal(18,2), [sli_status] int,
                                        [comp_code] int, [price_type] int, [ticket_no] int, [li_seq_no] int)

        CREATE CLUSTERED INDEX [ix_sli_perf_no] ON [#sli_temp_table] ([perf_no] ASC) ON [PRIMARY]
        CREATE NONCLUSTERED INDEX [ix_sli_zone_no] ON [#sli_temp_table] ([zone_no] ASC) ON [PRIMARY]
        CREATE NONCLUSTERED INDEX [ix_sli_order_no] ON [#sli_temp_table] ([order_no] ASC) ON [PRIMARY]
        CREATE NONCLUSTERED INDEX [ix_sli_comp_code] ON [#sli_temp_table] ([comp_code] ASC) ON [PRIMARY]
        CREATE NONCLUSTERED INDEX [ix_sli_price_type] ON [#sli_temp_table] ([price_type] ASC) ON [PRIMARY]
        CREATE NONCLUSTERED INDEX [ix_sli_ticket_no] ON [#sli_temp_table] ([ticket_no] ASC) ON [PRIMARY]

        IF OBJECT_ID('tempdb..#ord_temp_table') is not null DROP TABLE [#ord_temp_table]
        
        CREATE TABLE [#ord_temp_table] ([order_no] int, [total_due_amt] decimal(18,2), [total_paid_amt] decimal(18,2))

        CREATE CLUSTERED INDEX [ix_ord_order_no] ON [#ord_temp_table] ([order_no] ASC) ON [PRIMARY]

        IF OBJECT_ID('tempdb..#att_temp_table') is not null DROP TABLE [#att_temp_table]

        CREATE TABLE [#att_temp_table] ([attendance_type] varchar(50), [performance_type] varchar(50), [order_no] int, [sli_no] int, [li_seq_no] int, [title_name] varchar(30), [perf_no] int,
                                        [perf_date] char(10), [perf_time] char(8), [zone_no] int, [production_name] varchar(50), [production_name_short] varchar(50), [production_name_long] varchar(150), 
                                        [comp_code] int, [comp_code_name] varchar(50), [order_payment_status] varchar(50), [price_type] int, [price_type_name] varchar(50), [due_amt] decimal(18,2), 
                                        [paid_amt] decimal(18,2), [sale_total] int, [performance_date] char(10), [performance_time] char(8), [scan_admission_date] char(10), [scan_admission_time] char(8), 
                                        [scan_device] varchar(30), [scan_admission_adult] int, [scan_admission_child] int, [scan_admission_other] int, [scan_admission_auto] int, [scan_admission_total] int, 
                                        [sli_status] int, [report_start_date] datetime, [report_end_date] datetime, [include_partial_paid] char(1))


    /*  Get Products Sold For date range  -  This table will keep a list of all subline items we will be working with
        Looks specifically for subline items where the status is 3 (Seated, Paid) or 12 (Ticketed, Paid)   */

        INSERT INTO [#sli_temp_table] SELECT [order_no], [sli_no], [perf_no], [zone_no], [due_amt], [paid_amt], [sli_status], [comp_code], [price_type], [ticket_no], [li_seq_no]
                                      FROM [dbo].[T_SUB_LINEITEM] as sli (NOLOCK)
                                      WHERE sli.[perf_no] in (SELECT [perf_no] FROM [dbo].[T_PERF] WHERE [perf_dt] between @report_start_date and @report_end_date) and [sli_status] in (3,12)

    /*  Get Order Information the products sold - This table will keep a list of all orders we will be working with   */

        INSERT INTO [#ord_temp_table] SELECT [order_no], [tot_due_amt], [tot_paid_amt] FROM [dbo].[T_ORDER] (NOLOCK) WHERE [order_no] in (SELECT [order_no] FROM [#sli_temp_table])

    /*  Remove records that are not needed  */

        --Delete Unpaid Transactions from the order table
        DELETE FROM [#ord_temp_table] WHERE [total_paid_amt] = 0.00

        IF @include_partial_paid <> 'Y' DELETE FROM [#ord_temp_table] WHERE [total_paid_amt] < [total_due_amt]
        
        --Delete from subline items where the order is not included in the order table    
        DELETE FROM [#sli_temp_table] WHERE [order_no] not in (SELECT [order_no] FROM [#ord_temp_table])

    /*  Retrieve the TICKETED Attendance date from the database  */

        INSERT INTO [#att_temp_table]
        SELECT  CASE WHEN prf.[production_gate_attendance] = 'N' THEN 'Non-Gate' ELSE 'Gate A (Ticketed)' END
               ,prf.[performance_type_name], sli.[order_no], sli.[sli_no], sli.[li_seq_no], prf.[title_name], sli.[perf_no], prf.[performance_date], prf.[performance_time], sli.[zone_no], prf.[production_name], 
               prf.[production_name_short], prf.[production_name_long], IsNull(sli.[comp_code],0), IsNull(cmp.[description],''), 'paid' , IsNull(sli.[price_type],0), IsNull(typ.[description],'Unknown'), 
               sli.[due_amt], sli.[paid_amt], 1 ,prf.[performance_date], prf.[performance_time]
               ,CASE WHEN prf.[production_auto_scan] = 'Y' THEN prf.[performance_date] ELSE convert(char(10),att.[attend_dt],111) END
               ,CASE WHEN prf.[production_auto_scan] = 'Y' THEN prf.[performance_time] ELSE convert(char(8),att.[attend_dt],108) END
               ,IsNull(att.[device],''), IsNull(att.[admission_adult], 0), IsNull(att.[admission_child], 0),IsNull(att.[admission_other], 0)
               ,CASE WHEN prf.[production_auto_scan] = 'Y' THEN 1 ELSE 0 END
               ,CASE WHEN prf.[production_auto_scan] = 'Y' THEN 1 ELSE (IsNull(att.[admission_adult], 0) + IsNull(att.[admission_child], 0) + IsNull(att.[admission_other], 0)) END
               ,sli.[sli_status], @report_start_date, @report_end_date, @include_partial_paid
        FROM  [T_SUB_LINEITEM] as sli (NOLOCK)
             LEFT OUTER JOIN [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] as prf (NOLOCK) ON prf.[performance_no] = sli.[perf_no] and prf.[performance_zone] = sli.[zone_no]
             LEFT OUTER JOIN [dbo].[TR_COMP_CODE] as cmp (NOLOCK) ON cmp.[id] = sli.[comp_code]
             LEFT OUTER JOIN [dbo].[TR_PRICE_TYPE] as typ (NOLOCK) ON typ.[id] = sli.[price_type]
             LEFT OUTER JOIN [dbo].[T_ATTENDANCE] as att (NOLOCK) ON att.[ticket_no] = sli.[ticket_no]
        WHERE prf.[performance_no] in (SELECT [perf_no] FROM [#sli_temp_table])

    /*  Delete returned/refunded products  */
    DELETE FROM [#att_temp_table] WHERE [sli_status] not in (3, 12)

    /*  Set Traveling Programs Mileage Fee to its own product to separate it out in the report  */
     UPDATE [#att_temp_table] 
     SET production_name = 'Trav Prog Mileage Fee', production_name_short = 'Mileage Fee', production_name_long = 'Traveling Programs Mileage Fee'
     WHERE [price_type_name] like 'Trav Prog Mileage%'

    /*  Retrieve the NON-TICKETED Attendance data (minus Show and Go) from the database
        NOTE: Membership scans tie back to the Exhibit Hall product for that day.  */

        INSERT INTO [#att_temp_table]
        SELECT 'Gate B (Non-Ticketed)', 'Public', 0, 0, 0, 'Gate Scan', att.[perf_no], prf.[performance_date], prf.[performance_time], 0, lev.[description], lev.[description], 
               lev.[description], 0, 'No Discount', 'paid', 0, '', 0.00, 0.00, 0, prf.[performance_date], prf.[performance_time], convert(char(10),att.[attend_dt],111), 
               convert(char(8),att.[attend_dt],108), IsNull(att.[device],''), IsNull(att.[admission_adult], 0) as 'scan_admission_adult', IsNull(att.[admission_child], 0) as 'scan_admission_child', 
               IsNull(att.[admission_other], 0) as 'scan_addmission_other', 0, (IsNull(att.[admission_adult], 0) + IsNull(att.[admission_child], 0) + IsNull(att.[admission_other], 0)),
               0,  @report_start_date, @report_end_date, @include_partial_paid
        FROM [dbo].[T_ATTENDANCE] as att (NOLOCK)
             LEFT OUTER JOIN [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] as prf (NOLOCK) ON prf.[performance_no] = att.[perf_no] and prf.[performance_zone] = 236    --236 = primary zone # for public Exhibit Halls
             LEFT OUTER JOIN [dbo].[T_MEMB_LEVEL] as lev (NOLOCK) ON lev.[memb_level_no] = att.[memb_level_no]                                                      --It should never change.
        WHERE IsNull(att.[ticket_no], 0) = 0 and att.[attend_dt] between @report_start_date and @report_end_date and att.[area_no] is null

        
    /*  Retrieve the SHOW AND GO Attendance data (minus Show and Go) from the database  */

        INSERT INTO [#att_temp_table]
        SELECT 'Gate B (Non-Ticketed)', 'Public', 0, 0, 0, 'Show and Go', 0, '', '', 0, [show_and_go_name], [show_and_go_name], [show_and_go_name], 0, 'Free Admission', 'Free', 0, '', 0.00, 0.00, 0,
               [scan_date], [scan_time], [scan_date], [scan_time], [device_name], 0, 0, [scan_admission], 0, [scan_admission], 0, @report_start_date, @report_end_date, @include_partial_paid
        FROM [dbo].[LV_RPT_SHOW_AND_GO]
        WHERE [scan_dt] between @report_start_date and @report_end_date

        /*  Stamped Hand at Discovery Center means they were already scanned and counted somewhere else - Don't count them again  */

        DELETE FROM [#att_temp_table] WHERE [title_name] = 'Show and Go' and [production_name] = 'Stamped Hands at Discovery Center'

    */

      /*

    /*  NOT NEEDED ANY MORE  */

    CLEAN_UP:

        /*  Not really necessary, but the programmer in me wants to clean up my mess  */

        IF OBJECT_ID('tempdb..#sli_temp_table') is not null DROP TABLE [#att_temp_table]
        IF OBJECT_ID('tempdb..#sli_temp_table') is not null DROP TABLE [#ord_temp_table]
        IF OBJECT_ID('tempdb..#sli_temp_table') is not null DROP TABLE [#sli_temp_table]
  */
