SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
GO

USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_PERFORMANCE_SCHEDULE]') AND type IN (N'P', N'PC'))
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_SCHOLARSHIP_TRAVELING_PROGRAM] AS';
GO

EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LRP_SCHOLARSHIP_TRAVELING_PROGRAM] TO [impusers], [tessitura_app]';
GO

ALTER PROCEDURE [dbo].[LRP_SCHOLARSHIP_TRAVELING_PROGRAM]
        @report_start_dt DATETIME = NULL, 
        @report_end_dt DATETIME = NULL,
        @customer_type_id INT = 0,
        @scholarship_id INT = 0,
        @sort_by VARCHAR(30) = NULL
AS BEGIN
 
 -----------------

--DECLARE 
--	@report_start_dt DATETIME = '2019-12-01', 
--    @report_end_dt DATETIME = '2019-12-31',
--    @customer_type_id INT = 13,
--    @scholarship_id INT = 0,
--    @sort_by VARCHAR(30) = 1

----------------
   
    /*  Procedure Variables  */

        DECLARE @ord_no INT, @perf_date CHAR(10), @prod_name VARCHAR(50)
        DECLARE @cust_type_id_school INT, @cust_type_id_school_official INT 
        DECLARE @start_date CHAR(10), @end_date CHAR(10)

    
    /*  Check Parameters  */
    
        IF @report_start_dt IS NULL
            SELECT @report_start_dt = CONVERT(DATETIME,'7-1-' 
                                    + CASE WHEN DATEPART(MONTH,GETDATE()) < 7 THEN CONVERT(VARCHAR(30),DATEPART(YEAR,DATEADD(YEAR,-1,GETDATE()))) 
                                                                              ELSE CONVERT(VARCHAR(30),DATEPART(YEAR,GETDATE())) END)
        IF @report_end_dt IS NULL
            SELECT @report_end_dt = CONVERT(DATETIME,'6-30-' 
                                    + CASE WHEN DATEPART(MONTH,GETDATE()) >= 7 THEN CONVERT(VARCHAR(30),DATEPART(YEAR,DATEADD(YEAR,1,GETDATE()))) 
                                                                               ELSE CONVERT(VARCHAR(30),DATEPART(YEAR,GETDATE())) END) + '23:59:59.967'

        SELECT @customer_type_id = ISNULL(@customer_type_id,0)
        SELECT @scholarship_id = ISNULL(@scholarship_id,0)
        SELECT @sort_by = ISNULL(@sort_by,'Customer Name')
        

    /*  Make sure the date parameters include the full dates, starting at 00:00 and ending at 23:59  */

        SELECT @start_date = CONVERT(CHAR(10),@report_start_dt,111),
               @end_date = CONVERT(CHAR(10), @report_end_dt,111)

    /*  For the purposes of this report, the School and School Official Record customer types are the same thing.
        The id numbers are needed to combine the two  */

        SELECT @cust_type_id_school = [id] FROM [dbo].[TR_CUST_TYPE] WHERE [description] = 'School'
        SELECT @cust_type_id_school = ISNULL(@cust_type_id_school,0)

        SELECT @cust_type_id_school_official = [id] FROM [dbo].[TR_CUST_TYPE] WHERE [description] = 'School Official Record'
        SELECT @cust_type_id_school_official = ISNULL(@cust_type_id_school_official,0)


    /*  If customer type id selected was school official record, change to school  */

    IF @customer_type_id = @cust_type_id_school_official SELECT @customer_type_id = @cust_type_id_school
    
    /*  Create temporary tables  */
            
            IF OBJECT_ID('tempdb..#trav_prog_orders') IS NOT NULL DROP TABLE [#trav_prog_orders]

            CREATE TABLE [#trav_prog_orders] ([order_no] INT)

            CREATE UNIQUE CLUSTERED INDEX [ix_trav_prog_orders] ON [#trav_prog_orders] ([order_no] ASC) ON [PRIMARY]

            --,CONSTRAINT [PK_trav_prog_orders_order_no] PRIMARY KEY CLUSTERED ([order_no] ASC) ON [PRIMARY])

            IF OBJECT_ID('tempdb..#trav_prog_data') IS NOT NULL DROP TABLE [#trav_prog_data]

            CREATE TABLE [#trav_prog_data] ([data_type] VARCHAR(10), 
                                            [order_no] INT, 
                                            [sli_no] INT, 
                                            [customer_no] INT, 
                                            [performance_date] CHAR(10), 
                                            [title_name] VARCHAR(30), 
                                            [scholarship_no] INT,
                                            [production_name] VARCHAR(50), 
                                            [zone_name] VARCHAR(30), 
                                            [price_type_name] VARCHAR(30), 
                                            [create_dt] DATETIME, [due_amount] MONEY, 
                                            [total_paid] MONEY, 
                                            [perf_no] INT)

            CREATE CLUSTERED INDEX [ix_trav_prog_data_data_type] ON [#trav_prog_data] ([data_type] ASC) ON [PRIMARY]
            CREATE NONCLUSTERED INDEX [ix_trav_prog_data_production_name] ON [#trav_prog_data] ([production_name] ASC) ON [PRIMARY]
            CREATE NONCLUSTERED INDEX [ix_trav_prog_data_price_type_name] ON [#trav_prog_data] ([price_type_name] ASC) ON [PRIMARY]
            CREATE NONCLUSTERED INDEX [ix_trav_prog_data_performance_date] ON [#trav_prog_data] ([performance_date] ASC) ON [PRIMARY]
           

            IF OBJECT_ID('tempdb..#trav_prog_final') IS NOT NULL DROP TABLE [#trav_prog_final]

            CREATE TABLE [#trav_prog_final] ([rpt_message] VARCHAR(100), 
                                             [data_type] VARCHAR(10), 
                                             [order_no] INT, 
                                             [sli_no] INT, 
                                             [customer_no] INT, 
                                             [order_performance_date] CHAR(10), 
                                             [performance_date] CHAR(10),
                                             [title_name] VARCHAR(50), 
                                             [scholarship_no] INT, 
                                             [production_name] VARCHAR(50), 
                                             [zone_name] VARCHAR(50), 
                                             [performance_code] VARCHAR(25), 
                                             [price_type_name] VARCHAR(50), 
                                             [create_dt] DATETIME, 
                                             [due_amount] MONEY, 
                                             [total_paid] MONEY, 
                                             [customer_first_name] VARCHAR(50), 
                                             [customer_middle_name] VARCHAR(50), 
                                             [customer_last_name] VARCHAR(100), 
                                             [customer_type_no] INT, 
                                             [customer_type] VARCHAR(30),
                                             [address_type_no] INT, 
                                             [address_type] VARCHAR(30), 
                                             [address_street1] VARCHAR(75), 
                                             [address_street2] VARCHAR(75), 
                                             [address_city] VARCHAR(50), 
                                             [address_state] VARCHAR(50), 
                                             [address_zip_code] VARCHAR(50), 
                                             [address_city_state] VARCHAR(100), 
                                             [order_custom_1] VARCHAR(100), 
                                             [order_custom_2] VARCHAR(100), 
                                             [order_custom_3] VARCHAR(100), 
                                             [order_custom_4] VARCHAR(100), 
                                             [order_custom_5] VARCHAR(100), 
                                             [order_custom_6] VARCHAR(100), 
                                             [order_custom_7] VARCHAR(100), 
                                             [order_custom_8] VARCHAR(100), 
                                             [order_custom_9] VARCHAR(100), 
                                             [order_custom_10] VARCHAR(100), 
                                             [order_notes] VARCHAR(255),
                                             [order_scholarship] VARCHAR(50), 
                                             [sort_field] VARCHAR(255), 
                                             [sort_text] VARCHAR(255), 
                                             [program_counter] INT, 
                                             [production_desc] VARCHAR(30), 
                                             [attendance] INT)

            CREATE CLUSTERED INDEX [ix_trav_prog_final_data_type] ON [#trav_prog_final] ([data_type] ASC) ON [PRIMARY]
            CREATE NONCLUSTERED INDEX [ix_trav_prog_final_production_name] ON [#trav_prog_final] ([production_name] ASC) ON [PRIMARY]
            CREATE NONCLUSTERED INDEX [ix_trav_prog_final_order_no] ON [#trav_prog_final] ([order_no] ASC) ON [PRIMARY]
            CREATE NONCLUSTERED INDEX [ix_trav_prog_final_scholarship_no] ON [#trav_prog_final] ([scholarship_no] ASC) ON [PRIMARY]

    /*  Generate a list of all traveling programs sales for designated date range  */

        INSERT INTO [#trav_prog_orders]
        SELECT DISTINCT [order_no] FROM [dbo].[LV_ORDER_DETAIL]
        WHERE [performance_date] BETWEEN @start_date AND @end_date AND title_name = 'Traveling Programs'
          
    /*  Remove any orders where no scholarship payment was made  */

        DELETE FROM [#trav_prog_orders]
        WHERE [order_no] NOT IN (SELECT [order_no] FROM [#trav_prog_orders] WHERE [order_no] IN (SELECT [order_no] FROM [dbo].[LV_SCHOLARSHIP_PAYMENTS]))
        
    /* Get Traveling Programs Order and Payment Data */
    
        INSERT INTO [#trav_prog_data]
        SELECT 'ord_info', ord.[order_no], ord.[sli_no], ord.[customer_no], ord.[performance_date], ord.[title_name], 0, ord.[production_name], ord.[zone_name], ord.[price_type_name], ord.[create_dt], ord.[due_amount], 0.00, ord.[perf_no]
        FROM [dbo].[LV_ORDER_DETAIL] AS ord (NOLOCK)
        WHERE ord.[order_no] IN (SELECT [order_no] FROM [#trav_prog_orders])

        INSERT INTO [#trav_prog_data]
        SELECT 'pay_info', pay.[order_no], 0, pay.[customer_no], '', 'payment', pay.[scholarship_no], pay.[scholarship_name], '', pay.[payment_method], pay.[payment_dt], 0.00, SUM(pay.[payment_amount]),''
        FROM [dbo].[LV_SCHOLARSHIP_PAYMENTS] AS pay (NOLOCK)
        WHERE pay.[order_no] IN (SELECT [order_no] FROM [#trav_prog_orders])
        GROUP BY pay.[order_no], pay.[customer_no], pay.[scholarship_no], pay.[scholarship_name], pay.[payment_method], pay.[payment_dt]
        HAVING SUM([payment_amount]) <> 0.00
                
        DELETE FROM [#trav_prog_data] WHERE due_amount = 0.00 AND total_paid = 0.00
        UPDATE [#trav_prog_data] SET [production_name] = 'Interdepartmental Transfer' WHERE [data_type] = 'pay_info' and [production_name] = '' AND price_type_name = 'Interdepartmental Transfer'
        UPDATE [#trav_prog_data] SET [production_name] = 'Unknown Scholarship' WHERE [data_type] = 'pay_info' and  [production_name] = ''

        /*  This field needs to have a date value in it even if it's not going to be used - Put today's date into any blank fields  */

        UPDATE [#trav_prog_data] SET [performance_date] = CONVERT(CHAR(10),[create_dt],111) WHERE [performance_date] = ''

--SELECT * FROM #trav_prog_data

    /*  Create final data table  */

        INSERT INTO [#trav_prog_final]
        SELECT '', dat.[data_type], dat.[order_no], dat.[sli_no], dat.[customer_no], '', dat.[performance_date], dat.[title_name], dat.[scholarship_no], dat.[production_name], dat.[zone_name], '', 
               dat.[price_type_name], dat.[create_dt], dat.[due_amount], dat.[total_paid], ISNULL(cus.[fname], ''), ISNULL(cus.[mname], ''), ISNULL(cus.[lname], ''), ISNULL(cus.[cust_type], 0), 
               ISNULL(ctp.[description],''), ISNULL(adr.[address_type],0), ISNULL(atp.[description],''), ISNULL(adr.[street1], ''), ISNULL(adr.[Street2],''), IsNull(adr.[city],''), ISNULL(adr.[state],''), 
               ISNULL(adr.[postal_code],''), '', ISNULL(ord.[custom_1],''), ISNULL(ord.[custom_2],''), ISNULL(ord.[custom_3],''), ISNULL(ord.[custom_4],''), ISNULL(ord.[custom_5],''), 
               ISNULL(ord.[custom_6],''), ISNULL(ord.[custom_7],''), ISNULL(ord.[custom_8],''), ISNULL(ord.[custom_9],''), ISNULL(ord.[custom_0],''), ISNULL(ord.[notes],''), '', '', '', 0, od.production_name,
			   COALESCE(TRY_CONVERT(INT,ord.custom_1),0)
        FROM [#trav_prog_data] dat (NOLOCK)
             LEFT OUTER JOIN [dbo].[T_CUSTOMER] AS cus (NOLOCK) ON cus.[customer_no] = dat.[customer_no]
             LEFT OUTER JOIN [dbo].[TR_CUST_TYPE] AS ctp (NOLOCK) ON ctp.[id] = cus.[cust_type]
             LEFT OUTER JOIN [dbo].[T_ADDRESS] AS adr (NOLOCK) ON adr.[customer_no] = dat.[customer_no] AND adr.[inactive] = 'N' AND adr.primary_ind = 'Y'
             LEFT OUTER JOIN [dbo].[TR_ADDRESS_TYPE] AS atp (NOLOCK) ON atp.[id] = adr.[address_type]
             LEFT OUTER JOIN [dbo].[T_ORDER] AS ord (NOLOCK) ON ord.[order_no] = dat.[order_no]
			 LEFT OUTER JOIN [dbo].[LV_ORDER_DETAIL] od (NOLOCK) ON (od.order_no = dat.[order_no] AND od.perf_no = dat.perf_no)

    /* Count the number of programs */

        UPDATE [#trav_prog_final] SET [program_counter] = 1 WHERE [data_type] = 'ord_info' AND [zone_name] = 'Program Fee'

    /*  Add the dash in between the first five and the last four of zip plus four values  */
        
        UPDATE [#trav_prog_final] SET [address_zip_code] = LEFT([address_zip_code],5) + '-' + RIGHT([address_zip_code],4) WHERE LEN([address_zip_code]) = 9

    /*  Combine city and state into one field for sorting purposes  */

        UPDATE [#trav_prog_final] SET [address_city_state] = [address_city] + ', ' + [address_state]

    /*  Extrapolate the program code out of the notes field  -  will work as long as the code remains 11 characters long and always starts at the 19th character of the notes field  */

        UPDATE [#trav_prog_final] SET [performance_code] = SUBSTRING([order_notes],19,11) WHERE [data_type] = 'ord_info' AND [price_type_name] NOT LIKE '%Mileage%' and LEN([order_notes]) > 30

    /*  Set the performance code to Mileage Fee on all appropriate records  */

        UPDATE [#trav_prog_final] SET [performance_code] = 'Mileage Fee' WHERE [data_type] = 'ord_info' AND [price_type_name] LIKE '%Mileage%'

    /*  Combine School and School Official Record into a single customer type  */

        UPDATE [#trav_prog_final] SET [customer_type_no] = @cust_type_id_school, [customer_type] = 'School' WHERE [customer_type_no] = @cust_type_id_school_official

	/*  Insert into Performance Description   */

        UPDATE [#trav_prog_final] SET [production_desc] = (SELECT DISTINCT [production_name] FROM #trav_prog_data WHERE customer_no = [#trav_prog_final].customer_no AND order_no = [#trav_prog_final].order_no AND [data_type] = 'ord_info' )

    /*  Remove Interdepartmental Transfers */

        DELETE FROM [#trav_prog_final] WHERE [order_no] IN (SELECT [order_no] FROM [#trav_prog_final] WHERE [title_name] = 'payment' AND [production_name] = 'Interdepartmental Transfer')

    /*  Remove unwanted constiruent types and schlarships based on parameters  */

        IF @customer_type_id > 0
            DELETE FROM [#trav_prog_final] WHERE [order_no] NOT IN (SELECT [order_no] FROM [#trav_prog_final] WHERE [customer_type_no] = @customer_type_id)

        IF @scholarship_id > 0
            DELETE FROM [#trav_prog_final] WHERE [scholarship_no] <> @scholarship_id
            --DELETE FROM [#trav_prog_final] WHERE [order_no] NOT IN (SELECT [order_no] FROM [#trav_prog_final] WHERE [scholarship_no] = @scholarship_id)
        
    /*  Set Order Performance Date or Order Scholarship - Only if that's needed for the sort  */

            DECLARE date_cursor INSENSITIVE CURSOR FOR
            SELECT DISTINCT [order_no] FROM [#trav_prog_final]
            OPEN date_cursor
            BEGIN_DATE_LOOP:

                FETCH NEXT FROM date_cursor INTO @ord_no
                IF @@FETCH_STATUS = -1 GOTO END_DATE_LOOP

                SELECT @perf_date = MAX([performance_date]) FROM [#trav_prog_data] WHERE order_no = @ord_no and data_type = 'ord_info'
                SELECT @perf_date = ISNULL(@perf_date,'')
                
                --IF @ord_no = 902627 BEGIN
                --    SELECT @ord_no,@perf_date
                --    SELECT * FROM [#trav_prog_final] WHERE order_no = @ord_no
                --END

                UPDATE [#trav_prog_final] SET [order_performance_date] = @perf_date WHERE [order_no] = @ord_no
                                
                GOTO BEGIN_DATE_LOOP

            END_DATE_LOOP:
            CLOSE date_cursor
            DEALLOCATE date_cursor
            
            DECLARE scholarship_cursor INSENSITIVE CURSOR FOR
            SELECT [order_no], MAX([production_name]) FROM [#trav_prog_final] WHERE [data_type] = 'pay_info' GROUP BY [order_no]
            OPEN scholarship_cursor
            BEGIN_SCHOLARSHIP_LOOP:

                FETCH NEXT FROM scholarship_cursor INTO @ord_no, @prod_name
                IF @@FETCH_STATUS = -1 GOTO END_SCHOLARSHIP_LOOP

                UPDATE [#trav_prog_final] SET [order_scholarship] = @prod_name WHERE [order_no] = @ord_no

                GOTO BEGIN_SCHOLARSHIP_LOOP

            END_SCHOLARSHIP_LOOP:
            CLOSE scholarship_cursor
            DEALLOCATE scholarship_cursor

    /* Remove zero total payments */

        DECLARE zero_payment_cursor INSENSITIVE CURSOR FOR  
        SELECT order_no, production_name FROM [#trav_prog_final] WHERE data_type = 'pay_info' GROUP BY order_no, production_name HAVING SUM(total_paid) = 0.00
        OPEN zero_payment_cursor
        BEGIN_ZERO_PAYMENT_LOOP:

            FETCH NEXT FROM zero_payment_cursor INTO @ord_no, @prod_name
            IF @@FETCH_STATUS = -1 GOTO END_ZERO_PAYMENT_LOOP

            DELETE FROM [#trav_prog_final] WHERE order_no = @ord_no AND [data_type] = 'pay_info' AND [production_name] = @prod_name

            GOTO BEGIN_ZERO_PAYMENT_LOOP

        END_ZERO_PAYMENT_LOOP:
        CLOSE zero_payment_cursor
        DEALLOCATE zero_payment_cursor

            
    /*  Set Sort Field  */

        IF @sort_by = 'Performance Date'
            UPDATE [#trav_prog_final] 
            SET [sort_field] = [order_performance_date] + ' ' + [customer_last_name] + ' ' + [customer_first_name] + ' ' + [customer_middle_name], 
                 [sort_text] = [order_performance_date]

        ELSE IF @sort_by = 'Scholarship'
            UPDATE [#trav_prog_final] 
            SET [sort_field] = [order_scholarship] + ' ' + [customer_last_name] + ' ' + [customer_first_name] + ' ' + [customer_middle_name], 
                 [sort_text] = [order_scholarship]

        ELSE IF @sort_by = 'Customer Type'
            UPDATE [#trav_prog_final] 
            SET [sort_field] = [customer_type] + ' ' + [customer_last_name] + ' ' + [customer_first_name] + ' ' + [customer_middle_name], 
                 [sort_text] = [customer_type]

        ELSE IF @sort_by = 'Customer Name'
            UPDATE [#trav_prog_final] 
            SET [sort_field] = [customer_last_name] + ' ' + [customer_first_name] + ' ' + [customer_middle_name], 
                [sort_text] = [customer_last_name] + ' ' + [customer_first_name] + ' ' + [customer_middle_name]

        ELSE IF @sort_by = 'Zip code'
            UPDATE [#trav_prog_final] 
            SET [sort_field] = CASE WHEN ISNULL([address_zip_code],'') = '' THEN '00000' ELSE LEFT([address_zip_code],5) END 
                             + ' ' + [customer_last_name] + ' ' + [customer_first_name] + ' ' + [customer_middle_name], 
                [sort_text] = LEFT([address_zip_code],5)

        ELSE    --Default sort is Order Number 
            UPDATE [#trav_prog_final] 
            SET [sort_field] = CONVERT(VARCHAR(100),[order_no]), 
                [sort_text] = CONVERT(VARCHAR(100),[order_no])
         
    DONE:

        /*  If there is nothing in the #scr_final_table, add a single record with a message saying no records were found/  */
    
            IF NOT EXISTS (SELECT * FROM [#trav_prog_final])
                INSERT INTO [#trav_prog_final] (rpt_message) VALUES  ('No records found for the criteria entered.')
        
        /*  Select the final record set from #final_table  */

            SELECT [data_type], 
                   [order_no], 
                   [sli_no], 
                   [customer_no], 
                   [order_performance_date], 
                   [performance_date], 
                   [title_name], 
                   [scholarship_no], 
                   [production_name], 
                   [zone_name], 
                   [performance_code], 
                   [price_type_name], 
                   [program_counter], 
                   [create_dt], 
                   [due_amount], 
                   [total_paid], 
                   [customer_first_name], 
                   [customer_middle_name], 
                   [customer_last_name], 
                   [customer_type_no], 
                   [customer_type], 
                   [address_type_no], 
                   [address_type], 
                   [address_street1], 
                   [address_street2], 
                   [address_city], 
                   [address_state], 
                   [address_zip_code], 
                   [address_city_state], 
                   [order_custom_1], 
                   [order_custom_2], 
                   [order_custom_3], 
                   [order_custom_4], 
                   [order_custom_5], 
                   [order_custom_6], 
                   [order_custom_7], 
                   [order_custom_8], 
                   [order_custom_9], 
                   [order_custom_10], 
                   [order_notes], 
                   [sort_field], 
                   [sort_text], 
                   [order_scholarship], 
                   [rpt_message], 
                   [production_desc],
				   [attendance]
            FROM [#trav_prog_final]
            WHERE [data_type] = 'pay_info'
            ORDER BY [sort_field], [order_no], [data_type]


        /*  Clean up and Destroy Temporary Tables  */

            IF OBJECT_ID('tempdb..#trav_prog_final') IS NOT NULL DROP TABLE [#trav_prog_final]

            IF OBJECT_ID('tempdb..#trav_prog_data') IS NOT NULL DROP TABLE [#trav_prog_data]
            
            IF OBJECT_ID('tempdb..#trav_prog_orders') IS NOT NULL DROP TABLE [#trav_prog_orders]

        ALL_DONE:

END

GO


--EXECUTE [dbo].[LRP_SCHOLARSHIP_TRAVELING_PROGRAM] @report_start_dt = '9-1-2019', @report_end_dt = '12-31-2019', @customer_type_id = 0, @scholarship_id = 0, @sort_by = 'Zip Code'
--EXECUTE [dbo].[LRP_SCHOLARSHIP_TRAVELING_PROGRAM] @report_start_dt = '2019-12-01', @report_end_dt = '2019-12-31', @customer_type_id = 13, @scholarship_id = 0, @sort_by = 'Zip Code'



