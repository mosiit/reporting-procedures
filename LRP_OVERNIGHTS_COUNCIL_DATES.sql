USE [impresario];
GO

SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO

GO
ALTER PROCEDURE [dbo].[LRP_OVERNIGHTS_COUNCIL_DATES]
(
    @visit_start_dt DATETIME,
    @visit_end_dt DATETIME,
    @order_owner_id INT
)
AS

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
SET NOCOUNT ON;
-- =============================================
-- Author: Aileen Duffy-Brown
-- Create date: 7/11/2018 
-- TrackIt! Work Order #93437
-- Description:	Boy and Girl Scount Council specific report for Overnights.
-- Searches for all overnight orders where the VISIT DATE is between start and end dates for the customer_no.
--
-- Reworked and fixed the logic -- D.Jacob 6/23/2018
--
-- EXEC [LRP_OVERNIGHTS_COUNCIL_DATES] '9/1/2018', '7/1/2019', 2652672
-- =============================================
BEGIN

SELECT 
	X.order_no,
	X.order_dt,
	X.customer_no,
	cus.display_name AS constituentName,
	X.initiator_no,
	ISNULL(ini.display_name, '') AS initiatorName, 
	adr.street1,
	adr.street2,
	adr.street3,
	adr.city,
	adr.state,
	LEFT(adr.postal_code, 5) AS postal_code,
	X.perf_no,
	X.perf_dt,
    DATEADD(dd, -30, X.perf_dt) AS first_conf,
    DATEADD(dd, -15, X.perf_dt) AS second_conf,
    DATEADD(dd, -14, X.perf_dt) AS form_due,
	DATEPART(YEAR, X.season_start_dt) AS season_start,
	DATEPART(YEAR, X.season_end_dt) AS season_end,
	X.cnt,
	X.due_amt AS price
FROM 
( 
	SELECT 
		ord.order_no,
		ord.customer_no,
		ord.order_dt,
		ord.initiator_no,
		sli.perf_no,
		sli.due_amt,
		perf.perf_dt,
		perf.season,
		sea.start_dt AS season_start_dt,
		sea.end_dt AS season_end_dt,
		COUNT(*) AS cnt
	FROM dbo.T_ORDER AS ord
	INNER JOIN dbo.T_SUB_LINEITEM AS sli
		ON sli.order_no = ord.order_no
		AND sli.sli_status IN ( 2, 3, 11, 12 ) -- Seated, Unpaid|Seated, Paid|Upgraded|Ticketed, Paid
	INNER JOIN dbo.T_PERF AS perf
		ON perf.perf_no = sli.perf_no
		AND perf.perf_type = 12
	INNER JOIN dbo.TR_SEASON AS sea
		ON sea.id = perf.season
	WHERE perf.perf_dt BETWEEN @visit_start_dt AND @visit_end_dt
		AND ord.customer_no = @order_owner_id
	GROUP BY ord.order_no,
		ord.customer_no,
		ord.order_dt,
		ord.initiator_no,
		sli.perf_no,
		sli.due_amt,
		perf.perf_no,
		perf.perf_dt,
		perf.season,
		sea.start_dt,
		sea.end_dt
) X
left JOIN dbo.T_ADDRESS AS adr
    ON adr.customer_no = X.customer_no
       AND adr.primary_ind = 'Y'
LEFT OUTER JOIN dbo.FT_CONSTITUENT_DISPLAY_NAME() AS cus
	ON X.customer_no = cus.customer_no
LEFT OUTER JOIN dbo.FT_CONSTITUENT_DISPLAY_NAME() AS ini
	ON X.initiator_no = ini.customer_no

END;




