USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/* 
        LP_UPDATE_HISTORY

        Written by: Mark Sherwood June/July, 2017
        
        Procedure to update the LT_HISTORY_TICKET data mart that keeps track of attendance numbers.  This procedure is run each 
        early morning as part of the ticket history update and processes a single day (the previous day).

            Parameters: @history_dt = the date to be processed.  If nothing passed to this parameter, procedure defaults to yesterday
                        @create_dt = Usually null, which defaults to current date and time.  If running manually for more than one date
                                     and you want the run date/time to be the same on all dates prcocessed, pass the date and time to
                                     the procedure in this variable.
                        @include_partial_paid = Y/N on whether to include orders that are partially but noy fully paid for

        08-18-2018:  Added update statement near the end to take customer number from T_ORDER table rather than T_ATTENDANCE table
        06-21-2019:  Added exception telling procedure to ignore the Special Exhibitions venue on 6-14-2019 (Body Worlds staff Preview)
        06-29-2019:  Added the title_no and production_no columns to the data selection after adding columns to LT_HISTORY_TICKET table
        10-04-2019:  Added code to make sure the GOBoston numbers were being accurately reflected if a date is reprocessed after the initial import
        10-24-2020:  Added code to incorporate new process for identifying specific shows for buyout performances and changing production names accordingly
        01-09-2021:  Added a fiscal year column to the data selection after adding the column to the LT_HISTORY_TICKET table

*/

ALTER PROCEDURE [dbo].[LP_UPDATE_HISTORY_TICKET]
        @history_dt DATETIME = NULL,    --If nothing passed, it will default to yesterday's date.
        @create_dt DATETIME = NULL,     --When Calling for Multiple dates from another procedure so that all dates have the same logged date and time
        @include_partial_paid CHAR(1) = 'Y'
AS BEGIN

    SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

/***************************************************************************************************************************************************/

    /* Report Variables  */

        DECLARE @report_start_dt datetime, @report_end_dt datetime

        SELECT @history_dt = IsNull(@history_dt, dateadd(day, -1, getdate()))
        SELECT @create_dt = IsNull(@create_dt, getdate())

        DECLARE @city_book_adult INT = 3183412, @city_book_youth INT = 3183413
        DECLARE @city_mobile_adult INT = 3183412, @city_mobile_youth INT = 3183413

        DECLARE @mos_buyout_no INT = (SELECT ISNULL([id],0) FROM [dbo].[TR_MOS] WHERE [description] = 'Buyouts')

        DECLARE @fiscal_year INT = 0

    /*  Check parameters  */
    /* Make sure the dates passed to the procedure include the entire day (from midnight on the start date to 11:59 PM on the end date)  */

            SELECT @report_start_dt = convert(char(10),@history_dt,111) + ' 00:00:00',
                   @report_end_dt = convert(char(10),@history_dt,111) + ' 23:59:59'

            SELECT @include_partial_paid = IsNull(@include_partial_paid, 'Y')
            IF @include_partial_paid <> 'N' SELECT @include_partial_paid = 'Y'

            SELECT @fiscal_year = [dbo].[LF_GetFiscalYear](@history_dt)

    /*  Create temp tables needed for this report  */

        IF OBJECT_ID('tempdb..#prf_raw_data') is not null DROP TABLE [#prf_raw_data]
                    
        CREATE TABLE [#prf_raw_data] ([performance_no] INT, 
                                      [performance_zone] INT,
                                      [performance_date] CHAR(10),
                                      [performance_time] VARCHAR(8), 
                                      [title_no] INT,
                                      [title_name] VARCHAR(30),
                                      [production_no] INT,
                                      [production_name] VARCHAR(30),
                                      [production_name_short] VARCHAR(20), 
                                      [production_name_long] VARCHAR(150),
                                      [production_auto_scan] CHAR(1),
                                      [production_gate_attendance] CHAR(1), 
                                      [performance_type_name] VARCHAR(30))
        
        CREATE CLUSTERED INDEX [ix_prf_zone_no] ON [#prf_raw_data] ([performance_no] ASC, [performance_zone] ASC) ON [PRIMARY]
        
        IF OBJECT_ID('tempdb..#sli_temp_table') is not null DROP TABLE [#sli_temp_table]
                    
        CREATE TABLE [#sli_temp_table] ([order_no] INT,
                                        [sli_no] INT,
                                        [perf_no] INT,
                                        [zone_no] INT,
                                        [due_amt] DECIMAL (18,2),
                                        [paid_amt] DECIMAL (18,2), 
                                        [sli_status] INT,
                                        [comp_code] INT,
                                        [price_type] INT,
                                        [ticket_no] INT,
                                        [li_seq_no] INT)

        CREATE CLUSTERED INDEX [ix_sli_perf_no] ON [#sli_temp_table] ([perf_no] ASC) ON [PRIMARY]
        CREATE NONCLUSTERED INDEX [ix_sli_zone_no] ON [#sli_temp_table] ([zone_no] ASC) ON [PRIMARY]
        CREATE NONCLUSTERED INDEX [ix_sli_order_no] ON [#sli_temp_table] ([order_no] ASC) ON [PRIMARY]
        CREATE NONCLUSTERED INDEX [ix_sli_comp_code] ON [#sli_temp_table] ([comp_code] ASC) ON [PRIMARY]
        CREATE NONCLUSTERED INDEX [ix_sli_price_type] ON [#sli_temp_table] ([price_type] ASC) ON [PRIMARY]
        CREATE NONCLUSTERED INDEX [ix_sli_ticket_no] ON [#sli_temp_table] ([ticket_no] ASC) ON [PRIMARY]

        IF OBJECT_ID('tempdb..#ord_temp_table') is not null DROP TABLE [#ord_temp_table]
       
        CREATE TABLE [#ord_temp_table] ([order_no] INT,
                                        [total_due_amt] DECIMAL (18,2),
                                        [total_paid_amt] DECIMAL (18,2))

        CREATE CLUSTERED INDEX [ix_ord_order_no] ON [#ord_temp_table] ([order_no] ASC) ON [PRIMARY]
        CREATE NONCLUSTERED INDEX [ix_ord_total_paid] ON [#ord_temp_table] ([total_paid_amt] ASC) ON [PRIMARY]
        CREATE NONCLUSTERED INDEX [ix_ord_total_due] ON [#ord_temp_table] ([total_due_amt] ASC) ON [PRIMARY]
        
        IF OBJECT_ID('tempdb..#att_temp_table') is not null DROP TABLE [#att_temp_table]

        CREATE TABLE [#att_temp_table] ([attendance_type] VARCHAR (50),
                                        [performance_type] VARCHAR (50),
                                        [order_no] INT,
                                        [sli_no] INT,
                                        [li_seq_no] INT, 
                                        [title_no] INT,
                                        [title_name] VARCHAR (30),
                                        [perf_no] INT,
                                        [perf_date] CHAR (10),
                                        [perf_time] CHAR (8),
                                        [zone_no] INT, 
                                        [production_no] INT,
                                        [production_name] VARCHAR (50),
                                        [production_name_short] VARCHAR (50),
                                        [production_name_long] VARCHAR (150), 
                                        [comp_code] INT,
                                        [comp_code_name] VARCHAR (50),
                                        [order_payment_status] VARCHAR (50),
                                        [price_type] INT, 
                                        [price_type_name] VARCHAR (50),
                                        [due_amt] DECIMAL (18,2),
                                        [paid_amt] DECIMAL (18,2),
                                        [sale_total] INT, 
                                        [performance_date] CHAR (10),
                                        [performance_time] CHAR (8),
                                        [scan_admission_date] CHAR (10), 
                                        [scan_admission_time] CHAR (8),
                                        [scan_device] VARCHAR (30),
                                        [scan_admission_adult] INT,
                                        [scan_admission_child] INT, 
                                        [scan_admission_other] INT,
                                        [scan_admission_auto] INT,
                                        [scan_admission_total] INT,
                                        [sli_status] INT, 
                                        [customer_no] INT,
                                        [report_start_date] DATETIME,
                                        [report_end_date] DATETIME,
                                        [include_partial_paid] CHAR (1),
                                        [fiscal_year] INT)

    /***************************************************************************************************************************************************/

    /*  Get Performance information  */

        INSERT INTO [#prf_raw_data] ([performance_no], [performance_zone], [performance_date], [performance_time], [title_no], [title_name],
                                     [production_no], [production_name], [production_name_short], [production_name_long], [production_auto_scan],
                                     [production_gate_attendance], [performance_type_name])
        SELECT [performance_no], 
               [performance_zone], 
               [performance_date], 
               [performance_time], 
               [title_no], 
               [title_name], 
               [production_no], 
               [production_name], 
               [production_name_short], 
               [production_name_long], 
               [production_auto_scan], 
               [production_gate_attendance], 
               [performance_type_name]
        FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE]
        WHERE [performance_dt] between @report_start_dt AND @report_end_dt 
          AND performance_type_name <> 'Show & Go'


    /*  Get Products Sold For date range  -  This table will keep a list of all subline items we will be working with
        Looks specifically for subline items where the status is 3 (Seated, Paid) or 12 (Ticketed, Paid)   */

        INSERT INTO [#sli_temp_table] ([order_no], [sli_no], [perf_no], [zone_no], [due_amt], [paid_amt], [sli_status], 
                                       [comp_code], [price_type], [ticket_no], [li_seq_no])
        SELECT [order_no], 
               [sli_no], 
               [perf_no], 
               [zone_no], 
               [due_amt], 
               [paid_amt], 
               [sli_status], 
               [comp_code], 
               [price_type], 
               [ticket_no], 
               [li_seq_no]
        FROM [dbo].[T_SUB_LINEITEM] as sli
        WHERE sli.[perf_no] in (SELECT [performance_no] 
                                FROM [#prf_raw_data]) 
          AND sli.[sli_status] in (3,12)

    /*  Get Order Information the products sold - This table will keep a list of all orders we will be working with   */

        INSERT INTO [#ord_temp_table] ([order_no], [total_due_amt], [total_paid_amt])
        SELECT [order_no], 
               [tot_due_amt], 
               [tot_paid_amt] 
        FROM [dbo].[T_ORDER] 
        WHERE [order_no] in (SELECT [order_no] 
                             FROM [#sli_temp_table])

    /*  Remove records that are not needed  */

        --Delete Unpaid Transactions from the order table
        DELETE FROM [#ord_temp_table] 
        WHERE [total_due_amt] > 0.00 
          AND [total_paid_amt] = 0.00

        IF @include_partial_paid <> 'Y' 
            DELETE FROM [#ord_temp_table] 
            WHERE [total_paid_amt] < [total_due_amt]
        
        --Delete from subline items where the order is not included in the order table    
        DELETE FROM [#sli_temp_table] 
        WHERE [order_no] not IN (SELECT [order_no] 
                                 FROM [#ord_temp_table])

    /*  Retrieve the TICKETED Attendance date from the database  */

        INSERT INTO [#att_temp_table] ([attendance_type], [performance_type], [order_no], [sli_no], [li_seq_no], [title_no], [title_name], [perf_no], [perf_date],
                                       [perf_time], [zone_no], [production_no], [production_name], [production_name_short], [production_name_long], [comp_code],
                                       [comp_code_name], [order_payment_status], [price_type], [price_type_name], [due_amt], [paid_amt], [sale_total], [performance_date],
                                       [performance_time], [scan_admission_date], [scan_admission_time], [scan_device], [scan_admission_adult], [scan_admission_child],
                                       [scan_admission_other], [scan_admission_auto], [scan_admission_total], [sli_status], [customer_no], [report_start_date], [report_end_date],
                                       [include_partial_paid], [fiscal_year])
        SELECT  CASE WHEN prf.[production_gate_attendance] = 'N' THEN 'Non-Gate' ELSE 'Gate A (Ticketed)' END,
                prf.[performance_type_name], 
                sli.[order_no], 
                sli.[sli_no], 
                sli.[li_seq_no],
                prf.[title_no], 
                prf.[title_name], 
                sli.[perf_no], 
                prf.[performance_date], 
                prf.[performance_time], 
                sli.[zone_no], 
                prf.[production_no],
                prf.[production_name], 
                prf.[production_name_short], 
                prf.[production_name_long], 
                ISNULL(sli.[comp_code],0), 
                ISNULL(cmp.[description],''), 
                'paid' , 
                ISNULL(sli.[price_type],0), 
                ISNULL(typ.[description],'Unknown'), 
                sli.[due_amt], 
                sli.[paid_amt], 
                1 , 
                prf.[performance_date], 
                prf.[performance_time],
                CASE WHEN prf.[production_auto_scan] = 'Y' THEN prf.[performance_date] ELSE convert(char(10),att.[attend_dt],111) END,
                CASE WHEN prf.[production_auto_scan] = 'Y' THEN prf.[performance_time] ELSE convert(char(8),att.[attend_dt],108) END,
                ISNULL(att.[device],''), 
                ISNULL(att.[admission_adult], 0), 
                ISNULL(att.[admission_child], 0),
                ISNULL(att.[admission_other], 0),
                CASE WHEN prf.[production_auto_scan] = 'Y' THEN 1 ELSE 0 END,
                CASE WHEN prf.[production_auto_scan] = 'Y' THEN 1 ELSE (IsNull(att.[admission_adult], 0) + IsNull(att.[admission_child], 0) 
                                                                        + IsNull(att.[admission_other], 0)) END,
                sli.[sli_status], 
                ISNULL(att.[customer_no],0),
                @create_dt, 
                NULL, 
                NULL,
                @fiscal_year
        FROM  [dbo].[T_SUB_LINEITEM] as sli
              LEFT OUTER JOIN [#prf_raw_data] as prf ON prf.[performance_no] = sli.[perf_no] and prf.[performance_zone] = sli.[zone_no]
              LEFT OUTER JOIN [dbo].[TR_COMP_CODE] as cmp ON cmp.[id] = sli.[comp_code]
              LEFT OUTER JOIN [dbo].[TR_PRICE_TYPE] as typ ON typ.[id] = sli.[price_type]
              LEFT OUTER JOIN [dbo].[T_ATTENDANCE] as att ON att.[ticket_no] = sli.[ticket_no]
        WHERE prf.[performance_no] in (SELECT [perf_no] FROM [#sli_temp_table])

    /* Added 10/26/2017: Moves any order made in the Buyouts mode of sale to non-gate attendance regardless of what the show title is.
                         @mos_buyout_no is defined above and gets the mode of sale number from the TR_MOS table.  
                         It must find and if number > 0 for this to happen.  */
    
        IF @mos_buyout_no > 0
            UPDATE [#att_temp_table] 
            SET [attendance_type] = 'Non-Gate'
            WHERE [order_no] IN (SELECT [order_no] 
                                 FROM [dbo].[T_ORDER] 
                                 WHERE [order_no] IN (SELECT [order_no] 
                                                      FROM [#att_temp_table]) 
                                   AND [MOS] = @mos_buyout_no)


    /***************************************************************************************************************************************************/

    /*  Delete returned/refunded products  */

        DELETE FROM [#att_temp_table] 
        WHERE [sli_status] not in (3, 12)

    /*  Hard-Coded Exception specifically for July 4th Event on 7/4/2017  */

        IF CONVERT(DATE,@history_dt) = '7-4-2017'
            UPDATE [#att_temp_table]
            SET [production_name] = 'July 4th HC Parking', 
                [production_name_long] = 'July 4th HC Parking', 
                [production_name_short] = 'Parking'
            WHERE [performance_date] = '2017/07/04' 
              AND [production_name] = 'July 4th Event' 
              AND [perf_no] = 24297

    /*  Hard-Coded Except specifically for the Body Worlds Staff Preview on 6/14/2019 - Remove it */

        IF CONVERT(DATE,@history_dt) = '6-14-2019'
            DELETE FROM [#att_temp_table]
            WHERE [performance_date] = '2019/06/14' 
              AND [title_name] = 'Special Exhibitions'
        

    /*  Set Traveling Programs Mileage Fee to its own product to separate it out in the report  */

        UPDATE [#att_temp_table] 
        SET production_name = 'Trav Prog Mileage Fee', 
            production_name_short = 'Mileage Fee', 
            production_name_long = 'Traveling Programs Mileage Fee'
        WHERE [price_type_name] like 'Trav Prog Mileage%'

    /*  Update Library Pass Lines so that they appear as discounts in Attendance Report  */

        UPDATE [#att_temp_table] 
        SET [comp_code] = -1, 
            [comp_code_name] = 'Library Pass - FREE' 
        WHERE [title_name] = 'Exhibit Halls' 
          AND [price_type_name] = 'Exhibit Halls Library Free'

        UPDATE [#att_temp_table] 
        SET [comp_code] = -2, 
            [comp_code_name] = 'Library Pass - PAID' 
        WHERE [title_name] = 'Exhibit Halls' 
          AND [price_type_name] = 'Exhibit Halls Library Paid'

    /*  Retrieve the NON-TICKETED Attendance data (minus Show and Go) from the database  
        NOTE: Membership scans tie back to the Exhibit Hall product for that day.  */

        INSERT INTO [#att_temp_table] ([attendance_type], [performance_type], [order_no], [sli_no], [li_seq_no], [title_no], [title_name], [perf_no], [perf_date], [perf_time],
                                       [zone_no], [production_no], [production_name], [production_name_short], [production_name_long], [comp_code], [comp_code_name], [order_payment_status],
                                       [price_type], [price_type_name], [due_amt], [paid_amt], [sale_total], [performance_date], [performance_time], [scan_admission_date], 
                                       [scan_admission_time], [scan_device], [scan_admission_adult], [scan_admission_child], [scan_admission_other], [scan_admission_auto],
                                       [scan_admission_total], [sli_status], [customer_no], [report_start_date], [report_end_date], [include_partial_paid], [fiscal_year])
        SELECT 'Gate B (Non-Ticketed)', 
               'Public', 
               0, 
               0, 
               0, 
               0,
               'Gate Scan', 
               att.[perf_no], 
               prf.[performance_date], 
               ISNULL(prf.[performance_time], '09:00'),
               0, 
               0,
               lev.[description], 
               lev.[description], 
               lev.[description], 
               0, 
               'No Discount', 
               'paid', 
               0, 
               '', 
               0.00, 
               0.00, 
               0, 
               CONVERT(CHAR(10),att.[attend_dt],111), 
               ISNULL(prf.[performance_time], '09:00'),
               CONVERT(CHAR(10),att.[attend_dt],111), 
               CONVERT(CHAR(8),att.[attend_dt],108), 
               ISNULL(att.[device],''), 
               ISNULL(att.[admission_adult], 0), 
               ISNULL(att.[admission_child], 0), 
               ISNULL(att.[admission_other], 0), 
               0, 
               (ISNULL(att.[admission_adult], 0) + ISNULL(att.[admission_child], 0) + ISNULL(att.[admission_other], 0)), 
               0,  
               ISNULL(att.[customer_no],0),
               @create_dt, 
               NULL, 
               NULL,
               @fiscal_year
        FROM [dbo].[T_ATTENDANCE] AS att                                                                 --236=primary zone # for public Exhibit Halls
             LEFT OUTER JOIN [#prf_raw_data] AS prf ON prf.[performance_no] = att.[perf_no] AND prf.[performance_zone] = 236 
             LEFT OUTER JOIN [dbo].[T_MEMB_LEVEL] AS lev ON lev.[memb_level_no] = att.[memb_level_no]                        
        WHERE ISNULL(att.[ticket_no], 0) = 0 AND att.[attend_dt] BETWEEN @report_start_dt AND @report_end_dt AND att.[area_no] IS NULL


    /*  Retrieve the SHOW AND GO Attendance data from the database  */

        INSERT INTO [#att_temp_table] ([attendance_type], [performance_type], [order_no], [sli_no], [li_seq_no], [title_no], [title_name], [perf_no], [perf_date], [perf_time],
                                       [zone_no], [production_no], [production_name], [production_name_short], [production_name_long], [comp_code], [comp_code_name], [order_payment_status],
                                       [price_type], [price_type_name], [due_amt], [paid_amt], [sale_total], [performance_date], [performance_time], [scan_admission_date], 
                                       [scan_admission_time], [scan_device], [scan_admission_adult], [scan_admission_child], [scan_admission_other], [scan_admission_auto],
                                       [scan_admission_total], [sli_status], [customer_no], [report_start_date], [report_end_date], [include_partial_paid], [fiscal_year])
        SELECT 'Gate B (Non-Ticketed)', 
               'Public', 
               0, 
               0, 
               0, 
               0,
               'Show and Go', 
               0, 
               '', 
               '', 
               0, 
               0,
               [show_and_go_name], 
               [show_and_go_name], 
               [show_and_go_name], 
               0, 
               'Free Admission', 
               'Free', 
               0, 
               '', 
               0.00, 
               0.00, 
               0, 
               [scan_date], 
               ISNULL([scan_time], '09:00'),
               [scan_date], 
               [scan_time],
               [device_name], 
               0, 
               0, 
               [scan_admission], 
               0, 
               [scan_admission], 
               0, 
               ISNULL([show_and_go_no], 0),
               @create_dt, 
               NULL, 
               NULL,
               @fiscal_year
        FROM [dbo].[LV_RPT_SHOW_AND_GO]
        WHERE [scan_dt] BETWEEN @report_start_dt AND @report_end_dt

    /*  Retrieve the GO BOSTON Attendance data from the database  */

        INSERT INTO [#att_temp_table] ([attendance_type], [performance_type], [order_no], [sli_no], [li_seq_no], [title_no], [title_name], [perf_no], [perf_date], [perf_time],
                                       [zone_no], [production_no], [production_name], [production_name_short], [production_name_long], [comp_code], [comp_code_name], [order_payment_status],
                                       [price_type], [price_type_name], [due_amt], [paid_amt], [sale_total], [performance_date], [performance_time], [scan_admission_date], 
                                       [scan_admission_time], [scan_device], [scan_admission_adult], [scan_admission_child], [scan_admission_other], [scan_admission_auto],
                                       [scan_admission_total], [sli_status], [customer_no], [report_start_date], [report_end_date], [include_partial_paid], [fiscal_year])
        SELECT 'Gate B (Non-Ticketed)',                 --attendance_type
               'Public',                                --performance_type
               0,                                       --order_no
               999999,                                  --In sli_no column
               goc.[go_id],                             --in line_seq_no column
               0,                                       --title_no
               'Show and Go',                           --title_name
               0,                                       --perf_no
               '',                                      --perf_date (report uses performance_date instead)
               '',                                      --perf_time (report used performance_time instead)
               0,                                       --zone_no
               0,                                       --production_no
               'GoBoston',                              --production_name
               'GoBoston',                              --production_name_short
               'GoBoston',                              --production_name_long
               0,                                       --comp_code
               'Free Admission',                        --comp_code_name
               'Free',                                  --order_payment_status
               0,                                       --price_type
               '',                                      --price_type_name
               0.0,                                     --due_amt
               0.0,                                     --paid_amt
               0,                                       --sale_total
               CONVERT(CHAR(10),goc.[go_scan_dt],111),  --perf_date
               '09:00',                                 --perf_time
               CONVERT(CHAR(10),goc.[go_scan_dt],111),  --scan_admission_date
               CONVERT(CHAR(8),goc.[go_scan_dt],108),   --scan_admission_time
               goc.[go_confirmation_code],                --scan_device
               0,                                       --scan_admission_adult
               0,                                       --scan_admission_child
               1,                                       --scan_admission_other (all show and go is other)
               0,                                       --scan_admission_auto
               1,                                       --scan_admission_total
               0,                                       --slu_status
               0,                                       --customer_no
               @create_dt,                              --create_dt
               NULL,
               NULL,
               @fiscal_year
        FROM [dbo].[LT_GOCITY_SCANS] AS goc
        WHERE goc.[go_scan_dt] BETWEEN @report_start_dt AND @report_end_dt

        /*  Retrieve the REDEAM Attendance data from the database  */

        INSERT INTO [#att_temp_table] ([attendance_type], [performance_type], [order_no], [sli_no], [li_seq_no], [title_name], [perf_no], [perf_date], [perf_time], [zone_no],
                                               [production_name], [production_name_short], [production_name_long], [comp_code], [comp_code_name], [order_payment_status], [price_type],
                                               [price_type_name], [due_amt], [paid_amt], [sale_total], [performance_date], [performance_time], [scan_admission_date], [scan_admission_time],
                                               [scan_device], [scan_admission_adult], [scan_admission_child], [scan_admission_other], [scan_admission_auto], [scan_admission_total], 
                                               [sli_status], [customer_no], [report_start_date], [report_end_date], [include_partial_paid], [fiscal_year])
        SELECT 'Gate B (Non-Ticketed)',
               'Public',
               0,
               999999,
               rdm.[redeam_id],
               'Show and Go',
               0,
               '',
               '',
               0,
               'Redeam',
               'Redeam',
               'Redeam',
               0,
               '',
               '',
               0,
               '',
               0.0,
               0.0,
               0,
               CONVERT(CHAR(10), rdm.[redeam_dt], 111),
               '09:00',
               CONVERT(CHAR(10), rdm.[redeam_dt], 111),
               '09:00',
               'Redeam Scanner',
               0,
               0,
               rdm.[redeam_count],
               0,
               rdm.[redeam_count],
               0,
               0,
               @create_dt,
               NULL,
               NULL, 
               @fiscal_year
        FROM [dbo].[LT_REDEAM_SCANS] AS rdm
             LEFT OUTER JOIN [dbo].[LT_HISTORY_TICKET] AS his ON his.[performance_date] =  CONVERT(CHAR(10),rdm.[redeam_dt],111) AND his.[production_name] = 'Redeam'
        WHERE rdm.[redeam_dt] BETWEEN @report_start_dt AND @report_end_dt
              

    /*  Retrieve the SHOW AND COLLECTED Attendance data from the database  */

        INSERT INTO [#att_temp_table] ([attendance_type], [performance_type], [order_no], [sli_no], [li_seq_no], [title_no], [title_name], [perf_no], [perf_date], [perf_time],
                                       [zone_no], [production_no], [production_name], [production_name_short], [production_name_long], [comp_code], [comp_code_name], [order_payment_status],
                                       [price_type], [price_type_name], [due_amt], [paid_amt], [sale_total], [performance_date], [performance_time], [scan_admission_date], 
                                       [scan_admission_time], [scan_device], [scan_admission_adult], [scan_admission_child], [scan_admission_other], [scan_admission_auto],
                                       [scan_admission_total], [sli_status], [customer_no], [report_start_date], [report_end_date], [include_partial_paid], [fiscal_year])
        SELECT 'Gate B (Non-Ticketed)', 
               'Public', 
               0, 
               0, 
               0, 
               0,
               'Show and Collected', 
               0, 
               '', 
               '', 
               0, 
               0,
               [show_and_collected_name], 
               [show_and_collected_name], 
               [show_and_collected_name], 
               0, 
               'Free Admission', 
               'Free', 
               0, 
               '', 
               0.00, 
               0.00, 
               0, 
               [scan_date], 
               ISNULL([scan_time], '09:00'),
               [scan_date], 
               [scan_time], 
               [device_name], 
               0, 
               0, 
               [scan_admission], 
               0, 
               [scan_admission], 
               0, 
               ISNULL([show_and_collected_no],0),
               @create_dt, 
               NULL, 
               NULL,
               @fiscal_year
        FROM [dbo].[LV_RPT_SHOW_AND_COLLECTED]
        WHERE [scan_dt] BETWEEN @report_start_dt AND @report_end_dt
    

    /*  Stamped Hand at Discovery Center means they were already scanned and counted somewhere else - Don't count them again  */

        DELETE FROM [#att_temp_table] 
        WHERE [title_name] = 'Show and Go' 
          AND [production_name] = 'Stamped Hands at Discovery Center'


    /*  ****Added 8-16-2018****
        Customer number was being pulled by the T_ATTENDANCE (NScan) table and when a ticket wasn't scanned, it was coming in as NULL
        Not important until now because we didn't report on any history from the LT_HISTORY_TICKET table based on customer number.
        The new Library Attendance report makes it relevant so the update statement below will set the customer number to what's in
        the T_ORDER table rather than what's in the T_ATTENDANCE table.  */
    
        --UPDATE his
        --SET his.customer_no = ISNULL(ord.customer_no,0)
        --FROM [#att_temp_table] AS his
        --     LEFT OUTER join [dbo].[T_ORDER] AS ord ON ord.[order_no] = his.[order_no] and ord.customer_no > 0

    /*  ****Fixed 2/12/2020****
        When this was originally added, it failed to take into account records not tied to orders with customer numbers
        so on most of the records, it was resetting the customer number to zero - New Where Clause only makes the change
        if there is a customer number in the order - If no customer number, it leaves it alone  */

        UPDATE his
        SET his.[customer_no] = ISNULL(ord.customer_no,0)
        FROM [#att_temp_table] AS his
             LEFT OUTER join [dbo].[T_ORDER] AS ord ON ord.[order_no] = his.[order_no]
        WHERE ISNULL(ord.customer_no, 0) > 0
                
    /*  7-5-2019:  Added this line to change title name of Gate Scan to Membership Card Scan  */

        UPDATE [#att_temp_table]
        SET [title_name] = 'Membership Card Scan'
        WHERE [title_name] = 'Gate Scan'


    /*  10-24-2020:  Change added to better track Buyout Shows
        Came up with a process to identify what specific production is being shown in an "Omni Buyout" or "Omni Show" performance
        If the proper content exists, the production on the Buyout Record will be changed to the actual title being shown
        If the proper content does not exist, the show title remains unchanged  */

        UPDATE [his]
        SET his.[production_no] = CAST(ISNULL(con.[value],'0') AS INT),
            his.[production_name] = COALESCE(inv.[description], his.[production_name], '')
        FROM [#att_temp_table] AS his
             INNER JOIN [dbo].[TX_INV_CONTENT] AS con ON con.[inv_no] = his.[perf_no] AND con.[content_type] = 48  --48 = Omni Production
             INNER JOIN [dbo].[T_INVENTORY] AS inv ON inv.[inv_no] = CAST(ISNULL(con.[value],'0') AS INT) AND inv.[type] = 'P'
        WHERE his.[production_no] IN (235, 5970)    --235 = Omni Show / 5970 = Omni Buyout


    /* 4/23/2021:  This new update statement find Exhibit Hall history records for orders made uisng the new corporate pass procedure and it
                   Changes them to look like they were scanned at the gate.  This allows the various companies to be broken out  into separate 
                   rows on the report instead of being lumped into everything else.  The second statement finds non-Exhibit Hall history
                   records and changes the comp code name to be the name of the business (or the first 30 characters of a long name)  */

        UPDATE att
        SET att.[attendance_type] = 'Gate B (Non-Ticketed)',
            att.[sli_no] = 0,
            att.[li_seq_no] = 0,
            att.[title_no] = 0,
            att.[title_name] = 'Show and Go',
            att.[perf_no] = 0,
            att.[perf_date] = '',
            att.[perf_time] = '',
            att.[zone_no] = 0,
            att.[production_no] = 0,
            att.[production_name] = det.[source_name],
            att.[production_name_short] = det.[source_name],
            att.[production_name_long] = det.[source_name],
            att.[comp_code_name] = 'Free Admission',
            att.[order_payment_status] = 'Free',
            att.[price_type] = 0,
            att.[price_type_name] = '',
            att.[scan_admission_date] = att.[performance_date],
            att.[scan_admission_time] = left(att.[performance_time],5) + ':00',
            att.[scan_device] = 'N-Scan',
            att.[scan_admission_adult] = att.[sale_total],
            att.[scan_admission_total] = att.[sale_total],
            att.[sli_status] = 0,
            att.[sale_total] = 0
        FROM [#att_temp_table] AS att
             INNER JOIN [dbo].[LV_CORPORATE_PASS_ORDER_DETAIL] AS det ON det.[sli_no] = att.[sli_no]
        WHERE att.[title_no] = 27
             
        UPDATE att
        SET att.[comp_code_name] = LEFT(det.[source_name], 30),
            att.[order_payment_status] = 'Free'
        FROM [#att_temp_table] AS att
             INNER JOIN [dbo].[LV_CORPORATE_PASS_ORDER_DETAIL] AS det ON det.[sli_no] = att.[sli_no] 
        WHERE att.[title_no] <> 27

    /*  COVID 19 UPDATE (part 1)
        Adjusts history data so that the items that used to be scanned at the gate 
        look (to the attendance reports) as if they are still being scanned at the gate.  */
        
                        IF @history_dt >= '7-1-2020' BEGIN

                            /*  8/5/2020 - Code added to remove any orders that were nor scanned ("No Shows") 
                                           Process the rest like before  */

                                DELETE FROM [#att_temp_table]
                                WHERE [title_no] = 27
                                  AND [price_type_name] IN ('Show and Go Free','Show and Go Corp')
                                  AND [scan_admission_total] = 0

                                --SHOW AND GO (FREE AND CORP)
                                UPDATE his
                                SET his.[attendance_type] = 'Gate B (Non-Ticketed)',
                                    his.[sli_no] = 0,
                                    his.[li_seq_no] = 0,
                                    his.[title_name] = 'Show and Go',
                                    his.[perf_no] = 0,
                                    his.[perf_date] = '',
                                    his.[perf_time] = '',
                                    his.[zone_no] = 0,
                                    his.[comp_code] = 0,
                                    his.[production_name] = his.[comp_code_name],
                                    his.[production_name_long] = his.[comp_code_name],
                                    his.[production_name_Short] = his.[comp_code_name],
                                    his.[comp_code_name] = 'Free Admission',
                                    his.[order_payment_status] = 'Free',
                                    his.[price_type] = 0,
                                    his.[price_type_name] = '',
                                    his.[sale_total] = 0,
                                    his.[performance_time] = IsNull(his.[scan_admission_time],his.[performance_time]),
                                    his.[scan_admission_date] = IsNull(his.[scan_admission_date],his.[performance_date]),
                                    his.[scan_admission_time] = IsNull(his.[scan_admission_time],his.[performance_time]),
                                    his.[scan_admission_other] = 1,
                                    his.[scan_admission_total] = 1,
                                    --his.[customer_no] = ISNULL(map.[customer_no],0),
                                    his.[sli_status] = 0,
                                    his.[title_no] = 0,
                                    his.[production_no] = 0
                                FROM [#att_temp_table] AS his
                                     LEFT OUTER JOIN [dbo].[LTR_COMP_CUSTOMER_MAP] AS map ON map.[comp_code_no] = his.[comp_code]
                                WHERE his.[title_no] = 27 
                                  AND his.[price_type_name] IN ('Show and Go Free','Show and Go Corp')
  
                            /*  8/5/2020 - Code added to remove any orders that were nor scanned ("No Shows") 
                                           Process the rest like before  */

                                DELETE FROM [#att_temp_table]
                                WHERE [title_no] = 27
                                  AND [price_type_name] = 'Show and Go Collected'
                                  AND [scan_admission_total] = 0

                                --SHOW AND COLLECTED
                                UPDATE his
                                SET his.[attendance_type] = 'Gate B (Non-Ticketed)',
                                    his.[sli_no] = 0,
                                    his.[li_seq_no] = 0,
                                    his.[title_name] = 'Show and Collected',
                                    his.[perf_no] = 0,
                                    his.[perf_date] = '',
                                    his.[perf_time] = '',
                                    his.[zone_no] = 0,
                                    his.[comp_code] = 0,
                                    his.[production_name] = his.[comp_code_name],
                                    his.[production_name_long] = his.[comp_code_name],
                                    his.[production_name_Short] = his.[comp_code_name],
                                    his.[comp_code_name] = 'Free Admission',
                                    his.[order_payment_status] = 'Free',
                                    his.[price_type] = 0,
                                    his.[price_type_name] = '',
                                    his.[sale_total] = 0,
                                    his.[performance_time] = IsNull(his.[scan_admission_time],his.[performance_time]),
                                    his.[scan_admission_date] = IsNull(his.[scan_admission_date],his.[performance_date]),
                                    his.[scan_admission_time] = IsNull(his.[scan_admission_time],his.[performance_time]),
                                    his.[scan_admission_other] = 1,
                                    his.[scan_admission_total] = 1,
                                    --his.[customer_no] = ISNULL(map.[customer_no],0),
                                    his.[sli_status] = 0,
                                    his.[title_no] = 0,
                                    his.[production_no] = 0
                                FROM [#att_temp_table] AS his
                                     LEFT OUTER JOIN [dbo].[LTR_COMP_CUSTOMER_MAP] AS map ON map.[comp_code_no] = his.[comp_code]
                                WHERE his.[title_no] = 27
                                  AND his.[performance_date] = CONVERT(CHAR(10),@history_dt,111) 
                                  AND his.[price_type_name] = 'Show and Go Collected'


                            /*  8/5/2020 - Code added to remove any orders that were not scanned ("No Shows") 
                                           Process the rest like before  */

                                DELETE FROM [#att_temp_table]
                                WHERE [title_no] = 27
                                  AND [price_type_name] like '%Under 3%'
                                  AND [scan_admission_total] = 0


                               --UNDER 3 ADMISSIONS
                                UPDATE [#att_temp_table]
                                SET [attendance_type] = 'Gate B (Non-Ticketed)',
                                    [sli_no] = 0,
                                    [li_seq_no] = 0,
                                    [title_name] = 'Show and Go',
                                    [perf_no] = 0,
                                    [perf_date] = '',
                                    [perf_time] = '',
                                    [zone_no] = 0,
                                    [production_name] = 'Under 3',
                                    [production_name_long] = 'Under 3',
                                    [production_name_Short] = 'Under 3',
                                    [comp_code_name] = 'Free Admission',
                                    [order_payment_status] = 'Free',
                                    [price_type] = 0,
                                    [price_type_name] = '',
                                    [sale_total] = 0,
                                    [performance_time] = IsNull([scan_admission_time],[performance_time]),
                                    [scan_admission_date] = IsNull([scan_admission_date],[performance_date]),
                                    [scan_admission_time] = IsNull([scan_admission_time],[performance_time]),
                                    [scan_admission_other] = 1,
                                    [scan_admission_total] = 1,
                                    [sli_status] = 0,
                                    [title_no] = 0,
                                    [production_no] = 0
                                WHERE [title_no] = 27
                                  AND [price_type_name] like '%Under 3%';


                            /*  8/5/2020 - Code added to remove any orders that were nor scanned ("No Shows") 
                                           Process the rest like before  */

                                DELETE FROM [#att_temp_table]
                                WHERE [title_no] = 27
                                  AND [price_type_name] Like '%Library%'
                                  AND [paid_amt] = 0.0
                                  AND [scan_admission_total] = 0

                                --FREE LIBRARY PASSES
                                UPDATE his 
                                SET his.[attendance_type] = 'Gate B (Non-Ticketed)',
                                    his.[title_name] = 'Show and Collected',
                                    his.[perf_no] = 0,
                                    his.[perf_date] = '',
                                    his.[perf_time] = '',
                                    his.[zone_no] = '',
                                    his.[production_name] = ISNULL(cus.[lname],'Free Library'),
                                    his.[production_name_short] = ISNULL(cus.[lname],'Free Library'),
                                    his.[production_name_long] = ISNULL(cus.[lname],'Free Library'),
                                    his.[comp_code] = 0,
                                    his.[comp_code_name] = 'Free Admission',
                                    his.[order_payment_status] = 'Free',
                                    his.[scan_admission_date] = IsNull(his.[scan_admission_date],his.[performance_date]),
                                    his.[scan_admission_time] = IsNull(his.[scan_admission_time],his.[performance_time]),
                                    his.[scan_admission_other] = his.[sale_total],
                                    his.[scan_admission_total] = his.[sale_total],
                                    his.[title_no] = 0,
                                    his.[production_no] = 0
                                FROM [#att_temp_table] AS his
                                     LEFT OUTER JOIN [dbo].[T_CUSTOMER] AS cus ON cus.[customer_no] = his.[customer_no]
                                WHERE his.[price_type_name] Like '%Library%'
                                  AND his.[paid_amt] = 0.0;

     
                            /*  8-8-2020 - Added delete statemet to deal with GoBoston sales the same way as CityPass sales (below)  */
                            
                                DELETE FROM [#att_temp_table]
                                WHERE [price_type_name] like 'GoBoston%'
                                 AND [paid_amt] = 0.0;
      

                            /*  7-31-2020 - City Pass orders now being removed because they are also being scanned at the gate
                                            The Gate scans will be counted.  Counting these orders too is double-countint people  */

                                DELETE FROM [#att_temp_table]
                                WHERE [price_type_name] like 'CityPASS%'
                                  AND [paid_amt] = 0.0;


                            /*  8/5/2020 - Code added to remove any orders that were not scanned ("No Shows") 
                                           Process the rest like before - Memberships processed after data is moved to History Table  */
                            
                                DELETE FROM [#att_temp_table]
                                WHERE [title_no] = 27
                                  AND [paid_amt] = 0.0
                                  AND [comp_code_name] = ''
                                  AND [price_type_name] NOT LIKE '%Under 3%'
                                  AND [price_type_name] NOT LIKE '%Library%'
                                  AND [dbo].[LFS_ActiveMemberOnDate]([customer_no],0,[performance_date],'N') = 'Y'
                                  AND [scan_admission_total] = 0

                            END

    /*  Move data over to the History_table */

        IF EXISTS (SELECT * FROM [dbo].[LT_HISTORY_TICKET] WHERE performance_date = CONVERT(CHAR(10),@history_dt,111))
                DELETE FROM [dbo].[LT_HISTORY_TICKET] WHERE performance_date = CONVERT(CHAR(10),@history_dt,111)

        INSERT INTO [dbo].[LT_HISTORY_TICKET] ([attendance_type], [performance_type], [order_no], [sli_no], [li_seq_no], [title_name], [perf_no], [perf_date], [perf_time],
                                               [zone_no], [production_name], [production_name_short], [production_name_long], [comp_code], [comp_code_name], [order_payment_status],
                                               [price_type], [price_type_name], [due_amt], [paid_amt], [sale_total], [performance_date], [performance_time], [scan_admission_date],
                                               [scan_admission_time], [scan_device], [scan_admission_adult], [scan_admission_child], [scan_admission_other], [scan_admission_auto],
                                               [scan_admission_total], [sli_status], [customer_no], [create_dt], [title_no], [production_no], [fiscal_year])
        SELECT [attendance_type], 
               [performance_type], 
               [order_no], 
               [sli_no], 
               [li_seq_no], 
               [title_name], 
               [perf_no], 
               [perf_date], 
               [perf_time], 
               [zone_no], 
               [production_name], 
               [production_name_short], 
               [production_name_long], 
               [comp_code], 
               [comp_code_name], 
               [order_payment_status], 
               [price_type], 
               [price_type_name], 
               [due_amt], 
               [paid_amt], 
               [sale_total], 
               [performance_date], 
               [performance_time], 
               [scan_admission_date], 
               [scan_admission_time], 
               [scan_device], 
               [scan_admission_adult], 
               [scan_admission_child], 
               [scan_admission_other], 
               [scan_admission_auto], 
               [scan_admission_total], 
               [sli_status], 
               [customer_no], 
               [report_start_date],
               [title_no],
               [production_no],
               [fiscal_year]
        FROM [#att_temp_table]

            /*  COVID 19 UPDATE (part 2)
                The update to account for Membership Scans has to be done *After* the data has been
                moved over to the History table because the update keys on the history_no field,
                which is an identity field that does not exist in the temp table.  */

                            IF @history_dt >= '7-1-2020' BEGIN

                                --MEMBERSHIP CARD SCANS         
                                WITH [CTE_MEMBER_FREE_EXHIBIT_HALL_TIX] ([history_no], [title_no], [customer_no], [paid_amt], [membership_level])
                                AS (SELECT [history_no],
                                           [title_no], 
                                           [customer_no], 
                                           [paid_amt],
                                           [dbo].[LFS_ActiveMemberLevelOnDate]([customer_no],0,[performance_date])
                                    FROM [dbo].[LT_HISTORY_TICKET] 
                                    WHERE [performance_date] = CONVERT(CHAR(10),@history_dt,111)
                                      AND [title_no] = 27
                                      AND [paid_amt] = 0.0
                                      AND [comp_code_name] = ''
                                      AND [price_type_name] NOT LIKE '%Under 3%'
                                      AND [price_type_name] NOT LIKE '%Library%'
                                      AND dbo.[LFS_ActiveMemberOnDate]([customer_no],0,[performance_date],'N') = 'Y')
                                UPDATE his
                                SET his.[attendance_type] = 'Gate B (Non-Ticketed)',
                                    his.[title_name] = 'Membership Card Scan',
                                    his.[production_name] = cte.[membership_level],
                                    his.[production_name_short] = cte.[membership_level],
                                    his.[production_name_long] = cte.[membership_level],
                                    his.[comp_code_name] = 'No Discount',
                                    his.[scan_admission_date] = ISNULL(his.[scan_admission_date],his.[performance_date]),
                                    his.[scan_admission_time] = ISNULL(his.[scan_admission_time],his.[performance_time]),
                                    his.[scan_admission_other] = his.[sale_total],
                                    his.[scan_admission_total] = his.[sale_total],
                                    his.[title_no] = 0,
                                    his.[production_no] = 0
                                FROM [dbo].[LT_HISTORY_TICKET] AS his
                                     INNER JOIN [CTE_MEMBER_FREE_EXHIBIT_HALL_TIX] AS cte ON cte.[history_no] = his.[history_no];
    
                            END

        CLEAN_UP:

            IF OBJECT_ID('tempdb..#att_temp_table') IS NOT NULL DROP TABLE [#att_temp_table]
            IF OBJECT_ID('tempdb..#ord_temp_table') IS NOT NULL DROP TABLE [#ord_temp_table]
            IF OBJECT_ID('tempdb..#sli_temp_table') IS NOT NULL DROP TABLE [#sli_temp_table]
     
     DONE:
     
END
GO

GRANT EXECUTE ON [dbo].[LP_UPDATE_HISTORY_TICKET] TO [ImpUsers], [tessitura_app]
GO

--EXECUTE [dbo].[LP_UPDATE_HISTORY_TICKET] @history_dt = '4-17-2021', @create_dt = NULL, @include_partial_paid = 'Y'

