USE [impresario]
GO
/****** Object:  StoredProcedure [dbo].[LRP_MEMB_CUSTOM_SCREEN_DETAILS]    Script Date: 9/20/2017 10:16:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Alex Harris for Tessitura Network
-- Create date: June 3, 2016
-- Description:	Membership Custom Screen Details
-- =============================================

/* Sample Execute:

Execute LRP_MEMB_CUSTOM_SCREEN_DETAILS @customer_no = 3211625

*/

ALTER PROCEDURE [dbo].[LRP_MEMB_CUSTOM_SCREEN_DETAILS]

	@customer_no INT

AS
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @one_step_constituency INT
	DECLARE @delete_one_step INT
	DECLARE @memb_details TABLE (
				id_key INT NULL,
				customer_no INT NULL,
				fyear INT NOT NULL,
				fyear_desc VARCHAR(4) NOT NULL,
				init_dt DATETIME NULL,
				expr_dt DATETIME NULL,
				cust_memb_no INT NOT NULL,
				memb_description VARCHAR(30) NOT NULL,
				pin VARCHAR(4) NULL,
				pin_printed_dt DATETIME NULL,
				pin_active CHAR(1) NULL,
				expired_pin_reason VARCHAR(30) NULL,
				one_step_status VARCHAR(30) NULL,
				one_step_change DATETIME NULL,
				giver_name VARCHAR(100) NULL,
				giver_customer_no INT NULL,
				gift_memb_sent_to VARCHAR(30) NULL,
				af_prev_printed char(1) NULL,
				af_not_active char(1) NULL)
	
	SET		@one_step_constituency = 30
	SET		@delete_one_step = 15

	INSERT INTO @memb_details (id_key, customer_no, fyear, fyear_desc, init_dt, expr_dt, cust_memb_no,
					memb_description, pin, pin_printed_dt, pin_active, expired_pin_reason,
					giver_name, giver_customer_no, gift_memb_sent_to, af_prev_printed, af_not_active)
	SELECT	id_key = row_number() over (order by cm.cust_memb_no),
			customer_no = cm.customer_no,
			fyear = bp.fyear,
			fyear_desc = 'FY' +SUBSTRING(CONVERT(varchar,fyear),3,2),
			cm.init_dt,
			cm.expr_dt,
			cust_memb_no = cm.cust_memb_no,
			memb_description = ml.description,
			pin = mc.pin,
			pin_printed_dt = mc.printed_dt,
			pin_active = CASE WHEN mc.inactive = 'N' THEN 'Y' ELSE 'N' END,
			expired_pin_reason = NULL,
			giver_name = cs.esal1_desc,
			giver_customer_no = CASE WHEN ISNULL(cm.ben_provider,0) > 0 THEN cm.ben_provider ELSE NULL END,
			gift_memb_sent_to = CASE
									WHEN sr.category = 3 THEN 'Giver'
									WHEN sr.category = 2 THEN 'Recipient'
									ELSE NULL
								END,
			af_prev_printed = case when exists(Select 1 From dbo.LT_MOS_ACK_MEMBERSHIPS_AS_PRODUCTS As amp 
								Join TR_Batch_Period mbp on amp.expr_dt BETWEEN mbp.start_dt and mbp.end_dt 
								Where cm.customer_no = amp.customer_no 
								and bp.fyear = mbp.fyear--12/1/2016 BG(TSR)
								and cm.memb_org_no in(5,24))
				then 'Y' else 'N' end,
			af_not_active = case when cm.memb_org_no in(5,24) and cm.current_status not in(2,3) --Active, Pending
				then 'Y' else 'N' end 
	FROM	dbo.TX_CUST_MEMBERSHIP AS cm
	JOIN	dbo.TR_BATCH_PERIOD AS bp ON cm.expr_dt BETWEEN bp.start_dt AND bp.end_dt
	JOIN	dbo.T_MEMB_LEVEL AS ml ON cm.memb_level = ml.memb_level AND cm.memb_org_no = ml.memb_org_no
	LEFT JOIN	dbo.LT_MOS_MEMBER_CARDS AS mc ON cm.cust_memb_no = mc.cust_memb_no
	LEFT JOIN dbo.TX_CUST_SAL AS cs ON cm.ben_provider = cs.customer_no AND cs.default_ind = 'Y'
	LEFT JOIN dbo.LX_SLI_MEMB AS sm ON cm.cust_memb_no = sm.cust_memb_no
	LEFT JOIN dbo.T_SUB_LINEITEM AS sli ON sm.sli_no = sli.sli_no
	LEFT JOIN dbo.T_SPECIAL_REQ AS sr ON sli.li_seq_no = sr.li_seq_no
	WHERE	cm.customer_no = @customer_no

	
	UPDATE	md
	SET		md.one_step_status = 'Enrolled',
			md.one_step_change = cc.start_dt
	FROM	@memb_details AS md
	JOIN	dbo.TX_CONST_CUST AS cc
			ON md.customer_no = cc.customer_no
			AND md.init_dt <= Isnull(cc.end_dt,'2999-12-31') AND (md.expr_dt >= cc.start_dt)
			AND cc.constituency = @one_step_constituency

	
	UPDATE	md
	SET		md.one_step_status = 'Pending Renewal',
			md.one_step_change = CONVERT(DATETIME,CONVERT(DATE,DATEADD(dd,1,DATEADD(mm,-1,md.expr_dt))))
	FROM	@memb_details AS md
	JOIN	dbo.TX_CONST_CUST AS cc
			ON md.customer_no = cc.customer_no
			AND md.init_dt <= Isnull(cc.end_dt,'2999-12-31') AND (md.expr_dt >= cc.start_dt)
			AND cc.constituency = @one_step_constituency
			AND GETDATE() < md.expr_dt
			AND DATEDIFF(DAY,GETDATE(),md.expr_dt) < 30

	
	UPDATE	md
	SET		md.one_step_status = 'Removed',
			md.one_step_change = ca.create_dt
	FROM	@memb_details AS md
	JOIN	dbo.T_CUST_ACTIVITY AS ca
			ON md.customer_no = ca.customer_no
			AND ca.create_dt BETWEEN md.init_dt AND md.expr_dt
			AND ca.activity_type = @delete_one_step
		

	SELECT	id_key,
			customer_no,
			fyear,
			fyear_desc,
			init_dt,
			expr_dt,
			cust_memb_no,
			memb_description,
			pin,
			pin_printed_dt,
			pin_active,
			expired_pin_reason,
			one_step_status,
			one_step_change,
			giver_name,
			giver_customer_no,
			gift_memb_sent_to,
			af_prev_printed,
			af_not_active
	FROM	@memb_details
	ORDER BY expr_dt DESC, pin_printed_dt DESC

END
