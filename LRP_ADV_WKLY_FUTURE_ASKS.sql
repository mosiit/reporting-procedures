USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[LRP_ADV_WKLY_FUTURE_ASKS]
    	@numOfDays INT = 15,
        @workers_str VARCHAR(4000) = NULL
AS BEGIN

    SET NOCOUNT ON;
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

    /*  Procedure Variables  */

        DECLARE @start_dt DATETIME = NULL, @end_dt DATETIME = NULL

        DECLARE @worker_list TABLE ([worker_no] INT NOT NULL DEFAULT (0))
    
    /*  Check Parameters  */

        SELECT @numOfDays = ISNULL(@numOfDays, 7)

        SELECT @end_dt = DATEADD(dd, @numOfDays, GETDATE()) + ' 23:59:59.99'
        SELECT @start_dt = CAST(GETDATE() AS DATE);

        IF ISNULL(@workers_str,'') = ''
            INSERT INTO @worker_list ([worker_no])
            SELECT ISNULL([worker_customer_no], 0)
            FROM [dbo].[LV_CAMPAIGN_ACTIVITY_WORKERS] 
            WHERE [inactive] = 'N'
              AND ISNULL([worker_customer_no], 0) > 0
        ELSE
            INSERT INTO @worker_list ([worker_no])
            SELECT ISNULL(TRY_CAST([Element] AS INTEGER),0) 
            FROM [dbo].[FT_SPLIT_LIST](REPLACE(@workers_str,'"',''),',')
            WHERE ISNULL(TRY_CAST([Element] AS INTEGER),0) > 0;

    /*  Get Data  */

        -- CTE code taken from VS_PLAN_WITH_PRIMARY_WORKER
        WITH [CTE_PRIMARY_WORKER] ([plan_no], [primaryWorker], [primaryWorkerNo], [primaryWorkerTiny])
        AS (SELECT cp.[plan_no], 
		           dn.[display_name], 
		           dn.[customer_no], 
		           dn.[display_name_tiny]
	        FROM [dbo].[TX_CUST_PLAN] AS cp
                 INNER JOIN @worker_list AS lis ON lis.[worker_no] = cp.[customer_no]
	             INNER JOIN [dbo].[FT_CONSTITUENT_DISPLAY_NAME]() dn ON cp.[customer_no] = dn.[customer_no]
	        WHERE cp.[primary_ind] = 'Y')
        SELECT p.[customer_no], 
               cust.[display_name], 
               cust.[sort_name], 
               cam.[description] AS [campaign], 
               cdes.[description] AS [designation], 
               sta.[description] AS [planStatus],
               p.[ask_amt], 
               p.[start_dt] AS [ask_dt], 
               p.[complete_by_dt] AS [close_dt], 
               wkr.[primaryWorkerNo],
               wkr.[primaryWorker]
        FROM [dbo].[T_PLAN] AS p 
             INNER JOIN [dbo].[TR_PLAN_STATUS] AS sta ON sta.[id] = p.[status]
             INNER JOIN [dbo].[T_CAMPAIGN] AS cam ON cam.[campaign_no] = p.[campaign_no]
             INNER JOIN [dbo].[TR_CONT_DESIGNATION] AS cdes ON cdes.[id] = p.[cont_designation]
             INNER JOIN [dbo].[FT_CONSTITUENT_DISPLAY_NAME]() AS cust ON p.[customer_no] = cust.[customer_no]
             INNER JOIN [CTE_PRIMARY_WORKER] AS wkr ON wkr.[plan_no] = p.[plan_no]
        WHERE p.[type] = 7 -- Advancement Pipeline
          AND p.[start_dt] BETWEEN @start_dt AND @end_dt
        ORDER BY cust.[customer_no]

    DONE:

END
GO

EXECUTE [dbo].[LRP_ADV_WKLY_FUTURE_ASKS] @numOfDays = 15, @workers_str = ''
--EXECUTE [dbo].[LRP_ADV_WKLY_FUTURE_ASKS] @numOfDays = 15, @workers_str = '3881248, 3263169'




