USE [impresario]
GO
/****** Object:  StoredProcedure [dbo].[LRP_CONTRACT_CHECK_REQUEST]    Script Date: 10/1/2021 2:55:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[LRP_CONTRACT_CHECK_REQUEST]
        @contract_id INT = NULL,
        @report_month INT = NULL,
        @report_year VARCHAR(4) = NULL,
        @specific_amt MONEY = NULL,
        @record_payment CHAR(1) = 'N'
AS BEGIN

    SET NOCOUNT ON;
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

    /*  Procedure Variables  */

        --Constant Values
        DECLARE @req_name VARCHAR(100) = 'Omni Theater',
                @req_dept VARCHAR(100) = 'Immersive Theaters and Programs',
                @req_extension VARCHAR(10) = '0285',
                @req_email VARCHAR(100) = 'tsepersky@mos.org',
                @req_department VARCHAR(3) = '395',
                @req_object VARCHAR(4) = '5833',
                @req_source VARCHAR(4) = '0000',
                @req_fund CHAR(1) ='0';

        --Variable Values
        DECLARE @req_project VARCHAR(10) = '', @req_message VARCHAR(100) = '';
        DECLARE @contract_name VARCHAR(30) = '', @contract_production_name VARCHAR(30) = '', @contract_vendor VARCHAR(100);
        DECLARE @report_month_sort VARCHAR(10) = '', @report_month_start DATETIME = NULL;
        DECLARE @report_user_id VARCHAR(25) = '', @report_user_name VARCHAR(100) = ''

        

    /*  Check Parameters  */
    
        SELECT @report_month = ISNULL(@report_month, 0)
        SELECT @report_year = ISNULL(@report_year, '')

        SELECT @report_month_sort = @report_year + '/' + CASE WHEN @report_month < 10 THEN '0' ELSE '' END + CAST(@report_month AS VARCHAR(2))
        SELECT @report_month_start = @report_month_sort + '/01'

        SELECT @contract_id = ISNULL(@contract_id, 0)
        SELECT @specific_amt = ISNULL(@specific_amt, 0.0)
        SELECT @record_payment = ISNULL(@record_payment, 'N')

        IF @record_payment <> 'Y' SELECT @record_payment = 'N'

    /*  Get User Info */

        SELECT @report_user_id = [dbo].[FS_USERNAME]()
       
        SELECT @report_user_name = ISNULL([full_name],'') FROM [dbo].[LV_MuseumUserInfo] WHERE [userid] = @report_user_id
       

    /*  Get contract info  */

        SELECT @contract_name = mth.[contract_name],
               @contract_production_name = mth.[prod_name],
               @contract_vendor = ISNULL(ven.[vendor_name],''),
               @req_message = LEFT(DATENAME(MONTH, @report_month_start), 3) + ' ' + @report_year + ' ' + mth.[prod_name]
        FROM [dbo].[LT_HISTORY_OMNI_CONTRACT_MONTHLY] AS mth
             LEFT OUTER JOIN [dbo].[LTR_CONTRACT_TERMS] AS trm ON trm.[id] = mth.[contract_id]
             LEFT OUTER JOIN [dbo].[LTR_CONTRACT_VENDORS] AS ven ON ven.[vendor_customer_no] = trm.[vendor]
        WHERE [contract_id] = @contract_id
          AND [Perf_month_sort] = @report_month_sort

    /*  Set Project Code and Payment Amount  */

         SELECT @req_project = ISNULL([project_code],'')
                               FROM [dbo].[LTR_CONTRACT_TERMS]
                               WHERE [id] = @contract_id

        IF @specific_amt = 0
            SELECT @specific_amt =  SUM(ISNULL([total_owed], 0))
                                    FROM [dbo].[LT_HISTORY_OMNI_CONTRACT_MONTHLY]
                                    WHERE [contract_id] = @contract_id
                                      AND [Perf_month_sort] = @report_month_sort

    /*  Add Payment Record (if requested)  */

        IF @record_payment = 'Y' BEGIN

            IF NOT EXISTS (SELECT * FROM [dbo].[LTR_CONTRACT_PAYMENTS] WHERE [payment_notes] = @req_message AND [payment_amount] = @specific_amt)
                INSERT INTO [dbo].[LTR_CONTRACT_PAYMENTS] ([id], [contract_id], [check_no], [payment_date], [payment_amount], [payment_notes], [inactive])
                    SELECT MAX([id]) + 1,
                           @contract_id,
                           '',
                           GETDATE(),
                           @specific_amt,
                           @req_message,
                           'N'
                    FROM [dbo].[LTR_CONTRACT_PAYMENTS]

        END

    FINISHED:
    
        /*  Final Data Set (single row) */

         SELECT @contract_id AS [contract_id],
                @contract_name AS [contract_name],
                @contract_production_name AS [contract_prod_name],
                @contract_vendor AS [contract_vendor_name],
                @req_name AS [request_name],
                @req_dept AS [request_department],
                @req_extension AS [request_extension],
                @req_email AS [request_email],
                @req_department AS [department_code],
                @req_object AS [object_code],
                @req_project AS [project_code],
                @req_source AS [source_code],
                @req_fund AS [fund_code],
                LEFT(@req_message, 30) AS [request_message],
                @specific_amt AS [total_owed],
                @report_user_id AS [report_user_id],
                @report_user_name AS [report_user_name],
                GETDATE() AS [report_run_dt]
        
END
GO

EXECUTE [dbo].[LRP_CONTRACT_CHECK_REQUEST] @contract_id = 6, @report_month = 9, @report_year = '2021', @specific_amt = 100, @record_payment = 'N'