USE impresario;
GO

SET ANSI_NULLS ON;
SET QUOTED_IDENTIFIER ON;
GO

DROP PROCEDURE [dbo].[LRP_MEMBER_EXPIRE_DATE_UPDATE_UTILITY];
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_MEMBER_EXPIRE_DATE_UPDATE_UTILITY]') AND type in (N'P', N'PC')) BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_MEMBER_EXPIRE_DATE_UPDATE_UTILITY] AS';
    EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LRP_MEMBER_EXPIRE_DATE_UPDATE_UTILITY] TO [impusers], [tessitura_app]';
END;
GO

ALTER PROCEDURE [dbo].[LRP_MEMBER_EXPIRE_DATE_UPDATE_UTILITY]
        @memb_org INT = 0,
        @current_expr_dt DATETIME = NULL,
        @include_already_extended CHAR(1) = 'Y',
        @months_to_add INT = 1,
        @list_no INT = 0,
        @preview_only CHAR(1) ='Y',
        @customers_to_edit INT = 0
WITH RECOMPILE AS BEGIN

    /*  Procedure Variables  */

        DECLARE @log_updates CHAR(1) = 'Y'  --Can be set to N for testing purposes

        DECLARE @run_dt DATETIME = GETDATE()
        
        DECLARE @current_init_dt DATETIME = NULL;

        DECLARE @list_name VARCHAR(50) = ''

        DECLARE @pk_name VARCHAR(30) = 'uq_changememberships_' + FORMAT(GETDATE(),'yyyyMMddHHmmss');

        DECLARE @temp_records INT = 0

        DECLARE @error_msg VARCHAR(255) = ''

        DECLARE @temp_memb_list TABLE (cust_memb_no INT NOT NULL DEFAULT (0));  --FOR TESTING

    /*  Temp Tables  */

        IF OBJECT_ID('tempdb..#customer_list') IS NOT NULL DROP TABLE [#customer_list];

        CREATE TABLE [#customer_list] ([customer_no] INT NOT NULL DEFAULT (0))


        IF OBJECT_ID('tempdb..#change_memberships') IS NOT NULL DROP TABLE [#change_memberships];

        CREATE TABLE [#change_memberships] ([run_dt] DATETIME NOT NULL DEFAULT (GETDATE()),
                                            [cust_memb_no] INT NOT NULL DEFAULT (0),
                                            [customer_no] INT NOT NULL DEFAULT (0),
                                            [member_name] VARCHAR(150) NOT NULL DEFAULT (''),
                                            [sort_name] VARCHAR(150) NOT NULL DEFAULT (''),
                                            [memb_org_no] INT NOT NULL DEFAULT (0),
                                            [memb_org_name] VARCHAR(30) NOT NULL DEFAULT (''),
                                            [memb_level] VARCHAR(30) NOT NULL DEFAULT (''),
                                            [memb_level_name] VARCHAR(30) NOT NULL DEFAULT (''),
                                            [added_months] INT NOT NULL DEFAULT (1),
                                            [orig_init_dt] DATETIME NULL,
                                            [init_dt] DATETIME NULL,
                                            [new_init_dt] DATETIME NULL,
                                            [orig_expr_dt] DATETIME NULL,
                                            [expr_dt] DATETIME NULL,
                                            [new_expr_dt] DATETIME NULL,
                                            [memb_months] INT NOT NULL DEFAULT (0),
                                            [current_status] INT NOT NULL DEFAULT (0),
                                            [status_name] VARCHAR(30) NOT NULL DEFAULT (''),
                                            [membership_notes] VARCHAR(1024) NOT NULL DEFAULT (''),
                                            [message_str] VARCHAR(255) NOT NULL DEFAULT (''));

        --Create index dynamically to avoid multiple contstraints with the same name 
        --If procedure is run twice at the same time, multiple instances of this table can exist in tempdb at the same time.
        --An Index must have a unique name within the database
        EXECUTE ('CREATE UNIQUE CLUSTERED INDEX [' + @pk_name + '] ON [#change_memberships] ([cust_memb_no] ASC) '
               + 'WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF,'
               + 'DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)');

    /*  Check Parameters  */

        --Current expiration date is reqired.  If no value then something is wrong
        IF @current_expr_dt IS NULL GOTO FINISHED;

        --Membership Orgs is a required parameter.  It should always have a value.  if not, something is wrong
        IF ISNULL(@memb_org,0) = 0 GOTO FINISHED;

        --Make expiration date the last day of the month and the end of the day
        SELECT @current_expr_dt = EOMONTH(@current_expr_dt);
        SELECT @current_expr_dt = @current_expr_dt + ' 23:59:59';

        --Current init date is one day after the expiration date
        SELECT @current_init_dt = CAST(DATEADD(DAY,1,@current_expr_dt) AS DATE);

        SELECT @months_to_add = ISNULL(@months_to_add,1);
        
        IF @months_to_add > 24 BEGIN;
            SELECT @error_msg = CAST(@months_to_add AS VARCHAR(10)) + ' is too many months to add (Limit = 24)';
            THROW 51000, @error_msg, 1;
        END;

        --List Number is optional
        SELECT @list_no = ISNULL(@list_no,0);

        --If there is a list, get the name of it.
        IF @list_no = 0 SELECT @list_name = '(none)'
        ELSE SELECT @list_name = ISNULL([list_desc],'(unknown)') FROM [dbo].[T_LIST] WHERE [list_no] = @list_no;

        --If there is a list number put those values into the #customer_list temp table
        --If not, put all customer numbers with the designated expiration date into the table
        IF @list_no > 0
            INSERT INTO [#customer_list] ([customer_no])
            SELECT DISTINCT([customer_no])
            FROM [dbo].[T_LIST_CONTENTS] 
            WHERE [list_no] = @list_no;
        ELSE
            INSERT INTO [#customer_list] ([customer_no])
            SELECT DISTINCT (mem.[customer_no])
            FROM [dbo].[TX_CUST_MEMBERSHIP] AS mem
            WHERE CAST(mem.[expr_dt] AS DATE) = CAST(@current_expr_dt AS date)
              AND mem.[memb_org_no] = @memb_org
              AND mem.[current_status] = 2;
      
      

      SELECT @customers_to_edit = ISNULL(@customers_to_edit,0)

      IF ISNULL(@preview_only,'Y') <> 'N' SELECT @preview_only = 'Y'

    /*  Get all the Current (Active) Memberships to be changed  */

        INSERT INTO [#change_memberships] ([run_dt],[cust_memb_no],[customer_no],[member_name],[sort_name],[memb_org_no],[memb_org_name],[memb_level], 
                                           [memb_level_name],[added_months],[orig_init_dt],[init_dt],[new_init_dt],[orig_expr_dt],[expr_dt],
                                           [new_expr_dt],[memb_months],[current_status],[status_name],[membership_notes],[message_str])
        SELECT @run_dt,
               mem.[cust_memb_no],
               mem.[customer_no],
               nam.[display_name],
               LOWER(nam.[sort_name]),
               mem.[memb_org_no],
               org.[description],
               mem.[memb_level],
               lev.[description],
               @months_to_add,
               snp.[snap_init_dt],
               mem.[init_dt],
               NULL,
               snp.[snap_expr_dt],
               mem.[expr_dt],
               EOMONTH(DATEADD(MONTH,@months_to_add,mem.[expr_dt])),
               0,
               mem.[current_status],
               sta.[description],
               ISNULL(mem.[notes],''),
               ''
        FROM [dbo].[TX_CUST_MEMBERSHIP] AS mem
             INNER JOIN [#customer_list] AS cus ON cus.[customer_no] = mem.[customer_no]
             LEFT OUTER JOIN [dbo].[FT_CONSTITUENT_DISPLAY_NAME]() AS nam ON nam.[customer_no] = mem.[customer_no]
             LEFT OUTER JOIN [dbo].[T_MEMB_LEVEL] AS lev ON lev.[memb_level] = mem.[memb_level]
             LEFT OUTER JOIN [dbo].[TR_CURRENT_STATUS] AS sta ON sta.[id] = mem.[current_status]
             LEFT OUTER JOIN [dbo].[T_MEMB_ORG] AS org ON org.[memb_org_no] = mem.[memb_org_no]
             LEFT OUTER JOIN [dbo].[LTR_CLOSURE_MEMBERSHIP_SNAPSHOT] AS snp ON snp.[cust_memb_no] = mem.[cust_memb_no]
        WHERE CAST(mem.[expr_dt] AS DATE) = CAST(@current_expr_dt AS DATE)
          AND mem.[memb_org_no] = @memb_org
          AND mem.[current_status] = 2;

          

    /*  Include records that have already been extended  */

        IF @include_already_extended = 'N'
            DELETE FROM [#change_memberships] WHERE [orig_expr_dt] < [expr_dt]
        ELSE IF @include_already_extended = 'O'
            DELETE FROM [#change_memberships] WHERE [orig_expr_dt] >= [expr_dt]

    /*  Bring the customer list up to date  */
       
        DELETE FROM [#customer_list] 
        WHERE [customer_no] NOT IN (SELECT [customer_no] 
                                    FROM [#change_memberships]);

    /* Get All Pending Memberships to Change  */

        INSERT INTO [#change_memberships] ([run_dt],[cust_memb_no],[customer_no],[member_name],[sort_name],[memb_org_no],[memb_org_name],[memb_level], 
                                           [memb_level_name],[added_months],[orig_init_dt],[init_dt],[new_init_dt],[orig_expr_dt],[expr_dt],
                                           [new_expr_dt],[memb_months],[current_status],[status_name],[membership_notes],[message_str])
        SELECT @run_dt,
               mem.[cust_memb_no],
               mem.[customer_no],
               nam.[display_name],
               LOWER(nam.[sort_name]),
               mem.[memb_org_no],
               org.[description],
               mem.[memb_level],
               lev.[description],
               @months_to_add,
               snp.[snap_init_dt],
               mem.[init_dt],
               NULL,
               snp.[snap_expr_dt],
               mem.[expr_dt],
               EOMONTH(DATEADD(MONTH,@months_to_add,mem.[expr_dt])),
               ISNULL(snp.[memb_months],0),
               mem.[current_status],
               sta.[description],
               ISNULL(mem.[notes],''),
               ''
        FROM [dbo].[TX_CUST_MEMBERSHIP] AS mem
             INNER JOIN [#customer_list] AS cus ON cus.[customer_no] = mem.[customer_no]
             LEFT OUTER JOIN [dbo].[FT_CONSTITUENT_DISPLAY_NAME]() AS nam ON nam.[customer_no] = mem.[customer_no]
             LEFT OUTER JOIN [dbo].[T_MEMB_LEVEL] AS lev ON lev.[memb_level] = mem.[memb_level]
             LEFT OUTER JOIN [dbo].[TR_CURRENT_STATUS] AS sta ON sta.[id] = mem.[current_status]
             LEFT OUTER JOIN [dbo].[T_MEMB_ORG] AS org ON org.[memb_org_no] = mem.[memb_org_no]
             LEFT OUTER JOIN [dbo].[LTR_CLOSURE_MEMBERSHIP_SNAPSHOT] AS snp ON snp.[cust_memb_no] = mem.[cust_memb_no]
        WHERE CAST(mem.[init_dt] AS DATE) >= CAST(@current_init_dt AS DATE)
          AND mem.[memb_org_no] = @memb_org
          AND mem.[current_status] = 3;
          
    /*  Pending memberships created after the closure date will not be in the snapshot.  
        Add them now so that their original dates can be tracked  */

        INSERT INTO [dbo].[LTR_CLOSURE_MEMBERSHIP_SNAPSHOT] ([cust_memb_no],[customer_no],[constituent_name],[memb_org_no],[memb_org],[memb_level],[memb_level_name],
                                                             [snap_init_dt],[curr_init_dt],[snap_expr_dt],[curr_expr_dt],[status_no],[status_name],[memb_months])
        SELECT mem.[cust_memb_no],
               mem.[customer_no],
               nam.[display_name],
               mem.[memb_org_no],
               org.[description],
               mem.[memb_level],
               lev.[description],
               mem.[init_dt],
               mem.[init_dt],
               mem.[expr_dt],
               mem.[expr_dt],
               mem.[current_status],
               sta.[description],
               DATEDIFF(MONTH,mem.[init_dt],mem.[expr_dt]) + 1
        FROM [dbo].[TX_CUST_MEMBERSHIP] AS mem
             LEFT OUTER JOIN [dbo].[FT_CONSTITUENT_DISPLAY_NAME]() AS nam ON nam.[customer_no] = mem.[customer_no]
             LEFT OUTER JOIN [dbo].[T_MEMB_LEVEL] AS lev ON lev.[memb_level] = mem.[memb_level]
             LEFT OUTER JOIN [dbo].[TR_CURRENT_STATUS] AS sta ON sta.[id] = mem.[current_status]
             LEFT OUTER JOIN [dbo].[T_MEMB_ORG] AS org ON org.[memb_org_no] = mem.[memb_org_no]
             LEFT OUTER JOIN [dbo].[LTR_CLOSURE_MEMBERSHIP_SNAPSHOT] AS snp ON snp.[cust_memb_no] = mem.[cust_memb_no]
        WHERE mem.[cust_memb_no] IN (SELECT [cust_memb_no] 
                                     FROM [#change_memberships] 
                                     WHERE [orig_init_dt] IS NULL)    
          AND snp.[cust_memb_no] IS NULL

    /*  If they weren't in the snapshot, there isn't an original init date 
        Set it to the current init date in the temp table  */
        UPDATE [#change_memberships] 
        SET [orig_init_dt] = [init_dt]
        WHERE [orig_init_dt] IS NULL

    /*  Make sure all expiration dates are set to the end of the day  */

        UPDATE [#change_memberships]
        SET [expr_dt] = CONVERT(CHAR(10),[expr_dt],111) + ' 23:59:59',
            [new_expr_dt] = CONVERT(CHAR(10),[new_expr_dt],111) + ' 23:59:59';

    /*  Pending Membership Init Date should be one day after active membership's expiration date  */
    
        UPDATE pen
        SET pen.[new_init_dt] = DATEADD(DAY,1,act.[new_expr_dt])
        FROM  [#change_memberships] AS pen
              LEFT OUTER JOIN [#change_memberships] AS act ON act.[customer_no] = pen.[customer_no] AND act.[memb_org_no] = pen.[memb_org_no] AND act.[status_name] = 'Active'
        WHERE pen.[status_name] = 'Pending'
          AND pen.[new_init_dt] IS NULL;
          
    /*  Update Messages - To Display On the Report  */
        
        UPDATE [#change_memberships]
        SET [message_str] = CASE WHEN [expr_dt] IS NULL OR [new_expr_dt] IS NULL THEN 'Cannot change Expire Date (null value)'
                                 ELSE 'Expire' + CASE WHEN @preview_only = 'Y' THEN ' Will Be Changed From ' ELSE ' ' END 
                                               + FORMAT(ISNULL([orig_expr_dt],[expr_dt]),'MM/dd/yyyy') + ' to ' + FORMAT([new_expr_dt],'MM/dd/yyyy') END
        WHERE [current_status] = 2
          AND [expr_dt] IS NOT NULL
          AND [new_expr_dt] IS NOT NULL;

        UPDATE [#change_memberships]
        SET [message_str] = CASE WHEN [init_dt] IS NULL OR [new_init_dt] IS NULL THEN 'Cannot Change Init Date (null value)'
                                 ELSE 'Init' + CASE WHEN @preview_only = 'Y' THEN ' Will Be Changed From ' ELSE ' ' END  
                                             + FORMAT(ISNULL([orig_init_dt],[init_dt]),'MM/dd/yyyy') + ' to ' + FORMAT([new_init_dt],'MM/dd/yyyy') END  + ' / '
                          + CASE WHEN [expr_dt] IS NULL OR [new_expr_dt] IS NULL THEN 'Cannot change Expire Date (null value)'
                                 ELSE 'Expire' + CASE WHEN @preview_only = 'Y' THEN ' Will Be Changed From ' ELSE ' ' END  
                                               + FORMAT(ISNULL([orig_expr_dt],[expr_dt]),'MM/dd/yyyy') + ' to ' + FORMAT([new_expr_dt],'MM/dd/yyyy') END
        WHERE [current_status] = 3;

    /* Set new note text - To be copied to the Membership Record  */

        UPDATE [#change_memberships]
        SET [membership_notes] = FORMAT(GETDATE(),'MM/dd/yyyy') + ': ' 
                               + [message_str]
                               + CASE WHEN [membership_notes] <> '' THEN ' / ' + [membership_notes] 
                                      ELSE '' END;

    /*  Make the updates if not Preview Only - Done within a transaction  */

        IF @preview_only = 'N' BEGIN

            BEGIN TRY

                BEGIN TRANSACTION

                                --If procedure is in Test Mode only allow four records to be updated at a time.
                                IF @customers_to_edit > 0 BEGIN
                                    INSERT INTO @temp_memb_list ([cust_memb_no])
                                    SELECT TOP (@customers_to_edit) [cust_memb_no] 
                                    FROM [#change_memberships] 
                                    WHERE [status_name] = 'Pending' 
                                    ORDER BY [cust_memb_no]
                        
                                    SELECT @temp_records = @@ROWCOUNT

                                    IF @temp_records = 0
                                        INSERT INTO @temp_memb_list ([cust_memb_no])
                                        SELECT TOP (@customers_to_edit) [cust_memb_no] 
                                        FROM [#change_memberships] 
                                        WHERE [status_name] = 'Active' 
                                        ORDER BY [cust_memb_no]
                                    ELSE
                                        INSERT INTO @temp_memb_list ([cust_memb_no])
                                        SELECT [cust_memb_no] 
                                        FROM [#change_memberships] 
                                        WHERE [customer_no] IN (SELECT [customer_no] 
                                                                FROM [#change_memberships] 
                                                                WHERE [cust_memb_no] IN (SELECT [cust_memb_no] 
                                                                                         FROM @temp_memb_list))
                                          AND [status_name] = 'Active'                                        

                                    DELETE FROM [#change_memberships] WHERE [cust_memb_no] NOT IN (SELECT [cust_memb_no] FROM @temp_memb_list)

                                END
                                            
                    --Active Memberships (expire date only)
                    UPDATE mem
                    SET mem.[expr_dt] = ISNULL(chg.[new_expr_dt],mem.[expr_dt]),
                        mem.[notes] = chg.[membership_notes]
                    FROM [dbo].[TX_CUST_MEMBERSHIP] AS mem
                         INNER JOIN [#change_memberships] AS chg ON chg.[cust_memb_no] = mem.[cust_memb_no]
                    WHERE chg.[current_status] = 2;
                                                        
                    UPDATE mem
                    SET mem.[curr_expr_dt] = ISNULL(chg.[new_expr_dt],mem.[curr_expr_dt])
                    FROM [dbo].[LTR_CLOSURE_MEMBERSHIP_SNAPSHOT] AS mem
                         INNER JOIN [#change_memberships] AS chg ON chg.[cust_memb_no] = mem.[cust_memb_no]
                    WHERE chg.[current_status] = 2;

                    --Pending Memberships (init date and expire date)
                    UPDATE mem
                    SET mem.[init_dt] = ISNULL(chg.[new_init_dt],mem.[init_dt]),
                        mem.[expr_dt] = ISNULL(chg.[new_expr_dt],mem.[expr_dt]),
                        mem.[notes] = chg.[membership_notes]
                    FROM [dbo].[TX_CUST_MEMBERSHIP] AS mem
                         INNER JOIN [#change_memberships] AS chg ON chg.[cust_memb_no] = mem.[cust_memb_no]
                    WHERE chg.[current_status] = 3;
                
                    UPDATE mem
                    SET mem.[curr_init_dt] = ISNULL(chg.[new_init_dt],mem.[curr_init_dt]),
                        mem.[curr_expr_dt] = ISNULL(chg.[new_expr_dt],mem.[curr_expr_dt])
                    FROM [dbo].[LTR_CLOSURE_MEMBERSHIP_SNAPSHOT] AS mem
                         INNER JOIN [#change_memberships] AS chg ON chg.[cust_memb_no] = mem.[cust_memb_no]
                    WHERE chg.[current_status] = 3;

                    --Log the updates                    
                    IF @log_updates = 'Y'
                        INSERT INTO [admin].[dbo].[tessitura_member_expire_dt_change_log] ([run_dt],[cust_memb_no],[customer_no],[member_name],[sort_name],[memb_org_no],[memb_level], 
                                                   [memb_level_name],[added_months],[init_dt],[new_init_dt],[expr_dt],[new_expr_dt],[current_status],[status_name],[membership_notes])
                        SELECT [run_dt],
                               [cust_memb_no],
                               [customer_no],
                               [member_name],
                               [sort_name],
                               [memb_org_no],
                               [memb_level],
                               [memb_level_name],
                               [added_months],
                               [init_dt],
                               [new_init_dt],
                               [expr_dt],
                               [new_expr_dt],
                               [current_status],
                               [status_name],
                               [membership_notes]
                        FROM [#change_memberships]
                    
                COMMIT TRANSACTION;

            END TRY
            BEGIN CATCH

                WHILE @@TRANCOUNT > 0
                   ROLLBACK TRANSACTION;

                THROW;

            END CATCH;
           
        END;


    FINISHED:

        SELECT  [cust_memb_no],
                [customer_no],
                [member_name],
                [sort_name],
                [memb_org_no],
                [memb_org_name],
                [memb_level],
                [memb_level_name],
                [added_months],
                [orig_init_dt],
                [init_dt],
                [new_init_dt],
                [orig_expr_dt],
                [expr_dt],
                [new_expr_dt],
                [memb_months],
                [current_status],
                [status_name],
                [message_str],
                [membership_notes],
                @list_name AS [list_name]
        FROM [#change_memberships]
        ORDER BY [sort_name], [customer_no], [current_status];

    DONE:

        IF OBJECT_ID('tempdb..#change_memberships') IS NOT NULL DROP TABLE [#change_memberships];

END;
GO

GRANT EXECUTE ON [dbo].[LRP_MEMBER_EXPIRE_DATE_UPDATE_UTILITY] TO [impusers], [tessitura_app]
GO


EXECUTE [dbo].[LRP_MEMBER_EXPIRE_DATE_UPDATE_UTILITY] 
        @memb_org = 4,
        @current_expr_dt = '5-31-2020',
        @include_already_extended = 'Y', 
        @months_to_add = 1, 
        @list_no = 0, 
        @preview_only = 'Y', 
        @customers_to_edit = 4


       -- SELECT * FROM [dbo].[LV_MuseumUserInfo] WHERE [userid] = 'msherw00'


















  --SELECT * FROM [dbo].[T_MEMB_ORG]
  --SELECT * FROM [admin].[dbo].[tessitura_member_expire_dt_change_log] ORDER BY log_ID
  --SELECT * FROM [dbo].[TR_GOOESOFT_DROPDOWN] WHERE code = 1011 [description] = 'Only'

  /*
  
  --SELECT cust_memb_no FROM [#change_memberships] GROUP BY [cust_memb_no] HAVING COUNT(*) > 1

        SELECT * FROM [#customer_list] WHERE [customer_no] = 841187
        SELECT * FROM [dbo].[TX_CUST_MEMBERSHIP] WHERE [cust_memb_no] = 1222457

        SELECT * FROM [#change_memberships]
        WHERE [cust_memb_no] NOT IN (SELECT cust_memb_no FROM [dbo].[TX_CUST_MEMBERSHIP] WHERE [expr_dt] = @current_expr_dt AND [memb_org_no] = 4 AND [current_status] = 2)

        

        GOTO DONE
        

              --SELECT COUNT(*) FROM [#customer_list]
        --SELECT COUNT(DISTINCT customer_no) FROM [#change_memberships]
        --SELECT COUNT(DISTINCT customer_no) FROM [dbo].[TX_CUST_MEMBERSHIP] WHERE CAST([expr_dt] AS DATE) = CAST(@current_expr_dt AS DATE) AND [memb_org_no] = 4 AND [current_status] = 2
        --SELECT COUNT(DISTINCT customer_no) FROM [#customer_list]
        --SELECT * FROM [dbo].[LTR_CLOSURE_MEMBERSHIP_SNAPSHOT] WHERE customer_no IN (SELECT * FROM [#customer_list] WHERE [customer_no] NOT IN (SELECT [customer_no] FROM [#change_memberships]))

        --SELECT [cust_memb_no] FROM @temp_memb_list
                                        --(SELECT [customer_no] FROM [#change_memberships] WHERE [cust_memb_no] IN (SELECT [cust_memb_no] FROM @temp_memb_list))
                                        --SELECT * FROM [#change_memberships] WHERE [customer_no] IN (SELECT [customer_no] FROM [#change_memberships] WHERE [cust_memb_no] IN (SELECT [cust_memb_no] FROM @temp_memb_list))

                                        --SELECT [cust_memb_no] 
                                        --FROM [#change_memberships] 
                                        --WHERE [customer_no] IN (SELECT [customer_no] 
                                        --                        FROM [#change_memberships] 
                                        --                        WHERE [cust_memb_no] IN (SELECT [cust_memb_no] 
                                        --                                                 FROM @temp_memb_list))
                                        --  AND [status_name] = 'Active'
  
                                      SELECT COUNT(*) FROM [#customer_list]
                                      SELECT COUNT(*) FROM [#change_memberships]

                                       --SELECT * FROM [dbo].[TX_CUST_MEMBERSHIP] WHERE [cust_memb_no] IN (SELECT [cust_memb_no] FROM @temp_memb_list)
                                    --SELECT * FROM [dbo].[LTR_CLOSURE_MEMBERSHIP_SNAPSHOT] WHERE [cust_memb_no] IN (SELECT [cust_memb_no] FROM @temp_memb_list)

  */