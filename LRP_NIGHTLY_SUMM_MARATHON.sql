USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LRP_NIGHTLY_SUMM_MARATHON]    Script Date: 4/6/2016 10:15:14 AM ******/
DROP PROCEDURE [dbo].[LRP_NIGHTLY_SUMM_MARATHON]
GO

/****** Object:  StoredProcedure [dbo].[LRP_NIGHTLY_SUMM_MARATHON]    Script Date: 4/6/2016 10:15:14 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[LRP_NIGHTLY_SUMM_MARATHON]
	@section VARCHAR(300),
	@sortOrder INT,
	@runBy	VARCHAR(300),
	@cystart DATETIME,
	@cyend DATETIME
AS

BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	--SELECT	@cystart = CONVERT(VARCHAR(4), DATEPART(yy, GetDate()) - 7) + '-01-01',
	--		@cyend = CONVERT(VARCHAR(4), DATEPART(yy, GetDate()) - 7) + '-12-31'
	--SELECT 'debug 0', @cystart, @cyend, CONVERT(VARCHAR(4), @cystart)
	--SELECT	@cyear = RIGHT(CONVERT(VARCHAR(4), DATEPART(yy, GetDate())), 2)

	--SET @section = 'Solicitor Total_CY' + @cyear + ' Marathon Totals'
	--SET	@sortOrder = 36

	DECLARE @tblPassOne	TABLE	(
								customer_no	INT,
								value			MONEY
								)

	DECLARE @tblPassTwo TABLE	(
								customer_no		INT,
								value			MONEY
								)

	DECLARE @tblPassBoth TABLE	(
								customer_no		INT,
								value			MONEY
								)

	INSERT INTO @tblPassOne
        SELECT  c.worker_customer_no,
                ISNULL(SUM(c.cont_amt), 0)
        FROM    T_CONTRIBUTION AS c
        INNER JOIN T_CAMPAIGN AS g ON c.campaign_no = g.campaign_no
        WHERE   c.cont_type IN ('G', 'P')
                AND g.category IN ('36', '39')
                AND c.cont_amt > 0
                AND c.worker_customer_no IS NOT NULL
                AND c.worker_customer_no <> '2653100'
                AND CAST(c.cont_dt AS DATE) >= @cystart
                AND CAST(c.cont_dt AS DATE) <= @cyend
        GROUP BY c.worker_customer_no

	--SELECT 'debug 1', * FROM @tblNightSumm

	INSERT INTO @tblPassTwo
        SELECT  cp.customer_no,
                ISNULL(SUM(c.cont_amt), 0)
        FROM    T_CONTRIBUTION AS c
        INNER JOIN T_CAMPAIGN AS g ON c.campaign_no = g.campaign_no
        INNER JOIN T_PLAN AS p ON c.plan_no = p.plan_no
        INNER JOIN TX_CUST_PLAN AS cp ON p.plan_no = cp.plan_no
        WHERE   c.cont_type IN ('G', 'P')
                AND g.category IN ('36', '39')
                AND c.cont_amt > 0
                AND c.worker_customer_no = '2653100'
                AND CAST(c.cont_dt AS DATE) >= @cystart
                AND CAST(c.cont_dt AS DATE) <= @cyend
                AND cp.role_no = '1'
        GROUP BY cp.customer_no

	INSERT INTO @tblPassBoth
		SELECT * FROM @tblPassOne

	INSERT INTO @tblPassBoth
		SELECT * FROM @tblPassTwo

	--SELECT * FROM @tblPassBoth

	INSERT INTO LT_NIGHTLY_SUMMARY
		SELECT  customer_no,
				@section,
				SUM(ISNULL(value, 0)),
				'',
				@sortOrder,
				GETDATE(),
				@runBy
		FROM	@tblPassBoth
		GROUP BY customer_no

END

GO


