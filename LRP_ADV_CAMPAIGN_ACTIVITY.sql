USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_ADV_CAMPAIGN_ACTIVITY]') AND type IN (N'P', N'PC'))
BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_ADV_CAMPAIGN_ACTIVITY] AS' 
    EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LRP_ADV_CAMPAIGN_ACTIVITY] TO impusers'
END
GO

/*      LRP_ADV_CAMPAIGN_ACTIVITY
        Retrieves basic information for the current fiscal year and for the entire run of the campaign.
        Data is aggregated and returned as totals.  No parameters needed.

        NOTE: Uses LV_CAMPAIGN_ACTIVITY_CONTRIBUTIONS view which is a view that was created using the
              same logic in the MOS Advancement Contributions report (LRP_MOS_ADVANCEMENT_CONTRIBUTION_DETAIL).

*/
ALTER PROCEDURE [dbo].[LRP_ADV_CAMPAIGN_ACTIVITY]
       @fiscal_year INT = NULL
WITH RECOMPILE AS BEGIN

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

    /*  Check Fiscal Year Parameter - If none passed, set to current  */
        IF @fiscal_year IS NULL SELECT @fiscal_year = dbo.LF_GetFiscalYear(GETDATE())

    /*  Procedure Variables  */
        
        --Hard coded for the campaign (may change)
        DECLARE @overall_campaign_no INT = 1, @overall_campaign_name VARCHAR(30) = 'Campaign16';
        
        --Determine current fiscal year date information
        DECLARE @fiscal_start_dt DATETIME = [dbo].[LF_GetFiscalYearStartDt](@fiscal_year)
        DECLARE @fiscal_end_dt DATETIME = [dbo].[LF_GetFiscalYearEndDt](@fiscal_year)
        DECLARE @current_dt DATETIME = GETDATE()
        IF @current_dt > @fiscal_end_dt SELECT @current_dt = @fiscal_end_dt

        DECLARE @fiscal_year_to_date DECIMAL(18,2) = 0.0, @last_fiscal_year_to_date DECIMAL(18,2) = 0.0
        DECLARE @campaign_to_date DECIMAL(18,2) = 0.0, @campaign_goal DECIMAL(18,2) = 0.0, @campaign_percentage decimal(18,4) = 0.0

        /*  Get Fiscal Year totals (this year and last year)  */

            SELECT @fiscal_year_to_date  = SUM([contribution_amount])
			                               FROM [dbo].[LV_CAMPAIGN_ACTIVITY_CONTRIBUTIONS]
                                          WHERE [contribution_date] BETWEEN @fiscal_start_dt AND @current_dt
                                            AND [campaign_category] <> 'Skip'
                                           
           
            SELECT @last_fiscal_year_to_date  = SUM([contribution_amount])
	    		                           FROM [dbo].[LV_CAMPAIGN_ACTIVITY_CONTRIBUTIONS]
                                           WHERE [contribution_date] BETWEEN DATEADD(YEAR,-1,@fiscal_start_dt) AND DATEADD(YEAR,-1,@current_dt)
                                             AND [campaign_category] <> 'Skip'
                                             

    /*  Get the campaign to date contribution amount  */

       SELECT @campaign_to_date = SUM([contribution_amount])
                                  FROM [dbo].[LV_CAMPAIGN_ACTIVITY_CONTRIBUTIONS]
                                  WHERE [overall_campaign] = @overall_campaign_name
                                    AND [campaign_category] <> 'Skip'

    /*  Retrieve the Campaign Goal from LTR_CM_CATEGORY1  */

        SELECT @campaign_goal = SUM(ISNULL(ct.[goal],0))
        FROM [dbo].[LTR_CM_CATEGORY1] AS ct 
             INNER JOIN [dbo].[LTR_FUND_DETAIL] AS dt ON dt.cm_category1 = ct.[id] AND dt.[overall_cm_no] = @overall_campaign_no

        SELECT @campaign_goal = ISNULL(@campaign_goal,0.0)

    /*  Assuming the Campaign Goal is greater than zero, calculate the percentage toward goal  */
    
        IF @campaign_goal > 0 
            SELECT @campaign_percentage = (@campaign_to_date / @campaign_goal)

    FINISHED:

        /*  Select final data set to return to the report  */

            SELECT @fiscal_year_to_date AS [fiscal_year_to_date],
                   @last_fiscal_year_to_date AS [last_fiscal_year_to_date],
                   @campaign_to_date AS [campaign_to_date], 
                   @campaign_goal AS [campaign_goal], 
                   @campaign_percentage AS [campaign_percentage]

    DONE:

END
GO

EXECUTE [dbo].[LRP_ADV_CAMPAIGN_ACTIVITY] @fiscal_year = 2020
