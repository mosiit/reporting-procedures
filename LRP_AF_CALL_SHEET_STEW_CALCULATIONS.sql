USE [impresario]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[LRP_AF_CALL_SHEET_STEW_CALCULATIONS]
(
	@customer_no INT
)    
AS

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
SET NOCOUNT ON 

DECLARE @currentFY INT 

SELECT @currentFY = fyear
FROM dbo.TR_BATCH_Period
WHERE GETDATE() BETWEEN start_dt AND end_dt

-- Final results will be here 
DECLARE @resultsTbl TABLE 
(
	customer_no INT,
	[Current FY AF Des Plg Balance] MONEY,
	[Current FY Non AF Des GP Total] MONEY,
	[Last GP Amt] MONEY,
	[Last GP Date] DATE,
	[Last AF Des GP Amt] MONEY,
	[Last AF Des GP Date] DATE,
	[Num FYs gave large AF Designation] INT 
)

INSERT INTO @resultsTbl
	(customer_no,
	[Current FY AF Des Plg Balance],
	[Current FY Non AF Des GP Total],
	[Last GP Amt],
	[Last GP Date],
	[Last AF Des GP Amt],
	[Last AF Des GP Date],
	[Num FYs gave large AF Designation])
VALUES  ( 
	@customer_no,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL 
)

DECLARE @RawData TABLE
(
	[reference_no] [INT] NOT NULL,
	[customer_number] [INT] NOT NULL,
	[name] [VARCHAR](55) NULL,
	[type] [CHAR](1) NULL,
	[creditee_type] [VARCHAR](20) NOT NULL,
	[date] [DATE] NULL,
	[fiscal_year] [INT] NOT NULL,
	[fiscal_quarter] [INT] NULL,
	[fiscal_period] [INT] NOT NULL,
	[contribution_amount] [MONEY] NULL,
	[received_amount] [MONEY] NULL,
	[anonymous] [VARCHAR](30) NULL,
	[soft_credit_type] [VARCHAR](255) NULL,
	[fund_description] [VARCHAR](30) NULL,
	[printable_fund] [VARCHAR](80) NULL,
	[designation] [VARCHAR](30) NULL,
	[overall_campaign] [VARCHAR](30) NULL,
	[campaign] [VARCHAR](30) NULL,
	[campaign_category] [VARCHAR](30) NULL,
	[account_goal] [VARCHAR](30) NULL,
	[account_group] [VARCHAR](30) NULL,
	[appeal] [VARCHAR](30) NOT NULL,
	[media] [VARCHAR](30) NULL,
	[source] [VARCHAR](50) NULL,
	[notes] [VARCHAR](MAX) NULL,
	[cancel] [CHAR](1) NULL,
	[pledge_status_desc] [VARCHAR](30) NULL,
	[billing_type] [VARCHAR](30) NULL,
	[batch] [INT] NULL,
	[KG_xfer_dt] [DATETIME] NULL,
	[KGift_desc] [VARCHAR](50) NULL,
	[stock_ticker] [VARCHAR](255) NULL,
	[contribution_detail1] [VARCHAR](30) NULL,
	[contribution_detail2] [VARCHAR](30) NULL,
	[agreement_date] [VARCHAR](255) NULL,
	[challenge_earned] [VARCHAR](255) NULL,
	[pg_instrument] [VARCHAR](30) NULL,
	[solicitation] [VARCHAR](255) NULL,
	[solicitor_number] [INT] NULL,
	[solicitor_name] [VARCHAR](55) NULL,
	[cpf_flag] [CHAR](1) NULL,
	[exh_prog_cat] [VARCHAR](30) NULL,
	[full_fund_name] [VARCHAR](80) NULL,
	[cm_category1] [VARCHAR](80) NULL,
	[cm_intermediate1] [VARCHAR](80) NULL,
	[cm_pillar1] [VARCHAR](80) NULL,
	[cm_category2] [VARCHAR](80) NULL,
	[cm_intermediate2] [VARCHAR](80) NULL,
	[cm_category3] [VARCHAR](80) NULL,
	[cm_intermediate3] [VARCHAR](80) NULL,
	[custom_2] [VARCHAR](255) NULL,
	[custom_9] [VARCHAR](255) NULL,
	[main_customer_type] [VARCHAR](30) NULL,
	[original_customer_type] [VARCHAR](30) NULL,
	[sort_name] [VARCHAR](55) NULL,
	[channel] VARCHAR(30)
)

INSERT INTO @RawData
        (reference_no,
         customer_number,
         name,
         type,
         creditee_type,
         date,
         fiscal_year,
         fiscal_quarter,
         fiscal_period,
         contribution_amount,
         received_amount,
         anonymous,
         soft_credit_type,
         fund_description,
         printable_fund,
         designation,
         overall_campaign,
         campaign,
         campaign_category,
         account_goal,
         account_group,
         appeal,
         media,
         source,
         notes,
         cancel,
         pledge_status_desc,
         billing_type,
         batch,
         KG_xfer_dt,
         KGift_desc,
         stock_ticker,
         contribution_detail1,
         contribution_detail2,
         agreement_date,
         challenge_earned,
         pg_instrument,
         solicitation,
         solicitor_number,
         solicitor_name,
         cpf_flag,
         exh_prog_cat,
         full_fund_name,
         cm_category1,
         cm_intermediate1,
         cm_pillar1,
         cm_category2,
         cm_intermediate2,
         cm_category3,
         cm_intermediate3,
         custom_2,
         custom_9,
         main_customer_type,
         original_customer_type,
         sort_name,
		 channel)
EXEC [dbo].[LRP_MOS_ADVANCEMENT_CONTRIBUTION_STEW_DETAIL]
	@cont_start_dt = NULL,
	@cont_end_dt = NULL,
	@camp_category_str = NULL,
	@designation_str = NULL,
	@customer_no = @customer_no 


-- Current FY AF Des Plg Balance
UPDATE @resultsTbl
SET [Current FY AF Des Plg Balance] = total
FROM 
(
	SELECT customer_number, SUM(contribution_amount) AS total 
	FROM @RawData
	WHERE type = 'P'
	AND designation = 'Annual Fund'
	AND contribution_amount - received_amount > 0 
	AND fiscal_year = @currentFY
	GROUP BY customer_number
) X

--Current FY Non AF Des GP Total
UPDATE @resultsTbl
SET [Current FY Non AF Des GP Total] = total
FROM 
(
	SELECT customer_number, SUM(contribution_amount) AS total
	FROM @RawData
	WHERE fiscal_year = @currentFY
	AND designation <> 'Annual Fund'
	GROUP BY customer_number, fiscal_year, campaign_category
) X 

--Last GP Amt/Dt
UPDATE @resultsTbl
SET [Last GP Amt] = total, [Last GP Date] = date
FROM 
(
	SELECT customer_number, date, SUM(contribution_amount) AS total
	FROM @RawData
	WHERE date = (SELECT MAX(date) FROM @RawData WHERE date < GETDATE())
	GROUP BY customer_number, date 
) X

--Last AF Des GP Amt/Dt
UPDATE @resultsTbl
SET [Last AF Des GP Amt] = total , [Last AF Des GP Date] = date
FROM 
( 
	SELECT customer_number, date, SUM(contribution_amount) AS total
	FROM @RawData
	WHERE date = (SELECT MAX(date) FROM @RawData WHERE designation = 'Annual Fund' AND date < GETDATE())
		AND designation = 'Annual Fund'
	GROUP BY customer_number, date 
) X 

-- # FYs gave 2500+ AF Designation
UPDATE @resultsTbl
SET [Num FYs gave large AF Designation] = [# FYs gave 2500+ AF Designation] 
FROM 
( 
	SELECT COUNT(*) AS [# FYs gave 2500+ AF Designation] 
	FROM 
	(
		SELECT customer_number, fiscal_year, SUM(contribution_amount) AS total
		FROM @RawData
		GROUP BY customer_number, fiscal_year
		HAVING SUM(contribution_amount) >= 2500 
	) X 
) X1

SELECT * FROM @resultsTbl