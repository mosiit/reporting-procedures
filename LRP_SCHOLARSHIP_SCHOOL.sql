USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_SCHOLARSHIP_SCHOOL]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_SCHOLARSHIP_SCHOOL] AS' 
END
GO

ALTER PROCEDURE [dbo].[LRP_SCHOLARSHIP_SCHOOL]
        @report_start_dt DATETIME = NULL, 
        @report_end_dt DATETIME = NULL,
        @customer_type_id INT = 0,
        @scholarship_id VARCHAR(30) = '-1',
        @sort_by VARCHAR(30) = NULL,
        @list_no INT = NULL
WITH RECOMPILE AS BEGIN
 
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    SET NOCOUNT ON;
        
    /*  Procedure Variables  */

        DECLARE @ord_no INT, @perf_date CHAR(10), @prod_name VARCHAR(50);
        DECLARE @cust_type_id_school INT, @cust_type_id_school_official INT;
        DECLARE @list_name VARCHAR(50) = '';
    
    /*  Check Parameters  */
    
        IF @report_start_dt IS NULL
            SELECT @report_start_dt = dbo.LF_GetFiscalYearStartDt(dbo.LF_GetFiscalYear(GETDATE()));

        IF @report_end_dt IS NULL
            SELECT @report_end_dt = dbo.LF_GetFiscalYearStartDt(dbo.LF_GetFiscalYear(@report_start_dt));

        SELECT @customer_type_id = ISNULL(@customer_type_id,0);
        SELECT @sort_by = ISNULL(@sort_by,'Customer Name');

        SELECT @scholarship_id = ISNULL(@scholarship_id, '-1');
        IF @scholarship_id IN ('','0') SELECT @scholarship_id = '-1';

        SELECT @list_no = ISNULL(@list_no,0)

        SELECT @list_name = ISNULL([list_desc], '')
                            FROM [dbo].[T_LIST] 
                            WHERE [list_no] = @list_no
        
    /*  Make sure the date parameters include the full dates, starting at 00:00 and ending at 23:59  */

        SELECT @report_start_dt = CAST(@report_start_dt AS DATE),
               @report_end_dt = CONVERT(CHAR(10),@report_end_dt,111) + ' 23:59:59.957';

    /*  For the purposes of this report, the School and School Official Record customer types are the same thing.
        The id numbers are needed to combine the two  */

        SELECT @cust_type_id_school = ISNULL([id],0)
                                      FROM [dbo].[TR_CUST_TYPE] 
                                      WHERE [description] = 'School';

         SELECT @cust_type_id_school_official = ISNULL([id],0)
                                                FROM [dbo].[TR_CUST_TYPE] 
                                                WHERE [description] = 'School Official Record';
       
    /*  If customer type id selected was school official record, change to school  */

        IF @customer_type_id = @cust_type_id_school_official 
            SELECT @customer_type_id = @cust_type_id_school;
    
    /*  Create temporary tables  */
            
            IF OBJECT_ID('tempdb..#school_orders') IS NOT NULL DROP TABLE [#school_orders];

            CREATE TABLE [#school_orders] ([order_no] INT,
                                           [customer_no] INT);

            IF OBJECT_ID('tempdb..#school_data') IS NOT NULL DROP TABLE [#school_data];

            CREATE TABLE [#school_data] ([data_type] VARCHAR(10), 
                                         [order_no] INT, 
                                         [sli_no] INT, 
                                         [customer_no] INT, 
                                         [performance_date] CHAR(10), 
                                         [title_name] VARCHAR(30), 
                                         [scholarship_no] INT,
                                         [production_name] VARCHAR(50), 
                                         [zone_name] VARCHAR(30), 
                                         [price_type_name] VARCHAR(30), 
                                         [create_dt] DATETIME, 
                                         [due_amount] DECIMAL(18,2), 
                                         [total_paid] DECIMAL(18,2));

            IF OBJECT_ID('tempdb..#school_final') IS NOT NULL DROP TABLE [#school_final];

            CREATE TABLE [#school_final] ([rpt_message] VARCHAR(100), 
                                          [data_type] VARCHAR(10), 
                                          [order_no] INT, 
                                          [order_row_no] INT,
                                          [order_exh_attend] INT,
                                          [sli_no] INT, 
                                          [customer_no] INT, 
                                          [order_performance_date] CHAR(10), 
                                          [performance_date] CHAR(10),
                                          [title_name] VARCHAR(50), 
                                          [scholarship_no] INT, 
                                          [production_name] VARCHAR(50), 
                                          [zone_name] VARCHAR(50), 
                                          [performance_code] VARCHAR(25), 
                                          [price_type_name] VARCHAR(50), 
                                          [create_dt] DATETIME,
                                          [due_amount] MONEY, 
                                          [total_paid] MONEY, 
                                          [customer_first_name] VARCHAR(50), 
                                          [customer_middle_name] VARCHAR(50),
                                          [customer_last_name] VARCHAR(100), 
                                          [customer_type_no] INT, 
                                          [customer_type] VARCHAR(30), 
                                          [address_type_no] INT, 
                                          [address_type] VARCHAR(30), 
                                          [address_street1] VARCHAR(75), 
                                          [address_street2] VARCHAR(75), 
                                          [address_city] VARCHAR(50), 
                                          [address_state] VARCHAR(50), 
                                          [address_zip_code] VARCHAR(50), 
                                          [address_city_state] VARCHAR(100), 
                                          [order_custom_1] VARCHAR(100), 
                                          [order_custom_2] VARCHAR(100), 
                                          [order_custom_3] VARCHAR(100), 
                                          [order_custom_4] VARCHAR(100), 
                                          [order_custom_5] VARCHAR(100), 
                                          [order_custom_6] VARCHAR(100), 
                                          [order_custom_7] VARCHAR(100), 
                                          [order_custom_8] VARCHAR(100), 
                                          [order_custom_9] VARCHAR(100), 
                                          [order_custom_10] VARCHAR(100), 
                                          [order_notes] VARCHAR(255),
                                          [order_scholarship] VARCHAR(50), 
                                          [sort_field] VARCHAR(255), 
                                          [sort_text] VARCHAR(255), 
                                          [program_counter] int);

    /*  Generate a list of all school sales for designated date range  */

            WITH CTE_SCHOL_PAY ([order_no]) 
            AS (SELECT DISTINCT [order_no] 
                FROM [dbo].[LV_SCHOLARSHIP_PAYMENTS])
        INSERT INTO [#school_orders] ([order_no], [customer_no])
        SELECT DISTINCT det.[order_no], det.[customer_no]
        FROM [dbo].[LV_ORDER_DETAIL] AS det
             INNER JOIN [CTE_SCHOL_PAY] AS pay ON pay.[order_no] = det.[order_no]
             INNER JOIN [dbo].[T_ORDER] AS ord ON ord.[order_no] = det.[order_no] 
        WHERE [performance_dt] BETWEEN @report_start_dt AND @report_end_dt 
          AND ord.[MOS] IN (12, 13);     --12 = Schools / 13 = Web Sales School

        IF ISNULL(@list_no, 0) > 0
            DELETE FROM [#school_orders]
            WHERE [customer_no] NOT IN (SELECT [customer_no] FROM [dbo].[T_LIST_CONTENTS] WHERE [list_no] = @list_no)

    /* Get School Order Data */

           
        INSERT INTO [#school_data] ([data_type], [order_no], [sli_no], [customer_no], [performance_date], [title_name], [scholarship_no], [production_name], 
                                    [zone_name], [price_type_name], [create_dt], [due_amount], [total_paid])
        SELECT 'ord_info', 
                det.[order_no], 
                det.[sli_no], 
                det.[customer_no], 
                det.[performance_date], 
                det.[title_name], 
                0, 
                det.[production_name], 
                det.[zone_name], 
                det.[price_type_name], 
                det.[create_dt], 
                det.[due_amount], 
                0.00 
        FROM [dbo].[LV_ORDER_DETAIL] AS det
             INNER JOIN [#school_orders] AS ord ON ord.[order_no] = det.[order_no]

    /* Get Traveling Programs Payment Data */

        INSERT INTO [#school_data] ([data_type], [order_no], [sli_no], [customer_no], [performance_date], [title_name], [scholarship_no], [production_name], 
                                    [zone_name], [price_type_name], [create_dt], [due_amount], [total_paid])
        SELECT 'pay_info', 
               pay.[order_no], 
               0, 
               pay.[customer_no], 
               '', 
               'payment', 
               pay.[scholarship_no], 
               pay.[scholarship_name], 
               '', 
               pay.[payment_method], 
               pay.[payment_dt], 
               0.00, 
               SUM(ISNULL(pay.[payment_amount],0.0))
        FROM [dbo].[LV_SCHOLARSHIP_PAYMENTS] AS pay (NOLOCK)
             INNER JOIN [#school_orders] AS ord ON ord.[order_no] = pay.[order_no] 
        WHERE (pay.[scholarship_no] = @scholarship_id OR @scholarship_id = '-1')
        GROUP BY pay.[order_no], pay.[customer_no], pay.[scholarship_no], pay.[scholarship_name], pay.[payment_method], pay.[payment_dt]
        HAVING SUM(ISNULL([payment_amount],0.0)) <> 0.00;

    /*  Update Interdepartmental Transfer payments  */

        UPDATE [#school_data] 
        SET [production_name] = 'Interdepartmental Transfer' 
        WHERE [data_type] = 'pay_info' 
          AND [production_name] = '' 
          AND price_type_name = 'Interdepartmental Transfer';

    /*  Update Unknown Scholarship Names  */

        UPDATE [#school_data] 
        SET [production_name] = 'Unknown Scholarship' 
        WHERE [data_type] = 'pay_info' 
          AND  [production_name] = '';

--    /*  This field needs to have a date value in it even if it's not going to be used - Put today's date into any blank fields  */

        UPDATE [#school_data] 
        SET [performance_date] = CONVERT(CHAR(10),[create_dt],111) 
        WHERE [performance_date] = '';

    /*  Create final data table  */

        INSERT INTO [#school_final] ([rpt_message], [data_type], [order_no], [order_row_no], [sli_no], [customer_no], [order_performance_date], [performance_date], [title_name], [scholarship_no],
                                     [production_name], [zone_name], [performance_code], [price_type_name], [create_dt], [due_amount], [total_paid], [customer_first_name], 
                                     [customer_middle_name], [customer_last_name], [customer_type_no], [customer_type], [address_type_no], [address_type], [address_street1],
                                     [address_street2], [address_city], [address_state], [address_zip_code], [address_city_state], [order_custom_1], [order_custom_2], [order_custom_3],
                                     [order_custom_4], [order_custom_5], [order_custom_6], [order_custom_7], [order_custom_8], [order_custom_9], [order_custom_10], [order_notes],
                                     [order_scholarship], [sort_field], [sort_text], [program_counter])
        SELECT '', 
               dat.[data_type], 
               dat.[order_no], 
               ROW_NUMBER() OVER(PARTITION BY dat.[order_no] ORDER BY dat.[order_no], dat.[sli_no] ASC) AS [order_row_no],
               dat.[sli_no], 
               dat.[customer_no], 
               '', 
               dat.[performance_date], 
               dat.[title_name], 
               dat.[scholarship_no], 
               dat.[production_name], 
               dat.[zone_name], 
               '', 
               dat.[price_type_name], 
               dat.[create_dt], 
               dat.[due_amount], 
               dat.[total_paid], 
               ISNULL(cus.[fname], ''), 
               ISNULL(cus.[mname], ''), 
               ISNULL(cus.[lname], ''), 
               ISNULL(cus.[cust_type], 0) AS [cust_type], 
               ISNULL(ctp.[description],''), 
               ISNULL(adr.[address_type],0), 
               ISNULL(atp.[description],''), 
               ISNULL(adr.[street1], ''), 
               ISNULL(adr.[Street2],''), 
               ISNULL(adr.[city],''), 
               ISNULL(adr.[state],''), 
               ISNULL(adr.[postal_code],''), 
               '', 
               LTRIM(ISNULL(ord.[custom_1],'')), 
               LTRIM(ISNULL(ord.[custom_2],'')), 
               LTRIM(ISNULL(ord.[custom_3],'')), 
               LTRIM(ISNULL(ord.[custom_4],'')), 
               LTRIM(ISNULL(ord.[custom_5],'')), 
               LTRIM(ISNULL(ord.[custom_6],'')), 
               LTRIM(ISNULL(ord.[custom_7],'')), 
               LTRIM(ISNULL(ord.[custom_8],'')), 
               LTRIM(ISNULL(ord.[custom_9],'')), 
               LTRIM(ISNULL(ord.[custom_0],'')), 
               ISNULL(ord.[notes],''), 
               '', 
               '', 
               '', 
               0
        FROM [#school_data] AS dat
             LEFT OUTER JOIN [dbo].[T_CUSTOMER] AS cus ON cus.[customer_no] = dat.[customer_no]
             LEFT OUTER JOIN [dbo].[TR_CUST_TYPE] AS ctp ON ctp.[id] = cus.[cust_type]
             LEFT OUTER JOIN [dbo].[T_ADDRESS] AS adr ON adr.[customer_no] = dat.[customer_no] AND adr.[inactive] = 'N' AND adr.primary_ind = 'Y'
             LEFT OUTER JOIN [dbo].[TR_ADDRESS_TYPE] AS atp ON atp.[id] = adr.[address_type]
             LEFT OUTER JOIN [dbo].[T_ORDER] AS ord ON ord.[order_no] = dat.[order_no];

    /*  Add Exhibit Hall Attendance to each order  */

        WITH CTE_EXH_ATTEND
        AS (SELECT det.[order_no], 
                   det.[title_no], 
                   det.[title_name], 
                   SUM(det.[sale_total]) AS [sale_total]
            FROM [dbo].[LT_HISTORY_TICKET] AS det
                 INNER JOIN [#school_orders] AS ord ON ord.[order_no] = det.[order_no]
            WHERE [det].[title_no] = 27
            GROUP BY det.[order_no], det.[title_no], det.[title_name])
        UPDATE fin
        SET fin.[order_exh_attend] = ISNULL(cte.[sale_total],0)
        FROM [#school_final] AS fin
             LEFT OUTER JOIN [CTE_EXH_ATTEND] AS cte ON cte.[order_no] = fin.[order_no]

    /* Count the number of programs */

        UPDATE [#school_final] 
        SET [program_counter] = 1 
        WHERE [data_type] = 'ord_info';

    /*  Add the dash in between the first five and the last four of zip plus four values  */
        
        UPDATE [#school_final] 
        SET [address_zip_code] = LEFT([address_zip_code],5) + '-' + RIGHT([address_zip_code],4) 
        WHERE LEN([address_zip_code]) = 9;

    /*  Combine city and state into one field for sorting purposes  */

        UPDATE [#school_final] 
        SET [address_city_state] = [address_city] + ', ' + [address_state];
            
    /*  Combine School and School Official Record into a single customer type  */

        UPDATE [#school_final] 
        SET [customer_type_no] = @cust_type_id_school, [customer_type] = 'School' 
        WHERE [customer_type_no] = @cust_type_id_school_official;

    /*  Remove Interdepartmental Transfers */

        DELETE FROM [#school_final] 
        WHERE [order_no] IN (SELECT [order_no] 
                             FROM [#school_final] 
                             WHERE [title_name] = 'payment' 
                               AND [production_name] = 'Interdepartmental Transfer');

    /*  Remove unwanted constiruent types and schlarships based on parameters  */

        IF @customer_type_id > 0
            DELETE FROM [#school_final] 
            WHERE [order_no] NOT IN (SELECT [order_no] 
                                     FROM [#school_final] 
                                     WHERE [customer_type_no] = @customer_type_id);


    /*  Set Order Performance Date or Order Scholarship - Only if that's needed for the sort  */
                      
        WITH CTE_PERF_DATE ([order_no], [perf_date])
        AS (SELECT [order_no], MAX(ISNULL([performance_date],''))
            FROM [#school_final] 
            WHERE [data_type] = 'ord_info'
            GROUP BY [order_no]
           )
        UPDATE sch
        SET sch.[order_performance_date] = dat.[perf_date]         
        FROM [#school_final] AS sch
             INNER JOIN [CTE_PERF_DATE] AS dat ON dat.[Order_no] = sch.[order_no];
           
           
        WITH CTE_PRODUCTIONS ([order_no], [prod_name])
        AS (SELECT [order_no], MAX([production_name]) 
            FROM [#school_final] WHERE [data_type] = 'pay_info' 
            GROUP BY [order_no])
        UPDATE sch
        SET sch.[order_scholarship] = pro.[prod_name]
        FROM [#school_final] AS sch
             INNER JOIN [CTE_PRODUCTIONS] AS pro ON pro.[order_no] = sch.[order_no];

    /* Remove zero total payments */

        --WITH CTE_ZERO_PAYMENTS ([order_no], [prod_name], [total_paid])
        WITH CTE_ZERO_PAYMENTS ([order_no], [prod_name], [total_paid])
        AS (SELECT [order_no], 
                   [production_name],
                   SUM(ISNULL([total_paid],0.0)) 
            FROM [#school_final] 
            WHERE [data_type] = 'pay_info' 
            GROUP BY [order_no], [production_name]
            HAVING SUM(ISNULL([total_paid],0.0)) = 0.00
           )
           DELETE sch
           FROM [#school_final] AS sch
                INNER JOIN [CTE_ZERO_PAYMENTS] AS pay ON pay.[order_no] = sch.[order_no] AND pay.[prod_name] = sch.[production_name];
         
    /*  Set Sort Field  */

        IF @sort_by = 'Performance Date'
            UPDATE [#school_final] 
            SET [sort_field] = [order_performance_date] + ' ' + [customer_last_name] + ' ' + [customer_first_name] + ' ' + [customer_middle_name], 
                [sort_text] = [order_performance_date];
        ELSE IF @sort_by = 'Scholarship'
            UPDATE [#school_final] 
            SET [sort_field] = [order_scholarship] + ' ' + [customer_last_name] + ' ' + [customer_first_name] + ' ' + [customer_middle_name], 
                [sort_text] = [order_scholarship];
        ELSE IF @sort_by = 'Customer Type'
            UPDATE [#school_final] 
            SET [sort_field] = [customer_type] + ' ' + [customer_last_name] + ' ' + [customer_first_name] + ' ' + [customer_middle_name], 
                [sort_text] = [customer_type];
        ELSE IF @sort_by = 'Customer Name'
            UPDATE [#school_final] 
            SET [sort_field] = [customer_last_name] + ' ' + [customer_first_name] + ' ' + [customer_middle_name], 
                [sort_text] = [customer_last_name] + ' ' + [customer_first_name] + ' ' + [customer_middle_name];
        ELSE    --Default sort is Order Number 
            UPDATE [#school_final] 
            SET [sort_field] = CONVERT(VARCHAR(100),[order_no]), 
            [sort_text] = CONVERT(VARCHAR(100),[order_no]);
       
    FINISHED:

        /*  Select the final record set from #final_table  */

            SELECT [data_type], 
                   [order_no], 
                   [order_row_no],
                   ISNULL([order_exh_attend], 0) AS [order_exh_attend],
                   [sli_no], 
                   [customer_no], 
                   [order_performance_date], 
                   [performance_date], 
                   [title_name], 
                   [scholarship_no], 
                   [production_name], 
                   [zone_name], 
                   [performance_code], 
                   [price_type_name], 
                   [program_counter], 
                   [create_dt], 
                   [due_amount], 
                   [total_paid], 
                   [customer_first_name], 
                   [customer_middle_name], 
                   [customer_last_name], 
                   [customer_type_no], 
                   [customer_type], 
                   [address_type_no], 
                   [address_type], 
                   [address_street1], 
                   [address_street2], 
                   [address_city], 
                   [address_state], 
                   [address_zip_code], 
                   [address_city_state], 
                   [order_custom_1], 
                   [order_custom_2], 
                   [order_custom_3], 
                   [order_custom_4], 
                   [order_custom_5], 
                   [order_custom_6], 
                   [order_custom_7], 
                   [order_custom_8], 
                   [order_custom_9], 
                   [order_custom_10], 
                   [order_notes], 
                   [sort_field], 
                   [sort_text], 
                   [order_scholarship], 
                   @list_no AS [list_no],
                   @list_name AS [list_name],
                   [rpt_message]
            FROM [#school_final]
            WHERE [data_type] = 'pay_info'
            ORDER BY [sort_field], [order_no], [data_type], [order_row_no];

        /*  Clean up and Destroy Temporary Tables  */

            IF OBJECT_ID('tempdb..#school_final') IS NOT NULL DROP TABLE [#school_final];
            IF OBJECT_ID('tempdb..#school_data') IS NOT NULL DROP TABLE [#school_data];
            IF OBJECT_ID('tempdb..#school_orders') IS NOT NULL DROP TABLE [#school_orders];

        DONE:

END
GO

GRANT EXECUTE ON [dbo].[LRP_SCHOLARSHIP_SCHOOL] TO [ImpUsers] AS [dbo]
GO


EXECUTE [dbo].[LRP_SCHOLARSHIP_SCHOOL] @report_start_dt = '7-1-2019', @report_end_dt = '6-30-2020', @customer_type_id = 0, @scholarship_id = '-1', @sort_by = 'Scholarship', @list_no = 14129

