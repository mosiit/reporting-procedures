USE [impresario]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[LRP_MEMBER_VISIT_HISTORICAL_DTL_SCREEN]
	@customer_no		INT
AS

BEGIN

	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	--
	-- PULLING HISTORIC ATTENDANCE DATAS STORED IN LT_CORRESPONDENCE TABLE 
	--

	SELECT	DISTINCT @customer_no AS customer_no,
			0 AS id_key,
			att.[sent_dt] AS 'Visit Date',
			cname.description AS 'Scan Location',
			pmi.memb_level_description AS 'Membership Type',
			SUBSTRING(corrcomm,1, CHARINDEX('Time:',corrcomm)-3) corrcomm
	FROM [dbo].[LV_PAST_MEMBERSHIP_INFO] pmi
	INNER JOIN[dbo].[LT_CORRESPONDENCE] att 
		ON	att.[customer_no] = pmi.[customer_no]
		AND	att.[sent_dt] BETWEEN pmi.[initiation_date] AND pmi.[expiration_date]
	INNER JOIN dbo.LTR_CORR_NAME cname
		ON att.corrname_no = cname.id
	WHERE att.corrtype_no = 32 -- Membership Attendance
		AND pmi.customer_no = @customer_no
	ORDER BY 'Visit Date' DESC, 'Membership Type'

END

GO


