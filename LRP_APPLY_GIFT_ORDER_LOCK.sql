USE [impresario];
GO

SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
-- =============================================
-- Author:		Tessitura Network Consulting Jon Ballinger
-- Create date: 2/2/2018
-- Description:	Locks a gift to an order so that the entitlements associated with gift can be used in the order.  This is called from a ssrs report
-- LRP_APPLY_GIFT_ORDER_LOCK 1187784,'86F4-0334-8940,B169-0334-AB7C'

-- Updated: DSJ 6/2019 WO 99721, added 2 columns: 
-- =============================================
ALTER PROCEDURE [dbo].[LRP_APPLY_GIFT_ORDER_LOCK]
    @order_no INT,
    @gift_code VARCHAR(1000)
AS
BEGIN

    SET NOCOUNT ON;

    DECLARE @gift_no INT,
        @message VARCHAR(500),
        @lock_timeout INT,
        @gift_status CHAR(1);

    SELECT @lock_timeout = CONVERT(INT, dbo.[FS_GET_DEFAULT_VALUE](NULL, 'Entitlements', 'LD_ORDER_LOCK_TIMEOUT'));

    CREATE TABLE #gc
    (
        gc VARCHAR(50),
        gc_no INT NULL,
        gift_status CHAR(1) NULL,
        failed CHAR(1),
        message VARCHAR(500),
        num_items INT NULL,
        entitlement_desc VARCHAR(255) NULL,
        expr_dt DATETIME NULL,
		venue VARCHAR(30) NULL,
		pass_type VARCHAR(30) NULL 
    );

    INSERT INTO #gc
    SELECT Element,
        b.id_key,
        b.gift_status,
        CASE
            WHEN id_key IS NULL
                 OR gift_status = 'A' THEN 'Y'
            ELSE 'C'
        END,
        CASE
            WHEN id_key IS NULL THEN 'The gift code entered is invalid.'
            WHEN gift_status = 'A' THEN 'Already Accepted gifts can not be locked.'
            ELSE ''
        END,
        b.num_items,
        c.entitlement_desc,
        b.expr_dt,
		venue.description,
		price.description
    FROM FT_SPLIT_LIST(@gift_code, ',') a
    LEFT JOIN LT_ENTITLEMENT_GIFT b
        ON (
               a.Element = b.gift_code
               OR a.Element = b.group_gift_code
           )
    LEFT JOIN LTR_ENTITLEMENT c
        ON c.entitlement_no = b.entitlement_no
	LEFT JOIN tr_tkw venue
		ON venue.id = c.ent_tkw_id
	LEFT JOIN dbo.TR_PRICE_TYPE_GROUP price
		ON price.id = c.ent_price_type_group;

    --Remove locks if time is more than 15 minutes
    DELETE a
    FROM LTX_GIFT_ORDER_LOCK a
    JOIN #gc b
        ON a.gift_no = b.gc_no
           AND DATEADD(mi, 15, create_dt) < GETDATE()
           AND order_no = @order_no
           AND NOT EXISTS
                   (
                       SELECT gift_no FROM LTX_CUST_ENTITLEMENT x WHERE x.gift_no = a.gift_no
                   )
    WHERE COALESCE(failed, 'N') != 'Y';

    UPDATE a
    SET a.message = 'The gift is currently locked in another order.',
        a.failed = 'Y'
    FROM #gc a
    JOIN LTX_GIFT_ORDER_LOCK b
        ON b.gift_no = a.gc_no
           AND b.order_no = @order_no
    WHERE COALESCE(failed, 'N') != 'Y';

    INSERT INTO LTX_GIFT_ORDER_LOCK
    (
        order_no,
        gift_no
    )
    SELECT @order_no,
        gc_no
    FROM #gc
    WHERE COALESCE(failed, 'N') != 'Y';

    UPDATE #gc
    SET message = 'Successfully Locked' -- The lock for the order has been applied.'
    WHERE COALESCE(failed, 'N') != 'Y';

    --message_return:
    SELECT gc,
        gc_no,
        gift_status,
        failed,
        message,
        num_items,
        entitlement_desc,
        expr_dt,
		venue,
		pass_type
    FROM #gc;
END;
GO

GRANT EXECUTE ON [LRP_APPLY_GIFT_ORDER_LOCK] TO ImpUsers;
GO