USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LRP_FIND_QUERY_ELEMENTS]    Script Date: 6/23/2021 4:11:06 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



ALTER PROCEDURE [dbo].[LRP_FIND_QUERY_ELEMENTS] @element VARCHAR(300)
AS

-- ============================================================================================
-- H. Sheridan, 5/25/2021
--
-- Service Now ticket SCTASK0001515 (Research output sets in TR_QUERY_ELEMENT)
--
-- Details: Find out which output sets a given query element is a member of. 
--
-- ============================================================================================

BEGIN

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    SET NOCOUNT ON;

    SELECT q.query_desc,
           COALESCE(cb.fname + ' ' + cb.lname, q.created_by) AS 'Created by',
           q.create_dt AS 'Created on',
           COALESCE(lub.fname + ' ' + lub.lname, q.last_updated_by) AS 'Last Updated by',
           q.last_update_dt AS 'Last Updated on',
           q.last_used_dt AS 'Last Used on',
           q.create_loc
    FROM dbo.TX_QUERY_ELEMENT txq
    JOIN dbo.TR_QUERY_ELEMENT trq ON txq.element_no = trq.id
    JOIN dbo.T_QUERY q ON txq.query_no = q.query_no
    LEFT JOIN dbo.T_METUSER cb ON q.created_by = cb.userid
    LEFT JOIN dbo.T_METUSER lub ON q.last_updated_by = lub.userid
    WHERE trq.[description] = @element
          AND txq.inactive = 'N'
          AND trq.inactive = 'N'
    ORDER BY q.last_used_dt DESC;

END;

GO


