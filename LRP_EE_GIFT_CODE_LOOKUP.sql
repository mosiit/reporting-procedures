USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LRP_EE_GIFT_CODE_LOOKUP]    Script Date: 12/28/2018 3:16:21 PM ******/
DROP PROCEDURE [dbo].[LRP_EE_GIFT_CODE_LOOKUP]
GO

/****** Object:  StoredProcedure [dbo].[LRP_EE_GIFT_CODE_LOOKUP]    Script Date: 12/28/2018 3:16:21 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[LRP_EE_GIFT_CODE_LOOKUP]
(@gift_code VARCHAR(30))
AS
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
---- ===================================================================
---- Author:		Aileen Duffy-Brown, Tessitura Network Tech Services
---- Create date:   12/17/2018
---- Description:	Lookup tool for gifted entitlements.
---- Zen Desk Ticket #15297 for the Museum of Science
---- ===================================================================
BEGIN
    --DECLARE @gift_code VARCHAR(15) = 'f60a-0354-a995'
    --DECLARE @gift_code VARCHAR(15) = '6795-0363-abbf'
    --DECLARE @gift_code VARCHAR(15) = '6569-0364-9300'
    --DECLARE @gift_code VARCHAR(15) = '6569-0364-9300'

    --find gift codes used via NScan
    DECLARE @nscan TABLE
    (
        scan_str VARCHAR(30),
        device_name VARCHAR(20),
        nscan_perf_no INT,
        create_dt DATETIME,
        ticket_ok CHAR(1),
        ticket_msg VARCHAR(100)
    );
    INSERT INTO @nscan
    (
        scan_str,
        device_name,
        nscan_perf_no,
        create_dt,
        ticket_ok,
        ticket_msg
    )
    SELECT scan_str,
           device_name,
           perf_no,
           create_dt,
           ticket_ok,
           ticket_msg
    FROM T_NSCAN_EVENT_CONTROL
    WHERE scan_str = @gift_code;

    --SELECT *
    --FROM @nscan;

    --find gift codes used in an order
    DECLARE @used_gifts TABLE
    (
        giver_customer_no INT,
        gift_customer_no INT,
        gift_code VARCHAR(255),
        group_gift_code VARCHAR(255),
        gift_no INT,
        init_dt DATETIME,
        expr_dt DATETIME,
        last_update_dt DATETIME,
        last_updated_by VARCHAR(8),
        ltx_cust_entitlement_id INT,
        entitlement_no INT,
        id_key INT,
        num_items INT,
        g_status VARCHAR(10),
        set_no INT,
        order_no INT,
        sli_no INT,
        perf_no INT,
        num_used INT,
        pass_status VARCHAR(10),
        price_type_desc VARCHAR(30),
        short_desc VARCHAR(10)
    );
    INSERT INTO @used_gifts
    (
        giver_customer_no,
        gift_customer_no,
        gift_code,
        group_gift_code,
        gift_no,
        init_dt,
        expr_dt,
        last_update_dt,
        last_updated_by,
        ltx_cust_entitlement_id,
        entitlement_no,
        id_key,
        num_items,
        g_status,
        set_no,
        order_no,
        sli_no,
        perf_no,
        num_used,
        pass_status,
        price_type_desc,
        short_desc
    )
    SELECT b.customer_no,
           b.gift_customer_no,
           b.gift_code,
           b.group_gift_code,
           b.id_key,
           b.init_dt,
           b.expr_dt,
           b.last_update_dt,
           b.last_updated_by,
           b.ltx_cust_entitlement_id,
           b.entitlement_no,
           b.id_key,
           b.num_items,
           b.gift_status,
           a1.set_no,
           c.order_no,
           c.sli_no,
           c.perf_no,
           CASE
               WHEN c.order_no IS NULL THEN
                   0
               ELSE
                   c.num_used
           END AS num_used,
           d.pass_status,
           d.price_type_desc,
           d.short_desc
    FROM LTX_CUST_ENTITLEMENT AS a1
        INNER JOIN LT_ENTITLEMENT_GIFT AS b
            ON a1.id_key = b.ltx_cust_entitlement_id
        INNER JOIN LTX_CUST_ENTITLEMENT AS a2
            ON a2.gift_no = b.id_key
        INNER JOIN LTX_CUST_ORDER_ENTITLEMENT AS c
            ON c.ltx_cust_entitlement_id = a2.id_key
        LEFT OUTER JOIN LV_ORDERS_WITH_ENTITLEMENTS AS d
            ON c.order_no = d.order_no
               AND c.sli_no = d.sli_no
    WHERE (
              b.gift_code = @gift_code
              OR b.group_gift_code = @gift_code
          );

    --SELECT *
    --FROM @used_gifts;

    --**********************************************

    --find gift codes that are only in created and not used in an order
    DECLARE @created_gifts TABLE
    (
        giver_customer_no INT,
        gift_customer_no INT,
        gift_code VARCHAR(255),
        group_gift_code VARCHAR(255),
        gift_no INT,
        init_dt DATETIME,
        expr_dt DATETIME,
        last_update_dt DATETIME,
        last_updated_by VARCHAR(8),
        ltx_cust_entitlement_id INT,
        entitlement_no INT,
        id_key INT,
        num_items INT,
        num_used INT,
        g_status VARCHAR(10),
        set_no INT
    );
    INSERT INTO @created_gifts
    (
        giver_customer_no,
        gift_customer_no,
        gift_code,
        group_gift_code,
        gift_no,
        init_dt,
        expr_dt,
        last_update_dt,
        last_updated_by,
        ltx_cust_entitlement_id,
        entitlement_no,
        id_key,
        num_items,
        num_used,
        g_status,
        set_no
    )
    SELECT a.customer_no,
           a.gift_customer_no,
           a.gift_code,
           a.group_gift_code,
           a.id_key,
           a.init_dt,
           a.expr_dt,
           a.last_update_dt,
           a.last_updated_by,
           a.ltx_cust_entitlement_id,
           a.entitlement_no,
           a.id_key,
           a.num_items,
           NULL,
           a.gift_status,
           b.set_no
    FROM LT_ENTITLEMENT_GIFT AS a
        LEFT OUTER JOIN LTX_CUST_ENTITLEMENT AS b
            ON a.id_key = b.gift_no
    WHERE (
              gift_code = @gift_code
              OR group_gift_code = @gift_code
          );

    --SELECT *
    --FROM @created_gifts;

    --**********************************************
    --Final Select   
    SELECT a.order_no,
           a.sli_no,
           a.perf_no,
           a.giver_customer_no,
           a.gift_customer_no,
           a.gift_code,
           a.group_gift_code,
           a.gift_no,
           a.init_dt,
           a.expr_dt,
           a.last_update_dt,
           a.last_updated_by,
           a.ltx_cust_entitlement_id,
           a.entitlement_no,
           a.id_key,
           a.num_items,
           a.num_used,
           a.set_no,
           n1.display_name AS gift_giver,
           n2.display_name AS giftee,
           e.entitlement_desc,
           e.entitlement_code,
           e.is_adhoc,
           e.transferrable,
           e.allow_nscan,
           f.description AS tkw_description,
           CASE
               WHEN g.name IS NULL THEN
                   'None'
               ELSE
                   g.name
           END AS set_name,
           h.scan_str,
           h.device_name,
           h.nscan_perf_no,
           h.create_dt,
           h.ticket_ok,
           h.ticket_msg,
           CASE
               WHEN h.scan_str IS NULL THEN
                   'None'
               ELSE
                   'Scanned'
           END AS nscan_status,
           CASE
               WHEN a.g_status = 'C' THEN
                   'Created'
               WHEN a.g_status = 'A' THEN
                   'Accepted'
               ELSE
                   'Other'
           END AS gift_status
    FROM @used_gifts AS a
        LEFT OUTER JOIN [dbo].FT_CONSTITUENT_DISPLAY_NAME() AS n1
            ON a.giver_customer_no = n1.customer_no
        LEFT OUTER JOIN [dbo].FT_CONSTITUENT_DISPLAY_NAME() AS n2
            ON a.gift_customer_no = n2.customer_no
        LEFT OUTER JOIN LTR_ENTITLEMENT AS e
            ON e.entitlement_no = a.entitlement_no
        LEFT OUTER JOIN TR_TKW AS f
            ON f.id = e.ent_tkw_id
        LEFT OUTER JOIN LT_ENTITLEMENT_SET AS g
            ON g.set_no = a.set_no
        LEFT OUTER JOIN @nscan AS h
            ON h.scan_str = a.gift_code
    UNION ALL
    SELECT NULL AS order_no,
           NULL AS sli_no,
           NULL AS perf_no,
           a.giver_customer_no,
           a.gift_customer_no,
           a.gift_code,
           a.group_gift_code,
           a.gift_no,
           a.init_dt,
           a.expr_dt,
           a.last_update_dt,
           a.last_updated_by,
           a.ltx_cust_entitlement_id,
           a.entitlement_no,
           a.id_key,
           a.num_items,
           NULL AS num_used,
           a.set_no,
           n1.display_name AS gift_giver,
           n2.display_name AS giftee,
           e.entitlement_desc,
           e.entitlement_code,
           e.is_adhoc,
           e.transferrable,
           e.allow_nscan,
           f.description AS tkw_description,
           CASE
               WHEN g.name IS NULL THEN
                   'None'
               ELSE
                   g.name
           END AS set_name,
           h.scan_str,
           h.device_name,
           h.nscan_perf_no,
           h.create_dt,
           h.ticket_ok,
           h.ticket_msg,
           CASE
               WHEN h.scan_str IS NULL THEN
                   'None'
               ELSE
                   'Scanned'
           END AS nscan_status,
           CASE
               WHEN a.g_status = 'C' THEN
                   'Created'
               WHEN a.g_status = 'A' THEN
                   'Accepted'
               ELSE
                   'Other'
           END AS gift_status
    FROM @created_gifts AS a
        LEFT OUTER JOIN [dbo].FT_CONSTITUENT_DISPLAY_NAME() AS n1
            ON a.giver_customer_no = n1.customer_no
        LEFT OUTER JOIN [dbo].FT_CONSTITUENT_DISPLAY_NAME() AS n2
            ON a.gift_customer_no = n2.customer_no
        LEFT OUTER JOIN LTR_ENTITLEMENT AS e
            ON e.entitlement_no = a.entitlement_no
        LEFT OUTER JOIN TR_TKW AS f
            ON f.id = e.ent_tkw_id
        LEFT OUTER JOIN LT_ENTITLEMENT_SET AS g
            ON g.set_no = a.set_no
        LEFT OUTER JOIN @nscan AS h
            ON h.scan_str = a.gift_code;
END;

GO


