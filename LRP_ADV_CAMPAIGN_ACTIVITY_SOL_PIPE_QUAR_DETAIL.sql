USE impresario
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_ADV_CAMPAIGN_ACTIVITY_SOL_PIPE_QUAR_DETAIL]') AND type IN (N'P', N'PC'))
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_ADV_CAMPAIGN_ACTIVITY_SOL_PIPE_QUAR_DETAIL] AS' 
GO

EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LRP_ADV_CAMPAIGN_ACTIVITY_SOL_PIPE_QUAR_DETAIL] TO [impusers], [tessitura_app]'
GO

/*
        LRP_ADV_CAMPAIGN_ACTIVITY_SOL_PIPE_QUAR_DETAIL
        Pulls information for goals, numbers, and YTD percentages for all step activity, vists, ask/yield amounts, and actual contributions taken in.

        NOTE: Contribution amounts are based on what's in The Plan record under cont_amount
*/
ALTER PROCEDURE [dbo].[LRP_ADV_CAMPAIGN_ACTIVITY_SOL_PIPE_QUAR_DETAIL]
        @fiscal_year INT = NULL,
        @workers_str VARCHAR(4000) = NULL,
        @min_ask_amount DECIMAL(18,2) = NULL
WITH RECOMPILE AS BEGIN

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

    /*  Procedure Variables  */

        DECLARE @fiscal_start_dt DATETIME = NULL, @fiscal_end_dt DATETIME = NULL
        DECLARE @current_dt DATETIME = GETDATE()

        DECLARE @worker_list TABLE ([worker_no] INT NOT NULL DEFAULT (0))

    /*  Templorary Tables  */
        
        IF OBJECT_ID('tempdb..#plan_data') IS NOT NULL DROP TABLE #plan_data

        CREATE TABLE #plan_data ([plan_no] INT NOT NULL DEFAULT (0),
                                 [customer_no] INT NOT NULL DEFAULT (0),
                                 [customer_name] VARCHAR(160) NOT NULL DEFAULT (''),
                                 [customer_sort] VARCHAR(75) NOT NULL DEFAULT (''),
                                 [worker_customer_no] INT NOT NULL DEFAULT (0),
                                 [worker_name] VARCHAR(75) NOT NULL DEFAULT (0),
                                 [worker_initials] VARCHAR(75) NOT NULL DEFAULT (0),
                                 [worker_inactive] CHAR(1) NOT NULL DEFAULT ('N'),
                                 [start_dt] DATETIME NULL,
                                 [complete_by_dt] DATETIME NULL,
                                 [complete_by_quarter] VARCHAR(2) NOT NULL DEFAULT (''),
                                 [ask_amount] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                 [goal_amount] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                 [plan_amount] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                 [contribution_amount] DECIMAL (18,2) NOT NULL DEFAULT (0.0),
                                 [plan_status_id] INT NOT NULL DEFAULT (0),
                                 [plan_status] VARCHAR(50) NOT NULL DEFAULT (''),
                                 [plan_probability] DECIMAL (18,2) NOT NULL DEFAULT (0.0),
                                 [Plan_probability_display] VARCHAR(10) NOT NULL DEFAULT (''),
                                 [plan_probability_sort] CHAR(3) NOT NULL DEFAULT ('EEE'),
                                 [cont_designation] INT NOT NULL DEFAULT (0),
                                 [cont_desegnation_name] VARCHAR(30) NOT NULL DEFAULT (''),
                                 [plan_title] VARCHAR(255) NOT NULL DEFAULT (''))

    /*  Check Parameters  */

        IF @fiscal_year IS NULL SELECT @fiscal_year = dbo.LF_GetFiscalYear(GETDATE())
        
        SELECT @fiscal_start_dt = [dbo].[LF_GetFiscalYearStartDt](@fiscal_year)
        SELECT @fiscal_end_dt = [dbo].[LF_GetFiscalYearEndDt](@fiscal_year)

        IF @current_dt > @fiscal_end_dt SELECT @current_dt = @fiscal_end_dt

         IF ISNULL(@workers_str,'') = ''
            INSERT INTO @worker_list ([worker_no])
            SELECT ISNULL([worker_customer_no], 0)
            FROM [dbo].[LV_CAMPAIGN_ACTIVITY_WORKERS] 
            WHERE [inactive] = 'N'
              AND ISNULL([worker_customer_no], 0) > 0
        ELSE
            INSERT INTO @worker_list ([worker_no])
            SELECT ISNULL(TRY_CAST([Element] AS INTEGER),0) 
            FROM [dbo].[FT_SPLIT_LIST](REPLACE(@workers_str,'"',''),',')
            WHERE ISNULL(TRY_CAST([Element] AS INTEGER),0) > 0

        SELECT @min_ask_amount = ISNULL(@min_ask_amount, 0)

    /*  Get Plan Raw Data  */

        INSERT INTO [#plan_data] ([plan_no], [customer_no], [customer_name], [customer_sort],  [worker_customer_no], [worker_name], 
                                  [worker_initials], [worker_inactive], [start_dt], [complete_by_dt], [complete_by_quarter], [ask_amount], 
                                  [goal_amount], [contribution_amount], [plan_status_id], [plan_status], [plan_probability],
                                  [cont_designation], [cont_desegnation_name], [plan_title])
        SELECT pln.[plan_no],
               pln.[customer_no],
               nam.[display_name],
               nam.[sort_name],
               xpp.[customer_no],
               wrk.[worker_name],
               wrk.[worker_initials],
               wrk.[inactive],
               pln.[start_dt],
               pln.[complete_by_dt],
               'Q' + CAST([dbo].[LF_GetFiscalQuarter](pln.[complete_by_dt]) AS CHAR(1)),
               ISNULL(pln.[ask_amt],0.0),
               ISNULL(pln.[goal_amt],0.0),
               ISNULL(pln.[cont_amt],0.0),
               pln.[status],
               sta.[description],
               pln.[probability],
               pln.[cont_designation],
               dsg.[description],
               ISNULL(pln.[custom_1],'') AS Title
        FROM [dbo].[T_PLAN] AS pln
             LEFT OUTER JOIN [dbo].[TX_CUST_PLAN] AS xpp ON xpp.[plan_no] = pln.[plan_no] AND xpp.[role_no] = 1
             INNER JOIN @worker_list AS lis ON lis.[worker_no] = xpp.[customer_no]
             INNER JOIN [dbo].[LV_CAMPAIGN_ACTIVITY_WORKERS] AS wrk ON wrk.[worker_customer_no] = xpp.[customer_no]
             LEFT OUTER JOIN [dbo].[TR_PLAN_STATUS] AS sta ON sta.[id] = pln.[status]
             LEFT OUTER JOIN [dbo].[TR_CONT_DESIGNATION] AS dsg ON dsg.[id] = pln.[cont_designation]
             LEFT OUTER JOIN [dbo].[FT_CONSTITUENT_DISPLAY_NAME]() AS nam ON nam.[customer_no] = pln.[customer_no]
        WHERE pln.[complete_by_dt] BETWEEN @fiscal_start_dt AND @fiscal_end_dt
          AND pln.[type] = 7    --Pipeline
          AND pln.[status] IN (4, 22, 23)
          AND pln.[ask_amt] > @min_ask_amount;

     


    /*  Update Probability  */

        UPDATE [#plan_data]
        SET [plan_probability_display] = CASE WHEN [plan_probability] >= 0.76 THEN '100%'
                                              WHEN [plan_probability] BETWEEN 0.51 AND 0.77 THEN '75%'
                                              WHEN [plan_probability] BETWEEN 0.26 AND 0.50 THEN '50%'
                                              WHEN [plan_probability] BETWEEN 0.01 AND 0.25 THEN '25%'
                                              WHEN [plan_probability] = 0.00 THEN '0%'
                                              ELSE 'ZZZ' END,
            [plan_probability_sort] = CASE WHEN [plan_probability] >= 0.76 THEN 'AAA'
                                           WHEN [plan_probability] BETWEEN 0.51 AND 0.77 THEN 'BBB'
                                           WHEN [plan_probability] BETWEEN 0.26 AND 0.50 THEN 'CCC'
                                           WHEN [plan_probability] BETWEEN 0.01 AND 0.25 THEN 'DDD'
                                           WHEN [plan_probability] = 0.00 THEN 'EEE'
                                           ELSE 'ZZZ' END

    FINISHED:

        /*  Pull final list of plans to pass to report  */

            SELECT [complete_by_quarter], 
                   [plan_no], 
                   [customer_no], 
                   [customer_name], 
                   [customer_sort],  
                   [worker_customer_no], 
                   [worker_name], 
                   [worker_initials], 
                   [worker_inactive], 
                   [start_dt], 
                   [complete_by_dt], 
                   [ask_amount], 
                   [goal_amount], 
                   [contribution_amount], 
                   [plan_status_id], 
                   [plan_status], 
                   [plan_probability],
                   [Plan_probability_display],
                   [plan_probability_sort],
                   [cont_designation], 
                   [cont_desegnation_name], 
                   [plan_title],
                   1 AS [record_count],
                   @min_ask_amount AS [minimum_ask]
            FROM [#plan_data]
            ORDER BY [complete_by_quarter],
                     [ask_amount] DESC,
                     [goal_amount] DESC,
                     [customer_sort]

    DONE:

END
GO

--EXECUTE [dbo].[LRP_ADV_CAMPAIGN_ACTIVITY_SOL_PIPE_QUAR_DETAIL] @fiscal_year = NULL, @workers_str = NULL, @min_ask_amount = NULL  



    --SELECT * FROM T_PLAN WHERE [type] = 7 ORDER BY [complete_by_dt] DESC

