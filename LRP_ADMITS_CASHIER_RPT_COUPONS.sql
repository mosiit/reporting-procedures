USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LRP_ADMITS_CASHIER_RPT_COUPONS]    Script Date: 5/9/2016 4:40:06 PM ******/
DROP PROCEDURE [dbo].[LRP_ADMITS_CASHIER_RPT_COUPONS]
GO

/****** Object:  StoredProcedure [dbo].[LRP_ADMITS_CASHIER_RPT_COUPONS]    Script Date: 5/9/2016 4:40:06 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE  [dbo].[LRP_ADMITS_CASHIER_RPT_COUPONS]
       @rpt_dt DATETIME,
       @rpt_operator VARCHAR(8),
	   @include_library CHAR(1)
AS
	BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	--DECLARE	@cnt_passes	INT

    DECLARE @coupon_table TABLE ([coupon_date] CHAR(10),
                                 [coupon_operator] VARCHAR(8),
                                 [operator_first] VARCHAR(50),
                                 [operator_last] VARCHAR(50),
                                 [operator_location] VARCHAR(50),
                                 [price_type_name] VARCHAR(30),
                                 [comp_code_name_short] VARCHAR(20),
                                 [comp_code_name] VARCHAR(30),
                                 [total_coupons] INT,
                                 [total_transactions] INT,
                                 [elapsed_time] INT,
                                 [rpt_message] VARCHAR(100))

	INSERT INTO @coupon_table
		EXEC [dbo].[LP_RPT_CASHOUT_COUPONS] @rpt_dt, @rpt_operator

	--SELECT	@cnt_passes = COUNT([coupon_date]) FROM @coupon_table

	IF @include_library = 'Y'
		BEGIN
			IF (SELECT COUNT([total_coupons]) FROM @coupon_table WHERE [comp_code_name_short] LIKE 'LP%') = 0 OR (SELECT COUNT([total_coupons]) FROM @coupon_table WHERE [comp_code_name_short] LIKE 'LP%') = 0
				SELECT '' AS [coupon_date], '' AS [coupon_operator], '' AS [operator_first], '' AS [operator_last], '' AS [price_type_name], '' AS [comp_code_name_short], 'No library passes' AS [comp_code_name], 0 AS [total_coupons], 0 AS [total_tranansactions], 0 AS [nbr_of_passes]
			ELSE
				SELECT [coupon_date], [coupon_operator], [operator_first], [operator_last], [operator_location], [price_type_name], [comp_code_name_short], [comp_code_name], [total_coupons], [total_transactions] AS [nbr_of_passes] FROM @coupon_table WHERE [comp_code_name_short] LIKE 'LP%'
		END
	ELSE
		BEGIN
			IF (SELECT COUNT([total_coupons]) FROM @coupon_table WHERE [comp_code_name_short] NOT LIKE 'LP%') = 0 OR (SELECT COUNT([total_coupons]) FROM @coupon_table WHERE [comp_code_name_short] NOT LIKE 'LP%') = 0
				SELECT '' AS [coupon_date], '' AS [coupon_operator], '' AS [operator_first], '' AS [operator_last], '' AS [price_type_name], '' AS [comp_code_name_short], 'No coupons/pass codes' AS [comp_code_name], 0 AS [total_coupons], 0 AS [nbr_of_passes]
			ELSE
				SELECT [coupon_date], [coupon_operator], [operator_first], [operator_last], [operator_location], [price_type_name], [comp_code_name_short], [comp_code_name], [total_coupons], [total_transactions] AS [nbr_of_passes] FROM @coupon_table WHERE [comp_code_name_short] NOT LIKE 'LP%'
		END
	END
GO

GRANT EXECUTE ON [dbo].[LRP_ADMITS_CASHIER_RPT_COUPONS] TO impusers
GO
