USE [impresario]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALter PROCEDURE [dbo].[LRP_UNDERAGE_EMAIL_ADDRESSES]
	@MinimumAge INT
AS
-- ==============================================================================================================
--DSJ 2016/04/15 
-- Pass in an age to find all customers under that age for whom we have an email address. 
-- ==============================================================================================================

BEGIN


SELECT cust.customer_no, cust.fname AS [First Name], cust.lname AS [Last Name], piv.Birthdate_1 AS [Birthdate], piv.[Birthdate Verification], 
	email.Address, email.primary_ind AS [Primary Email], DATEDIFF(YEAR, birthdate_1, GETDATE()) AS Age
FROM
(
	SELECT kwrd.customer_no, ky.description, kwrd.key_value
	FROM dbo.TX_CUST_KEYWORD kwrd	
		INNER JOIN dbo.T_KEYWORD ky
			ON kwrd.keyword_no = ky.keyword_no
	WHERE ky.description IN ('Birthdate_1', 'Birthdate Verification')
) AS x
PIVOT
(
	MAX(key_value) FOR description IN (Birthdate_1, [Birthdate Verification])
) AS piv
INNER JOIN dbo.T_CUSTOMER cust
	ON piv.customer_no = cust.customer_no
INNER JOIN dbo.T_EADDRESS email
	ON cust.customer_no = email.customer_no
	AND email.inactive = 'N'
	AND email.eaddress_type IN (1,4) -- (Primary Email Address, Secondary Email Address)
WHERE DATEDIFF(YEAR, birthdate_1, GETDATE()) < @MinimumAge
ORDER BY age ASC, cust.lname, fname, [Primary Email] desc

END
