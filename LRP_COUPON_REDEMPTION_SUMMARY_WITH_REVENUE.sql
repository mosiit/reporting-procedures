USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_COUPON_REDEMPTION_SUMMARY_WITH_REVENUE]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LRP_COUPON_REDEMPTION_SUMMARY_WITH_REVENUE]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LRP_COUPON_REDEMPTION_SUMMARY_WITH_REVENUE]
        @report_start_dt datetime = Null, 
        @report_end_dt datetime = Null,
        @group_by varchar(20) = 'Title'       --Title, Production, or Performance      
AS BEGIN

        /*  If null passed to either date parameter, set to yesterday.
            IF null passed to group by parameter, set to title  */

            SELECT @report_start_dt = IsNull(@report_start_dt, dateadd(day, -1, getdate()))
            SELECT @report_end_dt = IsNUll(@report_end_dt, dateadd(day, -1, getdate()))
            SELECT @group_by = IsNull(@group_by, 'Title')

    /*  Will be pulling performance data based on a text date field in the yyyy/MM/dd format.  
        Convert @report_start_date and @report_end_date now to make it easier. */

        SELECT @report_start_dt = convert(char(10), @report_start_dt, 111) + ' 00:00:00',
               @report_end_dt = convert(char(10), @report_end_dt, 111) + ' 23:59:59.957'
               
                       
                IF OBJECT_ID('tempdb..#price_table') IS NOT NULL DROP TABLE [#price_table]

                CREATE TABLE #price_table ([performance_no] int, [performance_dt] datetime, [performance_date] char(10), [performance_zone] int, [performance_time] varchar(8), 
                                           [title_no] int, [title_name] varchar(30), [production_no] int, [production_name] varchar(30), [Production_name_long] varchar(100),
                                           [lowest_price] decimal(18,2), [highest_price] decimal(18,2), [average_price] decimal(18,2), [ref_price] decimal(18,2))

       
    /*  The #final_table temporary table contains the combined information from #performance_table and #coupon_redemption_table.
        The data is aggregated so that there should be a single row for each performance/coupon code combination.  */

               IF OBJECT_ID('tempdb..#final_table') IS NOT NULL DROP TABLE [#final_table]

                CREATE TABLE #final_table ([report_message] varchar(100), [perf_no] int, [zone_no] int, [title_name] varchar(30), [performance_date] char(10), 
                                           [performance_time] varchar(8), [production_name] varchar(30), [production_name_long] varchar(255), [price_type] int, [price_type_name] varchar(30), 
                                           [comp_code] int, [comp_code_name] varchar(30), [sale_admission] int, [scan_admission] int, [due_amt] decimal(18,2),
                                           [paid_amt] decimal(18,2), [ref_price] decimal(18,2), [lost_revenue] decimal(18,2))

               CREATE NONCLUSTERED INDEX [ix_final_table_title_name] ON #final_table ([title_name] ASC) ON [PRIMARY]

               CREATE NONCLUSTERED INDEX [ix_final_table_production_name_long] ON #final_table ([production_name_long] ASC) ON [PRIMARY]

        INSERT INTO #price_table
        SELECT prf.[performance_no], prf.[performance_dt], prf.[performance_date], prf.[performance_zone], prf.[performance_time], prf.[title_no], prf.[title_name], prf.[production_no],
               prf.[production_name], prf.[Production_name_long], min(pri.[current_price]), max(pri.[current_price]), avg(pri.[current_price]), 0.00
        FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] as prf (NOLOCK)
             LEFT OUTER JOIN [dbo].[LV_CURRENT_PRICES] as pri (NOLOCK) ON pri.[perf_no] = prf.[performance_no] and pri.[zone_no] = prf.[performance_zone]
        WHERE prf.[performance_dt] between @report_start_dt and @report_end_dt and prf.[performance_zone] is not null and pri.[current_price] <> 0.00
        GROUP BY prf.[performance_no], prf.[performance_dt], prf.[performance_date], prf.[performance_zone], prf.[performance_time], prf.[title_no], prf.[title_name], prf.[production_no],
                 prf.[production_name], prf.[Production_name_long]

        UPDATE #price_table SET [ref_price] = [highest_price]

        INSERT INTO #final_table
        SELECT '', his.[perf_no], his.[zone_no], his.[title_name], his.[performance_date], his.[performance_time], his.[production_name], his.[production_name_long],
               his.[price_type], his.[price_type_name], his.[comp_code], his.[comp_code_name], his.[sale_total], his.[scan_admission_total], 
               his.[due_amt], his.[paid_amt], pri.[ref_price], 0.00
        FROM [dbo].[LT_HISTORY_TICKET] as his (NOLOCK)
             LEFT OUTER JOIN [#price_table] as pri ON pri.[performance_no] = his.[perf_no] and pri.[performance_zone] = his.[zone_no]
        WHERE his.[performance_date] between convert(char(10),@report_start_dt,111) and convert(char(10),@report_end_dt,111) and his.[comp_code] <> 0

        UPDATE [#final_table] SET [ref_price] = 0.00 WHERE [ref_price] is null
        
        UPDATE [#final_table] SET [lost_revenue] = (([ref_price] - [paid_amt]) * -1)
        
    DONE:

    /*  If not records found, add single record with message saying no records were found.  */
        
            IF not exists (SELECT * FROM #final_table)
                INSERT INTO #final_table VALUES ('No records found for criteria entered.', 0, 0, '', null, '', '', '', 0, '', 0, '', 0, 0, 0, 0, 0, 0)

    /*  Select final aggregated record set
        If grouping by performance, include performance date/time and title to produce a single record for each performance/discount code combination.
        If grouping by production, leave out date/time but leave in title to produce a single record for each production/discount code combination.
        If grouping by title (venue), leave out date/time and title to produce a single record for each title/discount code combination (default if group_by is not known).  */

        IF @group_by = 'Performance'

            SELECT [report_message], [perf_no], [zone_no], [title_name], convert(datetime,[performance_date]) as 'performance_dt', [performance_time], [production_name], [production_name_long], 
                   [price_type], [price_type_name], [comp_code], [comp_code_name], sum([sale_admission]) as 'sale_total', sum([scan_admission]) as 'scan_admission_total', 
                   sum([paid_amt]) as 'paid_amount', sum([ref_price]) as 'reference_price', sum([lost_revenue]) as 'lost_revenue'
            FROM #final_table
            GROUP BY [report_message], [perf_no], [zone_no], [title_name], [performance_date], [performance_time], [production_name_long], 
                     [price_type], [price_type_name], [comp_code], [comp_code_name]
            ORDER BY [title_name], [performance_date], [performance_time], [production_name_long], [comp_code_name]

        ELSE IF @group_by = 'Production'

            SELECT [report_message], 0 as 'perf_no', 0 as 'zone_no', [title_name], null as 'performance_dt', '' as 'performance_time', [production_name], [production_name_long], 
                   [price_type], [price_type_name], [comp_code], [comp_code_name], sum([sale_admission]) as 'sale_total', sum([scan_admission]) as 'scan_admission_total', 
                   sum([paid_amt]) as 'paid_amount', sum([ref_price]) as 'reference_price', sum([lost_revenue]) as 'lost_revenue'
            FROM #final_table
            GROUP BY [report_message], [title_name], [production_name], [production_name_long], [price_type], [price_type_name], [comp_code], [comp_code_name]
            ORDER BY [title_name], [production_name_long], [comp_code_name]

        ELSE

            SELECT [report_message], 0 as 'perf_no', 0 as 'zone_no', [title_name], null as 'performance_dt', '' as 'performance_time', '' as 'production_name', convert(varchar(255),'') as 'production_name_long', 
                   [price_type], [price_type_name], [comp_code], [comp_code_name], sum([sale_admission]) as 'sale_total', sum([scan_admission]) as 'scan_admission_total', 
                   sum([paid_amt]) as 'paid_amount', sum([ref_price]) as 'reference_price', sum([lost_revenue]) as 'lost_revenue'
            FROM #final_table
            GROUP BY [report_message], [title_name], [price_type], [price_type_name], [comp_code], [comp_code_name]
            ORDER BY [title_name], [comp_code_name]


    /*  Destroy temporary tables  */

    IF OBJECT_ID('tempdb..#final_table') IS NOT NULL DROP TABLE [#final_table]

    IF OBJECT_ID('tempdb..#price_table') IS NOT NULL DROP TABLE [#price_table]

END
GO

GRANT EXECUTE ON [dbo].[LRP_COUPON_REDEMPTION_SUMMARY_WITH_REVENUE] TO impusers
GO

EXECUTE [dbo].[LRP_COUPON_REDEMPTION_SUMMARY_WITH_REVENUE] '6-1-2016', '6-30-2016', 'Production'
