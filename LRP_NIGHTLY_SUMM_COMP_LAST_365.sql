USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LRP_NIGHTLY_SUMM_COMP_LAST_365]    Script Date: 12/14/2020 2:26:36 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



ALTER PROCEDURE [dbo].[LRP_NIGHTLY_SUMM_COMP_LAST_365]
    @section VARCHAR(300),
	@sortOrder INT,
	@runBy	VARCHAR(300)
AS BEGIN

---- ===============================================================
---- M. Sherwood, 8/22/2020 - Total From Last 365 Days
---- ===============================================================

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

        --SET @section = 'Last 365 Total'
        --SET	@sortOrder = 99
        --SET @runBy = 'msherw00'

	    DECLARE @tblPassOne	TABLE ([customer_no] INT,
                                   [value] MONEY)

	    DECLARE @tblPassTwo TABLE ([customer_no] INT,
                                   [value] MONEY)

        DECLARE @tblPassThree TABLE ([customer_no] INT,
                                     [value] MONEY)

        DECLARE @tblPassFour TABLE ([customer_no] INT,
                                    [value] MONEY)

	    DECLARE @tblPassAll TABLE ([customer_no] INT,
                                   [value] MONEY)


    /*  Pass One and Two use adjusted code from LRP_NIGHTLY_SUMM_COMP_LIFE_TOTAL  */

	    INSERT INTO @tblPassOne ([customer_no], [value])
        SELECT c.[customer_no],
               SUM(ISNULL(c.[cont_amt], 0))
        FROM [dbo].[T_CONTRIBUTION] AS c
             INNER JOIN [dbo].[LTR_FUND_DETAIL] AS f ON c.[fund_no] = f.[fund_no]
		--2020/12/14, H. Sheridan.  Service Now ticket SCTASK0001710 - join to campaign table
		JOIN	T_CAMPAIGN cam
		ON		cam.campaign_no = c.campaign_no
        WHERE CAST(c.[cont_dt] AS DATE) BETWEEN CAST(DATEADD(DAY,-365,GETDATE()) AS DATE) AND CAST(GETDATE() AS DATE)
          AND c.[cont_type] IN ('G', 'P')
          AND c.[custom_1] IN ('(none)', 'Matching Gift Credit')
          AND f.[acct_grp_no] <> '3'
		  --2020/12/14, H. Sheridan.  Service Now ticket SCTASK0001710 - add criteria for campaign category
		  AND	cam.category NOT IN (8,9)

        GROUP BY c.[customer_no]
        
        INSERT INTO @tblPassTwo ([customer_no], [value])
        SELECT i.[customer_no],
               SUM(ISNULL(c.[cont_amt],0))
        FROM [dbo].[T_CONTRIBUTION] AS c
             INNER JOIN [dbo].[T_CREDITEE] AS t ON c.[ref_no] = t.[ref_no]
             INNER JOIN [dbo].[LTR_FUND_DETAIL] AS f ON c.[fund_no] = f.[fund_no]
             INNER JOIN [dbo].[T_CUSTOMER] AS i ON t.[creditee_no] = i.[customer_no]
		--2020/12/14, H. Sheridan.  Service Now ticket SCTASK0001710 - join to campaign table
		JOIN	T_CAMPAIGN cam
		ON		cam.campaign_no = c.campaign_no
        WHERE CAST(c.[cont_dt] AS DATE) BETWEEN CAST(DATEADD(DAY,-365,GETDATE()) AS DATE) AND CAST(GETDATE() AS DATE)
          AND c.[cont_type] IN ('G', 'P')
          AND c.[custom_1] IN ('Primary Soft Credit', 'Matching Gift Credit', 'Stewardship Soft Credit')
          AND t.[creditee_type] IN ('12', '15', '5', '10', '16', '1', '14')
          AND f.[acct_grp_no] <> '3'
		  --2020/12/14, H. Sheridan.  Service Now ticket SCTASK0001710 - add criteria for campaign category
		  AND	cam.category NOT IN (8,9)
        GROUP BY i.[customer_no]


    /*  Pass Three and Four use adjusted code from LRP_NIGHTLY_SUMM_LIFE_CORP_MEM  */

		--2020/12/14, H. Sheridan.  Service Now ticket SCTASK0001710 - no change needed to the next two code blocks
		INSERT INTO @tblPassThree ([customer_no], [value])
        SELECT  c.[customer_no],
                ISNULL(SUM(c.[cont_amt]), 0)
        FROM [dbo].[T_CONTRIBUTION] AS c
             INNER JOIN [dbo].[LTR_FUND_DETAIL] AS f ON c.[fund_no] = f.[fund_no]
             INNER JOIN [dbo].[T_CAMPAIGN] AS g ON c.[campaign_no] = g.[campaign_no]
        WHERE CAST(c.[cont_dt] AS DATE) BETWEEN CAST(DATEADD(DAY,-365,GETDATE()) AS DATE) AND CAST(GETDATE() AS DATE)
          AND c.[cont_type] IN ('G', 'P')
          AND c.[custom_1] IN ('(none)', 'Matching Gift Credit')
          AND f.[acct_grp_no] = '3'
          AND g.[category] = '31'
        GROUP BY c.[customer_no]

        INSERT INTO @tblPassFour ([customer_no], [value])
        SELECT i.[customer_no],
               ISNULL(SUM(c.[cont_amt]), 0)
        FROM [dbo].[T_CONTRIBUTION] AS c
             INNER JOIN [dbo].[T_CREDITEE] AS t ON c.[ref_no] = t.[ref_no]
             INNER JOIN [dbo].[LTR_FUND_DETAIL] AS f ON c.[fund_no] = f.[fund_no]
             INNER JOIN [dbo].[T_CAMPAIGN] AS g ON c.[campaign_no] = g.[campaign_no]
             INNER JOIN [dbo].[T_CUSTOMER] AS i ON t.[creditee_no] = i.[customer_no]
        WHERE CAST(c.[cont_dt] AS DATE) BETWEEN CAST(DATEADD(DAY,-365,GETDATE()) AS DATE) AND CAST(GETDATE() AS DATE)
          AND c.[cont_type] IN ('G', 'P')
          AND c.[custom_1] IN ('Primary Soft Credit', 'Matching Gift Credit', 'Stewardship Soft Credit')
          AND t.[creditee_type] IN ('12', '15', '5', '10', '16', '1', '14')
          AND f.[acct_grp_no] = '3'
          AND g.[category] = '31'
        GROUP BY i.[customer_no]

        --select 'debug 1', * from @tblPassOne
        --select 'debug 1', * from @tblPassTwo
        --select 'debug 1', * from @tblPassThree
        --select 'debug 1', * from @tblPassFour

    	INSERT INTO @tblPassAll ([customer_no],[value])
    	SELECT [customer_no],
               [value]
        FROM @tblPassOne

	    INSERT INTO @tblPassAll ([customer_no],[value])
	    SELECT [customer_no],
               [value]
        FROM @tblPassTwo

        INSERT INTO @tblPassAll ([customer_no],[value])
	    SELECT [customer_no],
               [value]
        FROM @tblPassThree

        INSERT INTO @tblPassAll ([customer_no],[value])
	    SELECT [customer_no],
               [value]
        FROM @tblPassFour

        --SELECT * FROM @tblPassAll

        DELETE FROM [dbo].[LT_NIGHTLY_SUMMARY] WHERE [sort_order] = @sortOrder

	    INSERT INTO [dbo].[LT_NIGHTLY_SUMMARY] ([customer_no],[section],[value],[apply_date],[sort_order],[created_on],[created_by])
        SELECT [customer_no],
               @section AS [section],
               SUM(ISNULL([value], 0)) AS [value],
               '' AS [apply_date],
               @sortOrder AS [sort_order],
               GETDATE() AS [created_on],
               @runBy AS [created_by]
        FROM @tblPassAll
        GROUP BY [customer_no]

END

GO


