USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_SCHOOL_ATTENDANCE_BY_CITY]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LRP_SCHOOL_ATTENDANCE_BY_CITY]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/*  LRP_SCHOOL_ATTENDANCE_BY_CITY
    Pulls a report of school visits by the city they are from.

    8/1/2018: Got a report that there was a problem with the report.  When tested, found that it ran for a long time and eventually timed out 
              when run for a full year's worth of data.  optimized the code to run faster, removing some that was not needed and adjusting 
              other code to fit better practices.  This code can probably be optimized more (to use fewer temp tables) but at the moment 
              I've got it running for a full year in 10 to 20 seconds so I will leave it as it is for now. -MS  */

CREATE PROCEDURE [dbo].[LRP_SCHOOL_ATTENDANCE_BY_CITY]
        @report_start_date DATETIME = NULL,
        @report_end_date DATETIME = NULL,
        @report_state varchar(50) = 'All',
        @include_partial_paid char(1) = 'Y',
        @count_students_only char(1) = 'N',
        @list_no INT = 0
AS BEGIN

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    SET NOCOUNT ON;

    /*  Procedure variables  */

        DECLARE @rpt_msg varchar(100)       SELECT @rpt_msg = ''

    /* Check Parameters  */

        SELECT @report_start_date = convert(char(10),@report_start_date,111) + ' 00:00:00',
               @report_end_date = convert(char(10),@report_end_date,111) + ' 23:59:59.997'

        SELECT @report_state = IsNull(@report_state, 'All')

        SELECT @include_partial_paid = IsNull(@include_partial_paid, 'Y')
        IF @include_partial_paid <> 'N' SELECT @include_partial_paid = 'Y'

        SELECT @count_students_only = IsNull(@count_students_only, 'N')
        IF @count_students_only <> 'Y' SELECT @count_students_only = 'N'
        

    /*  Create the Temp Tables needed for this procedure  */

        IF OBJECT_ID('tempdb..#perf_raw_data') IS NOT NULL DROP TABLE [#perf_raw_data]

        CREATE TABLE [#perf_raw_data] ([perf_no] int NOT NULL, [perf_code] varchar(10), [perf_type] int, [perf_type_name] varchar(30), [perf_dt] datetime,
                                       [perf_date] char(10), [perf_title_name] varchar(30))

        IF OBJECT_ID('tempdb..PK_PERF') IS NULL
            ALTER TABLE [#perf_raw_data] ADD CONSTRAINT [PK_PERF] PRIMARY KEY CLUSTERED ([perf_no] ASC) ON [PRIMARY]
    
        -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

        IF OBJECT_ID('tempdb..#sub_lineitem_raw_data') IS NOT NULL DROP TABLE [#sub_lineitem_raw_data]

        CREATE TABLE [#sub_lineitem_raw_data] ([sli_no] int NOT NULL, [order_no] int, [perf_no] int, [due_amt] decimal (18,2), [paid_amt] decimal(18,2), [sli_status] int, price_type int)

        IF OBJECT_ID('tempdb..PK_SUB') IS NULL
            ALTER TABLE [#sub_lineitem_raw_data] ADD CONSTRAINT [PK_SUB] PRIMARY KEY CLUSTERED ([sli_no] ASC) ON [PRIMARY]

        -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
        IF OBJECT_ID('tempdb..#order_raw_data') IS NOT NULL DROP TABLE [#order_raw_data]

        CREATE TABLE [#order_raw_data] ([order_no] int NOT NULL, [order_mos] int, [order_mos_name] varchar(30), [customer_no] int, [total_due_amt] decimal(18,2), [total_paid_amt] decimal(18,2), 
                                        [order_dt] datetime, [order_created_by] varchar(10), [order_custom_5] VARCHAR(100))

        IF OBJECT_ID('tempdb..PK_ORDER') IS NULL
            ALTER TABLE [#order_raw_data] ADD CONSTRAINT [PK_ORDER] PRIMARY KEY CLUSTERED ([order_no] ASC) ON [PRIMARY]

        -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

        IF OBJECT_ID('tempdb..#sub_lineitems') IS NOT NULL DROP TABLE [#sub_lineitems]
 
        CREATE TABLE [#sub_lineitems] ([order_no] int, [order_MOS] int, [sli_no] int, [perf_no] int, [perf_dt] datetime, [perf_date] char(10), [due_amt] decimal (18,2), [paid_amt] decimal(18,2), 
                                       [sli_status] int, [perf_type] int)

        -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    
        IF OBJECT_ID('tempdb..#orders') IS NOT NULL DROP TABLE [#orders]

        CREATE TABLE [#orders] ([order_no] int, [customer_no] INT, [total_due_amt] decimal(18,2), [total_paid_amt] decimal(18,2))
    
        -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    
        IF OBJECT_ID('tempdb..#products') IS NOT NULL DROP TABLE [#products]

        CREATE TABLE [#products] ([data_pass] INT, [order_no] int, [customer_no] int, [perf_date] char(10), [price_type] varchar(50), [ExhibitHalls] INT DEFAULT (0), 
                                  [OmniTheater] INT DEFAULT(0), [Planetarium] INT DEFAULT(0), [4DTheater] INT DEFAULT (0), [Butterfly] INT DEFAULT(0))

        -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        
        IF OBJECT_ID('tempdb..#final_table') IS NOT NULL DROP TABLE [#final_table]

        CREATE TABLE [#final_table] ([rpt_message] varchar(25), [order_no] int, [perf_date] char(10), [ExhibitHalls] int, [OmniTheater] int, [Planetarium] int, [4DTheater] int, [Butterfly] int, 
                     [customer_no] int, [last_name] varchar(55), [first_name] varchar(30), [middle_name] varchar(30), [street1] varchar(64), [street2] varchar(64), [city] varchar(30), [state] varchar(20), 
                     [postal_code] varchar(10), [country] varchar(30), [order_audience] VARCHAR(255), [order_lowest_grade] VARCHAR(255))

        -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /*  Check the state parameter  */

        IF @report_state <> 'All' and not exists (SELECT * FROM [dbo].[TR_STATE] WHERE [id] = @report_state) BEGIN
            SELECT @rpt_msg = 'Invalid State Parameter'
            GOTO FINISHED
        END
 
    /* Get performance raw data  */      

        INSERT INTO [#perf_raw_data]
        SELECT prf.[perf_no], prf.[perf_code], prf.[perf_type], typ.[description], prf.[perf_dt], convert(char(10),prf.[perf_dt],111), inv.[description]
        FROM [dbo].[T_PERF] as prf 
             LEFT OUTER JOIN [dbo].[T_PROD_SEASON] as sea ON sea.prod_season_no = prf.[prod_season_no]
             LEFT OUTER JOIN [dbo].[T_PRODUCTION] as pro ON pro.[prod_no] = sea.[prod_no]
             LEFT OUTER JOIN [dbo].[T_TITLE] as ttl ON ttl.[title_no] = pro.[title_no]
             LEFT OUTER JOIN [dbo].[T_INVENTORY] as inv on inv.[inv_no] = ttl.[title_no]
             LEFT OUTER JOIN [dbo].[TR_PERF_TYPE] as typ ON prf.[perf_type] = typ.[id]
        WHERE prf.[perf_dt] between @report_start_date and @report_end_date  

    /*  Get Sub Line Item raw data  */

        INSERT INTO [#sub_lineitem_raw_data]
        SELECT sli.[sli_no], sli.[order_no], sli.[perf_no], sli.[due_amt], sli.[paid_amt], sli.[sli_status], sli.[price_type]
        FROM [dbo].[T_SUB_LINEITEM] as sli
             INNER JOIN [#perf_raw_data] AS prf ON prf.[perf_no] = sli.[perf_no]
        WHERE sli.[sli_status] IN (3, 12)
           OR (@include_partial_paid = 'Y' AND sli.[sli_status] = 2)

    /*  Get Order raw data  */
        
        INSERT INTO [#order_raw_data]
        SELECT DISTINCT ord.[order_no], ord.[MOS], mos.[description], [customer_no], ord.[tot_due_amt], ord.[tot_paid_amt], ord.[order_dt], ord.[created_by], ord.[custom_5]
        FROM [dbo].[T_ORDER] as ord
             INNER JOIN [#sub_lineitem_raw_data] AS sli ON sli.[order_no] = ord.[order_no]
             LEFT OUTER JOIN [dbo].[TR_MOS] as mos ON mos.[id] = ord.[MOS]
         
    /*  Get Products Sold For date range  -  This table will keep a list of all subline items we will be working with
        Looks specifically for subline items where the status is 3 (Seated, Paid) or 12 (Ticketed, Paid)   */
               
        INSERT INTO [#sub_lineitems] 
        SELECT sli.[order_no], ord.[order_mos], sli.[sli_no], sli.[perf_no], prf.[perf_dt], prf.[perf_date], sli.[due_amt], sli.[paid_amt], sli.[sli_status], prf.[perf_type]
        FROM [#sub_lineitem_raw_data] as sli
              LEFT OUTER JOIN #order_raw_data as ord ON ord.[order_no] = sli.[order_no]
              LEFT OUTER JOIN #perf_raw_data as prf ON prf.perf_no = sli.[perf_no]
        WHERE prf.[perf_dt] between @report_start_date and @report_end_date and sli.[sli_status] in (3,12)  

    /*  Get Order Information the products sold - Only include orders with school only products and orders in one of the school modes of sale - 
       This table will keep a list of all orders we will be working with   */

        INSERT INTO [#orders]
        SELECT ord.[order_no], ord.[customer_no], ord.[total_due_amt], ord.[total_paid_amt] 
        FROM [#order_raw_data] AS ord
             INNER JOIN [#sub_lineitems] AS sli ON sli.order_no = ord.order_no AND sli.perf_type = 3
        WHERE @include_partial_paid = 'Y' OR ord.[total_paid_amt] >= ord.[total_due_amt]
        UNION SELECT [order_no], ord.[customer_no], [total_due_amt], [total_paid_amt] 
        FROM [#order_raw_data] AS ord
        WHERE (@include_partial_paid = 'Y' OR ord.[total_paid_amt] >= ord.[total_due_amt]) AND ord.order_mos IN (12,13) --MOS 12 = Schools, MOS 13 = Web School Sales

        IF OBJECT_ID('tempdb..#sub_lineitems') IS NOT NULL DROP TABLE [#sub_lineitems]
                                                                                                                                                 
    /*  Get product information - Exhibit Halls  */
    
        INSERT INTO [#products] ([data_pass],[order_no],[customer_no],[perf_date],[price_type],[ExhibitHalls])
        SELECT 1, 
               sli.[order_no], 
               ord.[customer_no], 
               prf.[perf_date],
               CASE WHEN typ.[description] like '%student%' or typ.[description] like '%child%' or typ.[description] like '%Pass%' THEN 'Students'
                    WHEN typ.[description] like '%chaperone%' or typ.[description] like '%Adult%' or typ.[description] like '%Tour Operator%' THEN 'Chaperones'
                    WHEN typ.[description] like '%teacher%' THEN 'Teachers' ELSE typ.[description] END,
               count(*)
        FROM [#sub_lineitem_raw_data] as sli
             INNER JOIN [#orders] AS ord ON ord.[order_no] = sli.[order_no]
             LEFT OUTER JOIN [#perf_raw_data] as prf ON prf.[perf_no] = sli.[perf_no]
             LEFT OUTER JOIN [dbo].[TR_PRICE_TYPE] as typ ON typ.[id] = sli.[price_type]
        WHERE prf.[perf_title_name] = 'Exhibit Halls'
        GROUP BY sli.[order_no], ord.[customer_no], prf.[perf_date],
                 CASE WHEN typ.[description] like '%student%' or typ.[description] like '%child%' or typ.[description] like '%Pass%' THEN 'Students'
                      WHEN typ.[description] like '%chaperone%' or typ.[description] like '%Adult%' or typ.[description] like '%Tour Operator%' THEN 'Chaperones'
                      WHEN typ.[description] like '%teacher%' THEN 'Teachers' ELSE typ.[description]  END
              
    /*  Get product information - Omni Theater  */

        INSERT INTO [#products] ([data_pass],[order_no],[customer_no],[perf_date],[price_type],[OmniTheater])
        SELECT 1, 
               sli.[order_no], 
               ord.[customer_no], 
               prf.[perf_date],
               CASE WHEN typ.[description] like '%student%' or typ.[description] like '%child%' or typ.[description] like '%Pass%' THEN 'Students'
                    WHEN typ.[description] like '%chaperone%' or typ.[description] like '%Adult%' or typ.[description] like '%Tour Operator%' THEN 'Chaperones'
                    WHEN typ.[description] like '%teacher%' THEN 'Teachers' ELSE typ.[description] END,
               count(*)
        FROM [#sub_lineitem_raw_data] as sli
             INNER JOIN [#orders] AS ord ON ord.[order_no] = sli.[order_no]
             LEFT OUTER JOIN [#perf_raw_data] as prf ON prf.[perf_no] = sli.[perf_no]
             LEFT OUTER JOIN [dbo].[TR_PRICE_TYPE] as typ ON typ.[id] = sli.[price_type]
        WHERE prf.[perf_title_name] = 'Mugar Omni Theater'
        GROUP BY sli.[order_no], ord.[customer_no], prf.[perf_date],
                 CASE WHEN typ.[description] like '%student%' or typ.[description] like '%child%' or typ.[description] like '%Pass%' THEN 'Students'
                      WHEN typ.[description] like '%chaperone%' or typ.[description] like '%Adult%' or typ.[description] like '%Tour Operator%' THEN 'Chaperones'
                      WHEN typ.[description] like '%teacher%' THEN 'Teachers' ELSE typ.[description]  END

    /*  Get product information - Planetarium  */

        INSERT INTO [#products] ([data_pass],[order_no],[customer_no],[perf_date],[price_type],[Planetarium])
        SELECT 1, 
               sli.[order_no], 
               ord.[customer_no], 
               prf.[perf_date],
               CASE WHEN typ.[description] like '%student%' or typ.[description] like '%child%' or typ.[description] like '%Pass%' THEN 'Students'
                    WHEN typ.[description] like '%chaperone%' or typ.[description] like '%Adult%' or typ.[description] like '%Tour Operator%' THEN 'Chaperones'
                    WHEN typ.[description] like '%teacher%' THEN 'Teachers' ELSE typ.[description] END as 'price_type',
               count(*)
        FROM [#sub_lineitem_raw_data] as sli
             INNER JOIN [#orders] AS ord ON ord.[order_no] = sli.[order_no]
             LEFT OUTER JOIN [#perf_raw_data] as prf ON prf.[perf_no] = sli.[perf_no]
             LEFT OUTER JOIN [dbo].[TR_PRICE_TYPE] as typ ON typ.[id] = sli.[price_type]
        WHERE prf.[perf_title_name] = 'Hayden Planetarium'
        GROUP BY sli.[order_no], ord.[customer_no], prf.[perf_date],
                 CASE WHEN typ.[description] like '%student%' or typ.[description] like '%child%' or typ.[description] like '%Pass%' THEN 'Students'
                      WHEN typ.[description] like '%chaperone%' or typ.[description] like '%Adult%' or typ.[description] like '%Tour Operator%' THEN 'Chaperones'
                      WHEN typ.[description] like '%teacher%' THEN 'Teachers' ELSE typ.[description]  END

    /*  Get product information - 4D-Theater  */

        INSERT INTO [#products] ([data_pass],[order_no],[customer_no],[perf_date],[price_type],[4DTheater])
        SELECT 1,
               sli.[order_no], 
               ord.[customer_no], 
               prf.[perf_date],
               CASE WHEN typ.[description] like '%student%' or typ.[description] like '%child%' or typ.[description] like '%Pass%' THEN 'Students'
                    WHEN typ.[description] like '%chaperone%' or typ.[description] like '%Adult%' or typ.[description] like '%Tour Operator%' THEN 'Chaperones'
                    WHEN typ.[description] like '%teacher%' THEN 'Teachers' ELSE typ.[description] END,
               count(*)
        FROM [#sub_lineitem_raw_data] as sli
             INNER JOIN [#orders] AS ord ON ord.[order_no] = sli.[order_no]
             LEFT OUTER JOIN [#perf_raw_data] as prf ON prf.[perf_no] = sli.[perf_no]
             LEFT OUTER JOIN [dbo].[TR_PRICE_TYPE] as typ ON typ.[id] = sli.[price_type]
        WHERE prf.[perf_title_name] = '4-D Theater'
        GROUP BY sli.[order_no], ord.[customer_no], prf.[perf_date],
                 CASE WHEN typ.[description] like '%student%' or typ.[description] like '%child%' or typ.[description] like '%Pass%' THEN 'Students'
                      WHEN typ.[description] like '%chaperone%' or typ.[description] like '%Adult%' or typ.[description] like '%Tour Operator%' THEN 'Chaperones'
                      WHEN typ.[description] like '%teacher%' THEN 'Teachers' ELSE typ.[description]  END

    /*  Get product information - Butterfly Garden  */

        INSERT INTO [#products]([data_pass],[order_no],[customer_no],[perf_date],[price_type],[Butterfly])
        SELECT 1, 
               sli.[order_no], 
               ord.[customer_no], 
               prf.[perf_date],
               CASE WHEN typ.[description] like '%student%' or typ.[description] like '%child%' or typ.[description] like '%Pass%' THEN 'Students'
                    WHEN typ.[description] like '%chaperone%' or typ.[description] like '%Adult%' or typ.[description] like '%Tour Operator%' THEN 'Chaperones'
                    WHEN typ.[description] like '%teacher%' THEN 'Teachers' ELSE typ.[description] END,
               COUNT(*)
        FROM [#sub_lineitem_raw_data] as sli
             INNER JOIN [#orders] AS ord ON ord.[order_no] = sli.[order_no]
             LEFT OUTER JOIN [#perf_raw_data] as prf (NOLOCK) ON prf.[perf_no] = sli.[perf_no]
             LEFT OUTER JOIN [dbo].[TR_PRICE_TYPE] as typ (NOLOCK) ON typ.[id] = sli.[price_type]
        WHERE prf.[perf_title_name] = 'Butterfly Garden'
        GROUP BY sli.[order_no], ord.[customer_no], prf.[perf_date],
                 CASE WHEN typ.[description] like '%student%' or typ.[description] like '%child%' or typ.[description] like '%Pass%' THEN 'Students'
                      WHEN typ.[description] like '%chaperone%' or typ.[description] like '%Adult%' or typ.[description] like '%Tour Operator%' THEN 'Chaperones'
                      WHEN typ.[description] like '%teacher%' THEN 'Teachers' ELSE typ.[description]  END

    /*  If Students Only, remove all other records  */

         IF @count_students_only = 'Y' DELETE FROM [#products] WHERE [price_type] <> 'students'
         ELSE DELETE FROM [#products] WHERE [price_type] not in ('Chaperones','Students','Teachers')

    /*  Aggregate all the individual totals into a single row per transaction and remove the raw data from passs 1 */

        INSERT INTO [#products] ([data_pass],[order_no],[customer_no],[perf_date],[price_type],[ExhibitHalls],[OmniTheater],[Planetarium],[4DTheater],[Butterfly])
        SELECT 2, [order_no], [customer_no], [perf_date], 'Total',  sum([ExhibitHalls]), sum([OmniTheater]), sum([Planetarium]), sum([4DTheater]), sum([Butterfly])
        FROM [#products] WHERE [data_pass] = 1 GROUP BY [order_no], [customer_no], [perf_date]

        DELETE FROM [#products] WHERE [data_pass] = 1

    /*  Populate final table that combines the product information with the customer (school) information  */

        INSERT INTO [#final_table] ([rpt_message], [order_no], [perf_date], [ExhibitHalls], [OmniTheater], [Planetarium], [4DTheater], [Butterfly],
                                    [customer_no], [last_name], [first_name], [middle_name], [street1], [street2], [city], [state], [postal_code],
                                    [country], [order_audience], [order_lowest_grade])
        SELECT '', 
               pro.[order_no], 
               pro.[perf_date], 
               pro.[ExhibitHalls], 
               pro.[OmniTheater], 
               pro.[Planetarium], 
               pro.[4DTheater], 
               pro.[Butterfly], 
               pro.[customer_no],
               ISNULL(cus.[lname],''), 
               ISNULL(cus.[fname],''), 
               ISNULL(cus.[mname],''), 
               ISNULL(adr.street1,''), 
               ISNULL(adr.[street2],''), 
               ISNULL(adr.[city],''), 
               ISNULL(adr.[state],''), 
               ISNULL(adr.[postal_code],''), 
               ISNULL(cou.[description],''),
               ISNULL(ord.[custom_5],''),
               ISNULL(ord.[custom_0],'')
        FROM [#products] as pro
             LEFT OUTER JOIN [dbo].[T_CUSTOMER] as cus ON cus.[customer_no] = pro.[customer_no]
             LEFT OUTER JOIN [dbo].[T_ADDRESS] as adr ON adr.[customer_no] = cus.[customer_no] and adr.[primary_ind] = 'Y' and adr.[inactive] = 'N'
             LEFT OUTER JOIN [dbo].[TR_COUNTRY] as cou ON cou.[id] = adr.[country]
             LEFT OUTER JOIN [dbo].[T_ORDER] AS ord ON pro.[order_no] = ord.[order_no]
        WHERE (@report_state = 'All' OR adr.[state] = @report_state) 
          AND (ISNULL(@list_no,0) = 0 OR pro.[customer_no] IN (SELECT [customer_no] FROM [dbo].[T_LIST_CONTENTS] WHERE [list_no] = @list_no))

    /*  If no city, change to 'Unknown City'  */

        UPDATE [#final_table] SET [city] = 'Unknown City', [postal_code] = '00000' WHERE [city] = ''
        
        UPDATE [#final_table] SET [state] = [city] WHERE [state] = ''

     FINISHED:
    
        /* If no records found, insert a single record into the final table  */
  
            IF (SELECT COUNT([order_no]) FROM [#final_table]) = 0 BEGIN
                IF @rpt_msg = '' SELECT @rpt_msg = 'No records found'
                INSERT INTO [#final_table] VALUES (@rpt_msg, 0, '', 0, 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '')
            END

        /* Select data from final table into the report  */

        SELECT [rpt_message], 
               [order_no], 
               [perf_date], 
               [ExhibitHalls], 
               [OmniTheater], 
               [Planetarium], 
               [4DTheater], 
               [Butterfly], 
               [customer_no], 
               LTRIM([last_name]) AS [last_name], 
               [first_name], 
               [middle_name], 
               [street1], 
               [street2], 
               [city], 
               [state], 
               dbo.AF_FORMAT_STRING(postal_code, '@@@@@-@@@@') AS [postal_code],   
			   [country], 
               [order_audience],
               [order_lowest_grade],
               @report_start_date as [p_start_date], 
               @report_end_date as [p_end_date], 
               @include_partial_paid as [p_include_partial_paid], 
               @count_students_only as [p_count_students_only]
        FROM [#final_table]
        ORDER BY last_name, order_no
		
    CLEAN_UP:

        /*  Not really necessary, but the programmer in me wants to clean up  */

            IF OBJECT_ID('tempdb..#perf_raw_data') IS NOT NULL DROP TABLE [#perf_raw_data]
            IF OBJECT_ID('tempdb..#sub_lineitem_raw_data') IS NOT NULL DROP TABLE [#sub_lineitem_raw_data]
            IF OBJECT_ID('tempdb..#order_raw_data') IS NOT NULL DROP TABLE [#order_raw_data]
            IF OBJECT_ID('tempdb..#sub_lineitems') IS NOT NULL DROP TABLE [#sub_lineitems]
            IF OBJECT_ID('tempdb..#orders') IS NOT NULL DROP TABLE [#orders]
            IF OBJECT_ID('tempdb..#products') IS NOT NULL DROP TABLE [#products]
            IF OBJECT_ID('tempdb..#final_table') IS NOT NULL DROP TABLE [#final_table]
            
    DONE:

END
GO

GRANT EXECUTE ON [dbo].[LRP_SCHOOL_ATTENDANCE_BY_CITY] TO impusers
GO

EXECUTE [dbo].[LRP_SCHOOL_ATTENDANCE_BY_CITY] '12-1-2018', '12-15-2018', 'MA', 'Y', 'N', 0



