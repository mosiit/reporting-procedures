USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_SCHOLARSHIP_SUMMER_COURSES_SUMMARY]') AND type IN (N'P', N'PC'))
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_SCHOLARSHIP_SUMMER_COURSES_SUMMARY] AS';
GO

EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LRP_SCHOLARSHIP_SUMMER_COURSES_SUMMARY] TO [impusers], [tessitura_app]';
GO

ALTER PROCEDURE [dbo].[LRP_SCHOLARSHIP_SUMMER_COURSES_SUMMARY]
        @report_start_dt DATETIME = NULL, 
        @report_end_dt DATETIME = NULL,
        @customer_type_id INT = 0,
        @scholarship_id INT = 0,
        @list_no INT = 0
WITH RECOMPILE AS BEGIN

-----------------

--DECLARE 
--	@report_start_dt DATETIME = '2019-7-01', --'2019-12-01', 
--    @report_end_dt DATETIME = '2019-8-01', --'2019-12-31',
--    @customer_type_id INT = 0,
--    @scholarship_id INT = 0

----------------   

    /*  Procedure Variables  */

        DECLARE @ord_no INT, @perf_date CHAR(10), @prod_name VARCHAR(50)
        DECLARE @start_date CHAR(10), @end_date CHAR(10)
        DECLARE @cust_type_id_school INT, @cust_type_id_school_official INT 


    /*  Create temporary tables  */
            
        IF OBJECT_ID('tempdb..#summer_course_orders') IS NOT NULL DROP TABLE [#summer_course_orders]

        CREATE TABLE [#summer_course_orders] ([order_no] INT,
                                              [customer_no] INT)
            
        --CREATE UNIQUE CLUSTERED INDEX [ix_summer_course_orders] ON [#summer_course_orders] ([order_no] ASC) ON [PRIMARY]
          
        IF OBJECT_ID('tempdb..#summer_course_data') IS NOT NULL DROP TABLE [#summer_course_data]

        CREATE TABLE [#summer_course_data] ([data_type] VARCHAR(10) NOT NULL DEFAULT (''), 
                                            [order_no] INT NOT NULL DEFAULT (0),
                                            [sli_no] INT NOT NULL DEFAULT (0),
                                            [customer_no] INT NOT NULL DEFAULT (0),
                                            [customer_type_no] INT NOT NULL DEFAULT (0),
                                            [performance_date] CHAR(10) NOT NULL DEFAULT (''),
                                            [title_name] VARCHAR(30) NOT NULL DEFAULT (''), 
                                            [scholarship_no] INT NOT NULL DEFAULT (0),
                                            [production_name] VARCHAR(50) NOT NULL DEFAULT (''),
                                            [zone_name] VARCHAR(30) NOT NULL DEFAULT (''),
                                            [price_type_name] VARCHAR(30) NOT NULL DEFAULT (''),
                                            [create_dt] DATETIME NULL,
                                            [due_amount] MONEY NOT NULL DEFAULT (0.0), 
											[total_paid] MONEY NOT NULL DEFAULT (0.0),
                                            [attendance] INT NOT NULL DEFAULT (0))

        IF OBJECT_ID('tempdb..#summer_course_final') IS NOT NULL DROP TABLE [#summer_course_final]

        CREATE TABLE [#summer_course_final] ([data_type] VARCHAR(10) NOT NULL DEFAULT (''),
                                             [order_no] INT NOT NULL DEFAULT (0),
                                             [order_row_no] INT,
                                             [order_course_attend] INT,
                                             [production_name] VARCHAR(50) NOT NULL DEFAULT (''),
                                             [customer_type] VARCHAR(30) NOT NULL DEFAULT (''),
                                             [due_amount] MONEY NOT NULL DEFAULT (0.0),
                                             [total_paid] MONEY NOT NULL DEFAULT (0.0),
                                             [program_counter] INT NOT NULL DEFAULT (0),
                                             [attendance] INT NOT NULL DEFAULT (0))

    /*  Check Parameters  */

        IF @report_start_dt IS NULL
            SELECT @report_start_dt = CONVERT(DATETIME,'7-1-' 
                                    + CASE WHEN DATEPART(MONTH,GETDATE()) < 7 THEN CONVERT(VARCHAR(30),DATEPART(YEAR,DATEADD(YEAR,-1,GETDATE()))) 
                                                                              ELSE CONVERT(VARCHAR(30),DATEPART(YEAR,GETDATE())) END)
        IF @report_end_dt IS NULL
            SELECT @report_end_dt = CONVERT(DATETIME,'6-30-' 
                                    + CASE WHEN DATEPART(MONTH,GETDATE()) >= 7 THEN CONVERT(VARCHAR(30),DATEPART(YEAR,DATEADD(YEAR,1,GETDATE()))) 
                                                                               ELSE CONVERT(VARCHAR(30),DATEPART(YEAR,GETDATE())) END) + '23:59:59.967'

        SELECT @customer_type_id = ISNULL(@customer_type_id,0)
        SELECT @scholarship_id = ISNULL(@scholarship_id,0)

    /*  Make sure the date parameters include the full dates, starting at 00:00 and ending at 23:59  */

        SELECT @start_date = CONVERT(CHAR(10),@report_start_dt,111),
               @end_date = CONVERT(CHAR(10), @report_end_dt,111)


     /*  For the purposes of this report, the School and School Official Record customer types are the same thing.
         The id numbers are needed to combine the two  */

        SELECT @cust_type_id_school = [id] FROM [dbo].[TR_CUST_TYPE] WHERE [description] = 'School'
        SELECT @cust_type_id_school = ISNULL(@cust_type_id_school,0)

        SELECT @cust_type_id_school_official = [id] FROM [dbo].[TR_CUST_TYPE] WHERE [description] = 'School Official Record'
        SELECT @cust_type_id_school_official = ISNULL(@cust_type_id_school_official,0)


    /*  If customer type id selected was school official record, change to school  */

        IF @customer_type_id = @cust_type_id_school_official SELECT @customer_type_id = @cust_type_id_school;
    
    
    /*  Generate a list of all Summer Course sales for designated date range  */

        WITH [CTE_COURSE_PERFS] AS
            (SELECT [performance_no], 
                    [performance_zone]
            FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] AS prf
            WHERE [performance_date] BETWEEN @start_date AND @end_date
              AND [title_no] = 1113)
        INSERT INTO [#summer_course_orders] ([order_no],[customer_no])
        SELECT DISTINCT sli.[order_no],
                        ord.[customer_no]
        FROM [dbo].[T_SUB_LINEITEM] AS sli
             INNER JOIN [CTE_COURSE_PERFS] AS cte ON cte.[performance_no] = sli.[perf_no] AND cte.[performance_zone] = sli.[zone_no]
             INNER JOIN [dbo].[T_ORDER] AS ord ON ord.[order_no] = sli.[order_no]
             INNER JOIN [dbo].[LV_SCHOLARSHIP_PAYMENTS] AS sch ON sch.[order_no] = ord.[order_no]

        --INSERT INTO [#summer_course_orders] ([order_no],[customer_no])
        --SELECT DISTINCT ord.[order_no], ord.[customer_no]
        --FROM [dbo].[LV_ORDER_DETAIL] AS ord
        --     INNER JOIN [dbo].[LV_SCHOLARSHIP_PAYMENTS] AS pay ON pay.[order_no] = ord.[order_no]
        --WHERE ord.[performance_date] BETWEEN @start_date AND @end_date 
        --  AND ord.title_name = 'Summer Courses'

        --IF USING A LIST, DELETE OTHERS
        IF ISNULL(@list_no, 0) > 0
            DELETE FROM [#summer_course_orders]
            WHERE [customer_no] NOT IN (SELECT [customer_no] FROM [dbo].[T_LIST_CONTENTS] WHERE [list_no] = @list_no)

    /* Get Summer Course Order and Payment Data */
     
        INSERT INTO [#summer_course_data] ([data_type],[order_no],[sli_no],[customer_no],[customer_type_no],[performance_date],[title_name],[scholarship_no],
                                           [production_name],[zone_name],[price_type_name],[create_dt],[due_amount],[total_paid],[attendance])
        SELECT 'ord_info', 
                ord.[order_no],
                ord.[sli_no],
                ISNULL(ord.[customer_no],0),
                cus.[cust_type],
                ord.[performance_date],
                ord.[title_name],
                0,
                ord.[production_name], 
                ord.[zone_name],
                ord.[price_type_name],
                ord.[create_dt],
                ord.[due_amount],
                0.00,
                COALESCE(TRY_CONVERT(INT,o.custom_1),0)
        FROM [dbo].[LV_ORDER_DETAIL] AS ord (NOLOCK)
             LEFT OUTER JOIN [dbo].[T_CUSTOMER] AS cus ON cus.[customer_no] = ord.[customer_no]
			 LEFT OUTER JOIN [dbo].[T_ORDER] o (NOLOCK) ON (o.order_no = ord.order_no AND o.customer_no = ord.customer_no)
        WHERE ord.[order_no] IN (SELECT [order_no] FROM [#summer_course_orders])

        INSERT INTO [#summer_course_data] ([data_type],[order_no],[sli_no],[customer_no],[customer_type_no],[performance_date],[title_name],[scholarship_no],
                                           [production_name],[zone_name],[price_type_name],[create_dt],[due_amount],[total_paid],[attendance])
        SELECT 'pay_info',
               pay.[order_no],
               0,
               pay.[customer_no],
               ISNULL(cus.[customer_no],0),
               '',
               'payment',
               pay.[scholarship_no],
               pay.[scholarship_name], 
               '',
               pay.[payment_method],
               pay.[payment_dt],
               0.00,
               SUM(pay.[payment_amount]),
               COALESCE(TRY_CONVERT(INT,o.custom_1),0)
        FROM [dbo].[LV_SCHOLARSHIP_PAYMENTS] AS pay (NOLOCK)
             LEFT OUTER JOIN [dbo].[T_CUSTOMER] AS cus (NOLOCK) ON cus.[customer_no] = pay.[customer_no]
			 LEFT OUTER JOIN [dbo].[T_ORDER] o (NOLOCK) ON (o.customer_no = cus.customer_no AND o.order_no = pay.order_no)
        WHERE pay.[order_no] IN (SELECT [order_no] FROM [#summer_course_orders])
        GROUP BY pay.[order_no], pay.[customer_no], ISNULL(cus.[customer_no],0), pay.[scholarship_no], pay.[scholarship_name], pay.[payment_method], pay.[payment_dt],o.[custom_1]
        HAVING SUM([payment_amount]) <> 0.00

        DELETE FROM [#summer_course_data] WHERE due_amount = 0.00 AND total_paid = 0.00
        DELETE FROM [#summer_course_data] WHERE [order_no] IN (SELECT [order_no] FROM [#summer_course_data] WHERE price_type_name = 'Interdepartmental Transfer')
        
        UPDATE [#summer_course_data] SET [production_name] = 'Unknown Scholarship' WHERE [data_type] = 'pay_info' and  [production_name] = ''

    /*  Combine School and School Official Record into a single customer type  */

        UPDATE [#summer_course_data] SET [customer_type_no] = @cust_type_id_school WHERE [customer_type_no] = @cust_type_id_school_official

    /*  Remove unwanted constiruent types and schlarships based on parameters  */

        IF @customer_type_id > 0
            DELETE FROM [#summer_course_data] WHERE [order_no] NOT IN (SELECT [order_no] FROM [#summer_course_data] WHERE [customer_type_no] = @customer_type_id)

        IF @scholarship_id > 0
            DELETE FROM [#summer_course_data] WHERE [scholarship_no] <> @scholarship_id

    /*  Create final data table  */
    
        INSERT INTO [#summer_course_final]([data_type],[order_no],[order_row_no],[production_name],[customer_type],[due_amount],[total_paid],[program_counter],[attendance])
        SELECT dat.[data_type],
               dat.[order_no],
               ROW_NUMBER() OVER(PARTITION BY dat.[order_no] ORDER BY dat.[order_no]) AS [order_row_no],
               dat.[production_name],
               ISNULL(ctp.[description],''),
               SUM(dat.[due_amount]),
               SUM(dat.[total_paid]),
               1,
               SUM(COALESCE(CAST(dat.attendance AS INT),0))
        FROM [#summer_course_data] dat (NOLOCK)
             LEFT OUTER JOIN [dbo].[T_CUSTOMER] AS cus (NOLOCK) ON cus.[customer_no] = dat.[customer_no]
             LEFT OUTER JOIN [dbo].[TR_CUST_TYPE] AS ctp (NOLOCK) ON ctp.[id] = cus.[cust_type]
        WHERE dat.[data_type] = 'pay_info'
        GROUP BY dat.[data_type], dat.[order_no], dat.[production_name], ISNULL(ctp.[description],'')
        HAVING SUM(dat.[total_paid]) <> 0.00;

    /*  Add Exhibit Hall Attendance to first row of each order  */

        WITH CTE_EXH_ATTEND
        AS (SELECT det.[order_no], 
                   det.[title_no], 
                   det.[title_name], 
                   SUM(det.[sale_total]) AS [sale_total]
            FROM [dbo].[LT_HISTORY_TICKET] AS det
                 INNER JOIN [#summer_course_orders] AS ord ON ord.[order_no] = det.[order_no]
            WHERE [det].[title_no] = 1113
            GROUP BY det.[order_no], det.[title_no], det.[title_name])
        UPDATE fin
        SET fin.[order_course_attend] = ISNULL(cte.[sale_total],0)
        FROM [#summer_course_final] AS fin
             LEFT OUTER JOIN [CTE_EXH_ATTEND] AS cte ON cte.[order_no] = fin.[order_no]
        WHERE fin.[order_row_no] = 1


        UPDATE [#summer_course_final] 
        SET [customer_type] = 'School' 
        WHERE [customer_type] = 'School Official Record'

        --UPDATE [#summer_course_final] SET [program_counter] = (SELECT COUNT(DISTINCT [sli_no]) FROM [dbo].[LV_ORDER_DETAIL] AS det
        --                                                   WHERE det.[order_no] = [#summer_course_final].order_no AND det.[zone_no] = 92)     --zone # 92 = 'Program Fee'
     
        --UPDATE [#summer_course_final] SET [program_counter] = (SELECT COUNT(DISTINCT [sli_no]) FROM [dbo].[T_SUB_LINEITEM] 
        --                                                   WHERE [order_no] = [#summer_course_final].[order_no] AND [zone_no] = 92 AND sli_status IN (3,12))     --zone # 92 = 'Program Fee'
     
        --(SELECT COUNT(*) FROM [#summer_course_data] WHERE data_type = 'ord_info' AND [zone_name] = 'Program Fee' AND [#summer_course_data].order_no = [#summer_course_final].order_no)

    FINISHED:
       
        /*  Select the final record set from #final_table  */

            SELECT [data_type], 
                   [order_no], 
                   [order_row_no],
                   ISNULL([order_course_attend], 0) AS [order_course_attend],
                   [production_name], 
                   [program_counter], --THIS IS ACTUALLY AN ORDER COUNTER, NOT A PROGRAM COUNTER
                   [due_amount],
                   [total_paid],
                   [customer_type],
                   [attendance]
            FROM [#summer_course_final]


        /*  Clean up and Destroy Temporary Tables  */

            IF OBJECT_ID('tempdb..#summer_course_final') IS NOT NULL DROP TABLE [#summer_course_final]
            IF OBJECT_ID('tempdb..#summer_course_data') IS NOT NULL DROP TABLE [#summer_course_data]
            IF OBJECT_ID('tempdb..#summer_course_orders') IS NOT NULL DROP TABLE [#summer_course_orders]

    DONE:

END
GO

EXECUTE [dbo].[LRP_SCHOLARSHIP_SUMMER_COURSES_SUMMARY] @report_start_dt = '7-1-2019', @report_end_dt = '10-31-2019', @customer_type_id = 0, @scholarship_id = 0, @list_no = 14129


