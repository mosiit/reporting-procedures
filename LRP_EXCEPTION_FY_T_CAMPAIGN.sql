USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LRP_EXCEPTION_FY_T_CAMPAIGN]    Script Date: 6/14/2021 8:37:32 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[LRP_EXCEPTION_FY_T_CAMPAIGN]
AS
-- T_CAMPAIGN Exception reporting
-- DSJ 6/13/2016

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

-----------------------------
-- T_CAMPAIGN discrepancies 
-----------------------------

SELECT campaign_no, description, start_dt, end_dt, fyear,
CASE 
	WHEN (description IS NULL) THEN 'description is NULL' 
	WHEN (LEN(fyear) <> 4) THEN 'LEN(fyear) <> 4'
	WHEN (start_dt IS NULL AND campaign_no <> 829) THEN 'start_dt IS NULL AND campaign_no <> 829'
	WHEN (end_dt IS NULL AND campaign_no <> 829 AND camp_type IN ('C', 'E'))
		THEN 'end_dt IS NULL AND campaign_no <> 829 AND camp_type IN (''C'', ''E'')'
	WHEN ( CONVERT(VARCHAR(12), start_dt, 114) <> '00:00:00:000')
		THEN ' CONVERT(VARCHAR(12), start_dt, 114) <> ''00:00:00:000'''
	WHEN (CONVERT(VARCHAR(12), end_dt, 114) NOT IN ('23:59:59:000', '23:59:59:997'))
		THEN 'CONVERT(VARCHAR(12), end_dt, 114) NOT IN (''23:59:59:000'', ''23:59:59:997'')'
	WHEN (camp_type IN ('C', 'E') AND fyear IS NULL AND campaign_no NOT IN (781, 829, 1989))
		THEN 'camp_type in (''C'', ''E'') AND fyear IS NULL AND campaign_no NOT IN (781, 829, 1989)'
	WHEN (description LIKE '%FY%' AND start_dt <= '2004-04-30' AND (MONTH(start_dt) <> '5' OR DAY(start_dt) <> 1))
		THEN 'description LIKE ''%FY%'' AND start_dt <= ''2004-04-30'' AND (month(start_dt) <> ''5'' OR day(start_dt) <> 1)'
	WHEN (description LIKE '%FY%' AND start_dt > '2004-04-30' AND start_dt < '2005-07-01' AND start_dt <> '2004-05-01')
		THEN 'description LIKE ''%FY%'' AND start_dt > ''2004-04-30'' AND start_dt < ''2005-07-01'' AND start_dt <> ''2004-05-01'''
	WHEN (description LIKE '%FY%' AND start_dt >= '2005-07-01' AND (MONTH(start_dt) <> '7' OR DAY(start_dt) <> 1))
		THEN 'description LIKE ''%FY%'' AND start_dt >= ''2005-07-01'' AND (month(start_dt) <> ''7'' OR day(start_dt) <> 1)'
	WHEN (description LIKE '%FY%' AND end_dt <= '2004-04-30 23:59:59.000' AND (MONTH(end_dt) <> '4' OR DAY(end_dt) <> '30'))
		THEN 'description LIKE ''%FY%'' AND end_dt <= ''2004-04-30 23:59:59.000'' AND (month(end_dt) <> ''4'' OR day(end_dt) <> ''30'')'
	WHEN (description LIKE '%FY%' AND end_dt > '2004-04-30 23:59:59.997' AND end_dt < '2005-07-01' AND end_dt <> '2005-06-30 23:59:59.997')
		THEN 'description like ''%FY%'' AND end_dt > ''2004-04-30 23:59:59.997'' AND end_dt < ''2005-07-01'' AND end_dt <> ''2005-06-30 23:59:59.997'''
	WHEN (description LIKE '%FY%' AND end_dt  >= '2005-07-01 23:59:59.000' AND (MONTH(end_dt) <> '6' OR DAY(end_dt) <> '30'))
		THEN 'description LIKE ''%FY%'' AND end_dt  >= ''2005-07-01 23:59:59.000'' AND (MONTH(end_dt) <> ''6'' OR DAY(end_dt) <> ''30'')'
	WHEN (description LIKE '%FY%' AND YEAR(end_dt) <> fyear)
		THEN 'description LIKE ''%FY%'' AND YEAR(end_dt) <> fyear'
	WHEN (description LIKE '%FY%' AND RIGHT(RTRIM(description), 4) <> fyear)
		THEN 'description LIKE ''%FY%'' AND right(rtrim(description), 4) <> fyear'
END AS reason
FROM t_campaign 
-- Service Now ticket SCTASK0001987, H. Sheridan - exclude more campaigns (put them in order for readability)
WHERE campaign_no NOT IN (2010,2011,2016,2156,2166,2405,2406,2407,2440,2453,2472,2473)
	AND
	(
		description IS NULL 
		OR LEN(fyear) <> 4
		OR (start_dt IS NULL AND campaign_no <> 829)
		OR (end_dt IS NULL AND campaign_no <> 829 AND camp_type IN ('C', 'E'))
		OR ( CONVERT(VARCHAR(12), start_dt, 114) <> '00:00:00:000')
		OR (CONVERT(VARCHAR(12), end_dt, 114) NOT IN ('23:59:59:000', '23:59:59:997'))
		OR (camp_type IN ('C', 'E') AND fyear IS NULL AND campaign_no NOT IN (781, 829, 1989))
		OR (description LIKE '%FY%' AND start_dt <= '2004-04-30' AND (MONTH(start_dt) <> '5' OR DAY(start_dt) <> 1))
		OR (description LIKE '%FY%' AND start_dt > '2004-04-30' AND start_dt < '2005-07-01' AND start_dt <> '2004-05-01')
		OR (description LIKE '%FY%' AND start_dt >= '2005-07-01' AND (MONTH(start_dt) <> '7' OR DAY(start_dt) <> 1))
		OR (description LIKE '%FY%' AND end_dt <= '2004-04-30 23:59:59.000' AND (MONTH(end_dt) <> '4' OR DAY(end_dt) <> '30'))
		OR (description LIKE '%FY%' AND end_dt > '2004-04-30 23:59:59.997' AND end_dt < '2005-07-01' AND end_dt <> '2005-06-30 23:59:59.997')
		OR (description LIKE '%FY%' AND end_dt  >= '2005-07-01 23:59:59.000' AND (MONTH(end_dt) <> '6' OR DAY(end_dt) <> '30'))
		OR (description LIKE '%FY%' AND YEAR(end_dt) <> fyear)
		OR (description LIKE '%FY%' AND RIGHT(RTRIM(description), 4) <> fyear)
	)
ORDER BY campaign_no

GO


