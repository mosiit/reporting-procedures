USE impresario
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_ADV_STEPS_BY_WORKER_PM_NEWER]') AND type IN (N'P', N'PC'))
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_ADV_STEPS_BY_WORKER_PM_NEWER] AS' 
GO

EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LRP_ADV_STEPS_BY_WORKER_PM_NEWER] TO [impusers], [tessitura_app]'
GO

ALTER PROCEDURE [dbo].[LRP_ADV_STEPS_BY_WORKER_PM_NEWER]
        @fiscal_year INT = NULL,
        @workers_str VARCHAR(4000) = NULL
AS BEGIN

    SET NOCOUNT ON 
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

    /*  Procedure Variables  */

        DECLARE @fiscal_start_dt DATETIME = NULL
        DECLARE @fiscal_end_dt DATETIME = NULL
        DECLARE @current_dt DATETIME = GETDATE()
        DECLARE @worker_no INT = 0
	    
    /*  Check Parameters  */

        IF @fiscal_year IS NULL SELECT @fiscal_year = dbo.LF_GetFiscalYear(GETDATE())
        
        SELECT @fiscal_start_dt = [dbo].[LF_GetFiscalYearStartDt](@fiscal_year)

        SELECT @fiscal_end_dt = [dbo].[LF_GetFiscalYearEndDt](@fiscal_year)
        
        IF @current_dt > @fiscal_end_dt SELECT @current_dt = @fiscal_end_dt

        SELECT @workers_str = ISNULL(@workers_str, 0)

        SELECT @worker_no = TRY_CONVERT(INT, REPLACE(@workers_str,'"',''))
        SELECT @worker_no = ISNULL(@worker_no, 0)

    FINISHED:

        /*  Get Data  */

            SELECT p.plan_no, 
                   p.customer_no,
                   cust.display_name customer_name,
                   cust.sort_name,
                   s.step_no,
                   s.step_dt, 
                   s.[completed_on_dt],
                   s.description AS stepDesc, 
                   typ.description AS stepTypeDesc,
                   s.worker_customer_no, 
                   worker.display_name stepWorker,
                   pm.pm_name constituentPM,
                   pm.pm_id
            FROM [dbo].[T_STEP] AS s
                 INNER JOIN [dbo].[TR_STEP_TYPE] AS typ ON typ.[id] = s.[step_type]
                 INNER JOIN [dbo].[T_PLAN] AS p ON p.[plan_no] = s.[plan_no]
                 INNER JOIN [dbo].[FT_CONSTITUENT_DISPLAY_NAME]() AS worker ON s.[worker_customer_no] = worker.[customer_no]
                 INNER JOIN [dbo].[FT_CONSTITUENT_DISPLAY_NAME]() AS cust ON cust.[customer_no] = p.[customer_no]
                 LEFT JOIN [dbo].[LVS_PROSPECT_MANAGER] AS pm ON pm.[customer_no] = p.[customer_no]
            WHERE typ.[description] LIKE 'Contact%' 
              AND s.[worker_customer_no] = @worker_no
              AND s.[step_dt] BETWEEN @fiscal_start_dt AND @current_dt
              AND s.[completed_on_dt] IS NOT NULL   

END
GO


EXEC LRP_ADV_STEPS_BY_WORKER_PM_NEWER @fiscal_year = 2021, @workers_str = 3504561