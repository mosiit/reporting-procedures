USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_AVERAGE_VENUES_PER_ORDER]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LRP_AVERAGE_VENUES_PER_ORDER]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LRP_AVERAGE_VENUES_PER_ORDER]
        @report_start_dt DATETIME,
        @report_end_dt DATETIME,
        @report_operator VARCHAR(10),
        @report_operator_location VARCHAR(30),
        @exclude_school_transactions CHAR(1),
        @exclude_group_transactions CHAR(1),
        @separate_member_non_member CHAR(1)
AS BEGIN

    /*  Check the parameters  */

        SELECT @report_operator = ISNULL(@report_operator,'')
        IF @report_operator = 'All' OR @report_operator = 'All Users' SELECT @report_operator = ''
        
        SELECT @report_start_dt = CONVERT(CHAR(10),@report_start_dt,111) + ' 00:00:00',
               @report_end_dt = CONVERT(CHAR(10),@report_end_dt, 111) + ' 23:59:59.927'

        SELECT @exclude_Group_transactions = ISNULL(@exclude_Group_transactions,'N')
        SELECT @exclude_school_transactions = ISNULL(@exclude_school_transactions,'N')
        
    /****************************************************************************************************************************************************************************************************/

    /*  Create Temporary Table for order information  */

        IF OBJECT_ID('tempdb..#order_table') IS NOT NULL DROP TABLE [#order_table]
            
        CREATE TABLE [#order_table] ([order_no] INT, [total_performances] DECIMAL(18,2), [visit_count] DECIMAL(18,2), [customer_no] INT, [customer_type] INT, [customer_type_name] VARCHAR(30), 
                                     [memb_level] VARCHAR(3), [memb_on_order] CHAR(1), [add_on_counter] DECIMAL(18,2), [notes] VARCHAR(50))

        CREATE UNIQUE CLUSTERED INDEX [ix_order_table_order_no] ON #order_table ([order_no] ASC) ON [PRIMARY]

    /*  Create Temporary Table for performance information  */

        IF OBJECT_ID('tempdb..#performance_table') IS NOT NULL DROP TABLE [#performance_table]

        CREATE TABLE [#performance_table] ([title_no] INT, [title_name] VARCHAR(30), [production_no] INT, [production_name] VARCHAR(30), [production_name_long] VARCHAR(255), [production_season_no] INT,
                                               [production_season_name] VARCHAR(30), [performance_no] INT, [performance_dt] DATETIME, [performance_date] CHAR(10), [performance_time] VARCHAR(8),
                                               [performance_time_display] VARCHAR(30), [performance_zone_no] INT, [performance_zone_name] VARCHAR(30), [performance_type_no] INT, [performance_type_name] VARCHAR(30))

        CREATE UNIQUE CLUSTERED INDEX [ix_performance_table_performance_no] ON #performance_table ([performance_no] ASC, [performance_zone_no] ASC) ON [PRIMARY]

        CREATE NONCLUSTERED INDEX [ix_performance_table_performance_dt] ON #performance_table ([performance_dt] ASC) ON [PRIMARY]

        CREATE NONCLUSTERED INDEX [ix_performance_table_performance_title_name] ON #performance_table ([title_name] ASC) ON [PRIMARY]

        CREATE NONCLUSTERED INDEX [ix_performance_table_performance_date_time] ON #performance_table ([title_no] ASC, [performance_date] ASC, [performance_time] ASC) ON [PRIMARY]

    /*  Create temporary table for visit count information  */

        IF OBJECT_ID('tempdb..#visit_count_raw_data') is not null DROP TABLE [#visit_count_raw_data]
            
        CREATE TABLE [#visit_count_raw_data] ([order_no] int, [perf_no] int, [zone_no] int, [sale_total] int)

        CREATE CLUSTERED INDEX [ix_visit_count_raw_data_order_no] ON #visit_count_raw_data ([order_no] ASC) ON [PRIMARY]

        CREATE NONCLUSTERED INDEX [ix_visit_count_raw_data_sales_total] ON #visit_count_raw_data ([sale_total] ASC) ON [PRIMARY]

    /*  Create temporary table for final report information  */

        IF OBJECT_ID('tempdb..#final_table') IS NOT NULL DROP TABLE [#final_table]
            
        CREATE TABLE [#final_table] ([operator_id] VARCHAR(10), [operator_name] VARCHAR(50), [operator_name_sort] VARCHAR(50), [operator_location] VARCHAR(30), [order_no] INT, [total_performances] DECIMAL(18,2), 
                                     [visit_count] DECIMAL(18,2), [customer_no] INT, [customer_type] INT, [customer_type_name] VARCHAR(30), [memb_level] VARCHAR(3), [add_on_counter]  DECIMAL(18,2), [notes] VARCHAR(50))

    /****************************************************************************************************************************************************************************************************/

    /*  Get initial list of orders that were either created on or updated on dates within the report date range  */
    
        INSERT INTO [#order_table]
            SELECT sli.[order_no], 0, 0, ord.[customer_no], cus.[cust_type], typ.[description], MAX(ISNULL(mem.[memb_level],'')), 'N', 0, ''
            FROM [dbo].[T_SUB_LINEITEM] AS sli (NOLOCK)
                 LEFT OUTER JOIN [dbo].[T_ORDER] AS ord (NOLOCK) ON ord.order_no = sli.order_no
                 LEFT OUTER JOIN [dbo].[T_CUSTOMER] AS cus (NOLOCK) ON cus.[customer_no] = ord.[customer_no]
                 LEFT OUTER JOIN [dbo].[TX_CUST_MEMBERSHIP] AS mem (NOLOCK) ON mem.[customer_no] = cus.[customer_no] AND mem.[cur_record] = 'Y' AND sli.[create_dt] BETWEEN mem.[init_dt] AND mem.[expr_dt]
                 LEFT OUTER JOIN [dbo].[TR_CUST_TYPE] AS typ (NOLOCK) ON typ.[id] = cus.[cust_type]
            WHERE (sli.[create_dt] BETWEEN @report_start_dt AND @report_end_dt OR sli.[last_update_dt] BETWEEN @report_start_dt AND @report_end_dt)  AND [sli_status] IN (3, 12)
            GROUP BY sli.[order_no], ord.[customer_no], cus.[cust_type], typ.[description]

    /*  Library Memberships should not count as memberships in this report  */

        UPDATE [#order_table] SET memb_level = '' WHERE memb_level = 'L'

    /* Specific Membership Levels don't matter, change field to Y/N */

        UPDATE [#order_table] SET memb_level = 'Y' WHERE [memb_level] <> ''

        UPDATE [#order_table] SET memb_level = 'N' WHERE [memb_level] = ''

    /*  Get a list of all performances on those orders  */

        INSERT INTO [#performance_table]
            SELECT [title_no], [title_name], [production_no], [production_name], [production_name_long], [production_season_no], [production_season_name], [performance_no], [performance_dt], 
                            [performance_date], [performance_time], [performance_time_display], [performance_zone], [performance_zone_text], [performance_type], [performance_type_name]
            FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] 
            WHERE [performance_no] IN (SELECT [perf_no] FROM [dbo].[T_SUB_LINEITEM] WHERE [order_no] IN (SELECT [order_no] FROM [#order_table]))

    /*  Determine which orders in the order table have a membership sale on them  */
            
            UPDATE [#order_table] SET [memb_on_order] = 'Y'
            WHERE [order_no] IN (SELECT DISTINCT [order_no] FROM [dbo].[T_SUB_LINEITEM]
                               WHERE [order_no] IN (SELECT [order_no] FROM [#order_table])
                                 AND [perf_no] IN (SELECT DISTINCT [performance_no] FROM [#performance_table] WHERE [title_name] = 'Membership' AND [production_name] = 'Household'))

    /*  We are only interested in the major venues . . .  Delete the rest  */
                  
        DELETE FROM [#performance_table] WHERE [title_name] NOT IN ('4-D Theater','Butterfly Garden','Exhibit Halls', 'Hayden Planetarium', 'Mugar Omni Theater', 'Special Exhibitions')

    /* Update the total performances sold on the  */
                
        UPDATE [#order_table] 
        SET [total_performances] = (SELECT COUNT(DISTINCT [perf_no]) FROM [dbo].[T_SUB_LINEITEM]
                                    WHERE [dbo].[T_SUB_LINEITEM].[order_no] = [#order_table].[order_no]
                                      AND [dbo].[T_SUB_LINEITEM].[perf_no] IN (SELECT [performance_no] FROM [#performance_table]))

    /*  For Membership Orders, add one performance to each order to cover their Exhibit Hall admission  */
 
        UPDATE [#order_table] SET [total_performances] = ([total_performances] + 1)  WHERE [memb_level] = 'Y' OR [memb_on_order] = 'Y'

    /*  Delete orders with no sales to any of the major performance venues */

        DELETE FROM [#order_table] WHERE [total_performances] = 0.00

    /*  Any order with more than 1 performance on it has an add on  */

        UPDATE [#order_table] SET [add_on_counter] = 1 WHERE [total_performances] > 1.00
        
    /*  Get raw data about order visit counts - Visit counts only needed as a way to exclude groups of 15 or more  */

        INSERT INTO [#visit_count_raw_data]
        SELECT sli.[order_no], sli.[perf_no], sli.[zone_no], COUNT(sli.[sli_no]) 
        FROM [dbo].[T_SUB_LINEITEM] AS sli (NOLOCK)
             LEFT OUTER JOIN [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] AS prf (NOLOCK) ON prf.[performance_no] = sli.perf_no AND prf.[performance_zone] = sli.[zone_no]
        WHERE [order_no] IN (SELECT order_no FROM [#order_table]) AND prf.[visit_count_title] = 'Y' AND prf.production_name NOT LIKE '%Buyout%'
        GROUP BY [order_no], [perf_no], [zone_no], [production_name] ORDER BY [order_no]
        
    /*  Update the visit counts in the order table  */
            
        UPDATE [#order_table] 
        SET [visit_count] = (SELECT max([sale_total]) FROM [#visit_count_raw_data] WHERE [#visit_count_raw_data].[order_no] = [#order_table].[order_no])

    /*  On orders that don't have any specific visit count venues, guess using the performances purchased  */

        UPDATE [#order_table] 
        SET [visit_count] = (SELECT (COUNT(*) / [#order_table].[total_performances]) FROM [dbo].[T_SUB_LINEITEM]
                             WHERE [order_no] = [#order_table].[order_no] AND [dbo].[T_SUB_LINEITEM].[sli_status] IN (3,12) AND [dbo].[T_SUB_LINEITEM].[perf_no] IN (SELECT [performance_no] FROM [#performance_table])),
            [notes] = 'Visit Count Guess'
        WHERE [visit_count] IS NULL

    /*  Round any decimal points to nearest whole number on guessed visit counts  */

        UPDATE [#order_table] SET [visit_count] = ROUND([visit_count],0) WHERE ROUND([visit_count],0) <> [visit_count]

    /*  Remove unwanted records (as defined in the report parameters  */

        IF @exclude_Group_transactions = 'Y'
                DELETE FROM [#order_table] WHERE visit_count >= 15 AND [customer_type_name] NOT LIKE '%School%'

        IF @exclude_school_transactions = 'Y'
                DELETE FROM [#order_table] WHERE [customer_type_name] LIKE '%School%'

    /*  If not separating out membership sales from non-member sales, set memb_level to blank on all records
        If are seperating out membership sales from non-member sales, set memb_level to 'Y' if there is a membership product on the order itself  */
        
        IF @separate_member_non_member <> 'Y'
                UPDATE [#order_table] SET [memb_level] = ''
        ELSE
                UPDATE [#order_table] SET [memb_level] = 'Y' WHERE [memb_level] = 'N' AND [memb_on_order] = 'Y'

    /*  Create final data set  */

        INSERT INTO [#final_table]
        SELECT DISTINCT sli.[last_updated_by], usr.[fname] + ' ' + usr.[lname], usr.[lname] + ', ' + usr.[fname], usr.[location], sli.[order_no], ord.[total_performances], ord.[visit_count], ord.[customer_no], 
                        ord.[customer_type], ord.[customer_type_name], ord.[memb_level], ord.[add_on_counter], ord.[notes]
        FROM [dbo].[T_SUB_LINEITEM] AS sli
             LEFT OUTER JOIN [#order_table] AS ord (NOLOCK) ON ord.order_no = sli.order_no
             INNER JOIN [dbo].[T_METUSER] AS usr (NOLOCK) ON usr.[userid] = sli.[last_updated_by]
        WHERE sli.[order_no] IN (SELECT [order_no] FROM [#order_table]) AND sli.[last_update_dt] BETWEEN @report_start_dt AND @report_end_dt

    /*  If being run for a single operator, delete all the rest  */

         IF @report_operator <> '' DELETE FROM [#final_table] WHERE [operator_id] <> @report_operator

    /*  If being run for a single operator location, delete all the rest.*/

        IF @report_operator_location <> '' DELETE FROM [#final_table] WHERE [operator_location] <> @report_operator_location

    /*  Select the final data-set that will be passed to the report  */
    
            SELECT [operator_id], [operator_name], [operator_name_sort], [operator_location], [memb_level],
                   COUNT(DISTINCT [order_no]) AS 'total_orders_counted', SUM([total_performances]) AS 'total_performances_sold', CONVERT(DECIMAL(18,2),AVG([total_performances])) AS 'average_performances_per_order',
                   SUM([add_on_counter]) AS 'orders_with_add_on', (SUM([add_on_counter]) / COUNT(DISTINCT [order_no])) AS 'percentage_of_orders_with_add_on'
            FROM [#final_table]
            GROUP BY [operator_id], [operator_name], [operator_name_sort], [operator_location], [memb_level]
            ORDER BY operator_name_sort

        DONE:

            /*  Delete temporary tables  */

                IF OBJECT_ID('tempdb..#final_table') IS NOT NULL DROP TABLE [#final_table]
                IF OBJECT_ID('tempdb..#visit_count_raw_data') is not null DROP TABLE [#visit_count_raw_data]
                IF OBJECT_ID('tempdb..#performance_table') IS NOT NULL DROP TABLE [#performance_table]
                IF OBJECT_ID('tempdb..#order_table') IS NOT NULL DROP TABLE [#order_table]
           
END
GO

GRANT EXECUTE ON [dbo].[LRP_AVERAGE_VENUES_PER_ORDER] TO ImpUsers
GO


--EXECUTE [dbo].[LRP_AVERAGE_VENUES_PER_ORDER]
--        @report_start_dt = '12-1-2016',
--        @report_end_dt = '12-1-2016',
--        @report_operator = 'mdalto00',
--        @report_operator_location = '',
--        @exclude_school_transactions = 'N',
--        @exclude_group_transactions = 'N',
--        @separate_member_non_member = 'N'

