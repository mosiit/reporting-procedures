USE [impresario]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[LRP_CORPORATE_MEMBERSHIP_RENEWAL_LETTER]
        @expr_dt DATETIME, 
        @expr_end_dt DATETIME,   --5/14/2021 - Changed from single sate to date range
	    @customer_no INT = 0 
AS BEGIN

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
    SET NOCOUNT ON 

    SELECT @expr_dt = CAST(@expr_dt AS DATE);
    SELECT @expr_end_dt = CAST(@expr_end_dt AS DATE);

                    /*  The commented CTE code and the one line commented out in the WHERE clause
                        will omit a member if they already have a pending membership.  
                        Uncomment it to turn that feature on - as of 5/18/2021, waiting to hear if they want it  */

    --WITH [CTE_PENDING] ([customer_no])
    --AS (SELECT [customer_no]
    --    FROM [dbo].[TX_CUST_MEMBERSHIP] 
    --    WHERE [memb_org_no] = 7 AND [current_status] = 3)
    SELECT cust.[customer_no], 
           cust.[lname] AS [companyName], 
           lev.[description] AS [memberLevel], 
           memb.[expr_dt], 
           memb.[notes] AS [membershipNotes], 
           lev.[admission_adult],
	       addr.[street1], 
           addr.[street2], 
           addr.[city], 
           addr.[state], 
           [dbo].[AF_FORMAT_STRING] (addr.[postal_code], '@@@@@-@@@@') AS [postal_code],
	       [dbo].[AF_FORMAT_STRING] (ph.[phone], '@@@-@@@-@@@@') AS [phone] 
    FROM [dbo].[T_CUSTOMER] AS cust 
	     INNER JOIN [dbo].[TX_CUST_MEMBERSHIP] AS memb ON cust.[customer_no] = memb.[customer_no] AND memb.[current_status] = 2 AND memb.[cur_record] = 'Y'
	     INNER JOIN [dbo].[T_MEMB_LEVEL] AS lev ON lev.memb_org_no = memb.memb_org_no AND lev.memb_level = memb.memb_level --AND lev.inactive = 'N'  WO 98919,  they want inactive memb levels to show up 
	     LEFT OUTER JOIN [dbo].[FT_GET_PRIMARY_ADDRESS]() addr ON addr.[customer_no] = cust.[customer_no]
	     OUTER APPLY [dbo].[FT_GET_PHONES] (NULL, cust.[customer_no], NULL) AS ph
    WHERE cust.[inactive] = 1
	  AND memb.[memb_org_no] = 7 -- Corporate Membership
	  AND CAST (memb.[expr_dt] AS DATE) BETWEEN @expr_dt AND @expr_end_dt
--      AND cust.[customer_no] NOT IN (SELECT [customer_no] FROM [CTE_PENDING])
      AND cust.[customer_no] = CASE WHEN @customer_no = 0 THEN cust.[customer_no] ELSE @customer_no END 
    ORDER BY cust.[lname]

    DONE:

END
GO

EXECUTE [dbo].[LRP_CORPORATE_MEMBERSHIP_RENEWAL_LETTER] @expr_dt = '5-1-2021', @expr_end_dt = '5-31-2021', @customer_no = 0 

--SELECT * FROM [dbo].[TX_CUST_MEMBERSHIP] WHERE [memb_org_no] = 7 AND CAST([expr_dt] AS DATE) > '5-31-2021' ORDER BY expr_dt
