USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LRP_GET_PERFORMANCE_ITEM_HISTORY]    Script Date: 3/28/2019 3:25:41 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



ALTER PROCEDURE [dbo].[LRP_GET_PERFORMANCE_ITEM_HISTORY] 
	@Description VARCHAR(MAX)
AS
	BEGIN
		SET NOCOUNT ON;
		SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

		DECLARE	@stripDesc	VARCHAR(MAX)

		SELECT	@stripDesc = CASE
						WHEN PATINDEX('%(#%', @Description) > 0 THEN LTRIM(RTRIM(SUBSTRING(@Description, 1, PATINDEX('%(#%', @Description)+1))) + '%'
						ELSE @Description
					END

		SELECT [Tool],
			   [Description],
			   DATENAME(dw, [Start Time]) AS 'Day Name',
			   FORMAT(CAST([Start Time] AS DATE), 'd', 'en-US') AS 'Start Date',
			   FORMAT([Start Time], N'hh:mm tt') AS 'Start Time',
			   CONVERT(INT, [Duration]) AS 'Duration',
			   [Run Status],
			   [Last Run By],
			   [Scheduled]
		FROM   LT_PERFORMANCE_ISSUES_HISTORY
		WHERE	[Description] LIKE @stripDesc
		ORDER BY CAST([Start Time] AS DATE) DESC, CAST([Start Time] AS TIME) DESC, [Duration]
	END;



GO


