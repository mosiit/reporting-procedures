USE impresario;
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_ADV_STW_PROFILE_SUB_BOARD_STATUS]') AND type in (N'P', N'PC'))
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_ADV_STW_PROFILE_SUB_BOARD_STATUS] AS';
GO

EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LRP_ADV_STW_PROFILE_SUB_BOARD_STATUS] TO [impusers], [tessitura_app]';
GO

ALTER PROCEDURE [dbo].[LRP_ADV_STW_PROFILE_SUB_BOARD_STATUS]
         @customer_no INT = NULL
AS BEGIN

    SET NOCOUNT ON;
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    
    /*  Procedute Variables and Temp Tables  */

        --NONE

    /*  Check Parameters */

        SELECT @customer_no = ISNULL(@customer_no, 0)
               
        /*  This procedure is a simple select statement
            It is in a procedure rather than a direct SQL Statement for consistency
            and to make it easier should a need for further data manipulation be needed in the future.  */

    FINISHED:

        /*  Get Final Data Set For Report  */
        
            SELECT a1.[customer_no], 
                   -1 AS name_ind,
                   nam.[best_name] AS [customer_name],
                   a1.[type] AS [name_ind_descrip],
                   a1.[board_status]
            FROM [dbo].[LV_BOARD_STATUS_MAIN] AS a1
                 LEFT OUTER JOIN [dbo].[LV_CUSTOMER_WITH_ALL_ADULT_AFFILIATIONS] a1a ON a1a.[customer_no] = a1.[customer_no] AND a1a.[name_ind] = -1
                 LEFT OUTER JOIN [dbo].[LV_ADV_BEST_NAME] AS nam ON nam.[customer_no] = a1a.[expanded_customer_no]
            WHERE a1.[customer_no] = @customer_no
            UNION
            SELECT a2.[main_customer_no] AS [customer_no], 
                   -2 AS name_ind,
                   nam.[best_name] AS [customer_name],
                   'A2' AS [name_ind_descrip],
                   a2.[description] AS [board_status]
            FROM [dbo].[LV_BOARD_STATUS_A2] AS a2
                 LEFT OUTER JOIN [dbo].[LV_CUSTOMER_WITH_ALL_ADULT_AFFILIATIONS] a2a ON a2a.[customer_no] = a2.[main_customer_no] AND a2a.[name_ind] = -2
                 LEFT OUTER JOIN [dbo].[LV_ADV_BEST_NAME] AS nam ON nam.[customer_no] = a2a.[expanded_customer_no]
            WHERE a2.[main_customer_no] = @customer_no

END
GO
