USE [impresario]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[LRP_ADV_STEPS_BY_WORKER_PM]
(
	@worker_PM_customer_no INT,
	@step_start_dt DATETIME,
	@step_end_dt DATETIME  
)
AS

--EXEC LRP_ADV_STEPS_BY_WORKER_PM
--	@worker_PM_customer_no = 672463,
--	@step_start_dt = '6/30/2018',
--	@step_end_dt = '3/25/2019'  

SET NOCOUNT ON 
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

SELECT p.plan_no, 
	p.customer_no,
	cust.display_name customer_name,
	cust.sort_name,
	s.step_no,
	s.step_dt, 
	s.description AS stepDesc, 
	typ.description AS stepTypeDesc,
	s.worker_customer_no, 
	worker.display_name stepWorker,
	pm.pm_name constituentPM,
	pm.pm_id
FROM T_STEP s
INNER JOIN dbo.TR_STEP_TYPE typ
	ON typ.id = s.step_type
INNER JOIN dbo.T_PLAN p
	ON p.plan_no = s.plan_no
INNER JOIN dbo.FT_CONSTITUENT_DISPLAY_NAME() AS worker 
	ON s.worker_customer_no = worker.customer_no
INNER JOIN dbo.FT_CONSTITUENT_DISPLAY_NAME() AS cust 
	ON cust.customer_no = p.customer_no 
LEFT JOIN dbo.LVS_PROSPECT_MANAGER pm
	ON pm.customer_no = p.customer_no
WHERE typ.description LIKE 'Contact%' 
	--AND typ.inactive = 'N' -- AP asked to remove this constraint 10/9/18
	AND (s.worker_customer_no = @worker_PM_customer_no OR pm.pm_id = @worker_PM_customer_no)
	AND s.step_dt BETWEEN @step_start_dt AND @step_end_dt
