USE [impresario]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LRP_EXCEPTION_FY_TR_SEASON]
AS
-- TR_SEASON Exception reporting
-- DSJ 6/13/2016

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

-----------------------------
-- TR_SEASON discrepancies 
-----------------------------

SELECT id, description, start_dt, end_dt, fyear,
	'Description must begin with "Fiscal Year".' AS reason 
FROM dbo.TR_SEASON
WHERE inactive = 'N'
AND description NOT LIKE 'Fiscal Year%'

UNION ALL 

SELECT id, description, start_dt, end_dt, fyear,
	'Length of Description field should not equal 16.' AS reason 
FROM dbo.TR_SEASON
WHERE inactive = 'N'
AND LEN(description) <> 16 

UNION ALL

SELECT id, description, start_dt, end_dt, fyear,
	'Year of start_dt should be 1 year prior to FY.' AS reason 
FROM dbo.TR_SEASON
WHERE inactive = 'N'
AND YEAR(start_dt)+1  <> fyear

UNION ALL 

SELECT id, description, start_dt, end_dt, fyear,
	'Year of end_dt should be equal to FY.' AS reason 
FROM dbo.TR_SEASON
WHERE inactive = 'N'
AND YEAR(end_dt) <> fyear

UNION ALL 

SELECT id, description, start_dt, end_dt, fyear,
	'fyear <= 2005 AND MONTH(start_dt) <> 5' AS reason 
FROM dbo.TR_SEASON
WHERE inactive = 'N'
AND (fyear <= 2005 AND MONTH(start_dt) <> 5)

UNION ALL

SELECT id, description, start_dt, end_dt, fyear,
	'fyear <= 2004 AND MONTH(end_dt) <> 4' AS reason 
FROM dbo.TR_SEASON
WHERE inactive = 'N'
AND (fyear <= 2004 AND MONTH(end_dt) <> 4)

UNION ALL 

SELECT id, description, start_dt, end_dt, fyear,
	'fyear > 2005 AND MONTH(start_dt) <> 7' AS reason 
FROM dbo.TR_SEASON
WHERE inactive = 'N'
AND (fyear > 2005 AND MONTH(start_dt) <> 7)

UNION ALL 

SELECT id, description, start_dt, end_dt, fyear,
	'fyear > 2004 AND MONTH(end_dt) <> 6' AS reason 
FROM dbo.TR_SEASON
WHERE inactive = 'N'
AND (fyear > 2004 AND MONTH(end_dt) <> 6)

UNION ALL

SELECT id, description, start_dt, end_dt, fyear,
	'LEN(fyear) <> 4' AS reason 
FROM dbo.TR_SEASON
WHERE inactive = 'N'
AND (LEN(fyear) <> 4)
