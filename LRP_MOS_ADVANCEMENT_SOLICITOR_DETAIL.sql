USE [impresario]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Alex Harris
-- Create date: May 16, 2016
-- Description:	Advancement Contribution Solicitor Report
-- =============================================

/* Sample Exec:

Execute dbo.LRP_MOS_ADVANCEMENT_SOLICITOR_DETAIL
	@cont_start_dt = '2018-05-01',
	@cont_end_dt = '2018-06-01'
*/

ALTER PROCEDURE [dbo].[LRP_MOS_ADVANCEMENT_SOLICITOR_DETAIL]

	@cont_start_dt DATETIME = NULL,
	@cont_end_dt DATETIME = NULL,
	@camp_category_str VARCHAR(4000) = NULL,
	@designation_str VARCHAR(4000) = NULL,
	@solicitor_str VARCHAR(4000) = NULL -- added DSJ, 6/9/2017

AS
BEGIN
	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;

	IF ISNULL(@cont_start_dt,CONVERT(DATETIME,'1900-01-01')) = CONVERT(DATETIME,'1900-01-01')
	  BEGIN
	    SET @cont_start_dt = CONVERT(DATETIME,'1900-01-01')
	  END

	IF ISNULL(@cont_end_dt,CONVERT(DATETIME,'2999-12-31')) = CONVERT(DATETIME,'2999-12-31')
	  BEGIN
	    SET @cont_end_dt = CONVERT(DATETIME,'2999-12-31')
	  END

	DECLARE	@camp_categories TABLE (camp_category INT NOT NULL)
		IF ISNULL(@camp_category_str,'') <> ''
		  BEGIN
			INSERT INTO @camp_categories (camp_category)
			SELECT Element From dbo.FT_SPLIT_LIST(@camp_category_str,',')
		  END
		ELSE
		  BEGIN
			INSERT INTO @camp_categories (camp_category)
			SELECT id
			FROM dbo.TR_CAMPAIGN_CATEGORY
		  END

	DECLARE @designations TABLE (cont_designation INT NOT NULL)
		IF ISNULL(@designation_str,'') <> ''
		  BEGIN
		    INSERT INTO @designations (cont_designation)
			SELECT Element From dbo.FT_SPLIT_LIST(@designation_str,',')
		  END
		ELSE
		  BEGIN
		    INSERT INTO @designations (cont_designation)
			SELECT id
			FROM dbo.TR_CONT_DESIGNATION
		  END

	-- DSJ 6/9/2017, copied exact same format as followed in this SP already
	DECLARE @solicitors TABLE (solicitor_id INT NOT NULL)
		IF ISNULL(@solicitor_str,'') <> ''
		  BEGIN
		    INSERT INTO @solicitors (solicitor_id)
			SELECT Element From dbo.FT_SPLIT_LIST(@solicitor_str,',')
		  END

	DECLARE @ResultsTbl TABLE 
	(
		[reference_no] [int] NOT NULL,
		[customer_number] [int] NOT NULL,
		[name] [varchar](55) NULL,
		[type] [char](1) NULL,
		[creditee_type] [varchar](20) NULL,
		[date] [date] NULL,
		[fiscal_year] [int] NOT NULL,
		[fiscal_quarter] [int] NULL,
		[fiscal_period] [int] NOT NULL,
		[contribution_amount] [money] NULL,
		[received_amount] [money] NULL,
		[sol_amount] [money] NULL,
		[sol_count] [int] NOT NULL,
		[solicitor_number] [int] NULL,
		[solicitor_name] [varchar](55) NULL,
		[sec_sol_number] INT NULL,
		[secondary_solicitor] VARCHAR(55) NULL,
		[anonymous] [varchar](30) NULL,
		[soft_credit_type] [varchar](30) NULL,
		[fund_description] [varchar](30) NULL,
		[printable_fund] [varchar](80) NULL,
		[designation] [varchar](30) NULL,
		[overall_campaign] [varchar](30) NULL,
		[campaign] [varchar](30) NULL,
		[campaign_category] [varchar](30) NULL,
		[account_goal] [varchar](30) NULL,
		[account_group] [varchar](30) NULL,
		[appeal] [varchar](30) NOT NULL,
		[media] [VARCHAR](30) NULL,
		[source] [VARCHAR](50) NULL,
		[notes] [VARCHAR](8000) NULL,
		[cancel] [CHAR](1) NULL,
		[pledge_status_desc] [VARCHAR](30) NULL,
		[billing_type] [VARCHAR](30) NULL,
		[batch] [INT] NULL,
		[KG_xfer_dt] [DATETIME] NULL,
		[KGift_desc] [VARCHAR](50) NULL,
		[stock_ticker] [VARCHAR](255) NULL,
		[contribution_detail1] [VARCHAR](30) NULL,
		[contribution_detail2] [VARCHAR](30) NULL,
		[agreement_date] [VARCHAR](255) NULL,
		[challenge_earned] [VARCHAR](255) NULL,
		[pg_instrument] [VARCHAR](30) NULL,
		[solicitation] [VARCHAR](255) NULL,
		[cpf_flag] [CHAR](1) NULL,
		[exh_prog_cat] [VARCHAR](30) NULL,
		[full_fund_name] [VARCHAR](80) NULL,
		[cm_category1] [VARCHAR](80) NULL,
		[cm_intermediate1] [VARCHAR](80) NULL,
		[cm_pillar1] [VARCHAR](80) NULL,
		[cm_category2] [VARCHAR](80) NULL,
		[cm_intermediate2] [VARCHAR](80) NULL,
		[cm_category3] [VARCHAR](80) NULL,
		[cm_intermediate3] [VARCHAR](80) NULL,
		[custom_2] [VARCHAR](255) NULL,
		[custom_9] [VARCHAR](255) NULL,
		[main_customer_type] [VARCHAR](30) NULL,
		[original_customer_type] [VARCHAR](30) NULL,
		[sort_name] [VARCHAR](55) NULL,
		[dept] [VARCHAR](80) NULL,
		[Channel] VARCHAR(30) NULL 
	)


	INSERT INTO @ResultsTbl
		(reference_no,
		customer_number,
		name,
		type,
		creditee_type,
		date,
		fiscal_year,
		fiscal_quarter,
		fiscal_period,
		contribution_amount,
		received_amount,
		sol_amount,
		sol_count,
		solicitor_number,
		solicitor_name,
		sec_sol_number,
		secondary_solicitor,
		anonymous,
		soft_credit_type,
		fund_description,
		printable_fund,
		designation,
		overall_campaign,
		campaign,
		campaign_category,
		account_goal,
		account_group,
		appeal,
		media,
		source,
		notes,
		cancel,
		pledge_status_desc,
		billing_type,
		batch,
		KG_xfer_dt,
		KGift_desc,
		stock_ticker,
		contribution_detail1,
		contribution_detail2,
		agreement_date,
		challenge_earned,
		pg_instrument,
		solicitation,
		cpf_flag,
		exh_prog_cat,
		full_fund_name,
		cm_category1,
		cm_intermediate1,
		cm_pillar1,
		cm_category2,
		cm_intermediate2,
		cm_category3,
		cm_intermediate3,
		custom_2,
		custom_9,
		main_customer_type,
		original_customer_type,
		sort_name,
		dept,
		Channel)
  	SELECT	DISTINCT reference_no = c.ref_no,
			customer_number = c.customer_no,
			name =  CASE WHEN cust.cust_type = 7 THEN cust.lname ELSE cs.esal1_desc END,
			type = c.cont_type,
			creditee_type = '',
			date = CONVERT(DATE,c.cont_dt),
			fiscal_year = bp.fyear,
			fiscal_quarter = bp.quarter,
			fiscal_period = bp.period,
			contribution_amount = c.cont_amt,
			received_amount = c.recd_amt,
			sol_amount = CASE
					WHEN c.worker_customer_no = 2653100 AND c.plan_no IS NULL THEN c.cont_amt
					WHEN ISNULL(c.worker_customer_no,0) = 0 THEN c.cont_amt
					WHEN c.worker_customer_no <> 2653100 THEN c.cont_amt
					ELSE c.cont_amt / ISNULL(sc.sol_count,1)
				END,
			sol_count =	CASE 
					WHEN c.worker_customer_no = 2653100 THEN ISNULL(sc.sol_count,1)
					ELSE 1
				END,
			solicitor_number = CASE 
					WHEN c.worker_customer_no = 2653100 AND c.plan_no IS NOT NULL THEN cs3.customer_no
					ELSE ISNULL(c.worker_customer_no,0) END,
			solicitor_name = CASE
					WHEN c.worker_customer_no = 2653100 AND c.plan_no IS NULL THEN 'ERROR - NEED PLAN'
					WHEN ISNULL(c.worker_customer_no,0) = 0 THEN ''
					WHEN c.worker_customer_no <> 2653100 THEN cs2.esal1_desc
					ELSE cs3.esal1_desc
				END,
			secondSol.customer_no,
			secondSol.display_name,
			anonymous = kv.key_value,
			soft_credit_type = kv2.key_value,
			fund_description = coa.fund_description,
			printable_fund = coa.printable_fund,
			designation = coa.designation,
			overall_campaign = coa.overall_cm,
			campaign = cam.description,
			campaign_category = coa.cm_category,
			account_goal = coa.acct_goal,
			account_group = coa.acct_grp,
			appeal = a.description,
			media = mt.description,
			source = amt.source_name,
			notes = REPLACE(REPLACE(c.notes,CHAR(13),'/'),CHAR(10),'/'),
			cancel = c.cancel,
			pledge_status_desc = ps.description,
			billing_type = bt.description,
			batch = c.batch_no,
			KG_xfer_dt = c.KG_xfer_dt,
			KGift_desc = c.KGift_desc,
			stock_ticker = c.custom_2,
			contribution_detail1 = kv3.key_value,
			contribution_detail2 = kv4.key_value,
			agreement_date = c.custom_6,
			challenge_earned = c.custom_7,
			pg_instrument = kv5.key_value,
			solicitation = c.custom_0,
			cpf_flag = coa.cpf_flag,
			exh_prog_cat = coa.exh_prog_cat,
			full_fund_name = coa.full_fund,
			cm_category1 = coa.cm_category1,
			cm_intermediate1 = coa.cm1_intermediatelevel,
			cm_pillar1 = coa.cm1_main_level,
			cm_category2 = coa.cm_category2,
			cm_intermediate2 = coa.cm2_intermediate_level,
			cm_category3 = coa.cm_category3,
			cm_intermediate3 = coa.cm3_intermediate_level,
			custom_2 = c.custom_2,
			custom_9 = c.custom_9,
			main_customer_type = ct.description,
			original_customer_type = ct.description,
			cust.sort_name,
			d2.description dept,
			chan.description
	FROM dbo.T_CONTRIBUTION AS c
	JOIN dbo.T_CUSTOMER AS cust ON c.customer_no = cust.customer_no
	JOIN dbo.TR_CUST_TYPE AS ct ON cust.cust_type = ct.id
	JOIN dbo.TX_CUST_SAL AS cs ON c.customer_no = cs.customer_no AND cs.default_ind = 'Y'
	JOIN dbo.TR_Batch_Period AS bp ON c.cont_dt BETWEEN bp.start_dt AND bp.end_dt
	JOIN dbo.TR_SALES_CHANNEL AS chan ON chan.id = c.channel
	LEFT JOIN dbo.T_KWCODED_VALUES AS kv ON c.custom_5 = kv.key_value AND kv.keyword_no IN (460)
	LEFT JOIN dbo.T_KWCODED_VALUES AS kv2 ON c.custom_1 = kv2.key_value AND kv2.keyword_no IN (456)
	JOIN dbo.T_CAMPAIGN AS cam ON c.campaign_no = cam.campaign_no
	JOIN dbo.LV_MOS_CHART_OF_ACCOUNTS AS coa ON c.fund_no = coa.fund_no AND cam.campaign_no = coa.campaign_no 
	JOIN dbo.T_APPEAL AS a ON c.appeal_no = a.appeal_no
	JOIN dbo.TR_MEDIA_TYPE AS mt ON c.media_type = mt.id
	JOIN dbo.TX_APPEAL_MEDIA_TYPE AS amt ON c.source_no = amt.source_no
	LEFT JOIN dbo.TR_BILLING_TYPE AS bt ON c.billing_type = bt.id
	LEFT JOIN dbo.T_KWCODED_VALUES AS kv3 ON c.custom_3 = kv3.key_value AND kv3.keyword_no IN (458)
	LEFT JOIN dbo.T_KWCODED_VALUES AS kv4 ON c.custom_4 = kv4.key_value AND kv4.keyword_no IN (459)
	LEFT JOIN dbo.T_KWCODED_VALUES AS kv5 ON c.custom_8 = kv5.key_value AND kv5.keyword_no IN (463)
	LEFT JOIN dbo.TX_CUST_SAL AS cs2 ON c.worker_customer_no = cs2.customer_no AND cs2.default_ind = 'Y'
	JOIN @camp_categories AS cat ON coa.cm_cat_id = cat.camp_category
	JOIN @designations AS des ON coa.desig_id = des.cont_designation
	LEFT JOIN dbo.T_PLAN AS p ON c.plan_no = p.plan_no
	LEFT JOIN dbo.TX_CUST_PLAN AS cp ON p.plan_no = cp.plan_no
	LEFT JOIN 
	(
		SELECT cp1.plan_no, cp1.customer_no, secondarySol.display_name
		FROM dbo.TX_CUST_PLAN cp1
		INNER JOIN dbo.FT_CONSTITUENT_DISPLAY_NAME() AS secondarySol 
			ON cp1.customer_no = secondarySol.customer_no
			AND cp1.role_no = 11 -- Secondary Solicitor
	) secondSol
	ON secondSol.plan_no = p.plan_no
	LEFT JOIN (SELECT plan_no, ISNULL(COUNT(DISTINCT customer_no),0) AS sol_count FROM dbo.TX_CUST_PLAN GROUP BY plan_no) AS sc ON cp.plan_no = sc.plan_no
	LEFT JOIN dbo.TX_CUST_SAL AS cs3 ON cp.customer_no = cs3.customer_no AND cs3.default_ind = 'Y'
	LEFT JOIN TR_PLEDGE_STATUS PS ON C.pledge_status = PS.id 
	LEFT JOIN LT_ATTRIBUTE_DETAIL LT_AD ON LT_AD.type_no = 15 AND LT_AD.customer_no =  CASE 
					WHEN c.worker_customer_no = 2653100 AND c.plan_no IS NOT NULL THEN cs3.customer_no
					ELSE ISNULL(c.worker_customer_no,0) END
	LEFT OUTER JOIN LTR_ATTDET_DESCR2 d2 ON LT_AD.descr2_no = d2.id 
	WHERE ISNULL(c.custom_1,'(none)') IN ('(none)','Matching Gift Credit')
	AND	c.cont_dt BETWEEN @cont_start_dt AND @cont_end_dt
	AND c.cont_amt > 0 
	
UNION ALL
	
  	SELECT	DISTINCT reference_no = c.ref_no,
			customer_number = cr.creditee_no,
			name =  CASE WHEN cust2.cust_type = 7 THEN cust2.lname ELSE cs.esal1_desc END,--cs.esal1_desc,
			type = c.cont_type,
			creditee_type = crt.descriptiuon,
			date = CONVERT(DATE,c.cont_dt),
			fiscal_year = bp.fyear,
			fiscal_quarter = bp.quarter,
			fiscal_period = bp.period,
			contribution_amount = c.cont_amt,
			received_amount = c.recd_amt,
			sol_amount = CASE
					WHEN ISNULL(c.worker_customer_no,0) = 0 THEN c.cont_amt
					WHEN c.worker_customer_no <> 2653100 THEN c.cont_amt
					ELSE c.cont_amt / ISNULL(sc.sol_count,1)
				END,
			sol_count =	CASE 
					WHEN c.worker_customer_no = 2653100 THEN ISNULL(sc.sol_count,1)
					ELSE 1
				END,
			solicitor_number = CASE 
					WHEN c.worker_customer_no = 2653100 AND c.plan_no IS NOT NULL THEN cs3.customer_no
					ELSE ISNULL(c.worker_customer_no,0) END,
			solicitor_name = CASE
					WHEN c.worker_customer_no = 2653100 AND c.plan_no IS NULL THEN 'ERROR - NEED PLAN'
					WHEN ISNULL(c.worker_customer_no,0) = 0 THEN ''
					WHEN c.worker_customer_no <> 2653100 THEN cs2.esal1_desc
					ELSE cs3.esal1_desc
				END,
			secondSol.customer_no,
			secondSol.display_name,
			anonymous = kv.key_value,
			soft_credit_type = kv2.key_value,
			fund_description = coa.fund_description,
			printable_fund = coa.printable_fund,
			designation = coa.designation,
			overall_campaign = coa.overall_cm,
			campaign = cam.description,
			campaign_category = coa.cm_category,
			account_goal = coa.acct_goal,
			account_group = coa.acct_grp,
			appeal = a.description,
			media = mt.description,
			source = amt.source_name,
			notes = REPLACE(REPLACE(c.notes,CHAR(13),'/'),CHAR(10),'/'),
			cancel = c.cancel,
			pledge_status_desc = ps.description,
			billing_type = bt.description,
			batch = c.batch_no,
			KG_xfer_dt = c.KG_xfer_dt,
			KGift_desc = c.KGift_desc,
			stock_ticker = c.custom_2,
			contribution_detail1 = kv3.key_value,
			contribution_detail2 = kv4.key_value,
			agreement_date = c.custom_6,
			challenge_earned = c.custom_7,
			pg_instrument = kv5.key_value,
			solicitation = c.custom_0,
			cpf_flag = coa.cpf_flag,
			exh_prog_cat = coa.exh_prog_cat,
			full_fund_name = coa.full_fund,
			cm_category1 = coa.cm_category1,
			cm_intermediate1 = coa.cm1_intermediatelevel,
			cm_pillar1 = coa.cm1_main_level,
			cm_category2 = coa.cm_category2,
			cm_intermediate2 = coa.cm2_intermediate_level,
			cm_category3 = coa.cm_category3,
			cm_intermediate3 = coa.cm3_intermediate_level,
			custom_2 = c.custom_2,
			custom_9 = c.custom_9,
			main_customer_type = ct.description,
			original_cust_type = ct.description,
			cust2.sort_name,
			d2.description dept,
			chan.description
	FROM dbo.T_CONTRIBUTION AS c
	JOIN dbo.T_CUSTOMER AS cust ON c.customer_no = cust.customer_no
	JOIN dbo.TR_CUST_TYPE AS ct ON cust.cust_type = ct.id
	JOIN dbo.T_CREDITEE AS cr ON c.ref_no = cr.ref_no
	JOIN dbo.TR_CREDITEE_TYPE AS crt ON cr.creditee_type = crt.id
	JOIN dbo.T_CUSTOMER AS cust2 ON cr.creditee_no = cust2.customer_no
	JOIN dbo.TR_CUST_TYPE AS ct2 ON cust2.cust_type = ct2.id
	JOIN dbo.TX_CUST_SAL AS cs ON cr.creditee_no = cs.customer_no AND cs.default_ind = 'Y'
	JOIN dbo.TR_Batch_Period AS bp ON c.cont_dt BETWEEN bp.start_dt AND bp.end_dt
	JOIN dbo.TR_SALES_CHANNEL AS chan ON chan.id = c.channel
	LEFT JOIN dbo.T_KWCODED_VALUES AS kv ON c.custom_5 = kv.key_value AND kv.keyword_no IN (460)
	LEFT JOIN dbo.T_KWCODED_VALUES AS kv2 ON c.custom_1 = kv2.key_value AND kv2.keyword_no IN (456)
	JOIN dbo.T_CAMPAIGN AS cam ON c.campaign_no = cam.campaign_no
	JOIN dbo.LV_MOS_CHART_OF_ACCOUNTS AS coa ON c.fund_no = coa.fund_no AND cam.campaign_no = coa.campaign_no 
	JOIN dbo.T_APPEAL AS a ON c.appeal_no = a.appeal_no
	JOIN dbo.TR_MEDIA_TYPE AS mt ON c.media_type = mt.id
	JOIN dbo.TX_APPEAL_MEDIA_TYPE AS amt ON c.source_no = amt.source_no
	LEFT JOIN dbo.TR_BILLING_TYPE AS bt ON c.billing_type = bt.id
	LEFT JOIN dbo.T_KWCODED_VALUES AS kv3 ON c.custom_3 = kv3.key_value AND kv3.keyword_no IN (458)
	LEFT JOIN dbo.T_KWCODED_VALUES AS kv4 ON c.custom_4 = kv4.key_value AND kv4.keyword_no IN (459)
	LEFT JOIN dbo.T_KWCODED_VALUES AS kv5 ON c.custom_8 = kv5.key_value AND kv5.keyword_no IN (463)
	LEFT JOIN dbo.TX_CUST_SAL AS cs2 ON c.worker_customer_no = cs2.customer_no AND cs2.default_ind = 'Y'
	JOIN @camp_categories AS cat ON coa.cm_cat_id = cat.camp_category
	JOIN @designations AS des ON coa.desig_id = des.cont_designation
	LEFT JOIN dbo.T_PLAN AS p ON c.plan_no = p.plan_no
	LEFT JOIN dbo.TX_CUST_PLAN AS cp ON p.plan_no = cp.plan_no
	LEFT JOIN
    (
		SELECT cp1.plan_no, cp1.customer_no, secondarySol.display_name
		FROM dbo.TX_CUST_PLAN cp1
		INNER JOIN dbo.FT_CONSTITUENT_DISPLAY_NAME() AS secondarySol 
			ON cp1.customer_no = secondarySol.customer_no
			AND cp1.role_no = 11 -- Secondary Solicitor
	) secondSol
	ON secondSol.plan_no = p.plan_no
	LEFT JOIN (SELECT plan_no, ISNULL(COUNT(DISTINCT customer_no),0) AS sol_count FROM dbo.TX_CUST_PLAN GROUP BY plan_no) AS sc ON cp.plan_no = sc.plan_no
	LEFT JOIN dbo.TX_CUST_SAL AS cs3 ON cp.customer_no = cs3.customer_no AND cs3.default_ind = 'Y'
	LEFT JOIN TR_PLEDGE_STATUS PS ON C.pledge_status = PS.id 
	LEFT JOIN LT_ATTRIBUTE_DETAIL LT_AD ON LT_AD.type_no = 15 AND LT_AD.customer_no =  CASE 
					WHEN c.worker_customer_no = 2653100 AND c.plan_no IS NOT NULL THEN cs3.customer_no
					ELSE ISNULL(c.worker_customer_no,0) END
	LEFT OUTER JOIN LTR_ATTDET_DESCR2 d2 ON LT_AD.descr2_no = d2.id 
	WHERE c.custom_1 IN ('Primary Soft Credit')
	AND	c.cont_dt BETWEEN @cont_start_dt AND @cont_end_dt
	AND cr.creditee_type IN (5,12,15)
	AND c.cont_amt > 0

	IF ISNULL(@solicitor_str,'') <> ''
		SELECT 
			reference_no,
			customer_number,
			name,
			type,
			creditee_type,
			date,
			fiscal_year,
			fiscal_quarter,
			fiscal_period,
			contribution_amount,
			received_amount,
			sol_amount,
			sol_count,
			solicitor_number,
			solicitor_name,
			r.sec_sol_number,
			r.secondary_solicitor,
			anonymous,
			soft_credit_type,
			fund_description,
			printable_fund,
			designation,
			overall_campaign,
			campaign,
			campaign_category,
			account_goal,
			account_group,
			appeal,
			media,
			source,
			notes,
			cancel,
			pledge_status_desc,
			billing_type,
			batch,
			KG_xfer_dt,
			KGift_desc,
			stock_ticker,
			contribution_detail1,
			contribution_detail2,
			agreement_date,
			challenge_earned,
			pg_instrument,
			solicitation,
			cpf_flag,
			exh_prog_cat,
			full_fund_name,
			cm_category1,
			cm_intermediate1,
			cm_pillar1,
			cm_category2,
			cm_intermediate2,
			cm_category3,
			cm_intermediate3,
			custom_2,
			custom_9,
			main_customer_type,
			original_customer_type,
			sort_name,
			dept,
			Channel
		FROM @ResultsTbl r
			INNER JOIN @solicitors s
			ON r.solicitor_number = s.solicitor_id
	ELSE --if no solicitors are selected, return all results
		SELECT 
			reference_no,
			customer_number,
			name,
			type,
			creditee_type,
			date,
			fiscal_year,
			fiscal_quarter,
			fiscal_period,
			contribution_amount,
			received_amount,
			sol_amount,
			sol_count,
			solicitor_number,
			solicitor_name,
			r.sec_sol_number,
			r.secondary_solicitor,
			anonymous,
			soft_credit_type,
			fund_description,
			printable_fund,
			designation,
			overall_campaign,
			campaign,
			campaign_category,
			account_goal,
			account_group,
			appeal,
			media,
			source,
			notes,
			cancel,
			pledge_status_desc,
			billing_type,
			batch,
			KG_xfer_dt,
			KGift_desc,
			stock_ticker,
			contribution_detail1,
			contribution_detail2,
			agreement_date,
			challenge_earned,
			pg_instrument,
			solicitation,
			cpf_flag,
			exh_prog_cat,
			full_fund_name,
			cm_category1,
			cm_intermediate1,
			cm_pillar1,
			cm_category2,
			cm_intermediate2,
			cm_category3,
			cm_intermediate3,
			custom_2,
			custom_9,
			main_customer_type,
			original_customer_type,
			sort_name,
			dept,
			Channel
		FROM @ResultsTbl r


END



GO


