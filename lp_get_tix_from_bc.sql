USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[lp_get_tix_from_bc]    Script Date: 10/18/2017 3:59:15 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[lp_get_tix_from_bc]
@barcode_no VARCHAR(20)=NULL,
@ticket_no INT=NULL,
@perf_no INT= NULL,
@sli_no INT= NULL, 
@sli_status INT=NULL 
AS 
BEGIN
declare @pad_ticket_no varchar(8)
DECLARE @pad_perf_no VARCHAR(10) 

 CREATE TABLE #barcode_calc
(perf_no INT, ticket_no INT, pad_perf_no VARCHAR(8), pad_ticket_no VARCHAR(10),nscan_no VARCHAR(20),sli_no INT, order_no INT, sli_status INT) 

IF @barcode_no IS NOT NULL AND @perf_no IS NULL AND @ticket_no IS NULL 
BEGIN 
TRUNCATE TABLE #barcode_calc 
SET @ticket_no = (SELECT SUBSTRING(@barcode_no,3,2)+SUBSTRING(@barcode_no,7,2)+SUBSTRING(@barcode_no,11,2)
	+SUBSTRING(@barcode_no,15,2)+SUBSTRING(@barcode_no,19,2))
	SET @perf_no = (SELECT SUBSTRING(@barcode_no,5,2)+SUBSTRING(@barcode_no,9,2)+SUBSTRING(@barcode_no,13,2)
	+SUBSTRING(@barcode_no,17,2))
 INSERT #barcode_calc (perf_no,ticket_no,nscan_no) 
 SELECT @perf_no,@ticket_no,@barcode_no 
 END 
IF @barcode_no IS NULL AND @perf_no IS NOT NULL AND @ticket_no IS NOT NULL 
BEGIN 
 INSERT #barcode_calc 
 (ticket_no, perf_no) 
  SELECT @ticket_no,@perf_no 
END 

 UPDATE #barcode_calc
 SET pad_perf_no = RIGHT(REPLICATE('0', 8)
+ CAST(perf_no AS VARCHAR(8)), 8)

UPDATE #barcode_calc
 SET pad_ticket_no = RIGHT(REPLICATE('0', 10)
+ CAST(ticket_no AS VARCHAR(10)), 10)

UPDATE #barcode_calc 
 SET sli_no = (SELECT TOP 1(a.sli_no) FROM t_sub_lineitem a JOIN #barcode_calc b 
  ON a.perf_no = b.perf_no AND a.ticket_no=b.ticket_no) 

UPDATE #barcode_calc 
 SET order_no = (SELECT TOP 1(a.order_no) FROM t_sub_lineitem a JOIN #barcode_calc b 
  ON a.sli_no = b.sli_no) 

--select * from #barcode_calc

UPDATE #barcode_calc 
SET nscan_no = '99'+SUBSTRING(pad_ticket_no,1,2)+SUBSTRING(pad_perf_no,1,2)+SUBSTRING(pad_ticket_no,3,2)
+SUBSTRING(pad_perf_no,3,2)+SUBSTRING(pad_ticket_no,5,2)+SUBSTRING(pad_perf_no,5,2)
+SUBSTRING(pad_ticket_no,7,2)+SUBSTRING(pad_perf_no,7,2)+SUBSTRING(pad_ticket_no,9,2)

SELECT DISTINCT(nscan_no),perf_no,ticket_no,pad_perf_no,pad_ticket_no,sli_no,order_no 
FROM #barcode_calc 

END 
GO


