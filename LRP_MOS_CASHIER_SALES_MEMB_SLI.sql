USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LRP_MOS_CASHIER_SALES_MEMB_SLI]    Script Date: 2/1/2019 5:56:15 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_MOS_CASHIER_SALES_MEMB_SLI]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_MOS_CASHIER_SALES_MEMB_SLI] AS' 
END
GO

ALTER PROCEDURE [dbo].[LRP_MOS_CASHIER_SALES_MEMB_SLI]

	@start_dt DATETIME,
	@end_dt DATETIME,
    @mode INT,
	@one_step INT = NULL  -- 1 = yes, include  2 = no, do not include
    
AS
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	IF (@start_dt IS NULL OR @end_dt IS NULL)
	  BEGIN
	    RAISERROR('A valid start and end date must be provided',11,2) WITH NOWAIT
		RETURN
	  END
	
	DECLARE @members table (customer_no int, cust_memb_no int)

	IF @one_step = 2
	Insert into @members
		Select a.customer_no, a.cust_memb_no  from TX_CUST_MEMBERSHIP a left join TX_CONST_CUST b on a.customer_no = b.customer_no and constituency = 30
		where constituency IS NULL OR NRR_status IN ('NE','RI')
	ELSE 
	INSERT INTO @members
		SELECT a.customer_no, a.cust_memb_no FROM TX_CUST_MEMBERSHIP a 
	

    DECLARE @output_table TABLE (last_name VARCHAR(50), rep_name VARCHAR(100), memb_org VARCHAR(30), memb_level_code VARCHAR(10), memb_level_desc VARCHAR(30), 
			                     order_no INT, cust_memb_no INT, start_amt MONEY, paid_amt MONEY, rep_mos_id VARCHAR(25))

	DECLARE @af_memb_org INT

	SET		@af_memb_org = 5

    INSERT INTO @output_table ([last_name], [rep_name], [rep_mos_id], [memb_org] , [memb_level_code], [memb_level_desc], [order_no], [cust_memb_no], [start_amt], [paid_amt])
    SELECT	last_name = mu.lname,
			rep_name = mu.fname + ' ' + mu.lname,
            rep_mos_id = ISNULL(mu.phone_exch,''),
			memb_org = mo.description,
			memb_level_code = cm.memb_level,
			memb_level_desc = ml.description,
			order_no = o.order_no,
			cust_memb_no = cm.cust_memb_no,
			start_amt = ml.start_amt,
			paid_amt = sli.paid_amt
	FROM	dbo.T_SUB_LINEITEM AS sli
	JOIN	dbo.LX_SLI_MEMB AS sm ON sli.sli_no = sm.sli_no
	JOIN	dbo.T_ORDER AS o ON sli.order_no = o.order_no
	JOIN	dbo.T_METUSER AS mu ON sli.created_by = mu.userid
	JOIN	dbo.TX_CUST_MEMBERSHIP AS cm ON sm.cust_memb_no = cm.cust_memb_no
	JOIN	dbo.T_MEMB_LEVEL AS ml ON cm.memb_level = ml.memb_level AND cm.memb_org_no = ml.memb_org_no
	JOIN	dbo.T_MEMB_ORG AS mo ON cm.memb_org_no = mo.memb_org_no
	JOIN	dbo.T_ZONE AS z ON sli.zone_no = z.zone_no
	JOIN	@members memb ON memb.cust_memb_no = cm.cust_memb_no
	WHERE	sli.create_dt BETWEEN @start_dt AND @end_dt
	AND		sli.sli_status IN (3,6,12)
	AND		sli.paid_amt > 0
	AND		z.description NOT LIKE '%Upgrade%'


	UNION ALL

SELECT		last_name = mu.lname,
			rep_name = mu.fname +' ' + mu.lname,
            rep_mos_id = ISNULL(mu.phone_exch,''),
			memb_org = mo.description,
			memb_level_code = cm.memb_level,
			memb_level_desc = ml.description,
			order_no = oc.order_no,
			cust_memb_no = cm.cust_memb_no,
			start_amt = ml.start_amt,
			paid_amt = oc.paid_amt
	FROM	dbo.T_ORDER_CONTRIBUTION AS oc
	JOIN	dbo.T_CONTRIBUTION AS C ON oc.ref_no = c.ref_no
	JOIN	dbo.T_METUSER AS mu ON c.created_by = mu.userid
	JOIN	dbo.TX_CONT_MEMB AS cont ON oc.ref_no = cont.cont_ref_no
	JOIN	dbo.TX_CUST_MEMBERSHIP AS cm ON cont.cust_memb_no = cm.cust_memb_no
	JOIN	dbo.T_MEMB_LEVEL AS ml ON cm.memb_level = ml.memb_level AND cm.memb_org_no = ml.memb_org_no
	JOIN	dbo.T_MEMB_ORG AS mo ON cm.memb_org_no = mo.memb_org_no
	JOIN	@members memb ON memb.cust_memb_no = cm.cust_memb_no
	WHERE	c.create_dt BETWEEN @start_dt AND @end_dt
	AND		cm.memb_org_no = 5
	AND		oc.paid_amt > 0
	ORDER BY mu.lname, memb_org, start_amt, order_no


    --UPDATE @output_table
    --SET rep_mos_id = 'ID Not Found'
    --WHERE rep_mos_id = ''

    FINISHED:

        IF @mode = 1
            SELECT [last_name], [rep_name], [rep_mos_id], '' AS 'memb_org', '' AS 'memb_level_code', '' AS 'memb_level_desc', COUNT(DISTINCT [order_no]) AS 'order_no', 
                   COUNT(DISTINCT [cust_memb_no]) AS 'cust_memb_no', SUM([start_amt]) AS 'start_amt', SUM([paid_amt]) AS 'paid_amt'
            FROM @output_table
            GROUP BY [last_name], [rep_name], [rep_mos_id]
        ELSE
            SELECT [last_name], [rep_name], [rep_mos_id], [memb_org], [memb_level_code], [memb_level_desc], [order_no], [cust_memb_no], [start_amt], [paid_amt]
            FROM @output_table

END

--GRANT EXECUTE on LRP_MOS_CASHIER_SALES_MEMB_SLI to impusers

GO


EXECUTE [dbo].[LRP_MOS_CASHIER_SALES_MEMB_SLI] @start_dt = '1-1-2019', @end_dt = '1-31-2019', @mode = 2, @one_step = 2
    
