USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_SCHOLARSHIP_SCHOOL_SUMMARY]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_SCHOLARSHIP_SCHOOL_SUMMARY] AS' 
END
GO

ALTER PROCEDURE [dbo].[LRP_SCHOLARSHIP_SCHOOL_SUMMARY]
        @report_start_dt DATETIME = NULL, 
        @report_end_dt DATETIME = NULL,
        @customer_type_id INT = 0,
        @scholarship_id INT = 0,
        @list_no INT = NULL
WITH RECOMPILE AS BEGIN

    /*  Procedure Variables  */

        DECLARE @ord_no INT, @perf_date CHAR(10), @prod_name VARCHAR(50)
        DECLARE @start_date CHAR(10), @end_date CHAR(10)
        DECLARE @cust_type_id_school INT, @cust_type_id_school_official INT 
        DECLARE @list_name VARCHAR(50) = ''

    /*  Check Parameters  */

        IF @report_start_dt IS NULL
            SELECT @report_start_dt = CONVERT(DATETIME,'7-1-' 
                                    + CASE WHEN DATEPART(MONTH,GETDATE()) < 7 THEN CONVERT(VARCHAR(30),DATEPART(YEAR,DATEADD(YEAR,-1,GETDATE()))) 
                                                                              ELSE CONVERT(VARCHAR(30),DATEPART(YEAR,GETDATE())) END)
        IF @report_end_dt IS NULL
            SELECT @report_end_dt = CONVERT(DATETIME,'6-30-' 
                                    + CASE WHEN DATEPART(MONTH,GETDATE()) >= 7 THEN CONVERT(VARCHAR(30),DATEPART(YEAR,DATEADD(YEAR,1,GETDATE()))) 
                                                                               ELSE CONVERT(VARCHAR(30),DATEPART(YEAR,GETDATE())) END) + '23:59:59.967'

        SELECT @customer_type_id = ISNULL(@customer_type_id,0)
        SELECT @scholarship_id = ISNULL(@scholarship_id,0)

        SELECT @list_no = ISNULL(@list_no,0)

        SELECT @list_name = ISNULL([list_desc], '')
                            FROM [dbo].[T_LIST] 
                            WHERE [list_no] = @list_no

    /*  Make sure the date parameters include the full dates, starting at 00:00 and ending at 23:59  */

        SELECT @start_date = CONVERT(CHAR(10),@report_start_dt,111),
               @end_date = CONVERT(CHAR(10), @report_end_dt,111)

     /*  For the purposes of this report, the School and School Official Record customer types are the same thing.
         The id numbers are needed to combine the two  */

        SELECT @cust_type_id_school = [id] FROM [dbo].[TR_CUST_TYPE] WHERE [description] = 'School'
        SELECT @cust_type_id_school = ISNULL(@cust_type_id_school,0)

        SELECT @cust_type_id_school_official = [id] FROM [dbo].[TR_CUST_TYPE] WHERE [description] = 'School Official Record'
        SELECT @cust_type_id_school_official = ISNULL(@cust_type_id_school_official,0)

    /*  If customer type id selected was school official record, change to school  */

        IF @customer_type_id = @cust_type_id_school_official SELECT @customer_type_id = @cust_type_id_school
    
    /*  Create temporary tables  */
            
            IF OBJECT_ID('tempdb..#school_orders') IS NOT NULL DROP TABLE [#school_orders]

            CREATE TABLE [#school_orders] ([order_no] INT,
                                           [customer_no] INT,
                    CONSTRAINT [PK_trav_prog_orders_summary_order_no] PRIMARY KEY CLUSTERED ([order_no] ASC) ON [PRIMARY])

            IF OBJECT_ID('tempdb..#school_data') IS NOT NULL DROP TABLE [#school_data]

            CREATE TABLE [#school_data] ([data_type] VARCHAR(10), 
                                         [order_no] INT, 
                                         [sli_no] INT,
                                         [customer_no] INT, 
                                         [customer_type_no] INT, 
                                         [performance_date] CHAR(10), 
                                         [title_name] VARCHAR(30), 
                                         [scholarship_no] INT, 
                                         [production_name] VARCHAR(50),
                                         [zone_name] VARCHAR(30),
                                         [price_type_name] VARCHAR(30),
                                         [create_dt] DATETIME,
                                         [due_amount] MONEY,
                                         [total_paid] MONEY)

            IF OBJECT_ID('tempdb..#school_final') IS NOT NULL DROP TABLE [#school_final]

            CREATE TABLE [#school_final] ([data_type] VARCHAR(10), 
                                          [order_no] INT, 
                                          [order_row_no] INT,
                                          [order_exh_attend] INT,
                                          [production_name] VARCHAR(50), 
                                          [customer_type] VARCHAR(30), 
                                          [due_amount] MONEY, 
                                          [total_paid] MONEY, 
                                          [program_counter] int);

    /*  Generate a list of all school sales for designated date range  */

            WITH CTE_SCHOL_PAY ([order_no]) AS 
            (SELECT DISTINCT [order_no] 
             FROM [dbo].[LV_SCHOLARSHIP_PAYMENTS])
        INSERT INTO [#school_orders] ([order_no], [customer_no])
        SELECT DISTINCT det.[order_no], det.[customer_no]
        FROM [dbo].[LV_ORDER_DETAIL] AS det
             INNER JOIN [CTE_SCHOL_PAY] AS pay ON pay.[order_no] = det.[order_no]
             INNER JOIN [dbo].[T_ORDER] AS ord ON ord.[order_no] = det.[order_no] 
        WHERE [performance_dt] BETWEEN @report_start_dt AND @report_end_dt 
          AND ord.[MOS] IN (12, 13);     --12 = Schools / 13 = Web Sales School
          
        IF ISNULL(@list_no,0) > 0
            DELETE FROM [#school_orders]
            WHERE [customer_no] NOT IN (SELECT [customer_no] FROM [dbo].[T_LIST_CONTENTS] WHERE [list_no] = @list_no)

    /*  Get Traveling Programs Order and Payment Data  */
    
        INSERT INTO [#school_data] ([data_type],[order_no],[sli_no],[customer_no],[customer_type_no],[performance_date],[title_name],
                                    [scholarship_no],[production_name],[zone_name],[price_type_name],[create_dt],[due_amount],[total_paid])
        SELECT 'ord_info', 
                ord.[order_no], 
                ord.[sli_no], 
                ISNULL(ord.[customer_no],0), 
                cus.[cust_type], 
                ord.[performance_date], 
                ord.[title_name], 
                0, 
                ord.[production_name], 
                ord.[zone_name], 
                ord.[price_type_name], 
                ord.[create_dt], 
                ord.[due_amount],
                0.00 
        FROM [dbo].[LV_ORDER_DETAIL] AS ord (NOLOCK)
             INNER JOIN [#school_orders] AS lis (NOLOCK) ON lis.[order_no] = ord.[order_no]
             LEFT OUTER JOIN [dbo].[T_CUSTOMER] AS cus ON cus.[customer_no] = ord.[customer_no]

        INSERT INTO [#school_data] ([data_type],[order_no],[sli_no],[customer_no],[customer_type_no],[performance_date],[title_name],
                                    [scholarship_no],[production_name],[zone_name],[price_type_name],[create_dt],[due_amount],[total_paid])
        SELECT 'pay_info', 
               pay.[order_no], 
               0, 
               pay.[customer_no],
               ISNULL(cus.[customer_no],0),
               '',
               'payment',
               pay.[scholarship_no],
               pay.[scholarship_name], 
               '',
               pay.[payment_method],
               pay.[payment_dt],
               0.00,
               SUM(pay.[payment_amount])
        FROM [dbo].[LV_SCHOLARSHIP_PAYMENTS] AS pay (NOLOCK)
             INNER JOIN [#school_orders] AS lis ON lis.[order_no] = pay.[order_no]
             LEFT OUTER JOIN [dbo].[T_CUSTOMER] AS cus (NOLOCK) ON cus.[customer_no] = pay.[customer_no]
        GROUP BY pay.[order_no], pay.[customer_no], ISNULL(cus.[customer_no],0), pay.[scholarship_no], pay.[scholarship_name], pay.[payment_method], pay.[payment_dt]
        HAVING SUM([payment_amount]) <> 0.00;

    /*  Delete unwanted orders  */
  
        DELETE FROM [#school_data] 
        WHERE due_amount = 0.00 
          AND total_paid = 0.00;

        --DELETE FROM [#school_data] 
        --WHERE [order_no] IN (SELECT [order_no] 
        --                     FROM [#school_data] 
        --                     WHERE [price_type_name] = 'Interdepartmental Transfer')
        
        UPDATE [#school_data] 
        SET [production_name] = 'Unknown Scholarship' 
        WHERE [data_type] = 'pay_info' 
          AND  [production_name] = ''


    /*  Combine School and School Official Record into a single customer type  */

        UPDATE [#school_data]
        SET [customer_type_no] = @cust_type_id_school 
        WHERE [customer_type_no] = @cust_type_id_school_official

   /*  Remove unwanted constiruent types and schlarships based on parameters  */

            IF @customer_type_id > 0
                DELETE FROM [#school_data] 
                WHERE [order_no] NOT IN (SELECT [order_no] 
                                         FROM [#school_data] 
                                         WHERE [customer_type_no] = @customer_type_id)

            IF @scholarship_id > 0
                DELETE FROM [#school_data] 
                WHERE [scholarship_no] <> @scholarship_id

    /*  Create final data table  */

        INSERT INTO [#school_final] ([data_type],[order_no],[order_row_no],[production_name],[customer_type],[due_amount],[total_paid],[program_counter])
        SELECT dat.[data_type], 
               dat.[order_no],
               ROW_NUMBER() OVER(PARTITION BY dat.[order_no] ORDER BY dat.[order_no]) AS [order_row_no],
               dat.[production_name],
               ISNULL(ctp.[description],''),
               SUM(dat.[due_amount]),
               SUM(dat.[total_paid]), 0
        FROM [#school_data] dat (NOLOCK)
             LEFT OUTER JOIN [dbo].[T_CUSTOMER] AS cus ON cus.[customer_no] = dat.[customer_no]
             LEFT OUTER JOIN [dbo].[TR_CUST_TYPE] AS ctp ON ctp.[id] = cus.[cust_type]
        WHERE dat.[data_type] = 'pay_info'
        GROUP BY dat.[data_type], dat.[order_no], dat.[production_name], ISNULL(ctp.[description],'')
        HAVING SUM(dat.[total_paid]) <> 0.00;

    /*  Add Exhibit Hall Attendance to first row of each order  */

        WITH CTE_EXH_ATTEND
        AS (SELECT det.[order_no], 
                   det.[title_no], 
                   det.[title_name], 
                   SUM(det.[sale_total]) AS [sale_total]
            FROM [dbo].[LT_HISTORY_TICKET] AS det
                 INNER JOIN [#school_orders] AS ord ON ord.[order_no] = det.[order_no]
            WHERE [det].[title_no] = 27
            GROUP BY det.[order_no], det.[title_no], det.[title_name])
        UPDATE fin
        SET fin.order_exh_attend = ISNULL(cte.[sale_total],0)
        FROM [#school_final] AS fin
             LEFT OUTER JOIN [CTE_EXH_ATTEND] AS cte ON cte.[order_no] = fin.[order_no]
        WHERE fin.[order_row_no] = 1

        UPDATE [#school_final] 
        SET [customer_type] = 'School' 
        WHERE [customer_type] = 'School Official Record'

        UPDATE [#school_final] 
        SET [program_counter] = (SELECT COUNT(DISTINCT [sli_no]) 
                                 FROM [dbo].[T_SUB_LINEITEM] 
                                 WHERE [order_no] = [#school_final].[order_no] 
                                   AND [zone_no] = 92 
                                   AND sli_status IN (3,12))     --zone # 92 = 'Program Fee'
     
    FINISHED:
       
        /*  Select the final record set from #final_table  */
        
            SELECT [data_type], 
                   [order_no], 
                   [order_row_no],
                   ISNULL([order_exh_attend], 0) AS [order_exh_attend],
                   [production_name],
                   [program_counter],
                   [due_amount],
                   [total_paid],
                   [customer_type],
                   @list_no AS [list_no],
                   @list_name AS [list_name]
            FROM [#school_final]

        /*  Clean up and Destroy Temporary Tables  */

            IF OBJECT_ID('tempdb..#trav_prog_final') IS NOT NULL DROP TABLE [#trav_prog_final]
            IF OBJECT_ID('tempdb..#trav_prog_data') IS NOT NULL DROP TABLE [#trav_prog_data]
            IF OBJECT_ID('tempdb..#trav_prog_orders') IS NOT NULL DROP TABLE [#trav_prog_orders]

    DONE:

END
GO

GRANT EXECUTE ON [dbo].[LRP_SCHOLARSHIP_SCHOOL_SUMMARY] TO [ImpUsers] AS [dbo]
GO

EXECUTE [dbo].[LRP_SCHOLARSHIP_SCHOOL_SUMMARY] @report_start_dt = '7-1-2019', @report_end_dt = '6-30-2020', @customer_type_id = 0, @scholarship_id = 0, @list_no = 0--13944
--EXECUTE [dbo].[LRP_SCHOLARSHIP_TRAVELING_PROGRAM_SUMMARY] @report_start_dt = '1-1-2019', @report_end_dt = '1-31-2019', @customer_type_id = 0, @scholarship_id = 0

