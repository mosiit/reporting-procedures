USE [impresario]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- CHART OF ACCOUNTS Exception reporting on T_CAMPAIGN
-- DSJ 06/15/2016
-- DKJ 05/28/2019 -- This is no longer used by the Chart of Accounts Excpeption report.  WO 99685
-- MES 01/02/2021 Reinstated and Updated per SCTASK0001778
-- MES 01/20/2021 Updated per SCTASK0001778
--                Changed code to update each exception individually rather than in one big case statement
--                This will make it much easier to add and/or remove exceptions from the report when needed.
ALTER PROCEDURE [dbo].[LRP_EXCEPTION_COA_T_CAMPAIGN]
AS BEGIN

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    SET NOCOUNT ON;
    SET ANSI_WARNINGS OFF

    /*  Procedure Variables  */

        DECLARE @results TABLE ([campaign_no] INT,
                                [category] INT,
                                [description] VARCHAR(30),
                                [camp_type] CHAR(1),
                                [reason] VARCHAR(350));

    /*  Get Campaign Info  */

        INSERT INTO @results ([campaign_no], [category], [description], [camp_type], [reason])
        SELECT [campaign_no], 
               [category], 
               [description],
               [camp_type],
               ''
        FROM dbo.T_CAMPAIGN
        WHERE CAST(last_update_dt AS DATE) >= '2020-07-24';

    /*  Identify Exceptions One At a Time  */

        UPDATE @results
        SET [reason] = 'category is null and campaign_no not in (781,827, 829, 1989, 2010, 2011, 2016, 2156, 2166,2383)'
        WHERE [category] is null 
          AND [campaign_no] not in (781, 827, 829, 1989, 2010, 2011, 2016, 2156, 2166, 2383)

        UPDATE @results
        SET [reason] = 'category = 7 AND description NOT LIKE ''Innovator FY%'''
        WHERE [category] = 7 
          AND [description] NOT LIKE 'Innovator FY%'

        UPDATE @results
        SET [reason] = 'category = 8 AND description NOT LIKE ''Household Memb FY%'''
        WHERE [category] = 8 
          AND description NOT LIKE 'Household Memb FY%'

        UPDATE @results
        SET [reason] = 'category = 9 AND description NOT LIKE ''Library Memb FY%'''
        WHERE [category] = 9 
          AND [description] NOT LIKE 'Library Memb FY%' 

        UPDATE @results
        SET [reason] = 'category = 10 AND description NOT LIKE ''Corporate FY%'''
        WHERE [category] = 10 
          AND [description] NOT LIKE 'Corporate FY%'
          AND [description] NOT LIKE 'Corporate Spons FY%'

        UPDATE @results
        SET [reason] = 'category = 12 AND description NOT LIKE ''Foundation FY%'''
        WHERE [category] = 12 
          AND [description] NOT LIKE 'Foundation FY%' 

        UPDATE @results
        SET [reason] = 'category = 13 AND description NOT LIKE ''Government FY%'''
        WHERE [category] = 13 
          AND [description] NOT LIKE 'Government FY%' 

        UPDATE @results
        SET [reason] = 'category = 14 AND description NOT LIKE ''Fundraising Events FY%'' AND camp_type = ''C'''
        WHERE [category] = 14 
          AND [description] NOT LIKE 'Fundraising Events FY%' 
          AND [camp_type] = 'C'  

        UPDATE @results
        SET [reason] = 'category = 24 AND description NOT LIKE ''Skip FY%'''
        WHERE [category] = 24 
          AND [description] NOT LIKE 'Skip FY%' 

        UPDATE @results
        SET [reason] = 'category = 28 AND description NOT LIKE ''Annual Giving FY%'''
        WHERE [category] = 28 
          AND [description] NOT LIKE 'Annual Giving FY%' 
          AND [description] NOT LIKE 'Annual Giving No Memb FY%' 

        UPDATE @results
        SET [reason] = 'category = 29 AND description NOT LIKE ''Planned Gifts FY%'''
        WHERE [category] = 29 
          AND [description] NOT LIKE 'Planned Gifts FY%' 

        UPDATE @results
        SET [reason] = 'category = 30 AND description NOT LIKE ''Exec Passholder Memb FY%'''
        WHERE [category] = 30 
          AND [description] NOT LIKE 'Exec Passholder Memb FY%' 

        UPDATE @results
        SET [reason] = 'category = 31 AND description NOT LIKE ''Corporate Memb FY%'''
        WHERE [category] = 31 
          AND [description] NOT LIKE 'Corporate Memb FY%' 
          AND [description] NOT LIKE 'Corp Mem No Memb FY%'

        UPDATE @results
        SET [reason] = 'category = 36 AND description NOT LIKE ''Events_Marathon FY%'''
        WHERE [category] = 36 
          AND [description] NOT LIKE 'Events_Marathon FY%' 

        UPDATE @results
        SET [reason] = 'category = 37 AND description NOT LIKE ''Events_Washburn Chall FY%'''
        WHERE [category] = 37 
          AND [description] NOT LIKE 'Events_Washburn Chall FY%' 

        UPDATE @results
        SET [reason] = 'category = 40 AND description NOT LIKE ''Leadership Giving FY%'''
        WHERE [category] = 40 
          AND [campaign_no] NOT IN (765, 775)
          AND [description] NOT LIKE 'Leadership Giving FY%' 
          AND [description] NOT LIKE 'Leadership FY%' 

        UPDATE @results
        SET [reason] = 'category IN (41, 42, 43) AND camp_type = ''C'''
        WHERE [category] IN (41, 42, 43) 
          AND [camp_type] = 'C' 

        INSERT INTO @results ([campaign_no], [category], [description], [camp_type], [reason])
        SELECT 0,
               '',
               'Multiple Campaigns',
               '',
               'Fund # ' + CAST(cf.fund_no AS VARCHAR(10)) + ' in ' + CAST(COUNT(DISTINCT c.category) AS VARCHAR(10)) + ' Campaign Categories'
        FROM TX_CAMP_FUND AS cf
             INNER JOIN T_CAMPAIGN AS c ON cf.campaign_no = c.campaign_no
        GROUP BY cf.fund_no
        HAVING COUNT(DISTINCT c.category) > 1   

    /*  Delete records with no exceptions  0*/
    
        DELETE FROM @results
        WHERE [reason] = ''

    FINISHED:

        /*  Final Data Set For The Report  */

            SELECT [campaign_no],
                   ISNULL([category],0) AS [category],
                   [description],
                   [reason] 
            FROM @results
            
            
            ORDER BY ISNULL([category],0)


END
GO

--EXECUTE LRP_EXCEPTION_COA_T_CAMPAIGN




        