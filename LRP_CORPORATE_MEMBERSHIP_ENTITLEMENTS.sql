SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[LRP_CORPORATE_MEMBERSHIP_ENTITLEMENTS]
(
	@customer_no INT
	,@expr_dt DATETIME
)    
AS

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
SET NOCOUNT ON 
IF OBJECT_ID('tempdb..#Entitlements') IS NOT NULL BEGIN DROP TABLE #Entitlements END 

SELECT
	*
INTO 
	#Entitlements
FROM(
SELECT
	ce.*
	,cm.expr_dt
FROM 
	[dbo].[LV_CUST_ENTITLEMENTS] ce
	INNER JOIN [dbo].[VXS_CUST_MEMBERSHIP] cm ON cm.customer_no = ce.customer_no 
WHERE 
	ce.customer_no = 
		CASE
			WHEN @customer_no <> 0 THEN @customer_no
			ELSE ce.customer_no 
		END
	AND CAST(cm.expr_dt AS DATE) =
		CASE 
			WHEN @customer_no = 0 THEN CAST(@expr_dt AS DATE)
			ELSE CAST(cm.expr_dt AS DATE)
		END
	AND cm.current_status = 2
	AND cm.memb_org_no = 7 --7 = Corporaate Client
	AND ce.price_type_group = 8 --8 = Electronic Entitlements
	AND ce.num_start_ent > 0
) q


DECLARE @entitlementsTbl TABLE (
	customer_no INT 
	,total_entitlements INT
	,total_entitlements_used INT
	,total_entitlements_unused INT
	,[4-D Theater] INT 
	,[Butterfly Garden] INT 
	,[Exhibit Halls] INT 
	,[Omni] INT 
	,[Planetarium] INT 
	,entitlements_remaining VARCHAR(MAX) NULL
)
		
INSERT INTO @entitlementsTbl

SELECT DISTINCT
	et.customer_no
	,total_entitlements = SUM(et.num_start_ent) OVER(PARTITION BY et.customer_no)
	,total_entitlements_used = SUM((et.num_start_ent - et.num_remain_ent)) OVER(PARTITION BY et.customer_no)
	,total_entitlements_unused = SUM(et.num_remain_ent) OVER(PARTITION BY et.customer_no)
	,[4-D Theater] = (SELECT et2.num_remain_ent FROM #Entitlements et2 WHERE et2.entitlement_keyword_desc = '4-D Theater' AND et2.customer_no = et.customer_no)
	,[Butterfly Garden] = (SELECT et2.num_remain_ent FROM #Entitlements et2 WHERE et2.entitlement_keyword_desc = 'Butterfly Garden' AND et2.customer_no = et.customer_no)
	,[Exhibit Halls] = (SELECT et2.num_remain_ent FROM #Entitlements et2 WHERE et2.entitlement_keyword_desc = 'Exhibit Halls' AND et2.customer_no = et.customer_no)
	,[Omni] = (SELECT et2.num_remain_ent FROM #Entitlements et2 WHERE et2.entitlement_keyword_desc = 'Omni' AND et2.customer_no = et.customer_no)
	,[Planetarium] = (SELECT et2.num_remain_ent FROM #Entitlements et2 WHERE et2.entitlement_keyword_desc = 'Planetarium' AND et2.customer_no = et.customer_no)
	,entitlements_remaining = STUFF(
		(
			SELECT DISTINCT 
					', '+ CAST(et1.entitlement_keyword_desc AS VARCHAR(100)) + ': '+ CAST(et1.num_remain_ent AS VARCHAR(4))
				FROM
					#Entitlements et1
				WHERE 
					et1.customer_no = 
						CASE
							WHEN @customer_no <> 0 THEN @customer_no
							ELSE et1.customer_no 
						END
					AND CAST(et1.expr_dt AS DATE) =
						CASE 
							WHEN @customer_no = 0 THEN CAST(@expr_dt AS DATE)
							ELSE CAST(et1.expr_dt AS DATE)
						END
					AND et1.customer_no = et.customer_no
			FOR XMl PATH('') 
		),1,1,N''
	)
FROM 
	#Entitlements et 
SELECT
	et.customer_no
	,et.total_entitlements 
	,et.total_entitlements_used 
	,et.total_entitlements_unused 
	,et.[4-D Theater] 
	,et.[Butterfly Garden] 
	,et.[Exhibit Halls] 
	,et.[Omni] 
	,et.[Planetarium] 
	,et.entitlements_remaining
FROM
	@entitlementsTbl et 

GO
