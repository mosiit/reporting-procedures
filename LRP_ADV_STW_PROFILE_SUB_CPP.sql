USE impresario;
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_ADV_STW_PROFILE_SUB_CPP]') AND type in (N'P', N'PC'))
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_ADV_STW_PROFILE_SUB_CPP] AS';
GO

EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LRP_ADV_STW_PROFILE_SUB_CPP] TO [impusers], [tessitura_app]';
GO

ALTER PROCEDURE [dbo].[LRP_ADV_STW_PROFILE_SUB_CPP]
         @customer_no INT = NULL
AS BEGIN

    SET NOCOUNT ON;
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    
    /*  Procedute Variables and Temp Tables  */

        --NONE

    /*  Check Parameters */

        SELECT @customer_no = ISNULL(@customer_no, 0)
               
        /*  This procedure is a simple select statement
            It is in a procedure rather than a direct SQL Statement for consistency
            and to make it easier should a need for further data manipulation be needed in the future.  */

    FINISHED:

        /*  Get Final Data Set For Report  */

            SELECT p.[description],
                   e.[customer_no], 
                   e.[address]
            FROM [dbo].[T_EADDRESS] AS e
                 INNER JOIN [dbo].[TX_CONTACT_POINT_PURPOSE] AS c ON c.[contact_point_id] = e.[eaddress_no]
                 INNER JOIN [dbo].[TR_CONTACT_POINT_PURPOSE] AS p ON p.[id] = c.[purpose_id]
            WHERE e.[customer_no] = @customer_no
              AND e.[inactive] = 'N'
              AND e.[market_ind] = 'Y'
              AND e.[eaddress_type] <> 10
              AND c.[purpose_id] IN (10, 12)
            ORDER BY p.[description]

END
GO
