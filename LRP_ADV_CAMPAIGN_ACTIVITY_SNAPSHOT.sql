USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_ADV_CAMPAIGN_ACTIVITY_SNAPSHOT]') AND type IN (N'P', N'PC'))
BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_ADV_CAMPAIGN_ACTIVITY_SNAPSHOT] AS' 
    EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LRP_ADV_CAMPAIGN_ACTIVITY_SNAPSHOT] TO impusers'
END
GO

/*      LRP_ADV_CAMPAIGN_ACTIVITY_SNAPSHOT
        Retrieves information for two bar charts that show comparison of current fiscal year, previous fiscal year and a three-year average.  
        The first chart shows contributions made/pledges, and the second chart shows cash received.  
        Also retrieves data on contributions and cash for the last 30 and 90 days and for the same date range in the previous year
        
        NOTE: Uses LV_CAMPAIGN_ACTIVITY_CONTRIBUTIONS view which is a view that was created using the
              same logic in the MOS Advancement Contributions report (LRP_MOS_ADVANCEMENT_CONTRIBUTION_DETAIL).

        NOTE: For cash numbers, this procedure runs the LRP_ADV_CASH_REPORT procedure with the full three year date range to populate
              a temporary table.

*/
ALTER PROCEDURE [dbo].[LRP_ADV_CAMPAIGN_ACTIVITY_SNAPSHOT]
        @fiscal_year INT = NULL
WITH RECOMPILE AS BEGIN

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

    /*  Check Fiscal Year Parameter - If none passed, set to current  */
        IF @fiscal_year IS NULL SELECT @fiscal_year = dbo.LF_GetFiscalYear(GETDATE())

    /*  Procedure Variables  */
        
        --Determine current fiscal year date information
        DECLARE @fiscal_start_dt DATETIME = [dbo].[LF_GetFiscalYearStartDt](@fiscal_year)
        DECLARE @fiscal_end_dt DATETIME = [dbo].[LF_GetFiscalYearEndDt](@fiscal_year)
        DECLARE @current_dt DATETIME = GETDATE()
        IF @current_dt > @fiscal_end_dt SELECT @current_dt = @fiscal_end_dt

        --Determine previous fiscal year date information
        DECLARE @prev_fiscal_year INT = (@fiscal_year - 1)
        DECLARE @prev_fiscal_start_dt DATETIME  = [dbo].[LF_GetFiscalYearStartDt](@prev_fiscal_year)
        DECLARE @prev_fiscal_end_dt DATETIME = [dbo].[LF_GetFiscalYearEndDt](@prev_fiscal_year)
        DECLARE @prev_current_dt DATETIME = DATEADD(YEAR,-1,@current_dt)

        --Determine date information for fiscal year two years ago
        DECLARE @first_fiscal_year INT = (@fiscal_year - 2)
        DECLARE @first_fiscal_start_dt DATETIME  = [dbo].[LF_GetFiscalYearStartDt](@first_fiscal_year)
        DECLARE @first_fiscal_end_dt DATETIME = [dbo].[LF_GetFiscalYearEndDt](@first_fiscal_year)
        DECLARE @first_current_dt DATETIME = DATEADD(YEAR,-2,@current_dt)

        --Determine start dates for 30 and 90 days ago for both current and previous fiscal year
        DECLARE @fiscal_last_30_start_dt DATETIME = DATEADD(DAY,-30, @current_dt)
        DECLARE @fiscal_last_90_start_dt DATETIME = DATEADD(DAY,-90, @current_dt)
        DECLARE @prev_fiscal_last_30_start_dt DATETIME = DATEADD(year,-1,@fiscal_last_30_start_dt)
        DECLARE @prev_fiscal_last_90_start_dt DATETIME = DATEADD(year,-1,@fiscal_last_90_start_dt)

        DECLARE @fiscal_last_30_total DECIMAL(18,2) = 0.0,
                @fiscal_last_30_cash DECIMAL(18,2) = 0.0,
                @fiscal_last_90_total DECIMAL(18,2) = 0.0,
                @fiscal_last_90_cash DECIMAL(18,2) = 0.0

        DECLARE @prev_fiscal_last_30_total DECIMAL(18,2) = 0.0,
                @prev_fiscal_last_30_cash DECIMAL(18,2) = 0.0,
                @prev_fiscal_last_90_total DECIMAL(18,2) = 0.0,
                @prev_fiscal_last_90_cash DECIMAL(18,2) = 0.0

        DECLARE @fiscal_ytd_total DECIMAL(18,2) = 0.0,
                @fiscal_ytd_cash DECIMAL(18,2) = 0.0,
                @prev_fiscal_ytd_total DECIMAL(18,2) = 0.0,
                @prev_fiscal_ytd_cash DECIMAL(18,2) = 0.0,
                @first_fiscal_ytd_total DECIMAL(18,2) = 0.0,
                @first_fiscal_ytd_cash DECIMAL(18,2) = 0.0

        DECLARE @fiscal_last_30_cash_difference DECIMAL(18,2) = 0.0,
                @fiscal_last_90_cash_difference DECIMAL(18,2) = 0.0,
                @fiscal_last_30_total_difference DECIMAL(18,2) = 0.0,
                @fiscal_last_90_total_difference DECIMAL(18,2) = 0.0

        DECLARE @averag_ytd_total DECIMAL(18,2) = 0.0
        DECLARE @average_ytd_cash DECIMAL(18,2) = 0.0

        DECLARE @final_table TABLE ([report_order] INT NOT NULL DEFAULT (0),
                                    [report_group_no] INT NOT NULL DEFAULT (0),
                                    [report_group] VARCHAR(30) NOT NULL DEFAULT (0),
                                    [fiscal_year] INT NOT NULL DEFAULT (0),
                                    [fiscal_year_label] VARCHAR(30) NOT NULL DEFAULT (''), 
                                    [total_label] VARCHAR(30) NOT NULL DEFAULT (''),
                                    [total_amount] DECIMAL(18,2) NOT NULL DEFAULT (0.0))

    /*  Last 30 and Last 90 start dates should not go back beyond the start of their respective fiscal years  */

        IF @fiscal_last_30_start_dt <= @fiscal_start_dt SELECT @fiscal_last_30_start_dt = @fiscal_start_dt
        IF @fiscal_last_90_start_dt <= @fiscal_start_dt SELECT @fiscal_last_90_start_dt = @fiscal_start_dt
        IF @prev_fiscal_last_30_start_dt <= @prev_fiscal_start_dt SELECT @prev_fiscal_last_30_start_dt = @prev_fiscal_start_dt
        IF @prev_fiscal_last_90_start_dt <= @prev_fiscal_start_dt SELECT @prev_fiscal_last_90_start_dt = @prev_fiscal_start_dt

    /* Temporary Tables  */
           
        IF OBJECT_ID('tempdb..#adv_cash') IS NOT NULL DROP TABLE [#adv_cash]

        CREATE TABLE [#adv_cash] ([ref_no] INT NOT NULL DEFAULT (0),
                                  [customer_no] INT NOT NULL DEFAULT (0),
                                  [main_donor] VARCHAR(76) NOT NULL DEFAULT (''),
                                  [hardcredit_donor] VARCHAR(76) NOT NULL DEFAULT (''),
                                  [pay_type] VARCHAR(10) NOT NULL DEFAULT (''),
                                  [pay_dt] DATETIME NULL,
                                  [pay_amt] MONEY NOT NULL DEFAULT (0.0),
                                  [fund_description] VARCHAR(30) NOT NULL DEFAULT (''),
                                  [fyear] INT NOT NULL DEFAULT (0),
                                  [EF_Unr_Rest] VARCHAR(15) NOT NULL DEFAULT (''),
                                  [unr_notef_flag] VARCHAR(15) NOT NULL DEFAULT (''),
                                  [rest_bdgtrel_flag] VARCHAR(15) NOT NULL DEFAULT (''), 
                                  [cmcategory] VARCHAR(30) NOT NULL DEFAULT (''),
                                  [advrevrpt] VARCHAR(30) NOT NULL DEFAULT (''),
                                  [designation] VARCHAR(30) NOT NULL DEFAULT (''),
                                  [acctgoal] VARCHAR(30) NOT NULL DEFAULT (''),
                                  [acctgrp] VARCHAR(30) NOT NULL DEFAULT (''),
                                  [cust_type] VARCHAR(30) NOT NULL DEFAULT (''),
                                  [sort_name] VARCHAR(55) NULL DEFAULT (''))

    /*  Use the LRP_ADV_CASH_REPORT to get the cash numbers for the current and previous two years to date  */

        INSERT INTO [#adv_cash] ([ref_no], [customer_no], [main_donor], [hardcredit_donor], [pay_type], [pay_dt], [pay_amt], [fund_description],
                                 [fyear], [EF_Unr_Rest], [unr_notef_flag], [rest_bdgtrel_flag], [cmcategory], [advrevrpt], [designation], 
                                 [acctgoal], [acctgrp], [cust_type], [sort_name])
        EXECUTE [dbo].[LRP_ADV_CASH_REPORT] @startDate = @first_fiscal_start_dt, @endDate = @current_dt, @GIK = 0

    /*  Calculate the year to date totals for the current and previous two years  */

        SELECT @fiscal_ytd_cash =  SUM([pay_amt]) 
                                   FROM [#adv_cash] 
                                   WHERE [pay_dt] BETWEEN @fiscal_start_dt AND @current_dt

        SELECT @prev_fiscal_ytd_cash =  SUM([pay_amt]) 
                                        FROM [#adv_cash] 
                                        WHERE [pay_dt] BETWEEN @prev_fiscal_start_dt AND @prev_current_dt

        SELECT @first_fiscal_ytd_cash =  SUM([pay_amt]) 
                                         FROM [#adv_cash] 
                                         WHERE [pay_dt] BETWEEN @first_fiscal_start_dt AND @first_current_dt

        SELECT @average_ytd_cash = ((@fiscal_ytd_cash + @prev_fiscal_ytd_cash + @first_fiscal_ytd_cash) / 3.0)

    /*  Calculate the cash last 30 and 90 days for this and the previous fiscal year and the differences between both  */

        SELECT @fiscal_last_30_cash = SUM([pay_amt]) 
                                       FROM [#adv_cash] 
                                       WHERE [pay_dt] BETWEEN @fiscal_last_30_start_dt AND @current_dt

        SELECT @prev_fiscal_last_30_cash = SUM([pay_amt]) 
                                           FROM [#adv_cash] 
                                          WHERE [pay_dt] BETWEEN @prev_fiscal_last_30_start_dt AND @prev_current_dt

        SELECT @fiscal_last_30_cash_difference = (@fiscal_last_30_cash - @prev_fiscal_last_30_cash)

        SELECT @fiscal_last_90_cash = SUM([pay_amt]) 
                                       FROM [#adv_cash] 
                                       WHERE [pay_dt] BETWEEN @fiscal_last_90_start_dt AND @current_dt

        SELECT @prev_fiscal_last_90_cash = SUM([pay_amt]) 
                                           FROM [#adv_cash] 
                                          WHERE [pay_dt] BETWEEN @prev_fiscal_last_90_start_dt AND @prev_current_dt

        SELECT @fiscal_last_90_cash_difference = (@fiscal_last_90_cash - @prev_fiscal_last_90_cash)

    /*  Use the LV_CAMPAIGN_ACTIVITY CONTRIBUTIONS REPORT to calculate the current and previous two years contribution amounts  */

        SELECT @fiscal_ytd_total = SUM([contribution_amount]) 
                                   FROM  [dbo].[LV_CAMPAIGN_ACTIVITY_CONTRIBUTIONS]
                                   WHERE [contribution_date] BETWEEN @fiscal_start_dt AND @current_dt
                                     AND [campaign_category] <> 'Skip'

        SELECT @prev_fiscal_ytd_total = SUM([contribution_amount]) 
                                        FROM  [dbo].[LV_CAMPAIGN_ACTIVITY_CONTRIBUTIONS]
                                        WHERE [contribution_date] BETWEEN @prev_fiscal_start_dt AND @prev_current_dt
                                          AND [campaign_category] <> 'Skip'

        SELECT @first_fiscal_ytd_total =  SUM([contribution_amount]) 
                                          FROM  [dbo].[LV_CAMPAIGN_ACTIVITY_CONTRIBUTIONS]
                                          WHERE [contribution_date] BETWEEN @first_fiscal_start_dt AND @first_current_dt
                                            AND [campaign_category] <> 'Skip'

        SELECT @averag_ytd_total = ((@fiscal_ytd_total + @prev_fiscal_ytd_total + @first_fiscal_ytd_total) / 3.0)

    /*  Calculate the contributions last 30 and 90 days for this and the previous fiscal year and the differences between both  */

        SELECT @fiscal_last_30_total = SUM([contribution_amount]) 
                                       FROM  [dbo].[LV_CAMPAIGN_ACTIVITY_CONTRIBUTIONS]
                                       WHERE [contribution_date] BETWEEN @fiscal_last_30_start_dt AND @current_dt
                                         AND [campaign_category] <> 'Skip'

        SELECT @prev_fiscal_last_30_total = SUM([contribution_amount]) 
                                            FROM  [dbo].[LV_CAMPAIGN_ACTIVITY_CONTRIBUTIONS]
                                            WHERE [contribution_date] BETWEEN @prev_fiscal_last_30_start_dt AND @prev_current_dt
                                              AND [campaign_category] <> 'Skip'

        SELECT @fiscal_last_30_total_difference = (@fiscal_last_30_total - @prev_fiscal_last_30_total)

        SELECT @fiscal_last_90_total = SUM([contribution_amount]) 
                                       FROM  [dbo].[LV_CAMPAIGN_ACTIVITY_CONTRIBUTIONS]
                                       WHERE [contribution_date] BETWEEN @fiscal_last_90_start_dt AND @current_dt
                                         AND [campaign_category] <> 'Skip'

        SELECT @prev_fiscal_last_90_total = SUM([contribution_amount]) 
                                            FROM  [dbo].[LV_CAMPAIGN_ACTIVITY_CONTRIBUTIONS]
                                            WHERE [contribution_date] BETWEEN @prev_fiscal_last_90_start_dt AND @prev_current_dt
                                              AND [campaign_category] <> 'Skip'

        SELECT @fiscal_last_90_total_difference = (@fiscal_last_90_total - @prev_fiscal_last_90_total)

    /*  Insert all the values into a table variable with grouping and sorting identifiers  */

        INSERT INTO @final_table ([report_order], [report_group_no], [report_group], [fiscal_year], [fiscal_year_label], [total_label], [total_amount])
        VALUES (1, 1, 'contributions', @fiscal_year, 'Current', 'YTD Total', @fiscal_ytd_total),
               (2, 1, 'contributions', @prev_fiscal_year, 'Previous', 'Previous YTD Total', @prev_fiscal_ytd_total),
               (3, 1, 'contributions', @first_fiscal_year, 'Earliest', 'First YTD Total', @first_fiscal_ytd_total),
               (4, 1, 'contributions', 0, 'Average', '3 Year Average Total', @averag_ytd_total),
               (5, 2, 'cash', @fiscal_year, 'Current', 'YTD Cash', @fiscal_ytd_cash),
               (6, 2, 'cash', @prev_fiscal_year, 'Previous', 'Previous YTD Cash', @prev_fiscal_ytd_cash),
               (7, 2, 'cash', @first_fiscal_year, 'Earliest', 'First YTD Cash', @first_fiscal_ytd_cash),
               (8, 2, 'cash', 0, 'Average', '3 Year Average Cash', @average_ytd_cash),
               (9, 3, 'difference', 0, 'Difference', 'Overall Contributions Last 30', @fiscal_last_30_total_difference),
               (10, 3, 'difference', 0, 'Difference', 'Overall Contributions Last 90', @fiscal_last_90_total_difference),
               (11, 3, 'difference', 0, 'Difference', 'Overall Cash Last 30', @fiscal_last_30_cash_difference),
               (12, 3, 'difference', 0, 'Difference', 'Overall Cash Last 90', @fiscal_last_90_cash_difference)


    FINISHED:

        /*  Select final data set to return to the report  */

            SELECT [report_order], 
                   [report_group_no], 
                   [report_group], 
                   [fiscal_year], 
                   [fiscal_year_label], 
                   [total_label], 
                   [total_amount]
            FROM @final_table        

    DONE:

        IF OBJECT_ID('tempdb..#adv_cash') IS NOT NULL DROP TABLE [#adv_cash]
        
END
GO

EXECUTE [dbo].[LRP_ADV_CAMPAIGN_ACTIVITY_SNAPSHOT] @fiscal_year = 2020


