USE impresario;
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_ADV_STW_PROFILE_SUB_GIVING_HISTORY]') AND type in (N'P', N'PC'))
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_ADV_STW_PROFILE_SUB_GIVING_HISTORY] AS';
GO

EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LRP_ADV_STW_PROFILE_SUB_GIVING_HISTORY] TO [impusers], [tessitura_app]';
GO

ALTER PROCEDURE [dbo].[LRP_ADV_STW_PROFILE_SUB_GIVING_HISTORY]
        @customer_no INT = NULL
AS BEGIN

    SET NOCOUNT ON;
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

    /*  Pull the Giving History information for the designated customer  */
    
                WITH CTE_HARD_CREDIT ([customer_no], [hard_credit_sum])
                AS (SELECT c.[customer_no], 
                           SUM (c.[cont_amt])
                    FROM [dbo].[T_CONTRIBUTION] AS c 
                         INNER JOIN [dbo].[T_CAMPAIGN] AS g ON c.[campaign_no] = g.[campaign_no]
                    WHERE c.[customer_no] = @customer_no
                    GROUP BY c.[customer_no])
        SELECT TOP (1) fst.[customer_no],
               ISNULL(tot.[value], 0.0) AS [total_lifetime_giving],
               ISNULL(hrd.[hard_credit_sum], 0.0) AS [total_hard_credit],
               fst.[apply_date] AS [first_gift_date],
               fst.[sum_value] AS [first_gift],
               lts.[apply_date] AS [latest_gift_date],
               ISNULL(lts.[sum_value], 0.0) AS [latest_gift],
               lrg.[apply_date] AS [largest_gift_date],
               ISNULL(lrg.[sum_value], 0.0) AS [largest_gift]
        FROM [dbo].[LV_NS_FIRST_GP] AS fst
             LEFT OUTER JOIN [dbo].[LV_NS_LATEST_GP] AS lts ON lts.[customer_no] = fst.[customer_no]
             LEFT OUTER JOIN [dbo].[LV_NS_LARGEST_GP] AS lrg ON lrg.[customer_no] = fst.[customer_no]
             LEFT OUTER JOIN [dbo].[LT_NIGHTLY_SUMMARY] AS tot ON tot.[customer_no] = fst.[customer_no] AND tot.[section] = 'Comprehensive Lifetime Total'
             LEFT OUTER JOIN [CTE_HARD_CREDIT] AS hrd ON hrd.[customer_no] = fst.[customer_no]
        WHERE fst.[customer_no] = @customer_no
        ORDER BY [latest_gift_date] DESC, [first_gift_date], tot.[value] DESC

END;
GO


