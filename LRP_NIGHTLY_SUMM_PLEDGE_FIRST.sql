USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LRP_NIGHTLY_SUMM_PLEDGE_FIRST]    Script Date: 12/14/2020 2:24:18 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



ALTER PROCEDURE [dbo].[LRP_NIGHTLY_SUMM_PLEDGE_FIRST]
	@section VARCHAR(300),
	@sortOrder INT,
	@runBy	VARCHAR(300)
AS
-- ===============================================================
-- H. Sheridan, 4/4/2016 - Pledge_First
-- ===============================================================

BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	--SET @section = 'Pledge_First'
	--SET	@sortOrder = 11

	DECLARE @tblPassOne	TABLE	(
								customer_no		INT,
								value			MONEY,
								cont_date		DATE
								)

	DECLARE @tblPassTwo TABLE	(
								customer_no		INT,
								value			MONEY,
								cont_date		DATE
								)

	DECLARE @tblPassBoth TABLE	(
								customer_no		INT,
								value			MONEY,
								cont_date		DATE
								)

	DECLARE @tbl_ns TABLE ([customer_no] [int] NOT NULL,
						   [section] [varchar](300) NOT NULL,
						   [value] [money] NULL,
						   [apply_date] [varchar](20) NULL,
						   [sort_order] [int] NULL,
						   [created_on] [datetime] NULL,
						   [created_by] [varchar](300) NULL)

	INSERT INTO @tblPassOne
        SELECT  c.customer_no,
                ISNULL(SUM(c.cont_amt), 0),
                CAST(c.cont_dt AS DATE)
        FROM    T_CONTRIBUTION AS c
        INNER JOIN LTR_FUND_DETAIL AS f ON c.fund_no = f.fund_no
		--2020/12/14, H. Sheridan.  Service Now ticket SCTASK0001710 - join to campaign table
		JOIN	T_CAMPAIGN cam
		ON		cam.campaign_no = c.campaign_no
        WHERE   c.cont_type = 'P'
                AND c.custom_1 IN ('(none)', 'Matching Gift Credit')
                AND c.customer_no > 0
                AND c.cont_amt > 0
                AND CAST(c.cont_dt AS DATE) <= CAST(GETDATE() AS DATE)
				--2020/12/14, H. Sheridan.  Service Now ticket SCTASK0001710 - add criteria for campaign category
				AND	cam.category NOT IN (8,9)
				--2020/12/14, H. Sheridan.  Service Now ticket SCTASK0001710 - remove criteria for acct_grp_no
                --AND f.acct_grp_no <> '3'
                --AND c.cont_dt IN (SELECT    MIN(c1.cont_dt)
                --                  FROM      T_CONTRIBUTION AS c1
                --                  INNER JOIN LTR_FUND_DETAIL AS f1 ON c1.fund_no = f1.fund_no
                --                  WHERE     c1.cont_type = 'P'
                --                            AND c1.custom_1 IN ('(none)', 'Matching Gift Credit')
                --                            AND c1.customer_no > 0
                --                            AND c1.cont_amt > 0
                --                            AND CAST(c1.cont_dt AS DATE) <= CAST(GETDATE() AS DATE)
                --                            AND f1.acct_grp_no <> '3'
                --                            AND c.customer_no = c1.customer_no)
        GROUP BY c.customer_no, c.cont_dt

	--SELECT 'debug 1', * FROM @tblNightSumm

	INSERT INTO @tblPassTwo
        SELECT  i.customer_no,
                ISNULL(SUM(c.cont_amt), 0),
                CAST(c.cont_dt AS DATE)
        FROM    T_CONTRIBUTION AS c
        INNER JOIN T_CREDITEE AS t ON c.ref_no = t.ref_no
        INNER JOIN LTR_FUND_DETAIL AS f ON c.fund_no = f.fund_no
        INNER JOIN T_CUSTOMER AS i ON t.creditee_no = i.customer_no
		--2020/12/14, H. Sheridan.  Service Now ticket SCTASK0001710 - join to campaign table
		JOIN	T_CAMPAIGN cam
		ON		cam.campaign_no = c.campaign_no
        WHERE   c.cont_type = 'P'
                AND c.custom_1 IN ('Primary Soft Credit', 'Matching Gift Credit', 'Stewardship Soft Credit')
                AND c.customer_no > 0
                AND c.cont_amt > 0
                AND CAST(c.cont_dt AS DATE) <= CAST(GETDATE() AS DATE)
                AND t.creditee_type IN ('12', '15', '5', '10', '16', '1', '14')
				--2020/12/14, H. Sheridan.  Service Now ticket SCTASK0001710 - add criteria for campaign category
				AND	cam.category NOT IN (8,9)
				--2020/12/14, H. Sheridan.  Service Now ticket SCTASK0001710 - remove criteria for acct_grp_no
                --AND f.acct_grp_no <> '3'
                --AND c.cont_dt IN (SELECT    MIN(c1.cont_dt)
                --                  FROM      T_CONTRIBUTION AS c1
                --                  INNER JOIN T_CREDITEE AS t1 ON c1.ref_no = t1.ref_no
                --                  INNER JOIN LTR_FUND_DETAIL AS f1 ON c1.fund_no = f1.fund_no
                --                  INNER JOIN T_CUSTOMER AS i1 ON t1.creditee_no = i1.customer_no
                --                  WHERE     c1.cont_type = 'P'
                --                            AND c1.custom_1 IN ('Primary Soft Credit', 'Matching Gift Credit', 'Stewardship Soft Credit')
                --                            AND c1.customer_no > 0
                --                            AND c1.cont_amt > 0
                --                            AND CAST(c1.cont_dt AS DATE) <= CAST(GETDATE() AS DATE)
                --                            AND t1.creditee_type IN ('12', '15', '5', '10', '16', '1', '14')
                --                            AND f1.acct_grp_no <> '3'
                --                            AND i.customer_no = i1.customer_no)
        GROUP BY i.customer_no, c.cont_dt

	INSERT INTO @tblPassBoth
		SELECT * FROM @tblPassOne

	INSERT INTO @tblPassBoth
		SELECT * FROM @tblPassTwo

	--SELECT * FROM @tblPassBoth

	INSERT INTO @tbl_ns
		SELECT  t1.customer_no,
				@section,
				ISNULL(t1.value, 0),
				t1.cont_date,
				@sortOrder,
				GETDATE(),
				@runBy
		FROM	@tblPassBoth t1
		WHERE	t1.cont_date IN (SELECT   MIN(t2.cont_date)
								FROM	@tblPassBoth t2
								WHERE	t1.customer_no = t2.customer_no
								)
		--GROUP BY t1.customer_no, t1.cont_date

	--SELECT	DISTINCT
	--		[customer_no],
	--		[section],
	--		[value],
	--		[apply_date],
	--		[sort_order],
	--		[created_on],
	--		[created_by] 
	--FROM    @tbl_ns

	INSERT INTO LT_NIGHTLY_SUMMARY
		SELECT  DISTINCT [customer_no],
				[section],
				SUM(ISNULL([value], 0)),
				[apply_date],
				[sort_order],
				[created_on],
				[created_by] 
		FROM    @tbl_ns
		GROUP BY [customer_no], [section], [apply_date], [sort_order], [created_on], [created_by]

END

GO


