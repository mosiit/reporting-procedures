USE [impresario]
GO
/****** Object:  StoredProcedure [dbo].[LRP_BOARD_ActiveBoardHandbook]    Script Date: 11/13/2020 10:35:59 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[LRP_BOARD_ActiveBoardHandbook]
AS

SET NOCOUNT ON 
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

---- GET ALL ACTIVE BOARD MEMBERS
SELECT 
	a.individual_customer_no AS ID,
	ISNULL(sal.esal1_desc, '') AS name,
	c.sort_name, 
	at.description AS "Board Affliation", 
	ISNULL(a.title, '') AS Position, 
	ISNULL(a.note, '') AS note, 
	pref.PrefAddr, 
	dbo.LFS_BOARD_GetBoardCell(a.individual_customer_no, 1) AS cell,
	dbo.LFS_BOARD_GetBoardEmail(a.individual_customer_no, 1) BoardEmail,
	dbo.LFS_BOARD_GetSpouse(a.individual_customer_no) AS [spouse/partner],
	YEAR(a.end_dt) AS class,
	employer.JobTitle AS JobTitle_main,
	employer.Employer AS Employer_main,
	addJob.JobTitle_additional,
	addJob.Employer_additional,
	employer.BAddress1,
	employer.BAddress2,
	employer.BAddress3,
	employer.BCity,
	employer.BStateCd,
	employer.BZipCode,
	employer.BPhone,
	employer.BNotes AS BREST,
	home.Address1 AS hAddr1,
	home.Address2 AS hAddr2,
	home.Address3 AS hAddr3,
	home.City AS hCity,
	home.StateCd AS hState,
	home.ZipCode AS hZipCode,
	home.Phone AS hphone,
	home.Notes AS HREST,
	add1.Address1 AS a1Addr1,
	add1.Address2 AS a1Addr2,
	add1.Address3 AS a1Addr3,
	add1.City AS a1City,
	add1.StateCd AS a1State,
	add1.ZipCode AS a1ZipCode,
	add1.Phone AS a1phone,
	add1.Notes AS a1REST,
	seas1.Address1 AS s1Addr1,
	seas1.Address2 AS s1Addr2,
	seas1.Address3 AS s1Addr3,
	seas1.City AS s1City,
	seas1.StateCd AS s1State,
	seas1.ZipCode AS s1ZipCode,
	seas1.Phone AS s1phone,
	seas1.Notes AS s1REST,
	add2.Address1 AS a2Addr1,
	add2.Address2 AS a2Addr2,
	add2.Address3 AS a2Addr3,
	add2.City AS a2City,
	add2.StateCd AS a2State,
	add2.ZipCode AS a2ZipCode,
	add2.Phone AS a2phone,
	add2.Notes AS a2REST,
	seas2.Address1 AS s2Addr1,
	seas2.Address2 AS s2Addr2,
	seas2.Address3 AS s2Addr3,
	seas2.City AS s2City,
	seas2.StateCd AS s2State,
	seas2.ZipCode AS s2ZipCode,
	seas2.Phone AS s2phone,
	seas2.Notes AS s2REST,
	nt.note AS [Board Contact Info]
FROM T_AFFILIATION AS a 
INNER JOIN TR_AFFILIATION_TYPE AS at 
	ON a.affiliation_type_id = at.id
INNER JOIN dbo.T_CUSTOMER c
	ON c.customer_no = a.individual_customer_no
INNER JOIN dbo.TX_CUST_SAL sal
	ON c.customer_no = sal.customer_no
	AND sal.signor = 5
LEFT JOIN BI.VT_CUST_NOTE nt
	ON nt.customer_no = c.customer_no
	AND nt.note_type_no = 7 -- Board Contact Info
LEFT JOIN
(
	SELECT addr.customer_no, typ.description AS PrefAddr
	FROM t_address addr
	INNER JOIN tr_address_type typ
		ON addr.address_type = typ.id
	WHERE addr.primary_ind = 'Y' AND addr.inactive = 'N'
) pref
ON pref.customer_no = a.individual_customer_no
OUTER apply dbo.LFT_BOARD_GetEmployerInformation(a.individual_customer_no, 1) employer
OUTER APPLY dbo.LFT_BOARD_GetAddress(a.individual_customer_no, 'Home', 1) home
OUTER APPLY dbo.LFT_BOARD_GetAddress(a.individual_customer_no, 'Additional1', 1) add1
OUTER APPLY dbo.LFT_BOARD_GetAddress(a.individual_customer_no, 'Seasonal1', 1) seas1
OUTER APPLY dbo.LFT_BOARD_GetAddress(a.individual_customer_no, 'Additional2', 1) add2
OUTER APPLY dbo.LFT_BOARD_GetAddress(a.individual_customer_no, 'Seasonal2', 1) seas2
LEFT JOIN 
(
	SELECT a1.individual_customer_no,
		a1.title AS JobTitle_additional,
		emp.lname AS Employer_additional
	FROM dbo.T_AFFILIATION a1
	INNER JOIN dbo.T_CUSTOMER emp
		ON emp.customer_no = a1.group_customer_no
	WHERE a1.affiliation_type_id = 10209 -- Employee_Additional
		AND a1.inactive = 'N'
) addJob
ON addJob.individual_customer_no = a.individual_customer_no
WHERE a.group_customer_no = 5976 
	AND at.relationship_category_id = 17 -- Board
	AND at.description IN ('Active Overseer', 'Active Trustee', 'Trustee Emeriti', 'Overseer Emeriti', 
                           'Life Trustee','Active Museum Advisor','Museum Advisor Emeriti') -- Added [Active Museum Advisor] Request #REQ0001293 
ORDER BY c.sort_name
--214
