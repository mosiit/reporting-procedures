USE [impresario]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*****************************************************************
Date:		 01/08/2012
Created by:  Caryl Jones

Purpose: Report to show the Nscan barcode number for tickets bought to a specific performance.
******************************************************************/

ALTER PROCEDURE [dbo].[LP_RAH_NSCAN_BARCODE](
	@season INT, 
	@prod_season_no INT,
	@perf_no INT,
	@list_no INT 
)	
AS
SET NOCOUNT ON  

CREATE TABLE #barcode (
	show_code INT,
	type INT,
	barcode VARCHAR (50),
	customer_no INT, 
	name VARCHAR (100),
	add1 VARCHAR(64),
	add2 VARCHAR (64),
	add3 VARCHAR (64),
	town VARCHAR (30),
	county VARCHAR (20),
	postcode VARCHAR (10),
	tickets INT,
	value INT,
	cardnum INT,
	cancelled VARCHAR (1),
	reader VARCHAR (1) )

	
INSERT INTO #barcode (show_code, type, barcode, customer_no, name, add1, add2, add3, town, county, postcode, tickets, value, cancelled)

SELECT sl.perf_no AS show_code,
	sl.price_type AS type,
	barcode = dbo.lfs_rah_get_barcode (sl.ticket_no, sl.perf_no),
	c.customer_no, 
	c.lname AS name,
	ISNULL(street1,'') AS street1,
	ISNULL(street2, '') AS street2,
	ISNULL(street3,'') AS street3,
	ISNULL(city,'') AS city,
	ISNULL(state,'') AS state,
	ISNULL(postal_code,'') AS postal_code,
	COUNT (sl.price_type) AS tickets,
	sl.paid_amt AS value,
	CASE WHEN sl.sli_status NOT IN (3,12) THEN 'X' ELSE ' ' END
FROM t_sub_lineitem (NOLOCK) sl
JOIN t_order o (NOLOCK) ON o.order_no = sl.order_no
JOIN t_customer (NOLOCK) c ON c.customer_no = o.customer_no
LEFT JOIN dbo.T_ADDRESS adr
	ON adr.customer_no = o.customer_no
	AND adr.primary_ind = 'Y' AND adr.inactive = 'N'
WHERE sl.perf_no = @perf_no --select perf_no of the chosen performance
AND sl.ticket_no IS NOT NULL
AND sl.sli_status IN (3,12) -- don't want to see cancelled orders
GROUP BY perf_no,price_type,ticket_no,c.customer_no,lname,ISNULL(street1,''),
	ISNULL(street2, ''),ISNULL(street3,''), ISNULL(city,''),ISNULL(state,''),ISNULL(postal_code,''),
	price_type,paid_amt,sli_status
ORDER BY barcode

IF @list_no IS NULL 
	SELECT * FROM #barcode
ELSE --@list_no is NOT NULL
	SELECT b.*
	FROM #barcode b
	INNER JOIN dbo.T_LIST_CONTENTS lst
	ON b.customer_no = lst.customer_no
		WHERE list_no = @list_no

GO


