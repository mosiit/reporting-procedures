USE [impresario]
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_MEMBERSHIP_COUNTS]') AND type in (N'P', N'PC'))
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_MEMBERSHIP_COUNTS] AS'; 
GO

EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LRP_MEMBERSHIP_COUNTS] TO [impusers], [tessitura_app]';
GO

ALTER PROCEDURE [dbo].[LRP_MEMBERSHIP_COUNTS]
        @requestDt DATETIME = '12-31-2020',
        @memb_type_str VARCHAR(4000) = 'Household'
AS BEGIN

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    SET ANSI_WARNINGS OFF;
    SET NOCOUNT ON;

    /*  Procedure Variables and Temp Tables  */

        DECLARE @last_year_requestDt DATETIME = NULL, 
                @last_month_requestDt DATETIME = NULL;

        DECLARE @final_table TABLE ([column_name] VARCHAR(30) NOT NULL DEFAULT (''),
                                    [membership_month] VARCHAR(10) NOT NULL DEFAULT (''),
                                    [membership_month_sort] VARCHAR(10) NOT NULL DEFAULT (''),
                                    [membership_type] VARCHAR(30) NOT NULL DEFAULT (''),
                                    [data_type] VARCHAR(50) NOT NULL DEFAULT (''),
                                    [data_value] DECIMAL(18,4) NOT NULL DEFAULT (0.0))

        IF OBJECT_ID('tempdb..#ActiveMembershipsRaw') IS NOT NULL DROP TABLE [#ActiveMembershipsRaw]

        CREATE TABLE #ActiveMembershipsRaw ([fiscal_year] VARCHAR(4) NOT NULL DEFAULT (''),
                                            [column_name] VARCHAR(10) NOT NULL DEFAULT (''),
                                            [membership_month] VARCHAR(25) NOT NULL DEFAULT (''),
                                            [membership_month_sort] VARCHAR(25) NOT NULL DEFAULT (''),
                                            [customer_no] INT NOT NULL DEFAULT (0),
                                            [customer_display_name] VARCHAR(200) NOT NULL DEFAULT (''),
                                            [memb_level_category_description] VARCHAR(30) NOT NULL DEFAULT (''), 
                                            [memb_level_description] VARCHAR(30) NOT NULL DEFAULT (''), 
                                            [initiation_date] DATETIME NULL, 
                                            [expiration_date] DATETIME NULL, 
                                            [current_status_desc] VARCHAR(30) NOT NULL DEFAULT (''),
                                            [membership_counter] INT NOT NULL DEFAULT (0),
                                            [onestep_counter] INT NOT NULL DEFAULT (0),
                                            [premier_counter] INT NOT NULL DEFAULT (0));


    /*  Check Parameters  */

        SELECT @requestDt = ISNULL(@requestDt, GETDATE())
        SELECT @last_month_requestDt = DATEADD(MONTH, -1, @requestDt)
        SELECT @last_year_requestDt = DATEADD(YEAR, -1, @requestDt)

        --IF running for current month, make request date end of day today
        IF @requestDt > GETDATE() SELECT @requestDt = CONVERT(CHAR(10),GETDATE(),111) + ' 23:59:59.957'
    

    /*  Get raw data for requested month  */

        INSERT INTO [#ActiveMembershipsRaw] ([customer_no], [customer_display_name], [memb_level_category_description], [memb_level_description], [initiation_date],
                                             [expiration_date], [current_status_desc], [membership_counter], [onestep_counter], [premier_counter])
        EXECUTE [dbo].[LRP_ACTIVE_MEMBERSHIPS_RAW_DATA] @MembershipActiveDate = @requestDt, @category_str = @memb_type_str

        UPDATE [#ActiveMembershipsRaw]
        SET [fiscal_year] = CAST([dbo].[LF_GetFiscalYear] (@requestDt) AS VARCHAR(4)),
            [column_name] = 'MONTH01',
            [membership_month] = LEFT(DATENAME(MONTH, @requestDt),3) + '-' + RIGHT(DATENAME(YEAR,@requestDt),2),
            [membership_month_sort] = DATENAME(YEAR,@requestDt) + '/' +  FORMAT(DATEPART(MONTH, @requestDt), '00')
        WHERE [column_name] = ''

    /*  Get raw data for month before requested month  */

        INSERT INTO [#ActiveMembershipsRaw] ([customer_no], [customer_display_name], [memb_level_category_description], [memb_level_description], [initiation_date],
                                             [expiration_date], [current_status_desc], [membership_counter], [onestep_counter], [premier_counter])
        EXECUTE [dbo].[LRP_ACTIVE_MEMBERSHIPS_RAW_DATA] @MembershipActiveDate = @last_month_requestDt, @category_str = @memb_type_str

        UPDATE [#ActiveMembershipsRaw]
        SET [fiscal_year] = CAST([dbo].[LF_GetFiscalYear] (@last_month_requestDt) AS VARCHAR(4)),
            [column_name] = 'MONTH02',
            [membership_month] = LEFT(DATENAME(MONTH, @last_month_requestDt),3) + '-' + RIGHT(DATENAME(YEAR,@last_month_requestDt),2),
            [membership_month_sort] = DATENAME(YEAR,@last_month_requestDt) + '/' +  FORMAT(DATEPART(MONTH, @last_month_requestDt), '00')
        WHERE [column_name] = ''


    /*  Get raw data for same month in the previous year  */

        INSERT INTO [#ActiveMembershipsRaw] ([customer_no], [customer_display_name], [memb_level_category_description], [memb_level_description], [initiation_date],
                                             [expiration_date], [current_status_desc], [membership_counter], [onestep_counter], [premier_counter])
        EXECUTE [dbo].[LRP_ACTIVE_MEMBERSHIPS_RAW_DATA] @MembershipActiveDate = @last_year_requestDt, @category_str = @memb_type_str

        UPDATE [#ActiveMembershipsRaw]
        SET [fiscal_year] = CAST([dbo].[LF_GetFiscalYear] (@last_year_requestDt) AS VARCHAR(4)),
            [column_name] = 'MONTH03',
            [membership_month] = LEFT(DATENAME(MONTH, @last_year_requestDt),3) + '-' + RIGHT(DATENAME(YEAR,@last_year_requestDt),2),
            [membership_month_sort] = DATENAME(YEAR,@last_year_requestDt) + '/' +  FORMAT(DATEPART(MONTH, @last_year_requestDt), '00')
        WHERE [column_name] = ''


    /*  Aggregate Enrolled Members */                               
                               
        INSERT INTO @final_table ([column_name],[membership_month],[membership_month_sort],[membership_type],[data_type],[data_value])
        SELECT mem.[column_name],
               mem.membership_month,
               mem.membership_month_sort,
               mem.[memb_level_category_description],
               'Active Enrolled Members',
               COUNT(DISTINCT [mem].[customer_no])
            FROM [#ActiveMembershipsRaw] AS mem
            GROUP BY mem.[column_name],mem.membership_month, mem.membership_month_sort, mem.[memb_level_category_description]


    /*  Aggregate One Step Members */                               

        INSERT INTO @final_table ([column_name],[membership_month],[membership_month_sort],[membership_type],[data_type],[data_value])
        SELECT mem.[column_name],
               mem.membership_month,
               mem.membership_month_sort,
               mem.[memb_level_category_description],
               '% of Active Members On One Step',
               SUM(onestep_counter) / CONVERT(DECIMAL(18,4),COUNT(*))
            FROM [#ActiveMembershipsRaw] AS mem
            GROUP BY mem.[column_name],mem.membership_month, mem.membership_month_sort, mem.[memb_level_category_description]
            ORDER BY mem.[membership_month_sort] DESC

    /*  Aggregate Premier Members */                               

        INSERT INTO @final_table ([column_name],[membership_month],[membership_month_sort],[membership_type],[data_type],[data_value])
        SELECT mem.[column_name],
               mem.membership_month,
               mem.membership_month_sort,
               mem.[memb_level_category_description],
               '% of Active Premier Members',
               SUM(premier_counter) / CONVERT(DECIMAL(18,4),COUNT(*))
            FROM [#ActiveMembershipsRaw] AS mem
            GROUP BY mem.[column_name],mem.membership_month, mem.membership_month_sort, mem.[memb_level_category_description]
            ORDER BY mem.[membership_month_sort] DESC

    FINISHED:

        /*  Get final data set to pass back to the report - One row per fiscal year  */

            SELECT [column_name],
                   [membership_month],
                   [membership_month_sort],
                   [membership_type],
                   [data_type],
                   [data_value]
            FROM @final_table
            ORDER BY [column_name], [membership_month]
            
    DONE:

END
GO

--EXECUTE [dbo].[LRP_MEMBERSHIP_COUNTS] @requestDt = '12-31-2020', @memb_type_str = 'Household'--,"Library",Corporate'
--EXECUTE [dbo].[LRP_ACTIVE_MEMBERSHIPS] @MembershipActiveDate = '12-31-2020', @category_str = 'Household'
