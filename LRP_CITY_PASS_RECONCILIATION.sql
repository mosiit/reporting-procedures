
USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_CITY_PASS_RECONCILIATION]') AND type IN (N'P', N'PC'))
BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_CITY_PASS_RECONCILIATION] AS' 
    EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LRP_CITY_PASS_RECONCILIATION] TO impusers'
END
GO

ALTER PROCEDURE [dbo].[LRP_CITY_PASS_RECONCILIATION]
        @report_dt AS DATETIME = NULL
AS BEGIN;

    /*  Procedure Variables  */

        DECLARE @citypass_pre_paid_voucher AS INT = 152;        --CityPASS Pre-Paid Voucher Payment from TR_PAYMENT_METHOD
    
    /*  Check Parameters  */

        SELECT @report_dt = ISNULL(@report_dt,GETDATE());
    
    /*  Pull City Pass Voucher Transaction for the designated date  */
    
        WITH CTE_PAYMENTS ([order_no], [payment_method])
        AS (
            SELECT DISTINCT trx.[order_no], pay.pmt_method
            FROM [dbo].[T_PAYMENT] AS pay 
                 INNER JOIN [dbo].[T_TRANSACTION] AS trx ON trx.transaction_no = pay.transaction_no AND trx.sequence_no = pay.sequence_no
            WHERE DATEDIFF(DAY,[pmt_dt],@report_dt) = 0
              AND [pmt_method] = @citypass_pre_paid_voucher
            )
        SELECT sli.[order_no],
               sli.[perf_no],
               sli.[zone_no],
               prf.[title_no],
               prf.[title_name],
               prf.[production_no],
               prf.[production_name],
               sli.[price_type],
               CASE WHEN typ.[description] = 'Child' THEN 'Youth'
                    ELSE typ.[description] END AS [price_type_name],
               sli.[sli_status],
               sta.[description] AS [subline_status],
               sli.[created_by],
               ISNULL(usr.[fname], '') + ' ' + ISNULL(usr.[lname], '') AS [user_name],
               sli.[create_dt],
               1 AS [item_counter]
        FROM [dbo].[T_SUB_LINEITEM] AS sli
             INNER JOIN CTE_PAYMENTS AS pay ON pay.[order_no] = sli.[order_no]
             INNER JOIN [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] AS prf ON prf.[performance_no] = sli.[perf_no] AND prf.[performance_zone] = sli.[zone_no]
             INNER JOIN dbo.T_METUSER AS usr ON usr.[userid] = sli.[created_by]
             INNER JOIN [dbo].[TR_PRICE_TYPE] AS typ ON typ.[id] = sli.[price_type]
             INNER JOIN [dbo].[TR_SLI_STATUS] AS sta ON sta.[id] = sli.[sli_status]
        WHERE sli.[sli_status] IN (3, 12)  --3 = Seated, Paid / 12 = Ticketed, Paid
          AND prf.[title_no] = 7179        --7179 = CityPASS
        ORDER BY sli.order_no, typ.[description]

    DONE:
    
END
GO

EXECUTE [dbo].[LRP_CITY_PASS_RECONCILIATION] '1-1-2019';





    