USE impresario;
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_SPECIAL_EXHIBITION_SALES_GRID]') AND type in (N'P', N'PC')) BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_SPECIAL_EXHIBITION_SALES_GRID] AS';
    EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LRP_SPECIAL_EXHIBITION_SALES_GRID] TO impusers';
END;
GO

ALTER PROCEDURE [dbo].[LRP_SPECIAL_EXHIBITION_SALES_GRID]
        @sale_start_dt DATETIME = '5-15-2019',
        @sale_end_dt DATETIME = '1-10-2020 23:59:59',
        @perf_start_dt DATETIME = '6-15-2019',
        @perf_end_dt DATETIME = '1-10-2020 23:59:59',
        @paid_tickets_only CHAR(1) = 'N',
        @totals_only CHAR(1) = 'Y'
AS BEGIN

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    SET NOCOUNT ON;
    SET ANSI_WARNINGS OFF;

    DECLARE @special_exhibition_no AS INT = 37;           --37 = Special Exhibitions Title Number
    DECLARE @audio_tour_no AS INT = 17828;                --17828 = Audio Tour Title Number
    DECLARE @special_exhibition_name AS VARCHAR(30) = ''

    DECLARE @include_audio_tour CHAR(1) = 'N'  --WAS A PARAMETER.  SET AS A CONSTANT FOR NOW.  MAY BE A PARAMETER AGAIN IN FUTURE

    --DROP TABLE [#special_sales]

    CREATE TABLE [#special_sales] ([perf_dt] DATETIME NULL,
                                   [perf_day] VARCHAR(10) NOT NULL DEFAULT(''),
                                   [sale_dt] DATETIME NULL,
                                   [sale_day] VARCHAR(10) NOT NULL DEFAULT(''),
                                   [source_no] INT NOT NULL DEFAULT(0),
                                   [source_name] VARCHAR(30) NOT NULL DEFAULT(''), 
                                   [source_abbrev] VARCHAR(10) NOT NULL DEFAULT(''),
                                   [title_no] INT NOT NULL DEFAULT(0),
                                   [title_name] VARCHAR(30) NOT NULL DEFAULT(''),
                                   [tickets_sold] INT NOT NULL DEFAULT(0),
                                   [due_amount] DECIMAL(18,2) NOT NULL DEFAULT(0.0),
                                   [paid_amount] DECIMAL(19,2) NOT NULL DEFAULT(0.0),
                                   )


    SELECT @special_exhibition_name = ISNULL(MAX([production_name]),'')
                                      FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] 
                                      WHERE [performance_dt] between @perf_start_dt AND @perf_end_dt
                                        AND [title_no] = @special_exhibition_no




    INSERT INTO [#special_sales] ([perf_dt], [perf_day], [sale_dt], [sale_day], [source_no], [source_name], [source_abbrev], 
                                  [title_no], [title_name],[tickets_sold], [due_amount], [paid_amount])
        SELECT CAST(prf.[performance_dt] AS DATE),
               CAST(FORMAT(prf.[performance_dt],'MM/dd') AS VARCHAR(10)),
               CAST(sli.[create_dt] AS DATE),
               CAST(FORMAT(sli.[create_dt],'MM/dd') AS VARCHAR(10)),
               ord.[source_no],
               med.[source_name],
               CASE WHEN @totals_only = 'Y' THEN '' 
                    ELSE LEFT(med.[source_name],3) END,
               prf.[title_no],
               prf.[title_name],
               1,
               sli.[due_amt],
               sli.[paid_amt]
        FROM [dbo].[T_SUB_LINEITEM] AS sli
             INNER JOIN [dbo].[T_ORDER] AS ord ON ord.[order_no] = sli.[order_no]
             LEFT OUTER JOIN dbo.LV_PRODUCTION_ELEMENTS_PERFORMANCE AS prf ON prf.[performance_no] = sli.[perf_no] AND prf.[performance_zone] = sli.[zone_no]
             LEFT OUTER JOIN dbo.TX_APPEAL_MEDIA_TYPE AS med ON med.[source_no] = ord.[source_no]
        WHERE  sli.create_dt BETWEEN @sale_start_dt AND @sale_end_dt
          AND  prf.performance_dt BETWEEN @perf_start_dt AND @perf_end_dt
          AND prf.title_no IN (@special_exhibition_no, @audio_tour_no)
          AND sli.[sli_status] IN (2, 3, 12)
          AND ord.[mos] NOT IN (9,33,34,35,36,37,38)
      
    IF @include_audio_tour <> 'Y' 
        DELETE FROM [#special_sales] WHERE [title_no] = @audio_tour_no

    IF @paid_tickets_only = 'Y'
        DELETE FROM [#special_sales] WHERE [due_amount] > 0.0 AND [paid_amount] = 0.0

    IF @special_exhibition_name <> ''
        UPDATE[#special_sales]
        SET [title_name] = @special_exhibition_name
        WHERE [title_no] = @special_exhibition_no


    --INSERT INTO [#special_sales] ([perf_dt], [perf_day], [sale_dt], [sale_day], [source_no], [source_name], [source_abbrev], 
    --                              [title_no], [title_name],[tickets_sold], [due_amount], [paid_amount])

    --    SELECT [return_dt],
    --           CAST(FORMAT([return_dt],'MM/dd') AS VARCHAR(10)),
    --           CAST(@sale_start_dt AS DATE),
    --           CAST(FORMAT(@sale_start_dt,'MM/dd') AS VARCHAR(10)),
    --           0,
    --           'Web',
    --           CASE WHEN @totals_only = 'Y' THEN '' 
    --                ELSE 'Web' END,
    --           @special_exhibition_no,
    --           @special_exhibition_name,
    --           0,
    --           0.0,
    --           0.0
    --    FROM [dbo].[LFT_INDIVIDUAL_DATES](@perf_start_dt, @perf_end_dt)
    --    WHERE [return_dt] NOT IN (SELECT [perf_dt] 
    --                              FROM [#special_sales])


    --INSERT INTO [#special_sales] ([perf_dt], [perf_day], [sale_dt], [sale_day], [source_no], [source_name], [source_abbrev], 
    --                              [title_no], [title_name],[tickets_sold], [due_amount], [paid_amount])

    --    SELECT @perf_start_dt,
    --           CAST(FORMAT(@perf_start_dt,'MM/dd') AS VARCHAR(10)),
    --           CAST([return_dt] AS DATE),
    --           CAST(FORMAT([return_dt],'MM/dd') AS VARCHAR(10)),
    --           0,
    --           'Web',
    --           CASE WHEN @totals_only = 'Y' THEN '' 
    --                ELSE 'Web' END,
    --           @special_exhibition_no,
    --           @special_exhibition_name,
    --           0,
    --           0.0,
    --           0.0
    --    FROM [dbo].[LFT_INDIVIDUAL_DATES](@sale_start_dt, @sale_end_dt)
    --    WHERE [return_dt] NOT IN (SELECT [sale_dt] 
    --                              FROM [#special_sales])
   

    FINISHED:
    
        SELECT [perf_dt], 
               [perf_day], 
               [sale_dt], 
               [sale_day], 
               [source_no], 
               [source_name], 
               [source_abbrev], 
               [title_no], 
               [title_name],
               [tickets_sold], 
               [due_amount],
               [paid_amount]
        FROM [#special_sales]

   DONE:     

END;
GO


EXECUTE [dbo].[LRP_SPECIAL_EXHIBITION_SALES_GRID]

    --SELECT * FROM dbo.TR_MOS




--SELECT * FROM LTR_ATTENDANCE_BUDGET_data