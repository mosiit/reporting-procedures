USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LRP_ACTIVE_MEMBERSHIP_BUT_NOT_VISITED_MOS]
	@membInitDt_start DATE, -- leave as DATE, not DATETIME
	@membInitDt_end DATE, -- leave as DATE, not DATETIME,
	@membOrgNo_Str VARCHAR(MAX) = NULL 
AS 
BEGIN

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
SET NOCOUNT ON;

DECLARE @membOrgNo_Tbl TABLE (memb_org_no INT NOT NULL)

IF ISNULL(@membOrgNo_Str,'') <> ''
	INSERT INTO @membOrgNo_Tbl (memb_org_no)
	SELECT Element From dbo.FT_SPLIT_LIST(@membOrgNo_Str,',')
ELSE
	INSERT INTO @membOrgNo_Tbl (memb_org_no)
	SELECT memb_org_no
	FROM dbo.T_MEMB_ORG;

-- Find all memberships that started within a certain date range, and return all customer_no that did not visit MOS since that time till now. 

-- All active memberships that began between @membInitDt_start AND @membInitDt_end
WITH ActiveMemberships
AS 
(
	SELECT m.customer_no, m.memb_level, m.memb_org_no,
		CAST(m.init_dt AS DATE) AS init_dt, CAST(m.expr_dt AS DATE) AS expr_dt, m.current_status, m.cur_record
	FROM dbo.TX_CUST_MEMBERSHIP m
	INNER JOIN @membOrgNo_Tbl memb
		ON memb.memb_org_no = m.memb_org_no
	WHERE CAST(m.init_dt AS DATE) BETWEEN @membInitDt_start AND @membInitDt_end
		AND m.current_status = 2
),
-- Max attend_dt for all constituents with an Active Membership
Attendance
AS
(
	SELECT DISTINCT a.customer_no, MAX(CAST(a.attend_dt AS DATE)) AS maxAttendDt
	FROM dbo.T_ATTENDANCE a
	WHERE  a.customer_no IN (SELECT DISTINCT customer_no FROM ActiveMemberships)
		AND CAST(a.attend_dt AS DATE) >= @membInitDt_start -- max visit date after @membInitDt_start
	GROUP BY a.customer_no
)
SELECT memb.customer_no, memb.memb_level, memb.memb_org_no, memb.init_dt, memb.expr_dt, memb.current_status, memb.cur_record, att.maxAttendDt
FROM ActiveMemberships memb
LEFT JOIN Attendance att
	ON att.customer_no = memb.customer_no
	AND memb.init_dt <= att.maxAttendDt -- want to make sure they visited MOS after their membership started 
WHERE att.maxAttendDt IS NULL 

END
GO

GRANT EXECUTE ON [dbo].[LRP_ACTIVE_MEMBERSHIP_BUT_NOT_VISITED_MOS] TO ImpUsers;
GO
