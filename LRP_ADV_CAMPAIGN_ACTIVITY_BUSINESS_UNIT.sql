USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_ADV_CAMPAIGN_ACTIVITY_BU]') AND type IN (N'P', N'PC'))
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_ADV_CAMPAIGN_ACTIVITY_BU] AS'
GO

EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LRP_ADV_CAMPAIGN_ACTIVITY_BU] TO [impusers], [tessitura_app]'
GO

/*      LRP_ADV_CAMPAIGN_ACTIVITY_BU

        Retrueves fiscal year closed and booked (contributions) broken down by business unit, 

        NOTE: Uses LV_CAMPAIGN_ACTIVITY_CONTRIBUTIONS view which is a view that was created using the
              same logic in the MOS Advancement Contributions report (LRP_MOS_ADVANCEMENT_CONTRIBUTION_DETAIL).

*/
ALTER PROCEDURE [dbo].[LRP_ADV_CAMPAIGN_ACTIVITY_BU]
        @fiscal_year INT = 2021
WITH RECOMPILE AS BEGIN

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

    /*  Check Fiscal Year Parameter - If none passed, set to current  */
        IF @fiscal_year IS NULL SELECT @fiscal_year = dbo.LF_GetFiscalYear(GETDATE())

    /*  Procedure Variables  */

        --Hard coded for the campaign (may change)
        DECLARE @overall_campaign_no INT = 1

        --Determine current fiscal year date information
        DECLARE @fiscal_start_dt DATETIME = [dbo].[LF_GetFiscalYearStartDt](@fiscal_year)
        DECLARE @fiscal_end_dt DATETIME = [dbo].[LF_GetFiscalYearEndDt](@fiscal_year)
        DECLARE @fiscal_end_dt_formatted VARCHAR(50) = FORMAT(@fiscal_end_dt,'MMMM d, yyyy','en-US')
        DECLARE @current_dt DATETIME = GETDATE()

        IF @current_dt > @fiscal_end_dt SELECT @current_dt = @fiscal_end_dt

        DECLARE @campaign_goal DECIMAL (18,2) = 0.00, @fiscal_year_goal DECIMAL(18,2) = 0.0

    /*  Temporary Tables  */

        IF OBJECT_ID('tempdb..#bu_detail') IS NOT NULL DROP TABLE [#bu_detail]

        CREATE TABLE [#bu_detail] ([business_unit] VARCHAR(50) NOT NULL DEFAULT (''),
                                   [business_unit_sort] VARCHAR(75) NOT NULL DEFAULT (''),
                                                                      [campaign_no] INT NOT NULL DEFAULT (0), 
                                   [goal_amt] DECIMAL (18,2) NULL DEFAULT (0.0),
                                   [unrestricted_amt] DECIMAL (18,2) NULL DEFAULT (0.0),
                                   [restricted_amt] DECIMAL (18,2) NULL DEFAULT (0.0),
                                   [ask_amt] DECIMAL (18,2) NULL DEFAULT (0.0),
                                   [plan_yield] DECIMAL (18,4) NOT NULL DEFAULT (0.0),
                                   [campaign_goal] DECIMAL (18,4) NOT NULL DEFAULT (0.0),
                                   [fiscal_year_goal] DECIMAL (18,4) NOT NULL DEFAULT (0.0))

        IF OBJECT_ID('tempdb..#final_table') IS NOT NULL DROP TABLE [#final_table]

        CREATE TABLE [#final_table] ([business_unit_type] VARCHAR(50) NOT NULL DEFAULT (''),
                                     [business_unit_type_sort] VARCHAR(50) NOT NULL DEFAULT (''),
                                     [business_unit] VARCHAR(50) NOT NULL DEFAULT (''),
                                     [business_unit_sort] VARCHAR(75) NOT NULL DEFAULT (''),
                                     [unrestricted_goal] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                     [restricted_goal] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                     [goal_amt] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                     [unrestricted_amt] DECIMAL (18,2) NOT NULL DEFAULT (0.0),
                                     [restricted_amt] DECIMAL (18,2) NOT NULL DEFAULT (0.0),
                                     [ask_amt] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                     [ask_amt_open] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                     [plan_yield] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                     [campaign_goal] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                     [fiscal_year_goal] DECIMAL(18,2) NOT NULL DEFAULT (0.0));
 
    /*  Get Contribution Data - This code is pulled directly from the ADV_Revenue report  */
    
        WITH BU_Goals ([Business_Unit], [BU_Unrestricted_Goal], [BU_Restricted_Goal])
        AS (SELECT lcbu.[Business_Unit], 
                   SUM(lcbu.[Unrestricted_Goal]), 
                   SUM(lcbu.[Restricted_Goal])
            FROM [dbo].[LTR_CAMPAIGN_BUSINESS_UNIT] AS lcbu
                 INNER JOIN [dbo].[T_CAMPAIGN] AS c ON c.[campaign_no] = lcbu.[campaign_no]
	        WHERE c.[description] LIKE @fiscal_year
	        GROUP BY lcbu.[Business_Unit])
        INSERT INTO [#bu_detail] ([business_unit], [business_unit_sort], [campaign_no], [unrestricted_amt], [restricted_amt], [ask_amt])
        SELECT bus.[business_unit], 
               bus.[business_unit], 
               cont.[campaign_no],
               SUM(CASE WHEN det.[acct_goal_no] = 3 OR det.[acct_grp_no] = 10 THEN cont.[cont_amt]
                        ELSE 0 END), --unrestricted
               SUM(CASE WHEN det.[acct_goal_no] <> 3 AND det.[acct_grp_no] <> 10 THEN cont.[cont_amt]
                    ELSE 0 END), --restricted
               SUM(cont.cont_amt) --total_ask_amt
        FROM [dbo].[T_CONTRIBUTION] AS cont
             INNER JOIN [dbo].[T_CAMPAIGN] AS cam ON cam.[campaign_no] = cont.[campaign_no]
             INNER JOIN [dbo].[TR_CAMPAIGN_CATEGORY] AS cat ON cam.[category] = cat.[id] AND cat.[id] <> 24 -- Skip
             INNER JOIN [dbo].[LTR_FUND_DETAIL] AS det ON det.[fund_no] = cont.[fund_no]
             LEFT JOIN [dbo].[LTR_CAMPAIGN_BUSINESS_UNIT] AS bus ON bus.[campaign_no] = cam.[campaign_no]
	         LEFT JOIN [BU_Goals] AS bugoals ON ISNULL(bugoals.[Business_Unit],'') = ISNULL(bus.[Business_Unit], '')
        WHERE cont.[cont_dt] BETWEEN @fiscal_start_dt AND @current_dt
          AND cont.[cont_amt] <> 0.00
          AND cont.[customer_no] <> 2653093 
        GROUP BY  bus.[Business_Unit], cont.[campaign_no];

    /*  Add in any business unit that has a campaign in the current fiscal year but does not yet exist in the data  */

        WITH [CTE_CAMPAIGN_NUMBERS] ([campaign_no])
        AS (SELECT DISTINCT [campaign_no] 
            FROM [dbo].[T_CAMPAIGN] 
            WHERE [fyear] = @fiscal_year)
        INSERT INTO [#bu_detail] ([Business_Unit], [business_unit_sort], [campaign_no],[ask_amt])
        SELECT bu.[Business_Unit],
               bu.[Business_Unit],
               bu.[Campaign_no],
               0.00
        FROM [dbo].[LTR_CAMPAIGN_BUSINESS_UNIT] AS bu
             INNER JOIN [CTE_CAMPAIGN_NUMBERS] AS cte ON cte.[campaign_no] = bu.[campaign_no]
        WHERE bu.[Business_Unit] NOT IN (SELECT DISTINCT [Business_Unit] FROM [#bu_detail]);

    /*  Get overall campaign goal and fiscal year goal  */
      
       SELECT @campaign_goal = ISNULL(SUM(ct.[goal]),0)
       FROM [dbo].[LTR_CM_CATEGORY1] AS ct 
             INNER JOIN [dbo].[LTR_FUND_DETAIL] AS dt ON dt.cm_category1 = ct.[id] AND dt.[overall_cm_no] = @overall_campaign_no

        SELECT @fiscal_year_goal = ISNULL(SUM(ISNULL(bu.[Unrestricted_Goal],0) + ISNULL(bu.[Restricted_Goal],0)),0)
        FROM [dbo].[LTR_CAMPAIGN_BUSINESS_UNIT] AS bu
             INNER JOIN [dbo].[T_CAMPAIGN] AS cm ON cm.[campaign_no] = bu.[campaign_no] AND cm.[fyear] = @fiscal_year;
 
    /*  Create final list of aggreated data  */

            
        WITH [CTE_FY_GOALS] ([fiscal_year], [business_unit], [unrestricted_goal], [restricted_goal], [total_goal])
        AS (SELECT @fiscal_year,
                   bu.[business_unit], 
                   SUM(bu.[Unrestricted_Goal]), 
                   SUM(bu.[Restricted_Goal]), 
                   SUM(bu.[Unrestricted_Goal] + bu.[Restricted_Goal])
            FROM [dbo].[LTR_CAMPAIGN_BUSINESS_UNIT] AS bu
                 INNER JOIN [dbo].[T_CAMPAIGN] AS cm ON cm.[campaign_no] = bu.[campaign_no] AND cm.[fyear] = @fiscal_year
            GROUP BY [bu].[Business_Unit])
        INSERT INTO [#final_table] ([Business_Unit], [business_unit_sort], [unrestricted_goal], [restricted_goal], [goal_amt], [unrestricted_amt],
                                    [restricted_amt], [ask_amt], [ask_amt_open], [plan_yield], [campaign_goal], [fiscal_year_goal])
        SELECT pd.[business_unit],
               pd.[business_unit_sort],
               MAX(ISNULL(gl.[unrestricted_goal],0)),
               MAX(ISNULL(gl.[restricted_goal],0)),
               MAX(ISNULL(gl.[total_goal],0)),
               SUM(pd.[unrestricted_amt]),
               SUM(pd.[restricted_amt]),
               SUM(pd.[ask_amt]),
               SUM(pd.[ask_amt]),
               SUM(pd.[plan_yield]),
               SUM(pd.[campaign_goal]),
               SUM(pd.[fiscal_year_goal])
            FROM [#bu_detail] AS pd
                 LEFT OUTER JOIN [CTE_FY_GOALS] AS gl ON gl.[business_unit] = pd.[business_unit]
            GROUP BY pd.[business_unit], pd.[business_unit_sort]
            ORDER BY pd.[business_unit_sort]

            

    /*  Update Totals  */

         UPDATE [#final_table]
         SET [restricted_goal] = (SELECT SUM([restricted_goal]) FROM [#final_table] WHERE [Business_Unit] <> 'Total'),
             [unrestricted_goal] = (SELECT SUM([unrestricted_goal]) FROM [#final_table] WHERE [Business_Unit] <> 'Total'),
             [goal_amt] = (SELECT SUM([goal_amt]) FROM [#final_table] WHERE [Business_Unit] <> 'Total'),
             [restricted_amt] = (SELECT SUM([restricted_amt]) FROM [#final_table] WHERE [Business_Unit] <> 'Total'),
             [unrestricted_amt] = (SELECT SUM([unrestricted_amt]) FROM [#final_table] WHERE [Business_Unit] <> 'Total'),
             [ask_amt] = (SELECT SUM([ask_amt]) FROM [#final_table] WHERE [Business_Unit] <> 'Total'),
             [ask_amt_open] = (SELECT SUM([ask_amt_open]) FROM [#final_table] WHERE [Business_Unit] <> 'Total'),
             [plan_yield] = (SELECT SUM([plan_yield]) FROM [#final_table] WHERE [Business_Unit] <> 'Total')
         WHERE [Business_Unit] = 'Total'

    /*  Update Types  */

        UPDATE [#final_table]
        SET [business_unit_type] = 'Fundraising',
            [business_unit_type_sort] = 'FUN'
        WHERE [business_unit] NOT IN ('Gifts In Kind','Total')

        UPDATE [#final_table]
        SET [business_unit_type] = 'Gifts in Kind',
            [business_unit_type_sort] = 'GIF'
        WHERE [business_unit] = 'Gifts In Kind'

        UPDATE [#final_table]
        SET [business_unit_type] = 'Total',
            [business_unit_type_sort] = 'TOT'
        WHERE [business_unit] = 'Total'

     
    FINISHED:

        /*  Select final data set to return to the report  */
       
            SELECT @fiscal_year AS [fiscal_year],
                   RIGHT(CAST(@fiscal_year AS VARCHAR(10)),2) AS [fiscal_year_2],
                   [business_unit_type],
                   [business_unit_type_sort],
                   [business_unit],
                   [business_unit_sort],
                   [unrestricted_goal],
                   [restricted_goal],
                   [goal_amt],
                   [unrestricted_amt],
                   [restricted_amt],
                   [ask_amt],
                   [ask_amt_open],
                   [plan_yield],
                   [campaign_goal],
                   [fiscal_year_goal]
            FROM [#final_table]
            ORDER BY [business_unit_type_sort], [business_unit_sort]      

    DONE:

        IF OBJECT_ID('tempdb..#final_table') IS NOT NULL DROP TABLE [#final_table]

        IF OBJECT_ID('tempdb..#bu_detail') IS NOT NULL DROP TABLE [#bu_detail]

END
GO

EXECUTE [dbo].[LRP_ADV_CAMPAIGN_ACTIVITY_BU] @fiscal_year = 2021

