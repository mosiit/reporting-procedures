USE impresario
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LP_RPT_CASHOUT_COUPONS]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LP_RPT_CASHOUT_COUPONS]
GO

CREATE PROCEDURE [dbo].[LP_RPT_CASHOUT_COUPONS]
        @rpt_dt datetime,
        @rpt_operator varchar(8)
AS BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

    DECLARE @rpt_date char(10), @rpt_message varchar(100), @report_start datetime
    DECLARE @coupon_table table ([coupon_date] char(10), [price_type_name] varchar(30), [comp_code_name_short] varchar(20), [comp_code_name] varchar(30), [total_coupons] int, [total_transactions] int, [coupon_operator] varchar(8),
                                 [operator_first] varchar(50), [operator_last] varchar(50), [operator_location] varchar(50), [elapsed_time] int, [rpt_message] varchar(100))

    SELECT @report_start = getdate()

    SELECT @rpt_message = '', @rpt_date = ''

    SELECT @rpt_operator = IsNull(@rpt_operator,'')
    
    IF @rpt_dt is null
        SELECT @rpt_message = 'error: invalid date passed to report.'
    ELSE BEGIN

        SELECT @rpt_date = convert(char(10),@rpt_dt,111)
    
        IF @rpt_operator = '' or not exists (SELECT * FROM T_METUSER WHERE [userid] = @rpt_operator)
            SELECT @rpt_message = 'error: invalid operator id passed to report (' + @rpt_operator + ').'
                    
    END
     
    IF @rpt_message <> '' GOTO FINISHED

    /* 6-16/16: Removed the statement below and replaced it with the slightly altered statement that takes price_type_name out of the equation.  
                It looks like an issue was occuring when the same comp code was used by multiple price types and somehow it was ignoring the second record.
                This change will tie the two records together.  

    INSERT INTO @coupon_table 
    SELECT [coupon_date], [price_type_name], [comp_code_name_short], [comp_code_name], count(*), [coupon_operator], [coupon_operator_first_name], [coupon_operator_last_name], [coupon_operator_location], 0, @rpt_message
    FROM [LV_RPT_CASHOUT_COUPONS] WHERE coupon_date = @rpt_date and coupon_operator = @rpt_operator
    GROUP BY [coupon_date], [price_type_name], [comp_code_name_short], [comp_code_name], [coupon_operator], [coupon_operator_first_name], [coupon_operator_last_name], [coupon_operator_location]

    */

    INSERT INTO @coupon_table 
    SELECT [coupon_date], '', [comp_code_name_short], [comp_code_name], count(*), count(distinct Order_no), [coupon_operator], [coupon_operator_first_name], [coupon_operator_last_name], [coupon_operator_location], 0, @rpt_message
    FROM [LV_RPT_CASHOUT_COUPONS] WHERE coupon_date = @rpt_date and coupon_operator = @rpt_operator
    GROUP BY [coupon_date], [comp_code_name_short], [comp_code_name], [coupon_operator], [coupon_operator_first_name], [coupon_operator_last_name], [coupon_operator_location]

    FINISHED:

        IF not exists (SELECT * FROM @coupon_table) BEGIN
            IF @rpt_message = '' SELECT @rpt_message = 'No coupons found for ' + @rpt_operator + ' on ' + @rpt_date +  '.'
            INSERT INTO @coupon_table VALUES (@rpt_date, '', '', '', 0, 0, @rpt_operator, '', '', '', 0, @rpt_message)
        END

        UPDATE @coupon_table SET [elapsed_time] = datediff(second,@report_start,getdate())

        SELECT [coupon_date], [coupon_operator], [operator_first], [operator_last], [operator_location], [price_type_name], [comp_code_name_short], [comp_code_name], [total_coupons], [total_transactions], [elapsed_time], [rpt_message] 
        FROM @coupon_table

END
GO

GRANT EXECUTE ON [dbo].[LP_RPT_CASHOUT_COUPONS] TO impusers
GO

