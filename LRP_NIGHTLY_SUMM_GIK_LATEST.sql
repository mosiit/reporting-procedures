USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LRP_NIGHTLY_SUMM_GIK_LATEST]    Script Date: 12/14/2020 2:23:56 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



ALTER PROCEDURE [dbo].[LRP_NIGHTLY_SUMM_GIK_LATEST]
	@section VARCHAR(300),
	@sortOrder INT,
	@runBy	VARCHAR(300)
AS
-- ===============================================================
-- H. Sheridan, 4/4/2016 - GIK_Latest
-- ===============================================================

BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	--SET @section = 'GIK_Latest'
	--SET	@sortOrder = 10

	DECLARE @tblPassOne	TABLE	(
								customer_no		INT,
								value			MONEY,
								cont_date		DATE
								)

	DECLARE @tblPassTwo TABLE	(
								customer_no		INT,
								value			MONEY,
								cont_date		DATE
								)

	DECLARE @tblPassBoth TABLE	(
								customer_no		INT,
								value			MONEY,
								cont_date		DATE
								)

	DECLARE @tbl_ns TABLE ([customer_no] [int] NOT NULL,
						   [section] [varchar](300) NOT NULL,
						   [value] [money] NULL,
						   [apply_date] [varchar](20) NULL,
						   [sort_order] [int] NULL,
						   [created_on] [datetime] NULL,
						   [created_by] [varchar](300) NULL)

	INSERT INTO @tblPassOne
        SELECT  c.customer_no,
                ISNULL(SUM(c.cont_amt), 0),
                CAST(c.cont_dt AS DATE)
        FROM    T_CONTRIBUTION AS c
        INNER JOIN LTR_FUND_DETAIL AS f ON c.fund_no = f.fund_no
		--2020/12/14, H. Sheridan.  Service Now ticket SCTASK0001710 - join to campaign table
		JOIN	T_CAMPAIGN cam
		ON		cam.campaign_no = c.campaign_no
        WHERE   c.cont_type = 'G'
                AND c.custom_1 IN ('(none)', 'Matching Gift Credit')
                AND c.KG_xfer_dt > '01-01-1900'
				--2020/12/14, H. Sheridan.  Service Now ticket SCTASK0001710 - add criteria for campaign category
				AND	cam.category NOT IN (8,9)
				--2020/12/14, H. Sheridan.  Service Now ticket SCTASK0001710 - remove criteria for acct_grp_no
                --AND f.acct_grp_no <> '3'
                --AND c.cont_dt IN (SELECT    MAX(c1.cont_dt)
                --                  FROM      T_CONTRIBUTION AS c1
                --                  INNER JOIN LTR_FUND_DETAIL AS f1 ON c1.fund_no = f1.fund_no
                --                  WHERE     c1.cont_type = 'G'
                --                            AND c1.custom_1 IN ('(none)', 'Matching Gift Credit')
                --                            AND c1.KG_xfer_dt > '01-01-1900'
                --                            AND f1.acct_grp_no <> '3'
                --                            AND c.customer_no = c1.customer_no)
        GROUP BY c.customer_no, c.cont_dt

	--SELECT 'debug 1', * FROM @tblNightSumm

	INSERT INTO @tblPassTwo
        SELECT  i.customer_no,
                ISNULL(SUM(c.cont_amt), 0),
                CAST(c.cont_dt AS DATE)
        FROM    T_CONTRIBUTION AS c
        INNER JOIN T_CREDITEE AS t ON c.ref_no = t.ref_no
        INNER JOIN LTR_FUND_DETAIL AS f ON c.fund_no = f.fund_no
        INNER JOIN T_CUSTOMER AS i ON t.creditee_no = i.customer_no
		--2020/12/14, H. Sheridan.  Service Now ticket SCTASK0001710 - join to campaign table
		JOIN	T_CAMPAIGN cam
		ON		cam.campaign_no = c.campaign_no
        WHERE   c.cont_type = 'G'
                AND c.custom_1 IN ('Primary Soft Credit', 'Matching Gift Credit', 'Stewardship Soft Credit')
                AND c.KG_xfer_dt > '01-01-1900'
                AND t.creditee_type IN ('12', '15', '5', '10', '16', '1', '14')
				--2020/12/14, H. Sheridan.  Service Now ticket SCTASK0001710 - add criteria for campaign category
				AND	cam.category NOT IN (8,9)
				--2020/12/14, H. Sheridan.  Service Now ticket SCTASK0001710 - remove criteria for acct_grp_no
                --AND f.acct_grp_no <> '3'
                --AND c.cont_dt IN (SELECT    MAX(c1.cont_dt)
                --                  FROM      T_CONTRIBUTION AS c1
                --                  INNER JOIN T_CREDITEE AS t1 ON c1.ref_no = t1.ref_no
                --                  INNER JOIN LTR_FUND_DETAIL AS f1 ON c1.fund_no = f1.fund_no
                --                  INNER JOIN T_CUSTOMER AS i1 ON t1.creditee_no = i1.customer_no
                --                  WHERE     c1.cont_type = 'G'
                --                            AND c1.custom_1 IN ('Primary Soft Credit', 'Matching Gift Credit', 'Stewardship Soft Credit')
                --                            AND c1.KG_xfer_dt > '01-01-1900'
                --                            AND t1.creditee_type IN ('12', '15', '5', '10', '16', '1', '14')
                --                            AND f1.acct_grp_no <> '3'
                --                            AND i.customer_no = i1.customer_no)
        GROUP BY i.customer_no, c.cont_dt

	INSERT INTO @tblPassBoth
		SELECT * FROM @tblPassOne

	INSERT INTO @tblPassBoth
		SELECT * FROM @tblPassTwo

	--SELECT * FROM @tblPassBoth

	INSERT INTO @tbl_ns
		SELECT  t1.customer_no,
				@section,
				ISNULL(t1.value, 0),
				t1.cont_date,
				@sortOrder,
				GETDATE(),
				@runBy
		FROM	@tblPassBoth t1
		WHERE	t1.cont_date IN (SELECT   MAX(t2.cont_date)
								FROM	@tblPassBoth t2
								WHERE	t1.customer_no = t2.customer_no
								)
		--GROUP BY t1.customer_no, t1.cont_date

	UPDATE	ns1
	SET		ns1.[apply_date] = (SELECT	MAX(ns2.[apply_date]) 
								FROM	@tbl_ns ns2
								WHERE	ns1.customer_no = ns2.customer_no
								AND		ns1.value = ns2.value)
	FROM	@tbl_ns ns1

	INSERT INTO LT_NIGHTLY_SUMMARY
		SELECT  t1.customer_no,
				@section,
				SUM(ISNULL(t1.value, 0)),
				t1.cont_date,
				@sortOrder,
				GETDATE(),
				@runBy
		FROM	@tblPassBoth t1
		WHERE	t1.cont_date IN	(
								SELECT	MAX(t2.cont_date)
								FROM	@tblPassBoth t2
								WHERE	t1.customer_no = t2.customer_no
								)
		GROUP BY t1.customer_no, t1.cont_date

END

GO


