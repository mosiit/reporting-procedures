USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_ADV_CAMPAIGN_ACTIVITY_VISIT]') AND type IN (N'P', N'PC'))
BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_ADV_CAMPAIGN_ACTIVITY_VISIT] AS' 
    EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LRP_ADV_CAMPAIGN_ACTIVITY_VISIT] TO impusers'
END
GO

/*      LRP_ADV_CAMPAIGN_ACTIVITY_VISIT
        Pulls metrics on solicitors and how many times they have visited their prospects in the past 12 months.
  
*/
ALTER PROCEDURE [dbo].[LRP_ADV_CAMPAIGN_ACTIVITY_VISIT]
        @fiscal_year INT = NULL
WITH RECOMPILE AS BEGIN

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

    /*  Check Fiscal Year Parameter - If none passed, set to current  */
        IF @fiscal_year IS NULL SELECT @fiscal_year = dbo.LF_GetFiscalYear(GETDATE())

    /*  Procedure Variables  */
        
        --Determine current fiscal year, fiscal start date, fiscal end date, and current date
        DECLARE @fiscal_start_dt DATETIME = [dbo].[LF_GetFiscalYearStartDt](@fiscal_year)
        DECLARE @fiscal_end_dt DATETIME = [dbo].[LF_GetFiscalYearEndDt](@fiscal_year)
        DECLARE @current_dt DATETIME = GETDATE()
        IF @current_dt > @fiscal_end_dt SELECT @current_dt = @fiscal_end_dt

        DECLARE @month_check INT = 0, @month_check_dt DATETIME

        --Determine the date one year ago (Make sure time is 00:00:00)
        DECLARE @one_year_ago_dt DATETIME = CONVERT(DATE,DATEADD(YEAR,-1,GETDATE()))

        DECLARE @months_into_fiscal DECIMAL(18,2) = DATEDIFF(MONTH,@fiscal_start_dt,GETDATE()) + 1
    
    /*  Create raw data temp table  */

        IF OBJECT_ID('tempdb..#visit_raw_data') IS NOT NULL DROP TABLE [#visit_raw_data]

        CREATE TABLE [#visit_raw_data]  ([step_no] INT NOT NULL DEFAULT (0), 
                                         [plan_no] INT NOT NULL DEFAULT (0), 
                                         [step_dt] DATETIME NULL, 
                                         [completed_on_dt] DATETIME NULL,
                                         [month_num] INT NOT NULL DEFAULT (0), 
                                         [month_abbrev] VARCHAR(30) NOT NULL DEFAULT (''),
                                         [month_sort] VARCHAR(7) NOT NULL DEFAULT (''),
                                         [month_year] VARCHAR(4) NOT NULL DEFAULT (''),
                                         [fiscal_year] INT NOT NULL DEFAULT (0),
                                         [is_current_fiscal] INT NOT NULL DEFAULT (0),
                                         [step_type] INT NOT NULL DEFAULT (0), 
                                         [step_type_name] VARCHAR(30) NOT NULL DEFAULT (''), 
                                         [step_description] VARCHAR(30) NOT NULL DEFAULT(''), 
                                         [associate_no] INT NOT NULL DEFAULT (0),
                                         [priority] INT NOT NULL DEFAULT (0), 
                                         [worker_customer_no] INT NOT NULL DEFAULT (0),
                                         [worker_name] VARCHAR(75) NOT NULL DEFAULT (''),
                                         [worker_initials] VARCHAR(10) NOT NULL DEFAULT (''))

        IF OBJECT_ID('tempdb..#final_visit_counts') IS NOT NULL DROP TABLE [#final_visit_counts]
        
        CREATE TABLE [#final_visit_counts] ([month_num] INT NOT NULL DEFAULT (0),
                                            [month_abbrev] VARCHAR(30) NOT NULL DEFAULT (''),
                                            [month_sort] VARCHAR(7) NOT NULL DEFAULT (''),
                                            [month_year] VARCHAR(4) NOT NULL DEFAULT (''),
                                            [fiscal_year] INT NOT NULL DEFAULT (0),
                                            [is_current_fiscal] INT NOT NULL DEFAULT (0),
                                            [worker_customer_no] INT NOT NULL DEFAULT (0),
                                            [worker_initials] VARCHAR(10) NOT NULL DEFAULT (''),
                                            [visit_count] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                            [is_an_average] INT NOT NULL DEFAULT (0))


    /*  Set @one_year_ago to first date of the following month so that the current month does not show up
        on the report twice (this year and last year)  */

        WHILE DATEPART(MONTH,@one_year_ago_dt) = DATEPART(MONTH,@current_dt)
            SELECT @one_year_ago_dt = DATEADD(DAY,1,@one_year_ago_dt)


    /*  Pulls a list of all visits that were completed in the past twelve months  */
    
        INSERT INTO [#visit_raw_data] ([step_no], [plan_no], [step_dt], [completed_on_dt], [month_num], [month_abbrev], [month_sort], [month_year], 
                                       [fiscal_year], [is_current_fiscal], [step_type], [step_type_name], [step_description], [associate_no], 
                                       [priority], [worker_customer_no], [worker_name], [worker_initials])
        SELECT stp.[step_no],
               stp.[plan_no], 
               stp.[step_dt], 
               stp.[completed_on_dt],
               DATEPART(MONTH,stp.[completed_on_dt]),
               LEFT(DATENAME(MONTH,stp.[completed_on_dt]),3),
               LEFT(CONVERT(VARCHAR(10),stp.[completed_on_dt],111),7),
               DATENAME(YEAR,stp.[completed_on_dt]),
               CASE WHEN  stp.[completed_on_dt] < @fiscal_start_dt THEN (@fiscal_year - 1) ELSE @fiscal_year END,
               CASE WHEN stp.[completed_on_dt] < @fiscal_start_dt THEN 0 ELSE 1 END,
               stp.[step_type], 
               typ.[description], 
               stp.[description], 
               ISNULL(stp.[associate_no],0),
               stp.[priority], 
               stp.[worker_customer_no],
               wrk.[worker_name],
               wrk.[worker_initials]
        FROM [dbo].[T_STEP] AS stp
             INNER JOIN [dbo].[LV_CAMPAIGN_ACTIVITY_WORKERS] AS wrk ON wrk.[worker_customer_no] = stp.[worker_customer_no] AND wrk.[inactive] = 'N'
             INNER JOIN [dbo].[TR_STEP_TYPE] AS typ ON typ.[id] = stp.[step_type]
        WHERE stp.[completed_on_dt] BETWEEN @one_year_ago_dt AND @current_dt
          AND stp.[step_type] = 29      --29 = Contact-Visit
          

    /*  Check to make sure there is at least one record in the table for every month in the past 12 (including this month)  */

        WHILE @month_check >= -11 BEGIN

            SELECT @month_check_dt = DATEADD(MONTH,@month_check,@current_dt)

            IF NOT EXISTS (SELECT * FROM [#visit_raw_data] WHERE month_sort = LEFT(CONVERT(VARCHAR(10),@month_check_dt,111),7))
                INSERT INTO [#visit_raw_data] ([step_no], [plan_no], [step_dt], [completed_on_dt], [month_num], [month_abbrev], [month_sort], [month_year], 
                                               [fiscal_year], [is_current_fiscal], [step_type], [step_type_name], [step_description], [associate_no], 
                                               [priority], [worker_customer_no], [worker_name], [worker_initials])
                VALUES (0, 0, @month_check_dt, @month_check_dt, DATEPART(MONTH,@month_check_dt), LEFT(DATENAME(MONTH,@month_check_dt),3), 
                        LEFT(CONVERT(VARCHAR(10),@month_check_dt,111),7), DATENAME(YEAR,@month_check_dt), @fiscal_year, 1, 0, '', '', 0, 0, 0, '', '+++')

            SELECT @month_check = (@month_check - 1)

        END

        
    /*  Determine number of visits for each solicitor in the current fiscal year so far  */

        INSERT INTO [#final_visit_counts] ([month_num], [month_abbrev], [month_sort], [month_year], [fiscal_year], [is_current_fiscal], 
                                           [worker_customer_no], [worker_initials], [visit_count], [is_an_average])
        SELECT 0, 
               'YTD Visits', 
               '9999/02', 
               '',
               @fiscal_year,
               1,
               [worker_customer_no],
               [worker_initials], 
               COUNT([step_no]),
               0
        FROM [#visit_raw_data]
        WHERE [completed_on_dt] BETWEEN @fiscal_start_dt AND @current_dt
        GROUP BY [worker_customer_no], [worker_initials]

    /*  Determine each solicitor's average per month for each month so far in the current fiscal year  */

        INSERT INTO [#final_visit_counts] ([month_num], [month_abbrev], [month_sort], [month_year], [fiscal_year], [is_current_fiscal], 
                                           [worker_customer_no], [worker_initials], [visit_count], [is_an_average])
        SELECT 0, 
               'Avg / Month', 
               '9999/01', 
               '',
               @fiscal_year,
               1,
               [worker_customer_no],
               [worker_initials], 
               (COUNT([step_no]) / @months_into_fiscal),
               1
        FROM [#visit_raw_data]
        WHERE [completed_on_dt] BETWEEN @fiscal_start_dt AND @current_dt
        GROUP BY [worker_customer_no], [worker_initials]
     
    /*  Determine how many visits for each solicitor in each month of the past 12 months  */

        INSERT INTO [#final_visit_counts] ([month_num], [month_abbrev], [month_sort], [month_year], [fiscal_year], [is_current_fiscal], 
                                           [worker_customer_no], [worker_initials], [visit_count],[is_an_average])
        SELECT [month_num], 
               [month_abbrev], 
               [month_sort],
               [month_year],  
               [fiscal_year],
               [is_current_fiscal],
               [worker_customer_no],
               [worker_initials], 
               COUNT([step_no]),
               0
        FROM [#visit_raw_data]
        GROUP BY [month_num], [month_abbrev], [month_sort], [month_year], [fiscal_year], [is_current_fiscal], [worker_customer_no], [worker_initials]

    /*  Determine total visits for each solicitor for the previous 12 months  */

        INSERT INTO [#final_visit_counts] ([month_num], [month_abbrev], [month_sort], [month_year], [fiscal_year], [is_current_fiscal], 
                                           [worker_customer_no], [worker_initials], [visit_count], [is_an_average])
        SELECT 0, 
               '12 Mn Total', 
               '0000/02', 
               '',
               @fiscal_year,
               1,
               [worker_customer_no],
               [worker_initials], 
               COUNT([step_no]),
               0
        FROM [#visit_raw_data]
        GROUP BY [worker_customer_no], [worker_initials]

    /*  Determine each solicitor's average per month for each month of the past 12  */

        INSERT INTO [#final_visit_counts] ([month_num], [month_abbrev], [month_sort], [month_year], [fiscal_year], [is_current_fiscal], 
                                           [worker_customer_no], [worker_initials], [visit_count], [is_an_average])
        SELECT 0, 
               '12 Mn Avg', 
               '0000/01', 
               '',
               @fiscal_year,
               1,
               [worker_customer_no],
               [worker_initials], 
               (COUNT([step_no]) / 12.0),
               1
        FROM [#visit_raw_data]
        GROUP BY [worker_customer_no], [worker_initials]

    /*  Zero out visit counts on place-holder records  */

       UPDATE [#final_visit_counts]
       SET [visit_count] = 0
       WHERE [worker_initials] = '+++'

    FINISHED:

        /*  Select final data set to return to the report  */

            SELECT [month_num], 
                   [month_abbrev], 
                   [month_sort], 
                   [month_year], 
                   [fiscal_year], 
                   [is_current_fiscal],
                   [worker_customer_no],
                   [worker_initials], 
                   [visit_count],
                   [is_an_average]
            FROM [#final_visit_counts]
            ORDER BY [month_sort], [worker_initials]

    DONE:

        IF OBJECT_ID('tempdb..#final_visit_counts') IS NOT NULL DROP TABLE [#final_visit_counts]
        IF OBJECT_ID('tempdb..#visit_raw_data') IS NOT NULL DROP TABLE [#visit_raw_data]        

END
GO

EXECUTE [dbo].[LRP_ADV_CAMPAIGN_ACTIVITY_VISIT] @fiscal_year = 2020






