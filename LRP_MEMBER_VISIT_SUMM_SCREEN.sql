USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LRP_MEMBER_VISIT_SUMM_SCREEN]    Script Date: 3/21/2016 3:18:11 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[LRP_MEMBER_VISIT_SUMM_SCREEN]
	@customer_no		INT
AS

-- ================================================================================================
-- Created:  H. Sheridan (MOS), 3/21/2016
--
-- Work order 81839 - [SCRN] Create member attendance custom screen in Tessitura
--
-- Notes:
--
--		Created as a stored procedure - even though this is just two SELECT's - to make it easier
--		to do edits down the road.  
--
--		Included in SSRS project:  E:\SSRS\Tessitura\ConstituentCustomScreens
--
-- ================================================================================================

BEGIN

	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
 
	--
	-- PULLING CURRENT MEMBERSHIP, NSCAN ATTENDANCE DATA
	--
	SELECT	@customer_no AS customer_no,
			0 AS id_key,
			'Current' AS 'Status',
			cmi.memb_level_description AS 'Membership Type',
			CONVERT(VARCHAR(10), cmi.initiation_date, 101) + ' - ' +  CONVERT(VARCHAR(10), cmi.expiration_date, 101) AS 'Membership Date Range',
			COUNT(att.id) AS '# Visits'
	FROM	[dbo].[LV_CURRENT_MEMBERSHIP_INFO] cmi
	LEFT JOIN	[dbo].[T_ATTENDANCE] att 
	ON		att.[cust_memb_no] = cmi.[customer_memb_no]
	AND		att.[attend_dt] BETWEEN cmi.[initiation_date] AND cmi.[expiration_date]
	AND		att.[cust_memb_no] IS NOT NULL
	WHERE	cmi.customer_no = @customer_no
	GROUP BY cmi.memb_level_description, cmi.initiation_date, cmi.expiration_date

	UNION

	SELECT previousMembVisits.customer_no, 
		previousMembVisits.id_key, 
		previousMembVisits.Status, 
		previousMembVisits.[Membership Type], 
		previousMembVisits.[Membership Date Range], 
		MAX(previousMembVisits.[# Visits])
	FROM
	( 	-- PULLING PREVIOUS MEMBERSHIP, NSCAN ATTENDANCE DATA
		SELECT	@customer_no AS customer_no,
				0 AS id_key,
				'Previous' AS 'Status',
				pmi.memb_level_description AS 'Membership Type',
				CONVERT(VARCHAR(10), pmi.initiation_date, 101) + ' - ' +  CONVERT(VARCHAR(10), pmi.expiration_date, 101) AS 'Membership Date Range',
				COUNT(att.id) AS '# Visits'
		FROM	[dbo].[LV_PAST_MEMBERSHIP_INFO] pmi
		LEFT JOIN	[dbo].[T_ATTENDANCE] att 
		ON		att.[cust_memb_no] = pmi.[customer_memb_no]
		AND		att.[attend_dt] BETWEEN pmi.[initiation_date] AND pmi.[expiration_date]
		AND		att.[cust_memb_no] IS NOT NULL
		WHERE	pmi.customer_no = @customer_no
		GROUP BY pmi.memb_level_description, pmi.initiation_date, pmi.expiration_date

		UNION 
		-- PULLING HISTORIC ATTENDANCE DATA STORED IN LT_CORRESPONDENCE TABLE 
		SELECT	@customer_no AS customer_no,
				0 AS id_key,
				'Previous' AS 'Status',
				pmi.memb_level_description AS 'Membership Type',
				CONVERT(VARCHAR(10), pmi.initiation_date, 101) + ' - ' +  CONVERT(VARCHAR(10), pmi.expiration_date, 101) AS 'Membership Date Range',
				COUNT(att.id_key) AS '# Visits'
		FROM	[dbo].[LV_PAST_MEMBERSHIP_INFO] pmi
		LEFT JOIN	[dbo].LT_CORRESPONDENCE att 
		ON		att.[customer_no] = pmi.[customer_no]
		AND		att.[sent_dt] BETWEEN pmi.[initiation_date] AND pmi.[expiration_date]
		WHERE	att.corrtype_no = 32 -- Membership Attendance
			AND pmi.customer_no = @customer_no
		GROUP BY pmi.memb_level_description, pmi.initiation_date, pmi.expiration_date
	) previousMembVisits
	GROUP BY previousMembVisits.customer_no, 
		previousMembVisits.id_key, 
		previousMembVisits.Status, 
		previousMembVisits.[Membership Type], 
		previousMembVisits.[Membership Date Range]

	ORDER BY 'Membership Date Range' DESC, memb_level_description

END

GO


