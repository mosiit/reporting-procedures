USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LRP_GET_PERFORMANCE_ISSUES_HISTORY]    Script Date: 3/25/2019 8:06:35 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




ALTER PROCEDURE [dbo].[LRP_GET_PERFORMANCE_ISSUES_HISTORY]
AS

SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

DECLARE	@LastRun		DATETIME,
		@Now			DATETIME,
		@MinParam		INT

SELECT	@LastRun = MAX(LastRun)
FROM	LT_PERFORMANCE_ISSUES_HISTORY

-- If no entry, set the last run back to 1/1/2018.  If you put the ISNULL on the statement above, it acts oddly.
SELECT @LastRun = ISNULL(@LastRun, '1/1/2018')

SELECT	@Now = CURRENT_TIMESTAMP

SELECT	@MinParam = DATEDIFF(minute, @LastRun, @Now)

--SELECT 'debug', @LastRun, @Now, @MinParam

CREATE TABLE #ExtResults (	StartTime DATETIME,
							EndTime DATETIME,
							Duration VARCHAR(15),
							ExtractNbr INT,
							ExtractDescription VARCHAR(50),
							DLNbr INT,
							RunStatus VARCHAR(20),
							TimeSlot VARCHAR(10),
							LastUpdatedBy VARCHAR(10));

CREATE TABLE #RptResults (	[Full Report Path] VARCHAR(MAX),
							[Report Name] VARCHAR(MAX),
							[Report Parameters] VARCHAR(MAX),
							Duration VARCHAR(30),
							RptType VARCHAR(60),
							RunStatus VARCHAR(100),
							StartTime DATETIME,
							EndTime DATETIME,
							LastRunBy VARCHAR(60),
							CreatedBy VARCHAR(60),
							Scheduled CHAR(1),
							RecordCount VARCHAR(10))

CREATE TABLE #DeadLockResults (	[DeadlockID] [bigint] NULL,
								[TransactionTime] [datetime] NULL,
								[DeadlockGraph] [xml] NULL,
								[DeadlockObjects] [nvarchar](max) NULL,
								[Victim] [int] NOT NULL,
								[SPID] [int] NULL,
								[ProcedureName] [varchar](200) NULL,
								[LockMode] [char](1) NULL,
								[Code] [varchar](8000) NULL,
								[ClientApp] [nvarchar](245) NULL,
								[HostName] [varchar](20) NULL,
								[LoginName] [varchar](20) NULL,
								[InputBuffer] [varchar](8000) NULL)

CREATE TABLE #AllResults	(
							Tool					VARCHAR(30),
							[Start Time]			DATETIME,
							[End Time]				DATETIME,
							Duration				VARCHAR(30),
							[Description]			VARCHAR(MAX),
							[Parameters]			VARCHAR(MAX),
							[Run Status]			VARCHAR(100),
							[Last Run By]			VARCHAR(100),
							[Created By]			VARCHAR(100),
							Scheduled				CHAR(1)
							)

INSERT INTO #ExtResults
	EXEC [impresario_cci].[dbo].[LRP_EXTRACTION_SUMMARY] @MinParam;

INSERT INTO #RptResults
	EXEC [impresario].[dbo].[LRP_REPORT_SUMMARY] @MinParam;

INSERT INTO #DeadLockResults 
	EXEC [impresario].[dbo].[LRP_DEADLOCK_SUMMARY] @MinParam;

INSERT INTO #AllResults
	SELECT	'Extract' AS 'Tool',
			StartTime,
			EndTime,
			Duration,
			ExtractDescription AS 'Description',
			'' AS [Parameters],
			RunStatus,
			LastUpdatedBy,
			'' AS 'CreatedBy',
			'' AS 'Scheduled'
	FROM	#ExtResults

	UNION

	SELECT	RptType AS 'Tool',
			StartTime,
			EndTime,
			Duration,
			--REPLACE(REPLACE(REPLACE(REPLACE([Full Report Path], '/Tessitura/CustomReports/', ''), '/Tessitura/ScheduledReports/', ''), '/Tessitura/Reports/', ''), '/', ' > ') AS 'Description', -- /Tessitura/CustomReports/Advancement/Pipeline
			--REPLACE([Full Report Path], '/Tessitura/', '') AS 'Description', -- /Tessitura/CustomReports/Advancement/Pipeline
			CASE 
				WHEN RptType = 'Screen' AND PATINDEX('%,%', [Report Parameters]) > 0 THEN REPLACE(REPLACE([Full Report Path], '/Tessitura/CustomReports/ConstituentCustomScreens/', '') + ' (' + SUBSTRING([Report Parameters], 1, PATINDEX('%,%', [Report Parameters]) - 1) + ')', 'customer_no=', '#')
				WHEN RptType = 'Screen' AND PATINDEX('%,%', [Report Parameters]) = 0 THEN REPLACE(REPLACE([Full Report Path], '/Tessitura/CustomReports/ConstituentCustomScreens/', '') + ' (' + [Report Parameters] + ')', 'customer_no=', '#')
				ELSE [Report Name] 
			END AS 'Description',
			[Report Parameters] AS 'Parameters',
			RunStatus,
			LastRunBy,
			CreatedBy,
			Scheduled
	FROM   #RptResults
	--LEFT JOIN [TR_CUSTOM_TAB] ct
	--ON		[Report Name] = REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(ct.d_object, '%/Tessitura/CustomReports/ConstituentCustomScreens/', ''), '%/Tessitura/CustomReports/EventsCustomScreens/', ''), '%/Tessitura/CustomReports/CampaignCustomScreens/', ''), '&customer_no=<<key>>', ''), '&campaign_no=<<key>>', ''), '&UserGroup=<<ug>>&UserId=<<user>>', '')
	
	UNION

	SELECT	'Deadlock' AS 'Tool',
			[TransactionTime],
			[TransactionTime],
			'',
			[DeadlockObjects] AS 'Description',
			[ProcedureName] AS 'Parameters',
			CASE WHEN [Victim] = 1 THEN 'Victim' ELSE 'Not Victim' END,
			ISNULL([LoginName], ''),
			[ClientApp],
			''
	FROM   #DeadLockResults

INSERT INTO LT_PERFORMANCE_ISSUES_HISTORY
	SELECT	@Now,
			Tool,
			[Start Time],
			[End Time],
			Duration AS 'Duration (Secs)',
			[Description],
			[Parameters],
			[Run Status],
			ISNULL([Last Run By], '') AS 'User',
			ISNULL([Created By], '') AS 'Group',
			Scheduled
	FROM	#AllResults
	ORDER BY [Start Time] DESC;

DROP TABLE #ExtResults
DROP TABLE #DeadLockResults
DROP TABLE #RptResults
DROP TABLE #AllResults


GO


