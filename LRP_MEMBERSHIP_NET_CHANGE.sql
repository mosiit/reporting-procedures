USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LRP_MEMBERSHIP_NET_CHANGE]    Script Date: 9/28/2017 1:16:06 PM ******/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_MEMBERSHIP_NET_CHANGE]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_MEMBERSHIP_NET_CHANGE] AS' 
END
GO

ALTER PROCEDURE [dbo].[LRP_MEMBERSHIP_NET_CHANGE]
            @month_name VARCHAR (20) = Null, 
            @year INT = Null
AS BEGIN

    /*  Procedure Variables*/

        DECLARE @memb_org INT = 4   --4 = Household
        DECLARE @monthly_total INT, @active_members INT
        DECLARE @data_set VARCHAR(20) = 'Current Month'
        DECLARE @data_set_num INT = 1
        DECLARE @include_message CHAR(1) = 'N'
        DECLARE @generic_data_set_name CHAR(1) = 'N'

        DECLARE @output_table TABLE ([data_set_num] INT, [data_set] VARCHAR(20), [data_object_num] INT, [data_object] VARCHAR(25), [data_value] INT, [report_message] VARCHAR(150))
                    
        DECLARE @start_dt DATETIME, @end_dt DATETIME, @expr_dt DATETIME
        DECLARE @last_month_start_dt DATETIME, @last_month_end_dt DATETIME, @last_month_expr_dt DATETIME
        DECLARE @last_year_start_dt DATETIME, @last_year_end_dt DATETIME, @last_year_expr_dt DATETIME

        DECLARE @act_mem INT = 0, @exp_prev INT = 0, @act_at_start INT = 0, @rejoins_mem INT = 0, @renew_mem INT = 0, @new_mem INT = 0, @upgrades_sold INT = 0
        DECLARE @month_total INT = 0, @net_change INT = 0, @obj_num INT = 0, @rpt_msg VARCHAR(150) = ''

    /* Determine all dates needed for the date selection - Expiration date is one day before start date  */

        /*  Start, End, and Expiration dates based on month and year entered as parameters  */

            SELECT @start_dt = CONVERT(DATETIME,(@month_name + ' 1, ' + CONVERT(VARCHAR(4),@year)))
            SELECT @end_dt = DATEADD(DAY,-1,DATEADD(MONTH,1,@start_dt))
            SELECT @expr_dt = DATEADD(DAY,-1,@start_dt)

        /*  Last Month Start, End, and Expiration dates based on one month before the month and year entered as parameters */

            SELECT @last_month_start_dt = DATEADD(MONTH, -1, @start_dt)
            SELECT @last_month_end_dt = DATEADD(DAY,-1,DATEADD(MONTH,1,@last_month_start_dt))
            SELECT @last_month_expr_dt = DATEADD(DAY,-1,@last_month_start_dt)

        /* Last Year Start, End, and Expiration dates based on same month, one year before the year entered as paraemeter  */

            SELECT @last_year_start_dt = DATEADD(YEAR, -1, @start_dt)
            SELECT @last_year_end_dt = DATEADD(DAY,-1,DATEADD(MONTH,1,@last_year_start_dt))
            SELECT @last_year_expr_dt = DATEADD(DAY,-1,@last_year_start_dt)

    /*  The data gathering code is run three separate times, once for the month entered, once for the previous month and once for the previous year
        The BEGIN_DATA_GATHER starting point is used as a start to the loop.  */

    BEGIN_DATA_GATHER:

        
        /*  Data Set Name = Month and Year being Reported On  */

           IF @generic_data_set_name = 'N' SELECT @data_set = DATENAME(MONTH,@start_dt) + ', ' + DATENAME(YEAR,@start_dt)
            
        /*  Number of active members pulled from the TX_CUST_MEMBERSHIP table
            Household memberships with an expiration date on or after the date in the @expr_dt variable but a create date before that date  */

            IF @include_message = 'Y' SELECT @rpt_msg = 'Total memberships including those expired at end of previous month - Based on initilze date and expiration date'
            SELECT @obj_num = 1

            SELECT @act_mem = COUNT(*) FROM [dbo].[TX_CUST_MEMBERSHIP] 
                              WHERE [memb_org_no] = @memb_org and CONVERT(DATE,[expr_dt]) >= @expr_dt 
                                AND CONVERT(DATE,[init_dt]) <= @expr_dt
                                AND current_status <= 3
            SELECT @act_mem = ISNULL(@act_mem,0)

            INSERT INTO @output_table ([data_set_num], [data_set], [data_object_num], [data_object], [data_value], [report_message])
            VALUES (@data_set_num, @data_set, @obj_num, 'Active Members', @act_mem, @rpt_msg)
    
        /*  Number of expired members pulled from the TX_CUST_MEMBERSHIP table
            Household memberships that expired on the date in the @expr_dt variable  */

            IF @include_message = 'Y' SELECT @rpt_msg = 'Subtract the number of memberships that expired at the end of the previous month - Based on expiration date'
            SELECT @obj_num = (@obj_num + 1)

            SELECT @exp_prev = COUNT(*) 
                               FROM [dbo].[TX_CUST_MEMBERSHIP] WHERE [memb_org_no] = @memb_org 
                                 AND CONVERT(DATE,[expr_dt]) = @expr_dt
                                 AND current_status <= 3
            SELECT @exp_prev = ISNULL(@exp_prev,0)

            INSERT INTO @output_table ([data_set_num], [data_set], [data_object_num], [data_object], [data_value], [report_message])
            VALUES (@data_set_num, @data_set, @obj_num, 'Expired Previous Month', (@exp_prev * -1), @rpt_msg)
       
        /*  Number of lapsed/rejoined members pulled from the TX_CUST_MEMBERSHIP table
            Household memberships with a status of RI (Reinitialized) created between the dates in the @start_dt and @end_dt variables  */

            IF @include_message = 'Y' SELECT @rpt_msg = 'Add Number of members who rejoined the museum during the month - Based on initialize date and NRR Status = "RI"'
            SELECT @obj_num = (@obj_num + 1)

            SELECT @rejoins_mem = COUNT(*) 
                                  FROM [dbo].[TX_CUST_MEMBERSHIP] 
                                  WHERE [memb_org_no] = @memb_org 
                                    AND CONVERT(DATE,[init_dt]) BETWEEN @start_dt AND @end_dt 
                                    AND [NRR_status] = 'RI'
                                    AND current_status <= 3
            SELECT @rejoins_mem = ISNULL(@rejoins_mem,0)

            INSERT INTO @output_table ([data_set_num], [data_set], [data_object_num], [data_object], [data_value], [report_message])
            VALUES (@data_set_num, @data_set, @obj_num, 'Lapsed Rejoins', @rejoins_mem, @rpt_msg)

        /*  Number of newly joined members pulled from the TX_CUST_MEMBERSHIP table
            Household memberships with a status of NE (New) created between the dates in the @start_dt and @end_dt variables  */

            IF @include_message = 'Y' SELECT @rpt_msg = 'Add Number of new members who joined the museum during the month - Based on initialize date and NRR Status = "NE"'
            SELECT @obj_num = (@obj_num + 1)

            SELECT @new_mem = COUNT(*) 
                              FROM [dbo].[TX_CUST_MEMBERSHIP] 
                              WHERE [memb_org_no] = @memb_org 
                                AND CONVERT(DATE,[init_dt]) BETWEEN @start_dt AND @end_dt 
                                AND [NRR_status] = 'NE'
                                AND current_status <= 3
            SELECT @new_mem = ISNULL(@new_mem,0)

            INSERT INTO @output_table ([data_set_num], [data_set], [data_object_num], [data_object], [data_value], [report_message])
            VALUES (@data_set_num, @data_set, @obj_num, 'New Joins', @new_mem, @rpt_msg)

        /*  Number of newly renewed memberships pulled from the TX_CUST_MEMBERSHIP table
            Household memberships with a status of RN (Renew) created between the dates in the @start_dt and @end_dt variables  */

            IF @include_message = 'Y' SELECT @rpt_msg = 'Add Number of renewed memberships that took effect during the month - Based on *initialize* date and NRR Status = "RN"'
            SELECT @obj_num = (@obj_num + 1)

            SELECT @renew_mem = COUNT(*) 
                                FROM [dbo].[TX_CUST_MEMBERSHIP] 
                                WHERE [memb_org_no] = @memb_org 
                                  AND CONVERT(DATE,[init_dt]) BETWEEN @start_dt AND @end_dt 
                                  AND [NRR_status] = 'RN'
                                  AND current_status <= 3
            SELECT @renew_mem = ISNULL(@renew_mem,0)

            INSERT INTO @output_table ([data_set_num], [data_set], [data_object_num], [data_object], [data_value], [report_message])
            VALUES (@data_set_num, @data_set, @obj_num, 'Renewed Memberships', @renew_mem, @rpt_msg)

        /*  Total for the month (sum of what's already in the output table for this data set.  */

            IF @include_message = 'Y' SELECT @rpt_msg = 'Total for month = active members - expired previous month + lapsed + new + renew'
            SELECT @obj_num = (@obj_num + 1)

            SELECT @month_total = (@act_mem - @exp_prev + @rejoins_mem + @new_mem + @renew_mem)
            SELECT @month_total = ISNULL(@month_total,0)
        
            INSERT INTO  @output_table ([data_set_num], [data_set], [data_object_num], [data_object], [data_value], [report_message])
            VALUES (@data_set_num, @data_set, @obj_num, 'Total for Month', @month_total, @rpt_msg)

        /*  Net Difference (Monthly Total minus Active Members  */

            IF @include_message = 'Y' SELECT @rpt_msg = 'Net Change for month = total for the month - active members from start'
            SELECT @obj_num = (@obj_num + 1)
                    
            SELECT @net_change = (@month_total - @act_mem)
            SELECT @net_change = ISNULL(@net_change,0)

            INSERT INTO  @output_table ([data_set_num], [data_set], [data_object_num], [data_object], [data_value], [report_message])
            VALUES (@data_set_num, @data_set, @obj_num, 'Net Difference', @net_change, @rpt_msg)

        /*  Number of purchased upgrades taken from the T_SUB_LINE_ITEM table joind with the LV_PRODUCTION_ELEMENTS_PERFORMANCE view
            Total sub_lineitems sold to Membership Upgrade products where the performance date is between the dates in the @start_dt and @end_dt variables  */

            IF @include_message = 'Y' SELECT @rpt_msg = 'Upgrades purchased during the month - Informational only - not included in any calculations'
            SELECT @obj_num = (@obj_num + 1)

            SELECT @upgrades_sold = COUNT(sli.[sli_no]) 
                                    FROM [dbo].[T_SUB_LINEITEM] AS sli (NOLOCK)
                                         LEFT OUTER JOIN [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] AS prf (NOLOCK) ON prf.[performance_no] = sli.[perf_no] AND prf.[performance_zone] = sli.[zone_no]
                                    WHERE prf.[performance_date] BETWEEN CONVERT(CHAR(10),@start_dt,111) AND CONVERT(CHAR(10),@end_dt,111) AND [title_name] = 'Membership' AND prf.[performance_zone_text] LIKE '%Upgrade%'
                                      AND sli.[sli_status] IN (3, 12)
            SELECT @upgrades_sold = ISNULL(@upgrades_sold,0)    

            INSERT INTO @output_table ([data_set_num], [data_set], [data_object_num], [data_object], [data_value], [report_message])
            VALUES (@data_set_num, @data_set, @obj_num, 'Upgrades Purchased', @upgrades_sold, @rpt_msg)

            --SELECT @active_members = [data_value] FROM @output_table WHERE [data_set] = @data_set AND [data_object] = 'Active Members'
            --SELECT @monthly_total = [data_value] FROM @output_table WHERE [data_set] = @data_set AND [data_object] = 'Total for Month'
--            INSERT INTO  @output_table ([data_set_num], [data_set], [data_object], [data_value], [report_message])
--            SELECT @data_set_num, @data_set, 'Net Difference', (@monthly_total - @active_members), ''

    /*  If this is the first data selection (current month), check to make sure some actual data was chosen
        If no, move on.  If yes, change the data set name to Last Month, change the dates and return to beginning of data selection
        If this is the second data selction (Last month), change the data set name to Last Year, change the dates and return to beginning of data selection
        If this is the third data selection (last year), move on  */

        IF @data_set_num = 1 BEGIN
           
            IF NOT EXISTS (SELECT * FROM @output_table WHERE [data_value] <> 0) BEGIN
                DELETE FROM @output_table 
                GOTO FINISHED
            END
          
           SELECT @data_set_num = 2, @data_set = 'Previous Month' 
           SELECT @start_dt = @last_month_start_dt, @end_dt = @last_month_end_dt, @expr_dt = @last_month_expr_dt
           GOTO BEGIN_DATA_GATHER

        END ELSE IF @data_set_num = 2 BEGIN

           SELECT @data_set_num = 3, @data_set = 'Previous Year' 
           SELECT @start_dt = @last_year_start_dt, @end_dt = @last_year_end_dt, @expr_dt = @last_year_expr_dt
           GOTO BEGIN_DATA_GATHER

        END

    /*  Do the math for each row in the table  */
    
--    --    UPDATE @output_table SET [monthly_total] = ([active_members] - [previous_month_expire] + [lapsed_rejoins] + [new_joins])
    
    FINISHED:

        /* If no records found, add a single record with a message in it  */

            IF NOT EXISTS (SELECT * FROM @output_table)
                INSERT INTO @output_table ([data_set_num], [data_set], [data_object], [data_value],[report_message])
                VALUES (1, '', '', 0, 'No data found for ' + @month_name + ', ' + CONVERT(VARCHAR(4),@year) + '.')


        /*  Pull the final data set to be passed to the report.  Group everything so that a single row for each data set (current, last month,
            and last year) is passed to the report  */

            SELECT   [data_set_num]
                   , [data_set]
                   , [data_object]
                   , [data_object_num]
                   , [data_value]
                   , [report_message] 
           FROM @output_table
           ORDER BY [data_set_num], [data_object_num], [data_object]
           
END

GO

GRANT EXECUTE ON [dbo].[LRP_MEMBERSHIP_NET_CHANGE] TO [ImpUsers] AS [dbo]
GO


EXECUTE [dbo].[LRP_MEMBERSHIP_NET_CHANGE] @month_name = 'April', @year = 2019


    

            


            --SELECT @start_dt, @end_dt, @expr_dt, @last_month_start_dt, @last_month_end_dt, @last_month_expr_dt, @last_year_start_dt, @last_year_end_dt, @last_year_expr_dt