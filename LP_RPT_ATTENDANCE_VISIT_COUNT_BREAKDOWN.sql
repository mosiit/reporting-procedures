USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LP_RPT_ATTENDANCE_VISIT_COUNT_BREAKDOWN]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LP_RPT_ATTENDANCE_VISIT_COUNT_BREAKDOWN]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LP_RPT_ATTENDANCE_VISIT_COUNT_BREAKDOWN]
        @report_start_dt datetime = Null,
        @report_end_dt datetime = Null,
        @include_partial_paid char(1) = 'Y'
AS BEGIN

    /*  Procedure Variables  */

        DECLARE @start_date char(10), @end_date char(10)
        DECLARE @ord_no int, @sale_total int, @perf_no INT
        
        DECLARE @mos_buyout_no INT = (SELECT ISNULL([id],0) FROM [dbo].[TR_MOS] WHERE [description] = 'Buyouts')

    /*  Check Parameters  */

        SELECT @start_date = convert(char(10),@report_start_dt,111), @end_date = convert(char(10),@report_end_dt,111)

    /*  Create temp tables needed for this report  */
        
        IF OBJECT_ID('tempdb..#visit_count_raw_data_1') is not null DROP TABLE [#visit_count_raw_data_1]
        CREATE TABLE [#visit_count_raw_data_1] ([order_no] int, [perf_no] int, [zone_no] int, [title_name] varchar(30), [prod_name] varchar(30), [sale_total] int, [scan_admission_total] int)

        IF OBJECT_ID('tempdb..#visit_count_raw_data_2') is not null DROP TABLE [#visit_count_raw_data_2]
        CREATE TABLE [#visit_count_raw_data_2] ([order_no] int, [perf_no] int, [zone_no] int, [comp_code_name] varchar(30), [price_type_name] varchar(30), [sale_total] int)

        IF OBJECT_ID('tempdb..#visit_count_orders') is not null DROP TABLE [#visit_count_orders]
        CREATE TABLE [#visit_count_orders] ([order_no] int, [perf_no] int, [zone_no] int, [sale_total] int, [scan_admission_total] int)

        IF OBJECT_ID('tempdb..#visit_count_final_data') is not null DROP TABLE [#visit_count_final_data]
        CREATE TABLE [#visit_count_final_data] ([sort_group] char(1), [comp_code_name] varchar(30), [price_type_name] varchar(30), [sale_total] int)
        

    /*  Retrieve the Visit Count data for ticketed events from the database  */
            
        INSERT INTO [#visit_count_raw_data_1]
        SELECT [order_no], [perf_no], [zone_no], [title_name], [production_name], sum([sale_total]), sum([scan_admission_total]) 
        FROM [dbo].[LT_HISTORY_TICKET]
        WHERE [performance_date] between @start_date and @end_date and 
             ([title_name] in (SELECT title_name FROM [dbo].[LV_SS_PRODUCTION_ELEMENTS_TITLE] WHERE [visit_count_title] = 'Y')
           OR [production_name] IN (SELECT [production_name] FROM [dbo].[LV_PRODUCTION_ELEMENTS_PRODUCTION] WHERE [visit_count_production] = 'Y'))
        GROUP BY [order_no], [perf_no], [zone_no], [title_name], [production_name] ORDER BY [order_no]
        
        DELETE FROM [#visit_count_raw_data_1] WHERE prod_name like '%Buyout%'

            /*  Added 2/7/2018:  Hard Coded exception added for the Exhibit Halls Special production, which is in the 
                         Exhibit Halls title but should not be counted in with the visit count.  */
         
        DELETE FROM [#visit_count_raw_data_1] WHERE [prod_name] = 'Exhibit Halls Special'
 
    /*  Added 10/26/2017:  Deletes orders from the visit count that were created in the buyouts mode of sale regardless of what the production name is.
                           @mos_buyout_no is defined above and gets the mode of sale number from the TR_MOS table.  
                           It must find and if number > 0 for this to happen.  */

            DELETE FROM [#visit_count_raw_data_1]
            WHERE [order_no] IN (SELECT [order_no] FROM [dbo].[T_ORDER] WHERE [order_no] IN (SELECT [order_no] FROM [#visit_count_raw_data_1]) AND [MOS] = @mos_buyout_no)

    /*  Get list of visit count orders*/
        
        INSERT INTO [#visit_count_orders] SELECT [order_no], 0, 0, MAX([sale_total]), max([scan_admission_total]) FROM [#visit_count_raw_data_1] GROUP By [Order_no]

        UPDATE [#visit_count_orders]
        SET [#visit_count_orders].[perf_no] = (SELECT MIN(rd1.[perf_no]) FROM [#visit_count_raw_data_1] AS rd1 
                                               WHERE rd1.[order_no] = [#visit_count_orders].[order_no] AND rd1.[sale_total] = [#visit_count_orders].[sale_total])

        UPDATE [#visit_count_orders]
        SET [#visit_count_orders].[zone_no] = (SELECT MIN(rd1.[zone_no]) FROM [#visit_count_raw_data_1] AS rd1 
                                               WHERE rd1.[order_no] = [#visit_count_orders].[order_no] AND rd1.[sale_total] = [#visit_count_orders].[sale_total] AND rd1.[perf_no] = [#visit_count_orders].[perf_no])

        INSERT INTO [#visit_count_raw_data_2]
        SELECT vco.[order_no], vco.[perf_no], vco.[zone_no], his.[comp_code_name], his.[price_type_name], his.[sale_total]
        FROM [#visit_count_orders] AS vco (NOLOCK)
             LEFT OUTER JOIN [dbo].[LT_HISTORY_TICKET] AS his (NOLOCK) ON his.[order_no] = vco.[order_no] AND his.[perf_no] = vco.[perf_no] AND his.[zone_no] = vco.[zone_no]
        WHERE his.[title_name] <> 'Membership Card Scan' 

    /*  Combine price types to their base levels to shorten the report  */

        UPDATE [#visit_count_raw_data_2] SET [Price_type_name] = RIGHT([Price_type_name], LEN([Price_type_name]) - 7) WHERE [Price_type_name] LIKE 'Add-on %'
        UPDATE [#visit_count_raw_data_2] SET [Price_type_name] = RIGHT([Price_type_name], LEN([Price_type_name]) - 7) WHERE [Price_type_name] LIKE 'School %'
        UPDATE [#visit_count_raw_data_2] SET [Price_type_name] = RIGHT([Price_type_name], LEN([Price_type_name]) - 6) WHERE [Price_type_name] LIKE 'Group %'
        UPDATE [#visit_count_raw_data_2] SET [Price_type_name] = RIGHT([Price_type_name], LEN([Price_type_name]) - 14) WHERE [Price_type_name] LIKE 'Tour Operator %'

    /*  If the coupon price type has the word adult, senior, or child in the name, put it into the appropriate group  */

        UPDATE [#visit_count_raw_data_2] SET [comp_code_name] = '', [price_type_name] = 'Senior' WHERE [comp_code_name] <> '' AND [price_type_name] like '%Senior%'
        UPDATE [#visit_count_raw_data_2] SET [comp_code_name] = '', [price_type_name] = 'Adult' WHERE [comp_code_name] <> '' AND [price_type_name] like '%Adult%'
        UPDATE [#visit_count_raw_data_2] SET [comp_code_name] = '', [price_type_name] = 'Child' WHERE [comp_code_name] <> '' AND [price_type_name] like '%Child%'
    
    /*  Coupons that do not have the word adult, senior, or child in the name are lumped into one group called Coupon/Pass*/
        
        UPDATE [#visit_count_raw_data_2] SET [comp_code_name] = 'Coupon/Pass', [price_type_name] = 'Coupon/Pass' WHERE [comp_code_name] <> '' OR [price_type_name] = 'Member Pass'

    /*  Insert aggregated data  into the final table */
    
        INSERT INTO [#visit_count_final_data]
        SELECT '', [comp_code_name], [price_type_name], SUM([sale_total]) FROM [#visit_count_raw_data_2]
        GROUP BY [comp_code_name], [price_type_name]

    /*  Set the sort_group values so that the price types appear in the proper order on the report  */

        UPDATE [#visit_count_final_data] SET [sort_group] = 'A' WHERE [price_type_name] like '%Adult%'
        UPDATE [#visit_count_final_data] SET [sort_group] = 'B' WHERE [price_type_name] LIKE '%Senior%'
        UPDATE [#visit_count_final_data] SET [sort_group] = 'C' WHERE [price_type_name] LIKE '%Child%'
        UPDATE [#visit_count_final_data] SET [sort_group] = 'D' WHERE [price_type_name] LIKE '%Member%'
        UPDATE [#visit_count_final_data] SET [sort_group] = 'E' WHERE [price_type_name] LIKE '%Guide%'
        UPDATE [#visit_count_final_data] SET [sort_group] = 'F' WHERE LEFT([price_type_name],7) IN ('Chapero', 'Student', 'Teacher')
        UPDATE [#visit_count_final_data] SET [sort_group] = 'G' WHERE [price_type_name] LIKE '%Internal%' OR [price_type_name] LIKE '%External%'
    
    /*  Sort group on anything else (that doesn't meet any of the above criteria) is set to Z  */

        UPDATE [#visit_count_final_data] SET [sort_group] = 'Z' WHERE [sort_group] = ''
         
    DONE:

        /* Select aggregated date into the report  */

            SELECT [sort_group], [comp_code_name], [price_type_name], SUM([sale_total]) AS 'sale_total'
            FROM [#visit_count_final_data]
            GROUP BY [sort_group], [comp_code_name], [price_type_name]
            ORDER BY [sort_group], [comp_code_name], [price_type_name]
  
    CLEAN_UP:

        /*  Not really necessary, but the programmer in me wants to clean up  */

        IF OBJECT_ID('tempdb..#visit_count_raw_data_1') is not null DROP TABLE [#visit_count_raw_data_1]
        IF OBJECT_ID('tempdb..#visit_count_raw_data_2') is not null DROP TABLE [#visit_count_raw_data_2]
        IF OBJECT_ID('tempdb..#visit_count_orders') is not null DROP TABLE [#visit_count_orders]
        IF OBJECT_ID('tempdb..#visit_count_final_data') is not null DROP TABLE [#visit_count_final_data]

END
GO

GRANT EXECUTE ON [dbo].[LP_RPT_ATTENDANCE_VISIT_COUNT_BREAKDOWN] to impusers
GO


--EXECUTE [dbo].[LP_RPT_ATTENDANCE_VISIT_COUNT_BREAKDOWN] @report_start_dt = '10-13-2017', @report_end_dt = '10-13-2017', @include_partial_paid = 'Y'


