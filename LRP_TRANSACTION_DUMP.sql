USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_TRANSACTION_DUMP]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LRP_TRANSACTION_DUMP]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/*  Pass a date and an operator id to the procedure and it will produce a timestamped list of every transactions that operator
    created or worked on during that day.  */

CREATE PROCEDURE [dbo].[LRP_TRANSACTION_DUMP]
        @report_start_dt datetime, 
        @operator_id varchar(8)
AS BEGIN    

    DECLARE @return_message varchar(100), @report_end_dt datetime

    DECLARE @order_table table (order_no int, [changed_by] varchar(8), [change_dt] varchar(20))

    DECLARE @order_info table ([change_dt] varchar(20), [changed_by] varchar(8), [changed_by_name] varchar(60), [changed_by_department] varchar(30), [order_no] int, [order_create_loc] varchar(16), [order_created_by] varchar(8),
                               [order_create_dt] datetime, [MOS_no] int, [MOS_name] varchar(30), [cancel_ind] char(1), [order_dt] datetime, [batch_no] int, [created_by] varchar(8), [create_dt] datetime, 
                               [create_loc] varchar(20), [last_updated_by] varchar(8), [last_update_dt] datetime, [sort_dt] datetime, [tot_ticket_purch_amt] decimal(18,2),  [tot_ticket_return_amt] decimal(18,2),  
                               [tot_fee_amt] decimal(18,2), [tot_contribution_amt] decimal(18,2),  [tot_due_amt] decimal(18,2),  [tot_ticket_paid_amt] decimal(18,2),  [tot_fee_paid_amt] decimal(18,2),  
                               [tot_contribution_paid_amt] decimal(18,2), [tot_paid_amt] decimal(18,2),  [transaction_no] decimal(18,2), [customer_no] int, [first_name] varchar(30), [middle_name] varchar(30), 
                               [last_name] varchar(100))

    DECLARE @detail_table table ([change_dt] varchar(20), [changed_by] varchar(8), [changed_by_name] varchar(60), [changed_by_department] varchar(30), [record_type] varchar(20), [order_no] int, [order_create_loc] varchar(16),
                                 [order_created_by] varchar(8), [order_create_dt] datetime, [MOS_no] int, [MOS_name] varchar(30), [batch_no] int, [sli_no] int, [li_seq_no] int, [sli_status_no] int, [sli_status_name] varchar(30), 
                                 [title_no] int, [title_name] varchar(30), [production_no] int, [production_name] varchar(30), [season_no] int, [season_name] varchar(30), [perf_no] int, [zone_no] int, [performance_date] char(10), 
                                 [performance_time] char(8), [price_type_no] int, [price_type_name] varchar(30), [comp_code_no] int, [comp_code_name] varchar(30), [counter] int, [due_amount] decimal(18,2), 
                                 [current_price] decimal(18,2), [price_edited_count] int, [returned_amount] decimal(18,2), [total_amount] decimal(18,2), [paid_amount] decimal(18,2), [returned_payment] decimal(18,2), 
                                 [total_paid] decimal(18,2), [fee_amt] decimal(18,2), [fee_parent_sli_no] int, [seat_no] int, [ticket_no] int, [created_by] varchar(8), [create_dt] datetime, [create_loc] varchar(30), 
                                 [last_updated_by] varchar(8), [last_update_dt] datetime, [time_stamp] datetime, [ret_parent_sli_no] int, [customer_no] int, [customer_name] varchar(100), [recipient_no] int, 
                                 [recipient_name] varchar(75), [rule_id] int, [rule_name] varchar(30), [rule_ind] char(1), [original_price_type] int, [report_message] varchar(100))

    
    /*  Verify parameters  */

    SELECT @operator_id = IsNull(@operator_id, '')
    IF @report_start_dt is null BEGIN
        SELECT @return_message = 'start date parameters is invalid.'
        GOTO DONE
    END ELSE IF @operator_id = '' BEGIN
        SELECT @return_message = 'Operator id parameter is invalid.'
        GOTO DONE
    END ELSE BEGIN
        SELECT @return_message = ''
    END

    /*  Make sure start date parameter starts at midnight and end date parameter ends at 11L59 PM  */

    SELECT @report_start_dt = (convert(char(10), @report_start_dt, 111) + ' 00:00:00'),
           @report_end_dt = (convert(char(10), @report_start_dt, 111) + ' 23:59:59')
           
    /*  Using a temp table called @order_table, create a list of all the orders for the designated cashirt on the designated date.
        The first pull looks for any record in T_SUB_LINEITEM where the create date matches and the created by operator matches
        of the last updated date matches and the last updated operator matches.  This should give a list of the orders that were
        created by or changed by this operator on this date.  */

    INSERT INTO @order_table
    SELECT DISTINCT order_no, null, null FROM T_SUB_LINEITEM 
    WHERE (create_dt between @report_start_dt and @report_end_dt and created_by = @operator_id)
       or (last_update_dt between @report_start_dt and @report_end_dt and last_updated_by = @operator_id)

    /*  Using the list of order numbers, pull two more lists, one for orders created by and one for orders updated by the cashier
        Once these two lists of orders are pulled, delete the initial list.  It's not needed anymore.  Also delete any record where
        the operator is not the designated operator in the @operator_id parameter.  */

    INSERT INTO @order_table
    SELECT DISTINCT [order_no], [created_by], left(convert(varchar(20), [create_dt], 120),16) FROM T_SUB_LINEITEM WHERE order_no in (SELECT order_no FROM @order_table WHERE [change_dt] is null)
    
    INSERT INTO @order_table
    SELECT DISTINCT [order_no], [last_updated_by], left(convert(varchar(20), [last_update_dt], 120),16) FROM T_SUB_LINEITEM WHERE order_no in (SELECT order_no FROM @order_table WHERE [change_dt] is null)

    DELETE FROM @order_table WHERE change_dt is null

    DELETE FROM @order_table WHERE changed_by <> @operator_id

    /*  Using the @order_table temp table (which contains the final list of order id numbers), pull additional information about the order 
        into another temp table called @order_info.  */

    INSERT INTO @order_info
    SELECT DISTINCT otb.change_dt, otb.changed_by, (usr.[fname] + ' ' + usr.[lname]), usr.[location], otb.[order_no], ord.[create_loc], ord.[created_by], ord.[create_dt], ord.[MOS], mos.[description], 
                    ord.[cancel_ind], ord.[order_dt], ord.[batch_no], ord.[created_by], ord.[create_dt], ord.[create_loc], ord.[last_updated_by], ord.[last_update_dt], null, ord.[tot_ticket_purch_amt], 
                    ord.[tot_ticket_return_amt], ord.[tot_fee_amt], ord.[tot_contribution_amt], ord.[tot_due_amt], ord.[tot_ticket_paid_amt], ord.[tot_fee_paid_amt], ord.[tot_contribution_paid_amt], 
                    ord.[tot_paid_amt],  ord.[transaction_no], ord.[customer_no], IsNull(cus.[fname], ''), IsNull(cus.[mname], ''), IsNull(cus.[lname], '')
    FROM @order_table as otb 
         LEFT OUTER JOIN [dbo].[T_ORDER] as ord (NOLOCK) ON ord.[order_no] = otb.[order_no]
         LEFT OUTER JOIN [dbo].[T_CUSTOMER] as cus (NOLOCK) ON cus.[customer_no] = ord.[customer_no]
         LEFT OUTER JOIN [dbo].[TR_MOS] as mos (NOLOCK) ON mos.[id] = ord.[MOS]
         LEFT OUTER JOIN [dbo].[T_METUSER] as usr (NOLOCK) ON usr.[userid] = otb.[changed_by]
    WHERE ord.[order_no] in (SELECT [order_no] FROM @order_table)

    /* Once the order info is gathered, use the @order_info temp table as a starting point to pull the specific detail about the 
       line items within the order.  To help with this, a local view called LV_ORDER_DETAIL has been created.  */

    INSERT INTO @detail_table
    SELECT oin.[change_dt], oin.[changed_by], oin.[changed_by_name], oin.[changed_by_department], 'Details', oin.[order_no], oin.[order_create_loc], oin.[order_created_by], [order_create_dt], oin.[MOS_no], 
           oin.[MOS_name], oin.[batch_no], odt.[sli_no], odt.[li_seq_no], odt.[sli_status_no], odt.[sli_status_name], odt.[title_no], odt.[title_name], odt.[production_no], odt.[production_name], odt.[season_no], 
           odt.[season_name], odt.[perf_no], odt.[zone_no], odt.[performance_date], odt.[performance_time], odt.[price_type_no], odt.[price_type_name], odt.[comp_code_no], odt.[comp_code_name_short], 1, odt.[due_amount], 
           odt.[current_price], 0, odt.[returned_amount], odt.[total_amount], odt.[paid_amount], odt.[returned_payment], odt.[total_paid], odt.[fee_amt], odt.[fee_parent_sli_no], odt.[seat_no], odt.[ticket_no], 
           odt.[created_by], odt.[create_dt], odt.[create_loc_description], odt.[last_updated_by], odt.[last_update_dt], odt.[create_dt], odt.[ret_parent_sli_no], oin.[customer_no], 
           (CASE WHEN oin.[first_name] = '' THEN '' ELSE oin.[first_name] + ' ' END + CASE WHEN oin.[middle_name] = '' THEN '' ELSE oin.[middle_name] + ' ' END + oin.[last_name]), 
           odt.[recipient_no], odt.[recipient_name], odt.[rule_id], odt.[rule_name], odt.[rule_ind], odt.[original_price_type], ''
    FROM @order_info as oin
         LEFT OUTER JOIN [dbo].[LV_ORDER_DETAIL] as odt (NOLOCK) ON odt.order_no = oin.[order_no]

    /*  Check for edited prices  */
       
    UPDATE @detail_table SET [price_edited_count] = 1 WHERE ([due_amount] <> [current_price]) and (([due_amount] * -1) <> [current_price])

    /* Using the @order_info temp table again as a starting point, pull the specific detail about the 
       payments made to the order.  To help with this, a local view called LV_ORDER_PAYMENTS has been created.  */
    
    INSERT INTO @detail_table
    SELECT oin.[change_dt], oin.[changed_by], oin.[changed_by_name], oin.[changed_by_department], 'Payments', oin.[order_no], oin.[order_create_loc], oin.[order_created_by], oin.[order_create_dt], oin.[MOS_no], 
           oin.[MOS_name], oin.[batch_no], 0, 0, 0, '', 0, '', IsNull(pay.[transaction_type_no], 0), IsNull(pay.[transaction_type_name], 'NO PAYMENT'), 0, '', IsNull(pay.[payment_no], 0), 
           0, IsNull(convert(char(10),pay.[payment_dt],111), ''), IsNull(left(convert(char(8),pay.[payment_dt],108),5), ''), IsNull(pay.[payment_method_no], 0), 
           IsNull(pay.[payment_method_name], 'No Payments'), 0, '', 1, 0.00, 0.00, 0, 0.00, 0.00, sum(IsNull(pay.[payment_amount], 0.00)), 0, sum(IsNull(pay.[transaction_amount], 0.00)),  0.00, 0, 0, 0, IsNull(pay.[payment_created_by], 
           pay.[transaction_created_by]), IsNull(pay.[payment_create_dt], pay.[transaction_create_dt]), IsNull(pay.[transaction_create_loc_description], ''), IsNull(pay.[payment_created_by], pay.[transaction_created_by]), 
           IsNull(pay.[payment_create_dt], pay.[transaction_create_dt]), IsNull(pay.[payment_create_dt], pay.[transaction_create_dt]), 0, oin.[customer_no], 
           (CASE WHEN oin.[first_name] = '' THEN '' ELSE oin.[first_name] + ' ' END + CASE WHEN oin.[middle_name] = '' THEN '' ELSE oin.[middle_name] + ' ' END + oin.[last_name]), 
           0, '', 0, '', '', 0, ''
    FROM @order_info as oin
         LEFT OUTER JOIN [dbo].[LV_ORDER_PAYMENTS] as pay (NOLOCK) on pay.[order_no] = oin.[order_no]
    GROUP BY oin.[change_dt], oin.[changed_by], oin.[changed_by_name], oin.[changed_by_department], oin.[order_no], oin.[order_create_loc], oin.[order_created_by], oin.[order_create_dt], oin.[MOS_no], 
           oin.[MOS_name], oin.[batch_no], IsNull(pay.[transaction_type_no], 0), IsNull(pay.[transaction_type_name], 'NO PAYMENT'), IsNull(pay.[payment_no], 0), 
           IsNull(convert(char(10),pay.[payment_dt],111), ''), IsNull(left(convert(char(8),pay.[payment_dt],108),5), ''), IsNull(pay.[payment_method_no], 0), 
           IsNull(pay.[payment_method_name], 'No Payments'), IsNull(pay.[payment_created_by], 
           pay.[transaction_created_by]), IsNull(pay.[payment_create_dt], pay.[transaction_create_dt]), IsNull(pay.[transaction_create_loc_description], ''), IsNull(pay.[payment_created_by], pay.[transaction_created_by]), 
           IsNull(pay.[payment_create_dt], pay.[transaction_create_dt]), IsNull(pay.[payment_create_dt], pay.[transaction_create_dt]), oin.[customer_no], 
           (CASE WHEN oin.[first_name] = '' THEN '' ELSE oin.[first_name] + ' ' END + CASE WHEN oin.[middle_name] = '' THEN '' ELSE oin.[middle_name] + ' ' END + oin.[last_name]) 
    
    
    /*  Delete Zero Payments */

    DELETE FROM @detail_table WHERE [record_type] = 'Payments' and [price_type_name] <> 'No Payments' and [due_amount] = 0.00 and [paid_amount] = 0.00          
    
    /*  Update some of the price type names and status names to be shorter and look nicer  */

    UPDATE @detail_table SET [price_type_name] = 'Cash' WHERE [record_type] = 'Payments' and [price_type_name] like '%Cash%'
    UPDATE @detail_table SET [price_type_name] = 'Amex' WHERE [record_type] = 'Payments' and [price_type_name] like '%Amex%'
    UPDATE @detail_table SET [price_type_name] = 'Amex' WHERE [record_type] = 'Payments' and [price_type_name] like '%American Express%'
    UPDATE @detail_table SET [price_type_name] = 'Visa' WHERE [record_type] = 'Payments' and [price_type_name] like '%Visa%'
    UPDATE @detail_table SET [price_type_name] = 'MasterCard' WHERE [record_type] = 'Payments' and [price_type_name] like '%Master%'
    UPDATE @detail_table SET [price_type_name] = 'Discover' WHERE [record_type] = 'Payments' and [price_type_name] like '%Discover%'

    UPDATE @detail_table SET [price_type_name] = REPLACE([price_type_name], 'Tour Operator', 'TOP') WHERE [price_type_name] LIKE '%Tour Operator%'
    UPDATE @detail_table SET [price_type_name] = 'Library Paid' WHERE [price_type_name] LIKE '%Library Paid%'
    UPDATE @detail_table SET [price_type_name] = 'Library Free' WHERE [price_type_name] LIKE '%Library Free%'

    UPDATE @detail_table SET [sli_status_name] = 'Voided' WHERE [record_type] = 'Details' and [sli_status_name] like '%Void-Returned in this order%'
    UPDATE @detail_table SET [sli_status_name] = 'Voided/Other' WHERE [record_type] = 'Details' and [sli_status_name] like '%Void-Returned in other order%'
    UPDATE @detail_table SET [sli_status_name] = 'Paid' WHERE [record_type] = 'Details' and [sli_status_name] like '%Ticketed, Paid%'
    UPDATE @detail_table SET [sli_status_name] = 'Unpaid' WHERE [record_type] = 'Details' and [sli_status_name] like '%Ticketed, Unaid%'
            
    DONE:

         /* If no records found, insert a single record into the final table  */
  
    	   IF (SELECT COUNT([order_no]) FROM @detail_table) = 0
               INSERT INTO @detail_table
               VALUES (null, @operator_id, '', '', '', 0, '', '', null, 0, '', 0, 0, 0, 0, '', 0, '', 0, '', 0, '', 0, 0, '', '', 0, '', 0, '', 0, 0.00, 0.00, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 
                       0, '', null, '', '', null, null, 0, 0, '', 0, 0, 0, '', '', 0, 'No records found that meet the criteria entered.')
    
        /*  Select final recordset from the @detail_table temp table.  */

        --DROP TABLE [TemporaryData].[dbo].[TransactionDumpTest1]

        --SELECT [change_dt], [time_stamp], [changed_by], [changed_by_name], [changed_by_department], [record_type], [order_no], [order_create_loc], [order_created_by], [order_create_dt], [MOS_no], [MOS_name], [batch_no], [sli_no], 
        --       [li_seq_no], [sli_status_no], [sli_status_name], [title_no], [title_name], [production_no], [production_name], [season_no], [season_name], [perf_no], [zone_no], [performance_date], [performance_time], 
        --       [price_type_no], [price_type_name], [comp_code_no], [comp_code_name], [counter], [due_amount], [current_price], [price_edited_count], [returned_amount], [total_amount], [paid_amount], [returned_payment], 
        --       [total_paid], [fee_amt], [fee_parent_sli_no], [seat_no], [ticket_no], [created_by], [create_dt], [create_loc], [last_updated_by], [last_update_dt], [ret_parent_sli_no], [customer_no], [customer_name], 
        --       [recipient_no], [recipient_name], [rule_id], [rule_name], [rule_ind], [original_price_type], [report_message], @report_start_dt as 'p_report_start_dt', @report_end_dt as 'p_report_end_dt'
        --INTO [TemporaryData].[dbo].[TransactionDumpTest1] FROM @detail_table

        --SELECT DISTINCT [price_type_name] fROM @detail_table WHERE record_type = 'Payments'

        SELECT [change_dt], [time_stamp], [changed_by], [changed_by_name], [changed_by_department], [record_type], [order_no], [order_create_loc], [order_created_by], 
               [order_create_dt], [MOS_no], [MOS_name], [batch_no], [sli_no], [li_seq_no], [sli_status_no], [sli_status_name], [title_no], [title_name], [production_no], [production_name], [season_no], 
               [season_name], [perf_no], [zone_no], [performance_date], [performance_time], [price_type_no], [price_type_name], [comp_code_no], [comp_code_name], [counter], [due_amount], [current_price], 
               [price_edited_count], [returned_amount], [total_amount], [paid_amount], [returned_payment], [total_paid], [fee_amt], [fee_parent_sli_no], [seat_no], [ticket_no], [created_by], [create_dt], 
               [create_loc], [last_updated_by], [last_update_dt], [ret_parent_sli_no], [customer_no], [customer_name], [recipient_no], [recipient_name], [rule_id], [rule_name], [rule_ind], [original_price_type], 
               [report_message], @report_start_dt as 'p_report_start_dt', @report_end_dt as 'p_report_end_dt'
        FROM @detail_table

END
GO
         
GRANT EXECUTE ON [dbo].[LRP_TRANSACTION_DUMP] TO Impusers
GO



--EXECUTE [dbo].[LRP_TRANSACTION_DUMP] @report_start_dt = '7-5-2016', @operator_id = 'jbattl00'
--EXECUTE [dbo].[LRP_TRANSACTION_DUMP] @report_start_dt = '5-30-2016', @operator_id = 'sso00'
--EXECUTE [dbo].[LRP_TRANSACTION_DUMP] @report_start_dt = '5-30-2016', @operator_id = 'candre00'

--SELECT * FROM T_PAYMENT WHERE convert(char(10),pmt_dt,111) = '2016/05/30' and created_by = 'WebAPI'
--SELECT * FROM [TemporaryData].[dbo].[TransactionDumpTest1] WHERE record_type = 'Payments'
--SELECT * FROM [TemporaryData].[dbo].[TransactionDumpTest1] WHERE order_no = 202284
--SELECT * FROM [dbo].[LV_ORDER_PAYMENTS] WHERE order_no = 202284
--SELECT * FROM T_TRANSACTION WHERE order_no = 202284
--SELECT * FROM T_PAYMENT WHERE transaction_no = 234937

--SELECT * FROM [TemporaryData].[dbo].[TransactionDumpTest1] WHERE record_type = 'Payments'