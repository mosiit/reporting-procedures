USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LRP_ATTEND_BUDGET_VS_GROSS]    Script Date: 7/11/2019 11:03:10 AM ******/
DROP PROCEDURE [dbo].[LRP_ATTEND_BUDGET_VS_GROSS]
GO

/****** Object:  StoredProcedure [dbo].[LRP_ATTEND_BUDGET_VS_GROSS]    Script Date: 7/11/2019 11:03:10 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LRP_ATTEND_BUDGET_VS_GROSS]
	@WeekEnding	DATETIME,
	@WhichYear	VARCHAR(4) = 'This' -- 'This' or 'Last'
AS BEGIN

    SET NOCOUNT ON;
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

    /*  Procedure Variables  */    

        DECLARE @tbl_Budget TABLE (which_year VARCHAR(4), history_dt DATE, budgetAmt INT)
        DECLARE	@tbl_Attend TABLE (which_year VARCHAR(4), history_dt DATE, attendAmt INT)

        DECLARE @StartDate          DATETIME,
                @EndDate            DATETIME,
                @FiscalYear         INT,
                @StartDateLast      DATETIME,
                @EndDateLast        DATETIME,
                @FiscalYearLast     INT,
                @WeekdayAttendThis  INT,
                @WeekendAttendThis  INT,
                @WeekdayAttendLast  INT,
                @WeekendAttendLast  INT,
                @WeekdayBudgetThis  INT,
                @WeekendBudgetThis  INT,
                @WeekdayBudgetLast  INT,
                @WeekendBudgetLast  INT

    /*  Determine Dates and Fiscal Years  */

        SELECT	@StartDate = cur_week_start_dt,
                @EndDate = cur_week_end_dt,
                @FiscalYear = cur_fiscal_year,
                @StartDateLast = prv_week_start_dt,
                @EndDateLast = prv_week_end_dt,
                @FiscalYearLast = prv_fiscal
        FROM [dbo].[LFT_ATTENDANCE_WEEK_DATES] (@WeekEnding, 0, 'Y')

    /*  Get This Year's Attendance Information*/

        INSERT INTO @tbl_Attend
	    SELECT 'This', 
               CAST(history_dt AS DATE), 
               SUM(visit_count) 
        FROM LT_HISTORY_VISIT_COUNT 
        WHERE [history_dt] BETWEEN @StartDate AND @EndDate 
        GROUP BY CAST(history_dt AS DATE)

    /*  Get Last Year's Attendance Information  */

        INSERT INTO @tbl_Attend
		SELECT 'Last', 
                CAST(history_dt AS DATE), 
                SUM(visit_count) 
        FROM LT_HISTORY_VISIT_COUNT 
        WHERE history_dt BETWEEN @StartDateLast AND @EndDateLast 
        GROUP BY CAST(history_dt AS DATE)


    /*  Get This Year's Budget Information  */
      
        INSERT INTO @tbl_Budget
        SELECT 'This',
               CAST([perf_dt] AS DATE),
               [budget]
        FROM   [dbo].[LTR_ATTENDANCE_BUDGET_DATA]
        WHERE [perf_dt] BETWEEN @StartDate AND @EndDate
              AND [title_group] = 'Entire Museum'
              AND [inactive] = 'N';

        INSERT INTO @tbl_Budget
        SELECT 'Last',
               CAST([perf_dt] AS DATE),
               [budget]
        FROM   [dbo].[LTR_ATTENDANCE_BUDGET_DATA]
        WHERE [perf_dt] BETWEEN @StartDateLast AND @EndDateLast
              AND [title_group] = 'Entire Museum'
              AND [inactive] = 'N';

    /*  Pull Budget Numbers  */

        SELECT @WeekdayBudgetThis = SUM([budgetAmt])
                                    FROM @tbl_Budget
                                    WHERE [which_year] = 'This'
                                      AND DATENAME(dw, [history_dt]) IN ('Monday','Tuesday','Wednesday','Thursday','Friday')

        SELECT @WeekendBudgetThis = SUM([budgetAmt])
               FROM @tbl_Budget
               WHERE [which_year] = 'This'
                 AND DATENAME(dw, [history_dt]) IN ('Saturday','Sunday')

        SELECT @WeekdayBudgetLast = SUM([budgetAmt])
                                    FROM @tbl_Budget
                                    WHERE [which_year] = 'Last'
                                      AND DATENAME(dw, [history_dt]) IN ('Monday','Tuesday','Wednesday','Thursday','Friday')

        SELECT @WeekendBudgetLast = SUM([budgetAmt])
               FROM @tbl_Budget
               WHERE [which_year] = 'Last'
                 AND DATENAME(dw, [history_dt]) IN ('Saturday','Sunday')

    /*  Pull This Year's Attendance Numbers  */

        SELECT @WeekdayAttendThis = SUM([attendAmt])
                                    FROM @tbl_Attend
                                    WHERE DATENAME(dw, [history_dt]) IN ('Monday','Tuesday','Wednesday','Thursday','Friday')
                                      AND [which_year] = 'This'

        SELECT @WeekendAttendThis = SUM([attendAmt])
                                    FROM @tbl_Attend
                                    WHERE DATENAME(dw, [history_dt]) IN ('Saturday','Sunday')
                                    AND [which_year] = 'This'

    /*  Pull Last Year's Attendance Number  */

        SELECT @WeekdayAttendLast = SUM([attendAmt])
                                    FROM @tbl_Attend
                                    WHERE DATENAME(dw, [history_dt]) IN ('Monday','Tuesday','Wednesday','Thursday','Friday')
                                    AND [which_year] = 'Last'

        SELECT	@WeekendAttendLast = SUM([attendAmt])
                                     FROM @tbl_Attend
                                     WHERE DATENAME(dw, [history_dt]) IN ('Saturday','Sunday')
                                       AND [which_year] = 'Last'

    FINISHED:

            IF @WhichYear = 'This'
                SELECT	@WeekdayBudgetThis AS [Weekday Budget],
                        @WeekendBudgetThis AS [Weekend Budget],
                        @WeekdayAttendThis AS [Weekday Attend],
                        @WeekendAttendThis AS [Weekend Attend],
                        'Fiscal Year ' + RIGHT(CONVERT(VARCHAR(4), @FiscalYear), 2) AS [Fiscal Year],
                        @WeekdayAttendThis AS [Weekday Attend This Year],
                        @WeekendAttendThis AS [Weekend Attend This Year],
                        CONVERT(DECIMAL(18,4), ((CONVERT(DECIMAL(18,4),@WeekdayAttendThis) - CONVERT(DECIMAL(18,4),@WeekdayBudgetThis))/CONVERT(DECIMAL(18,4),@WeekdayBudgetThis))) AS [ActVsBud or ThisVsLast WeekDay],
                        CONVERT(DECIMAL(18,4), ((CONVERT(DECIMAL(18,4),@WeekendAttendThis) - CONVERT(DECIMAL(18,4),@WeekendBudgetThis))/CONVERT(DECIMAL(18,4),@WeekendBudgetThis))) AS [ActVsBud or ThisVsLast WeekEnd]

            IF @WhichYear = 'Last'
                SELECT	@WeekdayBudgetLast AS [Weekday Budget],
                        @WeekendBudgetLast AS [Weekend Budget],
                        @WeekdayAttendLast AS [Weekday Attend],
                        @WeekendAttendLast AS [Weekend Attend],
                        'Fiscal Year ' + RIGHT(CONVERT(VARCHAR(4), @FiscalYearLast), 2) AS [Fiscal Year],
                        @WeekdayAttendThis AS [Weekday Attend This Year],
                        @WeekendAttendThis AS [Weekend Attend This Year],
                        CONVERT(DECIMAL(18,4), ((CONVERT(DECIMAL(18,4),@WeekdayAttendThis) - CONVERT(DECIMAL(18,4),@WeekdayAttendLast))/CONVERT(DECIMAL(18,4),@WeekdayAttendLast))) AS [ActVsBud or ThisVsLast WeekDay],
                        CONVERT(DECIMAL(18,4), ((CONVERT(DECIMAL(18,4),@WeekendAttendThis) - CONVERT(DECIMAL(18,4),@WeekendAttendLast))/CONVERT(DECIMAL(18,4),@WeekendAttendLast))) AS [ActVsBud or ThisVsLast WeekEnd]


END
GO

EXECUTE  [dbo].[LRP_ATTEND_BUDGET_VS_GROSS] @WeekEnding = '2-28-2021', @WhichYear = 'This'
EXECUTE  [dbo].[LRP_ATTEND_BUDGET_VS_GROSS] @WeekEnding = '2-28-2021', @WhichYear = 'Last'


--IF @WhichYear = 'Last'
--	BEGIN	

--		SELECT	@StartDate = cur_week_start_dt,
--				@EndDate = cur_week_end_dt,
--				@FiscalYear = cur_fiscal_year
--		FROM	[dbo].[LFT_ATTENDANCE_WEEK_DATES] (@WeekEnding, 0, 'Y')

----SELECT 'debug', @StartDate, @EndDate

--		INSERT INTO @tbl_Attend
--			SELECT 'This', CAST(history_dt AS DATE), SUM(visit_count) FROM LT_HISTORY_VISIT_COUNT WHERE history_dt BETWEEN @StartDate AND @EndDate GROUP BY CAST(history_dt AS DATE)

--		SELECT	@StartDate = prv_week_start_dt,
--				@EndDate = prv_week_end_dt,
--				@FiscalYear = prv_fiscal
--		FROM	[dbo].[LFT_ATTENDANCE_WEEK_DATES] (@WeekEnding, 0, 'Y')

----SELECT 'debug', @StartDate, @EndDate

--		INSERT INTO @tbl_Attend
--			SELECT 'Last', CAST(history_dt AS DATE), SUM(visit_count) FROM LT_HISTORY_VISIT_COUNT WHERE history_dt BETWEEN @StartDate AND @EndDate GROUP BY CAST(history_dt AS DATE)

--	END

--SELECT	DISTINCT CAST(ModifiedDate AS DATE),
--		[dbo].[LFS_ATTEND_BUDGET_DATA] (DATEADD(dd, DATEDIFF(dd, 0, ModifiedDate), 0))
--FROM	[dbo].[LT_WeatherBostonItemRSS]
--WHERE	ModifiedDate BETWEEN @StartDate AND @EndDate

--INSERT INTO @tbl_Attend
--	SELECT CAST(history_dt AS DATE), SUM(visit_count) FROM LT_HISTORY_VISIT_COUNT WHERE history_dt BETWEEN @StartDate AND @EndDate GROUP BY CAST(history_dt AS DATE)

--SELECT 'debug - attend', which_year, history_dt, attendAmt FROM @tbl_Attend ORDER BY history_dt
--SELECT 'debug - budget', history_dt, budgetAmt FROM @tbl_Budget ORDER BY history_dt

