USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_TESSITURA_CASHOUT_MEMBERSHIPS]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LRP_TESSITURA_CASHOUT_MEMBERSHIPS]

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE  [dbo].[LRP_TESSITURA_CASHOUT_MEMBERSHIPS]
       @report_dt DATETIME,
       @report_operator VARCHAR(8),
	   @recon_breakdown CHAR(1)
AS BEGIN

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE	@basic_sold	INT,
            @basic_one	INT,
            @premier_sold INT,
            @premier_one INT

		DECLARE @membership_table TABLE ([membership_date] CHAR(10), [membership_operator] VARCHAR(8), [operator_first] VARCHAR(50), [operator_last] VARCHAR(50), 
                                         [operator_location] VARCHAR(50), [membership_type] VARCHAR(30), [total_memberships] INT, [total_with_one_step] INT)

		DECLARE	@tblRtn TABLE ([membership_type] VARCHAR(30), [basic_sold] INT, [basic_one]  INT, [premier_sold] INT, [premier_one]  INT)

        
		INSERT INTO @membership_table
        SELECT [membership_date],  [membership_operator], [membership_operator_first_name], [membership_operator_last_name], [membership_operator_location],
               [membership_type],  count(*), sum([one_step_count])
        FROM [dbo].[LV_RPT_CASHOUT_MEMBERSHIPS] 
        WHERE membership_date = convert(char(10),@report_dt,111) and [membership_operator] = @report_operator
        GROUP BY [membership_date], [membership_type], [membership_operator], [membership_operator_first_name], [membership_operator_last_name], [membership_operator_location]

	IF @recon_breakdown = 'B' BEGIN
		
			INSERT INTO @tblRtn
				SELECT	[membership_type], 
						SUM([total_memberships]) AS [total_memberships],
						SUM([total_with_one_step]) AS [total_with_one_step],
						0,
						0
				FROM	@membership_table 
				WHERE	PATINDEX('%basic%', [membership_type]) > 0
				OR		PATINDEX('%premier%', [membership_type]) > 0
				GROUP BY [membership_type]

	END ELSE BEGIN

			SELECT	@basic_sold = SUM([total_memberships])
			FROM	@membership_table 
			WHERE	PATINDEX('%basic%', [membership_type]) > 0

			SELECT	@basic_one= SUM([total_with_one_step])
			FROM	@membership_table 
			WHERE	PATINDEX('%basic%', [membership_type]) > 0

			SELECT	@premier_sold = SUM([total_memberships])
			FROM	@membership_table 
			WHERE	PATINDEX('%premier%', [membership_type]) > 0

			SELECT	@premier_one= SUM([total_with_one_step])
			FROM	@membership_table 
			WHERE	PATINDEX('%premier%', [membership_type]) > 0
			
            SELECT @basic_sold = IsNull(@basic_sold, 0)
            SELECT @basic_one = IsNull(@basic_one, 0)
            SELECT @premier_sold = IsNull(@premier_sold, 0)
            SELECT @premier_one = IsNull(@premier_one, 0)

            IF (@basic_sold + @basic_one + @premier_sold + @premier_one) <> 0
    			INSERT INTO @tblRtn
	    			SELECT	'Recon', @basic_sold, @basic_one, @premier_sold, @premier_one

    END

        SELECT * FROM @tblRtn

END
GO

GRANT EXECUTE ON [dbo].[LRP_TESSITURA_CASHOUT_MEMBERSHIPS] TO impusers
GO


--EXEC [dbo].[LRP_TESSITURA_CASHOUT_MEMBERSHIPS] '10-3-2016', 'kaltom00', 'B'
--EXEC [dbo].[LRP_TESSITURA_CASHOUT_MEMBERSHIPS] '10-11-2016', 'candre01', 'B'
--GO



--SELECT * FROM [dbo].[LV_RPT_CASHOUT_MEMBERSHIPS] WHERE membership_date = '2016/10/03' and [membership_operator] = 'kaltom00'
