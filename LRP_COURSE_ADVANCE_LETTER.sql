USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_COURSE_ADVANCE_LETTER]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LRP_COURSE_ADVANCE_LETTER]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LRP_COURSE_ADVANCE_LETTER]
        @report_start_dt DATETIME = NULL,
        @report_end_dt DATETIME = NULL,
        @reprint_order_no INT = 0,
        @return_by_dt DATETIME = NULL
AS BEGIN

    /*  Procedure Variables  */

        DECLARE @course_year CHAR(4) = DATENAME(YEAR,GETDATE())
        DECLARE @print_dt DATETIME = GETDATE()
        DECLARE @is_Reprint CHAR(1) = 'N'
        DECLARE @rpt_message varchar(100) = ''
        

    /*  Check Parameters  */

        SELECT @report_start_dt = ISNULL(@report_start_dt,'7-1-' + @course_year)
        SELECT @report_end_dt = ISNULL(@report_end_dt,'8-31-' + @course_year)
        SELECT @return_by_dt = ISNULL(@return_by_dt,DATEADD(MONTH,1,GETDATE()))

    /*  Procedure Table Variables  */

        DECLARE @course_orders TABLE ([order_no] INT)
        DECLARE @early_late_stay TABLE ([order_no] INT, [recipient_no] INT, [perf_no] INT, [zone_no] INT, [title_name] VARCHAR(30), [production_name] VARCHAR(30), 
                                        [production_name_long] VARCHAR(100), [recipient_name] VARCHAR(100))
        DECLARE @course_data TABLE ([order_no] INT, [sli_no] INT, [sli_status] INT, [sli_status_name] VARCHAR(30), [customer_no] INT, [customer_name] VARCHAR(150), [street1] VARCHAR(100), 
                                    [street2] VARCHAR(100), [city] VARCHAR(50), [state] VARCHAR(25), [postal_code] VARCHAR(25), [home_phone] VARCHAR(30), [business_phone] VARCHAR(30), 
                                    [email_address] VARCHAR(255), [quantity] INT, [performance_no] INT, [title_no] INT, [title_name] VARCHAR(30), [performance_dt] DATETIME,
                                    [performance_date] CHAR(10), [performance_time] VARCHAR(8), [performance_time_display] VARCHAR(25), [course_name] VARCHAR(75), [recipient_no] INT,
                                    [recipient_name] VARCHAR(150), [recipient_sort_name] VARCHAR(150), [early_or_late] VARCHAR(75), [rpt_message] VARCHAR(100))

     /*  Get all early drop off / late stay participants and temporarily store in @early_late_stay table  */

        INSERT INTO @early_late_stay
        SELECT [order_no], [participant_id], [performance_no], [performance_zone], [course_venue], [extras_title], [extras_title_long], [participant_name]
        FROM [dbo].[LV_COURSE_PARTICIPANT_EXTRAS]
        WHERE CONVERT(DATE,[course_date]) BETWEEN @report_start_dt AND @report_end_dt

    /*  Get course Order ID Numbers  */

        IF @reprint_order_no > 0 BEGIN
        
            INSERT INTO @course_orders ([order_no]) VALUES  (@reprint_order_no)
        
            SELECT @is_Reprint = 'Y'

        END ELSE BEGIN

            INSERT INTO @course_orders ([order_no])
            SELECT DISTINCT [order_no] FROM [dbo].[LV_COURSE_PARTICIPANTS] 
            WHERE CONVERT(DATE,[course_date]) BETWEEN @report_start_dt AND @report_end_dt
              AND [order_no] NOT IN (SELECT [order_no] FROM [dbo].[LT_COURSE_ADVANCE_LETTER_LOG] WHERE [course_year] = @course_year)

            SELECT @is_Reprint = 'N'

        END

    /*  Get course Registration information */

        INSERT INTO @course_data
        SELECT [order_no], 0, 0, '', [customer_no], [customer_name], [street1], [street2], [city], [state], [postal_code], '', '', '', 1, [performance_no], [performance_zone], 
               [course_venue], [course_date], [course_date], [course_time], [course_type], [course_title_long], ISNULL([participant_id],0), [participant_name], [participant_sort_name], Null, ''
        FROM [dbo].[LV_COURSE_PARTICIPANTS] 
        WHERE [order_no] IN (SELECT [order_no] FROM @course_orders)


    /*  Update phone numbers and email address  */

        SET ANSI_WARNINGS OFF

        UPDATE @course_data 
        SET [home_phone] = (SELECT ISNULL(MAX([phone]),'') FROM [dbo].[T_PHONE] WHERE [customer_no] = [@course_data].[customer_no] AND [type] = 8)      --8 = Home

        UPDATE @course_data 
        SET [business_phone] = (SELECT ISNULL(MAX([phone]),'') FROM [dbo].[T_PHONE] WHERE [customer_no] = [@course_data].[customer_no] AND [type] IN (7,14,15,16,17))
                                                                           --7 = Business Direct / 14 = Business Main / 15 = Bisuness Assistant1 / 16 = Business Assistant2 / 17 = Business Other
        UPDATE @course_data
        SET [email_address] = (SELECT ISNULL(MAX([address]),'') FROM [dbo].[T_EADDRESS] WHERE [customer_no] = [@course_data].[customer_no] AND [inactive] = 'N' AND [primary_ind] = 'Y')

        UPDATE @course_data SET [home_phone] = LEFT([home_phone],3) + '-' + SUBSTRING([home_phone],4,3) + '-' + RIGHT([home_phone],4) WHERE LEN([home_phone]) = 10

        UPDATE @course_data SET [business_phone] = LEFT([business_phone],3) + '-' + SUBSTRING([business_phone],4,3) + '-' + RIGHT([business_phone],4) WHERE LEN([business_phone]) = 10

    /*  Check for Early Drop Off or Late Stay  */

        UPDATE @course_data 
        SET [early_or_late] = (SELECT max([els].[production_name_long]) FROM @early_late_stay AS els 
                               WHERE els.[order_no] = [@course_data].[order_no] AND els.[recipient_name] = [@course_data].[recipient_name] 
                                 AND els.[production_name] = 'Early Drop-Off' AND [@course_data].[performance_time_display] IN ('1/2 Day AM','Full Day'))
        WHERE [@course_data].[early_or_late] IS NULL

        UPDATE @course_data 
        SET [early_or_late] = (SELECT max([els].[production_name_long]) FROM @early_late_stay AS els 
                               WHERE els.[order_no] = [@course_data].[order_no] AND els.[recipient_no] = [@course_data].[recipient_no] 
                                 AND els.[production_name] = 'Late Stay' AND [@course_data].[performance_time_display] IN ('1/2 Day PM', 'Full Day'))
        WHERE [@course_data].[early_or_late] IS NULL
        
        UPDATE @course_data SET [early_or_late] = '' WHERE [early_or_late] IS NULL 

        
    /*  If no participant name, mark as "Missing Participant"  */

        UPDATE @course_data SET [recipient_name] = 'UNKNOWN PARTICIPANT', [recipient_sort_name] = 'ZZZ_UNKNOWN PARTICIPANT' WHERE [recipient_name] = ''

          
    FINISHED:

        /*  If no records found, insert a single message with a message  */

            IF NOT EXISTS (SELECT * FROM @course_data) BEGIN
                IF @rpt_message = '' SELECT @rpt_message = 'No records found that meet the criteria you entered.'
                INSERT INTO @course_data
                VALUES  (0, 0, 0, '', 0, '', '', '', '','', '', '', '', '', 0, 0, 0, '', NULL, '', '', '', '', 0, '', '', '', @rpt_message)
            END ELSE BEGIN

                INSERT INTO [dbo].[LT_COURSE_ADVANCE_LETTER_LOG] ([course_year], [order_no], [print_dt], [is_reprint])
                SELECT DISTINCT  @course_year, [order_no], @print_dt, @is_Reprint FROM @course_data

            END


        /*  Pull final data set to pass back to the report format  */

            SELECT [order_no], [sli_no], [sli_status], [sli_status_name], [customer_no], [customer_name], [street1], [street2], [city], [state], [postal_code],
                   [home_phone], [business_phone], [email_address], [quantity], [performance_no], [title_no], [title_name], [performance_dt], [performance_date], 
                   [performance_time], [performance_time_display], [course_name], [recipient_no], [recipient_name], [recipient_sort_name], [early_or_late],
                   @return_by_dt, [rpt_message]
            FROM @course_data
            ORDER BY [performance_date], [performance_time], [course_name], [recipient_sort_name]

    DONE:

    SET ANSI_WARNINGS ON

END
GO

GRANT EXECUTE ON [dbo].[LRP_COURSE_ADVANCE_LETTER] TO ImpUsers
GO


--EXECUTE [dbo].[LRP_COURSE_ADVANCE_LETTER] @report_start_dt = '7-10-2017', @report_end_dt = '7-10-2017', @reprint_order_no = 0, @return_by_dt = '6-14-2017'
--DELETE FROM [dbo].[LT_COURSE_ADVANCE_LETTER_LOG] WHERE course_year = '2017'
--SELECT * FROM [dbo].[LT_COURSE_ADVANCE_LETTER_LOG]
