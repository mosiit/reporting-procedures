USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LRP_CORPORATE_MEMBERSHIP_CONTACTS]    Script Date: 8/15/2018 10:59:55 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[LRP_CORPORATE_MEMBERSHIP_CONTACTS]
(
	@group_customer_no INT
)    
AS

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
SET NOCOUNT ON 

DECLARE @contactTbl TABLE
(
	customer_no INT, 
	contactType VARCHAR(20) NOT NULL,
	individual_customer_no INT NULL,
	esal1_desc VARCHAR(55),
	esal2_desc VARCHAR(55), 
	display_name VARCHAR(100) NULL,
	JobTitle VARCHAR(100) NULL,
	Employer VARCHAR(100) NULL,
	BAddress1 VARCHAR(100) NULL,
	BAddress2 VARCHAR(100) NULL,
	BAddress3 VARCHAR(100) NULL,
	BCity VARCHAR(100) NULL,
	BStateCd VARCHAR(2) NULL,
	BZipCode VARCHAR(10) NULL,
	BPhone VARCHAR(20) NULL,
	BFax VARCHAR(20) NULL,
	BEmail VARCHAR(100) NULL,
	BNotes VARCHAR(500) NULL
)

INSERT INTO @contactTbl
(
	customer_no, 
	contactType,
	individual_customer_no,
	esal1_desc,
	esal2_desc,
	display_name,
	JobTitle,
	Employer,
	BAddress1,
	BAddress2,
	BAddress3,
	BCity,
	BStateCd,
	BZipCode,
	BPhone,
	BFax,
	BEmail,
	BNotes
)
SELECT @group_customer_no,
	CASE aff.affiliation_type_id
		WHEN 10012 THEN 'Primary Contact'
		WHEN '10019' THEN 'Working Contact'
		ELSE NULL
	END AS contactType,
	aff.individual_customer_no, 
	sal.esal1_desc,
	sal.esal2_desc,
	cust.display_name, 
	emp.JobTitle,
	emp.Employer,
	emp.BAddress1,
	emp.BAddress2,
	emp.BAddress3,
	emp.BCity,
	emp.BStateCd,
	emp.BZipCode,
	emp.BPhone,
	emp.BFax,
	emp.BEmail,
	emp.BNotes
FROM dbo.T_AFFILIATION aff
	INNER JOIN dbo.FT_CONSTITUENT_DISPLAY_NAME() AS cust 
		ON aff.individual_customer_no = cust.customer_no
	LEFT JOIN tx_cust_sal sal
		ON sal.customer_no = cust.customer_no
		AND sal.signor = 2
		-- 20180815, H. Sheridan - WO 94151: 
		--		The sub report 'Sub_Corporate_Contacts' in the 'MOS ADV: Corporate Membership Renewal Letter' report failed when calling this function because
		--		the second argument for @Handbook was not included below.  I added an empty string to the call below for that value.
	OUTER APPLY [dbo].[LFT_BOARD_GetEmployerInformation](aff.individual_customer_no, '') emp
WHERE aff.group_customer_no = @group_customer_no
	AND aff.affiliation_type_id IN (10012, 10019) -- Corp Mem Primary Contact, Corp Mem Working Contact
ORDER BY cust.sort_name

IF NOT EXISTS (SELECT 1 FROM @contactTbl)
	INSERT INTO @contactTbl	(contactType)
	VALUES ('Primary Contact'), ('Working Contact')

SELECT customer_no,
	contactType,
	individual_customer_no,
	esal1_desc,
	esal2_desc,
	display_name,
	JobTitle,
	Employer,
	BAddress1,
	BAddress2,
	BAddress3,
	BCity,
	BStateCd,
	BZipCode,
	BPhone,
	BFax,
	BEmail,
	BNotes
FROM @contactTbl
GO


