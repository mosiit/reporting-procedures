USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_GROUP_ORDERS]') AND type in (N'P', N'PC')) BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_GROUP_ORDERS] AS' 
END
GO

ALTER PROCEDURE [dbo].[LRP_GROUP_ORDERS]
        @report_start_dt DATETIME = NULL,
        @report_end_dt DATETIME = NULL,
        @order_categories VARCHAR(4000) = ''
AS BEGIN

    SET NOCOUNT ON
    SET ANSI_WARNINGS OFF

    /* Report Variables  */

        DECLARE @fiscal_year INT = 0, @fiscal_start DATETIME, @fiscal_end DATETIME 
        DECLARE @ord_no INT = 0, @counter INT = 0, @max_records INT = 0

        DECLARE @order_category_list TABLE  ([order_category] VARCHAR(50))
      
    /* Temporary Tables  */

            IF OBJECT_ID('tempdb..#performance_cache') IS NOT NULL DROP TABLE [#performance_cache]

                CREATE TABLE [#performance_cache] ([performance_no] INT, [performance_zone] INT, [performance_dt] DATETIME, [performance_date] CHAR(10), [performance_time] VARCHAR(8), 
                                                   [performance_zone_text] VARCHAR(30), [title_no] INT, [title_name] VARCHAR(30), [production_no] INT, [production_name] VARCHAR(30))
                CREATE UNIQUE CLUSTERED INDEX [ix_performance_cache_perf_and_zone] ON [#performance_cache] ([performance_no] ASC, [performance_zone] ASC) ON [PRIMARY]

            IF OBJECT_ID('tempdb..#order_detail_cache') IS NOT NULL DROP TABLE [#order_detail_cache]             

                CREATE TABLE #order_detail_cache ([customer_no] INT, [order_no] INT, [order_grade] VARCHAR(255), [perf_no] INT, [zone_no] INT, [performance_date] CHAR(10), [performance_time] VARCHAR(8), 
                                                  [title_name] VARCHAR(30), [production_name] VARCHAR(30), [price_type_no] INT, [price_type_name] VARCHAR(30), [price_type_name_short] VARCHAR(20), 
                                                  [comp_code_no] INT, [comp_code_name] VARCHAR(30), [comp_code_name_short] VARCHAR(20), [sli_status_no] INT, [sli_status_name] VARCHAR(30),
                                                  [current_price] MONEY, ticket_count INT, [due_amount] MONEY)
                CREATE CLUSTERED INDEX [ix_order_detail_cache_customer_no] ON [#order_detail_cache] ([customer_no] ASC) ON [PRIMARY]

            IF OBJECT_ID('tempdb..#order_table') IS NOT NULL DROP TABLE [#order_table]
            
                CREATE TABLE [#order_table] ([order_no] INT, [customer_no] INT, [customer_type] INT, [customer_type_name] VARCHAR(30), [order_category] INT, [order_category_name] VARCHAR(30), 
                                             [time_flag] VARCHAR(20), [create_dt] datetime, [amount_due] MONEY, [amount_paid] MONEY)
                CREATE UNIQUE CLUSTERED INDEX [ix_order_table_order_no] ON [#order_table] ([order_no] ASC) ON [PRIMARY]
                CREATE NONCLUSTERED INDEX [ix_order_table_customer_no] ON [#order_table] ([customer_no] ASC) ON [PRIMARY]
                CREATE NONCLUSTERED INDEX [ix_order_table_order_category_name] ON [#order_table] ([order_category_name] ASC) ON [PRIMARY]
                CREATE NONCLUSTERED INDEX [ix_order_table_customer_type_name] ON [#order_table] ([customer_type_name] ASC) ON [PRIMARY]
                CREATE NONCLUSTERED INDEX [ix_order_table_customer_time_flag] ON [#order_table] ([time_flag] ASC) ON [PRIMARY]

            IF OBJECT_ID('tempdb..#order_raw_data') IS NOT NULL DROP TABLE [#order_raw_data]

                CREATE TABLE [#order_raw_data] ([order_no] INT, [order_grade] VARCHAR(255), [perf_no] INT, [zone_no] INT, [perf_date] CHAR(10), [title_name] VARCHAR(30), [title_name_abbrev] VARCHAR(30), [total_tix] INT)
                CREATE CLUSTERED INDEX [ix_order_raw_data_order_no] ON [#order_raw_data] ([order_no] ASC) ON [PRIMARY]
                CREATE NONCLUSTERED INDEX [ix_order_raw_data_title_name] ON [#order_raw_data] ([title_name] ASC) ON [PRIMARY]
   
            IF OBJECT_ID('tempdb..#order_data') IS NOT NULL DROP TABLE [#order_data]

                CREATE TABLE [#order_data] ([order_no] INT, [order_grade] VARCHAR(255), [performance_date] CHAR(10), [exhibits] INT, [Omni] INT, [planetarium] INT, [4D] INT, [butterfly] INT, [lunch] INT, [other] int)
                CREATE CLUSTERED INDEX [ix_order_data_order_no] ON [#order_data] ([order_no] ASC) ON [PRIMARY]
        
            IF OBJECT_ID('tempdb..#order_payments') IS NOT NULL DROP TABLE [#order_payments]

                CREATE TABLE [#order_payments] ([order_no] INT, [paid_amount] MONEY, [scholarship_used] VARCHAR(50))
                CREATE UNIQUE CLUSTERED INDEX [ix_order_payments_order_no] ON [#order_payments] ([order_no] ASC) ON [PRIMARY]

            IF OBJECT_ID('tempdb..#customer_info') IS NOT NULL DROP TABLE [#customer_info]

                CREATE TABLE [#customer_info] ([customer_no] INT, earliest_visit CHAR(10), [latest_visit] CHAR(10), [number_dates] INT, [number_of_orders] INT, [orders_current_fy] INT, [orders_previous_fy] INT)
                CREATE UNIQUE CLUSTERED INDEX [ix_customer_info_customer_no] ON [#customer_info] ([customer_no] ASC) ON [PRIMARY]

            IF OBJECT_ID('tempdb..#final_data') IS NOT NULL DROP TABLE [#final_data]

                CREATE TABLE [#final_data] ([customer_no] INT, [customer_name] VARCHAR(150), [address_type] VARCHAR(30), [street1] VARCHAR(50), [street2] VARCHAR(50),
                                            [street3] VARCHAR(50), [city] VARCHAR(50), [state] VARCHAR(30), [postal_code] VARCHAR(20), [country] VARCHAR(50),
                                            [city_state] VARCHAR(50), [customer_type] VARCHAR(30), [earliest_visit] CHAR(10), [latest_visit] CHAR(10), [number_of_dates] INT,
                                            [number_of_orders] INT, [current_fy] INT, [orders_current_fy] INT, [orders_previous_fy] INT, [order_no] INT, [order_grade] VARCHAR(255), 
                                            [order_category_name] VARCHAR(30), [order_create_date] CHAR(10), [visit_date] CHAR(10), [advance_days] INT, [exhibits] INT, [Omni] INT, 
                                            [planetarium] INT, [4D] INT, [butterfly] INT, [lunch] INT, [other] INT, [order_amount_due] MONEY, [order_amont_paid] MONEY, 
                                            [order_total_payments] MONEY, [scholarship_used] VARCHAR(50), [report_message] VARCHAR(100),
											[price_type] VARCHAR(30), [num_seats] INT)

    /*  Check Parameters - If null is passed to either date, use start and/or end of current fiscal year  */

        IF @report_start_dt IS NULL or @report_end_dt IS NULL BEGIN

            SELECT @fiscal_year = [dbo].[LF_GetFiscalYear](GETDATE())

            SELECT @report_start_dt = ISNULL(@report_start_dt,[dbo].[LF_GetFiscalYearStartDt](@fiscal_year))

            SELECT @report_end_dt = ISNULL(@report_end_dt,[dbo].[LF_GetFiscalYearEndDt](@fiscal_year))

        END

        SELECT @fiscal_year = [dbo].[LF_GetFiscalYear](@report_start_dt)

        SELECT @order_categories = ISNULL(@order_categories,'')

        IF @order_categories <> '' BEGIN

            INSERT INTO @order_category_list ([order_category]) 
            SELECT [item] FROM [dbo].[LFT_SPLIT_DILEMETED_STRING] (@order_categories, ',')

            UPDATE @order_category_list SET [order_category] = REPLACE([order_category],'"','')

        END


    /*  Create the performance cache for the date range  */

        DELETE FROM [#performance_cache]

        INSERT INTO [#performance_cache] ([performance_no], [performance_zone], [performance_dt], [performance_date], [performance_time], [performance_zone_text], [title_no], [title_name], [production_no], [production_name])
        SELECT [performance_no], [performance_zone], [performance_dt], [performance_date], [performance_time], [performance_zone_text], [title_no], [title_name], [production_no], [production_name]
        FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] (NOLOCK)
        WHERE [performance_dt] BETWEEN @report_start_dt AND @report_end_dt
    
    /*  Get a list if all orders that contain performances in the performance cache.
        Look for products that cost money and are flagged as Seated, Paid as well as any products that are flagged as Ticketed, Paid.  */

        DELETE FROM [#order_table]

        INSERT INTO [#order_table] ([order_no], [customer_no], [customer_type], [customer_type_name], [order_category], [order_category_name], [time_flag], [create_dt], [amount_due], [amount_paid])
        SELECT DISTINCT sli.[order_no], ord.[customer_no], cus.[cust_type], typ.[description], ord.[class], cat.[description], 'past', ord.[create_dt], ord.[tot_due_amt], ord.[tot_paid_amt]
        FROM [dbo].[T_SUB_LINEITEM] AS sli (NOLOCK) 
             INNER JOIN [dbo].[T_ORDER] AS ord (NOLOCK) ON ord.[order_no] = sli.[order_no]
             INNER JOIN [dbo].[T_CUSTOMER] AS cus (NOLOCK) ON cus.[customer_no] = ord.[customer_no]
             INNER JOIN [dbo].[TR_CUST_TYPE] AS typ (NOLOCK) ON typ.[id] = cus.[cust_type]
             INNER JOIN [dbo].[TR_ORDER_CATEGORY] AS cat (NOLOCK) ON cat.[id] = ord.[class]
        WHERE sli.[perf_no] IN (SELECT [performance_no] FROM [#performance_cache]) AND ((sli.[paid_amt] > 0.00 AND sli.[sli_status] = 3) OR sli.[sli_status] = 12)

    /*  If the end date of the report is later than the current date, future orders have to be considered.
        Look for products on future dates that are seated (unpaid or paid) from orders not already in the [#order_table] temporary table.  */

        IF @report_end_dt > GETDATE()
            INSERT INTO [#order_table] ([order_no], [customer_no], [customer_type], [customer_type_name], [order_category], [order_category_name], [time_flag], [create_dt], [amount_due], [amount_paid])
            SELECT DISTINCT sli.[order_no], ord.[customer_no], cus.[cust_type], typ.[description], ord.[class], cat.[description], 'Future', ord.[create_dt], ord.[tot_due_amt], ord.[tot_paid_amt]
            FROM [dbo].[T_SUB_LINEITEM] AS sli (NOLOCK) 
                 INNER JOIN [dbo].[T_ORDER] AS ord (NOLOCK) ON ord.[order_no] = sli.[order_no]
                 INNER JOIN [dbo].[T_CUSTOMER] AS cus (NOLOCK) ON cus.[customer_no] = ord.[customer_no]
                 INNER JOIN [dbo].[TR_CUST_TYPE] AS typ (NOLOCK) ON typ.[id] = cus.[cust_type]
                 INNER JOIN [dbo].[TR_ORDER_CATEGORY] AS cat (NOLOCK) ON cat.[id] = ord.[class]
            WHERE sli.[perf_no] IN (SELECT [performance_no] FROM [#performance_cache] WHERE performance_date >= CONVERT(CHAR(10),GETDATE(),111)) AND sli.[sli_status] in (2,3)
                  AND [sli].[order_no] NOT IN (SELECT [order_no] FROM [#order_table])

    /*  Double-check to remove past orders where there is an amount due on the order but no payments made to the order (No Shows)  */

        DELETE FROM [#order_table] WHERE [order_no] IN (SELECT ord.[order_no]
                                                        FROM [#order_table] AS ord (NOLOCK)
                                                              LEFT OUTER JOIN [dbo].[T_TRANSACTION] AS trx (NOLOCK) ON trx.[order_no] = ord.[order_no]
                                                              LEFT OUTER JOIN [dbo].[T_PAYMENT] AS pay (NOLOCK) ON pay.[transaction_no] = trx.[transaction_no] AND pay.[sequence_no] = trx.[sequence_no]
                                                        WHERE ord.[order_no] IN (SELECT order_no FROM [#order_table] WHERE [time_flag] = 'Past')
                                                        GROUP BY ord.[order_no], ord.order_category_name
                                                        HAVING ISNULL(SUM(ord.[amount_due]),0.00) > 0.00 AND ISNULL(SUM(pay.[pmt_amt]),0.00) <= 0.00)

    /*  Keep only groups, schools, and tour operators  */

        DELETE FROM [#order_table] WHERE [order_category_name] NOT IN ('Group Order','School','Tour Operator')
    
   /*  If a specific category or categories were asked for, remove the others  */      

        IF EXISTS (SELECT * FROM @order_category_list)
            DELETE FROM [#order_table] WHERE [order_category_name] NOT IN (SELECT [order_category] FROM @order_category_list)

    /*  Create the order detail cache for easier reference later on  */

        INSERT INTO [#order_detail_cache] ([customer_no], [order_no], [order_grade], [perf_no], [zone_no], [performance_date], [performance_time], [title_name], [production_name],
                                           [price_type_no], [price_type_name], [price_type_name_short], [comp_code_no], [comp_code_name], [comp_code_name_short], 
                                           [sli_status_no], [sli_status_name], [current_price], [ticket_count], [due_amount])
        SELECT det.[customer_no], det.[order_no], ISNULL(ord.[custom_5],''), det.[perf_no], det.[zone_no], det.[performance_date], det.[performance_time], det.[title_name], det.[production_name],
               det.[price_type_no], det.[price_type_name], det.[price_type_name_short], det.[comp_code_no], det.[comp_code_name], det.[comp_code_name_short],  det.[sli_status_no], det.[sli_status_name], 
               det.[current_price], COUNT(det.[sli_no]), SUM(det.[due_amount])
        FROM [dbo].[LV_ORDER_DETAIL] AS det (NOLOCK) 
             LEFT OUTER JOIN [dbo].[T_ORDER] AS ord (NOLOCK) ON ord.[order_no] = det.[order_no]
        WHERE det.[customer_no] IN (SELECT [customer_no] FROM [#order_table]) --AND sli_status_no IN (3,12)
        GROUP BY det.[customer_no], det.[order_no], ISNULL(ord.[custom_5],''), det.[perf_no], det.[zone_no], det.[performance_date], det.[performance_time], det.[title_name], det.[production_name], 
                 det.[price_type_no], det.[price_type_name], det.[price_type_name_short], det.[comp_code_no], det.[comp_code_name], det.[comp_code_name_short], det.[sli_status_no], 
                 det.[sli_status_name], det.[current_price]

    /*  Delete Past products that were never paid for  */

        DELETE FROM [#order_detail_cache] WHERE CONVERT(CHAR(10),[performance_date],111) < CONVERT(CHAR(10),GETDATE(),111) AND sli_status_no NOT IN (3,12)

    /*  Delete Future products that are not seated or paid for (removes voids and returns)  */
    
        DELETE FROM [#order_detail_cache] WHERE CONVERT(CHAR(10),[performance_date],111) >= CONVERT(CHAR(10),GETDATE(),111) AND sli_status_no NOT IN (2,3,12)        
     
    /*  Exclude Membership Sales and Food Vouchers 
        NOTE: Membership Sales and Food Vouchers have been excluded becuase it appears that when selling food vouchers, the product for the day of sale is used instead
              of the product for day of visit.  This is throwing off all the visit dates, showing orders with multiple visit dates that really do not have multiple dates.  */
              
              --SELECT * FROM [#order_detail_cache] WHERE order_no = 528994

        DELETE FROM [#order_detail_cache] WHERE title_name = 'Membership'

        DELETE FROM [#order_detail_cache] WHERE price_type_name = 'Food Voucher'

    /*  Create ticket breakdown for each order/performance date combination  */   

        DELETE FROM [#order_raw_data]

        INSERT INTO [#order_raw_data] ([order_no], [order_grade], sli.[perf_no], sli.[zone_no], prf.[perf_date], prf.[title_name], prf.[total_tix])
        SELECT [order_no], [order_grade], [perf_no], [zone_no], [performance_date], [title_name], COUNT(*)
        FROM [#order_detail_cache]
        WHERE [order_no] IN (SELECT [order_no] FROM [#order_table]) AND sli_status_no IN (2,3,12)
        GROUP BY [order_no], [order_grade], [perf_no], [zone_no], [performance_date], [title_name]

        UPDATE [#order_raw_data] 
        SET title_name_abbrev = CASE WHEN [title_name] = 'Exhibit Halls' THEN 'Exhibits'          WHEN [title_name] = 'Mugar Omni Theater' THEN 'Omni'
                                     WHEN [title_name] = 'Hayden Planetarium' THEN 'Planetarium'  WHEN [title_name] = '4-D Theater' THEN '4D'
                                     WHEN [title_name] = 'Butterfly Garden' THEN 'Butterfly'      WHEN [title_name] = 'Special Exhibit' THEN 'Special'
                                     WHEN [title_name] = 'School Lunch' THEN 'Lunch'              ELSE 'Other' END

        DELETE FROM [#order_data]

        INSERT INTO [#order_data] ([order_no], [order_grade], [performance_date], [exhibits], [Omni], [planetarium], [4D], [butterfly], [lunch], [other])
        SELECT * FROM (SELECT [order_no], [order_grade], [perf_date], [title_name_abbrev], [total_tix] FROM [#order_raw_data]) src 
        PIVOT (SUM(total_tix) FOR [title_name_abbrev] IN ([exhibits],[Omni],[Planetarium],[4D],[Butterfly],[Special],[Other])) AS [max_tix]
        
    /*  Retrieve Order Payment Information */
        
        INSERT INTO [#order_payments]
        SELECT trx.[order_no], SUM(pay.pmt_amt), ''
        FROM dbo.T_TRANSACTION AS trx (NOLOCK)
             INNER JOIN dbo.T_PAYMENT AS pay (NOLOCK) ON pay.[transaction_no] = trx.[transaction_no] AND pay.sequence_no = trx.[sequence_no]
        WHERE trx.order_no IN (SELECT order_no FROM [#order_detail_cache])
        GROUP BY trx.[order_no] ORDER BY [trx].[order_no]
        
        UPDATE [#order_payments] 
        SET [scholarship_used] = (SELECT ISNULL(MAX([scholarship_name]),'') 
                                  FROM [dbo].[LV_SCHOLARSHIP_PAYMENTS] AS sch (NOLOCK) 
                                  WHERE sch.[order_no] = [#order_payments].[order_no] AND sch.payment_method_no not in (32,63)) 
                                                                                          --32 = Scholarship On Account/63 = Interdepartmental Transfer   

        UPDATE [#order_payments] SET [scholarship_used] = 'Unknown' WHERE scholarship_used LIKE 'Invalid Scholarship%'

    /*  Retrieve customer stats from order detail cache  */

        
        INSERT INTO [#customer_info] ([customer_no], [earliest_visit], [latest_visit], [number_dates], [number_of_orders], [orders_current_fy], [orders_previous_fy])
        SELECT odc.[customer_no], Min(odc.[performance_date]), MAX(odc.[performance_date]), COUNT(DISTINCT odc.[performance_date]), COUNT(DISTINCT odc.[order_no]), 0, 0
        FROM [#order_detail_cache] odc (NOLOCK)
        GROUP BY odc.[customer_no]

        SELECT @fiscal_start = [dbo].[LF_GetFiscalYearStartDt](@fiscal_year)
        SELECT @fiscal_end = [dbo].[LF_GetFiscalYearEndDt](@fiscal_year)

        UPDATE [#customer_info] SET [orders_current_fy] = (SELECT COUNT(DISTINCT [order_no]) FROM [#order_detail_cache] AS cache
                                                           WHERE performance_date BETWEEN @fiscal_start AND @fiscal_end
                                                             AND cache.[customer_no] = [#customer_info].[customer_no])

        SELECT @fiscal_start = [dbo].[LF_GetFiscalYearStartDt](@fiscal_year - 1)
        SELECT @fiscal_end = [dbo].[LF_GetFiscalYearEndDt](@fiscal_year - 1)

        UPDATE [#customer_info] SET [orders_previous_fy] = (SELECT COUNT(DISTINCT [order_no]) FROM [#order_detail_cache] AS cache
                                                            WHERE performance_date BETWEEN @fiscal_start AND @fiscal_end
                                                              AND cache.[customer_no] = [#customer_info].[customer_no])
                                                                   
    /*  Produce Final Data Table  */

        DELETE FROM [#final_data]
       
        INSERT INTO [#final_data]
        SELECT ord.[customer_no]
             , LTRIM(RTRIM(ISNULL(cus.[fname],'') + CASE WHEN ISNULL(cus.[mname],'') = '' THEN '' ELSE cus.[mname] + ' ' END + ISNULL(cus.[lname],''))) AS 'customer_name'
             , ISNULL(aty.[description],'') AS 'address_type_name'
             , ISNULL(adr.[street1],'') AS 'street1'
             , ISNULL(adr.[street2],'') AS 'street2'
             , ISNULL(adr.[street3],'') AS 'street3'
             , ISNULL(adr.[city],'') AS 'city'
             , ISNULL(adr.[state],'') AS 'state'
             , ISNULL(adr.[postal_code],'') AS 'zip_code'
             , ISNULL(cou.[description],'') AS 'country'
             , ISNULL(adr.[city],'') + ', ' + ISNULL(adr.[state],'') AS 'city_state'
             , ord.[customer_type_name] AS 'customer_type'
             , cin.[earliest_visit]
             , cin.[latest_visit]
             , cin.[number_dates]
             , cin.[number_of_orders]
             , @fiscal_year
             , cin.[orders_current_fy]
             , cin.[orders_previous_fy]
             , ord.[order_no] AS 'order_no'
             , dat.[order_grade]
             , ord.[order_category_name] AS 'order_category'
             , CONVERT(CHAR(10),ord.[create_dt],111) AS 'order_create_date'
             , ISNULL(dat.[performance_date],'') AS 'visit_date'
             , DATEDIFF(DAY,CONVERT(DATE,ord.[create_dt]),CONVERT(DATE,dat.[performance_date]))
             , ISNULL(dat.[exhibits],0) AS 'exhibits'
             , ISNULL(dat.[Omni],0) AS 'omni'
             , ISNULL(dat.[planetarium],0) AS 'planetarium'
             , ISNULL(dat.[4D],0) AS '4D'
             , ISNULL(dat.[butterfly],0) AS 'Butterfly'
             , ISNULL(dat.[lunch],0) AS 'lunch'
             , ISNULL(dat.[other],0) AS 'other'
             , ord.[amount_due] AS 'order_amount_due'
             , ord.[amount_paid] AS 'order_amont_paid'
             , ISNULL(pay.[paid_amount],0.0) AS 'order_total_payments'
             , ISNULL(pay.[scholarship_used],'') AS 'scholarship_used'
             , ''
			 , ''
			 , 0
        FROM [#order_table] AS ord
             INNER JOIN [#order_data] AS dat ON dat.[order_no] = ord.[order_no]
             LEFT OUTER JOIN [dbo].[T_CUSTOMER] AS cus (NOLOCK) ON cus.[customer_no] = ord.[customer_no]
             LEFT OUTER JOIN [dbo].[T_ADDRESS] AS adr (NOLOCK) ON adr.[customer_no] = ord.[customer_no] AND adr.[primary_ind] = 'Y'
             LEFT OUTER JOIN [dbo].[TR_ADDRESS_TYPE] AS aty (NOLOCK) ON aty.[id] = adr.[address_type]
             LEFT OUTER JOIN [dbo].[TR_COUNTRY] AS cou (NOLOCK) ON cou.[id] = adr.[country]
             LEFT OUTER JOIN [#order_payments] AS pay (NOLOCK) ON pay.[order_no] = ord.[order_no]
             LEFT OUTER JOIN [#customer_info] AS cin (NOLOCK) ON cin.[customer_no] = ord.[customer_no]
        ORDER BY [order_category], [order_no]
        
		UPDATE	x
		SET		x.[price_type] = ISNULL(e.[description],''),
				x.[num_seats] = ISNULL(k.num_seats,'')
		FROM	[dbo].[T_TICKET_HISTORY] k
		LEFT OUTER JOIN [dbo].[TR_PRICE_TYPE] e ON k.price_type = e.id
		LEFT OUTER JOIN [#final_data] x ON x.order_no = k.order_no 

    FINISHED:

        /*  If no records found, add a single record with a "No Data Found" message  */
        
            IF NOT EXISTS (SELECT * FROM [#final_data])
                INSERT INTO [#final_data]
                VALUES (0, '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, 0, '', '', '', '', 
                        0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, '', 'No data found for the criteria entered.','',0)

        /*  Return final data set to the report  */    

            SELECT [customer_no], 
                   [customer_name], 
                   [city_state], 
                   [customer_type], 
                   [earliest_visit], 
                   [latest_visit], 
                   [number_of_dates], 
                   [number_of_orders], 
                   [current_fy], 
                   [orders_current_fy], 
                   [orders_previous_fy],
                   [order_no], 
                   [order_grade], 
                   [order_category_name], 
                   [order_create_date], 
                   [visit_date], 
                   [advance_days], 
                   [exhibits], 
                   [Omni], 
                   [planetarium], 
                   [4D], 
                   [butterfly], 
                   [lunch], 
                   [other], 
                   [order_amount_due], 
                   [order_total_payments], 
                   [scholarship_used], 
                   [price_type], 
                   [num_seats]
            FROM [#final_data] 
            ORDER BY [customer_name], [customer_no]
        
    DONE:

        /*  Destroy all the temp tables  */

            IF OBJECT_ID('tempdb..#final_data') IS NOT NULL DROP TABLE [#final_data]
            IF OBJECT_ID('tempdb..#performance_cache') IS NOT NULL DROP TABLE [#performance_cache]
            IF OBJECT_ID('tempdb..#order_detail_cache') IS NOT NULL DROP TABLE [#order_detail_cache]             
            IF OBJECT_ID('tempdb..#order_table') IS NOT NULL DROP TABLE [#order_table]
            IF OBJECT_ID('tempdb..#order_raw_data') IS NOT NULL DROP TABLE [#order_raw_data]
            IF OBJECT_ID('tempdb..#order_data') IS NOT NULL DROP TABLE [#order_data]
            IF OBJECT_ID('tempdb..#order_payments') IS NOT NULL DROP TABLE [#order_payments]
            IF OBJECT_ID('tempdb..#customer_info') IS NOT NULL DROP TABLE [#customer_info]

END
GO

GRANT EXECUTE ON [dbo].[LRP_GROUP_ORDERS] TO ImpUsers
GO            


--EXECUTE [dbo].[LRP_GROUP_ORDERS] '4-1-2019', '4-30-2019', '"Group Order","School","Tour Operator"'

