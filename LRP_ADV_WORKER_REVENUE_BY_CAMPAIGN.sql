USE [impresario]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LRP_ADV_WORKER_REVENUE_BY_CAMPAIGN]
(
	@department_str VARCHAR(MAX) = NULL, 
	@worker_str VARCHAR(MAX) = NULL,
	@cont_start_dt DATETIME,
	@cont_end_dt DATETIME  
)
AS

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

-- TABLE TO HOLD DEPARTMENT PARAMETERS
DECLARE	@deptTbl TABLE (deptID INT NOT NULL)

IF ISNULL(@department_str,'') <> ''
	INSERT INTO @deptTbl (deptID)
	SELECT Element From dbo.FT_SPLIT_LIST(@department_str,',')
ELSE
	INSERT INTO @deptTbl (deptID)
	SELECT id 
	FROM LTR_ATTDET_DESCR2
	WHERE inactive = 'N' -- department

--SELECT * FROM @deptTbl

-- TABLE TO HOLD WORKER PARAMETERS
DECLARE	@workerTbl TABLE (workerID INT NOT NULL)

IF ISNULL(@worker_str,'') <> ''
	INSERT INTO @workerTbl (workerID)
	SELECT Element From dbo.FT_SPLIT_LIST(@worker_str,',')
ELSE
	INSERT INTO @workerTbl (workerID)
	SELECT customer_no 
	FROM LT_ATTRIBUTE_DETAIL 
	WHERE type_no = 15 AND descr2_no <> 1;

--SELECT * FROM @workerTbl;

WITH worker 
AS
(
	SELECT a.customer_no, d2.description AS department, c.fname AS worker
	FROM LT_ATTRIBUTE_DETAIL AS a
	LEFT OUTER JOIN LTR_ATTDET_DESCR2 AS d2
		ON a.descr2_no = d2.id
	INNER JOIN T_CUSTOMER AS c
		ON a.customer_no = c.customer_no
	INNER JOIN @deptTbl dept
		ON dept.deptID = d2.id
	INNER JOIN @workerTbl worker
		ON worker.workerID = c.customer_no
	WHERE a.type_no = 15 AND a.descr2_no <> 1 
)
SELECT bus.Business_Unit, w.worker, w.department, cat.description AS campaignCat, cont.ref_no, cont.cont_dt, cont.cont_amt
FROM 
	dbo.T_CONTRIBUTION cont
INNER JOIN dbo.T_CAMPAIGN cam
	ON cam.campaign_no = cont.campaign_no
INNER JOIN dbo.TR_CAMPAIGN_CATEGORY cat
	ON cam.category = cat.id
INNER JOIN dbo.LTR_CAMPAIGN_BUSINESS_UNIT bus
	ON bus.campaign_no = cam.campaign_no
INNER JOIN worker w
	ON cont.worker_customer_no = w.customer_no
WHERE cont.cont_dt BETWEEN @cont_start_dt AND @cont_end_dt

