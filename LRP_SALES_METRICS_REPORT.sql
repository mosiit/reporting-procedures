USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_SALES_METRICS_REPORT]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LRP_SALES_METRICS_REPORT]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LRP_SALES_METRICS_REPORT]
        @report_start_dt DATETIME = Null,
        @report_end_dt DATETIME = NULL,
        @report_users VARCHAR(4000) = NULL,
        @user_locations varchar(4000) = NULL,
        @include_operator_detail CHAR(1) = NULL,
        @include_daily_detail CHAR(1) = NULL,
        @exclusion_accounts varchar(4000) = Null
AS BEGIN

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    SET ANSI_WARNINGS OFF
    SET NOCOUNT ON

 /*  Report Variables  */
            
        DECLARE @divide_total VARCHAR(20) = 'touched'
        DECLARE @user_list TABLE ([userid] VARCHAR(50))
        DECLARE @location_list TABLE ([user_location] VARCHAR(30))
        DECLARE @exclusions_list TABLE ([userid] VARCHAR(50))
        DECLARE @user_count INT = 0

    /* Check Parameters  */
            
        SELECT @report_start_dt = ISNULL(@report_start_dt,DATEADD(DAY,-1,GETDATE())),
               @report_end_dt = ISNULL(@report_end_dt,DATEADD(DAY,-1,GETDATE()))

        SELECT @include_operator_detail = ISNULL(@include_operator_detail,'Y'),
               @include_daily_detail = ISNULL(@include_daily_detail,'N')

        SELECT @report_start_dt = CONVERT(DATE,@report_start_dt)
        SELECT @report_end_dt = CONVERT(DATE,@report_end_dt)

        SELECT @report_users = ISNULL(@report_users,'')
        IF @report_users = 'All' OR @report_users = 'All Users' SELECT @report_users = ''
        SELECT @report_users = REPLACE(@report_users,'"','')

        SELECT @user_locations = ISNULL(@user_locations,'')
        IF @user_locations = 'All' OR @user_locations = 'All Locations' SELECT @user_locations = ''
        SELECT @user_locations = REPLACE(@user_locations,'"','')

        SELECT @include_operator_detail = ISNULL(@include_operator_detail,'Y')
        IF @include_operator_detail <> 'N' SELECT @include_operator_detail = 'Y'

        SELECT @exclusion_accounts = ISNULL(@exclusion_accounts,'')
        SELECT @exclusion_accounts = REPLACE(@exclusion_accounts,'"','')

    /*  Parse Out Users and Locations */

        IF @report_users <> ''
            INSERT INTO @user_list SELECT LTRIM(RTRIM(Item)) FROM [dbo].[LFT_SPLIT_DILEMETED_STRING] (@report_users,',')

        IF @user_locations <> ''
            INSERT INTO @location_list SELECT LTRIM(RTRIM(Item)) FROM [dbo].[LFT_SPLIT_DILEMETED_STRING] (@user_locations,',')

        IF @exclusion_accounts <> ''
            INSERT INTO @exclusions_list SELECT LTRIM(RTRIM(Item)) FROM [dbo].[LFT_SPLIT_DILEMETED_STRING] (@exclusion_accounts,',')
            
        SELECT @user_count = COUNT(*) FROM @user_list
        SELECT @user_count = ISNULL(@user_count,0)

    /*  Temporary Table */
    
            IF OBJECT_ID('tempdb..#trx_percentages') IS NOT NULL DROP TABLE [#trx_percentages]

            CREATE TABLE [#trx_percentages] ([sale_dt] DATETIME, 
                                             [sale_date] CHAR(10) DEFAULT '', 
                                             [user_id] VARCHAR(10) NOT NULL,
                                             [user_name] VARCHAR(100) DEFAULT '', 
                                             [user_name_sort] VARCHAR(100) DEFAULT '', 
                                             [user_location] VARCHAR(30) DEFAULT '', 
                                             [orders_created] DECIMAL(18,4) DEFAULT 0, 
                                             [orders_touched] DECIMAL(18,4) DEFAULT 0, 
                                             [membership_sales] DECIMAL(18,4) DEFAULT 0,
                                             [memb_conversion_eligible] DECIMAL(18,2) DEFAULT 0,
                                             [membership_percent] DECIMAL(18,4) DEFAULT 0, 
                                             [total_one_step_recovery] DECIMAL(18,4) DEFAULT 0, 
                                             [total_gift_memberships] DECIMAL(18,4) DEFAULT 0,
                                             [total_one_step_eligible] DECIMAL(18,4) DEFAULT 0, 
                                             [basic_2_sales] DECIMAL(18,4) DEFAULT 0, 
                                             [basic_2_percent] DECIMAL(18,4) DEFAULT 0, 
                                             [basic_5_sales] DECIMAL(18,4) DEFAULT 0, 
                                             [basic_5_percent] DECIMAL(18,4) DEFAULT 0, 
                                             [basic_8_sales] DECIMAL(18,4) DEFAULT 0, 
                                             [basic_8_percent] DECIMAL(18,4) DEFAULT 0, 
                                             [basic_total_sales] DECIMAL(18,4) DEFAULT 0, 
                                             [basic_total_percent] DECIMAL(18,4) DEFAULT 0, 
                                             [basic_one_step_recovery] DECIMAL(18,2) DEFAULT 0, 
                                             [basic_gift_memberships] DECIMAL(18,4) DEFAULT 0,
                                             [basic_one_step_eligible] DECIMAL(18,4) DEFAULT 0, 
                                             [one_step_basic] DECIMAL(18,4) DEFAULT 0, 
                                             [one_step_basic_percent] DECIMAL(18,4) DEFAULT 0, 
                                             [premier_2_sales] DECIMAL(18,4) DEFAULT 0, 
                                             [premire_2_percent] DECIMAL(18,4) DEFAULT 0, 
                                             [premier_5_sales] DECIMAL(18,4) DEFAULT 0, 
                                             [premier_5_percent] DECIMAL(18,4) DEFAULT 0,
                                             [premier_8_sales] DECIMAL(18,4) DEFAULT 0, 
                                             [premier_8_percent] DECIMAL(18,4) DEFAULT 0, 
                                             [premier_total_sales] DECIMAL(18,4) DEFAULT 0,
                                             [premier_total_percent] DECIMAL(18,4) DEFAULT 0, 
                                             [premier_one_step_recovery] DECIMAL(18,4) DEFAULT 0, 
                                             [premier_gift_memberships] DECIMAL(18,2) DEFAULT 0,
                                             [premier_one_step_eligible] DECIMAL(18,4) DEFAULT 0, 
                                             [one_step_premier] DECIMAL(18,4) DEFAULT 0, 
                                             [one_step_premier_percent] DECIMAL(18,4) DEFAULT 0,
                                             [one_step_total] DECIMAL(18, 4) DEFAULT 0, 
                                             [one_step_total_percent] DECIMAL(18,4) DEFAULT 0, 
                                             [orders_counted] DECIMAL(18,4) DEFAULT 0,
                                             [upsell_eligible] DECIMAL(18,2) DEFAULT 0,
                                             [multi_performances_orders] DECIMAL(18, 4) DEFAULT 0, 
                                             [multi_performance_percent] DECIMAL(18,4) DEFAULT 0, 
                                             [performances_sold] DECIMAL(18,4) DEFAULT 0,
                                             [average_perfs_per_order] DECIMAL(18,4) DEFAULT 0, 
                                             [user_count] INT DEFAULT 0)

            CREATE UNIQUE INDEX uq_sale_date_user_id_user_name ON [#trx_percentages] ([sale_date] ASC, [user_id] ASC, [user_name] ASC, [user_location] ASC) ON [PRIMARY];

            IF @include_daily_detail = 'Y'

                INSERT INTO [#trx_percentages] ([sale_dt], [sale_date], [user_id], [user_name], [user_name_sort], [user_location], [orders_created], [orders_touched], [membership_sales],
                                                [memb_conversion_eligible], [total_one_step_recovery], [total_gift_memberships], [total_one_step_eligible], [basic_2_sales], [basic_5_sales], 
                                                [basic_8_sales], [basic_total_sales], [basic_one_step_recovery], [basic_gift_memberships], [basic_one_step_eligible], [one_step_basic], 
                                                [premier_2_sales], [premier_5_sales], [premier_8_sales], [premier_total_sales], [premier_one_step_recovery], [premier_gift_memberships], 
                                                [premier_one_step_eligible], [one_step_premier], [one_step_total], [upsell_eligible], [multi_performances_orders], [performances_sold],
                                                [user_count])
                SELECT his.[history_dt], 
                       his.[history_date], 
                       his.[operator], 
                       ISNULL(his.[operator_name],''),
                       ISNULL(his.[operator_name_sort],''),
                       ISNULL(his.[operator_location],''), 
                       his.[orders_created], 
                       his.[orders_touched], 
                       his.[membership_sales],
                       his.[memb_conversion_eligible],
                       his.[total_one_step_recovery],
                       his.[total_gift_memberships],
                       his.[one_step_eligible],
                       [basic_2_sales], 
                       [basic_5_sales], 
                       [basic_8_sales], 
                       [basic_total_sales], 
                       [basic_one_step_recovery], 
                       [basic_gift_memberships],
                       [one_step_eligible_basic],
                       [one_step_basic], 
                       [premier_2_sales], 
                       [premier_5_sales],
                       [premier_8_sales], 
                       [premier_total_sales],
                       [premier_one_step_recovery],
                       [premier_gift_memberships],
                       [one_step_eligible_premier],
                       [one_step_premier],
                       [one_step_total],
                       [upsell_eligible], 
                       [upsell_orders],
                       [total_perfs_sold], 
                       @user_count
                FROM [dbo].[LT_HISTORY_SALES_METRICS] his
                WHERE [history_dt] BETWEEN @report_start_dt AND @report_end_dt
                  AND (@exclusion_accounts = '' OR [operator] NOT IN (SELECT [userid] FROM @exclusions_list))
            ELSE
                INSERT INTO [#trx_percentages] ([sale_dt], [sale_date], [user_id], [user_name], [user_name_sort], [user_location], [orders_created], [orders_touched], [membership_sales],
                                                [memb_conversion_eligible], [total_one_step_recovery], [total_gift_memberships], [total_one_step_eligible], [basic_2_sales], [basic_5_sales], 
                                                [basic_8_sales], [basic_total_sales], [basic_one_step_recovery], [basic_gift_memberships], [basic_one_step_eligible], [one_step_basic], 
                                                [premier_2_sales], [premier_5_sales], [premier_8_sales], [premier_total_sales], [premier_one_step_recovery], [premier_gift_memberships], 
                                                [premier_one_step_eligible], [one_step_premier], [one_step_total], [upsell_eligible], [multi_performances_orders], [performances_sold],
                                                [user_count])
                SELECT NULL,
                       '', 
                       his.[operator], 
                       ISNULL(his.[operator_name],''),
                       ISNULL(his.[operator_name_sort],''),
                       ISNULL(his.[operator_location],''),
                       SUM(his.[orders_created]), 
                       SUM(his.[orders_touched]), 
                       SUM(his.[membership_sales]),
                       SUM(his.[memb_conversion_eligible]),
                       SUM(his.[total_one_step_recovery]), 
                       SUM(his.[total_gift_memberships]),
                       SUM(his.[one_step_eligible]),
                       SUM(his.[basic_2_sales]), 
                       SUM(his.[basic_5_sales]),
                       SUM(his.[basic_8_sales]), 
                       SUM(his.[basic_total_sales]), 
                       SUM(his.[basic_one_step_recovery]), 
                       SUM(his.[basic_gift_memberships]),
                       SUM(his.[one_step_eligible_basic]),
                       SUM(his.[one_step_basic]), 
                       SUM(his.[premier_2_sales]), 
                       SUM(his.[premier_5_sales]),
                       SUM(his.[premier_8_sales]), 
                       SUM(his.[premier_total_sales]),
                       SUM(his.[premier_one_step_recovery]), 
                       SUM(his.[premier_gift_memberships]),
                       SUM(his.[one_step_eligible_premier]),
                       SUM([one_step_premier]),
                       SUM(his.[one_step_total]), 
                       SUM(his.[upsell_eligible]), 
                       SUM(his.[upsell_orders]), 
                       SUM(his.[total_perfs_sold]),
                       @user_count
                FROM [dbo].[LT_HISTORY_SALES_METRICS] AS his
                WHERE his.[history_dt] BETWEEN @report_start_dt AND @report_end_dt
                  AND (@exclusion_accounts = '' OR his.[operator] NOT IN (SELECT [userid] FROM @exclusions_list))
                GROUP BY his.[operator],  ISNULL(his.[operator_name],''), ISNULL(his.[operator_name_sort],''), ISNULL(his.[operator_location],'')



    /*  Filter out unwanted records based on the parameters
          Done with a delete rather than in the statement above because it needs to be one or the other
          If users and locations are passed to the report, it bases data on users and locations are ignored.  */

        IF EXISTS (SELECT * FROM @user_list)
            DELETE FROM [#trx_percentages] WHERE [user_id] NOT IN (SELECT [userid] FROM @user_list)
        ELSE IF EXISTS (SELECT * FROM @location_list)
            DELETE FROM [#trx_percentages] WHERE [user_location] NOT IN (SELECT [user_location] FROM @location_list)
 
    /*  Insert Operator totals (if daily detail is being displayed)  */
    
        IF @include_daily_detail = 'Y' AND @include_operator_detail = 'Y'

            INSERT INTO [#trx_percentages] ([sale_dt], [sale_date], [user_id], [user_name], [user_name_sort], [user_location], [orders_created], [orders_touched], [membership_sales],
                                            [memb_conversion_eligible], [total_one_step_recovery], [total_gift_memberships], [total_one_step_eligible], [basic_2_sales], [basic_5_sales], 
                                            [basic_8_sales], [basic_total_sales], [basic_one_step_recovery], [basic_gift_memberships], [basic_one_step_eligible], [one_step_basic], 
                                            [premier_2_sales], [premier_5_sales], [premier_8_sales], [premier_total_sales], [premier_one_step_recovery], [premier_gift_memberships], 
                                            [premier_one_step_eligible], [one_step_premier], [one_step_total], [upsell_eligible], [multi_performances_orders], [performances_sold],
                                            [user_count])
            SELECT NULL, 
                   '', 
                   '', 
                   [user_name] + ' Total', 
                   [user_name_sort] + '_ZZA', 
                   his.[user_location], 
                   SUM(his.[orders_created]), 
                   SUM(his.[orders_touched]), 
                   SUM(his.[membership_sales]),
                   SUM(his.[memb_conversion_eligible]),
                   SUM(his.[total_one_step_recovery]), 
                   SUM(his.[total_gift_memberships]),
                   SUM(his.[total_one_step_eligible]),
                   SUM(his.[basic_2_sales]), 
                   SUM(his.[basic_5_sales]),
                   SUM(his.[basic_8_sales]), 
                   SUM(his.[basic_total_sales]), 
                   SUM(his.[basic_one_step_recovery]), 
                   SUM(his.[basic_gift_memberships]),
                   SUM(his.[basic_one_step_eligible]),
                   SUM(his.[one_step_basic]), 
                   SUM(his.[premier_2_sales]), 
                   SUM(his.[premier_5_sales]),
                   SUM(his.[premier_8_sales]), 
                   SUM(his.[premier_total_sales]),
                   SUM(his.[premier_one_step_recovery]), 
                   SUM(his.[premier_gift_memberships]),
                   SUM(his.[premier_one_step_eligible]),
                   SUM([one_step_premier]),
                   SUM(his.[one_step_total]), 
                   SUM(his.[upsell_eligible]), 
                   SUM(his.[multi_performances_orders]), 
                   SUM(his.[performances_sold]),
                   @user_count
            FROM [#trx_percentages] AS his
            WHERE [user_id] IN (SELECT [user_id] FROM [#trx_percentages] GROUP BY [user_id] HAVING COUNT(DISTINCT [sale_date]) > 1)
            GROUP BY [user_name], his.user_name_sort, his.user_location
            HAVING SUM([orders_touched]) > 0

            

    /*  Insert deparmental totals - Daily if needed  */

        IF @include_daily_detail = 'Y' AND @include_operator_detail = 'N'
    
            INSERT INTO [#trx_percentages] ([sale_dt], [sale_date], [user_id], [user_name], [user_name_sort], [user_location], [orders_created], [orders_touched], [membership_sales],
                                            [memb_conversion_eligible], [total_one_step_recovery], [total_gift_memberships], [total_one_step_eligible], [basic_2_sales], [basic_5_sales], 
                                            [basic_8_sales], [basic_total_sales], [basic_one_step_recovery], [basic_gift_memberships], [basic_one_step_eligible], [one_step_basic], 
                                            [premier_2_sales], [premier_5_sales], [premier_8_sales], [premier_total_sales], [premier_one_step_recovery], [premier_gift_memberships], 
                                            [premier_one_step_eligible], [one_step_premier], [one_step_total], [upsell_eligible], [multi_performances_orders], [performances_sold],
                                            [user_count])
            SELECT his.[sale_dt],
                   his.[sale_date], 
                   '', 
                   [user_location] + ' Total', 
                   'ZZB_' + [user_location], 
                   [user_location], 
                   SUM(his.[orders_created]), 
                   SUM(his.[orders_touched]), 
                   SUM(his.[membership_sales]),
                   SUM(his.[memb_conversion_eligible]),
                   SUM(his.[total_one_step_recovery]), 
                   SUM(his.[total_gift_memberships]),
                   SUM(his.[total_one_step_eligible]),
                   SUM(his.[basic_2_sales]), 
                   SUM(his.[basic_5_sales]),
                   SUM(his.[basic_8_sales]), 
                   SUM(his.[basic_total_sales]), 
                   SUM(his.[basic_one_step_recovery]), 
                   SUM(his.[basic_gift_memberships]),
                   SUM(his.[basic_one_step_eligible]),
                   SUM(his.[one_step_basic]), 
                   SUM(his.[premier_2_sales]), 
                   SUM(his.[premier_5_sales]),
                   SUM(his.[premier_8_sales]), 
                   SUM(his.[premier_total_sales]),
                   SUM(his.[premier_one_step_recovery]), 
                   SUM(his.[premier_gift_memberships]),
                   SUM(his.[premier_one_step_eligible]),
                   SUM([one_step_premier]),
                   SUM(his.[one_step_total]), 
                   SUM(his.[upsell_eligible]), 
                   SUM(his.[multi_performances_orders]), 
                   SUM(his.[performances_sold]),
                   @user_count
            FROM [#trx_percentages] AS his
            GROUP BY his.[sale_dt], [sale_date], his.[user_location]
            --HAVING COUNT(DISTINCT [user_id]) > 1            

        /*  Insert deparmental totals - Full Total  */
            
            INSERT INTO [#trx_percentages] ([sale_dt], [sale_date], [user_id], [user_name], [user_name_sort], [user_location], [orders_created], [orders_touched], [membership_sales],
                                            [memb_conversion_eligible], [total_one_step_recovery], [total_gift_memberships], [total_one_step_eligible], [basic_2_sales], [basic_5_sales], 
                                            [basic_8_sales], [basic_total_sales], [basic_one_step_recovery], [basic_gift_memberships], [basic_one_step_eligible], [one_step_basic], 
                                            [premier_2_sales], [premier_5_sales], [premier_8_sales], [premier_total_sales], [premier_one_step_recovery], [premier_gift_memberships], 
                                            [premier_one_step_eligible], [one_step_premier], [one_step_total], [upsell_eligible], [multi_performances_orders], [performances_sold],
                                            [user_count])
            SELECT NULL,
                   '', 
                   '', 
                   [user_location] + ' Total', 
                   'ZZC_' + [user_location], 
                   [user_location], 
                   SUM(his.[orders_created]), 
                   SUM(his.[orders_touched]), 
                   SUM(his.[membership_sales]),
                   SUM(his.[memb_conversion_eligible]),
                   SUM(his.[total_one_step_recovery]), 
                   SUM(his.[total_gift_memberships]),
                   SUM(his.[total_one_step_eligible]),
                   SUM(his.[basic_2_sales]), 
                   SUM(his.[basic_5_sales]),
                   SUM(his.[basic_8_sales]), 
                   SUM(his.[basic_total_sales]), 
                   SUM(his.[basic_one_step_recovery]), 
                   SUM(his.[basic_gift_memberships]),
                   SUM(his.[basic_one_step_eligible]),
                   SUM(his.[one_step_basic]), 
                   SUM(his.[premier_2_sales]), 
                   SUM(his.[premier_5_sales]),
                   SUM(his.[premier_8_sales]), 
                   SUM(his.[premier_total_sales]),
                   SUM(his.[premier_one_step_recovery]), 
                   SUM(his.[premier_gift_memberships]),
                   SUM(his.[premier_one_step_eligible]),
                   SUM([one_step_premier]),
                   SUM(his.[one_step_total]), 
                   SUM(his.[upsell_eligible]), 
                   SUM(his.[multi_performances_orders]), 
                   SUM(his.[performances_sold]),
                   @user_count
            FROM [#trx_percentages] AS his
            WHERE [user_id] <> ''
            GROUP BY his.[user_location]
            --HAVING COUNT(DISTINCT [user_id]) > 1
                        
        /*  Insert Grand Total  */

            IF EXISTS (SELECT * FROM [#trx_percentages]) AND (SELECT COUNT(DISTINCT [user_location]) FROM [#trx_percentages] WHERE [user_id] <> '') > 1

                INSERT INTO [#trx_percentages] ([sale_dt], [sale_date], [user_id], [user_name], [user_name_sort], [user_location], [orders_created], [orders_touched], [membership_sales],
                                                [memb_conversion_eligible], [total_one_step_recovery], [total_gift_memberships], [total_one_step_eligible], [basic_2_sales], [basic_5_sales], 
                                                [basic_8_sales], [basic_total_sales], [basic_one_step_recovery], [basic_gift_memberships], [basic_one_step_eligible], [one_step_basic], 
                                                [premier_2_sales], [premier_5_sales], [premier_8_sales], [premier_total_sales], [premier_one_step_recovery], [premier_gift_memberships], 
                                                [premier_one_step_eligible], [one_step_premier], [one_step_total], [upsell_eligible], [multi_performances_orders], [performances_sold],
                                                [user_count])
                SELECT NULL,
                       '', 
                       '', 
                       'Grand Total', 
                       'ZZG_Grand Total', 
                       'ZZG_Grand Total', 
                       SUM(his.[orders_created]), 
                       SUM(his.[orders_touched]), 
                       SUM(his.[membership_sales]),
                       SUM(his.[memb_conversion_eligible]),
                       SUM(his.[total_one_step_recovery]), 
                       SUM(his.[total_gift_memberships]),
                       SUM(his.[total_one_step_eligible]),
                       SUM(his.[basic_2_sales]), 
                       SUM(his.[basic_5_sales]),
                       SUM(his.[basic_8_sales]), 
                       SUM(his.[basic_total_sales]), 
                       SUM(his.[basic_one_step_recovery]), 
                       SUM(his.[basic_gift_memberships]),
                       SUM(his.[basic_one_step_eligible]),
                       SUM(his.[one_step_basic]), 
                       SUM(his.[premier_2_sales]), 
                       SUM(his.[premier_5_sales]),
                       SUM(his.[premier_8_sales]), 
                       SUM(his.[premier_total_sales]),
                       SUM(his.[premier_one_step_recovery]), 
                       SUM(his.[premier_gift_memberships]),
                       SUM(his.[premier_one_step_eligible]),
                       SUM([one_step_premier]),
                       SUM(his.[one_step_total]), 
                       SUM(his.[upsell_eligible]), 
                       SUM(his.[multi_performances_orders]), 
                       SUM(his.[performances_sold]),
                       @user_count
                    FROM [#trx_percentages] as his
                    WHERE his.[user_id] <> ''
          
        /*  Delete fine detail if that's what asked for  */

            IF @include_operator_detail = 'N'
                DELETE FROM [#trx_percentages] WHERE [user_id] <> ''

        /*  Do the Math  */

            UPDATE [#trx_percentages] SET [membership_percent] = ([membership_sales] / [memb_conversion_eligible]) WHERE [memb_conversion_eligible] <> 0

            UPDATE [#trx_percentages] SET [basic_2_percent] = ([basic_2_sales] / [membership_sales]) WHERE [membership_sales] <> 0
            UPDATE [#trx_percentages] SET [basic_5_percent] = ([basic_5_sales] / [membership_sales]) WHERE [membership_sales] <> 0
            UPDATE [#trx_percentages] SET [basic_8_percent] = ([basic_8_sales] / [membership_sales]) WHERE [membership_sales] <> 0
            UPDATE [#trx_percentages] SET [basic_total_percent] = ([basic_total_sales] / [membership_sales]) WHERE [membership_sales] <> 0
            UPDATE [#trx_percentages] SET [one_step_basic_percent] = ([one_step_basic] / [basic_one_step_eligible]) WHERE [basic_one_step_eligible] <> 0

            UPDATE [#trx_percentages] SET [premire_2_percent] = ([premier_2_sales] / [membership_sales]) WHERE [membership_sales] <> 0
            UPDATE [#trx_percentages] SET [premier_5_percent] = ([premier_5_sales] / [membership_sales]) WHERE [membership_sales] <> 0
            UPDATE [#trx_percentages] SET [premier_8_percent] = ([premier_8_sales] / [membership_sales]) WHERE [membership_sales] <> 0
            UPDATE [#trx_percentages] SET [premier_total_percent] = ([premier_total_sales] / [membership_sales]) WHERE [membership_sales] <> 0
            UPDATE [#trx_percentages] SET [one_step_premier_percent] = ([one_step_premier] / [premier_one_step_eligible]) WHERE [premier_one_step_eligible] <> 0

            UPDATE [#trx_percentages] SET [one_step_total_percent] = ([one_step_total] / [total_one_step_eligible]) WHERE [total_one_step_eligible] <> 0

            UPDATE [#trx_percentages] SET [multi_performance_percent] = ([multi_performances_orders] / [upsell_eligible]) WHERE [upsell_eligible] <> 0
            UPDATE [#trx_percentages] SET [average_perfs_per_order] = ([performances_sold] / [upsell_eligible]) WHERE [upsell_eligible] <> 0

            UPDATE [#trx_percentages] SET [user_name_sort] = [user_location] + ' ' + [user_name_sort]

    FINISHED:

            SELECT [user_id], 
                   [sale_date], 
                   [user_name], 
                   [user_name_sort], 
                   [user_location], 
                   [orders_created], 
                   [orders_touched], 
                   [membership_sales], 
                   [memb_conversion_eligible],
                   [membership_percent], 
                   [total_one_step_recovery], 
                   [total_gift_memberships], 
                   [total_one_step_eligible], 
                   [basic_2_sales], 
                   [basic_2_percent], 
                   [basic_5_sales], 
                   [basic_5_percent], 
                   [basic_8_sales], 
                   [basic_8_percent], 
                   [basic_total_sales], 
                   [basic_total_percent], 
                   [basic_one_step_eligible],
                   [one_step_basic],
                   [one_step_basic_percent], 
                   [premier_2_sales], 
                   [premire_2_percent], 
                   [premier_5_sales], 
                   [premier_5_percent], 
                   [premier_8_sales], 
                   [premier_8_percent], 
                   [premier_total_sales], 
                   [premier_total_percent], 
                   [premier_one_step_eligible],
                   [one_step_premier], 
                   [one_step_premier_percent], 
                   [one_step_total], 
                   [one_step_total_percent], 
                   [orders_counted], 
                   [upsell_eligible],
                   [multi_performances_orders], 
                   [multi_performance_percent], 
                   [performances_sold], 
                   [average_perfs_per_order], 
                   @user_count AS 'user_count'
            FROM [#trx_percentages]
            ORDER BY [user_name_sort], [sale_date]

    DONE:

        IF OBJECT_ID('tempdb..#trx_percentages') IS NOT NULL DROP TABLE [#trx_percentages]

END
GO

GRANT EXECUTE ON [dbo].[LRP_SALES_METRICS_REPORT] TO ImpUsers
GO

--EXECUTE [dbo].[LRP_SALES_METRICS_REPORT] 
--        @report_start_dt = '12-1-2018', @report_end_dt = '12-24-2018', @report_users = '', @user_locations = '',
--        @include_operator_detail = 'Y', @include_daily_detail = 'N'
--EXECUTE [dbo].[LRP_SALES_PERCENTAGES_REPORT] @report_start_dt = '11-1-2017', @report_end_dt = '11-30-2017',@report_users = '"sluorn00', @user_locations = '"Science Central"',@include_operator_detail = 'Y', @exclusion_accounts = '"agerma00"'