USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LP_UPDATE_SALES_METRICS]    Script Date: 6/27/2018 10:27:34 AM ******/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LP_UPDATE_HISTORY_SALES_METRICS]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LP_UPDATE_HISTORY_SALES_METRICS] AS' 
END
GO

/* 
        LP_UPDATE_HISTORY_TRX_STATS
            Renamed to LP_UPDATE_SALES_METRICS during 2018 Rewite

        Written by: Mark Sherwood June/July, 2017
        Rewrite: June, 2018
        
        Procedure to update the LT_HISTORY_TRX_STATS data mart that keeps track of operators and their membership sales and one step percentages
        as well as their add on orders and average venues per order.  This procedure is run each early morning as part of the ticket history
        update and processes a single day (the previous day).

            Parameters: @history_dt = the date to be processed.  If nothing passed to this parameter, procedure defaults to yesterday
                        @create_dt = Usually null, which defaults to current date and time.  If running manually for more than one date
                                     and you want the run date/time to be the same on all dates prcocessed, pass the date and time to
                                     the procedure in this variable.
                        @operator = Usually Null or blank, which means run for all operators.  If need to run for a single operator,
                                    pass the Tessitura user_id to the procedure.
                        @return_or_write = Added so that this procedure can be used for same day for a single operator to be used in the
                                           cashout report.  If 'W' procedure writes data to the data mart table.  If 'R' no data is written
                                           to that table.  Data is returned as a dataset for a report.  If 'B' then it both writes the 
                                           data to the data mart and returns the data as a dataset.
                                           NOTE: Will default to 'R' if @operator is not blank or null.
*/
ALTER PROCEDURE [dbo].[LP_UPDATE_HISTORY_SALES_METRICS]
        @history_dt datetime = null,                --If nothing passed, it will default to yesterday's date.
        @run_dt datetime = NULL,                    --When Calling for Multiple dates from another procedure - so that all dates have the same logged date and time
        @operator VARCHAR(10) = NULL,               --Procedure can be run for a single operator or for one operator (if run for cashout report)
        @machine_locations VARCHAR(4000) = NULL,    --Used only when called for the cashout report
        @return_or_write CHAR(1) = 'R'              --W = Write to data mart table / R = Return data from procedure (to a report) / B = Both
AS BEGIN                                                    --Note: Raw Data is never returned, only written to the data mart table

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    SET ANSI_WARNINGS OFF;
    SET NOCOUNT ON;

    /***************************************************************************************************************************************************/

    /*  Procedure Variables  */

        DECLARE @go_live_date CHAR(10) = '2016/05/18';
        
        DECLARE @excluded_modes_of_sale TABLE ([mos_id] INT, [mos_name] VARCHAR(30), [exclusion_type] VARCHAR(25));
        DECLARE @location_list TABLE ([location_no] INT);
        DECLARE @machine_list TABLE ([machine_name] VARCHAR(50));
        DECLARE @valid_titles TABLE ([title_name] VARCHAR(30));

        DECLARE @audio_tour_booth_location INT = 18   --id from TR_LOCATION

        DECLARE @showgo_orders TABLE ([order_no] INT, [order_exclude] CHAR(1));
        DECLARE @C19_Exclusion_Table TABLE ([price_type_no] INT, [comp_code_no] INT, [description] VARCHAR(30))
        DECLARE @source_exclusions TABLE ([source_no] INT, [source_name] VARCHAR(50))
        DECLARE @price_type_exclusions TABLE ([price_type_no] INT, [price_type_name] VARCHAR(50))

        DECLARE @final_table TABLE ([history_dt] DATETIME,
                                    [operator] VARCHAR(25), 
                                    [orders_created] DECIMAL(18,2) DEFAULT 0, 
                                    [orders_touched] DECIMAL(18,2) DEFAULT 0,
                                    [memb_conversion_eligible] DECIMAL(18,2) DEFAULT 0,
                                    [membership_sales] DECIMAL(18,2) DEFAULT 0, 
                                    [basic_2_sales] DECIMAL(18,2) DEFAULT 0,
                                    [basic_5_sales] DECIMAL(18,2) DEFAULT 0, 
                                    [basic_8_sales] DECIMAL(18,2) DEFAULT 0, 
                                    [basic_total_sales] DECIMAL(18,2) DEFAULT 0,
                                    [premier_2_sales] DECIMAL(18,2) DEFAULT 0, 
                                    [premier_5_sales] DECIMAL(18,2) DEFAULT 0, 
                                    [premier_8_sales] DECIMAL(18,2) DEFAULT 0,
                                    [premier_total_sales] DECIMAL(18,2) DEFAULT 0, 
                                    [one_step_eligible] DECIMAL(18,2) DEFAULT 0,
                                    [one_step_eligible_basic] DECIMAL(18,2) DEFAULT 0,
                                    [one_step_eligible_premier] DECIMAL(18,2) DEFAULT 0,
                                    [one_step_basic] DECIMAL(18,2) DEFAULT 0,
                                    [one_step_premier] DECIMAL(18,2) DEFAULT 0, 
                                    [one_step_total] DECIMAL(18,2) DEFAULT 0, 
                                    [basic_one_step_recovery] DECIMAL(18,2) DEFAULT 0, 
                                    [premier_one_step_recovery] DECIMAL(18,2) DEFAULT 0, 
                                    [total_one_step_recovery] DECIMAL(18,2) DEFAULT 0,
                                    [basic_gift_memberships] DECIMAL(18,2) DEFAULT 0, 
                                    [premier_gift_memberships] DECIMAL(18,2) DEFAULT 0, 
                                    [total_gift_memberships] DECIMAL(18,2) DEFAULT 0,
                                    [upsell_eligible] DECIMAL(18,2) DEFAULT 0,
                                    [upsell_orders] DECIMAL(18,2) DEFAULT 0,
                                    [total_perfs_sold] DECIMAL (18,2) DEFAULT 0);
    
     /*  Check Parameters  */

        --If no date passed, run for yesterday's date
        SELECT @history_dt = ISNULL(@history_dt,DATEADD(DAY, -1, GETDATE()));

        --If no specific run_dt is passed, use current date and time
        SELECT @run_dt = ISNULL(@run_dt,GETDATE());

        --Remove time from the history_dt if any and set to 00:00:00
        SELECT @history_dt = CONVERT(DATE,@history_dt);

        SELECT @operator = ISNULL(@operator,'');

        --If anything other than (R)eturn or (B)oth is passed to this parameter, 
        --(W)rite is assumed (at least at first - see next statement)
        SELECT @return_or_write = ISNULL(@return_or_write,'W');
        IF @return_or_write NOT IN ('R','B','D') SELECT @return_or_write = 'W';

        --If being run for a single operator, (R)eturn is the only option.
        --It cannot be (W)ritten to table
        IF @operator <> '' SELECT @return_or_write = 'R';

        /*  If specific machine locations were passed to the procedure, parse them out into the @location_list table
            If no machine location passed, add all machine names to the machine_list table.  */

            IF ISNULL(@machine_locations,'') <> ''
                INSERT INTO @location_list ([location_no])
                SELECT [Element] FROM dbo.FT_SPLIT_LIST(REPLACE(@machine_locations,'"',''),',');

                    --If pulling specific locations, it's being called by the cashier report which may want the audio tour booth
                    --but sales metrics should never include the audio tour booth, so delete it even if it's specifically asked for
                    DELETE FROM @location_list WHERE [location_no] = @audio_tour_booth_location

            IF EXISTS (SELECT * FROM @location_list)
                INSERT INTO @machine_list ([machine_name])
                    SELECT mac.[machine_name] 
                    FROM [dbo].[TX_MACHINE_LOCATION] AS mac 
                         INNER JOIN @location_list AS loc ON loc.[location_no] = mac.[location];
            ELSE
                INSERT INTO @machine_list ([machine_name])
                    SELECT [machine_name]                                         --If no specific locations asked for
                    FROM [dbo].[TX_MACHINE_LOCATION]                              --include all machines except those
                    WHERE ISNULL([location],0) <> @audio_tour_booth_location;     --assigned to the audio tour booth location
 
    /***************************************************************************************************************************************************/

    /*  Temporary Tables  */

        IF OBJECT_ID('tempdb..#order_table') IS NOT NULL DROP TABLE [#order_table];
    
        CREATE TABLE [#order_table] ([operator] VARCHAR(10) NOT NULL,                   --Operator who created or updated the order
                                     [order_no] INT NOT NULL,                           --Order #
                                     [source_no] INT NOT NULL,                          --Source Number on the order
                                     [mode_of_sale_no] INT NOT NULL,                    --Mode of Sale (number) the order was created in
                                     [mode_of_sale] VARCHAR(30) NOT NULL,               --Mode of Sale (name) the order was created in
                                     [is_excluded_mos_mem] CHAR(1) DEFAULT 'N',         --(Y/N) Sale made in a mode of sale excluded from mem conversion
                                     [is_excluded_mos_upsell] CHAR(1) DEFAULT 'N',      --(Y/N) Sale made in a mode of sale excluded from upsell stats
                                     [is_group_order] CHAR(1) DEFAULT 'N',              --(Y/N) Order contains a performance with 15 or more people
                                     [create_loc] VARCHAR(50) NOT NULL,                 --Machine on which the order was created or changed
                                     [order_access] CHAR(1) NOT NULL,                   --(C/A) Whether order created by or added to by operator
                                     [is_sale_to_mem] CHAR(1) DEFAULT 'N',              --(Y/N) Whether sale made to an active member (at the time)
                                     [is_temp_card_order] CHAR(1) DEFAULT 'N',          --(Y/N) Whether sale made to an active member (at the time)
                                     [in_renewal_prd] CHAR(1) DEFAULT 'N',              --(Y/N) Whether active member within designated renewal time
                                     [has_pending] CHAR(1) DEFAULT 'N',                 --(Y/N) Whether active member has pending renewal
                                     [in_one_step] CHAR(1) DEFAULT 'N',                 --(Y/N) Whether active member is a part of one-step
                                     [is_mem_sale] CHAR(1) DEFAULT 'N',                 --(Y/N) Whether there is a membership sold on this order
                                     [is_mem_upgrade] CHAR(1) DEFAULT 'N',              --(Y/N) Whether this is an mid-cycle upgrade
                                     [mem_sale_level] VARCHAR(30) DEFAULT '',           --Level of membership was purchased
                                     [mem_is_renewal] CHAR(1) DEFAULT 'N',              --(Y/N) Membership sale is a renewal
                                     [mem_is_one_step_recovery] CHAR(1) DEFAULT 'N',    --(Y/N) Membership sale is a one-step recovery
                                     [mem_add_onestep] CHAR(1) DEFAULT 'N',             --(Y/N) Member newly added to one-step at time of sale
                                     [mem_one_step_count] INT DEFAULT 0,                --(1/0) Should be counted in the one_step rate
                                     [is_gift_mem_sale] CHAR(1) DEFAULT 'N',            --(Y/N) Wheter the membership sale is a gift
                                     [gift_mem_sale_level] VARCHAR(30) DEFAULT '',      --Level of gift membership was purchased
                                     [mem_sale_sli] INT DEFAULT(0),                     --Subline Item number of the membership sale
                                     [is_course_order] CHAR(1) DEFAULT 'N',             --(Y/N) Whether the order contains a course registration
                                     [customer_no] INT,                                 --Customer number assigned to the order
                                     [mem_no] INT,                                      --Membership number (if a member)
                                     [mem_expire] DATETIME,                             --Expiration date of the current membership
                                     [prev_mem_no] INT DEFAULT 0,                       --Previous Membership Number (if any)
                                     [prev_mem_expire] DATETIME,                        --Expiration date of the previous membership (if any)
                                     [prev_mem_on_one_step] CHAR(1) DEFAULT 'N',        --(Y/N) Previous Membership was enrolled in one-step
                                     [mem_conv_eligible] INT DEFAULT 1,                 --(1/0) Order eligible to counttoward member conversion
                                     [one_step_eligible] INT DEFAULT 1,                 --(1/0) Order eligible to counttoward once step conversion
                                     [one_step_eligible_basic] INT DEFAULT 0,           --(1/0) Order eligible to counttoward once step conversion (Basic Membership)
                                     [one_step_eligible_premier] INT DEFAULT 0,         --(1/0) Order eligible to counttoward once step conversion (Premier Membership)
                                     [upsell_eligible] INT DEFAULT 1,                   --(1/0) Order eligible to count toward upsell stats
                                     [total_perfs] DECIMAL(18,4) DEFAULT 0.0,           --Total Number of upsell-valid performances on the order 
                                     [up_sell_counter] Decimal(18,4) DEFAULT 0.0);      --(1/0) This is an upsell order

        IF OBJECT_ID('tempdb..#membership_performances') IS NOT NULL DROP TABLE [#membership_performances];
    
        CREATE TABLE [#membership_performances] ([title_name] VARCHAR(30), 
                                                 [Production_name] VARCHAR(30), 
                                                 [production_season_name] VARCHAR(30),
                                                 [performance_no] INT, 
                                                 [performance_code] VARCHAR(10), 
                                                 [zone_no] INT, [zone_name] VARCHAR(30),
                                                 [performance_date] CHAR(10));

        IF OBJECT_ID('tempdb..#sli_cache') IS NOT NULL DROP TABLE [#sli_cache];

        CREATE TABLE [#sli_cache] ([sli_no] INT, 
                                   [sli_status] INT, 
                                   [order_no] INT, 
                                   [customer_no] INT, 
                                   [recipient_no] INT, 
                                   [customer_name] VARCHAR(125), 
                                   [is_member] CHAR(1), 
                                   [MOS] INT, 
                                   [perf_no] INT, 
                                   [zone_no] INT, 
                                   [perf_dt] DATETIME, 
                                   [perf_date] CHAR(10), 
                                   [perf_time] VARCHAR(8), 
                                   [performance_type] VARCHAR(30), 
                                   [title_name] VARCHAR(30), 
                                   [production_name] VARCHAR(30), 
                                   [price_type] INT, 
                                   [price_type_name] VARCHAR(30), [comp_code] INT, [comp_code_name] VARCHAR(30), [create_dt] DATETIME, 
                                   [created_by] VARCHAR(30), [last_update_dt] DATETIME, [last_updated_by] VARCHAR(30));


        IF OBJECT_ID('tempdb..#perf_cache') IS NOT NULL DROP TABLE [#perf_cache];

        CREATE TABLE [#perf_cache] ([performance_no] INT, 
                                    [performance_zone] INT, 
                                    [performance_dt] DATETIME, 
                                    [performance_date] CHAR(10), 
                                    [performance_time] VARCHAR(10), 
                                    [performance_type_name] VARCHAR(30), 
                                    [title_name] VARCHAR(30), 
                                    [production_name] VARCHAR(30))

    /***************************************************************************************************************************************************/

    /*  Cache the list of valid titles for add on stats
        To be counted in the add on stats, the order must contain at least one of these titles.  */

        INSERT INTO @valid_titles ([title_name])
        VALUES  ('4-D Theater'),                ('Arctic Adventure'),           ('Butterfly Garden'),
                ('Exhibit Halls'),              ('Events'),                     ('Hayden Planetarium'),
                ('Member Only Events'),         ('Membership'),                 ('Mugar Omni Theater'),
                ('Special Exhibitions');

    /*  Due to changes that had to be made to deal with Covid 19, there are orders in Tessitura that used to be only scans at the gate.
        The following Price Type/Combo code combinations on them should be excluded from the membership conversion counts  */
    
        INSERT INTO @C19_Exclusion_Table ([price_type_no],[comp_code_no],[description])
        VALUES  (268, 63, 'WIC EBT'),           (268, 90, 'WIC EBT'),           (268, 92, 'Military'),
                (268, 128, 'ConnectorCare'),    (268, 130, 'MIT'),              (268, 156, 'Vetarans'),
                (388, 8, 'EMP'),                (388, 16, 'COR'),               (388, 14, 'Overnight'),
                (388, 93, 'Overnight'),         (388, 7, 'EH Raincheck'),       (388, 91, 'EH Raincheck'),
                (161, 0, 'Free Library Pass')

      
    /*  Orders with items that use the following price types are excluded from membership conversion but not necessarily from Upsell  */        
        INSERT INTO @price_type_exclusions ([price_type_no], [price_type_name])
        VALUES  (390,'CityPASS Book Adult'),    (391, 'CityPASS Book Child'),   (392, 'CityPASS Mobile Adult'),
                (393, 'CityPASS Mobile Child'), (396, 'GoBoston Adult'),        (397, 'GoBoston Child')

    /*  Orders made with the following sources are excluded from the membership Conversion, but not necessarily from Upsell.  */
        INSERT INTO @source_exclusions ([source_no], [source_name])
        VALUES  (13782, 'Biogen'),              (13783, 'Bloomberg'),           (13793, 'BNY Mellon'),
                (13792, 'Bank of America'),     (13792, 'Bose'),                (13931, 'Dell'),
                (13784, 'Mathworks'),           (13785, 'Meditech'),            (13786, 'Millipore'),
                (13795, 'National Grid'),       (13787, 'Raytheon'),            (13788, 'Takeda Pharma'),
                (13796, 'TJX'),                 (13947, 'Vertex'),              (13789, 'WCVB-TV Channel 5')
       
    /*  Cache the list of excluded modes of sale - Any order from any of these modes of sale will be excluded completely from 
        the membership conversion rate the or the upsell rate.  */

        --Membership Conversion Exclusions
        INSERT INTO @excluded_modes_of_sale
        SELECT [id],[description], 'Membership Conversion' 
        FROM [dbo].[TR_MOS]
        WHERE [description] IN ('Buyouts',                          'Cash Drop',                        'CityPASS',
                                'Consignment',                      'Groups',                           'Internal Tickets and Event Add',   
                                'Schools',                          'Thrill Ride Capsule A',            'Thrill Ride Capsule B',
                                'Tour Operators',                   'Web Sales School');

        --Upsell Exclusions
        INSERT INTO @excluded_modes_of_sale
        SELECT [id],[description], 'Upsell' 
        FROM [dbo].[TR_MOS]
        WHERE [description] IN ('Buyouts',                          'Cash Drop',                        'CityPASS',
                                'Consignment',                      'Gift Membership',                  'Groups',
                                'Internal Tickets and Event Add',   'Schools',                          'Thrill Ride Capsule A',
                                'Thrill Ride Capsule B',            'Tour Operators',                   'Web Sales School');

    /*  Cache all membership performances after go live for reference */

        INSERT INTO [#membership_performances]
        SELECT [title_name], 
               [production_name], 
               [production_season_name], 
               [performance_no], 
               [performance_code],
               [performance_zone], 
               [performance_zone_text], 
               [performance_date]
        FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE]
        WHERE [title_name] = 'Membership' 
          AND [production_name] <> 'Library' 
          AND [performance_zone_text] NOT IN ('Lost Card Replacement', 'Temporary Membership Card')
          AND [performance_date] > @go_live_date;


    RAW_DATA:

    /***************************************************************************************************************************************************
    #order_table will keep a list of all orders that were created or touched on the history date being processed, along with the operator who created 
    or modified that order and any other pertinent information about the order.
    ***************************************************************************************************************************************************/

    /*  First group or orders is flagged as CREATED and contains all the orders created by each operator on the history date being processed. This comes
        from the order table using create_dt and not order_dt because the order_dt can be changed by the user when processing a transaction. */

        INSERT INTO [#order_table] ([order_no], [source_no], [operator], [create_loc], [order_access], [mode_of_sale_no],
                                    [mode_of_sale], [customer_no], [is_sale_to_mem], [mem_no])
        SELECT DISTINCT ord.[order_no], 
                        ord.[source_no],
                        ISNULL(ord.[created_by],'Unknown'), 
                        ord.[create_loc], 
                        'C', 
                        ord.[MOS],
                        ISNULL(mos.[description],''), 
                        ord.[customer_no],
                        [dbo].[LFS_ActiveMemberOnDate] (ord.[customer_no], 4, ord.[create_dt], 'Y'), 
                        [dbo].[LFS_ActiveMemberNumberOnDate] (ord.[customer_no], 4, ord.[create_dt])
        FROM [dbo].[T_ORDER] AS ord
             INNER JOIN [dbo].[T_LINEITEM] AS lin ON lin.[order_no] = ord.[order_no]
             LEFT OUTER JOIN [dbo].[TR_MOS] AS mos ON mos.[id] = ord.[MOS]
        WHERE CONVERT(DATE,ord.[create_dt]) = @history_dt
          AND (@operator = '' OR ord.[created_by] = @operator)
          AND ord.[create_loc] IN (SELECT [machine_name] FROM @machine_list);


    /*  The second group of orders is flagged as ADDED TO but not created and containes all the orders where an operator
        did not create, but added something to the order on the history date being processed. This comes from the sublineitem 
        table using create_dt. */
        
        INSERT INTO [#order_table] ([order_no], [source_no], [operator], [create_loc], [order_access], [mode_of_sale_no], [mode_of_sale], 
                                    [customer_no], [is_sale_to_mem], [mem_no])
        SELECT DISTINCT sli.[order_no], 
                        ord.[source_no],
                        ISNULL(sli.[created_by],'Unknown'), 
                        sli.[create_loc], 
                        'A', 
                        ord.[MOS],
                        ISNULL(mos.[description],''),
                        ord.[customer_no],
                        [dbo].[LFS_ActiveMemberOnDate] (ord.[customer_no], 4, ord.[create_dt], 'Y'), 
                        [dbo].[LFS_ActiveMemberNumberOnDate] (ord.[customer_no], 4, ord.[create_dt])
        FROM [dbo].[T_SUB_LINEITEM] AS sli
             INNER JOIN @machine_list AS mac ON mac.[machine_name] = sli.[create_loc]
             LEFT OUTER JOIN [dbo].[T_ORDER] AS ord ON ord.[order_no] = sli.[order_no]
             LEFT OUTER JOIN [dbo].[TR_MOS] AS mos ON mos.[id] = ord.[MOS]
        WHERE CONVERT(DATE,sli.[create_dt]) = @history_dt
          AND (@operator = '' OR sli.[created_by] = @operator)
          AND sli.[sli_status] IN (3, 6, 12);

    /*  DEDUPE:  Each operator/order_no combination should only be in the table once.
                 For any record in there twice once for create and once for add on,
                 delete the add on  */

          DELETE FROM [#order_table]
          WHERE CAST([order_no] AS VARCHAR(25)) + '_' + [operator] in (SELECT CAST([order_no] AS VARCHAR(25)) + '_' + [operator]
                                                                       FROM [#order_table] 
                                                                       GROUP BY CAST([order_no] AS VARCHAR(25)) + '_' + [operator]
                                                                       HAVING COUNT(*) > 1)
            AND [order_access] = 'A';

                            /*  NOTE: There used to be a third insert statement that pulled based on last_update_dt, 
                                      but this was pulling transactions that were only printed (nothing added to the 
                                      order.  Those orders should be excluded  */

    /*  If sale to member = 'N' but memb no > 0 that is the membership they are purchasing - Blank Membership Info */
    
        UPDATE [#order_table]
        SET [mem_no] = 0,
            [in_one_step] = 'N',
            [mem_expire] = Null
        WHERE [is_sale_to_mem] = 'N'

    /*  Flag all orders that were made in one of the excluded modes of sale  */

        UPDATE [#order_table]
        SET [is_excluded_mos_mem] = 'Y'
        WHERE [mode_of_sale_no] IN (SELECT [mos_id] FROM @excluded_modes_of_sale WHERE [exclusion_type] = 'Membership Conversion');

        UPDATE [#order_table]
        SET [is_excluded_mos_upsell] = 'Y'
        WHERE [mode_of_sale_no] IN (SELECT [mos_id] FROM @excluded_modes_of_sale WHERE [exclusion_type] = 'Upsell');

    /***************************************************************************************************************************************************/

    /*  Cache all the subline item information for the orders in #order_table.
        NOTE: Original version of code did not cache and instead made separate calls back to the T_SUB_LINEITEM table
              Before caching, time to get through exclusions portion of script was almost three minutes.
              After caching, time to get through exlusions portion of script was about 20 seconds.  */

              WITH [CTE_ORDERS] ([order_no])
              AS (SELECT DISTINCT [order_no] 
                  FROM [#order_table]),

                   [CTE_SUBLINEITEM] ([sli_no], [sli_status], [order_no], [recipient_no], [perf_no], [zone_no], [price_type], 
                                    [comp_code], [create_dt], [created_by], [last_update_dt], [last_updated_by])
              AS (SELECT sli.[sli_no], 
                         sli.[sli_status], 
                         sli.[order_no], 
                         ISNULL(sli.[recipient_no],0),
                         sli.[perf_no], 
                         sli.[zone_no], 
                         sli.[price_type], 
                         ISNULL(sli.[comp_code],0), 
                         sli.[create_dt], 
                         sli.[created_by], 
                         sli.[last_update_dt], 
                         sli.[last_updated_by]
                 FROM dbo.T_SUB_LINEITEM AS sli
                      INNER JOIN [CTE_ORDERS] AS cte ON cte.[order_no] = sli.[order_no]),

                   [CTE_PERFORMANCES] ([performance_no], [performance_zone], [performance_dt], [performance_date], [performance_time], 
                                       [performance_type_name], [title_name], [production_name])
              AS (SELECT DISTINCT prf.[performance_no],
                                  prf.[performance_zone],
                                  prf.[performance_dt], 
                                  prf.[performance_date], 
                                  prf.[performance_time], 
                                  prf.[performance_type_name], 
                                  prf.[title_name], 
                                  prf.[production_name]
                  FROM dbo.LV_PRODUCTION_ELEMENTS_PERFORMANCE AS prf
                       INNER JOIN [CTE_SUBLINEITEM] AS sli ON sli.[perf_no] = prf.[performance_no] AND sli.[zone_no] = prf.[performance_zone])
        INSERT INTO [#sli_cache]
        SELECT sli.[sli_no], 
               sli.[sli_status], 
               sli.[order_no], 
               ord.[customer_no], 
               ISNULL(sli.[recipient_no],0),
               LTRIM(ISNULL(cus.[fname],'') + ' ' + ISNULL(cus.[lname],'')), 
               [dbo].[LFS_ActiveMemberOnDate] (ord.[customer_no], 4, ord.[create_dt], 'Y'),
               ord.[MOS], 
               sli.[perf_no], 
               sli.[zone_no], 
               prf.[performance_dt], 
               prf.[performance_date], 
               prf.[performance_time], 
               prf.[performance_type_name], 
               prf.[title_name], 
               prf.[production_name], 
               sli.[price_type], 
               typ.[description], 
               ISNULL(sli.[comp_code],0), 
               ISNULL(cmp.[description],''), 
               sli.[create_dt], 
               sli.[created_by], 
               sli.[last_update_dt], 
               sli.[last_updated_by]
        FROM [CTE_SUBLINEITEM] AS sli
             LEFT OUTER JOIN [dbo].[T_ORDER] AS ord ON ord.[order_no] = sli.[order_no]
             LEFT OUTER JOIN [dbo].[T_CUSTOMER] AS cus ON cus.[customer_no] = ord.[customer_no]
             LEFT OUTER JOIN [CTE_PERFORMANCES] AS prf ON prf.performance_no = sli.[perf_no] AND prf.[performance_zone] = sli.[zone_no]
             LEFT OUTER JOIN [dbo].[TR_PRICE_TYPE] AS typ ON typ.[id] = sli.[price_type]
             LEFT OUTER JOIN [dbo].[TR_COMP_CODE] AS cmp ON cmp.[id] = sli.[comp_code]
        WHERE sli.[sli_status] IN (2, 3, 6, 12);
        
    /*  EXCLUDE EMPTY ORDERS:  These are orders that have no sublineitem records with a status of 2 (seated, unpaid), 3 (seated, paid) or 12 (ticketed, paid)  */

        DELETE FROM [#order_table]
        WHERE [order_no] IN (SELECT ord.[order_no]
                             FROM [#order_table] AS ord 
                                   LEFT OUTER JOIN [#sli_cache] AS sli ON ISNULL(sli.[order_no],0) = ord.[order_no] AND ISNULL(sli.[sli_status],0) IN (2, 3, 12)
                             GROUP BY ord.[order_no] 
                             HAVING COUNT(DISTINCT sli.[sli_no]) = 0);

    /*  EXCLUDE GROUP ORDERS:  An order is designated as a group sale if it contains any single performance with 15 or more paying visitors attending. 
                               It will still be counted if the tickets were paid for but never printed. 
                               sli_status 3 = Seated, Paid and sli_status 12 = Ticketed, Paid   */
            
        UPDATE [#order_table] 
        SET [is_group_order] = 'Y'
        WHERE [order_no] IN (SELECT [order_no] 
                             FROM [#sli_cache]
                             WHERE [title_name] IN (SELECT [title_name] FROM @valid_titles) 
                               AND [sli_status] IN (2, 3, 12)
                             GROUP BY [order_no], [perf_no], [zone_no]
                             HAVING COUNT(*) >= 15);

    /*  Get Expiration Date of current membership and whether membership is enrolled in the one step.  */

        UPDATE [#order_table]
        SET [mem_expire] = (SELECT [expr_dt]
                            FROM [dbo].[TX_CUST_MEMBERSHIP] AS mbr
                            WHERE mbr.[cust_memb_no] = [#order_table].[mem_no]),
            [in_one_step] = (SELECT CASE WHEN ISNULL(mbr.[ship_method],0) = 3 THEN 'Y' ELSE 'N' END
                             FROM [dbo].[TX_CUST_MEMBERSHIP] AS mbr
                             WHERE mbr.[cust_memb_no] = [#order_table].[mem_no])
        WHERE [mem_no] > 0;

    /*  Determine which sales to members are within their membership renewal period  */

        UPDATE [#order_table]
        SET [In_renewal_prd] = 'Y'
        WHERE [order_no] IN (SELECT ord.[order_no]
                             FROM [#order_table] AS ord
                                  INNER JOIN [dbo].[TX_CUST_MEMBERSHIP] AS mem ON mem.[customer_no] = ord.[customer_no]
                                  INNER JOIN [dbo].[T_MEMB_LEVEL] AS lev ON lev.[memb_level] = mem.[memb_level]
                            WHERE ord.[is_sale_to_mem] = 'Y' 
                              AND @history_dt BETWEEN mem.[init_dt] AND mem.[expr_dt] 
                              AND mem.current_status IN (1, 2)
                              AND @history_dt BETWEEN DATEADD(MONTH,(lev.renewal * -1),mem.[expr_dt]) AND mem.[expr_dt]);

    /*  If the order is a membership sale (regular, gift or upgrade) determine the sub line item number for that sale
        Pull the highest subline item for a membership performance where the status is seated/Paid or Ticketed/Paid
        This sli number is going to be used to get additional information about the membership  */

        UPDATE [#order_table]
        SET [mem_sale_sli] = (SELECT ISNULL(MAX(sli.sli_no),0)
                              FROM [#sli_cache] AS sli (NOLOCK)
                                   INNER JOIN [#membership_performances] AS prf ON prf.[performance_no] = sli.[perf_no] 
                                                                               AND prf.[zone_no] = sli.[zone_no]
                               WHERE sli.[order_no] = [#order_table].[order_no] and sli.[sli_status] IN (3, 12));
            
    /*  Using the sub line item number generated in the previous step to determine the pertinent information
        about the membership sale using the LV_RPT_CASHOUT_MEMBERSHIPS view. */

        UPDATE ord
        SET ord.[mem_is_one_step_recovery] = mem.[Is_one_step_recovery],             
            ord.[is_gift_mem_sale] = mem.[is_gift_membership],
            ord.[mem_sale_level] = mem.[membership_type],
            ord.[mem_add_onestep] = mem.[add_one_step],
            ord.[mem_one_step_count] = mem.[one_step_count],
            ord.[is_mem_sale] = 'Y'
        FROM [#order_table] AS ord
             INNER JOIN [dbo].[LV_RPT_CASHOUT_MEMBERSHIPS] AS mem ON mem.[sli_no] = ord.[mem_sale_sli]
        WHERE ord.[mem_sale_sli] > 0;
        
    /*  If it's a gift membership, move the level information into the gift_mem_sale_level column and set mem sale to N 
            NOTE: In this context, it's either a membership sale or a gift membership sale, not both.  */

        UPDATE [#order_table]
        SET [gift_mem_sale_level] = [mem_sale_level],
            [mem_sale_level] = '',
            [is_mem_sale] = 'N'
        WHERE [is_gift_mem_sale] = 'Y';

    /*  If the membership sale is a mid-cycle upgrade, indicate that and set mem sale to N
            NOTE: In this context, it's either a membership sale or an upgrade, not both.  */

        UPDATE [#order_table]
        SET [is_mem_upgrade] = 'Y',
            [is_mem_sale] = 'N'
        WHERE ISNULL([mem_sale_level],'') LIKE '%Upgrade%' OR ISNULL([gift_mem_sale_level],'') LIKE '%Upgrade%';

    /*  If the order is a membership sale (not a gift) and it is a sale to an active memeber, the assumption is that
        the membership sale is a renewal of the active membership because if it wasn't, it would be a gift.  */

        UPDATE [#order_table]
        SET  [mem_is_renewal] = 'Y'
        WHERE [is_mem_sale] = 'Y' AND [is_sale_to_mem] = 'Y';

    /*  Indicate and customer who currently has a pending membership  */
            
        UPDATE ord 
        SET [has_pending] = CASE WHEN ISNULL(mem.[cust_memb_no],0) > 0 THEN 'Y' ELSE 'N' END
        FROM [#order_table] AS ord
             LEFT OUTER JOIN [dbo].[TX_CUST_MEMBERSHIP] AS mem ON mem.[customer_no] = ord.[customer_no] 
                                                              AND mem.[memb_org_no] = 4 
                                                              AND mem.[current_status] = 3;

      /*  Identify orders that contain course registrations  */
        
        UPDATE [#order_table]
        SET [is_course_order] = 'Y'
        WHERE [order_no] IN (SELECT [order_no] FROM [#sli_cache] WHERE title_name = 'Summer Courses');

    /*  Get previous membership number  */

        WITH [prev_memb_no] (customer_no, membership_no, membership_status,  previous_membership_no)
          AS (
              SELECT cur.[customer_no], cur.[cust_memb_no], cur.[current_status], ISNULL(MAX(prv.[cust_memb_no]),0) AS 'prev_cust_memb_no'
              FROM dbo.TX_CUST_MEMBERSHIP AS cur
                   LEFT OUTER JOIN [dbo].[TX_CUST_MEMBERSHIP] AS prv ON prv.customer_no = cur.customer_no
                                                                    AND prv.cust_memb_no < cur.cust_memb_no
                                                                    AND prv.current_status IN (1,2)
              WHERE cur.current_status IN (1, 2, 3)
              GROUP BY cur.[cust_memb_no], cur.[customer_no], cur.[current_status]
             )
        UPDATE ord
        SET  ord.[prev_mem_no] = ISNULL(prv.[previous_membership_no],0)
        FROM [#order_table] AS ord
             INNER JOIN [prev_memb_no] AS prv ON prv.[membership_no] = ord.[mem_no]
        WHERE ord.[mem_no] > 0;

    /*  Get previous membership information  */       
               
        WITH [prev_memb_info] (membership_no, membership_expire_dt, membership_ship_method) 
          AS (
              SELECT mem.[cust_memb_no], mem.[expr_dt], mem.[ship_method]
              FROM [dbo].[TX_CUST_MEMBERSHIP] AS mem
              WHERE mem.cust_memb_no IN (SELECT prev_mem_no FROM [#order_table] WHERE prev_mem_no > 0)
             )
        UPDATE ord
        SET ord.[prev_mem_expire] = prv.[membership_expire_dt],
            ord.[prev_mem_on_one_step] = CASE WHEN ISNULL(prv.[membership_ship_method],0) = 3 THEN 'Y' ELSE 'N' END
        FROM [#order_table] AS ord
             INNER JOIN [prev_memb_info] AS prv ON prv.membership_no = ord.[prev_mem_no];


    /*  On orders where the membership is added after the fact (stubs) make sure only the person selling the membership gets credit.  */

        WITH [missing_memb_perfs] ([order_no], [operator]) AS
            (SELECT ord.[order_no], ord.[operator]
             FROM [#order_table] AS ord
                  LEFT OUTER JOIN [#sli_cache] AS sli ON sli.[order_no] = ord.[order_no] AND sli.[created_by] = ord.[operator] 
                                                     AND sli.title_name = 'Membership' AND sli.sli_status IN (3,12)
             WHERE (ord.[is_mem_sale] = 'Y' OR ord.[is_gift_mem_sale] = 'Y') AND ISNULL(sli.title_name,'') = '')
        UPDATE ord
        SET ord.[is_mem_sale] = 'N', 
            ord.[mem_sale_level] = '', 
            ord.[is_gift_mem_sale] = 'N', 
            ord.[gift_mem_sale_level] = '', 
            ord.[mem_add_onestep] = 'N',
            ord.[mem_sale_sli] = 0
        FROM [#order_table] AS ord
             INNER JOIN [missing_memb_perfs] AS mem ON mem.order_no = ord.[order_no] AND mem.[operator] = ord.[operator];

    /*  Set the temp card flag  */

        UPDATE [#order_table]
        SET [is_temp_card_order] = 'Y'
        WHERE [order_no] IN (SELECT [order_no] FROM [#sli_cache] WHERE [zone_no] IN (337,400));

    /*********************************************************************************************/
    /************************************** EXCLUSIONS *******************************************/
    /*********************************************************************************************/

    /*  ***************************************************************************************  */
    /*  Exclude orders from what's eligible to be counted toward the MEMBERSHIP CONVERSION rate  */
    /*  ***************************************************************************************  */

        --Exclude all orders made in any of the excluded modes of sale
        UPDATE [#order_table] 
        SET [mem_conv_eligible] = 0 
        WHERE [is_excluded_mos_mem] = 'Y';

        --Exclude groups of 15 or more people
        UPDATE [#order_table] 
        SET [mem_conv_eligible] = 0 
        WHERE [is_group_order] = 'Y';

        --Exclude mid-cycle upgrades
        UPDATE [#order_table] 
        SET [mem_conv_eligible] = 0 
        WHERE [is_mem_upgrade] = 'Y';

        --Exclude if currently a member, not in their renewal period, and it is not a gift membership sale
        UPDATE [#order_table] 
        SET [mem_conv_eligible] = 0 
        WHERE [is_sale_to_mem] = 'Y' 
          AND [In_renewal_prd] = 'N' 
          AND [is_gift_mem_sale] = 'N';

        --Exclude if currently a member and in their renewal period but also enrolled in one-step and it's not a gift membership sale
        UPDATE [#order_table] 
        SET [mem_conv_eligible] = 0 
        WHERE [is_sale_to_mem] = 'Y' 
          AND [In_renewal_prd] = 'Y' 
          AND [in_one_step] = 'Y' 
          AND prev_mem_on_one_step = 'Y' 
          AND [is_gift_mem_sale] = 'N';
        
        --Exclude if currently a member and in their renewal perions but has a pending renewal but that renewal was not sold on this order and it's not a gift membership sale
        UPDATE [#order_table] 
        SET [mem_conv_eligible] = 0 
        WHERE [is_sale_to_mem] = 'Y' 
          AND [In_renewal_prd] = 'Y' 
          AND [has_pending] = 'Y'
          AND [is_mem_sale] = 'N'
          AND [is_gift_mem_sale] = 'N';

        --Exclude all orders that are one-step recoveries
        UPDATE [#order_table] 
        SET [mem_conv_eligible] = 0 
        WHERE [mem_is_one_step_recovery] = 'Y';

        --Include all orders that are gift memberships
        UPDATE [#order_table] 
        SET [mem_conv_eligible] = 1
        WHERE [is_gift_mem_sale] = 'Y';
        
        --Added 6/29/2021:  Exclude all orders that have designated source numbers
        --                  Source Exclusions table also added and populated above
        --                  [source_no] also added the the [#order_table] temp table and its INSERT statements
        UPDATE [#order_table]
        SET [mem_conv_eligible] = 0
        WHERE [source_no] IN (SELECT [source_no] 
                              FROM @source_exclusions)

        --Added 6/29/2021:  Exclude all orders that have items with the designated price Types
        --                  Price Type Exclusions table also added and populated above
       UPDATE ot 
        SET ot.[mem_conv_eligible] = 0
        FROM [#order_table] AS ot
             LEFT OUTER JOIN [#sli_cache] AS sl ON sl.[order_no] = ot.[order_no]
        WHERE sl.[price_type] IN (SELECT [price_type_no]
                                  FROM @price_type_exclusions)

    /*  Added 12/2020:  New for the updates made to deal with Covid 19  */

        --GET A LIST OF ALL COVID 19 ORDERS FOR THE DAY
        INSERT INTO @showgo_orders ([order_no], [order_exclude])
        SELECT DISTINCT [order_no], 'N'
        FROM [dbo].[LT_HISTORY_TICKET] 
        WHERE [performance_date] = FORMAT(@history_dt,'yyyy/MM/dd')
          AND (title_name LIKE 'Show and%' OR [price_type] = 161)
          AND [order_no] > 0;
  
        --Determine which of the orders need to be excluded
        UPDATE sgo 
        SET sgo.[order_exclude] = 'Y'
        FROM [dbo].[T_SUB_LINEITEM] AS sli
             INNER JOIN @showgo_orders AS sgo ON sgo.[order_no] = sli.[order_no]
             INNER JOIN @C19_Exclusion_Table AS exc ON exc.[price_type_no] = sli.[price_type] AND exc.[comp_code_no] = ISNULL(sli.[comp_code],0)
             INNER JOIN [dbo].[TR_PRICE_TYPE] AS ptp ON ptp.[id] = sli.[price_type]
             LEFT OUTER JOIN [dbo].[TR_COMP_CODE] AS cmp ON cmp.[id] = ISNULL(sli.[comp_code],0)
        WHERE sli.[sli_status] IN (3,12)

        UPDATE [#order_table]
        SET [mem_conv_eligible] = 0
        WHERE [order_no] IN (SELECT order_no 
                             FROM @showgo_orders 
                             WHERE [order_exclude] = 'Y')


        --Exclude Wonderfund Orders
        UPDATE [#order_table]
        SET [mem_conv_eligible] = 0
        WHERE [order_no] IN (SELECT DISTINCT h.[order_no]
                             FROM [dbo].[LT_HISTORY_TICKET] AS h
                                  INNER JOIN [dbo].[T_SUB_LINEITEM] AS s ON s.[sli_no] = h.[sli_no]
                                  INNER JOIN [dbo].[T_PRICING_RULE] AS r ON r.[id] = s.[rule_id] AND r.[description] LIKE 'WF%'
                             WHERE h.[performance_date] = FORMAT(@history_dt,'yyyy/MM/dd'));

        --Exclude Corporate Pass Orders
        UPDATE [#order_table]
        SET [mem_conv_eligible] = 0
        WHERE [order_no] IN (SELECT DISTINCT h.[order_no]
                             FROM [dbo].[LT_HISTORY_TICKET] AS h 
                                  INNER JOIN [dbo].[T_SUB_LINEITEM] AS s ON s.[order_no] = h.[order_no]
                             WHERE h.[performance_date] = FORMAT(@history_dt,'yyyy/MM/dd') 
                               AND s.[price_type] = 389);  --389 = Show and Go Corp
        

    /*  ************************************************************************  */
    /*  Exclude orders from what's eligible to be counted toward the UPSELL rate  */
    /*  ************************************************************************  */

        --Exclude all orders made in any of the excluded modes of sale
        UPDATE [#order_table] SET [upsell_eligible] = 0 
        WHERE [is_excluded_mos_upsell] = 'Y';

        --Exclude groups of 15 or more people
        UPDATE [#order_table] SET [upsell_eligible] = 0 
        WHERE [is_group_order] = 'Y';

        --Exclude couse registrations
        UPDATE [#order_table] SET [upsell_eligible] = 0 
        WHERE [is_course_order] = 'Y';

        --Exclude all orders that are one-step recoveries
        UPDATE [#order_table] SET [upsell_eligible] = 0 
        WHERE [mem_is_one_step_recovery] = 'Y';

        --Exclude all gift memberships from upsells
        UPDATE [#order_table] SET [upsell_eligible] = 0 
        WHERE [is_gift_mem_sale] = 'Y';

    /*  ****************************************************************************  */
    /*  Exclude orders from what's eligible to be counted toward ONE STEP CONVERSION  */
    /*  ****************************************************************************  */

        --Exclude all orders that are not membership sales
        UPDATE [#order_table] SET [one_step_eligible] = 0 
        WHERE [is_mem_sale] = 'N';

        --Exclude all orders not eligible to be counted in membership conversion
        UPDATE [#order_table] SET [one_step_eligible] = 0 
        WHERE [mem_conv_eligible] = 0;
        
        --Exclude all orders that are sold to members already in one-step
        UPDATE [#order_table] SET [one_step_eligible] = 0 
        WHERE [in_one_step] = 'Y';

        --Exclude all orders that are one-step recoveries
        UPDATE [#order_table] SET [one_step_eligible] = 0 
        WHERE [mem_is_one_step_recovery] = 'Y';

        --Exclude all orders that are one-step recoveries
        UPDATE [#order_table] SET [one_step_eligible] = 0 
        WHERE [is_gift_mem_sale] = 'Y' OR mode_of_sale = 'Gift Membership';

        --If not one-step eligible, make sure it is not being counted
        UPDATE [#order_table] SET [mem_one_step_count] = 0
        WHERE [one_step_eligible] = 0
        
    /*  Set the one step eligibility for specific membership types  */

        UPDATE [#order_table] SET [one_step_eligible_basic] = 1
        WHERE [one_step_eligible] = 1 AND mem_sale_level LIKE 'Basic%';

        UPDATE [#order_table] SET [one_step_eligible_premier] = 1
        WHERE [one_step_eligible] = 1 AND mem_sale_level LIKE 'Premier%';

    /*********************************************************************************************/
    /*********************************************************************************************/
    
    /*  Count the number of valid upsell performances on each order AND flag the orders that have multiple performances  */

        WITH performance_count ([order_no], [operator], [perf_count]) AS
            (SELECT ord.[order_no], sli.[created_by], COUNT(DISTINCT(CAST(sli.[perf_no] AS VARCHAR(20)) + '_' + CAST(sli.[zone_no] AS VARCHAR(20))))
             FROM  [#order_table] AS ord
                   INNER JOIN [#sli_cache] AS sli ON sli.[order_no] = ord.[order_no] AND [sli].[title_name] IN (SELECT title_name FROM @valid_titles)
             WHERE sli.[sli_status] IN (3, 12)
             GROUP BY ord.order_no, sli.[created_by])
        UPDATE [#order_table]
        SET [total_perfs] = ISNULL((SELECT MAX([perf_count])
                                    FROM [performance_count] AS prf 
                                    WHERE prf.[order_no] = [#order_table].[order_no] 
                                    AND prf.operator = [#order_table].[operator]),0)
        WHERE [upsell_eligible] = 1;
                
        UPDATE [#order_table] SET [up_sell_counter] = 1 WHERE [total_perfs] > 1;

    /*  If order contains no upsell eligible performances it is not upsell eligible  */

        UPDATE [#order_table] SET [upsell_eligible] = 0 
        WHERE [upsell_eligible] = 1 AND [total_perfs] = 0;

    /*  If it's an order the operator only added on to and it's not an upsell order, it's not eligible to be up sell
        NOTE: This gives credit if they do sell multiple things while not punishing if they don't */

        UPDATE [#order_table] SET [upsell_eligible] = 0 
        WHERE [upsell_eligible] = 1 AND [total_perfs] < 2 AND [order_access] = 'A';


    /*  Make sure there are no perfs attributed to orders that are not upsell eligible  */

        UPDATE [#order_table] SET [total_perfs] = 0.0 WHERE [upsell_eligible] = 0;

    METRICS:

    /* Orders Created:  All orders created by each operator on the date being processed  */

        INSERT INTO @final_table ([history_dt], [operator], [orders_created])
        SELECT @history_dt, [operator], COUNT([order_no]) 
        FROM [#order_table] 
        WHERE [order_access] = 'C' 
        GROUP BY [operator];

    /* Orders Touched:  All orders created by each operator plus any they did not create but added to
       on the date being processed   */

        INSERT INTO @final_table ([history_dt], [operator], [orders_touched])
        SELECT @history_dt, [operator], COUNT([order_no]) 
        FROM [#order_table] 
        GROUP BY [operator];

    /* Membership Conversion Eligible  Number of orders for the day that should be considered for membership conversion rates  */

        INSERT INTO @final_table ([history_dt], [operator], [memb_conversion_eligible])
        SELECT @history_dt, [operator], sum([mem_conv_eligible]) 
        FROM [#order_table] 
        GROUP BY [operator];

    /*  Membership Sales: Total Membership Sales for each operator (gift and regular)  */

        INSERT INTO @final_table ([history_dt], [operator], [membership_sales])
        SELECT @history_dt, [operator], COUNT([order_no]) 
        FROM [#order_table] 
        WHERE [mem_conv_eligible] = 1 and ([is_mem_sale] = 'Y' OR [is_gift_mem_sale] = 'Y')
        GROUP BY [operator];

    /*  Basic 2 Sales:  Total number of Basic 2 Memberships sold for each operator (gift and regular) */

        INSERT INTO @final_table ([history_dt], [operator], [basic_2_sales])
        SELECT @history_dt, [operator], COUNT([order_no]) 
        FROM [#order_table] 
        WHERE (([is_mem_sale] = 'Y' AND [mem_sale_level] = 'Basic 2') OR ([is_gift_mem_sale] = 'Y' AND [gift_mem_sale_level] = 'Basic 2'))
          AND mem_conv_eligible = 1
        GROUP BY [operator];

    /*  Basic 5 Sales:  Total number of Basic 5 Memberships sold for each operator (gift and regular) */

        INSERT INTO @final_table ([history_dt], [operator], [basic_5_sales])
        SELECT @history_dt, [operator], COUNT([order_no]) 
        FROM [#order_table] 
        WHERE (([is_mem_sale] = 'Y' AND [mem_sale_level] = 'Basic 5') OR ([is_gift_mem_sale] = 'Y' AND [gift_mem_sale_level] = 'Basic 5'))
          AND mem_conv_eligible = 1
        GROUP BY [operator];

    /*  Basic 8 Sales:  Total number of Basic 8 Memberships sold for each operator (gift and regular) */
        INSERT INTO @final_table ([history_dt], [operator], [basic_8_sales])
        SELECT @history_dt, [operator], COUNT([order_no]) 
        FROM [#order_table] 
        WHERE (([is_mem_sale] = 'Y' AND [mem_sale_level] = 'Basic 8') OR ([is_gift_mem_sale] = 'Y' AND [gift_mem_sale_level] = 'Basic 8'))
          AND mem_conv_eligible = 1
        GROUP BY [operator];

    /*  Premier 2 Sales:  Total number of Premier 2 Memberships sold for each operator (gift and regular) */
        
        INSERT INTO @final_table ([history_dt], [operator],  [Premier_2_sales])
        SELECT @history_dt, [operator], COUNT([order_no]) 
        FROM [#order_table] 
        WHERE (([is_mem_sale] = 'Y' AND [mem_sale_level] = 'Premier 2') OR ([is_gift_mem_sale] = 'Y' AND [gift_mem_sale_level] = 'Premier 2'))
          AND mem_conv_eligible = 1
        GROUP BY [operator];

    /*  Premier 5 Sales:  Total number of Premier 5 Memberships sold for each operator (gift and regular) */

        INSERT INTO @final_table ([history_dt], [operator], [Premier_5_sales])
        SELECT @history_dt, [operator], COUNT([order_no]) 
        FROM [#order_table] 
        WHERE (([is_mem_sale] = 'Y' AND [mem_sale_level] = 'Premier 5') OR ([is_gift_mem_sale] = 'Y' AND [gift_mem_sale_level] = 'Premier 5'))
          AND mem_conv_eligible = 1
        GROUP BY [operator];

    /*  Premier 8 Sales:  Total number of Premier 8 Memberships sold for each operator (gift and regular) */

        INSERT INTO @final_table ([history_dt], [operator], [Premier_8_sales])
        SELECT @history_dt, [operator], COUNT([order_no]) 
        FROM [#order_table] 
        WHERE (([is_mem_sale] = 'Y' AND [mem_sale_level] = 'Premier 8') OR ([is_gift_mem_sale] = 'Y' AND [gift_mem_sale_level] = 'Premier 8'))
          AND mem_conv_eligible = 1
        GROUP BY [operator];

    /*  One Step Eligible:  Total number of orders for each operator that should be considered when figuring out one step conversion */

        INSERT INTO @final_table ([history_dt], [operator], [one_step_eligible])
        SELECT @history_dt, [operator], COUNT([order_no]) 
        FROM [#order_table] 
        WHERE mem_conv_eligible = 1 AND one_step_eligible = 1
        GROUP BY [operator];

     /*  One Step Eligible BASIC:  Total number of orders for each operator that should be considered when figuring out one step conversion on Basic Memberships */

        INSERT INTO @final_table ([history_dt], [operator], [one_step_eligible_basic])
        SELECT @history_dt, [operator], COUNT([order_no]) 
        FROM [#order_table] 
        WHERE mem_conv_eligible = 1 AND one_step_eligible_basic = 1
        GROUP BY [operator];

    /*  One Step Eligible PREMIER:  Total number of orders for each operator that should be considered when figuring out one step conversion on Premier Memberships */

        INSERT INTO @final_table ([history_dt], [operator], [one_step_eligible_premier])
        SELECT @history_dt, [operator], COUNT([order_no]) 
        FROM [#order_table] 
        WHERE mem_conv_eligible = 1 AND one_step_eligible_premier = 1
        GROUP BY [operator];

    /*  One Step Basic:  Total number of eligible Basic membership sales that also added one step for each operator  */

        INSERT INTO @final_table ([history_dt], [operator], [one_step_basic])
        SELECT @history_dt, [operator], count([order_no]) 
        FROM [#order_table] 
        WHERE (([is_mem_sale] = 'Y' AND [mem_sale_level] like 'Basic%') OR ([is_gift_mem_sale] = 'Y' AND [gift_mem_sale_level] like 'Basic%'))
          AND [mem_conv_eligible] = 1
          AND [one_step_eligible] = 1
          AND [mem_add_onestep] = 'Y'
        GROUP BY [operator];

    /*  One Step Premier:  Total number of eligible Premier membership sales that also added one step for each operator  */

        INSERT INTO @final_table ([history_dt], [operator], [one_step_premier])
        SELECT @history_dt, [operator], count([order_no]) 
        FROM [#order_table] 
        WHERE (([is_mem_sale] = 'Y' AND [mem_sale_level] like 'Premier%') OR ([is_gift_mem_sale] = 'Y' AND [gift_mem_sale_level] like 'Premier%'))
          AND [mem_conv_eligible] = 1
          AND [one_step_eligible] = 1
          AND [mem_add_onestep] = 'Y'
        GROUP BY [operator];

    /*  Basic One Step Recovery:  Total number of Basic membership sales that were one-step recoveries  */

        INSERT INTO @final_table ([history_dt], [operator], [basic_one_step_recovery])
        SELECT @history_dt, [operator], count([order_no]) 
        FROM [#order_table] 
        WHERE [is_mem_sale] = 'Y' 
          AND [mem_sale_level] like 'Basic%'
          AND [mem_is_one_step_recovery] = 'Y'
        GROUP BY [operator];

    /*  Premier One Step Recovery:  Total number of Premier membership sales that were one-step recoveries  */

        INSERT INTO @final_table ([history_dt], [operator], [Premier_one_step_recovery])
        SELECT @history_dt, [operator], count([order_no]) 
        FROM [#order_table] 
        WHERE [is_mem_sale] = 'Y' 
          AND [mem_sale_level] like 'Premier%'
          AND [mem_is_one_step_recovery] = 'Y'
        GROUP BY [operator];

    /*  Basic Gift Memberships:  Total number of Basic gift membership sales for each operator  */
                
        INSERT INTO @final_table ([history_dt], [operator], [basic_gift_memberships])
        SELECT @history_dt, [operator], count([order_no]) 
        FROM [#order_table] 
        WHERE [is_gift_mem_sale] = 'Y' 
          AND [gift_mem_sale_level] like 'Basic%'
        GROUP BY [operator];

    /*  Premier Gift Memberships:  Total number of Premier gift membership sales for each operator  */

        INSERT INTO @final_table ([history_dt], [operator], [premier_gift_memberships])
        SELECT @history_dt, [operator], count([order_no]) 
        FROM [#order_table] 
        WHERE [is_gift_mem_sale] = 'Y' 
          AND [gift_mem_sale_level] like 'Premier%'
        GROUP BY [operator];

    /*  Upsell Eligible: Total number of orders for each operator that can be considered for the upsell rate  */        

        INSERT INTO @final_table ([history_dt], [operator], [upsell_eligible])
        SELECT @history_dt, [operator], sum([upsell_eligible]) 
        FROM [#order_table] 
        GROUP BY [operator];

    /*  Upsell Orders: Total number of orders for each operator that contain more than one performance  */        

        INSERT INTO @final_table ([history_dt], [operator], [upsell_orders])
        SELECT @history_dt, [operator], sum([up_sell_counter]) 
        FROM [#order_table] 
        GROUP BY [operator];

    /*  Total Performances Sold:  Total performances each operator sold on that day  */        

        INSERT INTO @final_table ([history_dt], [operator], [total_perfs_sold])
        SELECT @history_dt, [operator], sum([total_perfs]) 
        FROM [#order_table] 
        GROUP BY [operator];

    /*  Update totals for each type of membership  */

        UPDATE @final_table SET [basic_total_sales] = ([basic_2_sales] + [basic_5_sales] + [basic_8_sales]),
                                [premier_total_sales] = ([premier_2_sales] + [premier_5_sales] + [premier_8_sales]),
                                [one_step_total] = ([one_step_basic] + [one_step_premier]),
                                [total_one_step_recovery] = ([basic_one_step_recovery] + [premier_one_step_recovery]),
                                [total_gift_memberships] = ([basic_gift_memberships] + [premier_gift_memberships]);

    DATA_OUTPUT:
        
    /*  Return data from procedure or write data to the data mart table  */

        IF @return_or_write IN ('B','W') BEGIN

            /*  If data for this date already exists in the data mart, delete it  */
               
                DELETE FROM [dbo].[LT_HISTORY_SALES_METRICS] WHERE [history_dt] = @history_dt;
                DELETE FROM [dbo].[LT_HISTORY_SALES_METRICS_RAW] WHERE [history_dt] = @history_dt;

            /*  Write new data for this date to the data mart.  */
               
                INSERT INTO [dbo].[LT_HISTORY_SALES_METRICS] ([data_dt], [history_dt], [history_date], [operator], [operator_name], [operator_name_sort], [operator_location], [orders_created], 
                                                              [orders_touched], [memb_conversion_eligible], [membership_sales], [basic_2_sales], [basic_5_sales], [basic_8_sales], 
                                                              [basic_total_sales], [Premier_2_sales], [Premier_5_sales], [Premier_8_sales], [premier_total_sales], [one_step_eligible], 
                                                              [one_step_eligible_basic], [one_step_basic], [one_step_eligible_premier], [one_step_premier], [one_step_total], 
                                                              [basic_one_step_recovery], [premier_one_step_recovery], [total_one_step_recovery], [basic_gift_memberships], 
                                                              [premier_gift_memberships], [total_gift_memberships], [upsell_eligible], [upsell_orders], [total_perfs_sold])
                SELECT @run_dt AS [data_dt],
                       @history_dt AS [history_dt], 
                       CONVERT(VARCHAR(10),@history_dt,111) AS [history_date],
                       fin.[operator], 
                       usr.[full_name] AS [operator_name],
                       usr.[sort_name] AS [operator_name_sort],
                       usr.[location] AS [operator_location],
                       SUM(fin.[orders_created]) AS [orders_created], 
                       SUM(fin.[orders_touched]) AS [orders_touched],
                       SUM(fin.[memb_conversion_eligible]) AS [memb_conversion_eligible],
                       SUM(fin.[membership_sales]) AS [membership_sales],
                       SUM(fin.[basic_2_sales]) AS [basic_2_sales],
                       SUM(fin.[basic_5_sales]) AS [basic_5_sales],
                       SUM(fin.[basic_8_sales]) AS [basic_8_sales],
                       SUM(fin.[basic_total_sales]) AS [basic_total_sales],
                       SUM(fin.[Premier_2_sales]) AS [Premier_2_sales],
                       SUM(fin.[Premier_5_sales]) AS [Premier_5_sales],
                       SUM(fin.[Premier_8_sales]) AS [Premier_8_sales],
                       SUM(fin.[premier_total_sales]) AS [premier_total_sales],
                       SUM(fin.[one_step_eligible]) AS [one_step_eligible],
                       SUM(fin.[one_step_eligible_basic]) AS [one_step_eligible_basic],
                       SUM(fin.[one_step_basic]) AS [one_step_basic],
                       SUM(fin.[one_step_eligible_premier]) AS [one_step_eligible_premier],
                       SUM(fin.[one_step_premier]) AS [one_step_premier],
                       SUM(fin.[one_step_total]) AS [one_step_total],
                       SUM(fin.[basic_one_step_recovery]) AS [basic_one_step_recovery], 
                       SUM(fin.[premier_one_step_recovery]) AS [premier_one_step_recovery],
                       SUM(fin.[total_one_step_recovery]) AS [total_one_step_recovery],
                       SUM(fin.[basic_gift_memberships]) AS [basic_gift_memberships],
                       SUM(fin.[premier_gift_memberships]) AS [premier_gift_memberships],
                       SUM(fin.[total_gift_memberships]) as [total_gift_memberships],
                       SUM(fin.[upsell_eligible]) AS [upsell_eligible],
                       SUM(fin.[upsell_orders]) AS [upsell_orders],
                       SUM(fin.[total_perfs_sold]) AS [total_perfs_sold]
            FROM @final_table AS fin 
                 LEFT OUTER JOIN [dbo].[LV_MuseumUserInfo] AS usr ON usr.[userid] = fin.[operator]
            GROUP BY fin.[operator], usr.[full_name], usr.[sort_name], usr.[location];
            
            INSERT INTO [dbo].[LT_HISTORY_SALES_METRICS_RAW] ([data_dt], [history_dt], [history_date], [operator], [order_no], [mode_of_sale_no], [mode_of_sale], [is_excluded_mos_mem],
                                                              [is_excluded_mos_upsell], [is_group_order], [create_loc], [order_access], [is_sale_to_mem], [is_temp_card_order], [in_renewal_prd], 
                                                              [has_pending], [in_one_step], [is_mem_sale], [is_mem_upgrade], [mem_sale_level], [mem_is_renewal], [mem_is_one_step_recovery], 
                                                              [mem_add_onestep], [mem_one_step_count], [is_gift_mem_sale], [gift_mem_sale_level], [mem_sale_sli], [is_course_order], 
                                                              [customer_no], [mem_no], [mem_expire], [prev_mem_no], [prev_mem_expire], [prev_mem_on_one_step], [mem_conv_eligible], 
                                                              [one_step_eligible], [one_step_eligible_basic], [one_step_eligible_premier], [upsell_eligible], [total_perfs], [up_sell_counter])
            SELECT  @run_dt AS [data_dt],
                    @history_dt AS [history_dt],
                    CONVERT(CHAR(10),@history_dt,111) AS [history_date],
                    [operator],
                    [order_no],
                    [mode_of_sale_no],
                    [mode_of_sale],
                    [is_excluded_mos_mem],
                    [is_excluded_mos_upsell],
                    [is_group_order],
                    [create_loc],
                    [order_access],
                    [is_sale_to_mem],
                    [is_temp_card_order],
                    [in_renewal_prd],
                    [has_pending],
                    [in_one_step],
                    [is_mem_sale],
                    [is_mem_upgrade],
                    [mem_sale_level],
                    [mem_is_renewal],
                    [mem_is_one_step_recovery],
                    [mem_add_onestep],
                    [mem_one_step_count],
                    [is_gift_mem_sale],
                    [gift_mem_sale_level],
                    [mem_sale_sli],
                    [is_course_order],
                    [customer_no],
                    [mem_no],
                    [mem_expire],
                    [prev_mem_no],
                    [prev_mem_expire],
                    [prev_mem_on_one_step],
                    [mem_conv_eligible],
                    [one_step_eligible],
                    [one_step_eligible_basic],
                    [one_step_eligible_premier],
                    [upsell_eligible],
                    [total_perfs],
                    [up_sell_counter]
            FROM [#order_table];

        END
        
        IF @return_or_write IN ('B','R','D') BEGIN

            /*  Return new data for this date as a data set (for a report).  */

                IF @return_or_write = 'D'

                    SELECT  @run_dt AS [data_dt],
                            @history_dt AS [history_dt],
                            CONVERT(CHAR(10),@history_dt,111) AS [history_date],
                            [operator],
                            [order_no],
                            [mode_of_sale_no],
                            [mode_of_sale],
                            [is_excluded_mos_mem],
                            [is_excluded_mos_upsell],
                            [is_group_order],
                            [create_loc],
                            [order_access],
                            [is_sale_to_mem],
                            [is_temp_card_order],
                            [in_renewal_prd],
                            [has_pending],
                            [in_one_step],
                            [is_mem_sale],
                            [is_mem_upgrade],
                            [mem_sale_level],
                            [mem_is_renewal],
                            [mem_is_one_step_recovery],
                            [mem_add_onestep],
                            [mem_one_step_count],
                            [is_gift_mem_sale],
                            [gift_mem_sale_level],
                            [mem_sale_sli],
                            [is_course_order],
                            [customer_no],
                            [mem_no],
                            [mem_expire],
                            [prev_mem_no],
                            [prev_mem_expire],
                            [prev_mem_on_one_step],
                            [mem_conv_eligible],
                            [one_step_eligible],
                            [one_step_eligible_basic],
                            [one_step_eligible_premier],
                            [upsell_eligible],
                            [total_perfs],
                            [up_sell_counter]
                    FROM [#order_table];

                ELSE
                
                   SELECT  GETDATE() AS [data_dt],
                           @history_dt AS [history_dt], 
                           CONVERT(VARCHAR(10),@history_dt,111) AS [history_date],
                           fin.[operator] AS [operator], 
                           usr.[full_name] AS [operator_name],
                           usr.[sort_name] AS [operator_name_sort],
                           usr.[location] AS [operator_location],
                           SUM(fin.[orders_created]) AS [orders_created], 
                           SUM(fin.[orders_touched]) AS [orders_touched],
                           SUM(fin.[memb_conversion_eligible]) AS [memb_conversion_eligible],
                           SUM(fin.[membership_sales]) AS [membership_sales],
                           SUM(fin.[basic_2_sales]) AS [basic_2_sales],
                           SUM(fin.[basic_5_sales]) AS [basic_5_sales],
                           SUM(fin.[basic_8_sales]) AS [basic_8_sales],
                           SUM(fin.[basic_total_sales]) AS [basic_total_sales],
                           SUM(fin.[Premier_2_sales]) AS [Premier_2_sales],
                           SUM(fin.[Premier_5_sales]) AS [Premier_5_sales],
                           SUM(fin.[Premier_8_sales]) AS [Premier_8_sales],
                           SUM(fin.[premier_total_sales]) AS [premier_total_sales],
                           SUM(fin.[one_step_eligible]) AS [one_step_eligible],
                           SUM(fin.[one_step_eligible_basic]) AS [one_step_eligible_basic],
                           SUM(fin.[one_step_basic]) AS [one_step_basic],
                           SUM(fin.[one_step_eligible_premier]) AS [one_step_eligible_premier],
                           SUM(fin.[one_step_premier]) AS [one_step_premier],
                           SUM(fin.[one_step_total]) AS [one_step_total],
                           SUM(fin.[basic_one_step_recovery]) AS [basic_one_step_recovery], 
                           SUM(fin.[premier_one_step_recovery]) AS [premier_one_step_recovery],
                           SUM(fin.[total_one_step_recovery]) AS [total_one_step_recovery],
                           SUM(fin.[basic_gift_memberships]) AS [basic_gift_memberships],
                           SUM(fin.[premier_gift_memberships]) AS [premier_gift_memberships],
                           SUM(fin.[total_gift_memberships]) as [total_gift_memberships],
                           SUM(fin.[upsell_eligible]) AS [upsell_eligible],
                           SUM(fin.[upsell_orders]) AS [upsell_orders],
                           SUM(fin.[total_perfs_sold]) AS [total_perfs_sold]
                FROM @final_table AS fin
                     LEFT OUTER JOIN [dbo].[LV_MuseumUserInfo] AS usr ON usr.[userid] = fin.[operator]
                GROUP BY fin.[operator], usr.[full_name], usr.[sort_name], usr.[location]; 

    END

    DONE:

    /*  Destroy Temp Tables  */

        IF OBJECT_ID('tempdb..#sli_cache') IS NOT NULL DROP TABLE [#sli_cache];
        IF OBJECT_ID('tempdb..#membership_performances') IS NOT NULL DROP TABLE [#membership_performances];
        IF OBJECT_ID('tempdb..#order_table') IS NOT NULL DROP TABLE [#order_table];

END
GO

GRANT EXECUTE ON [dbo].[LP_UPDATE_HISTORY_SALES_METRICS] TO ImpUsers
GO

--EXECUTE [dbo].[LP_UPDATE_HISTORY_SALES_METRICS]
--        @history_dt = '9-7-2020',
--        @run_dt = NULL,
--        @operator = NULL,
--        @machine_locations = '',
--        @return_or_write = 'R'   

