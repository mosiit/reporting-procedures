USE [impresario]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[LRP_ADV_WKLY_NEW_CONTACTS]
    	@numOfDays INT = 15,
	    @workers_str VARCHAR(4000) = NULL,
        @show_notes CHAR(1) = 'A'
AS BEGIN

    -- Steps on Pipeline plans that are happening in the next 15 days 

    SET NOCOUNT ON 
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

    /*  Procedure Variables  */

        DECLARE @start_dt DATETIME = NULL, @end_dt DATETIME = NULL        

        DECLARE @worker_list TABLE ([worker_no] INT NOT NULL DEFAULT (0))

        DECLARE @abbrev_chars INT = 750

    /*  Check Parameters  */

        SELECT @numOfDays = ISNULL(@numOfDays, 7)
        
        SET @numOfDays = -1 * @numOfDays

        SELECT @end_dt = DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), 0) + ' 23:59:59.99'
        SELECT @start_dt = DATEDIFF(dd, 0, DATEADD(d, @numOfDays, @end_dt))

        SELECT @show_notes = ISNULL(@show_notes,'A')
        IF @show_notes NOT IN ('Y','N') SELECT @show_notes = 'A'
        
        IF ISNULL(@workers_str,'') = ''
            INSERT INTO @worker_list ([worker_no])
            SELECT ISNULL([worker_customer_no], 0)
            FROM [dbo].[LV_CAMPAIGN_ACTIVITY_WORKERS] 
            WHERE [inactive] = 'N'
              AND ISNULL([worker_customer_no], 0) > 0
        ELSE
            INSERT INTO @worker_list ([worker_no])
            SELECT ISNULL(TRY_CAST([Element] AS INTEGER),0) 
            FROM [dbo].[FT_SPLIT_LIST](REPLACE(@workers_str,'"',''),',')
            WHERE ISNULL(TRY_CAST([Element] AS INTEGER),0) > 0;

    /*  Get Data  */

        SELECT p.[customer_no], 
               cust.[display_name], 
               cust.[sort_name], 
               cam.[description] AS campaign_desc, 
               cdes.[description] AS cont_designation_desc,
               stype.[description] AS step_type_desc, 
               s.[step_dt], 
               s.[description] AS [step_desc],
               CASE WHEN @show_notes = 'N' THEN ''
                    WHEN @show_notes = 'Y' THEN REPLACE(ISNULL(s.[notes],''), CHAR(13) + CHAR(10), '')
                    ELSE LEFT(replace(ISNULL(s.[notes],''), CHAR(13) + CHAR(10), ''), @abbrev_chars) END AS [step_notes], 
               s.[worker_customer_no],
               wkr.[display_name] AS [worker_display_name],
               s.[create_dt]
        FROM [dbo].[T_PLAN] AS p
             INNER JOIN [dbo].[T_STEP] AS s ON s.[plan_no] = p.[plan_no]
             INNER JOIN [dbo].[TR_STEP_TYPE] AS stype ON stype.[id] = s.[step_type]
             INNER JOIN [dbo].[T_CAMPAIGN] AS cam ON cam.[campaign_no] = p.[campaign_no]
             INNER JOIN @worker_list AS lis ON lis.[worker_no] = s.[worker_customer_no]
             INNER JOIN [dbo].[TR_CONT_DESIGNATION] AS cdes ON cdes.[id] = p.[cont_designation]
             INNER JOIN [dbo].[FT_CONSTITUENT_DISPLAY_NAME]() AS cust ON p.[customer_no] = cust.[customer_no]
             LEFT JOIN [dbo].[FT_CONSTITUENT_DISPLAY_NAME]() AS wkr ON s.[worker_customer_no] = wkr.[customer_no]
        WHERE p.[type] = 7 -- Advancement Pipeline
          AND s.[step_type] NOT IN (-2, -1, 11, 13, 14, 37, 31, 33, 41, 42, 72)
          AND s.[create_dt] BETWEEN @start_dt AND @end_dt

END 
GO

EXECUTE [dbo].[LRP_ADV_WKLY_NEW_CONTACTS] @numOfDays = 15, @workers_str = '', @show_notes = 'A'

--worker_customer_no worker_display_name
--------------------- ---------------------------------- --------
--NULL                NULL                               8
--823834   6          David Sittenfeld DFS               1
--2776860  6          Dr. Ivana Magovcevic-Liebisch      2
--705080   6          Michael and Alison Bonney          3
--2747879  6          Mr. Chinh H. Pham                  1
--2671183  6          Mr. Michael G. Thonis              3
--2678473  5          Ms. Maria Lewis Kussmaul           5
--4013364  6          Naomi Rafal NLR                    1 -> 24

--SELECT wtp.[worker_customer_no], 
--       wtp.[worker_type], 
--       typ.[description],
--       wtp.[inactive]
--FROM [dbo].[TX_CUST_WORKER_TYPE] AS wtp
--     INNER JOIN [dbo].[TR_WORKER_TYPE] AS typ ON typ.[id] = wtp.[worker_type]
--WHERE [worker_customer_no] IN (4013364,2678473,2671183,2747879,705080,2776860,823834)


