USE impresario
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_ADV_QUALIFICATION_ACTIVITY_SUMMARY]') AND type IN (N'P', N'PC'))
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_ADV_QUALIFICATION_ACTIVITY_SUMMARY] AS' 
GO

EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LRP_ADV_QUALIFICATION_ACTIVITY_SUMMARY] TO [impusers], [tessitura_app]'
GO

/*      LRP_ADV_QUALIFICATION_ACTIVITY_SUMMARY

        NOTE: Contribution amounts are based on what's in The Plan record under cont_amount

*/

ALTER PROCEDURE [dbo].[LRP_ADV_QUALIFICATION_ACTIVITY_SUMMARY]
        @fiscal_year INT = NULL,
        @workers_str VARCHAR(4000) = NULL
WITH RECOMPILE AS BEGIN

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

    /* Procedure Variables  */

        --Determine current fiscal year, fiscal start date, fiscal end date, and current date
        DECLARE @fiscal_start_dt DATETIME = NULL, @fiscal_end_dt DATETIME = NULL
        DECLARE @current_dt DATETIME = GETDATE()

        DECLARE @worker_list TABLE ([worker_no] INT NOT NULL DEFAULT (0))

    /*  Temporary Tables  */
                                    
        IF OBJECT_ID('tempdb..#step_raw_data') IS NOT NULL DROP TABLE #step_raw_data

        CREATE TABLE #step_raw_data  ([step_no] INT NOT NULL DEFAULT (0), 
                                      [plan_no] INT NOT NULL DEFAULT (0), 
                                      [step_dt] DATETIME NULL, 
                                      [completed_on_dt] DATETIME NULL,
                                      [month_num] INT NOT NULL DEFAULT (0), 
                                      [month_abbrev] VARCHAR(30) NOT NULL DEFAULT (''),
                                      [month_sort] VARCHAR(7) NOT NULL DEFAULT (''),
                                      [month_year] VARCHAR(4) NOT NULL DEFAULT (''),
                                      [fiscal_year] INT NOT NULL DEFAULT (0),
                                      [is_current_fiscal] INT NOT NULL DEFAULT (0),
                                      [step_type] INT NOT NULL DEFAULT (0), 
                                      [step_type_name] VARCHAR(30) NOT NULL DEFAULT (''), 
                                      [step_description] VARCHAR(30) NOT NULL DEFAULT(''), 
                                      [associate_no] INT NOT NULL DEFAULT (0),
                                      [priority] INT NOT NULL DEFAULT (0), 
                                      [worker_customer_no] INT NOT NULL DEFAULT (0),
                                      [worker_name] VARCHAR(75) NOT NULL DEFAULT (''),
                                      [worker_initials] VARCHAR(10) NOT NULL DEFAULT (''),
                                      [worker_inactive] CHAR(1) NOT NULL DEFAULT ('N'),
                                      [is_visit] CHAR(1) NOT NULL DEFAULT ('N'),
                                      [is_qualification] CHAR(1) NOT NULL DEFAULT ('N')) 

        IF OBJECT_ID('tempdb..#final_table') IS NOT NULL DROP TABLE #final_table

        CREATE TABLE #final_table ([worker_customer_no] INT NOT NULL DEFAULT (0),
                                   [worker_name] VARCHAR(80) NOT NULL DEFAULT (''),
                                   [worker_initials] VARCHAR(75) NOT NULL DEFAULT (''),
                                   [worker_inactive] CHAR(1) NOT NULL DEFAULT ('N'),
                                   [plan_no] INT NOT NULL DEFAULT (0),
                                   [plan_status_no] INT NOT NULL DEFAULT (0),
                                   [plan_status] VARCHAR(30) NOT NULL DEFAULT (''),
                                   [campaign_no] INT NOT NULL DEFAULT (0),
                                   [campaign_description] VARCHAR(30) NOT NULL DEFAULT (''))

    /*  Check Parameters  */

        IF @fiscal_year IS NULL SELECT @fiscal_year = dbo.LF_GetFiscalYear(GETDATE())
        
        SELECT @fiscal_start_dt = [dbo].[LF_GetFiscalYearStartDt](@fiscal_year)

        SELECT @fiscal_end_dt = [dbo].[LF_GetFiscalYearEndDt](@fiscal_year)
        
        IF @current_dt > @fiscal_end_dt SELECT @current_dt = @fiscal_end_dt

        IF ISNULL(@workers_str,'') = ''
            INSERT INTO @worker_list ([worker_no])
            SELECT ISNULL([worker_customer_no], 0)
            FROM [dbo].[LV_CAMPAIGN_ACTIVITY_WORKERS] 
            WHERE [inactive] = 'N'
              AND ISNULL([worker_customer_no], 0) > 0
        ELSE
            INSERT INTO @worker_list ([worker_no])
            SELECT ISNULL(TRY_CAST([Element] AS INTEGER),0) 
            FROM [dbo].[FT_SPLIT_LIST](REPLACE(@workers_str,'"',''),',')
            WHERE ISNULL(TRY_CAST([Element] AS INTEGER),0) > 0

    /*  Raw Step Data  */
    
        INSERT INTO [#step_raw_data] ([step_no], [plan_no], [step_dt], [completed_on_dt], [month_num], [month_abbrev], [month_sort], [month_year], 
                                      [fiscal_year], [is_current_fiscal], [step_type], [step_type_name], [step_description], [associate_no], 
                                      [priority], [worker_customer_no], [worker_name], [worker_initials], [worker_inactive], [is_visit], [is_qualification])
        SELECT stp.[step_no],
               stp.[plan_no], 
               stp.[step_dt], 
               stp.[completed_on_dt],
               DATEPART(MONTH,stp.[completed_on_dt]),
               LEFT(DATENAME(MONTH,stp.[completed_on_dt]),3),
               LEFT(CONVERT(VARCHAR(10),stp.[completed_on_dt],111),7),
               DATENAME(YEAR,stp.[completed_on_dt]),
               CASE WHEN  stp.[completed_on_dt] < @fiscal_start_dt THEN (@fiscal_year - 1) ELSE @fiscal_year END,
               CASE WHEN stp.[completed_on_dt] < @fiscal_start_dt THEN 0 ELSE 1 END,
               stp.[step_type], 
               typ.[description], 
               stp.[description], 
               ISNULL(stp.[associate_no],0),
               stp.[priority], 
               stp.[worker_customer_no],
               wrk.[worker_name],
               wrk.[worker_initials],
               wrk.[inactive],
               CASE WHEN typ.[id] IN (29, 70, 76) THEN 'Y' ELSE 'N' END,
               CASE WHEN typ.[id] IN (70, 78) THEN 'Y' ELSE 'N' END
        FROM [dbo].[T_STEP] AS stp
             INNER JOIN @worker_list AS lis ON lis.[worker_no] = stp.[worker_customer_no]
             INNER JOIN [dbo].[LV_CAMPAIGN_ACTIVITY_WORKERS] AS wrk ON wrk.[worker_customer_no] = stp.[worker_customer_no] AND wrk.[inactive] = 'N'
             INNER JOIN [dbo].[TR_STEP_TYPE] AS typ ON typ.[id] = stp.[step_type]
        WHERE stp.[completed_on_dt] BETWEEN @fiscal_start_dt AND @current_dt

    /*  Final Qualifications Data  */

        INSERT INTO [#final_table] ([worker_customer_no],[worker_name],[worker_initials],[worker_inactive],[plan_no],[plan_status_no],[plan_status],[campaign_no],[campaign_description])
        SELECT stp.[worker_customer_no],
               stp.[worker_name],
               stp.[worker_initials],
               stp.[worker_inactive],
               stp.[plan_no],
               pln.[status] AS [plan_status_id],
               sta.[description] as [plan_status],
               pln.[campaign_no],
               cmp.[description] AS [campaign_description]
        FROM [#step_raw_data] AS stp
             LEFT OUTER JOIN [dbo].[T_PLAN] AS pln ON pln.[plan_no] = stp.[plan_no]
             LEFT OUTER JOIN [dbo].[TR_PLAN_STATUS] AS sta ON sta.[id] = pln.[status]
             LEFT OUTER JOIN [dbo].[T_CAMPAIGN] AS cmp ON cmp.[campaign_no] = pln.[campaign_no]
        WHERE stp.[completed_on_dt] BETWEEN @fiscal_start_dt AND @current_dt 
          AND stp.[is_qualification] = 'Y'
          AND stp.[worker_inactive] = 'N'


    FINISHED:       

        SELECT [worker_customer_no],
               [worker_name],
               [worker_initials],
               [worker_inactive],
               [plan_no],
               [plan_status_no],
               [plan_status],
               [campaign_no],
               [campaign_description],
               1 AS [plan_counter]
        FROM [#final_table]

        IF OBJECT_ID('tempdb..#final_table') IS NOT NULL DROP TABLE [#final_table]
        IF OBJECT_ID('tempdb..#step_raw_data') IS NOT NULL DROP TABLE #step_raw_data

END
GO

EXECUTE [dbo].[LRP_ADV_QUALIFICATION_ACTIVITY_SUMMARY] @fiscal_year = 2021, @workers_str = '672463'
--EXECUTE [dbo].[LRP_ADV_QUALIFICATION_ACTIVITY_SUMMARY] @fiscal_year = 2021, @workers_str = '3504561,3657886,672463,817730'

/*
--FOR PARAMETER
SELECT [worker_customer_no],                --DATA
       [worker_name]                        --DISPLAY
FROM  [dbo].[LV_CAMPAIGN_ACTIVITY_WORKERS]  --TABLE
WHERE [inactive] = 'N'                      --WHERE CLAUSE
ORDER BY [worker_sort]                      --SORT
--"3504561","3657886","672463","817730"
*/


