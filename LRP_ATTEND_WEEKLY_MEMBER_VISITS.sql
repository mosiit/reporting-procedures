USE impresario;
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_ATTEND_WEEKLY_MEMBER_VISITS]') AND type in (N'P', N'PC')) BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_ATTEND_WEEKLY_MEMBER_VISITS] AS';
    EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LRP_ATTEND_WEEKLY_MEMBER_VISITS] TO impusers';
END;
GO

ALTER PROCEDURE [dbo].[LRP_ATTEND_WEEKLY_MEMBER_VISITS]
        @week_ending DATETIME = '9-8-2019'
AS BEGIN

    SET NOCOUNT ON;
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    
    /*  Procedure Variables  */

        DECLARE @fiscal_year INT = 0
        DECLARE @week_start_dt DATETIME, @week_end_dt DATETIME;
        DECLARE @month_start_dt DATETIME, @month_end_dt DATETIME;

        DECLARE @prev_fiscal_year INT = 0
        DECLARE @prev_week_start_dt DATETIME, @prev_week_end_dt DATETIME; 
        DECLARE @prev_month_start_dt DATETIME, @prev_month_end_dt DATETIME;
        
        DECLARE @exhibit_halls_no INT = 27

    /*  Temporary Table  */

        IF OBJECT_ID('tempdb..#member_visits') is not null DROP TABLE [#member_visits]

        CREATE TABLE [#member_visits] ([fiscal_year] VARCHAR(10) NOT NULL DEFAULT (''),
                                       [performance_dt] DATE NULL,
                                       [performance_day] VARCHAR(25) NOT NULL DEFAULT(''),
                                       [member_scans] INT NOT NULL DEFAULT (0),
                                       [exhibit_hall_attendance] INT NOT NULL DEFAULT (0),
                                       [exhibit_hall_show_and_go] INT NOT NULL DEFAULT (0),
                                       [exhibit_hall_total] INT NOT NULL DEFAULT (0),
                                       [exhibit_hall_attendance_with_members] INT NOT NULL DEFAULT(0),
                                       [percent_of_exhibit_hall_attendance] DECIMAL(18,4) NOT NULL DEFAULT(0.0),
                                       [closed_msg] VARCHAR(10) NOT NULL DEFAULT(''))

    /*  Check Parameters  */

        SELECT @week_ending = CAST(ISNULL(@week_ending,GETDATE()) AS DATE)
        
    /*  Get date values from LFT_ATTENDANCE_WEEK_DATES function  */
        
        SELECT @fiscal_year = [cur_fiscal_year], 
               @week_start_dt = [cur_week_start_dt], 
               @week_end_dt = [cur_week_end_dt], 
               @month_start_dt = [cur_month_start_dt], 
               @month_end_dt = [cur_month_end_dt], 
               @prev_fiscal_year = [prv_fiscal], 
               @prev_week_start_dt = [prv_week_start_dt], 
               @prev_week_end_dt = [prv_week_end_dt], 
               @prev_month_start_dt = [prv_month_start_dt], 
               @prev_month_end_dt = [prv_month_end_dt]
        FROM [dbo].[LFT_ATTENDANCE_WEEK_DATES] (@week_ending, 0, 'Y')

    /*  Get Exhibit Hall Attendance  */

        INSERT INTO [#member_visits] ([fiscal_year], [performance_dt], [performance_day], [member_scans], [exhibit_hall_attendance], 
                                      [exhibit_hall_show_and_go], [exhibit_hall_attendance_with_members])
        SELECT CASE WHEN TRY_CONVERT(DATE,[performance_date]) BETWEEN @week_start_dt AND @week_end_dt THEN 'FY ' + CAST(@fiscal_year AS VARCHAR(4))
                    WHEN TRY_CONVERT(DATE,[performance_date]) BETWEEN @prev_week_start_dt AND @prev_week_end_dt THEN 'FY ' + CAST(@prev_fiscal_year AS VARCHAR(4))
                    ELSE '' END,
                    TRY_CONVERT(DATE,[performance_date]),
                    LEFT(DATENAME(WEEKDAY,TRY_CONVERT(DATE,[performance_date])),3),
                    0,
                    SUM(sale_total),
                    0,
                    0
        FROM [dbo].[LT_HISTORY_TICKET]
        WHERE (TRY_CONVERT(DATE,[performance_date]) BETWEEN @week_start_dt and @week_end_dt
               OR TRY_CONVERT(DATE,[performance_date]) BETWEEN @prev_week_start_dt AND @prev_week_end_dt)
          AND [title_no] = @exhibit_halls_no  --[title_name] = 'Exhibit Halls'
        GROUP BY CASE WHEN TRY_CONVERT(DATE,[performance_date]) BETWEEN @week_start_dt AND @week_end_dt THEN 'FY ' + CAST(@fiscal_year AS VARCHAR(4))
                      WHEN TRY_CONVERT(DATE,[performance_date]) BETWEEN @prev_week_start_dt AND @prev_week_end_dt THEN 'FY ' + CAST(@prev_fiscal_year AS VARCHAR(4))
                      ELSE '' END,
                 TRY_CONVERT(DATE,[performance_date]), 
                 DATENAME(WEEKDAY,TRY_CONVERT(DATE,[performance_date]))

    /*  Get Membership Scan Visit Count  */

        INSERT INTO [#member_visits] ([fiscal_year], [performance_dt], [performance_day], [member_scans], [exhibit_hall_attendance], 
                                      [exhibit_hall_show_and_go], [exhibit_hall_attendance_with_members])
        SELECT CASE WHEN [history_dt] BETWEEN @week_start_dt AND @week_end_dt THEN 'FY ' + CAST(@fiscal_year AS VARCHAR(4))
                WHEN [history_dt] BETWEEN @prev_week_start_dt AND @prev_week_end_dt THEN 'FY ' + CAST(@prev_fiscal_year AS VARCHAR(4))
                ELSE '' END AS [fiscal_year],
                [history_dt] AS [performance_dt],
                LEFT(DATENAME(WEEKDAY,[history_dt]),3) AS [performance_day],
                SUM([visit_count]),
                0,
                0,
                0              
        FROM [dbo].[LT_HISTORY_VISIT_COUNT]
        WHERE ([history_dt] BETWEEN @week_start_dt and @week_end_dt
               OR [history_dt] BETWEEN @prev_week_start_dt AND @prev_week_end_dt)
          AND [attendance_type] = 'Membership Card Scan'
        GROUP BY CASE WHEN [history_dt] BETWEEN @week_start_dt AND @week_end_dt THEN 'FY ' + CAST(@fiscal_year AS VARCHAR(4))
                    WHEN [history_dt] BETWEEN @prev_week_start_dt AND @prev_week_end_dt THEN 'FY ' + CAST(@prev_fiscal_year AS VARCHAR(4))
                    ELSE '' END,
                    [history_dt],
                    LEFT(DATENAME(WEEKDAY,[history_dt]),3)
    
    /*  Get Show and Go and Show and Collected and GOBoston Visit Count  */

        INSERT INTO [#member_visits] ([fiscal_year], [performance_dt], [performance_day], [member_scans], [exhibit_hall_attendance], 
                                      [exhibit_hall_show_and_go], [exhibit_hall_attendance_with_members])
        SELECT CASE WHEN [history_dt] BETWEEN @week_start_dt AND @week_end_dt THEN 'FY ' + CAST(@fiscal_year AS VARCHAR(4))
                    WHEN [history_dt] BETWEEN @prev_week_start_dt AND @prev_week_end_dt THEN 'FY ' + CAST(@prev_fiscal_year AS VARCHAR(4))
                    ELSE '' END AS [fiscal_year],
                    [history_dt] AS [performance_dt],
                    LEFT(DATENAME(WEEKDAY,[history_dt]),3) AS [performance_day],
                    0,
                    0,
                    SUM([visit_count]),
                    0
        FROM [dbo].[LT_HISTORY_VISIT_COUNT]
        WHERE ([history_dt] BETWEEN @week_start_dt and @week_end_dt
               OR [history_dt] BETWEEN @prev_week_start_dt AND @prev_week_end_dt)
          AND [attendance_type] NOT IN ('Ticketed Events', 'Membership Card Scan')
        GROUP BY CASE WHEN [history_dt] BETWEEN @week_start_dt AND @week_end_dt THEN 'FY ' + CAST(@fiscal_year AS VARCHAR(4))
                    WHEN [history_dt] BETWEEN @prev_week_start_dt AND @prev_week_end_dt THEN 'FY ' + CAST(@prev_fiscal_year AS VARCHAR(4))
                    ELSE '' END,
                    [history_dt],
                    LEFT(DATENAME(WEEKDAY,[history_dt]),3)


    /*  Add any dates that asre missing because the museum was closed  */


        INSERT INTO [#member_visits] ([fiscal_year], [performance_dt], [performance_day], [member_scans], [exhibit_hall_attendance], [exhibit_hall_show_and_go],
                                      [exhibit_hall_total], [exhibit_hall_attendance_with_members], [percent_of_exhibit_hall_attendance], [closed_msg])
        SELECT 'FY ' + CAST(@fiscal_year AS VARCHAR(4)),
               CAST(idt.return_dt AS DATE), 
               LEFT(idt.day_of_week,3), 
               0,
               0,
               0,
               0,
               0,
               0,
               'Closed'
        FROM dbo.LFT_INDIVIDUAL_DATES(@week_start_dt,@week_end_dt) AS idt
             LEFT OUTER JOIN [#member_visits] AS vis ON vis.performance_dt = CAST(idt.return_dt AS DATE)
        WHERE vis.[fiscal_year] IS NULL 

                INSERT INTO [#member_visits] ([fiscal_year], [performance_dt], [performance_day], [member_scans], [exhibit_hall_attendance], [exhibit_hall_show_and_go],
                                      [exhibit_hall_total], [exhibit_hall_attendance_with_members], [percent_of_exhibit_hall_attendance], [closed_msg])
        SELECT 'FY ' + CAST(@prev_fiscal_year AS VARCHAR(4)),
               CAST(idt.return_dt AS DATE), 
               LEFT(idt.day_of_week,3), 
               0,
               0,
               0,
               0,
               0,
               0,
               'Closed'
        FROM dbo.LFT_INDIVIDUAL_DATES(@prev_week_start_dt,@prev_week_end_dt) AS idt
             LEFT OUTER JOIN [#member_visits] AS vis ON vis.performance_dt = CAST(idt.return_dt AS DATE)
        WHERE vis.[fiscal_year] IS NULL 
  
      

    FINISHED:

        /*  Aggregate values and Return Member Visitation Information  */
        
            SELECT [fiscal_year],
                   [performance_dt], 
                   [performance_day],
                   SUM([member_scans]) AS [member_scans],
                   SUM([exhibit_hall_attendance]) AS [exhibit_hall_attendance],
                   SUM([exhibit_hall_show_and_go]) AS [exhibit_hall_show_and_go], 
                   SUM([exhibit_hall_attendance]) +  SUM([exhibit_hall_show_and_go]) AS [exhibit_hall_total],
                   SUM([exhibit_hall_attendance]) +  SUM([exhibit_hall_show_and_go]) + SUM([member_scans]) AS [exhibit_hall_attendance_with_members],
                   0.0000 AS [percent_of_exhibit_hall_attendance],   --CHANGED TO A HARD-CODED ZERO - PERCENTAGE WILL BE CALCULATED IN THE REPORT FORMAT
                   ISNULL([closed_msg],'') AS [closed_msg]
            FROM [#member_visits]
            GROUP BY [fiscal_year], [performance_dt], [performance_day], ISNULL([closed_msg],'')
            
    DONE:

        IF OBJECT_ID('tempdb..#member_visits') is not null DROP TABLE [#member_visits]

END
GO

--EXECUTE [dbo].[LRP_ATTEND_WEEKLY_MEMBER_VISITS] '9-8-2019'        
      
