USE [impresario];
GO

SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
-- =============================================
-- Author:		Mark Sherwood
-- Description:	CRM Was looking for a way to pass multiple gift codes to the LRP_APPLY_GIFT_ORDER_LOCK procedure using separate parameters
-- instead of one long comma-delimited string.  This report was written for us by Tessitura and not wanting to adjust their code, I
-- created this mini-procedure that takes up 
-- Locks a gift to an order so that the entitlements associated with gift can be used in the order.  This is called from a ssrs report
-- LRP_APPLY_GIFT_ORDER_LOCK 1187784,'86F4-0334-8940,B169-0334-AB7C'

-- Updated: DSJ 6/2019 WO 99721, added 2 columns: 
-- =============================================
ALTER PROCEDURE [dbo].[LRP_APPLY_GIFT_ORDER_LOCK_MULTI]
    @oNumber INT = 0,
    @gCode_0 VARCHAR(50) = '',
    @gCode_1 VARCHAR(50) = '',
    @gCode_2 VARCHAR(50) = '',
    @gCode_3 VARCHAR(50) = '',
    @gCode_4 VARCHAR(50) = '',
    @gCode_5 VARCHAR(50) = '',
    @gCode_6 VARCHAR(50) = '',
    @gCode_7 VARCHAR(50) = '',
    @gCode_8 VARCHAR(50) = '',
    @gCode_9 VARCHAR(50) = ''
AS BEGIN

    /*  This will create a single list of gift codes  */

        DECLARE @new_gift_code VARCHAR(1000) = ''

    /*  Combine all the gift codes into one coma-delimited file  */

        SELECT @new_gift_code = @new_gift_code + CASE WHEN ISNULL(@gCode_0,'') <> '' THEN CASE WHEN @new_gift_code <> '' THEN ',' ELSE '' END + @gCode_0 ELSE '' END
        SELECT @new_gift_code = @new_gift_code + CASE WHEN ISNULL(@gCode_1,'') <> '' THEN CASE WHEN @new_gift_code <> '' THEN ',' ELSE '' END + @gCode_1 ELSE '' END
        SELECT @new_gift_code = @new_gift_code + CASE WHEN ISNULL(@gCode_2,'') <> '' THEN CASE WHEN @new_gift_code <> '' THEN ',' ELSE '' END + @gCode_2 ELSE '' END
        SELECT @new_gift_code = @new_gift_code + CASE WHEN ISNULL(@gCode_3,'') <> '' THEN CASE WHEN @new_gift_code <> '' THEN ',' ELSE '' END + @gCode_3 ELSE '' END
        SELECT @new_gift_code = @new_gift_code + CASE WHEN ISNULL(@gCode_4,'') <> '' THEN CASE WHEN @new_gift_code <> '' THEN ',' ELSE '' END + @gCode_4 ELSE '' END
        SELECT @new_gift_code = @new_gift_code + CASE WHEN ISNULL(@gCode_5,'') <> '' THEN CASE WHEN @new_gift_code <> '' THEN ',' ELSE '' END + @gCode_5 ELSE '' END
        SELECT @new_gift_code = @new_gift_code + CASE WHEN ISNULL(@gCode_6,'') <> '' THEN CASE WHEN @new_gift_code <> '' THEN ',' ELSE '' END + @gCode_6 ELSE '' END
        SELECT @new_gift_code = @new_gift_code + CASE WHEN ISNULL(@gCode_7,'') <> '' THEN CASE WHEN @new_gift_code <> '' THEN ',' ELSE '' END + @gCode_7 ELSE '' END
        SELECT @new_gift_code = @new_gift_code + CASE WHEN ISNULL(@gCode_8,'') <> '' THEN CASE WHEN @new_gift_code <> '' THEN ',' ELSE '' END + @gCode_8 ELSE '' END
        SELECT @new_gift_code = @new_gift_code + CASE WHEN ISNULL(@gCode_9,'') <> '' THEN CASE WHEN @new_gift_code <> '' THEN ',' ELSE '' END + @gCode_9 ELSE '' END

    /*  Call the original procedure  */

        EXECUTE [dbo].[LRP_APPLY_GIFT_ORDER_LOCK] 
                @order_no = @oNumber,
                @gift_code = @new_gift_code

END;
GO

GRANT EXECUTE ON [LRP_APPLY_GIFT_ORDER_LOCK_MULTI] TO [ImpUsers], [tessitura_app];
GO


