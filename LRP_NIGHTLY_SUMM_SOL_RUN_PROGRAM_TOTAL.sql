USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LRP_NIGHTLY_SUMM_SOL_RUN_PROGRAM_TOTAL]    Script Date: 2/8/2021 2:04:56 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[LRP_NIGHTLY_SUMM_SOL_RUN_PROGRAM_TOTAL]
	@section VARCHAR(300),
	@sortOrder INT,
	@runBy	VARCHAR(300),
	@fystart DATETIME,
	@fyend DATETIME
AS

BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	INSERT INTO LT_NIGHTLY_SUMMARY
		SELECT	t.customer_no,
				@section,
				ISNULL(SUM(c.cont_amt), 0),
				'',
				@sortOrder,
				GETDATE(),
				@runBy
		FROM	T_CONTRIBUTION AS c
		INNER JOIN T_CAMPAIGN AS g ON c.campaign_no = g.campaign_no
		INNER JOIN T_CUSTOMER AS t ON c.worker_customer_no = t.customer_no
		WHERE c.cont_type IN ( 'G', 'P' )
			  AND g.category = '51'
			  AND c.cont_amt > 0
			  AND c.worker_customer_no IS NOT NULL
			  AND CAST(c.cont_dt AS DATE) >= @fystart
			  AND CAST(c.cont_dt AS DATE) <= @fyend
		GROUP BY t.customer_no;

END



GO


