USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LRP_ORDER_ACK_INVOICE_DETAIL]    Script Date: 3/2/2018 9:33:04 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




/*-- =============================================
-- Author:		Tanya Hoffmann (TN)
-- Create date: 1/4/2017
-- Description:	Custom version of tp_order_ack that allows for additional items on MOS invoices
-- 5/18/2017 -- TAH added @email_ind for when this is run for email for mail2
--08/31/2017 -- TAH fixed problem where fees were doubling

 01/31/2018 -- TAH -- made the column description in #details larger from 255 to 500 to see if truncation error would go a way.
 02/08/2018 -- TAH -- to account for negative payment amounts (refunds)
 02/09/2018 -- TAH - to make check_name varchar(100)  instead of varchar(30)
 02/26/2018 -- TAH -- reverted refund amounts back to what it was as this was adding incorrectly.
-- exec  LRP_ORDER_ACK_INVOICE_DETAIL  @order_no = 426905 -- 429624
-- =============================================  */
ALTER PROCEDURE [dbo].[LRP_ORDER_ACK_INVOICE_DETAIL]
	-- Add the parameters for the stored procedure here
	(--@order_start_dt	datetime = '11/28/2016',
	--@order_end_dt	datetime = '12/28/2016',
	--@season_str	varchar(255) = null,
	--@perf_start_dt	datetime = null,
	--@perf_end_dt	datetime = null,
	--@mos_str	varchar(255) = '5',
	@reprint_ind	char(1) = 'Y',
	@order_no int--,
	--@list_no	int = null,
	--@mailing_dt 	datetime = null,
	--@mailing_type 	varchar(8) = null,
	--@signer 	int = null,
	--@label_ind 	char(1) = null,
	--@include_general_public char(1) = 'N',
	--@eaddress_type 	int = null,
	--@eaddress_purpose varchar(8) = null,
	--@eaddress_market_ind_no char(1) = null,
	--@report_type 	char(1) = 'A',		-- 'A' for ack, 'C' for confirmation, 'O' for single order, 'B' for billing
	--@p_order_no 	int = null,
	--@habo_flag	int = null,	-- 1 = yes, 2 = no, 3 = all; only used for confirmations
	--@exception_flag int = null,	-- 1 = yes, 2 = no, 3 = all; only used for confirmations
	--@ob_no 		int = 0,
	--@balance_type	int = 4,	-- default is all orders; 1=no payment, 2=fully paid, 3=partially paid, 5=any balance due
	, @email_ind char(1) = 'N')
with execute as 'dbo'
AS


BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	-- These parameters were not being used so I moved them here to make the SSRS section easier to develop
	DECLARE
	@season_str	varchar(255) = null,
	@perf_start_dt	datetime = null,
	@perf_end_dt	datetime = null,
	@list_no	int = null,
	@mailing_dt 	datetime = null,
	@mailing_type 	varchar(8) = null,
	@signer 	int = null,
	@label_ind 	char(1) = null,
	@include_general_public char(1) = 'N',
	@eaddress_type 	int = null,
	@eaddress_purpose varchar(8) = null,
	@eaddress_market_ind_no char(1) = null,
	@report_type 	char(1) = 'O',		-- 'A' for ack, 'C' for confirmation, 'O' for single order, 'B' for billing
	@p_order_no 	int = null,
	@habo_flag	int = null,	-- 1 = yes, 2 = no, 3 = all; only used for confirmations
	@exception_flag int = null,	-- 1 = yes, 2 = no, 3 = all; only used for confirmations
	@ob_no 		int = 0,
	@balance_type	int = 4	-- default is all orders; 1=no payment, 2=fully paid, 3=partially paid, 5=any balance due



    -- Table for results of  [TP_ORDER_ACK] procedure
	CREATE table #torder (
	customer_no int null,
	order_no int null,
	order_dt datetime null,
	habo_ind char(1) null,
	tot_ticket_amt  money null,
	tot_fee_amt money null,
	tot_contribution_amt money null,
	tot_paid_amt money null,
	ord_notes varchar(255) null,
	delivery varchar(30) null,
	esal1_desc varchar(55) null,
	esal2_desc varchar(55)null,
	lsal_desc varchar(55) null,
	business_title varchar(55) null,
	street1 varchar(64) null,
	street2 varchar(64) null,
	street3 varchar(64) null,
	city varchar(30) null,
	state varchar(20) null,
	postal_code varchar(10) null,
	country varchar(30) null,
	country_short varchar(3) null,
	day_phone varchar(30) null,
	eve_phone varchar(30) null,
	fax_phone varchar(30) null,
	const_str varchar(255) null,
	eaddress varchar(80) null,
	li_count int null,
	--0,	-- placeholder for current page_no
	--0,	-- placeholder for total pages for this order
	current_page_no int null,
	total_pages int null,
	custom_1 varchar(255) null,
	custom_2 varchar(255) null,
	custom_3 varchar(255) null,
	custom_4 varchar(255) null,
	custom_5 varchar(255) null,
	custom_6 varchar(255) null,
	custom_7 varchar(255) null,
	custom_8 varchar(255) null,
	custom_9 varchar(255) null,
	custom_0 varchar(255) null,
	initiator_no int null,
	initiator_name varchar(55) null, 
	initiator_sort_name varchar(55) null,
	initiator_esal1_desc varchar(55) null,
	initiator_esal2_desc varchar(55) null,
	initiator_lsal_desc varchar(55) null,
	initiator_business_title varchar(55) null,
	pkg_no int null,
	perf_no int null,
	perf_dt datetime null,
	perf_desc varchar(30) null,
	pkg_desc varchar(30) null,
	no_of_seats_requested int null,
	no_of_seats_assigned int null,
	price_zone varchar(30) null,
	total_li_amt money null,
	unseatable_reason varchar(30) null,							
	li_priority int null)





	insert #torder
	
	exec [TP_ORDER_ACK]
	--@order_start_dt	 = @order_start_dt,--'11/28/2016 12:01 am',--'10/13/2016 15:00 ',
	--@order_end_dt	 = @order_end_dt,--'11/30/2016 11:59 pm',-- '10/13/2016 16:00 ',
	--@season_str	 = @season_str,--null,
	--@perf_start_dt	 = @perf_start_dt,--null,
	--@perf_end_dt	 = @perf_end_dt, --null,
	--@mos_str	 = @mos_str, --'6',
	@reprint_ind	 = @reprint_ind, --'N',
	--@list_no	 =  @list_no, --null,
	--@mailing_dt 	 = @mailing_dt, --null,
	--@mailing_type 	 = @mailing_type, --null,
	--@signer 	 = @signer, --null,
	--@label_ind 	= @label_ind, --null,
	--@include_general_public  = @include_general_public, --'N',
	--@eaddress_type 	= @eaddress_type, --- null,
	--@eaddress_purpose  = @eaddress_purpose, --null,
	--@eaddress_market_ind_no  = @eaddress_market_ind_no, --null,
	@report_type 	 = 'O',		-- 'A' for ack, 'C' for confirmation, 'O' for single order, 'B' for billing
	@p_order_no 	 = @order_no, --null,
	@habo_flag	 = null,	-- 1 = yes, 2 = no, 3 = all; only used for confirmations
	@exception_flag  = null,	-- 1 = yes, 2 = no, 3 = all; only used for confirmations
	@ob_no 		 = 0,
	@balance_type	 = @balance_type -- 4		-- default is all orders; 1=no payment, 2=fully paid, 3=partially paid, 5=any balance due


	--select * from #torder

	-- Select Payment information

	SELECT o.order_no, pm.description pmt_method_desc, pm.pmt_type, tt.description as trn_type,  p.*
	into #payment
	FROM dbo.T_PAYMENT p
	JOIN dbo.T_TRANSACTION t on p.transaction_no = t.transaction_no and t.sequence_no = p.sequence_no
	JOIN dbo.TR_PAYMENT_METHOD pm on pm.id = p.pmt_method
	JOIN dbo.TR_TRANSACTION_TYPE tt on tt.id = t.trn_type
	JOIN (select distinct order_no from #torder) o on t.order_no = o.order_no
	order by o.order_no
	
-- Create temp table for results
			
		create table #details(
		item varchar(30) null,
		sort int null,
		order_no int null,
		item_date datetime null,
		perf_dt datetime null,
		description varchar(500) null,
		price_zone varchar(30) null,
		no_of_seats_assigned int null,
		amount money null,
		--pmt_no int null,
		--pmt_amt money null,
		--pmt_dt datetime null,
		--pmt_description varchar(50) null,
		check_no int null,
		check_name varchar(100) null,
		account_no varchar(30) null,
		card_expiry_dt datetime null,
		pmt_note varchar(1024) null)
		

		-- Get order info
		INSERT into #details(item, sort, order_no, item_date, perf_dt, description, price_zone, no_of_seats_assigned, amount)
		SELECT 
			'Order',
			10,
			o.order_no,
			o.order_dt,
			o.perf_dt,
			CASE when o2.mos = 6 then o.perf_desc
				 when o2.mos = 20 then o.perf_desc
					else  o.perf_desc + ' for ' + cast(no_of_seats_assigned as varchar(10)) + ' participants' end, -- don't show number of participants for Travelling MOS
			o.price_zone,
			o.no_of_seats_assigned,								
			o.total_li_amt
					
		FROM #torder o
		JOIN dbo.T_ORDER o2 on o.order_no = o2.order_no
		--left outer JOIN #payment p on o.order_no = p.order_no where p.pmt_type != 4 -- not invoice types
		--left outer JOIN dbo.LTR_INVOICE_PMT ip on ip.pmt_method = p.pmt_method
		--join dbo.T_ORDER o2 on o2.order_no = o.order_no

		-- Get Fee Info
		INSERT into #details (item, sort, order_no, item_date, description,  amount)
		select 
			'Fee',
			20,
			o.order_no, 
			o.order_dt,			
			b.description,
			amount = case when a.fee_override_amt > 0 then fee_override_amt else a.fee_amt end			
			from T_SLI_FEE a
			join T_FEE b on a.fee_no = b.fee_no
			--join #torder o on o.order_no = a.order_no
			join ( select order_no = MAX(order_no), order_dt from #torder group by order_no, order_dt) o on a.order_no = o.order_no




		-- Get payment info	
		INSERT #details (item, sort, order_no, amount, item_date, description, check_no, check_name, account_no, card_expiry_dt, pmt_note)
		select 
			case when p.pmt_method_desc like '%scholarship%' then 'Scholarship' 
				 when trn_type like '%refund%' then 'Refund'
				 when p.pmt_method_desc like '%refund%' then 'Refund' else 'Payment' end,
			30,
			p.order_no,				
			--p.payment_no,
			--pmt_amt = case when pmt_amt < 0  then p.pmt_amt else p.pmt_amt * -1 end,
			p.pmt_amt * -1,
			p.pmt_dt,
			 --description = CASE WHEN check_no is null then coalesce(r.description, p.pmt_method_desc) 
				--else coalesce(r.description, p.pmt_method_desc) + ' Check Num:' + isnull(cast(check_no as varchar(10)), '') + ' :' + isnull(check_name, '') end,
			
			description = CASE when check_no is not null then coalesce(r.description, p.pmt_method_desc) + ', Check Num:' + isnull(cast(check_no as varchar(10)), '') + ', ' + isnull(check_name, '')
							   when isnull(account_no, '') <> ''  then 	coalesce(r.description, p.pmt_method_desc) + ', Account No:' + isnull(cast(account_no as varchar(10)), '') + ', ' + isnull(check_name, '')
							  -- when p.pmt_method_desc like '%scholarship' then cp.cust_pname
							   else coalesce(r.description, p.pmt_method_desc)  end,
			
			check_no,
			check_name,
			account_no,
			card_expiry_dt	,
			p.notes											
		FROM #payment p-- on o.order_no = p.order_no and p.pmt_type != 4 -- Invoice
		left outer join dbo.LTR_INVOICE_PMT r on r.pmt_method = p.pmt_method
		JOIN (select distinct order_no from #torder) o on  o.order_no = p.order_no and p.pmt_type != 4 -- Invoice
		--LEFT OUTER JOIN dbo.TX_CUST_PROGRAM cp on cp.customer_no = p.check_no and cp.program_no = 8 --Invoice Recognition - the check num = customer number
		order by o.order_no

		-- Update scholarship payment descriptions
		
		update a
		set description = b.cust_pname
		from #details a
		join dbo.TX_CUST_PROGRAM b on a.check_no = b.customer_no
		where a.description like '%scholar%'



		
		if @email_ind = 'N'
		begin
			select * from #details order by order_no, sort, item_date
		end

		if @email_ind = 'Y'
		begin
			--truncate table LTW_ORDER_ACK_INVOICE_EMAIL_DETAIL
			insert into LTW_ORDER_ACK_INVOICE_EMAIL_DETAIL
			select * from #details 
		end

		
	

	
END






GO


