USE [impresario]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[LRP_BOARD_COMMITTEE_ASSIGNMENTS]
(
	@customer_no INT = NULL, 
	@committee_status_filter VARCHAR(50) = 'Active'-- All or Active
)
AS

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

-- This can be run for all Board Members or just one at a time.

SELECT mem.individual_customer_no, description AS BoardType, cust.display_name AS name, cust.sort_name, emp.lname AS employer,
comm.CommitteeName,
	comm.Title,
	comm.StartDate,
	comm.EndDate,
	comm.CommitteeStatus,
	comm.MeetingAttended,
	comm.MeetingDidNotAttend,
	comm.MeetingsInvited
FROM lv_board_members  mem
INNER JOIN	dbo.FT_CONSTITUENT_DISPLAY_NAME() AS cust 
	ON mem.individual_customer_no = cust.customer_no
OUTER APPLY [dbo].[LFT_BOARD_GetActiveCommitteeData](mem.individual_customer_no, @committee_status_filter) comm
LEFT JOIN dbo.T_AFFILIATION empaff
	ON mem.individual_customer_no = empaff.individual_customer_no  
	AND empaff.affiliation_type_id = 10007 -- Employee
LEFT JOIN dbo.T_CUSTOMER emp
	ON emp.customer_no = empaff.group_customer_no
WHERE mem.description IN ('Active Overseer', 'Active Trustee', 'Trustee Emeriti', 'Overseer Emeriti', 'Life Trustee','Active Museum Advisor','Museum Advisor Emeriti')
	AND mem.individual_customer_no = ISNULL(@customer_no, mem.individual_customer_no)
ORDER BY cust.sort_name, comm.CommitteeStatus, comm.CommitteeName
-- 415



