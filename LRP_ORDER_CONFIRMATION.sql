ALTER PROCEDURE [LRP_ORDER_CONFIRMATION]
        @order_no INT = NULL
AS BEGIN;

    DECLARE @order_table TABLE ([order_no] INT,
                                [customer_no] INT,
                                [display_name] VARCHAR(100),
                                [MOS] INT,
                                [mode_of_sale_name] VARCHAR(30),
                                [order_dt] DATETIME,
                                [transaction_no] INT,
                                [tot_due_amt] DECIMAL (18,2),
                                [tot_paid_amt] DECIMAL (18,2),
                                [payment_method] VARCHAR(30),
                                [sli_no] INT,
                                [due_amt] DECIMAL(18,2),
                                [paid_amt] DECIMAL(18,2),
                                [price_type] INT,
                                [price_type_name] VARCHAR(30),
                                [price_type_name_display] VARCHAR(30),
                                [ticket_no] INT,
                                [create_dt] DATETIME,
                                [comp_code] INT,
                                [comp_code_name] VARCHAR(30),
                                [sli_status] INT,
                                [sli_status_name] VARCHAR(30),
                                [perf_no] INT,
                                [zone_no] INT,
                                [performance_date] CHAR(10),
                                [performance_time] VARCHAR(20),
                                [performance_time_display] VARCHAR(20),
                                [title_name] VARCHAR(30),
                                [production_name] VARCHAR(30),
                                [production_name_long] VARCHAR(150),
                                [performance_facility_no] INT,
                                [performance_facility_name] VARCHAR(30),
                                [location] VARCHAR(30),
                                [recipient_no] INT,
                                [rule_id] INT,
                                [rule_ind] VARCHAR(10),     
                                [original_price_type] INT,
                                [original_price_type_name] VARCHAR(30),
                                [qr_code_url] VARCHAR(255));


    IF ISNULL(@order_no,0) <= 0
        SELECT @order_no = (SELECT TOP (1) [order_no] 
                            FROM [Admin].[dbo].[tessitura_qr_codes]
                            WHERE [confirmation_run] = 'N'
                            ORDER BY [order_create_dt]);

    IF ISNULL(@order_no,0) <= 0 GOTO DONE;
    
    WITH [CTE_ORDER_PAYMENT] ([transaction_no], [sequence_no], [order_no], [pmt_dt], [pmt_method], [payment_method]) AS 
    (
        SELECT TOP (1) trx.[transaction_no], 
                       trx.[sequence_no], 
                       trx.[order_no], 
                       pay.[pmt_dt], 
                       pay.[pmt_method], 
                       mth.[description]
        FROM [dbo].[T_TRANSACTION] AS trx
             LEFT OUTER JOIN [dbo].[T_PAYMENT] AS pay ON pay.[transaction_no] = trx.[transaction_no] AND pay.[sequence_no] = trx.[sequence_no]
             LEFT OUTER JOIN [dbo].[TR_PAYMENT_METHOD] AS mth ON mth.[id] = pay.[pmt_method]
        WHERE trx.[order_no] = @order_no
        ORDER BY trx.[order_no], pay.[pmt_dt] DESC, pay.[pmt_method] DESC
    )
    INSERT INTO @order_table ([order_no],[customer_no],[display_name],[MOS],[mode_of_sale_name],[order_dt],[transaction_no],[tot_due_amt],[tot_paid_amt],[payment_method],
                              [sli_no],[due_amt],[paid_amt],[price_type],[price_type_name],[price_type_name_display],[ticket_no],[create_dt],[comp_code],[comp_code_name],
                              [sli_status],[sli_status_name],[perf_no],[zone_no],[performance_date],[performance_time],[performance_time_display],[title_name],[production_name],
                              [production_name_long],[performance_facility_no],[performance_facility_name],[location],[recipient_no],[rule_id],[rule_ind],[original_price_type],
                              [original_price_type_name],[qr_code_url])
            SELECT ord.[order_no],
                   ord.[customer_no],
                   nam.[display_name],
                   ord.[MOS] AS [mode_of_sale],
                   mos.[description] AS [mode_of_sale_name],
                   ord.[order_dt],
                   ord.[transaction_no],
                   ord.[tot_due_amt],
                   ord.[tot_paid_amt],
                   cte.[payment_method],
                   sli.[sli_no],
                   sli.[due_amt],
                   sli.[paid_amt],
                   sli.[price_type],
                   pri.[description] AS [price_type_name],
                   CASE WHEN pri.[description] LIKE '%Adult%' THEN 'ADULT'
                        WHEN pri.[description] LIKE '%Child%' THEN 'CHILD'
                        WHEN pri.[description] LIKE '%Senior%' THEN 'SENIOR'
                        WHEN pri.[description] LIKE '%Pass%' THEN 'PASS'
                        ELSE UPPER(pri.[description]) END AS [price_type_name_display],
                   sli.[ticket_no],
                   sli.[create_dt],
                   ISNULL(sli.[comp_code],0) AS [comp_code],
                   ISNULL(cmp.[description],'') AS [comp_code_name],
                   sli.[sli_status],
                   sta.[description] AS [sli_status_name],
                   sli.[perf_no],
                   sli.[zone_no],
                   prf.[performance_date],
                   prf.[performance_time],
                   prf.[performance_time_display],
                   prf.[title_name],
                   prf.[production_name],
                   prf.[production_name_long],
                   prf.[performance_facility_no],
                   prf.[performance_facility_name],
                   the.[description] AS [location],
                   ISNULL(sli.[recipient_no],0) AS [recipient_no],
                   ISNULL(sli.[rule_id],0) AS [rule_id],
                   ISNULL(sli.[rule_ind],'') AS [rule_ind],
                   sli.[original_price_type],
                   ori.[description] AS [original_price_type_name],
                   'https://live-proxy.mos.org/latest/latest/tessitura/moslive/generateqr?type=&qrcode=sli_no_' + CAST(sli.[sli_no] AS VARCHAR(20)) + 'price_category_' + CAST(sli.[sli_no] AS VARCHAR(20)) AS [qr_code_url]
            FROM [dbo].[T_ORDER] AS ord
                 INNER JOIN [dbo].[TR_MOS] AS mos ON mos.[id] = ord.[MOS]
                 INNER JOIN [dbo].[FT_CONSTITUENT_DISPLAY_NAME]() AS nam ON nam.[customer_no] = ord.[customer_no]
                 INNER JOIN [dbo].[T_SUB_LINEITEM] AS sli ON sli.[order_no] = ord.[order_no]
                 INNER JOIN [dbo].[TR_PRICE_TYPE] AS pri ON pri.[id] = sli.[price_type]
                 LEFT OUTER JOIN [dbo].[TR_COMP_CODE] AS cmp ON cmp.[id] = sli.[comp_code]
                 INNER JOIN [dbo].[TR_SLI_STATUS] AS sta ON sta.[id] = sli.[sli_status]
                 LEFT OUTER JOIN [dbo].[TR_PRICE_TYPE] AS ori ON ori.[id] = sli.[original_price_type]
                 LEFT OUTER JOIN [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] AS prf ON prf.[performance_no] = sli.[perf_no] AND prf.[performance_zone] = sli.[zone_no]
                 LEFT OUTER JOIN [dbo].[T_FACILITY] AS fac ON fac.[facil_no] = prf.[performance_facility_no]
                 LEFT OUTER JOIN [dbo].[TR_THEATER] AS the ON the.[id] = fac.[th_no]
                 LEFT OUTER JOIN [CTE_ORDER_PAYMENT] AS cte ON cte.[order_no] = ord.[order_no]
            WHERE ord.[order_no] = @order_no


    FINISHED:

        UPDATE [Admin].[dbo].[tessitura_qr_codes]
        SET [confirmation_run] = 'Y'
        WHERE [order_no] = @order_no

         SELECT [order_no],
                [customer_no],
                [display_name],
                [MOS],
                [mode_of_sale_name],
                [order_dt],
                [transaction_no],
                [tot_due_amt],
                [tot_paid_amt],
                [payment_method],
                [sli_no],
                [due_amt],
                [paid_amt],
                [price_type],
                [price_type_name],
                [price_type_name_display],
                [ticket_no],
                [create_dt],
                [comp_code],
                [comp_code_name],
                [sli_status],
                [sli_status_name],
                [perf_no],
                [zone_no],
                [performance_date],
                [performance_time],
                [performance_time_display],
                [title_name],
                [production_name],
                [production_name_long],
                [performance_facility_no],
                [performance_facility_name],
                [location],
                [recipient_no],
                [rule_id],
                [rule_ind],
                [original_price_type],
                [original_price_type_name],
                [qr_code_url]
        FROM @order_table
        
    DONE:

END
GO

--EXECUTE [dbo].[LRP_ORDER_CONFIRMATION] @order_no = 2282068
--EXECUTE [dbo].[LRP_ORDER_CONFIRMATION] @order_no = 2282069
EXECUTE [dbo].[LRP_ORDER_CONFIRMATION]

--SELECT * FROM [dbo].[T_SUB_LINEITEM] WHERE [order_no] = 2282068
--SELECT * FROM [dbo].[T_FACILITY] WHERE [facil_no] = 64
--SELECT * FROM [dbo].[TR_THEATER] WHERE [id] = 6

--UPDATE [Admin].[dbo].[tessitura_qr_codes] SET [confirmation_run] = 'N' WHERE [order_no] IN (2282068,2282069)  PRINT @@ROWCOUNT
--SELECT * FROM [Admin].[dbo].[tessitura_qr_codes] WHERE [order_no] IN (2282068,2282069)
