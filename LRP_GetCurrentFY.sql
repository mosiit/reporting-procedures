USE [impresario]
GO 

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LRP_GetCurrentFY]
(
	@fyear INT OUTPUT,
	@startDt DATETIME OUTPUT,
	@endDt DATETIME OUTPUT
)
AS

SELECT @fyear = fyear
FROM dbo.TR_BATCH_Period
WHERE GETDATE() BETWEEN start_dt AND end_dt

SELECT @startDt = MIN(start_dt), @endDt = MAX(end_dt)
FROM TR_BATCH_PERIOD 
WHERE inactive = 'n' AND fyear = @fyear

GO

