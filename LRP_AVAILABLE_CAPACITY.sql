USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_AVAILABLE_CAPACITY]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LRP_AVAILABLE_CAPACITY]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LRP_AVAILABLE_CAPACITY]
        @report_start_dt datetime = null,
        @report_end_dt datetime = null,
        @include_titles VARCHAR(4000) = NULL,
        @include_thrill_ride char(1) = 'N',     --@include_thrill_ride originally used to include thrill ride or not - Use has changed but parameter name did not
        @sold_percent_threshold INT = 0         --Now parameter refers to whether or not the two Thrill Ride capsules should be reported as separate venues
AS BEGIN

    SET NOCOUNT ON;
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    SET ANSI_WARNINGS OFF;

    /*  02/17/2018: @include_titles parameter added to allow users to choose the venues they want to report on
                    @include_thrill_ride parameter repurposed to mean 'Separate Thrill Ride Capsules'  If Thrill Ride is
                    included in the report and this is set to Y, the Thrill Ride 360 title will be separated out into
                    two venues called Thrill Ride Capsule A and Thrill Ride Capsule B  */
    
    /*  Variable declaration and initialization (where necessary)  */

        DECLARE @title_table TABLE ([title_no] INT NOT NULL DEFAULT (0))
    
        DECLARE @report_run_dt DATETIME = getdate()

    /*  Check Parameters */

        --If Null Values passed to both date parameters, run for today
        SELECT @report_start_dt = IsNull(@report_start_dt, getdate())
        SELECT @report_end_dt = IsNull(@report_end_dt, @report_start_dt)

        SELECT @include_titles = ISNULL(@include_titles,'')
        SELECT @include_thrill_ride = IsNull(@include_thrill_ride, 'N')
        
        --If any other letter besides Y for yes is passed to procedure, assume N for no
        IF @include_thrill_ride <> 'Y' SELECT @include_thrill_ride = 'N'

        IF ISNULL(@sold_percent_threshold,0) <= 0
            SELECT @sold_percent_threshold = 0
        ELSE IF ISNULL(@sold_percent_threshold, 0) > 100
            SELECT @sold_percent_threshold = 100

        --Make sure start date starts at 00:00:00 and end date ends at 23:59:59.997
        SELECT @report_start_dt = convert(char(10),@report_start_dt,111) + ' 00:00:00',
               @report_end_dt = convert(char(10),@report_end_dt,111) + ' 23:59:59.997'

        --Parse out the title list (if any)
        IF ISNULL(@include_titles,'') = ''
            INSERT INTO @title_table ([title_no])
            SELECT ttl.title_no
            FROM [dbo].[T_TITLE] AS ttl
                 LEFT OUTER JOIN [dbo].[TX_INV_CONTENT] AS con ON con.[inv_no] = ttl.[title_no] AND con.[content_type] = 36 AND con.[value] <> 'N'
        ELSE
            INSERT INTO @title_table ([title_no])
            SELECT CAST([Element] AS INT) 
            FROM dbo.[FT_SPLIT_LIST] (REPLACE(@include_titles,'"',''),',')

    /*  Create work table to store the raw data  */

        IF OBJECT_ID('tempdb..#work_table') IS NOT NULL DROP TABLE [#work_table]

        CREATE TABLE [#work_table] ([title_no] INT, 
                                    [title_name] varchar(30), 
                                    [perf_no] int, [perf_code] varchar(10), 
                                    [perf_dt] datetime, [perf_day] varchar(30), 
                                    [perf_date] char(10), 
                                    [perf_time] char(8), 
                                    [prod_name] varchar(30), 
                                    [prod_name_long] varchar(150), 
                                    [zone_map_no] int, 
                                    [zone_map_name] varchar(30), 
                                    [zone_no] int,
                                    [total_capacity] int, 
                                    [held_seats] int, 
                                    [soft_capacity] int, 
                                    [tix_sold] int, 
                                    [tix_avail] int, 
                                    [inactive] char(1))
                                      
        CREATE CLUSTERED INDEX [ix_work_table_title_name] ON #work_table ([title_name] ASC) ON [PRIMARY]
        CREATE NONCLUSTERED INDEX [ix_work_table_prod_name] ON #work_table ([prod_name] ASC) ON [PRIMARY]
        CREATE NONCLUSTERED INDEX [ix_work_table_prod_name_long] ON #work_table ([prod_name_long] ASC) ON [PRIMARY]
        CREATE NONCLUSTERED INDEX [ix_work_table_perf_no_perf_time] ON #work_table ([perf_no] ASC, [perf_time] ASC) ON [PRIMARY]
        CREATE NONCLUSTERED INDEX [ix_work_table_perf_day] ON #work_table ([perf_day] ASC) ON [PRIMARY]
        CREATE NONCLUSTERED INDEX [ix_work_table_zone_no] ON #work_table ([zone_no] ASC) ON [PRIMARY]

    /*  Create results table to store the final data  */

        IF OBJECT_ID('tempdb..#results_table') IS NOT NULL DROP TABLE [#results_table]

        CREATE TABLE [#results_table] ([report_run_dt] datetime, 
                                       [report_message] varchar(100), 
                                       [perf_no] INT, 
                                       [title_no] INT, 
                                       [title_name] varchar(30), 
                                       [prod_name] varchar(200), 
                                       [perf_date] varchar(10), 
                                       [perf_time] varchar(8), 
                                       [title_order] int, 
                                       [rule_message] VARCHAR(8000), 
                                       [hard_capacity] int, 
                                       [held_tickets] INT, 
                                       [soft_capacity] int, 
                                       [tix_sold] int, 
                                       [percent_sold] decimal(18,2), 
                                       [tickets_available] int, 
                                       [record_count] int)

        CREATE CLUSTERED INDEX [ix_results_table_title_name] ON #results_table ([title_name] ASC) ON [PRIMARY]
        CREATE NONCLUSTERED INDEX [ix_results_table_prod_name] ON #results_table ([prod_name] ASC) ON [PRIMARY]
        CREATE NONCLUSTERED INDEX [ix_results_table_perf_no] ON #results_table ([perf_no] ASC) ON [PRIMARY]
        CREATE NONCLUSTERED INDEX [ix_results_table_perf_date] ON #results_table ([perf_date] ASC) ON [PRIMARY]
        CREATE NONCLUSTERED INDEX [ix_results_table_perf_time] ON #results_table ([perf_time] ASC) ON [PRIMARY]
        
    /*  Insert raw data into the work table  */
 
        INSERT INTO #work_table ([title_no], [title_name], [perf_no], [perf_code], [perf_dt], [perf_day], [perf_date], [perf_time], [prod_name], [prod_name_long], [zone_map_no], [zone_map_name], [zone_no],
                                 [total_capacity], [held_seats], [soft_capacity], [tix_sold], [tix_avail])
        SELECT prf.[title_no], 
               prf.[title_name], 
               prf.[performance_no], 
               prf.[performance_code], 
               prf.[performance_dt], 
               DATENAME(WEEKDAY,prf.[performance_dt]), 
               prf.[performance_date], 
               prf.[performance_time], 
               prf.[production_name], 
               prf.[production_name_long], 
               prf.[performance_zone_map], 
               prf.[performance_zone_map_name], 
               prf.[performance_zone],
               [dbo].[LF_GetCapacity] (prf.[performance_no], prf.[performance_zone], 'total'),
               [dbo].[LF_GetCapacity] (prf.[performance_no], prf.[performance_zone], 'held'),
               [dbo].[LF_GetCapacity] (prf.[performance_no], prf.[performance_zone], 'capacity'),
               [dbo].[LF_GetCapacity] (prf.[performance_no], prf.[performance_zone], 'sold'),
               [dbo].[LF_GetCapacity] (prf.[performance_no], prf.[performance_zone], 'available')
        FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] AS prf
             INNER JOIN @title_table AS ttl ON ttl.[title_no] = prf.[title_no]
        WHERE PRF.[performance_dt] BETWEEN @report_start_dt AND @report_end_dt

    /*  If Thrill Ride is included, Separate out into Capsule A and Capsule B  */
    
        IF @include_thrill_ride = 'Y' BEGIN

            UPDATE [#work_table] 
            SET title_name = 'Thrill Ride Capsule A' 
            WHERE [title_name] = 'Thrill Ride 360' AND RIGHT(perf_code,1) = 'A'
            
            UPDATE [#work_table] 
            SET title_name = 'Thrill Ride Capsule B' 
            WHERE [title_name] = 'Thrill Ride 360' AND RIGHT(perf_code,1) = 'B'

        END

    /*  Delete any inactive performances that might have slipped into the list  */

        DELETE FROM #work_table WHERE inactive = 'Y'

    /*  Aggregate information and insert into the results table  */

        INSERT INTO [#results_table] ([report_run_dt],[report_message],[perf_no],[title_no],[title_name],[prod_name],[perf_date],[perf_time],
                                      [title_order],[rule_message],[hard_capacity],[held_tickets],[soft_capacity],[tix_sold],[percent_sold],
                                      [tickets_available],[record_count])
        SELECT @report_run_dt, 
               '', 
               [perf_no], 
               [title_no], 
               [title_name], 
               [prod_name], 
               [perf_date], 
               [perf_time], 
               0, '', 
               SUM([total_capacity]), 
               SUM([held_seats]), 
               SUM([soft_capacity]), 
               SUM([tix_sold]),  
               CASE WHEN sum([soft_capacity]) = 0 THEN 0.00 
                    ELSE (sum([tix_sold]) / sum([soft_capacity])) END,
               (sum([soft_capacity]) - sum([tix_sold])), count([prod_name])
        FROM [#work_table] 
        GROUP BY [perf_no], [title_no], [title_name], [prod_name], [perf_date], [perf_time]
    
    /*  Delete buyouts from the results table  */

        DELETE FROM #results_table WHERE prod_name like '%Buyout%'
        
    /*  CHECK FROM PRICING RULE MESSAGES  */

        UPDATE [#results_table] SET [rule_message] = [dbo].[LF_GetPricingRuleMessage] (perf_no)
        WHERE title_name = 'Exhibit Halls'


    /*  Assign each main Title a primary sequential number in the order we want them to appear on the report.
        Titles not specifically listed will be assigned 99 and sort alphabetically at the end of the report.  */

        UPDATE [#results_table] SET [title_order] = 1 WHERE [title_name] = 'Exhibit Halls'
        UPDATE [#results_table] SET [title_order] = 2 WHERE [title_name] = 'Mugar Omni Theater'
        UPDATE [#results_table] SET [title_order] = 3 WHERE [title_name] = 'Hayden Planetarium'
        UPDATE [#results_table] SET [title_order] = 4 WHERE [title_name] = '4-D Theater'
        UPDATE [#results_table] SET [title_order] = 5 WHERE [title_name] = 'Butterfly Garden'
        UPDATE [#results_table] SET [title_order] = 6 WHERE [title_name] like 'Thrill Ride%'
        UPDATE [#results_table] SET [title_order] = 7 WHERE [title_name] = 'School Lunch'
        UPDATE [#results_table] SET [title_order] = 8 WHERE [title_name] = 'Cahners Theater'
        UPDATE [#results_table] SET [title_order] = 9 WHERE [title_name] = 'Current Science & Technology'
        UPDATE [#results_table] SET [title_order] = 10 WHERE [title_name] = 'Adult Offerings'
        UPDATE [#results_table] SET [title_order] = 99 WHERE [title_order] = 0

        UPDATE [#results_table] 
        SET [percent_sold] = CASE WHEN [soft_capacity] = 0 THEN 0
                                  ELSE ((CAST([tix_sold] AS DECIMAL(18,4)) / CAST([soft_capacity] AS DECIMAL(18,4))) * 100) END
    
        IF @sold_percent_threshold > 0
            DELETE FROM [#results_table]
            WHERE ISNULL([percent_sold],0) < @sold_percent_threshold

    DONE:

        /*  If no records exist in the results table, add a single record with a "No Records Found Message  */

            IF not exists (SELECT * FROM [#results_table]) 
                INSERT INTO [#results_table] ([report_run_dt],[report_message],[perf_no],[title_no],[title_name],[prod_name],[perf_date],[perf_time],
                                              [title_order],[rule_message],[hard_capacity],[held_tickets],[soft_capacity],[tix_sold],[percent_sold],
                                              [tickets_available],[record_count])
                VALUES (@report_run_dt, 'No records found for criteria entered.', 0, '', '', '', '', '', '', 0, '', 0, 0, 0, 0, 0.0, 0)

        /*  Final selection from the results table to be displayed on the report  */

            SELECT [report_run_dt], 
                   [report_message], 
                   [perf_date], 
                   [title_no], 
                   [title_name],  
                   [perf_time], 
                   [prod_name], 
                   [title_order], 
                   ISNULL([hard_capacity],0) AS [hard_capacity], 
                   ISNULL([held_tickets],0) AS [held_tickets], 
                   ISNULL([soft_capacity],0) as [total_capacity], 
                   ISNULL([tix_sold],0) AS [tix_sold], 
                   ISNULL([percent_sold],0) AS [percent_sold], 
                   ISNULL([tickets_available],0) AS [tickets_available], 
                   [record_count], 
                   [rule_message]
            FROM [#results_table] 
            ORDER BY [perf_date], [title_order], [title_name], [perf_time], [prod_name]

    CLEAN_UP:

        /*  Delete the work and results tables after we are done using them  */

            IF OBJECT_ID('tempdb..#results_table') IS NOT NULL DROP TABLE [#results_table]

            IF OBJECT_ID('tempdb..#work_table') IS NOT NULL DROP TABLE [#work_table]

END
GO

GRANT EXECUTE ON [dbo].[LRP_AVAILABLE_CAPACITY] TO impusers
GO

--EXECUTE [dbo].[LRP_AVAILABLE_CAPACITY] @report_start_dt = '3-1-2020', @report_end_dt = '4-30-2020', @include_titles = '', @include_thrill_ride = 'N', @sold_percent_threshold = 30

