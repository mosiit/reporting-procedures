USE [impresario]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[LRP_ADV_REVENUE_RPT]
(
	@cont_start_dt DATETIME,
	@cont_end_dt DATETIME,
	@GIK BIT = 0 -- 0 is default, which means pull everything. 1 means only pull GIK 
)
AS

--EXEC [LRP_ADV_REVENUE_RPT] @includePreviousFY  = 'Y', @cont_start_dt = '7/1/2017', @cont_end_dt = '5/21/2018', @GIK = 1

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
SET NOCOUNT ON 

-- The @cont_start_dt and @cond_end_dt MUST be in the same FY. If not, return 0 rows. 

DECLARE @startFY VARCHAR(4),
	@endFY VARCHAR(4) 

SELECT @startFY = dbo.LF_GetFiscalYear(@cont_start_dt), @endFY = dbo.LF_GetFiscalYear(@cont_end_dt)

IF @startFY <> @endFY
RETURN;

SET @startFY = '%' + RIGHT(@startFY,2);

WITH BU_Goals
AS
(
	SELECT Business_Unit, SUM(Unrestricted_Goal) AS BU_Unrestricted_Goal, SUM(Restricted_Goal) AS BU_Restricted_Goal
	FROM dbo.LTR_CAMPAIGN_BUSINESS_UNIT lcbu
	INNER JOIN dbo.T_CAMPAIGN c
	ON c.campaign_no = lcbu.campaign_no
	WHERE c.description LIKE @startFY
	GROUP BY Business_Unit
)
SELECT bus.Business_Unit, ISNULL(bugoals.BU_Restricted_Goal, 0.00) + ISNULL(bugoals.BU_Unrestricted_Goal,0.00) AS goal_amt, 
	ISNULL(bugoals.BU_Restricted_Goal, 0.00) AS BU_Restricted_Goal,
	ISNULL(bugoals.BU_Unrestricted_Goal,0.00) AS BU_Unrestricted_Goal,
	cat.description AS campaignCat, cont.ref_no, cont.cont_dt, cont.cont_amt, det.acct_goal_no, det.acct_grp_no,
	CASE 
		WHEN det.acct_goal_no = 3 OR det.acct_grp_no = 10 THEN cont.cont_amt
		ELSE 0
	END AS unrestricted,
	CASE 
		WHEN det.acct_goal_no <> 3 AND det.acct_grp_no <> 10 THEN cont.cont_amt
		ELSE 0
	END AS restricted,
	ROW_NUMBER() OVER (PARTITION BY bus.Business_Unit ORDER BY bus.Business_Unit ASC) AS BU_rnk
FROM 
	dbo.T_CONTRIBUTION cont
INNER JOIN dbo.T_CAMPAIGN cam
	ON cam.campaign_no = cont.campaign_no
INNER JOIN dbo.TR_CAMPAIGN_CATEGORY cat
	ON cam.category = cat.id
	AND cat.id <> 24 -- Skip
INNER JOIN dbo.LTR_FUND_DETAIL det
	ON det.fund_no = cont.fund_no
LEFT JOIN dbo.LTR_CAMPAIGN_BUSINESS_UNIT bus -- Note: the goals here are by campaign. In the report, the goals are summed over Business Unit. 
	ON bus.campaign_no = cam.campaign_no
LEFT JOIN BU_Goals bugoals
	ON ISNULL(bugoals.Business_Unit,'') = ISNULL(bus.Business_Unit, '')
WHERE cont.cont_dt BETWEEN @cont_start_dt AND @cont_end_dt
	AND cont.cont_amt <> 0.00
	AND cont.customer_no <> 2653093 
	AND 
	(
		(@GIK = 0 AND bus.Business_Unit = bus.Business_Unit) -- all records
		OR
		(@GIK = 1 AND bus.Business_Unit = 'Gifts In Kind') -- only GIK
	)

GO
