USE [impresario]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[LRP_GetChartOfAccounts]
AS

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

SELECT DISTINCT
	cc.description "cmcategory",
	cc.id "cmcatid",
	f.fund_no,
	f.inactive "fund_inactive",
	f.description "fund_description",
	f.restricted_income_gl_no,
	f.nonrestricted_income_gl_no,
	f.future_rec_gl_no,
	f.current_rec_gl_no,
	f.written_off_gl_no,
	ag1.description "acctgoal",
	ag2.description "acctgrp",
	d.cpf_flag,
	e.description "exhprogcat",
	d.full_fund,
	d.printable_fund,
	cd.description "designation",
	cd.id "desigid",
	o.description "overallcm",
	c1.description "cmcategory1",
	cl1.description "cm1intermediatelevel",
	cl2.description "cm1mainlevel",
	c2.description "cmcategory2",
	cl3.description "cm2intermediatelevel",
	c3.description "cmcategory3",
	cl4.description "cm3intermediatelevel",
	d.notes,
	d.inactive "funddetail_inactive"
FROM    T_FUND AS f
	INNER JOIN TX_CAMP_FUND AS cf
		ON f.fund_no = cf.fund_no
	INNER JOIN T_CAMPAIGN AS cm
		ON cf.campaign_no = cm.campaign_no
	INNER JOIN TR_CAMPAIGN_CATEGORY AS cc
		ON cm.category = cc.id
	LEFT OUTER JOIN LTR_FUND_DETAIL AS d
		ON f.fund_no = d.fund_no
	LEFT OUTER JOIN LTR_ACCT_GOAL AS ag1
		ON d.acct_goal_no = ag1.id
	LEFT OUTER JOIN LTR_ACCT_GRP AS ag2
		ON d.acct_grp_no = ag2.id
	LEFT OUTER JOIN LTR_EXH_PRG_CAT AS e
		ON d.exh_prg_cat_no = e.id
	LEFT OUTER JOIN LTR_OVERALL_CAMPAIGN AS o
		ON d.overall_cm_no = o.id
	LEFT OUTER JOIN LTR_CM_CATEGORY1 AS c1
		ON d.cm_category1 = c1.id
	LEFT OUTER JOIN LTR_CM_LEVEL AS cl1
		ON c1.cm_level_no = cl1.id
	LEFT OUTER JOIN LTR_CM_LEVEL AS cl2
		ON cl1.cm_level_grp = cl2.id
	LEFT OUTER JOIN LTR_CM_CATEGORY2 AS c2
		ON d.cm_category2 = c2.id
	LEFT OUTER JOIN LTR_CM_LEVEL AS cl3
		ON c2.cm_level_no = cl3.id
	LEFT OUTER JOIN LTR_CM_CATEGORY3 AS c3
		ON d.cm_category3 = c3.id
	LEFT OUTER JOIN LTR_CM_LEVEL AS cl4
		ON c3.cm_level_no = cl4.id
	LEFT OUTER JOIN TR_CONT_DESIGNATION AS cd
		ON d.designation = cd.id;
