USE [impresario];
GO

/****** Object:  StoredProcedure [dbo].[LRP_ACTIVE_CORPORATE_MEMBERSHIPS]    Script Date: 6/14/2021 7:59:10 AM ******/
SET ANSI_NULLS ON;
GO

SET QUOTED_IDENTIFIER ON;
GO


ALTER PROCEDURE [dbo].[LRP_ACTIVE_CORPORATE_MEMBERSHIPS]
(@exp_dt DATE -- do not change this to DATETIME
)
AS

--EXEC [LRP_ACTIVE_CORPORATE_MEMBERSHIPS] '12/31/2018'

SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

SELECT m.customer_no,
       cs.esal1_desc,
       m.memb_level,
	   -- H. Sheridan, Service Now ticket SCTASK0002306 - MOS: Active Corporate Membership report expiration date incorrect
	   -- Dates are correct but the '23:59:59.997' portion of the expr_dt is causing exported results in particular to move
	   -- to the next day.  Cast it here explicitly as the date alone to address this.
       CAST(m.expr_dt AS DATE) AS expr_dt,
       m.init_dt,
       m.NRR_status,
       t.description AS membtrend,
       cr.description AS currentstatus,
       i.sort_name
FROM LV_ADV_BEST_LAST_MEMBERSHIP AS m (NOLOCK)
INNER JOIN TX_CUST_SAL AS cs ON m.customer_no = cs.customer_no
                                AND cs.default_ind = 'Y'
LEFT OUTER JOIN TR_MEMB_TREND AS t ON m.memb_trend = t.id
LEFT OUTER JOIN TR_CURRENT_STATUS AS cr ON m.current_status = cr.id
INNER JOIN T_CUSTOMER AS i ON m.customer_no = i.customer_no
WHERE m.memb_org_no = 7 -- Corporate Membership
      AND m.current_status IN ( 2, 3 ) -- Active, Pending
      AND CAST(m.expr_dt AS DATE) >= @exp_dt;

GO


