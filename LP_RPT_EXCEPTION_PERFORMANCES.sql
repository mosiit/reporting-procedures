USE impresario
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LP_RPT_EXCEPTION_PERFORMANCES]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LP_RPT_EXCEPTION_PERFORMANCES]
GO

CREATE PROCEDURE [dbo].[LP_RPT_EXCEPTION_PERFORMANCES]
        @report_start_dt datetime = Null,
        @report_end_dt datetime = Null,
        @ignore_previous_dates char(1) = 'Y',
        @return_message varchar(100) = Null OUTPUT
AS BEGIN

    DECLARE @exceptions_table_perf table ([exception_no] int IDENTITY (1,1), [exception_level] varchar(30), [exception_type] varchar(30), [exception_notes] varchar(255), [perf_no] int, [perf_code] varchar(10), [perf_date] char(10),
                                         [production_season_no] int, [production_season_fiscal] int, [production_season_name] varchar(30), [production_no] int, [production_name] varchar(30), [title_no] int, [title_name] varchar(30))
     
    DECLARE @exLevel varchar(30), @exType varchar(30), @exNotes varchar(255)

    SELECT @return_message= ''

    IF @report_start_dt is null SELECT @report_start_dt = getdate()
    SELECT @report_start_dt = convert(char(10),@report_start_dt,111) + ' 00:00:00'
    
    IF @report_end_dt is null SELECT @report_end_dt = max([performance_dt]) FROM LV_PRODUCTION_ELEMENTS_PERFORMANCE
    SELECT @report_end_dt = convert(char(10),@report_end_dt,111) + ' 23:59:59'


    /*  Exception Set # 1 - Title */

    --SELECT * FROM [dbo].[LV_SS_PRODUCTION_ELEMENTS_TITLE]

    BEGIN TRY

        SELECT @exLevel = '1. Title'

        --exception_level, exception_type, exception_notes, perf_no, perf_code, perf_date, production_season_no, production_season_fiscal, production_season_name, 
        --production_no, production_name, title_no, title_name

        SELECT @exType = 'Short Name', @exNotes = ' does not have a short name.'
        INSERT INTO @exceptions_table_perf SELECT @exLevel, @exType, [title_name] + @exNotes, 0, '', '', 0, 0, '', 0, '', [title_no], [title_name] 
                                           FROM [dbo].[LV_SS_PRODUCTION_ELEMENTS_TITLE] WHERE [title_short_name] = '' or [title_short_name] is null
    
        SELECT @exType = 'Content - Default Facility', @exNotes = ' does not have a default facility assigned to it.'
        INSERT INTO @exceptions_table_perf SELECT @exLevel, @exType, [title_name] + @exNotes, 0, '', '', 0, 0, '', 0, '', [title_no], [title_name] 
                                           FROM [dbo].[LV_SS_PRODUCTION_ELEMENTS_TITLE] WHERE [default_facility] = 0

        SELECT @exType = 'Content - Prefix Code', @exNotes = ' does not have a prefix code assigned to it.'
        INSERT INTO @exceptions_table_perf SELECT @exLevel,  @exType, [title_name] + @exNotes, 0, '', '', 0, 0, '', 0, '', [title_no], [title_name] 
                                           FROM [dbo].[LV_SS_PRODUCTION_ELEMENTS_TITLE] WHERE [perf_code_prefix] = ''

        SELECT @exType = 'Content - Location', @exNotes = ' does not have a location assigned to it.'
        INSERT INTO @exceptions_table_perf SELECT @exLevel,  @exType, [title_name] + @exNotes, 0, '', '', 0, 0, '', 0, '', [title_no], [title_name] 
                                           FROM [dbo].[LV_SS_PRODUCTION_ELEMENTS_TITLE] WHERE [title_location] = ''

    END TRY
    BEGIN CATCH

        SELECT @return_message = left(Error_message(),100)
        IF @return_message is null SELECT @return_message = 'an error occurred while looking for performance level 1 (title) exceptions.'
        GOTO DONE

    END CATCH
        
    /*  Exception Set # 2 - Production */

    --SELECT * FROM [dbo].[LV_PRODUCTION_ELEMENTS_PRODUCTION] WHERE title_no = 157

    BEGIN TRY

        SELECT @exLevel = '2. Production'

        SELECT @exType = 'Name Length', @exNotes = ' has a title longer than 23 characters.'
        INSERT INTO @exceptions_table_perf SELECT @exLevel, @exType, [production_name] + ' (' + [title_name] + ')' + @exNotes, 0, '', '', 0, 0, '', [Production_no], [production_name], [title_no], [title_name] 
                                           FROM [dbo].[LV_PRODUCTION_ELEMENTS_PRODUCTION] WHERE len(ltrim(rtrim([production_name]))) > 23

        SELECT @exType = 'Content - Long Title', @exNotes = ' does not have a long title.'
        INSERT INTO @exceptions_table_perf SELECT @exLevel, @exType, [production_name] + ' (' + [title_name] + ')' + @exNotes, 0, '', '', 0, 0, '', [Production_no], [production_name], [title_no], [title_name] 
                                           FROM [dbo].[LV_PRODUCTION_ELEMENTS_PRODUCTION] WHERE [production_name_long] = ''

        SELECT @exType = 'Content - Short Title', @exNotes = ' does not have a short title.'
        INSERT INTO @exceptions_table_perf SELECT @exLevel, @exType, [production_name] + ' (' + [title_name] + ')' + @exNotes, 0, '', '', 0, 0, '', [Production_no], [production_name], [title_no], [title_name] 
                                           FROM [dbo].[LV_PRODUCTION_ELEMENTS_PRODUCTION] WHERE [production_name_short] = ''

        SELECT @exType = 'Content - Abbreviated Title', @exNotes = ' does not have an abbreviated title.'
        INSERT INTO @exceptions_table_perf SELECT @exLevel, @exType, [production_name] + ' (' + [title_name] + ')' + @exNotes, 0, '', '', 0, 0, '', [Production_no], [production_name], [title_no], [title_name] 
                                           FROM [dbo].[LV_PRODUCTION_ELEMENTS_PRODUCTION] WHERE [production_name_abbreviated] = ''

        SELECT @exType = 'Content - Duration', @exNotes = ' does not have a duration.'
        INSERT INTO @exceptions_table_perf SELECT @exLevel, @exType, [production_name] + ' (' + [title_name] + ')' + @exNotes, 0, '', '', 0, 0, '', [Production_no], [production_name], [title_no], [title_name] 
                                           FROM [dbo].[LV_PRODUCTION_ELEMENTS_PRODUCTION] WHERE [production_duration] <= 0

    END TRY
    BEGIN CATCH
    
        SELECT @return_message = left(Error_message(),100)
        IF @return_message is null SELECT @return_message = 'an error occurred while looking for performance level 2 (production) exceptions.'
        GOTO DONE

    END CATCH
     
    /*  Exception Set # 3 - Production Season  */

    --SELECT * FROM LV_PRODUCTION_ELEMENTS_SEASON

    BEGIN TRY

        SELECT @exLevel = '3. Production Season'

        SELECT @exType = 'Invalid Fiscal Year', @exNotes = ' appears to have an invalid fiscal year.'
        INSERT INTO @exceptions_table_perf SELECT @exLevel, @exType, [production_season_name] + @exNotes, 0, '', '', [production_season_no], [season_fiscal_year], [production_season_name], [Production_no], [production_name], [title_no], [title_name] 
                                           FROM [dbo].[LV_PRODUCTION_ELEMENTS_SEASON] WHERE isnumeric(right([production_season_name], 4)) = 1 and convert(int,right([production_season_name], 4)) <> [season_fiscal_year]

 
        SELECT @exType = 'Invalid Season Name', @exNotes = ' appears to be an invalid production season name.'
        INSERT INTO @exceptions_table_perf SELECT @exLevel, @exType, [production_season_name] + @exNotes, 0, '', '', [production_season_no], [season_fiscal_year], [production_season_name], [Production_no], [production_name], [title_no], [title_name] 
                                           FROM [dbo].[LV_PRODUCTION_ELEMENTS_SEASON] WHERE [production_season_name] <> ([Production_name] + ' FY' + convert(varchar(4),[season_fiscal_year]))
                                                                                        and [production_season_no] not in (SELECT [production_season_no] FROM @exceptions_table_perf)
    
    END TRY
    BEGIN CATCH

        SELECT @return_message = left(Error_message(),100)
        IF @return_message is null SELECT @return_message = 'an error occurred while looking for performance level 3 (production season) exceptions.'
        GOTO DONE

    END CATCH


    /*  Exception Set # 4 - Performance */

    --SELECT * FROM LV_PRODUCTION_ELEMENTS_PERFORMANCE

    BEGIN TRY

        SELECT @exLevel = '4. Performance'

        SELECT @exType = 'Invalid Perf Code', @exNotes = ' appears to have an invalid performance code.'
        INSERT INTO @exceptions_table_perf SELECT DISTINCT @exLevel, @exType, [production_name] + ' on ' + [performance_date] + ' in ' + [title_name] + @exNotes +  ' Perf Code Prefix should be ' + [perf_code_prefix] + '.',
                                                           [performance_no], [performance_code], [performance_date], [production_season_no], [season_fiscal_year], [production_season_name], [Production_no], [production_name], [title_no], [title_name] 
                                           FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] 
                                           WHERE [performance_dt] between @report_start_dt and @report_end_dt and [perf_code_prefix] <> '' and left([performance_code],1) <> [perf_code_prefix]

        SELECT @exType = 'Perf Code Check # 1', @exNotes = ' might to have an invalid performance code.'
        INSERT INTO @exceptions_table_perf SELECT DISTINCT @exLevel, @exType, [production_name] + ' on ' + [performance_date] + ' in ' + [title_name] + @exNotes +  '  Unknown Perf Code Prefix.',
                                                          [performance_no], [performance_code], [performance_date], [production_season_no], [season_fiscal_year], [production_season_name], [Production_no], [production_name], [title_no], [title_name] 
                                           FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] 
                                           WHERE [performance_dt] between @report_start_dt and @report_end_dt and [perf_code_prefix] = ''

        SELECT @exType = 'Perf Code Check # 2', @exNotes = ' either has an invalid performance code or exists in the wrong production season.'
        INSERT INTO @exceptions_table_perf SELECT DISTINCT @exLevel, @exType, [production_name] + ' on ' + [performance_date] + ' in ' + [title_name] + @exNotes + ' (' + [performance_code] + '/' + [production_name_abbreviated] + ')',
                                                          [performance_no], [performance_code], [performance_date], [production_season_no], [season_fiscal_year], [production_season_name], [Production_no], [production_name], [title_no], [title_name] 
                                           FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] 
                                           WHERE left(performance_code,1) not in ('T', 'W', 'X', 'Z') and SUBSTRING(performance_code,6,4) <> production_name_abbreviated

        SELECT @exType = 'Invalid Perf Type', @exNotes = ' is still set to the default performance type.'
        INSERT INTO @exceptions_table_perf SELECT DISTINCT @exLevel, @exType, [production_name] + ' on ' + [performance_date] + ' in ' + [title_name] + @exNotes,
                                                           [performance_no], [performance_code], [performance_date], [production_season_no], [season_fiscal_year], [production_season_name], [Production_no], [production_name], [title_no], [title_name] 
                                           FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] 
                                           WHERE [performance_dt] between @report_start_dt and @report_end_dt and [performance_type] <= 1 

    END TRY
    BEGIN CATCH
    
        SELECT @return_message = left(Error_message(),100)
        IF @return_message is null SELECT @return_message = 'an error occurred while looking for performance level 4 (performance) exceptions.'
        GOTO DONE
        
    END CATCH

    IF @ignore_previous_dates <> 'N' DELETE FROM @exceptions_table_perf WHERE isDate([perf_date]) = 1 and [perf_date] < convert(char(10),getdate(),111)

    IF @return_message = '' SELECT @return_message = 'success'

    DONE:

        SELECT * FROM @exceptions_table_perf ORDER BY [exception_level], [exception_type], [title_name], [production_name], [production_season_fiscal], [perf_code]

        IF @return_message = '' SELECT @return_message = 'unknown error'

END
GO

GRANT EXECUTE ON [dbo].[LP_RPT_EXCEPTION_PERFORMANCES] TO impusers
GO


