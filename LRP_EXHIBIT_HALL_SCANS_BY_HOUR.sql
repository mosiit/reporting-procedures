USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[LRP_EXHIBIT_HALL_SCANS_BY_HOUR]
	@report_start_dt DATETIME,
	@report_end_dt DATETIME,
	@days_of_the_week_str VARCHAR(4000),
	@special_hours_str VARCHAR(4000)
AS 
BEGIN

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    SET ANSI_WARNINGS OFF

-- EXECUTE [dbo].[LRP_EXHIBIT_HALL_SCANS_BY_HOUR] @report_start_dt = '2-15-2018', @report_end_dt = '2-22-2018', @days_of_the_week_str = '', @special_hours_str = '', @display_type = 'Daily'

    -- Procedure Variables

    DECLARE @days_of_the_week_list TABLE ([day] VARCHAR(15));
        
    DECLARE @special_hours_list TABLE ([special] VARCHAR(50));

    DECLARE @hour_keywords TABLE ([kw_id] INT, [kw_description] VARCHAR(30), [valid_time] CHAR(1));
                 
    DECLARE @scan_time_table TABLE (
		[admission_date] CHAR(10), 
		[admission_date_display] VARCHAR(40), 
		[day_of week] VARCHAR(20), 
        [scan_hour] VARCHAR(8), 
		[scan_hour_display] VARCHAR(25), 
		[close_time] VARCHAR(10), 
        [close_time_display] VARCHAR(25), 
		[special_hours] VARCHAR(30), 
		[total_scanned] INT
		);

    -- Check Parameters 
    IF @report_start_dt IS NULL OR @report_end_dt IS NULL 
	RETURN
        
	IF CONVERT(DATE,@report_start_dt) >= CONVERT(DATE,GETDATE()) OR CONVERT(DATE,@report_end_dt) >= CONVERT(DATE,GETDATE()) 
	RETURN 
    
    --Parse days of the week if any are passed
    IF ISNULL(@days_of_the_week_str,'') <> ''
        INSERT INTO @days_of_the_week_list ([day])
        SELECT Element From dbo.FT_SPLIT_LIST(REPLACE(@days_of_the_week_str, CHAR(34), ''),',')

    --Parse special day types if any are passed
    IF  ISNULL(@special_hours_str,'') <> ''
        INSERT INTO @special_hours_list ([special])
		SELECT Element From dbo.FT_SPLIT_LIST(REPLACE(@special_hours_str, CHAR(34), ''),',')

	-- GET a list of all hour keywords where category = 12 (Hours) and identify if it's a valid time (isdate = true) */
    INSERT INTO @hour_keywords ([kw_id], [kw_description], [valid_time])
    SELECT [id], [description], 
		CASE 
			WHEN ISDATE(description) = 1 THEN 'Y'
			ELSE 'N'
		END 
	FROM [dbo].[TR_TKW] 
	WHERE [category] = 12;

   
/*  Get The list of scans per hour 
    The first join to keywords looks for keywords that are valid times defaulting to 17:00:00/5:00PM if nothing is found.
    The second join to keywords looks for keywords that are not valid times, defaulting to 'Regular' if nothing is found.  */
             
    INSERT INTO @scan_time_table ([admission_date], [admission_date_display], [day_of week], [scan_hour], [scan_hour_display], 
                                    [close_time], [close_time_display], [special_hours], [total_scanned])
    SELECT his.[scan_admission_date]
            , FORMAT ( CAST(his.[scan_admission_date] AS DATE), 'D', 'en-US' ) 
            , DATENAME(WEEKDAY, his.[scan_admission_date])
            , CASE WHEN DATEPART(hour, his.[scan_admission_time]) < 10 THEN '0' --Leading zero added for proper sorting
                ELSE '' END + DATENAME(hour, [scan_admission_time]) + ':00'
            , ''
            , ISNULL(MAX(kw1.[description]),'17:00:00')
            , ''
            , ISNULL(MAX(kw2.[description]),'Regular')
            , SUM([scan_admission_total])
    FROM [dbo].[LT_HISTORY_TICKET] AS his
            LEFT OUTER JOIN [dbo].[TX_INV_TKW] AS kwh ON kwh.[inv_no] = his.perf_no 
                                                    AND kwh.[tkw] IN (SELECT [kw_id] FROM @hour_keywords WHERE [valid_time] = 'Y')
            LEFT OUTER JOIN [dbo].[TR_TKW] AS kw1 ON kw1.[id] = kwh.[tkw]
            LEFT OUTER JOIN [dbo].[TX_INV_TKW] AS kws ON kws.[inv_no] = his.perf_no 
                                                    AND kws.[tkw] IN (SELECT [kw_id] FROM @hour_keywords WHERE [valid_time] = 'N')
            LEFT OUTER JOIN [dbo].[TR_TKW] AS kw2 ON kw2.[id] = kws.[tkw]
    WHERE CAST(scan_admission_date AS DATE) BETWEEN @report_start_dt AND @report_end_dt
        AND title_name IN ('gate_scan', 'exhibit halls', 'show and go' )
    GROUP BY scan_admission_date, 
             CASE WHEN DATEPART(hh, scan_admission_time) < 10 THEN '0' ELSE '' END + DATENAME(hh, scan_admission_time) + ':00';

	-- If specific days of the week are chosen, delete the others  */
	IF EXISTS (SELECT * FROM @days_of_the_week_list)
        DELETE FROM @scan_time_table WHERE [day_of week] NOT IN (SELECT [day] FROM @days_of_the_week_list);


    /*  Needed to add these update statements because the statement above was pulling wrong close time and special hours type 
        because of non-Exhibit Halls scans. This goes specificically to the exhibit halls product for that day and finds the 
        appropriate keyword if it is there. */
            
        UPDATE @scan_time_table
        SET [close_time] = (SELECT ISNULL(MAX(kwd.[description]),'17:00:00')
                            FROM [dbo].[TX_INV_TKW] AS kwx
                                 LEFT OUTER JOIN [dbo].[TR_TKW] AS kwd ON [kwd].id = kwx.[tkw]
                            WHERE kwx.[inv_no] = (SELECT MAX([performance_no]) 
                                                  FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] 
                                                  WHERE [performance_date] = [@scan_time_table].[admission_date] 
                                                    AND [title_name] = 'Exhibit Halls' 
                                                    AND [production_name] = 'Exhibit Halls')
                                  AND kwd.category = 12
                                  AND ISDATE(kwd.[description]) = 1);

        UPDATE @scan_time_table
        SET [special_hours] = (SELECT ISNULL(MAX(kwd.[description]),'Regular')
                             FROM [dbo].[TX_INV_TKW] AS kwx
                                  LEFT OUTER JOIN [dbo].[TR_TKW] AS kwd ON [kwd].id = kwx.[tkw]
                             WHERE kwx.[inv_no] = (SELECT MAX([performance_no]) 
                                                   FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] 
                                                   WHERE [performance_date] = [@scan_time_table].[admission_date] 
                                                     AND [title_name] = 'Exhibit Halls' 
                                                     AND [production_name] = 'Exhibit Halls')
                                   AND kwd.category = 12
                                   AND ISDATE(kwd.[description]) = 0 );

   /*  IF specific day types are chosen, delete the others  */
        IF EXISTS (SELECT * FROM @special_hours_list)
            DELETE FROM @scan_time_table WHERE [special_hours] NOT IN (SELECT [special] FROM @special_hours_list);

    /*  Set 12 hour display times for the 24 hour clock  */

        UPDATE @scan_time_table
        SET scan_hour_display = REPLACE(RIGHT(CONVERT(VARCHAR(50),CONVERT(DATETIME,[scan_hour]),9),14),':00:00:000',' ')
        WHERE ISDATE([scan_hour]) = 1;

        UPDATE @scan_time_table
        SET close_time_display = REPLACE(RIGHT(CONVERT(VARCHAR(50),CONVERT(DATETIME,[close_time]),9),14),':00:00:000',' ')
        WHERE ISDATE([close_time]) = 1;

 
        SELECT [admission_date]
                , [admission_date_display]
                , [day_of week]
                , [scan_hour]
                , [scan_hour_display]
                , [close_time]
                , [close_time_display]
                , [special_hours]
                , [total_scanned]
        FROM @scan_time_table 
        ORDER BY admission_date, scan_hour;
            
END
GO

GRANT EXECUTE ON [dbo].[LRP_EXHIBIT_HALL_SCANS_BY_HOUR] TO ImpUsers;
GO



