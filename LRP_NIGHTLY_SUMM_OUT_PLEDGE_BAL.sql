USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LRP_NIGHTLY_SUMM_OUT_PLEDGE_BAL]    Script Date: 12/14/2020 2:21:03 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



ALTER PROCEDURE [dbo].[LRP_NIGHTLY_SUMM_OUT_PLEDGE_BAL]
	@section VARCHAR(300),
	@sortOrder INT,
	@runBy	VARCHAR(300)
AS
-- ===============================================================
-- H. Sheridan, 4/4/2016 - Outstanding Pledge Balance
-- ===============================================================

BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	--SET @section = 'Outstanding Pledge Balance'
	--SET	@sortOrder = 3

	DECLARE @tblPassOne	TABLE	(
								customer_no	INT,
								value			MONEY
								)

	DECLARE @tblPassTwo TABLE	(
								customer_no		INT,
								value			MONEY
								)

	DECLARE @tblPassBoth TABLE	(
								customer_no		INT,
								value			MONEY
								)

	INSERT INTO @tblPassOne
        SELECT  c.customer_no,
                (c.cont_amt - c.recd_amt)
        FROM    T_CONTRIBUTION c
		--2020/12/14, H. Sheridan.  Service Now ticket SCTASK0001710 - join to campaign table
		JOIN	T_CAMPAIGN cam
		ON		c.campaign_no = cam.campaign_no
        WHERE   c.cont_type = 'P'
                AND c.custom_1 IN ('(none)', 'Matching Gift Credit')
                AND c.cont_amt > c.recd_amt
				--2020/12/14, H. Sheridan.  Service Now ticket SCTASK0001710 - add criteria for campaign category
				AND	cam.category NOT IN (8,9)

	--SELECT 'debug 1', * FROM @tblNightSumm

	INSERT INTO @tblPassTwo
        SELECT  i.customer_no,
                (c.cont_amt - c.recd_amt)
        FROM    T_CONTRIBUTION AS c
        INNER JOIN T_CREDITEE AS t ON c.ref_no = t.ref_no
        INNER JOIN T_CUSTOMER AS i ON t.creditee_no = i.customer_no
		--2020/12/14, H. Sheridan.  Service Now ticket SCTASK0001710 - join to campaign table
		JOIN	T_CAMPAIGN cam
		ON		c.campaign_no = cam.campaign_no
        WHERE   c.cont_type = 'P'
                AND c.custom_1 IN ('Primary Soft Credit', 'Matching Gift Credit', 'Stewardship Soft Credit')
                AND c.cont_amt > c.recd_amt
                AND t.creditee_type IN ('12', '15', '5', '10', '16', '1', '14')
				--2020/12/14, H. Sheridan.  Service Now ticket SCTASK0001710 - add criteria for campaign category
				AND	cam.category NOT IN (8,9)

	INSERT INTO @tblPassBoth
		SELECT * FROM @tblPassOne

	INSERT INTO @tblPassBoth
		SELECT * FROM @tblPassTwo

	--SELECT * FROM @tblPassBoth

	INSERT INTO LT_NIGHTLY_SUMMARY
		SELECT  customer_no,
				@section,
				SUM(ISNULL(value, 0)),
				'',
				@sortOrder,
				GETDATE(),
				@runBy
		FROM	@tblPassBoth
		GROUP BY customer_no

END

GO


