USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_ATTENDANCE_BY_TIME]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LRP_ATTENDANCE_BY_TIME]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LRP_ATTENDANCE_BY_TIME]
        @report_start_dt datetime = Null,
        @report_end_dt datetime = Null
AS BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

    DECLARE @start_date char(10), @end_date char(10)
    
    IF OBJECT_ID('tempdb..#final_table') IS NOT NULL DROP TABLE [#final_table]

            CREATE TABLE #final_table ([report_message] varchar(100), [attendance_type] varchar(30), [performance_type] varchar(30), [title_name] varchar(30), 
                                       [production_name] varchar(50), [production_name_short] varchar(50), [production_name_long] varchar(255),
                                       [performance_date] char(10), [performance_time] varchar(8), [sale_total] int, [scan_admission_total] int,
                                       [due_amt] decimal(18,2), [paid_amt] decimal(18,2))

            CREATE NONCLUSTERED INDEX [ix_final_table_title_name] ON #final_table ([title_name] ASC) ON [PRIMARY]

            CREATE NONCLUSTERED INDEX [ix_final_table_production_name_long] ON #final_table ([production_name_long] ASC) ON [PRIMARY]

    /*  Performance Dates in the History table are stored in yyyy/MM/dd format.  These variables are used for searching that table.  */

        SELECT @report_start_dt = IsNull(@report_start_dt, dateadd(day, -1, getdate())),
               @report_end_dt = IsNull(@report_end_dt, dateadd(day, -1, getdate()))

        SELECT @start_date = convert(char(10),@report_start_dt,111), @end_date = convert(char(10),@report_end_dt,111)

        INSERT INTO #final_table
        SELECT '', [attendance_type], [performance_type], [title_name], [production_name], [production_name_short], [production_name_long], 
               [performance_date], [performance_time], sum([sale_total]) as 'sale_total', 
               sum([scan_admission_total]) as 'scan_admission_total', sum([due_amt]) as 'due_amt', sum([paid_amt]) as 'paid_amt'
        FROM [dbo].[LT_HISTORY_TICKET] (NOLOCK)
        WHERE performance_date between @start_date and @end_date
        GROUP BY [attendance_type], [performance_type], [title_name], [production_name], [production_name_short], [production_name_long], 
                 [performance_date], [performance_time]


        DELETE FROM [#final_table] WHERE [title_name] = 'Gate Scan'

        UPDATE [#final_table] SET [performance_time] = left([performance_time],3) + '00' WHERE [title_name] = 'Show and Go'
        
    DONE:
    
    IF not exists (SELECT * FROM #final_table)
        INSERT INTO #final_table VALUES ('No rows found', '', '', '', '', '', '', '', 0, 0, 0.00, 0.00)
  
       
    /* Select aggregated date into the report  */
  
	SELECT [report_message], [attendance_type], [performance_type], [title_name], [production_name], [production_name_short], [production_name_long],
           [performance_time], sum([sale_total]) as 'sale_total', sum([scan_admission_total]) as 'scan_admission_total', 
           sum([due_amt]) as 'due_amt', sum([paid_amt]) as 'paid_amt', @report_start_dt as 'p_report_start_date', @report_end_dt as 'p_report_end_date' 
    FROM #final_table
    GROUP BY [report_message], [attendance_type], [performance_type], [title_name], [production_name], [production_name_short], [production_name_long], [performance_time]
    ORDER BY [title_name], [performance_time], [production_name]

END
GO

GRANT EXECUTE ON [dbo].[LRP_ATTENDANCE_BY_TIME] to impusers
GO


