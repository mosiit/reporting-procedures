USE [impresario]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[LRP_ALL_STEPS_BY_CUSTOMER_NO]
(
	@customer_no INT = NULL,
	@list_no INT = NULL,
	@start_dt DATETIME = NULL,
	@end_dt DATETIME = NULL 
)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

-- Either a @customer_no or a @list_no must be passed into this SP
IF ISNULL(@customer_no, 0) = 0 AND ISNULL(@list_no, 0) = 0
	RETURN 

DECLARE @customer_no_tbl TABLE
	(customer_no INT)

IF ISNULL(@customer_no, 0) <> 0
	INSERT INTO @customer_no_tbl (customer_no)
	VALUES (@customer_no)
ELSE
	INSERT INTO @customer_no_tbl (customer_no)
	SELECT customer_no
	FROM dbo.T_LIST_CONTENTS
	WHERE list_no = @list_no

SELECT 
	p.customer_no, 
	dn.display_name_short AS customer_name,
	dn.sort_name, 
	NULL AS id_key, -- required for custom screens
	p.plan_no, 
	pdn.display_name plan_display_name, 
	s.step_no, 
	s.step_dt, 
	s.completed_on_dt, 
	s.description step_type,
	s.notes, 
	adn.display_name_short assoc_display_name_short, 
	adn.sort_name assoc_sort_name, 
	wdn.customer_no worker_customer_no, 
	wdn.display_name_short worker_display_name_short, 
	wdn.sort_name worker_sort_name
FROM [dbo].T_STEP s
	JOIN T_PLAN p ON p.plan_no = s.plan_no
	JOIN [dbo].FT_PLAN_DISPLAY_NAME() pdn ON p.plan_no = pdn.plan_no
	JOIN @customer_no_tbl tbl
		ON tbl.customer_no = p.customer_no
	JOIN [dbo].FT_CONSTITUENT_DISPLAY_NAME() dn ON p.customer_no = dn.customer_no
	LEFT OUTER JOIN [dbo].FT_CONSTITUENT_DISPLAY_NAME() adn ON s.associate_no = adn.customer_no
	LEFT OUTER JOIN [dbo].FT_CONSTITUENT_DISPLAY_NAME() wdn ON s.worker_customer_no = wdn.customer_no
WHERE s.notes IS NOT NULL -- p.customer_no = @customer_no AND 
	AND s.step_dt between ISNULL(@start_dt, '1900-1-1') and ISNULL(@end_dt, '2999-12-31')
ORDER BY p.customer_no, p.plan_no, s.step_dt
		