USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[RP_PLAN_SUMMARY]    Script Date: 1/19/2017 3:54:32 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[LP_PLAN_SUMMARY](
	@campaign_category VARCHAR(4000),
	@campaign_FY VARCHAR(4000),
	@desig_no VARCHAR(4000) = NULL,
	@fund_no VARCHAR(4000) = NULL,
	@worker_no VARCHAR(4000) = NULL,
	@pri_worker_no VARCHAR(4000) = NULL, 
	@plan_status_no VARCHAR(4000) = NULL,
	@plan_priority_no VARCHAR(4000)  = NULL,
	@plan_source_no VARCHAR(4000) = NULL,
	@plan_start_dt DATETIME = NULL,
	@plan_end_dt DATETIME = NULL,
	@show_plan_workers VARCHAR(15), 
	@list_no INT = NULL
	)
AS
SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
/************************************************************************************************
1-19-2017 DSJ - Copied from RP_PLAN_SUMMARY

7/03/2013 #2551 SEB- New procedure
11/26/2013 #7367 SEB- Adding ref table id keys for report text output
Modified CWR 8/11/2014 #8860 -- increased display name column lengths
Modifed RWC #8946:  Added sequence number to Steps, Plans for distinct summing
Modified 12/12/14 #7494 - Added old/new values to output for steps for status and campaign change types
2/4/2015 #9954 - Fixed prev/next step values to be determined by completed_on_dt, not step_dt
2/10/2015 #9954 -- added prev step completed on dt, and next step due_dt to output, now next steps only considered where completed_on dt is NULL

exec rp_plan_summary @campaign_no=N'12,13,14,11,10,17,16,25,26,27,24,21,33,3,4,28,29,30,32,22,34,50,48,46,23,35,56,51,49,47,45,58,52,55,53,54,15,7,20,19,18,9,5,6,8',@desig_no=NULL,@fund_no=NULL,@worker_no=NULL,@pri_worker_no=NULL,@plan_type_no=NULL,@plan_status_no=NULL,@plan_priority_no=NULL,@plan_source_no=NULL,@plan_start_dt=NULL,@plan_end_dt=NULL,@incl_custom=N'N',@incl_notes=N'N',@incl_steps=N'Y',@list_no=N'42',@incl_workers=N'N'
exec rp_plan_summary @campaign_no=N'50',@desig_no=NULL,@fund_no=NULL,@worker_no=NULL,@pri_worker_no=NULL,@plan_type_no=NULL,@plan_status_no='33',@plan_priority_no=NULL,@plan_source_no=NULL,@plan_start_dt=NULL,@plan_end_dt=NULL,@incl_custom=N'N',@incl_notes=N'N',@incl_steps=N'Y',@list_no=NULL,@incl_workers=N'N'
***********************************************************************************************/

--check for valid list if passed
IF @list_no > 0 AND NOT EXISTS (SELECT * FROM [dbo].VS_LIST WHERE list_no = @list_no)
	BEGIN
		RAISERROR('List provided is not valid.', 11, 1) WITH NOWAIT
		RETURN
	END

--massage dates
IF @plan_start_dt IS NULL
	SELECT @plan_start_dt = '1900-1-1'

IF @plan_end_dt IS NULL
	SELECT @plan_end_dt = '9999-12-31'

/******************************Get campaigns list*****************************************/
--Create and populate a temp table to hold the Campaign Categories
CREATE TABLE #CampaignCat (
	campaign_cat_id INT
)

INSERT INTO #CampaignCat
SELECT CONVERT(INT,Element) FROM dbo.FT_SPLIT_LIST (@campaign_category,',')

--Create and populate a temp table to hold the Campaign FY
CREATE TABLE #CampaignFY (
	campaign_cat_fy INT
)

INSERT INTO #CampaignFY
SELECT CONVERT(INT,Element) FROM dbo.FT_SPLIT_LIST (@campaign_FY,',')

DECLARE @campaign TABLE (campaign_no INT, description VARCHAR(30) NULL)

INSERT 	@campaign (campaign_no, description)
SELECT	c.campaign_no, c.description 
FROM [dbo].T_CAMPAIGN c
	INNER JOIN #CampaignCat cat
		ON c.category = cat.campaign_cat_id
	INNER JOIN #CampaignFY fy
		ON c.fyear = fy.campaign_cat_fy

/******************************Get designations list*****************************************/
IF @desig_no IS NULL
	SELECT @desig_no = ''

SELECT @desig_no = RTRIM(@desig_no)

DECLARE @designation TABLE (desig_no INT, description VARCHAR(30) NULL)
 
IF @desig_no <> '' AND @desig_no IS NOT NULL
	BEGIN
		INSERT 	@designation (desig_no, description)
		SELECT	id, description FROM [dbo].TR_CONT_DESIGNATION
			WHERE 	CHARINDEX(',' + CONVERT(VARCHAR, id) + ',' , ',' + @desig_no + ',') > 0
	END
ELSE
	BEGIN
		INSERT 	@designation (desig_no, description)
		SELECT	id, description FROM [dbo].TR_CONT_DESIGNATION
	END

/******************************Get funds list*****************************************/
IF @fund_no IS NULL
	SELECT @fund_no = ''

SELECT @fund_no = RTRIM(@fund_no)

DECLARE @fund TABLE (fund_no INT, description VARCHAR(30) NULL)
 
IF @fund_no <> '' AND @fund_no IS NOT NULL
	BEGIN
		INSERT 	@fund (fund_no, description)
		SELECT	fund_no, description FROM [dbo].T_FUND
			WHERE 	CHARINDEX(',' + CONVERT(VARCHAR, fund_no) + ',' , ',' + @fund_no + ',') > 0
	END
ELSE
	BEGIN
		INSERT 	@fund (fund_no, description)
		SELECT	fund_no, description FROM [dbo].T_FUND
	END  
  	
/******************************Get workers*****************************************/
DECLARE @workers TABLE (plan_no INT, customer_no INT, display_name VARCHAR(160), sort_name VARCHAR(55))

IF @worker_no IS NULL
	SELECT @worker_no = ''

SELECT @worker_no = RTRIM(@worker_no)

IF @worker_no <> '' AND @worker_no IS NOT NULL
	BEGIN
		INSERT @workers (plan_no, customer_no, display_name, sort_name)
		SELECT cp.plan_no, cp.customer_no, dn.display_name_short, dn.sort_name
		FROM [dbo].TX_CUST_PLAN cp
		JOIN [dbo].FT_CONSTITUENT_DISPLAY_NAME() dn ON cp.customer_no = dn.customer_no
		WHERE CHARINDEX(',' + CONVERT(VARCHAR, cp.customer_no) + ',' , ',' + @worker_no + ',') > 0
	END
ELSE
	BEGIN
		INSERT @workers (plan_no, customer_no, display_name, sort_name)
		SELECT cp.plan_no, cp.customer_no, dn.display_name_short, dn.sort_name
		FROM [dbo].TX_CUST_PLAN cp
		JOIN [dbo].FT_CONSTITUENT_DISPLAY_NAME() dn ON cp.customer_no = dn.customer_no
	END

/******************************Get primary workers*****************************************/
IF @pri_worker_no IS NULL
	SELECT @pri_worker_no = ''

SELECT @pri_worker_no = RTRIM(@pri_worker_no)

DECLARE @priworkers TABLE (plan_no INT, customer_no INT, display_name VARCHAR(160), sort_name VARCHAR(55))

IF @pri_worker_no <> '' AND @pri_worker_no IS NOT NULL
	BEGIN
		INSERT @priworkers (plan_no, customer_no, display_name, sort_name)
		SELECT cp.plan_no, cp.customer_no, dn.display_name_short, dn.sort_name
		FROM [dbo].TX_CUST_PLAN cp
		JOIN [dbo].FT_CONSTITUENT_DISPLAY_NAME() dn ON cp.customer_no = dn.customer_no
		WHERE CHARINDEX(',' + CONVERT(VARCHAR, cp.customer_no) + ',' , ',' + @pri_worker_no + ',') > 0
		AND UPPER(cp.primary_ind) = 'Y'
	END
ELSE
	BEGIN
		INSERT @priworkers (plan_no, customer_no, display_name, sort_name)
		SELECT cp.plan_no, cp.customer_no, dn.display_name_short, dn.sort_name
		FROM [dbo].TX_CUST_PLAN cp
		JOIN [dbo].FT_CONSTITUENT_DISPLAY_NAME() dn ON cp.customer_no = dn.customer_no
		WHERE UPPER(cp.primary_ind) = 'Y'
	END

/******************************Get plan statuses*************************************/
DECLARE @planstatus TABLE (plan_status_no INT, description VARCHAR(30) NULL, [rank] INT)

IF @plan_status_no <> '' AND @plan_status_no IS NOT NULL
	BEGIN
		INSERT @planstatus (plan_status_no, description, [rank])
		SELECT id, description, [rank]
		FROM [dbo].TR_PLAN_STATUS
		WHERE CHARINDEX(',' + CONVERT(VARCHAR, id) + ',' , ',' + @plan_status_no + ',') > 0
	END
ELSE
	BEGIN
		INSERT @planstatus (plan_status_no, description, [rank])
		SELECT id, description, [rank]
		FROM [dbo].TR_PLAN_STATUS
	END

/******************************Get plan priorities*************************************/
DECLARE @planpriority TABLE (plan_priority_no INT, description VARCHAR(30) NULL)

IF @plan_priority_no <> '' AND @plan_priority_no IS NOT NULL
	BEGIN
		INSERT @planpriority (plan_priority_no, description)
		SELECT id, description
		FROM [dbo].TR_PLAN_PRIORITY
		WHERE CHARINDEX(',' + CONVERT(VARCHAR, id) + ',' , ',' + @plan_priority_no + ',') > 0
	END
ELSE
	BEGIN
		INSERT @planpriority (plan_priority_no, description)
		SELECT id, description
		FROM [dbo].TR_PLAN_PRIORITY
	END

/******************************Get plan sources*************************************/
DECLARE @plansource TABLE (plan_source_no INT, description VARCHAR(30) NULL)

IF @plan_source_no <> '' AND @plan_source_no IS NOT NULL
	BEGIN
		INSERT @plansource (plan_source_no, description)
		SELECT id, description
		FROM [dbo].TR_PLAN_SOURCE
		WHERE CHARINDEX(',' + CONVERT(VARCHAR, id) + ',' , ',' + @plan_source_no + ',') > 0
	END
ELSE
	BEGIN
		INSERT @plansource (plan_source_no, description)
		SELECT id, description
		FROM [dbo].TR_PLAN_SOURCE
	END
	
/******************************Get first and last step data*************************************/
DECLARE @planstepsfirstlast TABLE (plan_no INT, position CHAR(1), step_no INT, step_dt DATETIME, due_dt DATETIME NULL, 
								completed_on_dt DATETIME NULL, description VARCHAR(30), notes VARCHAR(MAX));

WITH prev_step AS(
	SELECT ROW_NUMBER() OVER(PARTITION BY plan_no ORDER BY completed_on_dt DESC) AS priority,
			plan_no, step_no, step_dt, due_dt, completed_on_dt, description, notes
	FROM T_STEP WHERE completed_on_dt <= GETDATE()
),
next_step AS(
	SELECT ROW_NUMBER() OVER(PARTITION BY plan_no ORDER BY due_dt ASC) AS priority,
			plan_no, step_no, step_dt, due_dt, completed_on_dt, description, notes
	FROM T_STEP WHERE completed_on_dt IS NULL AND step_type > 0 -- no audit/system steps
		AND step_type NOT IN (12,31,33,35,39,40) -- per Melanie
)

INSERT @planstepsfirstlast (plan_no, position, step_no, step_dt, due_dt, completed_on_dt, description, notes)
	SELECT plan_no, 'P' AS position, step_no, step_dt, due_dt, completed_on_dt, description, notes
	FROM prev_step
	WHERE priority = 1
	UNION
	SELECT plan_no, 'N' AS position, step_no, step_dt, due_dt, completed_on_dt, description, notes
	FROM next_step
	WHERE priority = 1

/****************************Custom Data****************************************************************/
DECLARE @customFieldsTbl TABLE
(
	plan_no INT,
	column_name VARCHAR(30),
	custom_id INT,
	data_type INT,
	edit_mask VARCHAR(30),
	column_value VARCHAR(255)
)

DECLARE @planNums TABLE
	(id INT IDENTITY,
	plan_no INT 
	)

INSERT INTO @planNums(plan_no)
SELECT DISTINCT p.plan_no 
	FROM [dbo].T_PLAN p
	JOIN @campaign c ON c.campaign_no = p.campaign_no
	ORDER BY p.plan_no

DECLARE @currentID INT = 0,
	@current_plan_no INT,
	@maxID INT

SELECT @maxID = MAX(id), @currentID = MIN(id) FROM @planNums

WHILE @currentID <= @maxID
BEGIN
	SELECT @current_plan_no = plan_no FROM @planNums WHERE id = @currentID

	INSERT INTO @customFieldsTbl
        (plan_no,
         column_name,
         custom_id,
         data_type,
         edit_mask,
         column_value)
	EXEC [dbo].[RP_PLAN_SUMMARY_CUSTOM] @current_plan_no, 'Y'

	SELECT @currentID = @currentID + 1
END 

--SELECT *
--FROM
--(
--	SELECT plan_no, column_name, column_value
--	FROM @customFieldsTbl
--) tbl2Pivot
--PIVOT (MAX(column_value) FOR column_name IN ([Proposal Title],[Lead Organization],[Subcontracts],[Grant Maker Program],[Indirect Ask Amt],
--[Indirect Funded Amt],[Overall Ask Amt],[Overall Funded Amt],[Release To Mem Flag],[Assigned EF Number])) AS customPivot


/****************************Plans****************************************************************/

DECLARE @PlanData TABLE (plan_no INT, plan_display_name VARCHAR(160), campaign_no INT, campaign_desc VARCHAR(30), customer_no INT, customer_display_name VARCHAR(160), 
		customer_sort_name VARCHAR(55), cont_desig_no INT, cont_desig_desc VARCHAR(30), fund_no INT, fund_desc VARCHAR(30), original_source_no INT,
		original_source_desc VARCHAR(30), plan_status_no INT, plan_status_desc VARCHAR(30), plan_status_rank INT, plan_type_no INT, plan_type_desc VARCHAR(30), notes VARCHAR(MAX), goal_amt MONEY, ask_amt MONEY, 
		cont_amt MONEY, recorded_amt MONEY, start_dt DATETIME, completed_by_dt DATETIME, plan_priority_no INT, priority VARCHAR(30), probability DECIMAL(5,4), 
		prev_step_no INT, prev_step_dt DATETIME, prev_step_completed_on_dt DATETIME NULL, prev_step_description VARCHAR(30), prev_step_notes VARCHAR(MAX),
		next_step_no INT, next_step_dt DATETIME, next_step_due_dt DATETIME NULL, next_step_description VARCHAR(30), next_step_notes VARCHAR(MAX))

INSERT INTO @PlanData
	SELECT p.plan_no, 
			pdn.display_name, 
			c.campaign_no, 
			c.description, 
			p.customer_no, 
			cdn.display_name, 
			cdn.sort_name, 
			d.desig_no, 
			d.description, 
			f.fund_no, 
			f.description, 
			s.plan_source_no, 
			s.description, 
			st.plan_status_no,  
			st.description, 
			st.[rank], 
			t.id, 
			t.description, 
			p.notes, 
			p.goal_amt, 
			p.ask_amt, 
			p.cont_amt, 
			p.recorded_amt, 
			p.start_dt, 
			p.complete_by_dt, 
			pr.plan_priority_no, 
			pr.description, 
			p.probability, 
			pps.step_no, 
			pps.step_dt,
			pps.completed_on_dt,
			pps.description, 
			pps.notes,
			nps.step_no, 
			nps.step_dt,
			nps.due_dt,
			nps.description,
			nps.notes
	FROM [dbo].T_PLAN p
		JOIN @campaign c ON c.campaign_no = p.campaign_no
		JOIN @designation d ON d.desig_no = p.cont_designation
		LEFT OUTER JOIN @fund f ON f.fund_no = p.fund_no
		JOIN @plansource s ON s.plan_source_no = p.original_source
		JOIN @planstatus st ON st.plan_status_no = p.status
		JOIN TR_PLAN_TYPE t ON t.id = p.type
		JOIN @planpriority pr ON pr.plan_priority_no = p.priority
		JOIN [dbo].FT_CONSTITUENT_DISPLAY_NAME() cdn ON p.customer_no = cdn.customer_no
		JOIN [dbo].FT_PLAN_DISPLAY_NAME() pdn ON p.plan_no = pdn.plan_no
		--LEFT OUTER JOIN @priworkers pw ON pw.plan_no = p.plan_no	
		LEFT OUTER JOIN @planstepsfirstlast pps ON pps.plan_no = p.plan_no AND pps.position = 'P'
		LEFT OUTER JOIN @planstepsfirstlast nps ON nps.plan_no = p.plan_no AND nps.position = 'N'
	WHERE (COALESCE(@list_no, 0) = 0 OR p.customer_no IN (SELECT customer_no FROM [dbo].T_LIST_CONTENTS WHERE list_no = @list_no))
		AND (p.plan_no IN (SELECT plan_no FROM @workers  WHERE CHARINDEX(',' + CONVERT(VARCHAR, customer_no) + ',' , ',' + @worker_no + ',') > 0) OR @worker_no = '')
		AND (p.plan_no IN (SELECT plan_no FROM @priworkers  WHERE CHARINDEX(',' + CONVERT(VARCHAR, customer_no) + ',' , ',' + @pri_worker_no + ',') > 0) OR @pri_worker_no = '')
		AND NOT (@fund_no <> '' AND f.description IS NULL)
		--AND NOT (@pri_worker_no <> '' AND pw.customer_no IS NULL)
		AND ((p.start_dt BETWEEN @plan_start_dt AND @plan_end_dt) OR (@plan_start_dt = '1900-1-1' AND p.start_dt IS NULL))
		AND p.type = 7 -- Pipeline only 
		;

-- The output of the columns differs from the orginal report based on the user's request
--
WITH CustomDataCTE (plan_no, [Proposal Title],[Lead Organization],[Subcontracts],[Grant Maker Program],[Indirect Ask Amt],
	[Indirect Funded Amt],[Overall Ask Amt],[Overall Funded Amt],[Release To Mem Flag],[Assigned EF Number])
AS 
(
	SELECT *
	FROM
	(
		SELECT plan_no, column_name, column_value
		FROM @customFieldsTbl
	) tbl2Pivot
	PIVOT (MAX(column_value) FOR column_name IN ([Proposal Title],[Lead Organization],[Subcontracts],[Grant Maker Program],[Indirect Ask Amt],
	[Indirect Funded Amt],[Overall Ask Amt],[Overall Funded Amt],[Release To Mem Flag],[Assigned EF Number])) AS customPivot
)
SELECT pd.plan_no AS PlanNo, 
	pd.customer_no AS CustomerNo, 
	pd.customer_display_name AS Name, 
	pd.customer_sort_name AS SortName,
	[Proposal Title],
	pd.campaign_desc AS CampaignDesc, 
	pd.cont_desig_desc AS DesignationDesc, 
	pd.fund_desc AS FundDesc, 
	pd.original_source_desc AS Plan_Source, 
	pd.plan_status_desc AS [Status],
	[Release To Mem Flag],	
	[Lead Organization],
	[Subcontracts],		
	CONVERT(VARCHAR(10), pd.start_dt, 101) AS Start_Dt, 
	CONVERT(VARCHAR(10), pd.completed_by_dt, 101) AS Funded_Dt, 
	[Grant Maker Program],
	pd.priority AS Priority,
	pd.probability AS Probability, 
	cp.customer_no AS PlanWorkerCustomerNo, 
	dn.display_name AS PlanWorker, 
	dn.sort_name AS PlanWorkerSortName,
	wr.description AS PlanWorkerRole, 
	cp.primary_ind AS PlanWorker_PrimaryFlag,
	FORMAT(pd.goal_amt, 'C', 'en-us') AS Goal_Amt,
	FORMAT(CAST([Overall Ask Amt] AS DECIMAL), 'C', 'en-us') AS [Overall Ask Amt], 
	FORMAT(pd.ask_amt, 'C', 'en-us') AS MOS_Ask_Amt,
	FORMAT(CAST([Indirect Ask Amt] AS DECIMAL), 'C', 'en-us') AS [Indirect Ask Amt],
	FORMAT(pd.cont_amt, 'C', 'en-us') AS Cont_Amt, 
	FORMAT(CAST([Overall Funded Amt] AS DECIMAL), 'C', 'en-us') AS [Overall Funded Amt],
	FORMAT(pd.recorded_amt, 'C', 'en-us') AS Recorded_Amt, 
	FORMAT(CAST([Indirect Funded Amt] AS DECIMAL),'C', 'en-us') AS [Indirect Funded Amt],
	pd.prev_step_no AS PrevStepNo, 
	CONVERT(VARCHAR(10), pd.prev_step_completed_on_dt, 101) AS PrevStepCompletedDt,
	pd.prev_step_description AS PrevStepDesc, 
	pd.prev_step_notes AS PrevStepNotes,
	pd.next_step_no AS NextStepNo, 
	CONVERT(VARCHAR(10), pd.next_step_due_dt, 101) AS NextStepDueDt,
	pd.next_step_description AS NextStepDesc,
	pd.next_step_notes AS NextStepNotes,
	pd.notes AS Plan_Notes,
	[dbo].[LFS_BOARD_GetProspectManager](pd.customer_no) AS PM_full_name, 
	[dbo].[LFS_BOARD_GetProspectManagerInitials](pd.customer_no) AS PM_Initials,
	ratings.AGL,
	ratings.MGL,
	ratings.PGL
FROM @PlanData pd
	INNER JOIN CustomDataCTE customData
		ON pd.plan_no = customData.plan_no
	LEFT JOIN [dbo].TX_CUST_PLAN cp
		ON pd.plan_no = cp.plan_no
		AND cp.primary_ind = CASE WHEN @show_plan_workers = 'Primary Worker' THEN 'Y' ELSE cp.primary_ind END 
	LEFT JOIN [dbo].FT_CONSTITUENT_DISPLAY_NAME() dn 
		ON cp.customer_no = dn.customer_no
	LEFT JOIN tr_worker_role wr
		ON wr.id = cp.role_no
	LEFT JOIN 
	(
		SELECT *
		FROM
		(
			SELECT r.customer_no, 
				CASE r.research_type
					WHEN 70 THEN 'AGL'
					WHEN 71 THEN 'MGL'
					WHEN 72 THEN 'PGL'
					ELSE NULL
				END research_type,
				r.research_source 
			FROM dbo.T_CUST_RESEARCH r
			WHERE research_type IN (70,71,72)
		) tbl2Pivot
		PIVOT  (MAX(research_source) FOR research_type IN (AGL, MGL, PGL)) AS ratingsPvt
	) ratings
		ON pd.customer_no = ratings.customer_no
ORDER BY pd.customer_sort_name, pd.plan_display_name
	
RETURN

GO


