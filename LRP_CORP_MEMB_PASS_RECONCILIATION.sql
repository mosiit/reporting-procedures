USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LRP_CORP_MEMB_PASS_RECONCILIATION]    Script Date: 3/18/2022 9:18:39 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



/*
    [LRP_CORP_MEMB_PASS_RECONCILIATION]

    This report/utility allows the Corporate Membership department to monitor their members to make sure they have not gone over
    their alloted number of passes for each venue.  The report produces a grid where the background colors of the cells are maniplulated to 
    allow users to see at a quick glance, who went over their allotment.  If run as a utility (parameter @review_only = 'N'), the
    utility will automatically remove the source code from the appropriate pricing rule to disable that particular type of pass on
    that particular corporate member if they have gone over their limit.  It will also re-add the source code back to the
    pricing rule if more passes are allotted to the member for customer service reasons.

    Creating this utility required the creation of a new system table called LTR_CORPORATE_PASS_LIMITS.  This table contains the
    allotted number of passes for each member and is used to compare with the number of passes redeemed to verify that they have
    not overspent their limit.  The table contains the expiration date of each membership for each member and that expiration
    date is compared with the visit date of the pass was used to determine which membership the pass was redeemed on.

*/

ALTER PROCEDURE [dbo].[LRP_CORP_MEMB_PASS_RECONCILIATION]
        @source_no_str VARCHAR(4000) = '',
        @almost_gone_threshold INT = 15,
        @review_only CHAR(1) = 'Y'
AS BEGIN

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;
  
    /*  Procedure Variables and Temp Tables  */
  
        DECLARE @today DATE = CAST(GETDATE() AS DATE);

        --Colors are set here and passed to the report format as part of the data set to allow for simple changes to colors in one place
        --If this were done in the report file and a color changed, upwards of 80 individual formulas would need to be opened and adjusted
        DECLARE @all_gone_backcolor VARCHAR(25) = 'DarkRed',                @almost_gone_backcolor VARCHAR(25) = 'PaleGoldenrod',
                @inactive_backcolor VARCHAR(25) = 'DimGray',                @default_backcolor AS VARCHAR(25) = 'White',
                @readd_backcolor AS VARCHAR(25) = 'Blue',                   @ignore_backcolor AS VARCHAR(25) = 'Lavender'

        DECLARE @all_gone_forecolor VARCHAR(25) = 'Gainsboro',              @almost_gone_forecolor VARCHAR(25) = 'Black',
                @inactive_forecolor VARCHAR(25) = 'WhiteSmoke',             @default_forecolor AS VARCHAR(25) = 'Black',
                @readd_forecolor VARCHAR(25) = 'Gainsboro',                 @ignore_forecolor AS VARCHAR(25) = 'DarkGray'

        DECLARE @source_no INT = 0, 
                @rule_id INT = 0,
                @rule_description VARCHAR(30) = '',
                @cust_memb_no INT = 0,
                @difference INT = 0

        DECLARE @price_type_source_list TABLE ([source_no] INT NOT NULL DEFAULT (0), 
                                               [rule_description] varchar(50) NOT NULL DEFAULT (''), 
                                               [rule_id] INT NOT NULL DEFAULT (0), 
                                               [pass_diff] INT NOT NULL DEFAULT (0), 
                                               [cust_memb_no] INT NOT NULL DEFAULT (0), 
                                               [difference] INT NOT NULL DEFAULT (0))

        DECLARE @source_list TABLE ([source_no] INT NOT NULL DEFAULT (0))

        /*  Used to hold the raw data of all corporate membership passes redeemed.  This data is then aggregated and pivoted into the #corp_pass_redemption table  */
        IF OBJECT_ID('tempdb..#redemption_raw_data') IS NOT NULL DROP TABLE [#redemption_raw_data];

        CREATE TABLE [#redemption_raw_data] ([source_no] INT NOT NULL DEFAULT (0),
                                             [source_name] VARCHAR(50) NOT NULL DEFAULT (''), 
                                             [appeal_no] INT NOT NULL DEFAULT (0),
                                             [appeal_name] VARCHAR(50) NOT NULL DEFAULT (''), 
                                             [campaign_no] INT NOT NULL DEFAULT (0),
                                             [campaign_name] VARCHAR(50) NOT NULL DEFAULT (''), 
                                             [cust_memb_no] INT NOT NULL DEFAULT (0),
                                             [source_group_name] VARCHAR(50) NOT NULL DEFAULT (''), 
                                             [source_customer_no] INT NOT NULL DEFAULT (0),
                                             [source_customer_name] VARCHAR(80) NOT NULL DEFAULT (''), 
                                             [source_customer_sort] VARCHAR(80) NOT NULL DEFAULT (''), 
                                             [order_no] INT NOT NULL DEFAULT (0),
                                             [sli_no] INT NOT NULL DEFAULT (0),
                                             [price_type] INT NOT NULL DEFAULT (0),
                                             [price_type_name] VARCHAR(50) NOT NULL DEFAULT (''), 
                                             [due_amt] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                             [paid_amt] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                             [ticket_no] INT NOT NULL DEFAULT (0),
                                             [sli_status] INT NOT NULL DEFAULT (0),
                                             [sli_status_name] VARCHAR(50) NOT NULL DEFAULT (''), 
                                             [perf_no] INT NOT NULL DEFAULT (0),
                                             [zone_no] INT NOT NULL DEFAULT (0),
                                             [init_dt] DATETIME NULL,
                                             [expr_dt] DATETIME NULL,
                                             [performance_dt] DATETIME NULL,
                                             [performance_time] VARCHAR(50) NOT NULL DEFAULT (''), 
                                             [performance_time_display] VARCHAR(50) NOT NULL DEFAULT (''), 
                                             [title_no] INT NOT NULL DEFAULT (0),
                                             [title_name] VARCHAR(50) NOT NULL DEFAULT (''), 
                                             [production_no] INT NOT NULL DEFAULT (0),
                                             [production_name] VARCHAR(50) NOT NULL DEFAULT (''), 
                                             [production_name_long] VARCHAR(50) NOT NULL DEFAULT (''), 
                                             [rule_ind] CHAR(1) NOT NULL DEFAULT (''),
                                             [rule_id] INT NOT NULL DEFAULT (0),
                                             [rule_name] VARCHAR(50) NOT NULL DEFAULT (''), 
                                             [customer_no] INT NOT NULL DEFAULT (0),
                                             [customer_name] VARCHAR(80) NOT NULL DEFAULT (''), 
                                             [customer_sort] VARCHAR(80) NOT NULL DEFAULT (''),
                                             [seat_counter] INT NOT NULL DEFAULT (0))
        
        --Used to hold the basic information about how many passes each member is allowed to use in their given membership year
        IF OBJECT_ID('tempdb..#corp_pass_limits') IS NOT NULL DROP TABLE [#corp_pass_limits];

        CREATE TABLE [#corp_pass_limits] ([source_no] INT NOT NULL DEFAULT (0),
                                          [cust_memb_no] INT NOT NULL DEFAULT (0),
                                          [customer_no] INT NOT NULL DEFAULT (0),
                                          [source_name] VARCHAR(80) NOT NULL DEFAULT (''),
                                          [customer_name] VARCHAR(80) NOT NULL DEFAULT (''),
                                          [level_code] VARCHAR(50) NOT NULL DEFAULT (''),
                                          [level_name] VARCHAR(50) NOT NULL DEFAULT (''),
                                          [group] VARCHAR(50) NOT NULL DEFAULT (''),
                                          [init_dt] DATETIME NULL,
                                          [expr_dt] DATETIME NULL,
                                          [exhibit_hall_limit] INT NOT NULL DEFAULT (0),
                                          [special_exhibitions_limit] INT NOT NULL DEFAULT (0),
                                          [omni_theater_limit] INT NOT NULL DEFAULT (0),
                                          [planetarium_limit] INT NOT NULL DEFAULT (0),
                                          [theater_4d_limit] INT NOT NULL DEFAULT (0),
                                          [butterfly_garden_limit] INT NOT NULL DEFAULT (0),
                                          [total_limit] INT NOT NULL DEFAULT (0))

        --Used to hold the aggregated and pivoted redemption data from the #redemption_raw_data table
        IF OBJECT_ID('tempdb..#corp_pass_redemption') IS NOT NULL DROP TABLE [#corp_pass_redemption];

        CREATE TABLE [#corp_pass_redemption] ([source_no] INT NOT NULL DEFAULT (0),
                                              [source_name] VARCHAR(80) NOT NULL DEFAULT (''),
                                              [source_group_name] VARCHAR(50) NOT NULL DEFAULT (''),
                                              [expr_dt] DATETIME NULL,
                                              [order_count] INT NOT NULL DEFAULT (0),
                                              [exhibit_hall_redeemed] INT NOT NULL DEFAULT (0),
                                              [special_exhibitions_redeemed] INT NOT NULL DEFAULT (0),
                                              [omni_theater_redeemed] INT NOT NULL DEFAULT (0),
                                              [planetarium_redeemed] INT NOT NULL DEFAULT (0),
                                              [theater_4d_redeemed] INT NOT NULL DEFAULT (0),
                                              [butterfly_garden_redeemed] INT NOT NULL DEFAULT (0),
                                              [Total_redeemed] INT NOT NULL DEFAULT(0))

        --Holds the final data set derived from the #corp_pass_limits and #corp_pass_redemption
        IF OBJECT_ID('tempdb..#final_table') IS NOT NULL DROP TABLE [#final_table];

        CREATE TABLE [#final_table] ([row_num] INT NOT NULL DEFAULT (0),
                                     [source_no] INT NOT NULL DEFAULT (0),
                                     [cust_memb_no] INT NOT NULL DEFAULT (0),
                                     [customer_no] INT NOT NULL DEFAULT (0),
                                     [source_name] VARCHAR(50) NOT NULL DEFAULT (''),
                                     [customer_name] VARCHAR(80) NOT NULL DEFAULT (''),
                                     [level_code] VARCHAR(15) NOT NULL DEFAULT (''),
                                     [level_name] VARCHAR(50) NOT NULL DEFAULT (''),
                                     [group] VARCHAR(50) NOT NULL DEFAULT (''),
                                     [init_dt] DATETIME NULL,
                                     [expr_dt] DATETIME NULL,
                                     [order_count] INT NULL DEFAULT (0),
                                     [exhibit_hall_limit] INT NOT NULL DEFAULT (0),
                                     [exhibit_hall_redeemed] INT NOT NULL DEFAULT (0),
                                     [exhibit_hall_difference] INT NOT NULL DEFAULT (0),
                                     [exhibit_hall_backcolor] VARCHAR(50) NOT NULL DEFAULT (''),
                                     [exhibit_hall_forecolor] VARCHAR(50) NOT NULL DEFAULT (''),
                                     [special_exhibitions_limit] INT NOT NULL DEFAULT (0),
                                     [special_exhibitions_redeemed] INT NOT NULL DEFAULT (0),
                                     [special_exhibitions_difference] INT NOT NULL DEFAULT (0),
                                     [special_exhibitions_backcolor] VARCHAR(50) NOT NULL DEFAULT (''),
                                     [special_exhibitions_forecolor] VARCHAR(50) NOT NULL DEFAULT (''),
                                     [omni_theater_limit] INT NOT NULL DEFAULT (0),
                                     [omni_theater_redeemed] INT NOT NULL DEFAULT (0),
                                     [omni_theater_difference] INT NOT NULL DEFAULT (0),
                                     [Omni_theater_backcolor] VARCHAR(50) NOT NULL DEFAULT (''),
                                     [Omni_theater_forecolor] VARCHAR(50) NOT NULL DEFAULT (''),
                                     [planetarium_limit] INT NOT NULL DEFAULT (0),
                                     [planetarium_redeemed] INT NOT NULL DEFAULT (0),
                                     [planetarium_difference] INT NOT NULL DEFAULT (0),
                                     [planetarium_backcolor] VARCHAR(50) NOT NULL DEFAULT (''),
                                     [planetarium_forecolor] VARCHAR(50) NOT NULL DEFAULT (''),
                                     [theater_4d_limit] INT NOT NULL DEFAULT (0),
                                     [theater_4d_redeemed] INT NOT NULL DEFAULT (0),
                                     [theater_4d_difference] INT NOT NULL DEFAULT (0),
                                     [theater_4d_backcolor] VARCHAR(50) NOT NULL DEFAULT (''),
                                     [theater_4d_forecolor] VARCHAR(50) NOT NULL DEFAULT (''),
                                     [butterfly_garden_limit] INT NOT NULL DEFAULT (0),
                                     [butterfly_garden_redeemed] INT NOT NULL DEFAULT (0),
                                     [butterfly_garden_difference] INT NOT NULL DEFAULT (0),
                                     [butterfly_garden_backcolor] VARCHAR(50) NOT NULL DEFAULT (''),
                                     [butterfly_garden_forecolor] VARCHAR(50) NOT NULL DEFAULT (''))

    /*  Check Parameters - Sets default values for nulls  */

        IF ISNULL(@almost_gone_threshold, 0) < 1 SELECT @almost_gone_threshold = 1
      
        SELECT @review_only = ISNULL(@review_only, 'Y')

        --Parse out list of sources into a table variable to use in an inner join
        --If no source number list, enter all source numbers from the LV_ACTIVE_CORPORATE_MEMBERS_VIEW
        IF ISNULL(@source_no_str,'') = ''
            INSERT INTO @source_list ([source_no])
            SELECT DISTINCT [source_no] FROM [dbo].[LV_ACTIVE_CORPORATE_MEMBERS]
        ELSE
            INSERT INTO @source_list ([source_no])
            SELECT [element] FROM [dbo].[FT_SPLIT_LIST](REPLACE(@source_no_str,'"',''),',')

    /*  Get Memberships to Report On
        This starts with the LTR_CORPORATE_PASS_LIMITS system table.  It will report on any membership in that table that is not set to inactive
        It will also omit any memberships where the expiration date is before today if "No" is passed to the @current_and_future_only parameter.  */

        INSERT INTO [#corp_pass_limits] ([source_no], [cust_memb_no], [customer_no], [source_name], [customer_name], [level_code], [level_name], [group], [init_dt], [expr_dt],
                                         [exhibit_hall_limit], [special_exhibitions_limit], [omni_theater_limit], [planetarium_limit], [theater_4d_limit], [butterfly_garden_limit])
        SELECT DISTINCT pss.[source_no],
                mem.[cust_memb_no],
                cor.[customer_no],
                cor.[source_name],
                cor.[customer_name],
                mem.[memb_level] AS [level_code],
                lev.[description] AS [level_name],
                REPLACE(lev.[description],' Membership','') AS [group],
                CAST(mem.[init_dt] AS DATE),
                pss.[expr_dt],
                pss.[exhibit_hall],
                pss.[special_exhibitions],
                pss.[omni_theater],
                pss.[planetarium],
                pss.[theater_4d],
                pss.[butterfly_garden]
        FROM [dbo].[LTR_CORPORATE_PASS_LIMITS] AS pss
                INNER JOIN [dbo].[LV_ACTIVE_CORPORATE_MEMBERS] AS cor ON cor.source_no = pss.[source_no]
                INNER JOIN @source_list AS lis ON lis.[source_no] = cor.[source_no]
                INNER JOIN [dbo].[TX_CUST_MEMBERSHIP] AS mem ON mem.[customer_no] = cor.[customer_no] AND CAST(mem.[expr_dt] AS DATE) = pss.[expr_dt]
                INNER JOIN [dbo].[T_MEMB_LEVEL] AS lev ON lev.[memb_level] = mem.[memb_level]
        WHERE [pss].[inactive] = 'N'
          --AND (@current_and_future_only = 'N' OR pss.[expr_dt] >= @today)
          AND mem.[current_status] IN (2, 3);

          

    /*  Set the totals (sum of all passes allowed in all venues)  */

        UPDATE [#corp_pass_limits]
        SET [total_limit] = ([exhibit_hall_limit] + [special_exhibitions_limit] + [omni_theater_limit] + [planetarium_limit] + [theater_4d_limit] + [butterfly_garden_limit]);

    /*  Get Redemption Raw Data
        This is based primarily on the pricing rules.  It gets a list of all the pricing rules that contain appeal # 1760 and uses that list as an inner join
        to filter the T_SUB_LINEITEM table.  The passes redemmed are connected to the membership they apply to based on the performance date of what they are going
        to see (see the LEFT OUTER JOIN on [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] in the FROM clause).  It also filters on sli_status to only include valid
        tickets and ommit returns.  */


        WITH [CTE_RULES] ([rule_no], [rule_name]) AS (SELECT [id], 
                                                             [description] 
                                                      FROM [dbo].[T_PRICING_RULE]
                                                      WHERE [appeals] = '1760' 
                                                         OR [appeals] LIKE '%1760,%' 
                                                         OR [appeals] LIKE '%,1760%'),
             [CTE_ORDERS] ([order_no], [customer_no], [source_no]) AS (SELECT DISTINCT ord.[order_no], 
                                                                                       ord.[customer_no], 
                                                                                       lit.[line_source_no]
                                                                        FROM [dbo].[T_LINEITEM] AS lit
                                                                             INNER JOIN @source_list AS sou ON sou.[source_no] = lit.line_source_no
                                                                             INNER JOIN [dbo].[T_ORDER] AS ord ON ord.[order_no] = lit.[order_no])
        INSERT INTO [#redemption_raw_data] ([source_no],[source_name],[appeal_no],[appeal_name],[campaign_no],[campaign_name],[cust_memb_no],[source_group_name],[source_customer_no],[source_customer_name],
                                            [source_customer_sort],[order_no],[sli_no],[price_type],[price_type_name],[due_amt],[paid_amt],[ticket_no],[sli_status],[sli_status_name],[perf_no],
                                            [zone_no],[init_dt],[expr_dt],[performance_dt],[performance_time],[performance_time_display],[title_no],[title_name],[production_no],[production_name],
                                            [production_name_long],[rule_ind], [rule_id],[rule_name],[customer_no],[customer_name],[customer_sort],[seat_counter])
        SELECT     lim.[source_no],
                   lim.[source_name],
                   sou.[appeal_no],
                   apl.[description],
                   apl.[campaign_no],
                   cmp.[description] AS [campaign_name],
                   lim.[cust_memb_no],
                   lim.[group],
                   lim.[customer_no] AS [source_customer_no],
                   snm.[display_name] AS [source_customer_name],
                   snm.[sort_name] AS [source_customer_sort],
                   ord.[order_no],
                   sli.[sli_no],   
                   sli.[price_type],
                   ptp.[description] AS [price_type_name],
                   sli.[due_amt],
                   sli.[paid_amt],
                   ISNULL(sli.[ticket_no], 0),
                   sli.[sli_status],
                   sta.[description] AS [sli_status_name],
                   sli.[perf_no],
                   sli.[zone_no],
                   lim.[init_dt],
                   lim.[expr_dt],
                   prf.[performance_dt],
                   prf.[performance_time],
                   prf.[performance_time_display],
                   prf.[title_no],
                   prf.[title_name],
                   prf.[production_no],
                   prf.[production_name],
                   prf.[production_name_long],
                   sli.[rule_ind],
                   ISNULL(sli.[rule_id], 0) AS [rule_id],
                   rul.[rule_name],
                   ord.[customer_no],
                   nam.[display_name],
   				   -- H. Sheridan, Service Now ticket INC0124249.  Even though the default in the temp table definition is '', it appears to still put in a null.  Forcing a '' if it is null.
                   ISNULL(nam.[sort_name],''),
                   1
            FROM [#corp_pass_limits] AS lim
                 INNER JOIN [CTE_ORDERS] AS ord ON ord.[source_no] = lim.[source_no]
                 INNER JOIN [dbo].[T_SUB_LINEITEM] AS sli ON sli.[order_no] = ord.[order_no]
                 INNER JOIN [CTE_RULES] AS rul ON rul.[rule_no] = ISNULL(sli.[rule_id], 0)
                 LEFT OUTER JOIN [dbo].[FT_CONSTITUENT_DISPLAY_NAME]() AS snm ON snm.[customer_no] = lim.[customer_no]
                 INNER JOIN [dbo].[TR_PRICE_TYPE] AS ptp ON ptp.[id] = sli.[price_type]
                 INNER JOIN [dbo].[TR_SLI_STATUS] AS sta ON sta.[id] = sli.[sli_status]
                 INNER JOIN [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] AS prf ON prf.[performance_no] = sli.[perf_no] AND prf.[performance_zone] = sli.[zone_no]
                                                                             AND CAST(prf.[performance_dt] AS DATE) BETWEEN CAST(lim.[init_dt] AS DATE) AND CAST(lim.[expr_dt] AS DATE)
                 INNER JOIN [dbo].[TX_APPEAL_MEDIA_TYPE] AS sou ON sou.[source_no] = ord.[source_no]
                 LEFT OUTER JOIN [dbo].[T_APPEAL] AS apl ON apl.[appeal_no] = sou.[appeal_no]
                 LEFT OUTER JOIN [dbo].[T_CAMPAIGN] AS cmp ON cmp.[campaign_no] = apl.[campaign_no]
                 LEFT OUTER JOIN [dbo].[FT_CONSTITUENT_DISPLAY_NAME]() AS nam ON nam.[customer_no] = ord.[customer_no]
            WHERE sli.[sli_status] IN (3, 12)
              AND sli.[rule_ind] = 'R';

    /*  We are counting only free tickets paid for with passes.  Remove tickets on the same orders that were paid for  */

        DELETE FROM [#redemption_raw_data]
        WHERE [due_amt] <> 0.0;

    /*  Aggregate and Pivot Redemption Data
        This gives each type of pass its own column.  As of this writing, those are Exhibit Halls, Omni Theater, Planetarum and 4-D Theater.
        I have already added code that accomodate Butterfly Garden and Special Exhibits (PIXAR) if and when they are added.  
        Anything above and beyond that will require some kind of rewrite of this procedure to include them  */

        INSERT INTO [#corp_pass_redemption] ([source_no],[source_name],[source_group_name],[expr_dt],[exhibit_hall_redeemed],[special_exhibitions_redeemed],
                                             [omni_theater_redeemed],[planetarium_redeemed],[theater_4d_redeemed],[butterfly_garden_redeemed])
        SELECT  pvt.[source_no],
                pvt.[source_name], 
                pvt.[source_group_name], 
                pvt.[expr_dt], 
                ISNULL(pvt.[Exhibit Halls], 0) AS [Exhibit Halls], 
                ISNULL([Special Exhibitions], 0) AS [Special Exhibitions],
                ISNULL([Mugar Omni Theater], 0) AS [Omni Theater], 
                ISNULL([Hayden Planetarium], 0) AS [Planetarium],
                ISNULL([4-D Theater], 0) AS [4D Theater],
                ISNULL([Butterfly Garden], 0) AS [Butterfly]
            FROM   (SELECT [source_no],
                           [source_name],
                           [source_group_name],
                           [expr_dt],
                           [title_name],
                           [seat_counter]
                    FROM [#redemption_raw_data]) p  
            PIVOT (SUM ([seat_counter])  
                   FOR [title_name] IN  ([Exhibit Halls], [Special Exhibitions], [Mugar Omni Theater], [Hayden Planetarium], [4-D Theater], [Butterfly Garden])) AS pvt  
            ORDER BY pvt.[source_name];  

    /*  Count the number of orders on each membership (Distinct Order Number)  */

        WITH [CTE_ORDER_COUNTS] ([source_no], [expr_dt], [order_count]) 
        AS (SELECT rdm.[source_no], 
                   rdm.[expr_dt], 
                   COUNT(DISTINCT rdt.[order_no])
            FROM [#corp_pass_redemption] AS rdm
                 LEFT OUTER JOIN [#redemption_raw_data] AS rdt ON rdt.[source_no] = rdm.[source_no]
            GROUP BY rdm.[source_no], rdm.[source_name], rdm.[expr_dt])
        UPDATE rdm
        SET rdm.[order_count] = ISNULL(cte.[order_count], 0)
        FROM [#corp_pass_redemption] AS rdm
             LEFT OUTER JOIN [CTE_ORDER_COUNTS] AS cte ON cte.[source_no] = rdm.[source_no] AND cte.[expr_dt] = rdm.[expr_dt]

    /*  Set the totals (sum of all passes redeemed in all venues)  */

        UPDATE [#corp_pass_redemption]
        SET [total_redeemed] = ([exhibit_hall_redeemed] + [special_exhibitions_redeemed] + [omni_theater_redeemed]
                             +  [planetarium_redeemed] + [theater_4d_redeemed] + [butterfly_garden_redeemed])

    /*  compile the final data set
        This contains a number of CASE statements that will set the background and font colors for each column set in each row.  This is the base color
        based on the criteria of whether or not they are over their limit or alomst at their limit (threshold for almost is determined by each user using the @almost_gone_threshold paramter).
        Colors might change again in the next step based on whether the source needs to be removed from or added back to the pricing rule.

        As mentioned above Colors are set here in the stored procedure and passed to the report format as part of the data set to allow for simple changes to colors in one place
        If cell color was set in the report file itself and a color changed, upwards of 80 individual formulas would need to be opened within the report file and adjusted.  
        By doing it here, someone can change the name of the color in the variable declaration at the top of this procedure and as long as it is a valid color understood by SSRS, 
        it will cascade down  */

        INSERT INTO [#final_table] ([row_num],[source_no],[cust_memb_no],[customer_no],[source_name],[customer_name],[level_code],[level_name],[group],[init_dt],[expr_dt],[order_count],[exhibit_hall_limit],
                                    [exhibit_hall_redeemed],[exhibit_hall_difference],[exhibit_hall_backcolor],[exhibit_hall_forecolor],[special_exhibitions_limit],[special_exhibitions_redeemed],
                                    [special_exhibitions_difference],[special_exhibitions_backcolor],[special_exhibitions_forecolor],[omni_theater_limit],[omni_theater_redeemed],[omni_theater_difference],
                                    [Omni_theater_backcolor],[Omni_theater_forecolor],[planetarium_limit],[planetarium_redeemed],[planetarium_difference],[planetarium_backcolor],[planetarium_forecolor],
                                    [theater_4d_limit],[theater_4d_redeemed],[theater_4d_difference],[theater_4d_backcolor],[theater_4d_forecolor],[butterfly_garden_limit],[butterfly_garden_redeemed],
                                    [butterfly_garden_difference],[butterfly_garden_backcolor],[butterfly_garden_forecolor])
            SELECT  ROW_NUMBER() OVER(PARTITION BY lim.[customer_no] ORDER BY lim.[expr_dt] DESC),
                    lim.[source_no],
                    lim.[cust_memb_no],
                    lim.[customer_no],
                    lim.[source_name],
                    lim.[customer_name],
                    lim.[level_code],
                    lim.[level_name],
                    lim.[group],
                    lim.[init_dt],
                    lim.[expr_dt],
                    ISNULL(rdm.[order_count], 0),
                    lim.[exhibit_hall_limit],
                    ISNULL(rdm.[exhibit_hall_redeemed],0) AS [exhibit_hall_redeemed],
                    (lim.[exhibit_hall_limit] - ISNULL(rdm.[exhibit_hall_redeemed],0)) AS [exhibit_hall_difference],

                    CASE WHEN (lim.[exhibit_hall_limit] - ISNULL(rdm.[exhibit_hall_redeemed],0)) <= 0 THEN @all_gone_backcolor
                         WHEN (lim.[exhibit_hall_limit] - ISNULL(rdm.[exhibit_hall_redeemed],0)) <= @almost_gone_threshold THEN @almost_gone_backcolor
                         ELSE @default_backcolor END AS [exhibit_hall_backcolor],
                    
                    CASE WHEN (lim.[exhibit_hall_limit] - ISNULL(rdm.[exhibit_hall_redeemed],0)) <= 0 THEN @all_gone_forecolor
                         WHEN (lim.[exhibit_hall_limit] - ISNULL(rdm.[exhibit_hall_redeemed],0)) <= @almost_gone_threshold THEN @almost_gone_forecolor
                         ELSE @default_forecolor END AS [exhibit_hall_forecolor],
                    
                    lim.[special_exhibitions_limit],
                    ISNULL(rdm.[special_exhibitions_redeemed],0) AS [special_exhibitions_redeemed],
                    (lim.[special_exhibitions_limit] - ISNULL(rdm.[special_exhibitions_redeemed],0)) AS [special_exhibitions_difference],
                    
                    CASE WHEN (lim.[special_exhibitions_limit] - ISNULL(rdm.[special_exhibitions_redeemed],0)) <= 0 THEN @all_gone_backcolor
                         WHEN (lim.[special_exhibitions_limit] - ISNULL(rdm.[special_exhibitions_redeemed],0)) <= @almost_gone_threshold THEN @almost_gone_backcolor
                         ELSE @default_backcolor END AS [special_exhibitions_backcolor],
                    
                    CASE WHEN (lim.[special_exhibitions_limit] - ISNULL(rdm.[special_exhibitions_redeemed],0)) <= 0 THEN @all_gone_forecolor
                         WHEN (lim.[special_exhibitions_limit] - ISNULL(rdm.[special_exhibitions_redeemed],0)) <= @almost_gone_threshold THEN @almost_gone_forecolor
                         ELSE @default_forecolor END AS [special_exhibitions_forecolor],
                    
                    lim.[omni_theater_limit],
                    ISNULL(rdm.[omni_theater_redeemed],0) AS [omni_theater_redeemed],
                    (lim.[omni_theater_limit] - ISNULL(rdm.[omni_theater_redeemed],0)) AS [omni_theater_difference],
                    
                    CASE WHEN (lim.[omni_theater_limit] - ISNULL(rdm.[omni_theater_redeemed],0)) <= 0 THEN @all_gone_backcolor
                         WHEN (lim.[omni_theater_limit] - ISNULL(rdm.[omni_theater_redeemed],0)) <= @almost_gone_threshold THEN @almost_gone_backcolor
                         ELSE @default_backcolor END AS [Omni_theater_backcolor],
                    
                    CASE WHEN (lim.[omni_theater_limit] - ISNULL(rdm.[omni_theater_redeemed],0)) <= 0 THEN @all_gone_forecolor
                         WHEN (lim.[omni_theater_limit] - ISNULL(rdm.[omni_theater_redeemed],0)) <= @almost_gone_threshold THEN @almost_gone_forecolor
                         ELSE @default_forecolor END AS [Omni_theater_forecolor],
                    
                    lim.[planetarium_limit],
                    ISNULL(rdm.[planetarium_redeemed],0) AS [planetarium_redeemed],
                    (lim.[planetarium_limit] - ISNULL(rdm.[planetarium_redeemed],0)) AS [planetarium_difference],
                    
                    CASE WHEN (lim.[planetarium_limit] - ISNULL(rdm.[planetarium_redeemed],0)) <= 0 THEN @all_gone_backcolor
                         WHEN (lim.[planetarium_limit] - ISNULL(rdm.[planetarium_redeemed],0)) <= @almost_gone_threshold THEN @almost_gone_backcolor
                         ELSE @default_backcolor END AS [planetarium_backcolor],
                    
                    CASE WHEN (lim.[planetarium_limit] - ISNULL(rdm.[planetarium_redeemed],0)) <= 0 THEN @all_gone_forecolor
                         WHEN (lim.[planetarium_limit] - ISNULL(rdm.[planetarium_redeemed],0)) <= @almost_gone_threshold THEN @almost_gone_forecolor
                         ELSE @default_forecolor END AS [planetarium_forecolor],
                    
                    lim.[theater_4d_limit],
                    ISNULL(rdm.[theater_4d_redeemed],0) AS [theater_4d_redeemed],
                    (lim.[theater_4d_limit] - ISNULL(rdm.[theater_4d_redeemed],0)) AS [theater_4d_difference],
                    
                    CASE WHEN (lim.[theater_4d_limit] - ISNULL(rdm.[theater_4d_redeemed],0)) <= 0 THEN @all_gone_backcolor
                         WHEN (lim.[theater_4d_limit] - ISNULL(rdm.[theater_4d_redeemed],0)) <= @almost_gone_threshold THEN @almost_gone_backcolor
                         ELSE @default_backcolor END AS [theater_4d_backcolor],
                    
                    CASE WHEN (lim.[theater_4d_limit] - ISNULL(rdm.[theater_4d_redeemed],0)) <= 0 THEN @all_gone_forecolor
                         WHEN (lim.[theater_4d_limit] - ISNULL(rdm.[theater_4d_redeemed],0)) <= @almost_gone_threshold THEN @almost_gone_forecolor
                         ELSE @default_forecolor END AS [theater_4d_forecolor],
                    
                    lim.[butterfly_garden_limit],
                    ISNULL(rdm.[butterfly_garden_redeemed],0) AS [butterfly_garden_redeemed],
                    (lim.[butterfly_garden_limit] - ISNULL(rdm.[butterfly_garden_redeemed],0)) AS [butterfly_garden_difference],
                    
                    CASE WHEN (lim.[butterfly_garden_limit] - ISNULL(rdm.[butterfly_garden_redeemed],0)) <= 0 THEN @all_gone_backcolor
                         WHEN (lim.[butterfly_garden_limit] - ISNULL(rdm.[butterfly_garden_redeemed],0)) <= @almost_gone_threshold THEN @almost_gone_backcolor
                         ELSE @default_backcolor END AS [butterfly_garden_backcolor],
                    
                    CASE WHEN (lim.[butterfly_garden_limit] - ISNULL(rdm.[butterfly_garden_redeemed],0)) <= 0 THEN @all_gone_forecolor
                         WHEN (lim.[butterfly_garden_limit] - ISNULL(rdm.[butterfly_garden_redeemed],0)) <= @almost_gone_threshold THEN @almost_gone_forecolor
                         ELSE @default_forecolor END AS [butterfly_garden_forecolor]

            FROM [#corp_pass_limits] AS lim
                 LEFT OUTER JOIN [#corp_pass_redemption] AS rdm ON rdm.[source_no] = lim.[source_no] AND rdm.[expr_dt] = lim.[expr_dt]

    /*  Utility only acts on the membership with the latest expiration date (in row 1 for each customer)
        Set colors to the "ignore" colors for everything with a row number that's greater than 1  */

        UPDATE [#final_table]
        SET [exhibit_hall_backcolor] = @ignore_backcolor,
            [exhibit_hall_forecolor] = @ignore_forecolor,
            [special_exhibitions_backcolor] = @ignore_backcolor,
            [special_exhibitions_forecolor] = @ignore_forecolor,
            [omni_theater_backcolor] = @ignore_backcolor,
            [omni_theater_forecolor] = @ignore_forecolor,
            [planetarium_backcolor] = @ignore_backcolor,
            [planetarium_forecolor] = @ignore_forecolor,
            [theater_4d_backcolor] = @ignore_backcolor,
            [theater_4d_forecolor] = @ignore_forecolor,
            [butterfly_garden_backcolor] = @ignore_backcolor,
            [butterfly_garden_forecolor] = @ignore_forecolor
        WHERE [row_num] > 1


    /*  Check all the pricing rules to see if a source has to be removed from or added back in
        Each membership in the report is first stored in a table variable and processed one at a time through a read only cursor  */

        INSERT INTO @price_type_source_list ([source_no], [rule_description], [rule_id], [pass_diff], [cust_memb_no], [difference])

            SELECT fin.[source_no], 
                   'Corp Members ' + fin.[group] + ' EH', 
                   ISNULL(rul.[id], 0), 
                   fin.[exhibit_hall_difference], 
                   fin.[cust_memb_no], 
                   fin.[exhibit_hall_difference]
            FROM [#final_table]  AS fin
                 LEFT OUTER JOIN [dbo].[T_PRICING_RULE] AS rul ON rul.[description] = ('Corp Members ' + fin.[group] + ' EH')
            WHERE fin.[row_num] = 1
                    
            UNION SELECT fin.[source_no],
                         'Corp Members ' + fin.[group] + ' Omni', 
                         ISNULL(rul.[id], 0), 
                         fin.[omni_theater_difference], 
                         fin.[cust_memb_no], 
                         fin.[omni_theater_difference]
            FROM [#final_table]  AS fin
                 LEFT OUTER JOIN [dbo].[T_PRICING_RULE] AS rul ON rul.[description] = ('Corp Members ' + fin.[group] + ' Omni')
            WHERE fin.[row_num] = 1

            UNION SELECT fin.[source_no],
                         'Corp Members ' + fin.[group] + ' PL', 
                         ISNULL(rul.[id], 0),
                         fin.[planetarium_difference],
                         fin.[cust_memb_no],
                         fin.[planetarium_difference]
            FROM [#final_table]  AS fin
                 LEFT OUTER JOIN [dbo].[T_PRICING_RULE] AS rul ON rul.[description] = ('Corp Members ' + fin.[group] + ' PL')
            WHERE fin.[row_num] = 1

            UNION SELECT fin.[source_no],
                         'Corp Members ' + fin.[group] + ' 4D',
                         ISNULL(rul.[id], 0), 
                         fin.[theater_4d_difference], 
                         fin.[cust_memb_no], 
                         fin.[theater_4d_difference]
            FROM [#final_table]  AS fin
                 LEFT OUTER JOIN [dbo].[T_PRICING_RULE] AS rul ON rul.[description] = ('Corp Members ' + fin.[group] + ' 4D')
            WHERE fin.[row_num] = 1
            
            UNION SELECT fin.[source_no],
                         'Corp Members ' + fin.[group] + ' BG',
                         ISNULL(rul.[id], 0),
                         fin.[butterfly_garden_difference],
                         fin.[cust_memb_no],
                         fin.[butterfly_garden_difference]
            FROM [#final_table]  AS fin
                 LEFT OUTER JOIN [dbo].[T_PRICING_RULE] AS rul ON rul.[description] = ('Corp Members ' + fin.[group] + ' BG')
            WHERE fin.[row_num] = 1
            
            UNION SELECT fin.[source_no], 
                         'Corp Members ' + fin.[group] + ' SP',
                         ISNULL(rul.[id], 0),
                         fin.[special_exhibitions_difference],
                         fin.[cust_memb_no],
                         fin.[special_exhibitions_difference]
            FROM [#final_table]  AS fin
                 LEFT OUTER JOIN [dbo].[T_PRICING_RULE] AS rul ON rul.[description] = ('Corp Members ' + fin.[group] + ' SP')
            WHERE fin.[row_num] = 1
            
            --Delete records where no pricing rule id could be found
            DELETE FROM @price_type_source_list WHERE [rule_id] = 0;

            --SELECT * FROM @price_type_source_list WHERE [source_no] = 13814

        /*  Since each column set in each row can have its own colors, the pricing rule cursor will allow each one to be processed one at a time */

            DECLARE [pricing_rule_cursor] INSENSITIVE CURSOR FOR
            SELECT [source_no], [rule_id], [rule_description], [cust_memb_no], [difference]
            FROM @price_type_source_list
            
            OPEN [pricing_rule_cursor]
            
            FETCH NEXT FROM [pricing_rule_cursor] INTO @source_no, @rule_id, @rule_description, @cust_memb_no, @difference

            WHILE @@FETCH_STATUS <> -1 BEGIN
            
                /*  If the source does not exist on the pricing rule and the remaining passes (difference) <= 0, the color is changed to indicate the pass has already been disabled.
                    If the source does not exist on the pricing rule and the remaining passes (difference) > 0, the color is changed to indicate the pass will be re-enabled.
                       Renabling a pases (if necessary) is accomplised in a separate step at the end of the color update statements
                       To Add a source to or remove it from a pricing rule, I wrote a separate stored procedure called LP_ADD_REMOVE_SOURCE_ON_PRICING_RULE
                       To determine whether or not a source exists on a pricing rule, I write a function called LFS_SourceExistsOnPricingRule that returns Y or N  */

        

                IF (SELECT [dbo].[LFS_SourceExistsOnPricingRule] (@rule_id, @source_no)) = 'N' BEGIN

                    IF @rule_description LIKE '% EH'
                        UPDATE [#final_table] 
                        SET [exhibit_hall_backcolor] = CASE WHEN @difference <= 0 THEN @inactive_backcolor 
                                                            ELSE @readd_backcolor END, 
                            [exhibit_hall_forecolor] = CASE WHEN @difference <= 0 THEN @inactive_forecolor 
                                                            ELSE @readd_forecolor END
                        WHERE [source_no] = @source_no AND [cust_memb_no] = @cust_memb_no

                    ELSE IF @rule_description LIKE '% Omni'
                        UPDATE [#final_table] 
                        SET [omni_theater_backcolor] = CASE WHEN @difference <= 0 THEN @inactive_backcolor 
                                                            ELSE @readd_backcolor END, 
                            [omni_theater_forecolor] = CASE WHEN @difference <= 0 THEN @inactive_forecolor 
                                                            ELSE @readd_forecolor END 
                        WHERE [source_no] = @source_no AND [cust_memb_no] = @cust_memb_no

                    ELSE IF @rule_description LIKE '% PL'
                        UPDATE [#final_table] 
                        SET [planetarium_backcolor] = CASE WHEN @difference <= 0 THEN @inactive_backcolor 
                                                           ELSE @readd_backcolor END,
                            [planetarium_forecolor] = CASE WHEN @difference <= 0 THEN @inactive_forecolor 
                                                           ELSE @readd_forecolor END 
                        WHERE [source_no] = @source_no AND [cust_memb_no] = @cust_memb_no

                    ELSE IF @rule_description LIKE '% 4D'
                        UPDATE [#final_table] 
                        SET [theater_4d_backcolor] = CASE WHEN @difference <= 0 THEN @inactive_backcolor 
                                                          ELSE @readd_backcolor END, 
                            [theater_4d_forecolor] = CASE WHEN @difference <= 0 THEN @inactive_forecolor 
                                                          ELSE @readd_forecolor END 
                        WHERE [source_no] = @source_no AND [cust_memb_no] = @cust_memb_no

                    ELSE IF @rule_description LIKE '% BG'
                        UPDATE [#final_table] 
                        SET [butterfly_garden_backcolor] = CASE WHEN @difference <= 0 THEN @inactive_backcolor 
                                                                ELSE @readd_backcolor END, 
                            [butterfly_garden_forecolor] = CASE WHEN @difference <= 0 THEN @inactive_forecolor 
                                                                ELSE @readd_forecolor END 
                        WHERE [source_no] = @source_no AND [cust_memb_no] = @cust_memb_no

                    END ELSE IF @rule_description LIKE '% SP'
                        UPDATE [#final_table] 
                        SET [special_exhibitions_backcolor] = CASE WHEN @difference <= 0 THEN @inactive_backcolor 
                                                                   ELSE @readd_backcolor END, 
                            [special_exhibitions_forecolor] = CASE WHEN @difference <= 0 THEN @inactive_forecolor 
                                                                   ELSE @readd_forecolor END 
                        WHERE [source_no] = @source_no AND [cust_memb_no] = @cust_memb_no

                    --If the source does not exist on the pricing rule and the remaining passes (difference) > 0 and the report is not being run in Review Only mode,
                    --execute the [LP_ADD_REMOVE_SOURCE_ON_PRICING_RULE] stored procedure to add the source back to the pricing rule
                    IF @difference > 0 AND @review_only = 'N'
                        EXECUTE [dbo].[LP_ADD_REMOVE_SOURCE_ON_PRICING_RULE] @source_no = @source_no, @rule_id = @rule_id, @add_or_remove = 'Add'
                   
                ELSE IF (@difference <= 0 AND @review_only = 'N') BEGIN
                    
                    --If the source DOES exist on the pricing rule and the remaining passes (difference) <= 0 and the report is not being run in Review Only mode,
                    --execute the [LP_ADD_REMOVE_SOURCE_ON_PRICING_RULE] stored procedure to remove the source from the pricing rule
                   EXECUTE [dbo].[LP_ADD_REMOVE_SOURCE_ON_PRICING_RULE] @source_no = @source_no, @rule_id = @rule_id, @add_or_remove = 'Remove'

                END

                FETCH NEXT FROM [pricing_rule_cursor] INTO @source_no, @rule_id, @rule_description, @cust_memb_no, @difference

            END
            CLOSE [pricing_rule_cursor]
            DEALLOCATE [pricing_rule_cursor]

    FINISHED:
       
          /*  Pull the final data set to pass back to the report file  */

             SELECT [row_num],
                    [source_no],
                    [cust_memb_no],
                    [customer_no],
                    [source_name],
                    [customer_name],
                    [level_code],
                    [level_name],
                    [group],
                    [init_dt],
                    [expr_dt],
                    [order_count],
                    [exhibit_hall_limit],
                    [exhibit_hall_redeemed],
                    [exhibit_hall_difference],
                    [exhibit_hall_backcolor],
                    [exhibit_hall_forecolor],
                    [special_exhibitions_limit],
                    [special_exhibitions_redeemed],
                    [special_exhibitions_difference],
                    [special_exhibitions_backcolor],
                    [special_exhibitions_forecolor],
                    [omni_theater_limit],
                    [omni_theater_redeemed],
                    [omni_theater_difference],
                    [Omni_theater_backcolor],
                    [Omni_theater_forecolor],
                    [planetarium_limit],
                    [planetarium_redeemed],
                    [planetarium_difference],
                    [planetarium_backcolor],
                    [planetarium_forecolor],
                    [theater_4d_limit],
                    [theater_4d_redeemed],
                    [theater_4d_difference],
                    [theater_4d_backcolor],
                    [theater_4d_forecolor],
                    [butterfly_garden_limit],
                    [butterfly_garden_redeemed],
                    [butterfly_garden_difference],
                    [butterfly_garden_backcolor],
                    [butterfly_garden_forecolor]
             FROM [#final_table]
        
    DONE:

        --Clean Up
        IF OBJECT_ID('tempdb..#final_table') IS NOT NULL DROP TABLE [#final_table];
        IF OBJECT_ID('tempdb..#corp_pass_redemption') IS NOT NULL DROP TABLE [#corp_pass_redemption];
        IF OBJECT_ID('tempdb..#corp_pass_limits') IS NOT NULL DROP TABLE [#corp_pass_limits];
        IF OBJECT_ID('tempdb..#redemption_raw_data') IS NOT NULL DROP TABLE [#redemption_raw_data];

END

GO

----FOR TESTING
--EXECUTE [dbo].[LRP_CORP_MEMB_PASS_RECONCILIATION] @source_no_str = '', @almost_gone_threshold = 5, @review_only = 'Y'

--EXECUTE [dbo].[LP_ADD_REMOVE_SOURCE_ON_PRICING_RULE] @source_no = 13783, @rule_id = 1430, @add_or_remove = 'Add'
--EXECUTE [dbo].[LP_ADD_REMOVE_SOURCE_ON_PRICING_RULE] @source_no = 13793, @rule_id = 1439, @add_or_remove = 'Add'

--EXECUTE [dbo].[LP_ADD_REMOVE_SOURCE_ON_PRICING_RULE] @source_no = 13783, @rule_id = 1430, @add_or_remove = 'Remove'
--EXECUTE [dbo].[LP_ADD_REMOVE_SOURCE_ON_PRICING_RULE] @source_no = 13793, @rule_id = 1439, @add_or_remove = 'Remove'



--SELECT * FROM [dbo].[LTR_CORPORATE_PASS_LIMITS] WHERE id = 9
--SELECT TOP (2) * FROM [dbo].[TX_CUST_MEMBERSHIP] WHERE [customer_no] = 110214 ORDER BY [expr_dt] DESC
--SELECT * FROM [dbo].[LV_ACTIVE_CORPORATE_MEMBERS] WHERE source_no = 13947
--SELECT * FROM [dbo].[LV_ACTIVE_CORPORATE_MEMBERS] WHERE source_name like '%TJX%'


--SELECT [source_no] FROM T_ORDER WHERE [order_no] = 2477742
--SELECT [line_source_no] FROM [dbo].[T_LINEITEM] WHERE [order_no] = 2477743  --IN (2477685,2477734,2477735)

--SELECT * FROM T_ORDER WHERE [source_no] = 13796
--SELECT [line_source_no] FROM [dbo].[T_LINEITEM] WHERE [order_no] IN (2477742)



