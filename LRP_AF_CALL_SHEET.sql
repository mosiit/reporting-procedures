USE [impresario]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[LRP_AF_CALL_SHEET]
(
	@list_no INT
)    
AS

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
SET NOCOUNT ON 

--DECLARE @list_no INT = 508 -- 47 members

DECLARE @lastEventAttended TABLE
(
	customer_no INT,
	eventType INT,
	startdate DATETIME, 
	eventName VARCHAR(110),
	mostRecent INT -- bit, 1=Yes, 0=No
)

--
--GET LAST EVENT ATTENDED (MOSTRECENT = 1) AND LAST DISCOVERER EVENT ATTENDED.
--
INSERT INTO @lastEventAttended
        (customer_no,
         eventType,
		 startdate,
         eventName,
		 mostRecent)
SELECT X.customer_no, X.evnttype, X.start_dt, X.eventname, 0
FROM 
(
SELECT DISTINCT eg.customer_no, 
CASE ed.evnttype -- We want to look at evnttype =29 separately, if they exist. The other types are grouped together.
	WHEN 29 THEN 29
	ELSE 0
END AS evnttype,
ed.eventname, c.start_dt,
ROW_NUMBER() OVER (PARTITION BY eg.customer_no, CASE ed.evnttype WHEN 29 THEN 29 ELSE 0 END  ORDER BY eg.customer_no, c.start_dt DESC) AS rownum
FROM TX_EVENT_GUEST AS eg 
INNER JOIN T_CAMPAIGN AS c
	ON eg.campaign_no = c.campaign_no
INNER JOIN LTR_EVENT_DETAIL AS ed
	ON c.campaign_no = ed.campaign_no
INNER JOIN dbo.T_LIST_CONTENTS lst
	ON lst.customer_no = eg.customer_no
	AND lst.list_no = @list_no
WHERE
(eg.seat_no = ' ' OR eg.seat_no IS NULL) AND
eg.evex_no IN (SELECT evex_no FROM TX_EVENT_EXTRACT WHERE inv_status = 3) 
) X
WHERE X.rownum = 1 -- get most recent event attended 

UPDATE l
SET mostRecent = 1
FROM @lastEventAttended l
INNER JOIN 
(
SELECT customer_no, MAX(startdate) maxStrDt
FROM @lastEventAttended
GROUP BY customer_no
) X
ON X.customer_no = l.customer_no AND l.startdate = X.maxStrDt

--
--  GET HOME ADDRESS, PHONE AND CELL 
-- 

DECLARE @AddTbl TABLE
(	customer_no INT, 
	Address1 VARCHAR(100),
	Address2 VARCHAR(100),
	Address3 VARCHAR(100),
	City VARCHAR(100),
	StateCd VARCHAR(2),
	ZipCode VARCHAR(10),
	Phone VARCHAR(20),
	Cell VARCHAR(20)
)

INSERT INTO @AddTbl
(
	customer_no,
	Address1,
	Address2,
	Address3,
	City,
	StateCd,
	ZipCode,
	Phone,
	Cell
)
SELECT 
	lst.customer_no,
	ISNULL(home.Address1, ''),
	ISNULL(home.Address2, ''),
	ISNULL(home.Address3, ''),
	ISNULL(home.City, ''),
	ISNULL(home.StateCd, ''),
	ISNULL(home.ZipCode, ''),
	home.phone,
	dbo.AF_FORMAT_STRING(cell.phone, '@@@-@@@-@@@@ @@@@') AS cell
FROM dbo.T_LIST_CONTENTS lst
	LEFT JOIN T_phone cell
		ON cell.customer_no = lst.customer_no
		AND cell.type = 5
	OUTER APPLY dbo.LFT_BOARD_GetAddress(lst.customer_no, 'Home', 0) home
WHERE lst.list_no = @list_no

--
-- GET BOARD INFORMATION
-- WE WANT TO PULL ANY BOARD MEMBERS (ADULT AFFLIATIONS) THAT ARE AFFILIATED WITH THE CONSTITUENT RECORD 
-- 
DECLARE @boardTbl TABLE 
(
	customer_no INT,
	brdDesc VARCHAR(500)
)

INSERT INTO @boardTbl
        (customer_no,
         brdDesc)
SELECT DISTINCT --a.customer_no, a.expanded_customer_no, brd.description, cust.fname, nm.description
a.customer_no, cust.fname + ' - '+brd.description+'    '+nm.description AS fullDesc
FROM dbo.LV_CUSTOMER_WITH_ALL_ADULT_AFFILIATIONS AS a
	INNER JOIN dbo.LV_BOARD_MEMBERS brd
		ON a.expanded_customer_no = brd.individual_customer_no
	INNER JOIN dbo.T_CUSTOMER cust
		ON a.expanded_customer_no = cust.customer_no
	INNER JOIN dbo.T_LIST_CONTENTS lst
		ON lst.customer_no = a.customer_no
		AND lst.list_no = @list_no
	LEFT OUTER JOIN dbo.TR_NAMESTATUS nm
		ON cust.name_status = nm.id;



WITH BoardRelationships AS
( -- This statement will concatenate multiple board members into a comma seperated list
SELECT customer_no, 
    abc = STUFF(
                 (SELECT ',' + LTRIM(RTRIM(brdDesc)) FROM @boardTbl b1 WHERE b1.customer_no = b2.customer_no FOR XML PATH ('')), 1, 1, ''
               ) 
FROM @boardTbl b2 
GROUP BY customer_no
)
SELECT c.customer_no,
	CASE 
		WHEN c.cust_type = 7 THEN c.lname
		WHEN c.cust_type <> 7 AND sal.default_ind = 'Y' THEN sal.esal1_desc
	END AS custName, 
	familiarSal.lsal_desc AS FamiliarSal,
	brd.abc AS BrdAffiliation,
	adr.Address1,
	adr.Address2,
	adr.Address3,
	adr.City,
	adr.StateCd,
	adr.ZipCode,
	adr.Phone,
	adr.Cell,
	persMemb.PersonalMembType, 
	persMemb.PersonalMembExpirationDate,
	ask.description CampaignName,
	ask.ask_amt ,
	lea.eventName as LastEventAttended,
	lastDiscovererEvent.eventName AS lastDiscovererEvent,
	c.sort_name 
--select count(*)
FROM t_customer c
	INNER JOIN dbo.T_LIST_CONTENTS lst
		ON lst.customer_no = c.customer_no
		AND lst.list_no = @list_no
	INNER JOIN TX_CUST_SAL sal
		ON sal.customer_no = c.customer_no
		AND sal.default_ind = 'Y' 
	left JOIN dbo.TX_CUST_SAL familiarSal
		ON familiarSal.customer_no = c.customer_no
		AND familiarSal.signor = 2
	LEFT JOIN @AddTbl adr
		ON adr.customer_no = c.customer_no
	LEFT JOIN BoardRelationships brd
		ON brd.customer_no = c.customer_no
	LEFT JOIN 
	(
		SELECT p.customer_no, c.description, p.ask_amt
		FROM T_PLAN AS p
		INNER JOIN T_CAMPAIGN AS c
		ON p.campaign_no = c.campaign_no
		WHERE 
		c.category in (28,7) and 
		c.fyear IN (SELECT fyear FROM dbo.TR_BATCH_Period WHERE GETDATE() BETWEEN start_dt AND end_dt)
	) ask
	ON ask.customer_no = c.customer_no 
	LEFT JOIN @lastEventAttended lea
		ON lea.customer_no = c.customer_no
		AND lea.mostRecent = 1
	LEFT JOIN  @lastEventAttended lastDiscovererEvent
		ON lastDiscovererEvent.customer_no = c.customer_no
		AND lastDiscovererEvent.eventType = 29
OUTER APPLY dbo.LFT_BOARD_GetPersonalMembership(c.customer_no) persMemb
ORDER BY sort_name



