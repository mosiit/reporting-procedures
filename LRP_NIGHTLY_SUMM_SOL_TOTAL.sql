USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LRP_NIGHTLY_SUMM_SOL_TOTAL]    Script Date: 12/14/2020 2:26:58 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[LRP_NIGHTLY_SUMM_SOL_TOTAL]
	@section VARCHAR(300),
	@sortOrder INT,
	@runBy	VARCHAR(300),
	@fystart DATETIME,
	@fyend DATETIME
AS

BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE @tblPassOne	TABLE	(
								customer_no	INT,
								value			MONEY
								)

	DECLARE @tblPassTwo TABLE	(
								customer_no		INT,
								value			MONEY
								)

	DECLARE @tblPassBoth TABLE	(
								customer_no		INT,
								value			MONEY
								)

	INSERT INTO @tblPassOne
		SELECT  t.customer_no,
				ISNULL(SUM(c.cont_amt), 0)
		FROM    T_CONTRIBUTION AS c
		INNER JOIN T_CAMPAIGN AS g ON c.campaign_no = g.campaign_no
		INNER JOIN T_CUSTOMER AS t ON c.worker_customer_no = t.customer_no
		WHERE   c.cont_type IN ('G', 'P')
				AND c.cont_amt > 0
				AND c.worker_customer_no IS NOT NULL
				AND c.worker_customer_no <> '2653100'
				AND CAST(c.cont_dt AS DATE) >= @fystart
				AND CAST(c.cont_dt AS DATE) <= @fyend
				--2020/12/14, H. Sheridan.  Service Now ticket SCTASK0001710 - add criteria for campaign category
				AND	g.category NOT IN (8,9)
        GROUP BY t.customer_no

	--SELECT 'debug 1', * FROM @tblNightSumm

	INSERT INTO @tblPassTwo
		SELECT  cp.customer_no,
				ISNULL(SUM(c.cont_amt), 0)
		FROM    T_CONTRIBUTION AS c
		INNER JOIN T_CAMPAIGN AS g ON c.campaign_no = g.campaign_no
		INNER JOIN T_PLAN AS p ON c.plan_no = p.plan_no
		INNER JOIN TX_CUST_PLAN AS cp ON p.plan_no = cp.plan_no
		WHERE   c.cont_type IN ('G', 'P')
				AND c.cont_amt > 0
				AND c.worker_customer_no = '2653100'
				AND CAST(c.cont_dt AS DATE) >= @fystart
				AND CAST(c.cont_dt AS DATE) <= @fyend
				AND cp.role_no = '1'
				--2020/12/14, H. Sheridan.  Service Now ticket SCTASK0001710 - add criteria for campaign category
				AND	g.category NOT IN (8,9)
        GROUP BY cp.customer_no

	INSERT INTO @tblPassBoth
		SELECT * FROM @tblPassOne

	INSERT INTO @tblPassBoth
		SELECT * FROM @tblPassTwo

	--SELECT * FROM @tblPassBoth

	INSERT INTO LT_NIGHTLY_SUMMARY
		SELECT  customer_no,
				@section,
				SUM(ISNULL(value, 0)),
				'',
				@sortOrder,
				GETDATE(),
				@runBy
		FROM	@tblPassBoth
		GROUP BY customer_no

END


GO


