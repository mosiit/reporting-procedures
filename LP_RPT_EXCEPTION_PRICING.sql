DECLARE @source_dt datetime,
        @source_code varchar(10),
        @check_start_dt datetime, 
        @check_end_dt datetime,
        @price_check_level varchar(10),
        @exclude_buyout_productions char(1),
        @return_message varchar(100)
SELECT @source_dt = '4-1-2016', @source_code = 'O0401WILD', @price_check_level = 'title', @exclude_buyout_productions = 'Y'

    DECLARE @performance_no int, @source_date char(10), @check_start_date char(10), @check_end_date char(10), @source_prefix char(1), @source_abbrev char(4)
    DECLARE @price_type_name varchar(40), @start_price decimal(18,2), @start_min_price decimal(18,2),@perf_code char(10)
    DECLARE @source_table table ([title_name] varchar(30), [perf_no] int, [source_date] char(10), [source_code] varchar(10), [perf_time] char(8), [price_type_name] varchar(30), [start_price] decimal(18,2),
                                 [start_min_price] decimal(18,2), [edit_ind] char(1))
    DECLARE @check_table table ([title_name] varchar(30), [perf_no] int, [perf_date] char(10), [perf_code] varchar(10), [perf_time] char(8), [price_type_name] varchar(30), [start_price] decimal(18,2),
                                [start_min_price] decimal(18,2), [edit_ind] char(1), [problem_ind] char(2))

    DECLARE @price_exceptions table ([title_name] varchar(30), [perf_no] int, [perf_date] char(10), [perf_code] varchar(10), [perf_time] char(8), [price_type_name] varchar(30), 
                                     [start_price] decimal(18,2), [source_start_price] decimal(18,2), [start_min_price] decimal(18,2), [source_start_min_price] decimal(18,2), 
                                     [edit_ind] char(1), [problem_ind] char(2), [problem_text] varchar(100))


    SELECT @return_message = ''

    SELECT @exclude_buyout_productions = IsNull(@exclude_buyout_productions, 'Y')

    SELECT @source_dt = IsNull(@source_dt, getdate())
    SELECT @source_date = convert(char(10),@source_dt,111)

    SELECT @source_prefix = left(ltrim(@source_code),1)
    SELECT @source_prefix = IsNull(@source_prefix, '')

    SELECT @source_abbrev = substring(ltrim(@source_code), 6,4)
    SELECT @source_abbrev = IsNUll(@source_abbrev, '')
       
    SELECT @check_start_dt = isnull(@check_start_dt, @source_dt)

    IF @check_end_dt is null BEGIN
        IF @price_check_level = 'Production'
            SELECT @check_end_dt = max([performance_dt]) FROM LV_PRODUCTION_ELEMENTS_PERFORMANCE WHERE production_name_abbreviated = @source_abbrev
        ELSE
            SELECT @check_end_dt = max([performance_dt]) FROM LV_PRODUCTION_ELEMENTS_PERFORMANCE WHERE perf_code_prefix = @source_prefix

        SELECT @check_end_dt = IsNull(@check_end_dt, dateadd(day,30,@check_start_dt))
    END

    SELECT @performance_no = performance_no FROM LV_PRODUCTION_ELEMENTS_PERFORMANCE WHERE performance_date = @source_date and performance_code = @source_code
    SELECT @performance_no = IsNull(@performance_no, 0)
    IF @performance_no <= 0 BEGIN
        SELECT @return_message = 'invalid source code (' + @source_code + ')'
        GOTO DONE
    END
    
    SELECT @check_start_date = convert(char(10), @check_start_dt,111)
    SELECT @check_end_date = convert(char(10), @check_end_dt,111)

    INSERT INTO @source_table       
    SELECT zmp.[title_name], ppr.[perf_no], @source_date, @source_code, convert(char(8),convert(datetime,zmp.[zone_time]),108) as 'Time', typ.[description], ppr.[start_price], ppr.[start_min_price], ppr.[edit_ind]
    FROM [dbo].[T_PERF_PRICE] as ppr
         INNER JOIN [dbo].[LV_SS_ZONE_MAPS] as zmp ON zmp.[zone_no] = ppr.[zone_no]
         INNER JOIN [dbo].[T_PERF_PRICE_TYPE] as ptp ON ptp.[id] = ppr.[perf_price_type]
         INNER JOIN [dbo].[TR_PRICE_TYPE] as typ ON typ.[id] = ptp.[price_type]
    WHERE ppr.[perf_no] = @performance_no and ppr.[start_enabled] = 'Y' and typ.[inactive] = 'N'

    DELETE FROM @source_table WHERE [perf_time] <> (SELECT min([perf_time]) FROM @source_table)

    IF not exists (SELECT * FROM @source_table) BEGIN
        SELECT @return_message = 'Unable to determine pricing for the source code.'
        GOTO DONE
    END
    
    IF @price_check_level = 'Production'
        INSERT INTO @check_table
        SELECT zmp.[title_name], ppr.[perf_no], convert(char(10),prf.[perf_dt],111), prf.[perf_code], convert(char(8),convert(datetime,zmp.[zone_time]),108) as 'Time', 
               typ.[description], ppr.[start_price], ppr.[start_min_price], ppr.[edit_ind], 'NP'
        FROM [dbo].[T_PERF_PRICE] as ppr
             INNER JOIN [dbo].[LV_SS_ZONE_MAPS] as zmp ON zmp.[zone_no] = ppr.[zone_no]
             INNER JOIN [dbo].[T_PERF_PRICE_TYPE] as ptp ON ptp.[id] = ppr.[perf_price_type]
             INNER JOIN [dbo].[TR_PRICE_TYPE] as typ ON typ.[id] = ptp.[price_type]
             INNER JOIN [dbo].[T_PERF] as prf ON prf.[perf_no] = ppr.[perf_no]
        WHERE convert(char(10),prf.[perf_dt],111) between @check_start_date and @check_end_date and ppr.[start_enabled] = 'Y' and typ.[inactive] = 'N'
              and substring(prf.[perf_code], 6, 4) = @source_abbrev
        ORDER BY prf.[perf_code], convert(char(8),convert(datetime,zmp.[zone_time]),108), typ.[description]
    ELSE
        INSERT INTO @check_table
        SELECT zmp.[title_name], ppr.[perf_no], convert(char(10),prf.[perf_dt],111), prf.[perf_code], convert(char(8),convert(datetime,zmp.[zone_time]),108) as 'Time', 
               typ.[description], ppr.[start_price], ppr.[start_min_price], ppr.[edit_ind], 'NP'
        FROM [dbo].[T_PERF_PRICE] as ppr
             INNER JOIN [dbo].[LV_SS_ZONE_MAPS] as zmp ON zmp.[zone_no] = ppr.[zone_no]
             INNER JOIN [dbo].[T_PERF_PRICE_TYPE] as ptp ON ptp.[id] = ppr.[perf_price_type]
             INNER JOIN [dbo].[TR_PRICE_TYPE] as typ ON typ.[id] = ptp.[price_type]
             INNER JOIN [dbo].[T_PERF] as prf ON prf.[perf_no] = ppr.[perf_no]
        WHERE convert(char(10),prf.[perf_dt],111) between @check_start_date and @check_end_date and ppr.[start_enabled] = 'Y' and typ.[inactive] = 'N'
              and left(prf.[perf_code], 1) = @source_prefix
        ORDER BY prf.[perf_code], convert(char(8),convert(datetime,zmp.[zone_time]),108), typ.[description]

        IF @exclude_buyout_productions = 'Y' DELETE FROM @check_table WHERE perf_code like '%buy%'


        INSERT INTO @Price_Exceptions
        SELECT chk.[title_name], chk.[perf_no], chk.[perf_date], chk.[perf_code], chk.[perf_time], chk.[price_type_name], chk.[start_price], src.[start_price] as 'source_price', chk.[start_min_price], src.[start_min_price] as 'source_min_price',
               chk.[edit_ind], 'EP', 'Extra Price'
        FROM @check_table as chk 
             INNER JOIN @source_table as src ON src.[price_type_name] = chk.[price_type_name]
        WHERE chk.[price_type_name] not in (SELECT [price_type_name] FROM @source_table)


        DECLARE price_exists_cursor INSENSITIVE CURSOR FOR
        SELECT DISTINCT [perf_code] FROM @check_table
        OPEN price_exists_cursor
        BEGIN_PRICE_EXISTS_LOOP:

            FETCH NEXT FROM price_exists_cursor INTO @perf_code
            IF @@FETCH_STATUS = -1 GOTO END_PRICE_EXISTS_LOOP

            INSERT INTO @Price_Exceptions
            SELECT chk.[title_name], chk.[perf_no], chk.[perf_date], chk.[perf_code], chk.[perf_time], chk.[price_type_name], chk.[start_price], src.[start_price] as 'source_price', chk.[start_min_price], src.[start_min_price] as 'source_min_price',
                   chk.[edit_ind], 'MP', 'Missing Price'
            FROM @check_table as chk 
                 LEFT OUTER JOIN @source_table as src ON src.[price_type_name] = chk.[price_type_name]
            WHERE chk.[perf_code] = @perf_code and src.[price_type_name] is null

            GOTO BEGIN_PRICE_EXISTS_LOOP

        END_PRICE_EXISTS_LOOP:
        CLOSE price_exists_cursor
        DEALLOCATE price_exists_cursor
              
        DECLARE price_check_cursor INSENSITIVE CURSOR FOR
        SELECT price_type_name, start_price, start_min_price FROM @source_table order by price_type_name
        OPEN price_check_cursor
        BEGIN_PRICE_CHECK_LOOP:

            FETCH NEXT FROM price_check_cursor INTO @price_type_name, @start_price, @start_min_price
            IF @@FETCH_STATUS = -1 GOTO END_PRICE_CHECK_LOOP
            
            UPDATE @check_table SET problem_ind = 'WP' WHERE [price_type_name] = @price_type_name and start_price <> @start_price
           
            UPDATE @check_table SET problem_ind = 'WM' WHERE [price_type_name] = @price_type_name and problem_ind = 'NP' and start_min_price <> @start_min_price
            
           GOTO BEGIN_PRICE_CHECK_LOOP
            
        END_PRICE_CHECK_LOOP:
        CLOSE price_check_cursor
        DEALLOCATE price_check_cursor

        INSERT INTO @Price_Exceptions
        SELECT chk.[title_name], chk.[perf_no], chk.[perf_date], chk.[perf_code], chk.[perf_time], chk.[price_type_name], chk.[start_price], src.[start_price] as 'source_price', chk.[start_min_price], src.[start_min_price] as 'source_min_price',
               chk.[edit_ind], chk.[problem_ind], ''
        FROM @check_table as chk 
             INNER JOIN @source_table as src ON src.[price_type_name] = chk.[price_type_name]
        WHERE chk.[problem_ind] <> 'NP'
        ORDER BY chk.[perf_date], chk.[perf_time], chk.[price_type_name]
        
         
    IF @return_message = '' SELECT @return_message = 'success'

    DONE:

        PRINT @return_message
        SELECT * FROM @price_exceptions




    --SELECT * FROM T_ZONE
    --SELECT * FROM LV_SS_ZONE_MAPS
    --SELECT * FROM TR_PRICE_TYPE 
    --SELECT * FROM TX_SALES_LAYOUT_PRICE_TYPE


--    SELECT * FROM T_PERF_PRICE_TYPE
    --SELECT * FROM T_PERF_PRICE_LAYER WHERE perf_no = @performance_no