USE [impresario]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[LRP_MOS_ADVANCEMENT_CONTRIBUTION_STEW_DETAIL]
	@cont_start_dt DATETIME = NULL,
	@cont_end_dt DATETIME = NULL,
	@camp_category_str VARCHAR(4000) = NULL,
	@designation_str VARCHAR(4000) = NULL,
	@soft_credit_type varchar(4000) = NULL, 
	@customer_no INT = 0,
	@list_no INT = 0
AS
BEGIN
-- =============================================
-- Author: Debbie Jacob
-- Create Date: 2/16/17, 12/7/2017


-- This SP is called in 3 reports
-- 1. MOS: Advancement Contribution Stewardship Detail
--		- Start Date and End Date are required here (requirement is enforced by the report setup
-- 2. MOS: Advancement Detail Giving History
--		- List No or Customer_No are required here (requirement is enforced by code below in SP)
-- 3. MOS: Contribution Sum By Date
--		- Start Date and End Date are required here (requirement is enforced by the report setup

--EXEC	[dbo].[LRP_MOS_ADVANCEMENT_CONTRIBUTION_STEW_DETAIL]
--		@cont_start_dt = N'7/1/2013',
--		@cont_end_dt = N'6/30/2025',
--		@camp_category_str = NULL,
--		@designation_str = NULL,
--		@soft_credit_type = NULL,
--		@customer_no = 179923

-- Copied from : 
	-- Author: Alex Harris
	-- Create date: May 16, 2016
	-- Description:	Advancement Contribution Detail Report
-- =============================================

/* Sample Exec:
Execute dbo.LRP_MOS_ADVANCEMENT_CONTRIBUTION_STEW_DETAIL
	@cont_start_dt = '1/1/1945',
	@cont_end_dt = '2/27/2017',
	@customer_no = 10184
*/	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;

	-- NOTE: @cont_start_dt AND @cont_end_dt are both required by MOS: Advancement Contribution Stewardship Detail report.
	-- However, in MOS: Advancement Detail Giving History report, @cont_start_dt AND @cont_end_dt are not used 
	--	but either @customer_no or @list_no MUST BE FILLED IN 
	--IF @cont_start_dt IS NULL AND @cont_end_dt IS NULL AND ISNULL(@list_no,0) = 0 AND ISNULL(@customer_no,0) = 0
	--	RETURN

	-- SETTING DEFAULT DATES
	IF @cont_start_dt IS NULL 
	    SET @cont_start_dt = CONVERT(DATETIME,'1900-01-01')

	IF @cont_end_dt IS NULL 
	    SET @cont_end_dt = CONVERT(DATETIME,'2999-12-31')

	-- SETTING CAMPAIGN CATEGORIES
	DECLARE	@camp_categories TABLE (camp_category INT NOT NULL)
	IF ISNULL(@camp_category_str,'') <> ''
		INSERT INTO @camp_categories (camp_category)
		SELECT Element From dbo.FT_SPLIT_LIST(@camp_category_str,',')
	ELSE
		INSERT INTO @camp_categories (camp_category)
		SELECT id
		FROM dbo.TR_CAMPAIGN_CATEGORY

	-- SETTING DESIGNATIONS
	DECLARE @designations TABLE (cont_designation INT NOT NULL)
	IF ISNULL(@designation_str,'') <> ''
		INSERT INTO @designations (cont_designation)
		SELECT Element From dbo.FT_SPLIT_LIST(@designation_str,',')
	ELSE
		INSERT INTO @designations (cont_designation)
		SELECT id
		FROM dbo.TR_CONT_DESIGNATION;

	-- SETTING SOFT CREDIT TYPES
	DECLARE @softCreditTypes TABLE (scType VARCHAR(30) NOT NULL)
	IF ISNULL(@soft_credit_type, '') <> ''
	BEGIN 

		SET @soft_credit_type = REPLACE(@soft_credit_type,'"', '')

		INSERT INTO @softCreditTypes(scType)
		SELECT Element From dbo.FT_SPLIT_LIST(@soft_credit_type,',')
	END 
	ELSE 
		INSERT INTO @softCreditTypes(scType)
		SELECT key_value 
		FROM t_kwcoded_values 
		WHERE keyword_no = 456

	-- SETTING THE CUSTOMER LIST 
	CREATE TABLE #customerList (customer_no INT)

	INSERT INTO #customerList (customer_no)
	SELECT customer_no
	FROM dbo.T_CUSTOMER
	WHERE ISNULL(@list_no,0) = 0 AND ISNULL(@customer_no,0) = 0
	UNION
	SELECT c.customer_no
	FROM t_customer c
	INNER JOIN dbo.T_LIST_CONTENTS l
	ON l.customer_no = c.customer_no
	AND ISNULL(list_no,0) > 0 AND l.list_no = @list_no
	UNION
	SELECT @customer_no
	WHERE ISNULL(@customer_no,0) <> 0 

	-- Gifts and Pledges - Hard Credit
	SELECT	DISTINCT reference_no = c.ref_no,
			customer_number = c.customer_no,
			name = CASE WHEN cust.cust_type = 7 THEN cust.lname ELSE cs.esal1_desc END,
			type = c.cont_type,
			creditee_type = '',
			date = CONVERT(DATE,c.cont_dt),
			fiscal_year = bp.fyear,
			fiscal_quarter = bp.quarter,
			fiscal_period = bp.period,
			contribution_amount = c.cont_amt,
			received_amount = c.recd_amt,
			anonymous = kv.key_value,
			soft_credit_type = c.custom_1,
			fund_description = coa.fund_description,
			printable_fund = coa.printable_fund,
			designation = coa.designation,
			overall_campaign = coa.overall_cm,
			campaign = cm.description,
			campaign_category = coa.cm_category,
			account_goal = coa.acct_goal,
			account_group = coa.acct_grp,
			appeal = a.description,
			media = mt.description,
			source = amt.source_name,
			notes = REPLACE(REPLACE(c.notes,CHAR(13),'/'),CHAR(10),'/'),
			cancel = c.cancel, --they've requested we leave this out of the report, but I'm leaving it here for now in case they change their minds soon
			pledge_status_desc = ps.description,
			billing_type = bt.description,
			batch = c.batch_no,
			KG_xfer_dt = c.KG_xfer_dt,
			KGift_desc = c.KGift_desc,
			stock_ticker = c.custom_2,
			contribution_detail1 = kv3.key_value,
			contribution_detail2 = kv4.key_value,
			agreement_date = c.custom_6,
			challenge_earned = c.custom_7,
			pg_instrument = kv5.key_value,
			solicitation = c.custom_0,
			solicitor_number = c.worker_customer_no,
			solicitor_name = WCS.esal1_desc,
			cpf_flag = coa.cpf_flag,
			exh_prog_cat = coa.exh_prog_cat,
			full_fund_name = coa.full_fund,
			cm_category1 = coa.cm_category1,
			cm_intermediate1 = coa.cm1_intermediatelevel,
			cm_pillar1 = coa.cm1_main_level,
			cm_category2 = coa.cm_category2,
			cm_intermediate2 = coa.cm2_intermediate_level,
			cm_category3 = coa.cm_category3,
			cm_intermediate3 = coa.cm3_intermediate_level,
			custom_2 = c.custom_2,
			custom_9 = c.custom_9,
			main_customer_type = ct.description,
			original_customer_type = ct.description,
			sort_name = cust.sort_name,
			chan.description AS Channel
	FROM dbo.T_CONTRIBUTION AS c
	JOIN dbo.T_CUSTOMER AS cust ON c.customer_no = cust.customer_no
	INNER JOIN #customerList list 
		ON list.customer_no = cust.customer_no
	JOIN dbo.TR_CUST_TYPE AS ct ON cust.cust_type = ct.id
	JOIN dbo.TX_CUST_SAL AS cs ON c.customer_no = cs.customer_no AND cs.default_ind = 'Y'
	JOIN dbo.TR_SALES_CHANNEL AS chan ON chan.id = c.channel
	LEFT JOIN TX_CUST_SAL WCS ON C.worker_customer_no = WCS.customer_no AND WCS.default_ind = 'Y' 
	LEFT JOIN dbo.T_KWCODED_VALUES AS kv ON c.custom_5 = kv.key_value AND kv.keyword_no IN (460)
	LEFT JOIN dbo.T_KWCODED_VALUES AS kv2 ON c.custom_1 = kv2.key_value AND kv2.keyword_no IN (456)
	JOIN dbo.TR_Batch_Period AS bp ON c.cont_dt BETWEEN bp.start_dt AND bp.end_dt
	JOIN dbo.T_CAMPAIGN AS cm ON c.campaign_no = cm.campaign_no
	JOIN dbo.LV_MOS_CHART_OF_ACCOUNTS AS coa ON c.fund_no = coa.fund_no AND cm.campaign_no = coa.campaign_no 
	JOIN dbo.T_APPEAL AS a ON c.appeal_no = a.appeal_no
	JOIN dbo.TR_MEDIA_TYPE AS mt ON c.media_type = mt.id
	JOIN dbo.TX_APPEAL_MEDIA_TYPE AS amt ON c.source_no = amt.source_no
	LEFT JOIN dbo.TR_BILLING_TYPE AS bt ON c.billing_type = bt.id
	LEFT JOIN dbo.T_KWCODED_VALUES AS kv3 ON c.custom_3 = kv3.key_value AND kv3.keyword_no IN (458)
	LEFT JOIN dbo.T_KWCODED_VALUES AS kv4 ON c.custom_4 = kv4.key_value AND kv4.keyword_no IN (459)
	LEFT JOIN dbo.T_KWCODED_VALUES AS kv5 ON c.custom_8 = kv5.key_value AND kv5.keyword_no IN (463)
	LEFT JOIN dbo.TX_CUST_SAL AS cs2 ON c.worker_customer_no = cs2.customer_no AND cs2.default_ind = 'Y'
	JOIN @camp_categories AS cat ON cm.category = cat.camp_category
	JOIN @designations AS des ON coa.desig_id = des.cont_designation
	JOIN @softCreditTypes sct
		ON sct.scType = c.custom_1
	LEFT JOIN TR_PLEDGE_STATUS PS ON C.pledge_status = PS.id 
	WHERE c.cont_dt BETWEEN @cont_start_dt AND @cont_end_dt
	AND c.cont_amt > 0 
	AND ISNULL(c.custom_1,'(none)') IN ('(none)','Matching Gift Credit') 
	AND c.cont_type IN ('G','P')
	
	UNION ALL

	-- Gifts and Pledges - Soft Credit
	SELECT	DISTINCT reference_no = c.ref_no,
			customer_number = cr.creditee_no,
			name = CASE WHEN cust2.cust_type = 7 THEN cust2.lname ELSE cs.esal1_desc END,
			type = c.cont_type,
			creditee_type = crt.descriptiuon,
			date = CONVERT(DATE,c.cont_dt),
			fiscal_year = bp.fyear,
			fiscal_quarter = bp.quarter,
			fiscal_period = bp.period,
			contribution_amount = c.cont_amt,
			received_amount = c.recd_amt,
			anonymous = kv.key_value,
			soft_credit_type = c.custom_1,
			fund_description = coa.fund_description,
			printable_fund = coa.printable_fund,
			designation = coa.designation,
			overall_campaign = coa.overall_cm,
			campaign = cm.description,
			campaign_category = coa.cm_category,
			account_goal = coa.acct_goal,
			account_group = coa.acct_grp,
			appeal = a.description,
			media = mt.description,
			source = amt.source_name,
			notes = REPLACE(REPLACE(c.notes,CHAR(13),'/'),CHAR(10),'/'),
			cancel = c.cancel, --they've requested we leave this out of the report, but I'm leaving it here for now in case they change their minds soon
			pledge_status_desc = ps.description,
			billing_type = bt.description,
			batch = c.batch_no,
			KG_xfer_dt = c.KG_xfer_dt,
			KGift_desc = c.KGift_desc,
			stock_ticker = c.custom_2,
			contribution_detail1 = kv3.key_value,
			contribution_detail2 = kv4.key_value,
			agreement_date = c.custom_6,
			challenge_earned = c.custom_7,
			pg_instrument = kv5.key_value,
			solicitation = c.custom_0,
			solicitor_number = c.worker_customer_no,
			solicitor_name = WCS.esal1_desc,
			cpf_flag = coa.cpf_flag,
			exh_prog_cat = coa.exh_prog_cat,
			full_fund_name = coa.full_fund,
			cm_category1 = coa.cm_category1,
			cm_intermediate1 = coa.cm1_intermediatelevel,
			cm_pillar1 = coa.cm1_main_level,
			cm_category2 = coa.cm_category2,
			cm_intermediate2 = coa.cm2_intermediate_level,
			cm_category3 = coa.cm_category3,
			cm_intermediate3 = coa.cm3_intermediate_level,
			custom_2 = c.custom_2,
			custom_8 = c.custom_9,
			main_customer_type = ct2.description,
			original_customer_type = ct.description,
			sort_name = cust2.sort_name,
			chan.description AS Channel 
	FROM dbo.T_CONTRIBUTION AS c
	JOIN dbo.T_CUSTOMER AS cust ON c.customer_no = cust.customer_no
	JOIN dbo.TR_CUST_TYPE AS ct ON cust.cust_type = ct.id
	JOIN dbo.TR_SALES_CHANNEL AS chan ON chan.id = c.channel
	LEFT JOIN TX_CUST_SAL WCS ON C.worker_customer_no = WCS.customer_no AND WCS.default_ind = 'Y' 
	JOIN dbo.T_CREDITEE AS cr ON c.ref_no = cr.ref_no
	INNER JOIN #customerList list 
		ON list.customer_no = cr.creditee_no
	JOIN dbo.TR_CREDITEE_TYPE AS crt ON cr.creditee_type = crt.id
	JOIN dbo.T_CUSTOMER AS cust2 ON cr.creditee_no = cust2.customer_no
	JOIN dbo.TR_CUST_TYPE AS ct2 ON cust2.cust_type = ct2.id
	JOIN dbo.TX_CUST_SAL AS cs ON cr.creditee_no = cs.customer_no AND cs.default_ind = 'Y'
	LEFT JOIN dbo.T_KWCODED_VALUES AS kv ON c.custom_5 = kv.key_value AND kv.keyword_no IN (460)
	LEFT JOIN dbo.T_KWCODED_VALUES AS kv2 ON c.custom_1 = kv2.key_value AND kv2.keyword_no IN (456)
	JOIN dbo.TR_Batch_Period AS bp ON c.cont_dt BETWEEN bp.start_dt AND bp.end_dt
	JOIN dbo.T_CAMPAIGN AS cm ON c.campaign_no = cm.campaign_no
	JOIN dbo.LV_MOS_CHART_OF_ACCOUNTS AS coa ON c.fund_no = coa.fund_no AND cm.campaign_no = coa.campaign_no 
	JOIN dbo.T_APPEAL AS a ON c.appeal_no = a.appeal_no
	JOIN dbo.TR_MEDIA_TYPE AS mt ON c.media_type = mt.id
	JOIN dbo.TX_APPEAL_MEDIA_TYPE AS amt ON c.source_no = amt.source_no
	LEFT JOIN dbo.TR_BILLING_TYPE AS bt ON c.billing_type = bt.id
	LEFT JOIN dbo.T_KWCODED_VALUES AS kv3 ON c.custom_3 = kv3.key_value AND kv3.keyword_no IN (458)
	LEFT JOIN dbo.T_KWCODED_VALUES AS kv4 ON c.custom_4 = kv4.key_value AND kv4.keyword_no IN (459)
	LEFT JOIN dbo.T_KWCODED_VALUES AS kv5 ON c.custom_8 = kv5.key_value AND kv5.keyword_no IN (463)
	LEFT JOIN dbo.TX_CUST_SAL AS cs2 ON c.worker_customer_no = cs2.customer_no AND cs2.default_ind = 'Y'
	JOIN @camp_categories AS cat ON cm.category = cat.camp_category
	JOIN @designations AS des ON coa.desig_id = des.cont_designation
	JOIN @softCreditTypes sct
		ON sct.scType = c.custom_1
	LEFT JOIN TR_PLEDGE_STATUS PS ON C.pledge_status = PS.id 
	WHERE c.custom_1 IN ('Stewardship Soft Credit','Matching Gift Credit','Primary Soft Credit')
	AND c.cont_type IN ('G','P')
	AND cr.creditee_type IN (1,5,10,12,14,15,16)
	AND	c.cont_dt BETWEEN @cont_start_dt AND @cont_end_dt
	AND c.cont_amt > 0 
	AND (
		c.customer_no IN (SELECT customer_no FROM #customerList)
		OR 
		cr.creditee_no IN (SELECT customer_no FROM #customerList)
		)
	ORDER BY sort_name

	DROP TABLE #customerList   
END

GO


