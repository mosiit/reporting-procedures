USE [impresario]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[LRP_GIFT_NOTIFICATION]
(
	@step_date DATETIME
)
AS
SET NOCOUNT ON
/************************************************************************************************
-- DSJ 3/8/2017
-- Loosely copied from RP_STEP_DETAIL

EXEC [dbo].[LRP_GIFT_NOTIFICATION] '2/1/2017'
***********************************************************************************************/

CREATE TABLE #StepData (
		step_no int, 
		plan_no int, 
		customer_no int, 
		constituent_display_name varchar(160),
		constituent_sort_name varchar(55), 
		plan_display_name varchar(100), 
		step_dt datetime, 
		step_type_desc varchar(30), 
		step_desc varchar(30),
		notes varchar(max), 
		step_due_dt datetime, 
		step_completed_on_dt datetime, 
		[priority] int, 
		worker_customer_no int, 
		worker_display_name varchar(160), 
		worker_sort_name varchar(55),
		old_value_desc varchar(30),
		new_value_desc varchar(30),
		step_type int
		)

INSERT INTO #StepData
	SELECT s.step_no, 
			p.plan_no, 
			p.customer_no, 
			cdn.display_name_short, 
			cdn.sort_name, 
			pdn.display_name, 
			s.step_dt, 
			st.description, 
			s.description, 
			s.notes, 
			s.due_dt, 
			s.completed_on_dt, 
			s.priority, 
			wdn.customer_no, 
			wdn.display_name_short, 
			wdn.sort_name,
			old_value_desc = COALESCE(psold.description, cold.description),
			new_value_desc = COALESCE(psnew.description, cnew.description),
			s.step_type
	FROM [dbo].T_STEP s
		JOIN T_PLAN p ON p.plan_no = s.plan_no
		JOIN dbo.TR_STEP_TYPE st ON st.id = s.step_type
		JOIN [dbo].FT_CONSTITUENT_DISPLAY_NAME() cdn ON p.customer_no = cdn.customer_no
		LEFT OUTER JOIN [dbo].FT_CONSTITUENT_DISPLAY_NAME() wdn ON s.worker_customer_no = wdn.customer_no
		JOIN [dbo].FT_PLAN_DISPLAY_NAME() pdn ON p.plan_no = pdn.plan_no
		LEFT OUTER JOIN [dbo].TR_PLAN_STATUS psold ON s.old_value = psold.id AND s.step_type = -1
		LEFT OUTER JOIN [dbo].TR_PLAN_STATUS psnew ON s.new_value = psnew.id AND s.step_type = -1
		LEFT OUTER JOIN [dbo].T_CAMPAIGN cold ON s.old_value = cold.campaign_no AND s.step_type = -2
		LEFT OUTER JOIN [dbo].T_CAMPAIGN cnew ON s.new_value = cnew.campaign_no AND s.step_type = -2
	WHERE (DATEDIFF(d,s.step_dt, @step_date) = 0 OR s.completed_on_dt IS NULL)
		AND s.step_type IN (11,42) -- Gift Information, Contribution Process
	
	SELECT step_no, 
			plan_no, 
			customer_no, 
			constituent_display_name, 
			constituent_sort_name, 
			plan_display_name, 
			step_dt, 
			step_type_desc, 
			step_desc, 
			notes,
			step_due_dt, 
			step_completed_on_dt, 
			[priority], 
			worker_customer_no, 
			worker_display_name,
			worker_sort_name,
			old_value_desc,
			new_value_desc,
			step_type
	FROM  #StepData
	
RETURN

GO


