USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LRP_NIGHTLY_SUMM_OUT_AFPLEDGE_CFY]    Script Date: 4/6/2016 10:15:18 AM ******/
DROP PROCEDURE [dbo].[LRP_NIGHTLY_SUMM_OUT_AFPLEDGE_CFY]
GO

/****** Object:  StoredProcedure [dbo].[LRP_NIGHTLY_SUMM_OUT_AFPLEDGE_CFY]    Script Date: 4/6/2016 10:15:18 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[LRP_NIGHTLY_SUMM_OUT_AFPLEDGE_CFY]
	@section VARCHAR(300),
	@sortOrder INT,
	@runBy	VARCHAR(300),
	@fy INT,
	@fystart DATETIME,
	@fyend DATETIME
AS
-- =================================================================
-- H. Sheridan, 4/4/2016 - Outstanding AF Pledge Balance_Current FY
-- =================================================================

BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	--SET @section = 'Outstanding AF Pledge Balance_Current FY'
	--SET	@sortOrder = 4

	DECLARE @tblPassOne	TABLE	(
								customer_no	INT,
								value			MONEY
								)

	DECLARE @tblPassTwo TABLE	(
								customer_no		INT,
								value			MONEY
								)

	DECLARE @tblPassBoth TABLE	(
								customer_no		INT,
								value			MONEY
								)

	INSERT INTO @tblPassOne
        SELECT  c.customer_no,
                (c.cont_amt - c.recd_amt)
        FROM    T_CONTRIBUTION AS c
        INNER JOIN LTR_FUND_DETAIL AS f ON c.fund_no = f.fund_no
        WHERE   c.cont_type = 'P'
                AND c.custom_1 IN ('(none)', 'Matching Gift Credit')
                AND c.cont_amt > c.recd_amt
                AND CAST(c.cont_dt AS DATE) >= @fystart
                AND CAST(c.cont_dt AS DATE) <= @fyend
                AND f.designation = '2' 

	INSERT INTO @tblPassTwo
        SELECT  i.customer_no,
                (c.cont_amt - c.recd_amt)
        FROM    T_CONTRIBUTION AS c
        INNER JOIN T_CREDITEE AS t ON c.ref_no = t.ref_no
        INNER JOIN LTR_FUND_DETAIL AS f ON c.fund_no = f.fund_no
        INNER JOIN T_CUSTOMER AS i ON t.creditee_no = i.customer_no
        WHERE   c.cont_type = 'P'
                AND c.custom_1 IN ('Primary Soft Credit', 'Matching Gift Credit', 'Stewardship Soft Credit')
                AND c.cont_amt > c.recd_amt
                AND CAST(c.cont_dt AS DATE) >= @fystart
                AND CAST(c.cont_dt AS DATE) <= @fyend
                AND t.creditee_type IN ('12', '15', '5', '10', '16', '1', '14')
                AND f.designation = '2'

	INSERT INTO @tblPassBoth
		SELECT * FROM @tblPassOne

	INSERT INTO @tblPassBoth
		SELECT * FROM @tblPassTwo

	--SELECT * FROM @tblPassBoth

	INSERT INTO LT_NIGHTLY_SUMMARY
		SELECT  customer_no,
				@section,
				SUM(ISNULL(value, 0)),
				'',
				@sortOrder,
				GETDATE(),
				@runBy
		FROM	@tblPassBoth
		GROUP BY customer_no

END
GO


