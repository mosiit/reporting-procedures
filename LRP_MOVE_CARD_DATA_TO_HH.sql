USE [impresario];
GO

SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO

IF OBJECT_ID('LRP_MOVE_CARD_DATA_TO_HH') IS NOT NULL
BEGIN
    DROP PROCEDURE [dbo].[LRP_MOVE_CARD_DATA_TO_HH];
END;
GO

CREATE PROCEDURE [dbo].[LRP_MOVE_CARD_DATA_TO_HH] (@customer_no INT)
AS
/*
created 12/19/2017 BG(Tess)
Moves 
for use in a custom constituent report
should be used in conjunction with the Move Transactions to Houseshold 
if memberships were moved.

exec LRP_MOVE_CARD_DATA_TO_HH @customer_no = 2921407

*/

/*For Testing purposes:

select distinct  c.description mem_org, x.memb_level, x.expr_dt, a.customer_no original_customer_no, x.customer_no new_customer_no
from LT_MOS_MEMBER_CARDS a 
join TX_CUST_MEMBERSHIP x on a.cust_memb_no = x.cust_memb_no and x.customer_no = dbo.FS_GET_HOUSEHOLD(@customer_no)
join T_MEMB_ORG c on x.memb_org_no = c.memb_org_no 
where 1=1 
and a.customer_no = @customer_no 
union
select distinct  c.description memb_org, x.memb_level, x.expr_dt, a.customer_no original_customer_no, x.customer_no new_customer_no
from LT_MOS_ACK_MEMBERSHIPS_AS_PRODUCTS a 
join TX_CUST_MEMBERSHIP x on a.cust_memb_no = x.cust_memb_no and x.customer_no = dbo.FS_GET_HOUSEHOLD(@customer_no)
join T_MEMB_ORG c on x.memb_org_no = c.memb_org_no 
where 1=1 
and a.customer_no = @customer_no 
return 
*/
DECLARE @results TABLE
(
    cust_memb_no INT,
    original_customer_no INT,
    new_customer_no INT
);

UPDATE a
SET a.customer_no = x.customer_no
OUTPUT deleted.cust_memb_no,
    deleted.customer_no,
    inserted.customer_no
INTO @results
FROM LT_MOS_MEMBER_CARDS a
JOIN TX_CUST_MEMBERSHIP x
    ON a.cust_memb_no = x.cust_memb_no
       AND x.customer_no = dbo.FS_GET_HOUSEHOLD(@customer_no)
WHERE 1 = 1
      AND a.customer_no = @customer_no;


UPDATE a
SET a.customer_no = x.customer_no
OUTPUT deleted.cust_memb_no,
    deleted.customer_no,
    inserted.customer_no
INTO @results
FROM LT_MOS_ACK_MEMBERSHIPS_AS_PRODUCTS a
JOIN TX_CUST_MEMBERSHIP x
    ON a.cust_memb_no = x.cust_memb_no
       AND x.customer_no = dbo.FS_GET_HOUSEHOLD(@customer_no)
WHERE 1 = 1
      AND a.customer_no = @customer_no;


SELECT DISTINCT
    c.description mem_org,
    b.memb_level,
    b.expr_dt,
    a.original_customer_no,
    a.new_customer_no
FROM @results a
JOIN TX_CUST_MEMBERSHIP b
    ON a.cust_memb_no = b.cust_memb_no
JOIN T_MEMB_ORG c
    ON b.memb_org_no = c.memb_org_no;

RETURN;


GO

GRANT EXEC ON LRP_MOVE_CARD_DATA_TO_HH TO ImpUsers;
GO

-- DSJ ( 4/11/18) This probably does not need to be run again. Commenting it out so it doesn't accidentily get run again.
-- 
--DECLARE @next_id INT;
--SELECT @next_id = MAX(id) + 1
--FROM T_CONSTITUENT_REPORT;
--INSERT INTO T_CONSTITUENT_REPORT
--(
--    id,
--    description,
--    d_object,
--    security_equal,
--    display_in_browser
--)
--SELECT @next_id,
--    'Move Mbr Card Data to HH',
--    'http://superman/ReportServer?/Tessitura/CustomReports/ConstituentCustomScreens/MOS_MoveCardDataToHH&customer_no=<<key>>&UserId=<<user>>&UserGroup=<<ug>>',
--    'PG_BIO',
--    'Y';
