USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_SALES_PERCENTAGES]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LRP_SALES_PERCENTAGES]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LRP_SALES_PERCENTAGES]
        @performance_start_dt DATETIME = NULL,
        @performance_end_dt DATETIME = NULL,
        @order_start_dt DATETIME = NULL,
        @order_end_dt DATETIME = NULL,
        @title_str VARCHAR(4000) = NULL,
        @production_str VARCHAR(4000) = NULL,
        @mode_of_sale_str varchar(4000) = NULL
AS BEGIN

    /*  Procedure Variables  */

        DECLARE @perf_start CHAR(10), @perf_end CHAR(10)
        DECLARE @rpt_message VARCHAR(100) = ''
        

    /*  Title, Production, and Mode of Sale ID Tables  */

        DECLARE @title_table TABLE ([title_no] INT, [title_no_str] VARCHAR(25))

        DECLARE @production_table TABLE ([production_no] INT, [production_no_str] VARCHAR(25))

        DECLARE @mode_of_sale_table TABLE ([mode_of_sale_no] INT, [mode_of_sale_no_str] VARCHAR(25))


    /*  For some reason I don't quite understand, joining to the TR_SALES_CHANNEL table increased the run time by over 400% 
        despite the fact that there are only 50ish records in the table.  When I cache the table into a variable first, 
        it runs fine and since I am doing that for Sales Channel, I decided to do it for mode of sale as well  */

        DECLARE @sales_channel_cache TABLE ([id] INT, [description] VARCHAR(30))
        INSERT INTO @sales_channel_cache SELECT [id], [description] FROM [dbo].[TR_SALES_CHANNEL] (NOLOCK)

        DECLARE @mode_of_sale_cache TABLE ([id] INT, [description] VARCHAR(30))
        INSERT INTO @mode_of_sale_cache SELECT [id], [description] FROM [dbo].[TR_MOS] (NOLOCK)
        

    /*  Create tables for the statistics, one to keep the list of orders to work with and the other to keep the stats from the order detail
        NOTE: Tried to do this with a table variable.  Changing it to a temp table with indexes shortened the run time from 51 seconds to 4.  */

        IF OBJECT_ID('tempdb..#trx_stat_orders') IS NOT NULL DROP TABLE [#trx_stat_Orders]

        CREATE TABLE [#trx_stat_orders] ([order_no] INT, [order_create_dt] DATETIME, [order_mode_of_sale] INT, [order_sales_channel] INT, [order_customer_no] INT, 
                                         [order_customer_name] VARCHAR(150), [order_customer_type] CHAR(1), [order_customer_ind] VARCHAR(25))

        CREATE CLUSTERED INDEX [ix_trx_stat_orders_order_no] ON [#trx_stat_orders] ([order_no] ASC) ON [PRIMARY]

        CREATE NONCLUSTERED INDEX [ix_trx_stat_orders_order_create_dt] ON [#trx_stat_orders] ([order_create_dt] ASC) ON [PRIMARY]

        CREATE NONCLUSTERED INDEX [ix_trx_stat_orders_customer_no] ON [#trx_stat_orders] ([order_customer_no] ASC) ON [PRIMARY]

        CREATE NONCLUSTERED INDEX [ix_trx_stat_orders_order_customer_name] ON [#trx_stat_orders] ([order_customer_name] ASC) ON [PRIMARY]

        CREATE NONCLUSTERED INDEX [ix_trx_stat_orders_order_customer_type] ON [#trx_stat_orders] ([order_customer_type] ASC) ON [PRIMARY]

        CREATE NONCLUSTERED INDEX [ix_trx_stat_orders_order_mode_of_sale] ON [#trx_stat_orders] ([order_mode_of_sale] ASC) ON [PRIMARY]

        CREATE NONCLUSTERED INDEX [ix_trx_stat_orders_order_sales_channel] ON [#trx_stat_orders] ([order_sales_channel] ASC) ON [PRIMARY]


        IF OBJECT_ID('tempdb..#trx_stat_table') IS NOT NULL DROP TABLE [#trx_stat_table]

        CREATE TABLE [#trx_stat_table]  ([order_no] INT, [order_create_dt] DATETIME, [order_mode_of_sale] INT, [mode_of_sale_name] VARCHAR(30), [order_sales_channel] INT, [sales_channel_name] VARCHAR(30),
                                         [order_customer_no] INT, [order_customer_name] VARCHAR(150), [order_customer_type] CHAR(1), [order_customer_ind] VARCHAR(25), [title_no] INT, [title_name] VARCHAR(30), 
                                         [production_no] INT, [production_name] VARCHAR(30), [counter] INT, [due_amount] MONEY, [paid_amount] MONEY, [total_paid] MONEY, [rpt_message] VARCHAR(100))

        CREATE CLUSTERED INDEX [ix_trx_stat_table_title_no] ON [#trx_stat_table] ([title_no] ASC) ON [PRIMARY]

        CREATE NONCLUSTERED INDEX [ix_trx_stat_table_production_no] ON [#trx_stat_table] ([production_no] ASC) ON [PRIMARY]

        CREATE NONCLUSTERED INDEX [ix_trx_stat_table_order_mode_of_sale] ON [#trx_stat_table] ([order_mode_of_sale] ASC) ON [PRIMARY]

        CREATE NONCLUSTERED INDEX [ix_trx_stat_table_order_sales_channel] ON [#trx_stat_table] ([order_sales_channel] ASC) ON [PRIMARY]


    /*  Check Parameters - If nulls passed to date parameters, run for yesterday.  */

        SELECT @performance_start_dt = IsNull(@performance_start_dt, dateadd(day, -1, getdate())),
               @performance_end_dt = IsNull(@performance_end_dt, dateadd(day, -1, getdate()))

        SELECT @title_str = ISNULL(@title_str,'')

        SELECT @production_str = ISNULL(@production_str,'')

        SELECT @mode_of_sale_str = ISNULL(@mode_of_sale_str,'')

        
    /*  Make sure the date parameters include the full dates, starting at 00:00 and ending at 23:59  */

        SELECT @performance_start_dt = convert(char(10),@performance_start_dt,111) + ' 00:00:00',
               @performance_end_dt = convert(char(10), @performance_end_dt,111) + ' 23:59:59'

        SELECT @perf_start = CONVERT(CHAR(10),@performance_start_dt,111),
               @perf_end = CONVERT(CHAR(10),@performance_end_dt,111)


    /*  Parse out table, production, and mode of sale id numbers from the lists of values passed to the report  */

        IF @title_str <> '' BEGIN
            INSERT INTO @title_table SELECT 0, LTRIM(RTRIM(Item)) FROM [dbo].[LFT_SPLIT_DILEMETED_STRING] (@title_str,',')
            DELETE FROM @title_table WHERE ISNUMERIC([title_no_str]) = 0
            UPDATE @title_table SET [title_no] = CAST([title_no_str] AS INT)
        END

        IF @production_str <> '' BEGIN
            INSERT INTO @production_table SELECT 0, LTRIM(RTRIM(Item)) FROM [dbo].[LFT_SPLIT_DILEMETED_STRING] (@production_str,',')
            DELETE FROM @production_table WHERE ISNUMERIC([production_no_str]) = 0
            UPDATE @production_table SET [production_no] = CAST([production_no_str] AS INT)
        END

        IF @mode_of_sale_str <> '' BEGIN
            INSERT INTO @mode_of_sale_table SELECT 0, LTRIM(RTRIM(Item)) FROM [dbo].[LFT_SPLIT_DILEMETED_STRING] (@mode_of_sale_str,',')
            DELETE FROM @mode_of_sale_table WHERE ISNUMERIC([mode_of_sale_no_str]) = 0
            UPDATE @mode_of_sale_table SET [mode_of_sale_no] = CAST([mode_of_sale_no_str] AS INT)
        END


    /*  Get a distinct list of all orders for the title list passed to the report  */

        INSERT INTO [#trx_stat_orders]
        SELECT DISTINCT det.[order_no], ord.[create_dt], ord.[MOS], ord.[channel], ord.[customer_no], 
                        LTRIM(ISNULL(cus.[fname],'') + ' ' + CASE WHEN ISNULL(cus.[mname],'') = '' THEN '' ELSE cus.[mname] + ' ' END + ISNULL(cus.[lname],'')),
                        CASE WHEN ord.[customer_no] = 0 THEN 'A' ELSE 'K' END,
                        CASE WHEN ord.[customer_no] = 0 THEN 'Annonymous' ELSE 'Known' END
        FROM [dbo].[LV_ORDER_DETAIL] AS det (NOLOCK)
             INNER JOIN [dbo].[T_ORDER] AS ord (NOLOCK) ON ord.[order_no] = det.[order_no]
             LEFT OUTER JOIN [dbo].[T_CUSTOMER] AS cus (NOLOCK) ON cus.[customer_no] = det.[customer_no]
        WHERE performance_date BETWEEN @perf_start AND @perf_end AND [title_no] IN (SELECT [title_no] FROM @title_table)


    /*  If no orders found, jump to the end  */

        IF NOT EXISTS (SELECT * FROM [#trx_stat_orders]) GOTO FINISHED

    
    /*  If Order Dates are passed - Remove everything outside the date range  */

        IF @order_start_dt IS NOT NULL AND @order_end_dt IS NOT NULL
            DELETE FROM [#trx_stat_orders] WHERE CONVERT(DATE,[order_create_dt]) < @order_start_dt OR CONVERT(DATE,[order_create_dt]) > @order_end_dt 


    /*  Delete the modes of sale that were not passed to the report in the modes of sale list  */

        IF EXISTS (SELECT * FROM @mode_of_sale_table)
            DELETE FROM [#trx_stat_orders] WHERE [order_mode_of_sale] NOT IN (SELECT [mode_of_sale_no] FROM @mode_of_sale_table)

    /*  If no orders remain, jump to the end  */

        IF NOT EXISTS (SELECT * FROM [#trx_stat_orders]) GOTO FINISHED


    /*  If the customer had an active membership at the time of the order, it is counted as a member order  */

        UPDATE #trx_stat_orders SET [order_customer_type] = 'M', [order_customer_ind] = 'Member'
        WHERE [order_customer_no] IN 
                (SELECT [order_customer_no]
                        FROM #trx_stat_orders AS trx
                        INNER JOIN [dbo].[TX_CUST_MEMBERSHIP] AS mem (NOLOCK) ON mem.[customer_no] = trx.[order_customer_no] AND trx.[order_create_dt] BETWEEN mem.[init_dt] AND mem.[expr_dt])


    /*  If it is a known order attributed to the Guest User (web sales) or Kiosk Customer (kiosk sales) then make it annonymous  */
        
        UPDATE #trx_stat_orders SET [order_customer_type] = 'A', [order_customer_ind] = 'Annonymous'
        WHERE order_customer_name IN ('guest user','Kiosk Customer')


    /*  Any remaining customer who is a known customer is nonmember  */

        UPDATE #trx_stat_orders SET [order_customer_type] = 'N', [order_customer_ind] = 'NonMember'
        WHERE [order_customer_type] = 'K'


    /*  Get order detail information using the list of orders in #trx_stat_orders  */

        INSERT INTO [#trx_stat_table]
        SELECT ord.[order_no], ord.[order_create_dt], ord.[order_mode_of_sale], mos.[description], ord.[order_sales_channel], cha.[description], ord.[order_customer_no], ord.[order_customer_name], 
               ord.[order_customer_type], ord.[order_customer_ind], det.[title_no], det.[title_name], det.[production_no], det.[production_name], 1, det.[due_amount], det.[paid_amount], det.[total_paid], ''
        FROM [#trx_stat_orders] AS ord
             INNER JOIN @mode_of_sale_cache AS mos ON mos.[id] = ord.[order_mode_of_sale]
             INNER JOIN @sales_channel_cache AS cha ON cha.[id] = ord.[order_sales_channel]
             INNER JOIN [dbo].[LV_ORDER_DETAIL] AS det (NOLOCK) ON det.[order_no] = ord.[order_no]


    /*  Delete the titles that were not passed to the report in the title list  */

        IF EXISTS (SELECT [title_no] FROM @title_table)
            DELETE FROM [#trx_stat_table] WHERE [title_no] NOT IN (SELECT [title_no] FROM @title_table) 


    /*  Delete the productions that were not passed to the report in the production list  */
      
        IF EXISTS (SELECT [production_no] FROM @production_table)
            DELETE FROM [#trx_stat_table] WHERE [production_no] NOT IN (SELECT [production_no] FROM @production_table)


    /*  Combine all the various kiosks sales channels into one kiosk channel to shorten the report.  */

        UPDATE #trx_stat_table SET [sales_channel_name] = 'Kiosk'
        WHERE [sales_channel_name] like '%Kiosk%'


    FINISHED:

        /*  If no records, add a single record with a "No Records Found" message  */

            IF NOT EXISTS (SELECT * FROM [#trx_stat_table]) BEGIN
                IF @rpt_message = '' SELECT @rpt_message = 'No records found for the criteria you entered'
                INSERT INTO #trx_stat_table VALUES  (0, NULL, 0, '', 0, '', 0, '', '', '', 0, '', 0, '', 1, 0.00, 0.00, 0.00, @rpt_message)
            END


        /*  Pull final data set to be passed back to the report  */

            SELECT [order_mode_of_sale], [mode_of_sale_name], [order_sales_channel], [sales_channel_name], 
                   [order_customer_type], [order_customer_ind], [title_no], [title_name], [production_no], [production_name], COUNT(DISTINCT [order_no]) AS 'order_count', 
                   SUM([counter]) AS 'visitor_count', SUM([due_amount]) AS 'due_amount', SUM([paid_amount]) AS 'paid_amount', SUM([total_paid]) AS 'total_paid', [rpt_message]
            FROM [#trx_stat_table]
            GROUP BY [order_mode_of_sale], [mode_of_sale_name], [order_sales_channel], [sales_channel_name],
                     [order_customer_type], [order_customer_ind], [title_no], [title_name], [production_no], [production_name], [rpt_message]

    DONE:

        /*  Cleanup  */

            IF OBJECT_ID('tempdb..#trx_stat_table') IS NOT NULL DROP TABLE [#trx_stat_table]

            IF OBJECT_ID('tempdb..#trx_stat_orders') IS NOT NULL DROP TABLE [#trx_stat_orders]

END
GO

GRANT EXECUTE ON [dbo].[LRP_SALES_PERCENTAGES] TO ImpUsers
GO

--EXECUTE [dbo].[LRP_SALES_PERCENTAGES] @performance_start_dt = '1-1-2017', @performance_end_dt = '1-31-2017', @order_start_dt = NULL, @order_end_dt = NULL, 
--            @title_str = '173, 161', @production_str = '1201,15268,18916,1203,1392,11943,1207,1208'
            --, @production_str = '1201,15268,18916,1203,1392,11943,1207,1208,18766,1210,15276,1211,1222,1223,1292,1224,1403,1397,11942',
            --@mode_of_sale_str = '3,7,10,11,12,15,17,23,28,29,30,35,36'

            --@title_str = NULL, @production_str = NULL, @mode_of_sale_str = null
