USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_SALES_METRICS_RAW_DATA_REPORT]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LRP_SALES_METRICS_RAW_DATA_REPORT]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LRP_SALES_METRICS_RAW_DATA_REPORT]
        @report_start_dt DATETIME = Null,
        @report_end_dt DATETIME = NULL,
        @report_user VARCHAR(10) = NULL
AS BEGIN

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    SET ANSI_WARNINGS OFF
    SET NOCOUNT ON

    /* Check Parameters  */
            
        SELECT @report_start_dt = ISNULL(@report_start_dt,DATEADD(DAY,-1,GETDATE())),
               @report_end_dt = ISNULL(@report_end_dt,DATEADD(DAY,-1,GETDATE()))

        SELECT @report_start_dt = CONVERT(DATE,@report_start_dt)
        SELECT @report_end_dt = CONVERT(DATE,@report_end_dt)

        SELECT @report_user = ISNULL(@report_user,'')
        
    /*  Select Raw Data  */

            SELECT [data_dt], 
                   [history_dt], 
                   [history_date], 
                   [operator], 
                   [order_no], 
                   [mode_of_sale_no], 
                   [mode_of_sale], 
                   [is_excluded_mos_mem], 
                   [is_excluded_mos_upsell], 
                   [is_group_order], 
                   [create_loc], 
                   [order_access], 
                   [is_sale_to_mem], 
                   [is_temp_card_order],
                   [in_renewal_prd], 
                   [has_pending], 
                   [in_one_step], 
                   [is_mem_sale], 
                   [is_mem_upgrade], 
                   [mem_sale_level], 
                   [mem_is_renewal], 
                   [mem_is_one_step_recovery], 
                   [mem_add_onestep], 
                   [mem_one_step_count], 
                   [is_gift_mem_sale], 
                   [gift_mem_sale_level], 
                   [mem_sale_sli], 
                   [is_course_order], 
                   [customer_no], 
                   [mem_no], 
                   [mem_expire],
                   [prev_mem_no], 
                   [prev_mem_expire], 
                   [prev_mem_on_one_step], 
                   [mem_conv_eligible], 
                   [one_step_eligible], 
                   [one_step_eligible_basic], 
                   [one_step_eligible_premier], 
                   [upsell_eligible], 
                   [total_perfs], 
                   [up_sell_counter]
             FROM [dbo].[LT_HISTORY_SALES_METRICS_RAW] 
             WHERE history_dt BETWEEN @report_start_dt AND @report_end_dt
               AND (@report_user = '' OR [operator] = @report_user)

    DONE:

END
GO

GRANT EXECUTE ON [dbo].[LRP_SALES_METRICS_RAW_DATA_REPORT] TO ImpUsers
GO

--EXECUTE [dbo].[LRP_SALES_METRICS_RAW_DATA_REPORT]
--        @report_start_dt = '6-15-2018',
--        @report_end_dt = '6-15-2018',
--        @report_user = ''

