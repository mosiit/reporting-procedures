USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_OVERNIGHT_INSTRUCTORS]') AND type IN (N'P', N'PC'))
BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_OVERNIGHT_INSTRUCTORS] AS' 
    EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LRP_OVERNIGHT_INSTRUCTORS] TO impusers'
END
GO

/*      LRP_OVERNIGHT_SNACK_TIMES
   


*/
ALTER PROCEDURE [dbo].[LRP_OVERNIGHT_INSTRUCTORS]
        @report_dt DATETIME = NULL
AS BEGIN

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

    /*  Procedure Variables  */

    DECLARE @instructor_data TABLE ([row_num] INT, [is_max_row] CHAR(1), [booking_no] INT, [overnight_dt] DATETIME, [day_of_week] VARCHAR(50),  
                                    [assigned_resource] VARCHAR(50), [resource_notes] VARCHAR(100), [schedule_shift] CHAR(1), [schedule_shift_time] VARCHAR(10), 
                                    [schedule_shift_label] VARCHAR(30), [worker_customer_no] INT, schedule_instructor VARCHAR(75));

    DECLARE @instructor_schedule TABLE ([booking_no] INT, [overnight_dt] DATETIME, [assigned_resource] VARCHAR(50), [resource_notes] VARCHAR(50), [row_num] INT, 
                                        [is_max_row] CHAR(1), [early_shift] VARCHAR(75), [late_shift] VARCHAR(75));


    /*  Get raw data about the instructor schedule  */

        WITH [max_rows] (assigned_resource, max_row) AS
            (SELECT assigned_resource, MAX(row_num)
             FROM [dbo].[LV_OVERNIGHT_INSTRUCTOR_SCHEDULE] (NOLOCK)
             WHERE [overnight_dt] = @report_dt
             GROUP BY assigned_resource)
        INSERT INTO @instructor_data ([row_num], [is_max_row], [booking_no], [overnight_dt], [day_of_week], [assigned_resource], [resource_notes], 
                                      [schedule_shift], [schedule_shift_time], [schedule_shift_label], [worker_customer_no], [schedule_instructor])
        SELECT  s.[row_num],
                CASE WHEN ISNULL(r.max_row,0) > 0 THEN 'Y' ELSE 'N' END AS [is_max_row],
                s.[booking_no],
                s.[overnight_dt],
                s.[day_of_week],
                s.[assigned_resource],
                s.[resource_schedule],
                s.[schedule_shift],
                s.[schedule_shift_time],
                s.[schedule_shift_label],
                s.[worker_customer_no],
                s.[schedule_instructor]
        FROM [dbo].[LV_OVERNIGHT_INSTRUCTOR_SCHEDULE] AS s (NOLOCK)
             LEFT OUTER JOIN [max_rows] AS r ON r.[assigned_resource] = s.[assigned_resource] AND r.[max_row] = s.[Row_num]
        WHERE s.[overnight_dt] = @report_dt
        ORDER BY s.assigned_resource, s.Row_num
        OPTION (RECOMPILE);

    /*  Pivot Data  */

        INSERT INTO @instructor_schedule ([booking_no], [overnight_dt], [assigned_resource], [resource_notes], [row_num], [is_max_row], [early_shift], [late_shift])
        SELECT [booking_no], [overnight_dt], [assigned_resource], [resource_notes], [row_num], [is_max_row], ISNULL([Early],''), ISNULL([Late],'')
        FROM (SELECT [booking_no], [overnight_dt], [assigned_resource], [resource_notes], [row_num], [is_max_row], [schedule_instructor], [schedule_shift_time] FROM @instructor_data) d
              PIVOT (min(schedule_instructor)
                         FOR schedule_shift_time IN ([Early], [Late])
              ) piv;

        
    FINISHED:


        SELECT [booking_no], 
               [overnight_dt], 
               [assigned_resource],
               [resource_notes], 
               [row_num],
               [is_max_row], 
               [early_shift],
               [late_shift]
        FROM @instructor_schedule

END
GO


--EXECUTE [dbo].[LRP_OVERNIGHT_INSTRUCTORS] '10-18-2018'