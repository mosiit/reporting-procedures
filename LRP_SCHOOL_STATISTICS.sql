USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_SCHOOL_STATISTICS]') AND type in (N'P', N'PC')) BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_SCHOOL_STATISTICS] AS' 
END
GO

ALTER PROCEDURE [dbo].[LRP_SCHOOL_STATISTICS]
        @report_start_dt DATETIME = NULL,
        @report_end_dt DATETIME = NULL,
        @report_start_dt_2 DATETIME = NULL,
        @report_end_dt_2 DATETIME = NULL,
        @include_no_shows CHAR(1) = 'N'
AS BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

        /*  Procedure Variables  */

        DECLARE @rpt_message VARCHAR(100) = ''
        DECLARE @date_range VARCHAR(20) = 'First Date Range'

        DECLARE @go_live_date CHAR(10) = '2016/05/18'
        DECLARE @special_exhibit_name VARCHAR(50) = ''

        DECLARE @rpt_start_date CHAR(10) = '', @rpt_end_date CHAR(10) = '',
                @his_start_date CHAR(10)= '', @his_end_date CHAR(10) = '',
                @arc_start_date CHAR(10)= '', @arc_end_date CHAR(10) = '',
                @fut_start_date CHAR(10)= '', @fut_end_date CHAR(10) = ''

        DECLARE @school_orders_history TABLE ([order_no] VARCHAR(50))

        DECLARE @school_orders_archive TABLE ([order_no] VARCHAR(50))

        DECLARE @valid_orders TABLE ([order_no] VARCHAR(50))

        DECLARE @payment_table TABLE ([date_range] VARCHAR(50), [order_no] VARCHAR(50), [primary_order_record] CHAR(1), [trx_seq_no] INT, [trx_no] INT, [payment_no] INT,
                                      [payment_dt] DATETIME, [payment_type] VARCHAR(50), [payment_method] VARCHAR(100), [payment_amount] DECIMAL(18,2), [payment_check_no] INT, 
                                      [payment_notes] VARCHAR(1024), [visit_count] INT, [report_message] VARCHAR(100))

    /*  Temporary Table  */

        IF OBJECT_ID('tempdb..#attendance_cache') IS NOT NULL DROP TABLE [#attendance_cache] 
        
        CREATE TABLE [#attendance_cache] ([attendance_type] VARCHAR(50), [order_no] VARCHAR(50), [performance_type] VARCHAR(30), [title_name] VARCHAR(30), [production_name] VARCHAR(50), 
                                          [production_name_short] VARCHAR(50), [production_name_long] VARCHAR(150), [comp_code_name] VARCHAR(30), [order_payment_status] VARCHAR(30), 
                                          [sale_total] INT, [scan_admission_total] INT, [due_amt] DECIMAL(18,2), [paid_amt] decimal(18,2), [customer_no] INT, [customer_city] VARCHAR(50), 
                                          [customer_state] VARCHAR(50), [customer_state_full] VARCHAR(100), [customer_zip_code] VARCHAR(50), [school_type] VARCHAR(50))

        IF OBJECT_ID('tempdb..#school_stats') IS NOT NULL DROP TABLE [#school_stats] 

        CREATE TABLE [#school_stats] ([data_pass] CHAR(1), [date_range] VARCHAR(20), [order_no] VARCHAR(50), [exhibit_halls] INT, [exhibit_halls_paid] DECIMAL (18,2), [special_exhibit] INT,
                                      [special_exhibit_paid] DECIMAL(18,2), [omni_theater] INT, [omni_theater_paid] DECIMAL (18,2), [planetarium] INT, [planetarium_paid] DECIMAL (18,2), 
                                      [4d_theater] INT, [4d_theater_paid] DECIMAL (18,2), [butterfly] INT, [butterfly_paid] DECIMAL (18,2), [other] INT, [other_paid] DECIMAL (18,2), 
                                      [biggest_venue] VARCHAR(50), [visit_count] INT, [total_venues] INT, [total_paid] DECIMAL (18,2), [combo_no] INT, [combo_name] VARCHAR(100), [customer_no] INT, 
                                      [customer_city] VARCHAR(50), [customer_state] VARCHAR(50), [customer_state_full] VARCHAR(100), [customer_zip_code] VARCHAR(50), [customer_district] VARCHAR(100),
                                      [customer_school_type] VARCHAR(50), [sort_field_combo] VARCHAR(100), [sort_field_location] VARCHAR(100), [rpt_message] VARCHAR(100))

    /*  If two date ranges are entered, they are processed one at a time.
        After the first range is processed, the code is sent back to this point to start the second date range  */

        
    /*  Check Parameters  */

        --IF NULLS PASSED TO PRIMARY DATE RANGE, INSERT ONE BLANK RECORD AND JUMP TO THE END
        IF @report_start_dt IS NULL AND @report_end_dt IS NULL BEGIN
            INSERT INTO [#school_stats] ([data_pass], [date_range], [order_no], [exhibit_halls], [exhibit_halls_paid], [special_exhibit],
                                         [special_exhibit_paid], [omni_theater], [omni_theater_paid], [planetarium], [planetarium_paid], 
                                         [4d_theater], [4d_theater_paid], [butterfly], [butterfly_paid], [other], [other_paid], 
                                         [biggest_venue], [visit_count], [total_venues], [total_paid], [combo_no], [combo_name], [customer_no], 
                                         [customer_city], [customer_state], [customer_state_full], [customer_zip_code], [customer_district],
                                         [customer_school_type], [sort_field_combo], [sort_field_location], [rpt_message])
            VALUES ('A', 'First Date Range', 0, 0, 0.0, 0, 0.0, 0, 0.0, 0, 0.0, 0, 0.0, 0, 0.0, 0, 0.0, '', 0, 0, 0.0, 1, '', 0, '', '', '', '', '', '', '', '', '')
            GOTO FINISHED
        END
        
        IF @report_end_dt < @report_start_dt BEGIN
            SELECT @rpt_message = 'Invalid dates - End Date Before Start date.'
            GOTO FINISHED
        END

        IF @report_start_dt_2 IS NOT NULL AND @report_end_dt_2 IS NOT NULL and @report_end_dt_2 < @report_start_dt_2 BEGIN
            SELECT @rpt_message = 'Invalid dates - End Date Before Start date.'
            GOTO FINISHED
        END

    BEGIN_PROCESS:  

    /*  Get the yyyy/mm/dd version of the start and end date*/
        
        SELECT @rpt_start_date = CONVERT(CHAR(10),@report_start_dt,111),
               @rpt_end_date = CONVERT(CHAR(10),@report_end_dt,111)

    /*  If both start and end date are *BEFORE* Tessitura Go Live, only select out of the archve table, nothing from history
        If both start and end date are on or *AFTER* Tessitura Go Live, only select from history, nothing from archive
        If start date is *BEFORE* and end date is on or *AFTER* Tessitura Go Live, select from archive for dates before and from history for dates on or after  */

        IF @rpt_start_date < @go_live_date AND @rpt_end_date < @go_live_date
            SELECT @his_start_date = '',
                   @his_end_date = '',
                   @fut_start_date = '',
                   @fut_end_date = '',
                   @arc_start_date = CONVERT(CHAR(10), @report_start_dt,111),
                   @arc_end_date = CONVERT(CHAR(10),@report_end_dt,111)
        ELSE IF @rpt_start_date > @go_live_date AND @rpt_end_date > @go_live_date
            SELECT @his_start_date = CONVERT(CHAR(10), @report_start_dt,111),
                   @his_end_date = CONVERT(CHAR(10),@report_end_dt,111),
                   @arc_start_date = '',
                   @arc_end_date = ''
        ELSE
           SELECT @his_start_date = @go_live_date,
                  @his_end_date = CONVERT(CHAR(10), @report_end_dt,111),
                  @arc_start_date = CONVERT(CHAR(10), @report_start_dt,111),
                  @arc_end_date = CONVERT(CHAR(10), DATEADD(DAY,-1,CONVERT(DATETIME, @go_live_date)),111)

    /*  Added 3/14/2018 - Determine dates if being run for the future (data has to be pulled from live transactions rather than history  */

        IF @his_start_date <> '' and @his_end_date <> '' AND @his_start_date >= CONVERT(CHAR(10),GETDATE(),111) and @his_end_date >= CONVERT(CHAR(10),GETDATE(),111)
            SELECT @fut_start_date = @his_start_date,
                   @fut_end_date = @his_end_date,
                   @his_start_date = '',
                   @his_end_date = ''
        ELSE IF @his_end_date <> '' and @his_end_date >= CONVERT(CHAR(10),GETDATE(),111)
            SELECT @fut_start_date = CONVERT(CHAR(10),GETDATE(),111),
                   @fut_end_date = @rpt_end_date,
                   @his_end_date = CONVERT(CHAR(10),DATEADD(DAY,-1,GETDATE()),111)
        ELSE
            SELECT @fut_start_date = '',
                   @fut_end_date = ''

    /*  All school attendance information is cached in [#attendance_cache]  */
    
    /* Get attendance data for dates on or after go live from the history table  */

        IF ISDATE(@his_start_date) = 1 AND ISDATE(@his_end_date) = 1
            INSERT INTO [#attendance_cache] ([attendance_type],[order_no],[performance_type],[title_name],[production_name],[production_name_short],[production_name_long],
                                             [comp_code_name],[order_payment_status],[sale_total],[scan_admission_total],[due_amt],[paid_amt], [customer_no], [customer_city], 
                                             [customer_state], [customer_state_full], [customer_zip_code], [school_type])
            SELECT tik.[attendance_type]
                 , tik.[order_no]
                 , tik.[performance_type]
                 , tik.[title_name]
                 , tik.[production_name]
                 , tik.[production_name]
                 , tik.[production_name]
                 , tik.[comp_code_name]
                 , tik.[order_payment_status]
                 , tik.[sale_total]
                 , tik.[scan_admission_total]
                 , tik.[due_amt]
                 , tik.[paid_amt]
                 , ISNULL(ord.[customer_no],0)
                 , ISNULL(adr.[city],'')
                 , ISNULL(adr.[state],'')
                 , ''
                 , ISNULL(LEFT(adr.[postal_code],5),'')
                 , ISNULL(stp.[key_value],'Unknown')
            FROM [dbo].[LT_HISTORY_TICKET] (NOLOCK) AS tik
                 LEFT OUTER JOIN [dbo].[T_ORDER] AS ord (NOLOCK) ON ord.[order_no] = tik.[order_no]
                 LEFT OUTER JOIN [dbo].[T_ADDRESS] AS adr (NOLOCK) ON adr.[customer_no] = ord.[customer_no] AND adr.[primary_ind] = 'Y'
                 LEFT OUTER JOIN [dbo].[TX_CUST_KEYWORD] AS stp (NOLOCK) ON stp.customer_no = ord.[customer_no] AND stp.keyword_no = 520    --520 = P1 Public/Private
            WHERE tik.[order_no] IN (SELECT [order_no] FROM [dbo].[LT_HISTORY_TICKET] WHERE [performance_date] BETWEEN @his_start_date AND @his_end_date
                                                                                        AND ([performance_type] = 'School Only' OR [price_type_name] LIKE '%school%'))
              --AND tik.[performance_date] BETWEEN @his_start_date AND @his_end_date 
              AND tik.[attendance_type] = 'Gate A (Ticketed)'
              AND tik.[price_type_name] <> 'Internal Group'
            

    /*  Get attendance date for dates before go live  */

        IF ISDATE(@arc_start_date) = 1 AND ISDATE(@arc_end_date) = 1
            INSERT INTO [#attendance_cache] ([attendance_type],[order_no],[performance_type],[title_name],[production_name],[production_name_short],[production_name_long],
                                             [comp_code_name],[order_payment_status],[sale_total],[scan_admission_total],[due_amt],[paid_amt], [customer_no], [customer_city],
                                             [customer_state], [customer_state_full], [customer_zip_code], [school_type])
            SELECT tik.[attendance_type]
                 , tik.[order_no]
                 , tik.[performance_type]
                 , tik.[title_name]
                 , tik.[production_name]
                 , tik.[production_name]
                 , tik.[production_name]
                 , tik.[comp_code_name]
                 , tik.[order_payment_status]
                 , tik.[sale_total]
                 , tik.[scan_admission_total]
                 , tik.[due_amt]
                 , tik.[paid_amt]
                 , ISNULL(tik.[customer_no],0)
                 , ISNULL(cus.[city],'Unknown')
                 , ISNULL(cus.[state],'')
                 , ''
                 , ISNULL(LEFT(cus.[zip_code],5),'')
                 , ISNULL(stp.[key_value],'Unknown')
            FROM [dbo].[LT_HISTORY_TICKET_ARCHIVE] AS tik
                 LEFT OUTER JOIN [dbo].[LT_HISTORY_CUSTOMER_ARCHIVE] AS cus ON cus.[order_no] = tik.[order_no]
                 LEFT OUTER JOIN [dbo].[TX_CUST_KEYWORD] AS stp (NOLOCK) ON stp.customer_no = ISNULL(tik.[customer_no],0) AND stp.keyword_no = 520    --520 = P1 Public/Private
            WHERE tik.[order_no] IN (SELECT [order_no] FROM [dbo].[LT_HISTORY_TICKET_ARCHIVE] WHERE [performance_date] BETWEEN @arc_start_date AND @arc_end_date
                                                                                               AND ([performance_type] = 'School Only' OR [price_type_name] LIKE '%school%'))
              --AND tik.[performance_date] BETWEEN @arc_start_date AND @arc_end_date 
              AND tik.[attendance_type] = 'Gate A (Ticketed)'
              AND tik.[price_type_name] <> 'Internal Group'



        IF ISDATE(@fut_start_date) = 1 AND ISDATE(@fut_end_date) = 1
            INSERT INTO [#attendance_cache] ([attendance_type],[order_no],[performance_type],[title_name],[production_name],[production_name_short],[production_name_long],
                                             [comp_code_name],[order_payment_status],[sale_total],[scan_admission_total],[due_amt],[paid_amt], [customer_no], [customer_city],
                                             [customer_state], [customer_state_full], [customer_zip_code], [school_type])
            SELECT 'Gate A (Ticketed)'
                 , sli.[order_no]
                 , prf.[performance_type]
                 , prf.[title_name]
                 , prf.[production_name]
                 , prf.[production_name]
                 , prf.[production_name]
                 , cmp.[description]
                 , 'Future Unpaid'
                 , COUNT(DISTINCT sli_no)
                 , 0
                 , SUM(sli.[due_amt])
                 , 0.00
                 , ISNULL(ord.[customer_no],0)
                 , ISNULL(adr.[city],'')
                 , ISNULL(adr.[state],'')
                 , ''
                 , ISNULL(LEFT(adr.[postal_code],5),'')
                 , ISNULL(stp.[key_value],'Unknown')
           FROM [dbo].[T_SUB_LINEITEM] AS sli (NOLOCK)
                LEFT OUTER JOIN [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] AS prf (NOLOCK) ON prf.performance_no = sli.perf_no AND prf.performance_zone = sli.zone_no
                LEFT OUTER JOIN [dbo].[TR_COMP_CODE] AS cmp (NOLOCK) ON cmp.[id] = sli.[comp_code]
                LEFT OUTER JOIN [dbo].[T_ORDER] AS ord (NOLOCK) ON ord.[order_no] = sli.[order_no]
                LEFT OUTER JOIN [dbo].[T_ADDRESS] (NOLOCK) AS adr ON adr.[customer_no] = ord.[customer_no] AND adr.[primary_ind] = 'Y'
                LEFT OUTER JOIN [dbo].[TR_PERF_TYPE] AS pty (NOLOCK) ON pty.id = prf.performance_type
                LEFT OUTER JOIN [dbo].[TR_PRICE_TYPE] AS rty (NOLOCK) ON rty.id = sli.price_type
                LEFT OUTER JOIN [dbo].[TX_CUST_KEYWORD] AS stp (NOLOCK) ON stp.customer_no = ord.[customer_no] AND stp.keyword_no = 520    --520 = P1 Public/Private
            WHERE prf.[performance_date] BETWEEN @fut_start_date AND @fut_end_date
              AND sli.[sli_status] IN (2,3,12)
              AND (pty.[description] = 'School Only' OR rty.[description] LIKE '%School%')             
        GROUP BY sli.[order_no], prf.[performance_type], prf.[title_name], prf.[production_name], prf.[production_name], prf.[production_name], cmp.[description], 
                 ISNULL(ord.[customer_no],0), ISNULL(adr.[city],''), ISNULL(adr.[state],''), ISNULL(LEFT(adr.[postal_code],5),''), ISNULL(stp.[key_value],'Unknown')

        DELETE FROM [#attendance_cache]
        WHERE [title_name] NOT IN ('Exhibit Halls', 'Special Exhibitions', 'Mugar Omni Theater', 'Hayden Planetarium', '4-D Theater', 'Butterfly Garden')

    /*  Get the full state name from TR_STATE table (to be used when looking at State Breakdown  */           

        UPDATE [#attendance_cache]
        SET [customer_state_full] = (SELECT ISNULL(MAX([description]),'') 
                                     FROM [dbo].[TR_STATE] AS sta
                                     WHERE sta.[id] = [#attendance_cache].[customer_state] AND sta.[country] IN (1, 32))

        UPDATE [#attendance_cache] SET [customer_state_full] = [customer_state] WHERE [customer_state_full] = ''

        UPDATE [#attendance_cache] SET [customer_state_full] = 'Unknown' WHERE [customer_state_full] = ''

        UPDATE [#attendance_cache] SET [customer_city] = 'Unknown' WHERE [customer_city] = ''

        
    /*  Statistical information for the school transactions is brought in one venue at a time, passing zeros into the fields for all the other venues  */

        /*  Exhibit Halls  */
        
            INSERT INTO [#school_stats] ([data_pass], [date_range], [order_no], [exhibit_halls], [exhibit_halls_paid], [special_exhibit], [special_exhibit_paid], [omni_theater], 
                                         [omni_theater_paid], [planetarium], [planetarium_paid], [4d_theater], [4d_theater_paid], [butterfly], [butterfly_paid], [other], [other_paid], 
                                         [biggest_venue], [total_venues], [visit_count], [total_paid], [combo_no], [combo_name], [customer_no], [customer_city], [customer_state], 
                                         [customer_state_full], [customer_zip_code], [customer_school_type])
            SELECT 'A', @date_range, att.[order_no], SUM(att.[sale_total]), SUM(att.[paid_amt]), 0, 0.00, 0, 0.00, 0, 0.00, 0, 0.00, 0, 0.00, 0, 0.00, '', 0, 0, 0.00, 0, '', att.[customer_no], 
                   att.[customer_city], att.[customer_state], att.[customer_state_full], att.[customer_zip_code], att.[school_type]
            FROM [#attendance_cache] AS att (NOLOCK)
            WHERE att.[title_name] = 'Exhibit Halls'
            GROUP BY att.[order_no], att.[customer_no], att.[customer_city], att.[customer_state], att.[customer_state_full], att.[customer_zip_code], att.[school_type]
        
        /*  Special Exhibits  */
        
            INSERT INTO [#school_stats] ([data_pass], [date_range], [order_no], [exhibit_halls], [exhibit_halls_paid], [special_exhibit], [special_exhibit_paid], [omni_theater], 
                                         [omni_theater_paid], [planetarium], [planetarium_paid], [4d_theater], [4d_theater_paid], [butterfly], [butterfly_paid], [other], [other_paid], 
                                         [biggest_venue], [total_venues], [visit_count], [total_paid], [combo_no], [combo_name], [customer_no], [customer_city], [customer_state], 
                                         [customer_state_full], [customer_zip_code], [customer_school_type])
            SELECT 'A', @date_range, att.[order_no], 0, 0.00, SUM(att.[sale_total]), SUM(att.[paid_amt]), 0, 0.00, 0, 0.00, 0, 0.00, 0, 0.00, 0, 0.00, '', 0, 0, 0.00, 0, '', att.[customer_no], 
                   att.[customer_city], att.[customer_state], att.[customer_state_full], att.[customer_zip_code], att.[school_type]
            FROM [#attendance_cache] AS att
            WHERE att.[title_name] = 'Special Exhibitions'
            GROUP BY att.[order_no], att.[customer_no], att.[customer_city], att.[customer_state], att.[customer_state_full], att.[customer_zip_code], att.school_type

        /*  Mugar Omni Theater  */

            INSERT INTO [#school_stats] ([data_pass], [date_range], [order_no], [exhibit_halls], [exhibit_halls_paid], [special_exhibit], [special_exhibit_paid], [omni_theater], 
                                         [omni_theater_paid], [planetarium], [planetarium_paid], [4d_theater], [4d_theater_paid], [butterfly], [butterfly_paid], [other], [other_paid], 
                                         [biggest_venue], [total_venues], [visit_count], [total_paid], [combo_no], [combo_name], [customer_no], [customer_city], [customer_state], 
                                         [customer_state_full], [customer_zip_code], [customer_school_type])
            SELECT 'A', @date_range, att.[order_no], 0, 0.00, 0, 0.00, SUM(att.[sale_total]), SUM(att.[paid_amt]), 0, 0.00, 0, 0.00, 0, 0.00, 0, 0.00, '', 0, 0, 0.00, 0, '', att.[customer_no], 
                   att.[customer_city], att.[customer_state], att.[customer_state_full], att.[customer_zip_code], att.[school_type]
            FROM [#attendance_cache] AS att
            WHERE att.[title_name] = 'Mugar Omni Theater'
            GROUP BY att.[order_no], att.[customer_no], att.[customer_city], att.[customer_state], att.[customer_state_full], att.[customer_zip_code], att.[school_type]

        /* Hayden Planetarium  */        

            INSERT INTO [#school_stats] ([data_pass], [date_range], [order_no], [exhibit_halls], [exhibit_halls_paid], [special_exhibit], [special_exhibit_paid], [omni_theater], 
                                         [omni_theater_paid], [planetarium], [planetarium_paid], [4d_theater], [4d_theater_paid], [butterfly], [butterfly_paid], [other], [other_paid], 
                                         [biggest_venue], [total_venues], [visit_count], [total_paid], [combo_no], [combo_name], [customer_no], [customer_city], [customer_state], 
                                         [customer_state_full], [customer_zip_code], [customer_school_type])
            SELECT 'A', @date_range, att.[order_no], 0, 0.00, 0, 0.00, 0, 0.00, SUM(att.[sale_total]), SUM(att.[paid_amt]), 0, 0.00, 0, 0.00, 0, 0.00, '', 0, 0, 0.00, 0, '', att.[customer_no], 
                   att.[customer_city], att.[customer_state], att.[customer_state_full], att.[customer_zip_code], att.[school_type]
            FROM [#attendance_cache] AS att
            WHERE att.[title_name] = 'Hayden Planetarium'
            GROUP BY att.[order_no], att.[customer_no], att.[customer_city], att.[customer_state], att.[customer_state_full], att.[customer_zip_code], att.[school_type]

        /*  4-D Theater*/

            INSERT INTO [#school_stats] ([data_pass], [date_range], [order_no], [exhibit_halls], [exhibit_halls_paid], [special_exhibit], [special_exhibit_paid], [omni_theater], 
                                         [omni_theater_paid], [planetarium], [planetarium_paid], [4d_theater], [4d_theater_paid], [butterfly], [butterfly_paid], [other], [other_paid], 
                                         [biggest_venue], [total_venues], [visit_count], [total_paid], [combo_no], [combo_name], [customer_no], [customer_city], [customer_state], 
                                         [customer_state_full], [customer_zip_code], [customer_school_type])
            SELECT 'A', @date_range, att.[order_no], 0, 0.00, 0, 0.00, 0, 0.00, 0, 0.00, SUM(att.[sale_total]), SUM(att.[paid_amt]), 0, 0.00, 0, 0.00, '', 0, 0, 0.00, 0, '', att.[customer_no], 
                   att.[customer_city], att.[customer_state], att.[customer_state_full], att.[customer_zip_code], att.[school_type]
            FROM [#attendance_cache] AS att
            WHERE att.[title_name] = '4-D Theater'
            GROUP BY att.[order_no], att.[customer_no], att.[customer_city], att.[customer_state], att.[customer_state_full], att.[customer_zip_code], att.[school_type]

        /*  Butterfly Garden  */

            INSERT INTO [#school_stats] ([data_pass], [date_range], [order_no], [exhibit_halls], [exhibit_halls_paid], [special_exhibit], [special_exhibit_paid], [omni_theater], 
                                             [omni_theater_paid], [planetarium], [planetarium_paid], [4d_theater], [4d_theater_paid], [butterfly], [butterfly_paid], [other], [other_paid], 
                                             [biggest_venue], [total_venues], [visit_count], [total_paid], [combo_no], [combo_name], [customer_no], [customer_city], [customer_state], 
                                             [customer_state_full], [customer_zip_code], [customer_school_type])
            SELECT 'A', @date_range, att.[order_no], 0, 0.00, 0, 0.00, 0, 0.00, 0, 0.00, 0, 0.00, SUM(att.[sale_total]), SUM(att.[paid_amt]), 0, 0.00, '', 0, 0, 0.00, 0, '', att.[customer_no], 
                   att.[customer_city], att.[customer_state], att.[customer_state_full], att.[customer_zip_code], att.[school_type]
            FROM [#attendance_cache] AS att
            WHERE att.[title_name] = 'Butterfly Garden'
            GROUP BY att.[order_no], att.[customer_no], att.[customer_city], att.[customer_state], att.[customer_state_full], att.[customer_zip_code], att.[school_type]

        /*  Other Gate Attendance  */
    
            INSERT INTO [#school_stats] ([data_pass], [date_range], [order_no], [exhibit_halls], [exhibit_halls_paid], [special_exhibit], [special_exhibit_paid], [omni_theater], 
                                         [omni_theater_paid], [planetarium], [planetarium_paid], [4d_theater], [4d_theater_paid], [butterfly], [butterfly_paid], [other], [other_paid], 
                                         [biggest_venue], [total_venues], [visit_count], [total_paid], [combo_no], [combo_name], [customer_no], [customer_city], [customer_state], 
                                         [customer_state_full], [customer_zip_code], [customer_school_type])
            SELECT 'A', @date_range, att.[order_no], 0, 0.00, 0, 0.00, 0, 0.00, 0, 0.00, 0, 0.00, 0, 0.00, SUM(att.[sale_total]), SUM(att.[paid_amt]), '', 0, 0, 0.00, 0, '', att.[customer_no], 
                   att.[customer_city], att.[customer_state], att.[customer_state_full], att.[customer_zip_code], att.school_type
            FROM [#attendance_cache] AS att
            WHERE att.[title_name] NOT IN ('4-D Theater', 'Butterfly Garden', 'Exhibit Halls', 'Hayden Planetarium', 'Mugar Omni Theater', 'Special Exhibitions')
            GROUP BY att.[order_no], att.[customer_no], att.[customer_city], att.[customer_state], att.[customer_state_full], att.[customer_zip_code], att.[school_type]


             --IF @include_no_shows <> 'Y'
             --   INSERT INTO @payment_table ([date_range], [order_no], [primary_order_record], [trx_seq_no], [trx_no], [payment_no], [payment_dt], [payment_type], 
             --                               [payment_method], [payment_amount], [payment_check_no], [payment_notes], [visit_count], [report_message])
             --   EXECUTE [dbo].[LRP_SCHOOL_STATISTICS_PAYMENTS] @report_start_dt = @his_start_date, @report_end_dt = @his_end_date, @report_start_dt_2 = Null, @report_end_dt_2 = Null


        /*  If a second date range was passed to the report, change the date information, truncate the table and send it back to the beginning to do it all again  */

            IF @date_range = 'First Date Range' and @report_start_dt_2 IS NOT NULL AND @report_end_dt_2 IS NOT NULL BEGIN

                SELECT @report_start_dt = @report_start_dt_2,
                       @report_end_dt = @report_end_dt_2,
                       @date_range = 'Second Date Range'

                TRUNCATE TABLE [#attendance_cache]

                GOTO BEGIN_PROCESS

            END

        /*  Aggregate everything down to one row per order - This is data pass B.  Once done, delete data pass A */
        
            INSERT INTO [#school_stats] ([data_pass], [date_range], [order_no], [exhibit_halls], [exhibit_halls_paid], [special_exhibit], [special_exhibit_paid], [omni_theater], [omni_theater_paid], 
                                         [planetarium], [planetarium_paid], [4d_theater], [4d_theater_paid], [butterfly], [butterfly_paid], [other], [other_paid], [biggest_venue], [total_venues], 
                                         [visit_count], [total_paid], [combo_no], [combo_name], [customer_no], [customer_city], [customer_state], [customer_state_full], [customer_zip_code], [customer_school_type])
            SELECT 'B', [date_range], [order_no], SUM([exhibit_halls]), SUM([exhibit_halls_paid]), SUM([special_exhibit]), SUM([special_exhibit_paid]), SUM([omni_theater]), SUM([omni_theater_paid]), 
                   SUM([planetarium]), SUM([planetarium_paid]), SUM([4d_theater]), SUM([4d_theater_paid]), SUM([butterfly]), SUM([butterfly_paid]), SUM([other]), SUM([other_paid]), '', 0, 0, 0.00, 0, '', 
                   [customer_no], [customer_city], [customer_state], [customer_state_full], [customer_zip_code], [customer_school_type]
            FROM [#school_stats]
            GROUP BY [date_range], [order_no], [customer_no], [customer_city], [customer_state], [customer_state_full], [customer_zip_code], [customer_school_type]

            DELETE FROM [#school_stats] WHERE [data_pass] = 'A'

        /*  Find the school district in the T_ASSOCIATION table.  If no district is found, the district is set to Unknown plus the city and state  */

            UPDATE [#school_stats]
            SET [customer_district] = (SELECT LTRIM(ISNULL(cust.[fname],'') + ' ' + CASE WHEN ISNULL(cust.[mname],'') = '' THEN '' ELSE cust.[mname] + ' ' END + ISNULL(cust.[lname],''))
                                       FROM [dbo].[T_CUSTOMER] AS cust (NOLOCK)
                                       WHERE cust.[customer_no] = (SELECT MAX(asoc.[associated_customer_no])
                                                                   FROM [dbo].[T_ASSOCIATION] AS asoc (NOLOCK) 
                                                                   WHERE asoc.[customer_no] = [#school_stats].[customer_no] AND asoc.[association_type_id] = 59))

            UPDATE [#school_stats]
            SET [customer_district] = 'Unknown (' + LTRIM(RTRIM([customer_city])) + ', ' + LTRIM(RTRIM([customer_state])) + ')'
            WHERE [customer_district] IS null

        /*  Update School Types  */

            UPDATE [#school_stats]
            SET [customer_school_type] = CASE WHEN [customer_school_type] = 'Private' THEN 'Private Schools'
                                              WHEN [customer_school_type] = 'Public' THEN 'Public Schools'
                                              WHEN [customer_school_type] = 'Unknown' THEN 'Unknown School Type'
                                              ELSE [Customer_school_type] END

        /*  If requested, exclude orders with no payments  */

            IF @include_no_shows <> 'Y' BEGIN

                INSERT INTO @valid_orders ([order_no])
                SELECT DISTINCT [order_no] FROM [#school_stats] WHERE dbo.FS_isReallyNumeric(order_no) = 0

                INSERT INTO @valid_orders ([order_no])
                SELECT DISTINCT [order_no] FROM [dbo].[T_SUB_LINEITEM] WHERE [order_no] IN (SELECT DISTINCT order_no FROM [#school_stats] WHERE dbo.FS_isReallyNumeric(order_no) = 1)   --Price Type: 8 = Teacher / 27 = Teacher Package
                                                                         AND [price_type] NOT IN (8, 27) AND [sli_status] IN (3,12)                                                     --SLI Status: 3 = seated, Paid / 12 = Ticketed, Paid (printed)

                DELETE FROM [#school_stats] WHERE [order_no] NOT IN (SELECT [order_no] FROM @valid_orders)
                
            END

          
        /*  Set the combo name for each order  
            --NOTE: Not everyone has to go to everything.  If anyone in the group goes to a venue, it is accounted for here  */

            UPDATE [#school_stats] SET [combo_name] = 'Exhibits\', [total_venues] = ([total_venues] + 1) WHERE [exhibit_halls] > 0
            UPDATE [#school_stats] SET [combo_name] = [combo_name] + 'Special\', [total_venues] = ([total_venues] + 1) WHERE [special_exhibit] > 0
            UPDATE [#school_stats] SET [combo_name] = [combo_name] + 'Omni\', [total_venues] = ([total_venues] + 1) WHERE [omni_theater] > 0
            UPDATE [#school_stats] SET [combo_name] = [combo_name] + 'Planetarium\', [total_venues] = ([total_venues] + 1) WHERE [planetarium] > 0
            UPDATE [#school_stats] SET [combo_name] = [combo_name] + '4-D\', [total_venues] = ([total_venues] + 1) WHERE [4d_theater] > 0
            UPDATE [#school_stats] SET [combo_name] = [combo_name] + 'Butterfly\', [total_venues] = ([total_venues] + 1) WHERE [butterfly] > 0
            UPDATE [#school_stats] SET [combo_name] = LEFT([combo_name], len([combo_name]) - 1) WHERE LEN([combo_name]) > 0     --Removes \ at end of name
            UPDATE [#school_stats] SET [combo_name] = [combo_name] + ' Only' WHERE [total_venues] = 1

        /*  Set the visit count  
            NOTE: Not everyone has to go to everything.  Visit count is the largest of all the venues (excluding the other category because that could include multiple venues)  */

            UPDATE [#school_stats] SET [biggest_venue] = 'Exhibit Halls', [visit_count] = [exhibit_halls]
            UPDATE [#school_stats] SET [biggest_venue] = 'Special Exhibits', [visit_count] = [special_exhibit] WHERE [special_exhibit] > [visit_count]
            UPDATE [#school_stats] SET [biggest_venue] = 'Omni Theater', [visit_count] = [omni_theater] WHERE [omni_theater] > [visit_count]
            UPDATE [#school_stats] SET [biggest_venue] = 'Planetarium', [visit_count] = [planetarium] WHERE [planetarium] > [visit_count]
            UPDATE [#school_stats] SET [biggest_venue] = '4-D Theater', [visit_count] = [4d_theater] WHERE [4d_theater] > [visit_count]
            UPDATE [#school_stats] SET [biggest_venue] = 'Butterfly', [visit_count] = [butterfly] WHERE [butterfly] > [visit_count]

        /*  Update the total paid field to be the sum of all the individual venue paid amounts  */

            UPDATE [#school_stats] 
            SET [total_paid] = ([exhibit_halls_paid] + [special_exhibit_paid] + [omni_theater_paid] + [planetarium_paid] + [4d_theater_paid] + [butterfly_paid])-- + [other_paid])

        /* For ease of sorting in the report.  Update the combo number
           If order has exhibit halls, combo_no = 1; if no exhbit halls but has special exhibit, combo_no = 2; if no exhibit halls or special but has omni, combo_no = 3, etc  */

            UPDATE [#school_stats] SET [combo_no] = 1 WHERE [combo_no] = 0 AND [combo_name] LIKE '%Exhibit%'
            UPDATE [#school_stats] SET [combo_no] = 2 WHERE [combo_no] = 0 AND [combo_name] LIKE '%Special%'
            UPDATE [#school_stats] SET [combo_no] = 3 WHERE [combo_no] = 0 AND [combo_name] LIKE '%Omni%'
            UPDATE [#school_stats] SET [combo_no] = 4 WHERE [combo_no] = 0 AND [combo_name] LIKE '%Planetarium%'
            UPDATE [#school_stats] SET [combo_no] = 5 WHERE [combo_no] = 0 AND [combo_name] LIKE '%4-D%'
            UPDATE [#school_stats] SET [combo_no] = 6 WHERE [combo_no] = 0 AND [combo_name] LIKE '%Butterfly%'
            UPDATE [#school_stats] SET [combo_no] = 7 WHERE [combo_no] = 0 
            UPDATE [#school_stats] SET [total_venues] = 9 WHERE combo_name = ''

        /*  Also for ease of sorting in the report.  Create various sort fields  */

            UPDATE [#school_stats] SET [sort_field_combo] = CONVERT(VARCHAR(5),[total_venues]) + '_' + CONVERT(VARCHAR(5),[combo_no]) + '_' + [combo_name]
            WHERE [combo_name] <> ''

            UPDATE [#school_stats] SET [sort_field_combo] = '' WHERE [sort_field_combo] IS NULL 

            UPDATE [#school_stats] SET [sort_field_location] = LEFT([customer_zip_code],5) + '_' + [customer_city] + '_' + [customer_state]
            WHERE [customer_city] <> '' OR [customer_state] <> '' OR [customer_zip_code] <> ''

            UPDATE [#school_stats] SET [sort_field_location] = '' WHERE [sort_field_location] IS NULL 

    FINISHED:

        /*  If no data found, add a single record with a message */

            IF NOT EXISTS (SELECT * FROM [#school_stats]) BEGIN
                IF ISNULL(@rpt_message,'') = '' SELECT @rpt_message = 'No Data Found For Criteria Entered'
                INSERT INTO [#school_stats] ([data_pass], [date_range], [order_no], [exhibit_halls], [exhibit_halls_paid], [special_exhibit], [special_exhibit_paid], [omni_theater], [omni_theater_paid], 
                                         [planetarium], [planetarium_paid], [4d_theater], [4d_theater_paid], [butterfly], [butterfly_paid], [other], [other_paid], [biggest_venue], [total_venues], 
                                         [visit_count], [total_paid], [combo_no], [combo_name], [customer_no], [customer_city], [customer_state], [customer_state_full], [customer_zip_code],
                                         [customer_district], [customer_school_type], [sort_field_combo], [sort_field_location], [rpt_message])
                VALUES ('A', 'First Date Range', 0, 0, 0.0, 0, 0.0, 0, 0.0, 0, 0.0, 0, 0.0, 0, 0.0, 0, 0.0, '', 0, 0, 0.0, 1, '', 0, '', '', '', '', '', '', '', '', @rpt_message)
            END ELSE BEGIN
                UPDATE [#school_stats] SET [rpt_message] = ''
            END

        /*  Return final data set for the report to display  */

            --SELECT date_range, COUNT(DISTINCT [order_no]), SUM([visit_count]), SUM([total_paid]) FROM [#school_stats] WHERE combo_name = 'Exhibit Halls Only' GROUP BY date_range
            --SELECT * FROM [#school_stats] WHERE [date_range] = 'Second Date Range' AND  combo_name = 'Exhibit Halls Only'
            --SELECT order_no FROM [#school_stats] GROUP BY order_no HAVING COUNT(*) > 1

            SELECT [date_range]
                 , [order_no]
                 , [exhibit_halls]
                 , [exhibit_halls_paid]
                 , [special_exhibit]
                 , [special_exhibit_paid]
                 , [omni_theater]
                 , [omni_theater_paid]
                 , [planetarium]
                 , [planetarium_paid]
                 , [4d_theater]
                 , [4d_theater_paid]
                 , [butterfly]
                 , [butterfly_paid]
                 , [other]
                 , [other_paid]
                 , [biggest_venue]
                 , [visit_count]
                 , [total_venues]
                 , [total_paid]
                 , [combo_no]
                 , [combo_name]
                 , [customer_no]
                 , [customer_city]
                 , [customer_state]
                 , [customer_state_full]
                 , [customer_zip_code]
                 , [customer_district]
                 , [customer_school_type]
                 , [sort_field_combo]
                 , [sort_field_location]
                 , [rpt_message]
            FROM [#school_stats]
                    
    DONE:

END
GO

GRANT EXECUTE ON [dbo].[LRP_SCHOOL_STATISTICS] TO [ImpUsers] AS [dbo]
GO


--EXECUTE [dbo].[LRP_SCHOOL_STATISTICS] @report_start_dt = NULL, @report_end_dt = NULL, @report_start_dt_2 = Null, @report_end_dt_2 = Null
EXECUTE [dbo].[LRP_SCHOOL_STATISTICS] @report_start_dt = '6-1-2021', @report_end_dt = '6-30-2021', @report_start_dt_2 = Null, @report_end_dt_2 = Null, @include_no_shows = 'Y'
--EXECUTE [dbo].[LRP_SCHOOL_STATISTICS] @report_start_dt = '5-1-2014', @report_end_dt = '5-31-2014', @report_start_dt_2 = '5-1-2016', @report_end_dt_2 = '5-31-2016'
--EXECUTE [dbo].[LRP_SCHOOL_STATISTICS] @report_start_dt = '5-1-2017', @report_end_dt = '5-31-2017', @report_start_dt_2 = '6-1-2017', @report_end_dt_2 = '6-30-2017', @include_no_shows = 'N'



--SELECT * FROM [dbo].[TR_GOOESOFT_DROPDOWN] WHERE code = 1025 ORDER BY id

--SELECT * FROM [dbo].[T_ORDER] WHERE [order_no] = 2416803
--SELECT * FROM [dbo].[LV_ORDER_DETAIL] WHERE [order_no] = 2416803