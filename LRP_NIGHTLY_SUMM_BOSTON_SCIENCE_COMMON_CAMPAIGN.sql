USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LRP_NIGHTLY_SUMM_BOSTON_SCIENCE_COMMON_CAMPAIGN]    Script Date: 1/19/2021 10:25:39 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



ALTER PROCEDURE [dbo].[LRP_NIGHTLY_SUMM_BOSTON_SCIENCE_COMMON_CAMPAIGN]
	@section VARCHAR(300),
	@sortOrder INT,
	@runBy	VARCHAR(300)
AS
-- ===============================================================
-- H. Sheridan, 1/19/2021 - Boston Science Common Campaign Total
-- ===============================================================

BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE @tblPassOne	TABLE	(
								customer_no	INT,
								value			MONEY
								)

	DECLARE @tblPassTwo TABLE	(
								customer_no		INT,
								value			MONEY
								)

	DECLARE @tblPassBoth TABLE	(
								customer_no		INT,
								value			MONEY
								)

	INSERT INTO @tblPassOne
        SELECT  c.customer_no,
                ISNULL(SUM(c.cont_amt), 0)
        FROM    T_CONTRIBUTION AS c
        INNER JOIN LTR_FUND_DETAIL AS f ON c.fund_no = f.fund_no
        WHERE   c.cont_type IN ('G', 'P')
                AND c.custom_1 IN ('(none)', 'Matching Gift Credit')
                AND f.overall_cm_no = '13'
        GROUP BY c.customer_no

	--SELECT 'debug 1', customer_no, value, count(*) FROM @tblPassOne group by customer_no, value order by 3 Desc

	INSERT INTO @tblPassTwo
        SELECT  i.customer_no,
                ISNULL(SUM(c.cont_amt), 0)
        FROM    T_CONTRIBUTION AS c
        INNER JOIN T_CREDITEE AS t ON c.ref_no = t.ref_no
        INNER JOIN LTR_FUND_DETAIL AS f ON c.fund_no = f.fund_no
        INNER JOIN T_CUSTOMER AS i ON t.creditee_no = i.customer_no
        WHERE   c.cont_type IN ('G', 'P')
                AND c.custom_1 IN ('Primary Soft Credit', 'Matching Gift Credit', 'Stewardship Soft Credit')
                AND t.creditee_type IN ('12', '15', '5', '10', '16', '1', '14')
               AND f.overall_cm_no = '13'
        GROUP BY i.customer_no

	--SELECT 'debug 2', customer_no, value, count(*) FROM @tblPassTwo group by customer_no, value order by 3 Desc

	INSERT INTO @tblPassBoth
		SELECT * FROM @tblPassOne

	INSERT INTO @tblPassBoth
		SELECT * FROM @tblPassTwo

	--SELECT * FROM @tblPassBoth

	INSERT INTO LT_NIGHTLY_SUMMARY
		SELECT  customer_no,
				@section,
				SUM(ISNULL(value, 0)),
				'',
				@sortOrder,
				GETDATE(),
				@runBy
		FROM	@tblPassBoth
		GROUP BY customer_no

END

GO


