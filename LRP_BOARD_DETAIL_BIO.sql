USE [impresario]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[LRP_BOARD_DETAIL_BIO]
(
	@customer_no INT
)    
AS

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

-- NOTE: [dbo].[LFS_BOARD_GetSpouse] will eventually go away and be replaced with LV_BOARD_STATUS_A2

WITH BoardStartDates
AS
(
	SELECT individual_customer_no, Trustee AS Trustee_StartDt, Overseer AS Overseer_StartDt
	FROM 
	(
		SELECT a.individual_customer_no, a.start_dt,
		CASE 
			WHEN at.description LIKE '%Trustee%' THEN 'Trustee'
			WHEN at.description LIKE '%Overseer%' THEN 'Overseer'
			ELSE 'Unknown'
		END brdType
		FROM T_AFFILIATION AS a
		 INNER JOIN TR_AFFILIATION_TYPE AS at
					ON a.affiliation_type_id = at.id
			WHERE   a.affiliation_type_id IN (10086, 10088, 10087, 10090, 10089, 10091, 10095, 10092, 10094)
			AND a.individual_customer_no = @customer_no
	) AS tbl2Pivot
	PIVOT (MIN(start_dt) FOR brdType IN (Trustee, Overseer) ) AS BrdHistory
)
SELECT brd.individual_customer_no,
	brd.description boardType, brdstdt.Trustee_StartDt, brdstdt.Overseer_StartDt, 
	cust.display_name, [dbo].[LFS_BOARD_GetSpouse](brd.individual_customer_no) AS spouse,
	[dbo].[LFS_BOARD_GetBoardEmail](brd.individual_customer_no, 0) AS email,
	(case when age.keyword_no =1 then DATEDIFF ( month , age.key_value , GETDATE() ) /12 end) as 'age',
	[dbo].[LFS_BOARD_GetProspectManager](brd.individual_customer_no) AS ProspectMgr,
	[dbo].[LFS_BOARD_GetNominatingSuggester](brd.individual_customer_no, brd.description) AS NominatingSuggester,
	persMemb.PersonalMembType, persMemb.PersonalMembExpirationDate,
	corpMemb.CorporateMembType, corpMemb.CorporateMembExpirationDate,
	indust.key_value AS Industry,
	CASE Colby.cnt
		WHEN 0 THEN 'No'
		WHEN 1 THEN 'Yes'
		ELSE ''
	END ColbySocityMember,
	ISNULL(home.Address1, '') AS hAddr1,
	ISNULL(home.Address2, '') AS hAddr2,
	ISNULL(home.Address3, '') AS hAddr3,
	ISNULL(home.City, '') AS hCity,
	ISNULL(home.StateCd, '') AS hState,
	ISNULL(home.ZipCode, '') AS hZipCode,
	ISNULL(home.Phone, '') AS hphone,
	ISNULL(home.Fax, '') AS hfax,
	ISNULL(home.Notes, '') AS HREST,
	employer.JobTitle,
	employer.Employer,
	ISNULL(employer.BAddress1, '') AS BAddress1,
	ISNULL(employer.BAddress2, '') AS BAddress2,
	ISNULL(employer.BAddress3, '') AS BAddress3,
	ISNULL(employer.BCity, '') AS BCity,
	ISNULL(employer.BStateCd, '') AS BStateCd,
	ISNULL(employer.BZipCode, '') AS BZipCode,
	ISNULL(employer.BPhone, '') AS BPhone,
	ISNULL(employer.BFax, '') AS BFax,
	ISNULL(employer.BNotes, '') AS BREST
FROM dbo.LV_BOARD_MEMBERS brd
	INNER JOIN BoardStartDates brdstdt
		ON brdstdt.individual_customer_no = brd.individual_customer_no
	INNER JOIN dbo.FT_CONSTITUENT_DISPLAY_NAME() AS cust 
		ON brd.individual_customer_no = cust.customer_no
	INNER JOIN 
	(
		SELECT @customer_no AS customer_no, COUNT(*) AS cnt
		FROM dbo.LT_ATTRIBUTE_DETAIL 
		WHERE descr1_no = 2 -- Active 
		AND type_no = 5 -- Colby Society 
		AND customer_no IN (SELECT expanded_customer_no FROM dbo.V_CUSTOMER_WITH_PRIMARY_GROUP WHERE customer_no = @customer_no)
	) Colby
	ON Colby.customer_no = brd.individual_customer_no
	LEFT JOIN vxs_cust_keyword age 
      ON brd.individual_customer_no = age.customer_no
	LEFT JOIN dbo.TX_CUST_KEYWORD indust
		ON indust.customer_no = brd.individual_customer_no
			AND indust.keyword_no = 421 -- Industry
	OUTER APPLY dbo.LFT_BOARD_GetAddress(brd.individual_customer_no, 'Home', 0) home
	OUTER APPLY dbo.LFT_BOARD_GetPersonalMembership(brd.individual_customer_no) persMemb
	OUTER APPLY dbo.LFT_BOARD_GetCorporateMembership(brd.individual_customer_no) corpMemb
	OUTER APPLY [dbo].[LFT_BOARD_GetEmployerInformation](brd.individual_customer_no, 0) employer
WHERE age.keyword_no = 1 AND brd.individual_customer_no = @customer_no
