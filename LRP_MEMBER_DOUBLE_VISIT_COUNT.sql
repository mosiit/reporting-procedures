USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_MEMBER_DOUBLE_VISIT_COUNT]') AND type in (N'P', N'PC')) BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_MEMBER_DOUBLE_VISIT_COUNT] AS' 
    EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LRP_MEMBER_DOUBLE_VISIT_COUNT] TO impusers' 
END
GO

ALTER PROCEDURE [dbo].[LRP_MEMBER_DOUBLE_VISIT_COUNT]
        @report_start_dt DATETIME = NULL,
        @report_end_dt DATETIME = NULL,
        @title_str VARCHAR(4000) = ''
AS BEGIN

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    SET NOCOUNT ON;

    /*  Procedure Variables and Temp Tables  */

        DECLARE @title_list TABLE ([title_no] INT NOT NULL DEFAULT(0))

        IF OBJECT_ID('tempdb..#dupe_check') IS NOT NULL DROP TABLE [#dupe_check];

        CREATE TABLE [#dupe_check] ([row_num] INT NOT NULL DEFAULT (1),
                                    [order_no] INT NOT NULL DEFAULT (0),
                                    [attendance_id] INT NOT NULL DEFAULT (0),
                                    [attendance_date] DATE NULL,
                                    [customer_no] INT NOT NULL DEFAULT (0),
                                    [title_name] VARCHAR(30) NOT NULL DEFAULT (''),
                                    [cust_memb_no] INT NOT NULL DEFAULT (0),
                                    [memb_level] VARCHAR(50) NOT NULL DEFAULT (''),
                                    [perf_no] INT NOT NULL DEFAULT (0),
                                    [zone_no] INT NOT NULL DEFAULT (0),
                                    [attendance_scan] INT NOT NULL DEFAULT (0),
                                    [attendance_venue] INT NOT NULL DEFAULT (0));

        IF OBJECT_ID('tempdb..#final_table') IS NOT NULL DROP TABLE [#final_table];
   
        CREATE TABLE #final_table ([attendance_date] DATE NULL,
                                   [customer_no] INT NOT NULL DEFAULT (0),
                                   [member_scan] INT NOT NULL DEFAULT (0), 
                                   [venue_tickets] INT NOT NULL DEFAULT (0),
                                   [double_counted] INT NOT NULL DEFAULT (0))


    /*  Check Parameters  */

        --If either date parameter is null, set it to yesterday
        SELECT @report_start_dt = ISNULL(@report_start_dt,DATEADD(DAY,-1,GETDATE()))
        SELECT @report_end_dt = ISNULL(@report_end_dt,DATEADD(DAY,-1,GETDATE()))
    
        --Set start date to midnight and end date to 11:59
        SELECT @report_start_dt = CAST(@report_start_dt AS DATE)
        SELECT @report_end_dt = CONVERT(CHAR(10),@report_end_dt,111) + ' 23:59:59.957'
        
        --PARSE TITLE LIST
        IF ISNULL(@title_str,'') <> ''
            INSERT INTO @title_list ([title_no])
            SELECT CAST([Element] AS INT) FROM dbo.FT_SPLIT_LIST(REPLACE(@title_str,'"',''),',')
        ELSE
            INSERT INTO @title_list ([title_no])
            SELECT [title_no]
            FROM [dbo].[LV_PRODUCTION_ELEMENTS_TITLE]
            WHERE [content_type] = 'Visit Count' 
              AND [content_value] = 'Y'
              AND [title_no] <> 27  --27 = Exhibit Halls.  It is removed under presumption that no one who scanned a 
                                    --                     member card would also buy themself an Exhibit Hall Ticket.

    /*  First select pulls membership scans from the T_ATTENDANCE table for designated dates  */    

        INSERT INTO [#dupe_check] ([row_num], [order_no], [attendance_id], [attendance_date], [customer_no], [title_name], 
                                   [cust_memb_no], [memb_level], [perf_no], [zone_no], [attendance_scan], [attendance_venue])
        SELECT 1,                                   --All row numbers are 1 in this selection,
               0,                                   --No order number in this data
               att.[id], 
               CAST(att.[attend_dt] AS DATE),
               ISNULL(att.[customer_no],0),
               'Member Scan',                       --All the same title Name
               ISNULL(att.[cust_memb_no],0),
               ISNULL(lev.[description],''),        --Membership Level
               ISNULL(att.[perf_no],0),
               236,                                 --All the same zone number - 236 = primary zone # for public Exhibit Halls
               ISNULL(att.[admission_adult], 0) 
                    + ISNULL(att.[admission_child], 0) 
                    + ISNULL(att.[admission_other], 0),
               0                                    --No Venue Attendance in this data
        FROM [dbo].[T_ATTENDANCE] AS att                                                                
             LEFT OUTER JOIN [dbo].[T_MEMB_LEVEL] AS lev ON lev.[memb_level_no] = att.[memb_level_no]
        WHERE ISNULL(att.[ticket_no], 0) = 0 
          AND att.[attend_dt] BETWEEN @report_start_dt AND @report_end_dt 
          AND att.[area_no] IS NULL;

    /*  Second select pulls order information for the customers gathered in the first select.  Each row has a row number that resets with
        the order number.  It uses a CTE of order information where the performance date is within the designated dates, and the performance
        title is a visit count title (except exhibit Halls), and the subline status is either seated, Paid or Ticketed Paid.  */

        WITH [CTE_ORDERS]
        AS (
            SELECT sli.[order_no],
                   ord.[customer_no],
                   sli.[perf_no],
                   sli.[zone_no],
                   CAST(prf.[performance_dt] AS DATE) AS [performance_dt],
                   prf.[title_name],
                   COUNT(sli.[sli_no]) AS [attendance_venue]
            FROM [dbo].[T_SUB_LINEITEM] AS sli
                 INNER JOIN [dbo].[T_ORDER] AS ord ON ord.[order_no] = sli.[order_no]
                 LEFT OUTER JOIN [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] AS prf ON prf.[performance_no] = sli.[perf_no] 
                                                                                  AND prf.[performance_zone] = sli.[zone_no]
                 INNER JOIN @title_list AS ttl ON ttl.[title_no] = prf.[title_no]
            WHERE prf.performance_dt BETWEEN @report_start_dt AND @report_end_dt
              AND ord.customer_no IN (SELECT [customer_no] FROM [#dupe_check])
              AND sli.[sli_status] IN (3, 12)
              AND prf.[title_name] <> 'Exhibit Halls'  
            GROUP BY sli.[order_no], ord.[customer_no], sli.[perf_no], sli.[zone_no], prf.[performance_dt], prf.[title_name]
           )
                INSERT INTO [#dupe_check] ([row_num], [order_no], [attendance_id], [attendance_date], [customer_no], [title_name], 
                                           [cust_memb_no], [memb_level], [perf_no], [zone_no], [attendance_scan], [attendance_venue])
                SELECT ROW_NUMBER() OVER(PARTITION BY cte.[order_no] ORDER BY cte.[attendance_venue] DESC),
                       cte.[order_no],
                       0,                           --No Attendance Id in this data
                       cte.[performance_dt],
                       cte.[customer_no],
                       cte.[title_name],
                       0,                           --No cust_memb_no in this data
                       '',                          --No Membership Level in this data
                       cte.[perf_no],
                       cte.[zone_no],
                       0,                           --No Scan Attendance in this data
                       cte.[attendance_venue]
                FROM [CTE_ORDERS] AS cte
                ORDER BY cte.[customer_no], cte.[order_no], cte.[attendance_venue]

    /*  Delete anything from the temp table that has a row number > 1.  This will make sure there is only one row per order in the table  */
                
        DELETE FROM [#dupe_check] 
        WHERE [row_num] > 1
        
    /*  Aggregate the date based on attendance date and customer number.  
        The double_counted value is the lower of the two attendance values
            --attendance_scan or attendance_venue  */

        INSERT INTO [#final_table] ([attendance_date], [customer_no], [member_scan], [venue_tickets], [double_counted])
        SELECT [attendance_date], 
               [customer_no], 
               SUM([attendance_scan]), 
               SUM([attendance_venue]),
               CASE WHEN SUM([attendance_scan]) < SUM([attendance_venue]) THEN SUM([attendance_scan])
                    ELSE SUM([attendance_venue]) END AS [double_counted]
        FROM [#dupe_check] 
        GROUP BY [attendance_date], [customer_no]
        HAVING SUM(attendance_scan) > 0 AND SUM([attendance_venue]) > 0
                

    FINISHED:

        /*  Pull final data set for report  */

            SELECT [attendance_date],
                   DATENAME(MONTH,[attendance_date]) + ', ' + DATENAME(YEAR,[attendance_date]) AS [attendance_month],
                   LEFT(CONVERT(CHAR(10),[attendance_date],111),7) AS [attendance_month_sort],
                   [customer_no],
                   [member_scan],
                   [venue_tickets],
                   [double_counted]
            FROM [#final_table]

  END
  GO


  --EXECUTE [dbo].[LRP_MEMBER_DOUBLE_VISIT_COUNT] @report_start_dt = '12-20-2019', @report_end_dt = '12-20-2019', @title_str = ''


