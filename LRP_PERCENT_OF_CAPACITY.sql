USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_LIBRARY_ATTENDANCE]') AND type in (N'P', N'PC'))
BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_PERCENT_OF_CAPACITY] AS' 
    EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LRP_PERCENT_OF_CAPACITY] TO ImpUsers' 
END
GO

/*  9-28-2019:  Updated procedure to include title parameter
                Also rewrote a little of the code to no longer need a cursor
*/
ALTER PROCEDURE [dbo].[LRP_PERCENT_OF_CAPACITY]
        @report_start_date DATETIME = NULL,
        @report_end_date DATETIME = NULL,
        @capacity_type VARCHAR(30) = 'Soft Capacity',    --Soft Capacity or Hard Capacity  
        @group_level VARCHAR(30) = 'Production',         --Title, Production, or Peformance
        @day_of_week VARCHAR(30) = 'All Days',           --Weekdays, WeekEnds, or Single day of the week
        @show_individual_dates CHAR(1) = 'N',            --Separate Data By Date?
        @title_str VARCHAR(4000) = NULL                  --List of specific Title numbers to include
AS BEGIN

    SET NOCOUNT ON;
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    SET ANSI_WARNINGS OFF;

    /*  Procedure Variables and Temp Tables  */

        DECLARE @exh_title_no INT = 27;           --27 = Inventory Number for Exhibit Halls

        DECLARE @title_list TABLE ([title_no] INT);

        IF OBJECT_ID('tempdb..#work_table') IS NOT NULL DROP TABLE [#work_table]

        CREATE TABLE #work_table ([title_no] INT,
                                  [title_name] VARCHAR(30), 
                                  [perf_no] INT, 
                                  [perf_code] VARCHAR(10), 
                                  [perf_dt] DATETIME, 
                                  [perf_day] VARCHAR(30), 
                                  [perf_date] CHAR(10), 
                                  [perf_time] VARCHAR(10), 
                                  [prod_name] VARCHAR(30),
                                  [prod_name_long] VARCHAR(150), 
                                  [prod_season_name] VARCHAR(30),
                                  [zone_map_no] INT, 
                                  [zone_map_name] VARCHAR(30), 
                                  [zone_no] INT,  
                                  [total_capacity] DECIMAL(18,2), 
                                  [held_seats] DECIMAL(18,2), 
                                  [soft_capacity] DECIMAL(18,2), 
                                  [tix_sold] DECIMAL(18,2), 
                                  [tix_avail] DECIMAL(18,2),
                                  [tix_scanned] DECIMAL(18,2),
                                  [exh_halls_sold] DECIMAL(18,2))

        CREATE TABLE [#exh_hall_sales] ([perf_date] CHAR(10), 
                                        [tix_sold] DECIMAL(18,2))

        IF OBJECT_ID('tempdb..#results_table') IS NOT NULL DROP TABLE [#results_table]

        CREATE TABLE #results_table ([title_no] INT,
                                     [title_name] VARCHAR(30), 
                                     [prod_name] VARCHAR(200), 
                                     [perf_date] VARCHAR(10), 
                                     [perf_time] VARCHAR(8), 
                                     [hard_capacity] DECIMAL(18,2), 
                                     [soft_capacity] DECIMAL(18,2),
                                     [tix_sold] DECIMAL(18,2), 
                                     [tix_scanned] DECIMAL(18,2),
                                     [percent_sold] DECIMAL(18,2), 
                                     [percent_scanned] DECIMAL(18,2),
                                     [record_count] INT)

    /*  Check Parameters  */
        
        SELECT @report_start_date = ISNULL(@report_start_date,GETDATE())

        SELECT @report_end_date = ISNULL(@report_end_date,GETDATE())

        SELECT @report_start_date = CAST(@report_start_date AS DATE),
               @report_end_date = convert(char(10),@report_end_date,111) + ' 23:59:59.957'
  
        SELECT @capacity_type = IsNull(@capacity_type, '')
        IF @capacity_type = 'Hard' SELECT @capacity_type = 'Hard Capacity'
        IF @capacity_type <> 'Hard Capacity' SELECT @capacity_type = 'Soft Capacity'

        SELECT @group_level = IsNull(@group_level,'')
        IF @group_level not in ('Title', 'Performance') SELECT @group_level = 'Production'

        SELECT @day_of_week = IsNull(@day_of_week,'All Days')
        IF @day_of_week in ('Weekday', 'Weekend') SELECT @day_of_week = (@day_of_week + 's')
        IF @day_of_week not in ('Weekdays', 'Weekends', 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday') SELECT @day_of_week = 'All Days'

        SELECT @show_individual_dates = ISNULL(@show_individual_dates,'N')

        IF ISNULL(@title_str,'') = ''
            INSERT INTO @title_list ([title_no])
            SELECT [inv_no]
            FROM [dbo].[T_INVENTORY]
            WHERE [type] = 'T'
              AND [inv_no] NOT IN (42, 61, 1126, 1148, 1395, 1398, 5541, 7179, 7314, 17825, 53191)
        ELSE
            INSERT INTO @title_list ([title_no])
            SELECT [Element]
            FROM dbo.FT_SPLIT_LIST(REPLACE(@title_str,'"',''),',')
           
    /*  Get raw performance data  */
        
        INSERT INTO #work_table ([title_no], [title_name], [perf_no], [perf_code], [perf_dt], [perf_day], [perf_date], [perf_time], [prod_name], 
                                 [prod_name_long], [prod_season_name], [zone_map_no], [zone_map_name], [zone_no],[total_capacity], [held_seats], 
                                 [tix_sold], [tix_scanned])
        SELECT prf.[title_no],
               prf.[title_name], 
               prf.[performance_no], 
               prf.[performance_code], 
               prf.[performance_dt], 
               DATENAME(WEEKDAY,prf.[performance_dt]), 
               prf.[performance_date], 
               prf.[performance_time], 
               prf.[production_name], 
               prf.[production_name_long], 
               prf.[production_season_name],
               prf.[performance_zone_map], 
               prf.[performance_zone_map_name], 
               prf.[performance_zone],
               [dbo].[LF_GetCapacity] (prf.[performance_no], prf.[performance_zone], 'total'),
               [dbo].[LF_GetCapacity] (prf.[performance_no], prf.[performance_zone], 'hold'),
               [dbo].[LF_GetCapacity] (prf.[performance_no], prf.[performance_zone], 'sold'),
               [dbo].[LF_GetCapacity] (prf.[performance_no], prf.[performance_zone], 'scanned')
        FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] as prf
             INNER JOIN @title_list AS ttl ON ttl.[title_no] = prf.[title_no]
        WHERE prf.[performance_dt] between @report_start_date and @report_end_date
          AND prf.[production_name] NOT LIKE '%Buyout%'
          AND LEFT(prf.[production_season_name],2) <> 'z-';

    /*  Get the Exhibit Hall Total Sales for each date in the range  */

        INSERT INTO [#exh_hall_sales] ([perf_date], [tix_sold])
        SELECT performance_date,
               SUM([dbo].[LF_GetCapacity] ([performance_no], [performance_zone], 'Sold'))
        FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE]
        WHERE performance_dt BETWEEN @report_start_date AND @report_end_date
          AND [title_no] = @exh_title_no
          AND [production_name] NOT LIKE '%Buyout%'
          AND LEFT([production_season_name],2) <> 'z-'
        GROUP BY performance_date;

    /*  Remove Unwanted Days  */

        IF @day_of_week = 'Weekends'
            DELETE FROM #work_table 
            WHERE [perf_day] not in ('Saturday','Sunday')
        ELSE IF @day_of_week = 'Weekdays'
            DELETE FROM #work_table 
            WHERE [perf_day] in ('Saturday','Sunday')
        ELSE IF @day_of_week <> 'All Days'
            DELETE FROM #work_table 
            WHERE [perf_day] <> @day_of_week

    /*  Update calculated fields  */

        UPDATE [#work_table]
        SET [soft_capacity] = ([total_capacity] - [held_seats])

        UPDATE [#work_table]
        SET [tix_avail] = CASE WHEN @capacity_type = 'Soft Capacity' THEN ([soft_capacity] - [tix_sold])
                               ELSE ([total_capacity] - [tix_sold]) END

    /*  Create Final Results - Percents based on Soft Capacity by Default - Change later if needed  */

        IF @group_level = 'Title' 

            INSERT INTO [#results_table] ([title_no], [title_name], [prod_name], [perf_date], [perf_time], 
                                          [hard_capacity], [soft_capacity], [tix_sold], [percent_sold], [record_count])
            SELECT [title_no],
                   [title_name], 
                   '', 
                   CASE WHEN @show_individual_dates = 'Y' THEN [perf_date] ELSE '' END, 
                   '', 
                   SUM([total_capacity]), 
                   SUM([soft_capacity]), 
                   SUM([tix_sold]),
                   CASE WHEN sum([soft_capacity]) = 0 THEN 0.00 
                        ELSE (sum([tix_sold]) / sum([soft_capacity])) END, 
                   COUNT([title_name])
            FROM [#work_table] 
            GROUP BY [title_no], [title_name],
                     CASE WHEN @show_individual_dates = 'Y' THEN [perf_date] ELSE '' END

        ELSE IF @group_level = 'Production' 

            INSERT INTO [#results_table] ([title_no], [title_name], [prod_name], [perf_date], [perf_time], 
                                          [hard_capacity], [soft_capacity], [tix_sold], [tix_scanned], [percent_sold], 
                                          [percent_scanned], [record_count])
            SELECT [title_no],
                   [title_name], 
                   [prod_name], 
                   CASE WHEN @show_individual_dates = 'Y' THEN [perf_date] ELSE '' END, 
                   CONVERT(varchar(8),''), 
                   SUM([total_capacity]), 
                   SUM([soft_capacity]), 
                   SUM([tix_sold]),  
                   SUM([tix_scanned]),
                   CASE WHEN SUM([soft_capacity]) = 0 THEN 0.00 
                        ELSE (SUM([tix_sold]) / SUM([soft_capacity])) END, 
                   CASE WHEN SUM([tix_sold]) = 0 THEN 0.00
                        ELSE (SUM([tix_scanned]) / SUM([tix_sold])) END,
                   COUNT([prod_name])
            FROM [#work_table] 
            GROUP BY [title_no], [title_name], [prod_name], 
                     CASE WHEN @show_individual_dates = 'Y' THEN [perf_date] ELSE '' END

        ELSE

            INSERT INTO [#results_table] ([title_no], [title_name], [prod_name], [perf_date], [perf_time], 
                                          [hard_capacity], [soft_capacity], [tix_sold], [tix_scanned], [percent_sold], 
                                          [percent_scanned], [record_count])
            SELECT [title_no],
                   [title_name], 
                   [prod_name], 
                   [perf_date], 
                   [perf_time], 
                   [total_capacity], 
                   [soft_capacity], 
                   [tix_sold],
                   [tix_scanned],
                   CASE WHEN [soft_capacity] = 0 THEN 0.00 
                        ELSE ([tix_sold] / [soft_capacity]) END, 
                   CASE WHEN [tix_sold] = 0 THEN 0.00
                        ELSE ([tix_scanned] / [tix_sold]) END,
                   1
            FROM [#work_table] 

    /*  If grouped by Performance, adjust the production name to include perf data and time  */

        IF @group_level = 'Performance' 
            UPDATE [#results_table] 
            SET [prod_name] = left([prod_name],10) + ' ' + [perf_date] + ' ' + [perf_time]

    /*  If percents should be based on hard or total capacity, change the percentage now  */
        
        IF @capacity_type in ('Hard', 'Hard Capacity') 
            UPDATE [#results_table] 
            SET [percent_sold] = CASE WHEN [hard_capacity] = 0 THEN 0.00
                                      ELSE ([tix_sold] / [hard_capacity]) END
       
    FINISHED:

        IF @show_individual_dates = 'N'
            UPDATE [#exh_hall_sales] SET [perf_date] = ''
                
        /*  Pull final data selection  */

            SELECT r.[title_name], 
                   r.[prod_name], 
                   r.[perf_date], 
                   r.[perf_time], 
                   CASE WHEN @capacity_type in ('Hard', 'Hard Capacity') THEN r.[hard_capacity]
                        ELSE r.[soft_capacity] END as [total_capacity], 
                   r.[tix_sold], 
                   r.[percent_sold], 
                   r.[record_count],
                   r.[tix_scanned],
                   r.[percent_scanned],
                   SUM(e.[tix_sold]) AS [total_exhibit_sales],
                   CAST(r.[tix_sold] / SUM(e.[tix_sold]) AS DECIMAL(18,4)) AS [percent_of_exh_sales]                   
            FROM [#results_table] AS r
                 LEFT OUTER JOIN [#exh_hall_sales] AS e ON e.[perf_date] = r.[perf_date]
            GROUP BY r.[title_name], r.[prod_name], r.[perf_date], r.[perf_time], 
                     CASE WHEN @capacity_type in ('Hard', 'Hard Capacity') THEN r.[hard_capacity]
                          ELSE r.[soft_capacity] END, 
                     r.[tix_sold], r.[percent_sold], r.[record_count], r.[tix_scanned], r.[percent_scanned]
            ORDER BY r.[title_name], r.[prod_name], r.[perf_date], r.[perf_time]

    CLEAN_UP:

        IF OBJECT_ID('tempdb..#results_table') IS NOT NULL DROP TABLE [#results_table]

        IF OBJECT_ID('tempdb..#work_table') IS NOT NULL DROP TABLE [#work_table]

END
GO


--EXECUTE [dbo].[LRP_PERCENT_OF_CAPACITY] '7-21-2020', '7-28-2020', 'Soft Capacity', 'Production', 'All Days', 'Y', '27'
--EXECUTE [dbo].[LRP_PERCENT_OF_CAPACITY] '7-22-2020', '7-28-2020', 'Soft Capacity', 'Performance', 'All Days', 'Y', '27'

     


     --SELECT * FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] WHERE performance_date = '2020/07/28' AND performance_time = '13:00'
     --82992 / 728
