USE [impresario]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LRP_CORP_MEMB_CONTRIBUTIONS]
( -- do not make these parameters DATETIME
	@start_dt1 DATE,
	@end_dt1 DATE,
	@start_dt2 DATE,
	@end_dt2 DATE 
)
AS

--EXEC [LRP_CORP_MEMB_CONTRIBUTIONS] '7/1/2017', '12/31/2017', '7/1/2018', '12/31/2018'

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

SELECT c.customer_no,
    cs.esal1_desc,
    c.cont_dt,
	btch.fyear,
	btch.period,
    MONTH(cont_dt) mnth,
    c.cont_amt,
    m.cust_memb_no,
    m.memb_level,
    m.NRR_status,
    t.description membtrend,
    cust.sort_name
FROM T_CONTRIBUTION AS c
INNER JOIN dbo.T_CAMPAIGN camp
	ON camp.campaign_no = c.campaign_no
	AND camp.category = 31 --Corporate Memb
INNER JOIN TR_Batch_Period btch
	ON c.cont_dt BETWEEN btch.start_dt AND btch.end_dt
INNER JOIN TX_CUST_SAL AS cs
    ON c.customer_no = cs.customer_no
	AND cs.default_ind = 'Y'
INNER JOIN T_CUSTOMER AS cust
    ON c.customer_no = cust.customer_no
LEFT OUTER JOIN TX_CONT_MEMB AS cm
    ON c.ref_no = cm.cont_ref_no
LEFT OUTER JOIN TX_CUST_MEMBERSHIP AS m
    ON cm.cust_memb_no = m.cust_memb_no
LEFT OUTER JOIN TR_MEMB_TREND AS t
    ON m.memb_trend = t.id
WHERE c.cont_amt > 0
AND 
(
    CAST(c.cont_dt AS DATE) BETWEEN @start_dt1 AND @end_dt1
    OR CAST(c.cont_dt AS DATE) BETWEEN @start_dt2 AND @end_dt2
)
