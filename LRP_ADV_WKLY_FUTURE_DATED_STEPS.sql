USE [impresario]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[LRP_ADV_WKLY_FUTURE_DATED_STEPS]
(
	@numOfDays INT = 15,
	@camp_category_str VARCHAR(4000) = NULL
)
AS
--FUTURE DATED STEPS 

-- USED IN 2 REPORTS: MOS: WEEKLY ACTIVITY REPORT AND ADV PM PORTFOLIO ACTIVITY

SET NOCOUNT ON 
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

DECLARE	@camp_categories TABLE (id INT NOT NULL)

IF ISNULL(@camp_category_str,'') <> ''
	INSERT INTO @camp_categories (id)
	SELECT Element From dbo.FT_SPLIT_LIST(@camp_category_str,',')
ELSE
	INSERT INTO @camp_categories (id)
	SELECT id
	FROM dbo.TR_CAMPAIGN_CATEGORY

SELECT p.customer_no, cust.display_name, cust.sort_name, cam.description AS campaign_desc, cdes.description AS cont_designation_desc,
stype.description AS step_type_desc, s.step_dt, s.description AS step_desc, LEFT(s.notes, 300) AS step_notes, s.worker_customer_no, wkr.display_name AS worker_display_name
FROM dbo.T_PLAN p
INNER JOIN T_STEP s
	ON s.plan_no = p.plan_no
INNER JOIN dbo.TR_STEP_TYPE stype
	ON stype.id = s.step_type
INNER JOIN dbo.T_CAMPAIGN cam
	ON cam.campaign_no = p.campaign_no
INNER JOIN @camp_categories ccat
	ON ccat.id = cam.category
INNER JOIN dbo.TR_CONT_DESIGNATION cdes
	ON cdes.id = p.cont_designation
INNER JOIN dbo.FT_CONSTITUENT_DISPLAY_NAME() AS cust 
	ON p.customer_no = cust.customer_no
LEFT JOIN dbo.FT_CONSTITUENT_DISPLAY_NAME() AS wkr
	ON s.worker_customer_no = wkr.customer_no
WHERE p.type = 7 -- Advancement Pipeline
	AND DATEDIFF(d, GETDATE(), s.step_dt) BETWEEN 1 AND @numOfDays -- The step date <> today�s date. 
