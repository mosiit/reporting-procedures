USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_TESSITURA_CASHOUT_CASH_DROPS]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LRP_TESSITURA_CASHOUT_CASH_DROPS]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LRP_TESSITURA_CASHOUT_CASH_DROPS]
        @report_dt DATETIME = null,
        @report_operator VARCHAR(10) = '',
        @machine_locations VARCHAR(4000) = ''
AS BEGIN

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

    /*  Procedure Variables  */

        DECLARE @location_list TABLE ([location_no_str] VARCHAR(20), [location_no] INT)
        DECLARE @machine_list TABLE ([machine_name] VARCHAR(50))
        DECLARE @cash_drops TABLE ([operator_id] VARCHAR(10), 
                                   [create_dt] DATETIME,
                                   [create_loc] VARCHAR(30),
                                   [order_no] INT, 
                                   [production_name] VARCHAR(30), 
                                   [drop_amount] DECIMAL(18,2))

    /*  Check Parameters  - Null Date = Today, Null Operator = All  */

        SELECT @report_dt = CAST(ISNULL(@report_dt, GETDATE()) AS DATE)
        SELECT @report_operator = ISNULL(@report_operator,'')
        SELECT @machine_locations = ISNULL(@machine_locations,'')
    
    /*  Parse machine_locations parameter  */

        INSERT INTO @location_list ([location_no_str]) 
        SELECT [item] FROM [dbo].[LFT_SPLIT_DILEMETED_STRING] (@machine_locations, ',')

        DELETE FROM @location_list WHERE [dbo].[FS_isReallyNumeric] ([location_no_str]) = 0

        UPDATE @location_list SET [location_no] = CONVERT(INT,[location_no_str])

        INSERT INTO @machine_list ([machine_name])
        SELECT [machine_name] FROM [dbo].[TX_MACHINE_LOCATION] WHERE [location] IN (SELECT [location_no] FROM @location_list)

    /*  GET All membership Records for this operator on this day  */
        
        INSERT INTO @cash_drops ([operator_id], [create_dt], [create_loc], [order_no], [production_name], [drop_amount])
        SELECT det.[created_by], 
               det.[create_dt],
               det.[create_loc],
               det.[order_no], 
               det.[production_name], 
               CASE WHEN [dbo].[FS_isReallyNumeric]([price_type_name_short]) = 0 THEN 0.0
                    ELSE CAST(det.[price_type_name_short] AS DECIMAL(18,2)) END
        FROM [dbo].[LV_ORDER_DETAIL] AS det
             --INNER JOIN @machine_list AS mac ON mac.machine_name = det.create_loc
        WHERE CAST(det.[create_dt] AS DATE) = @report_dt
          AND (det.[created_by]= @report_operator OR @report_operator = '')
          AND det.[production_name] = 'Cash Drop'

    FINISHED:  

        SELECT [operator_id], 
               [create_dt],
               [create_loc],
               [order_no], 
               [production_name], 
               [drop_amount]
        FROM @cash_drops

END
GO

GRANT EXECUTE ON [dbo].[LRP_TESSITURA_CASHOUT_CASH_DROPS] TO ImpUsers
GO

--EXECUTE [dbo].[LRP_TESSITURA_CASHOUT_CASH_DROPS] @report_dt = '1-22-2019', @report_operator = 'ahile00', @machine_locations = '15,16'


          