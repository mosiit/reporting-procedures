USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LRP_CASH_OUT_ENTRY_REPORT]    Script Date: 5/17/2016 2:24:26 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

DROP PROCEDURE [dbo].[LRP_CASH_OUT_ENTRY_REPORT]
GO

CREATE PROCEDURE [dbo].[LRP_CASH_OUT_ENTRY_REPORT]
       @rpt_dt DATETIME,
	   @rpt_location VARCHAR(30)
AS 
	BEGIN

	   SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	   SET NOCOUNT ON

       DECLARE @tblValues TABLE	(
	   							[Date] CHAR(10),
								[Location] VARCHAR(30),
                                [Operator] VARCHAR(8),
								[Operator Name] VARCHAR(60),
                                [Cash] DECIMAL(18, 2),
                                [Check] DECIMAL(18, 2),
                                [Credit Card] DECIMAL(18, 2),
                                [Gift Certificate] DECIMAL(18, 2),
								[Invoice] DECIMAL(18, 2),
                                [Refunds] DECIMAL(18, 2),
                                [Other] DECIMAL(18, 2),
								[Adult CityPass Voucher] DECIMAL(18,2),
								[Child CityPass Voucher] DECIMAL(18,2),
								[Total] DECIMAL(18,2)
								)

       INSERT   INTO @tblValues
                SELECT  [payment_date],
						[payment_operator_location],
                        [payment_operator],
						[payment_operator_last_name] + ', ' + [payment_operator_first_name] AS [Operator Name],
                        ISNULL([Cash], 0) AS [Cash],
                        ISNULL([Check], 0) AS [Check],
                        ISNULL([Credit Card], 0) AS [Credit Card],
                        ISNULL([Gift Certificate], 0) AS [Gift Certificate],
						ISNULL([Invoice], 0) AS [Invoice],
                        ISNULL([Refunds], 0) AS [Refunds],
                        ISNULL([Other], 0) AS [Other],
						ISNULL([Adult CityPass Voucher], 0) AS [Adult CityPass Voucher],
						ISNULL([Child CityPass Voucher], 0) AS [Child CityPass Voucher],
                        (ISNULL([Cash], 0) + ISNULL([Check], 0) + ISNULL([Credit Card], 0) + ISNULL([Gift Certificate], 0) + ISNULL([Invoice], 0) + ISNULL([Other], 0)) - (ISNULL([Refunds], 0)) AS [Total]
                FROM    (SELECT [payment_date],
								[payment_operator],
								[payment_operator_last_name],
								[payment_operator_first_name],
								[payment_operator_location],
                                [payment_type_name],
                                [payment_amount]
                         FROM   [dbo].[LT_CASH_OUT_DATA]
						 WHERE	[payment_date] = @rpt_dt
						 AND	[payment_operator_location] = @rpt_location) AS SourceTable PIVOT
	( SUM([payment_amount]) FOR payment_type_name IN ([Cash], [Check], [Credit Card], [Gift Certificate], [Invoice], [Refunds], [Other], [Adult CityPass Voucher], [Child CityPass Voucher], [Total]) ) AS PivotTable; 

	SELECT  [Date],
			Location,
			Operator,
			[Operator Name],
			Cash,
			[Check],
			[Credit Card],
			[Gift Certificate],
			Invoice,
			Refunds,
			Other,
			[Adult CityPass Voucher],
			[Child CityPass Voucher],
			Total 
	FROM	@tblValues
	ORDER BY Operator

	END

GO


