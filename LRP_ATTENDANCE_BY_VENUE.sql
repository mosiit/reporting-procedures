USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LRP_ATTENDANCE_BY_VENUE]    Script Date: 6/23/2016 1:41:34 PM ******/
DROP PROCEDURE [dbo].[LRP_ATTENDANCE_BY_VENUE]
GO

/****** Object:  StoredProcedure [dbo].[LRP_ATTENDANCE_BY_VENUE]    Script Date: 6/23/2016 1:41:34 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[LRP_ATTENDANCE_BY_VENUE]
       @title_name VARCHAR(30),
       @report_start_date DATETIME,
       @report_end_date DATETIME,
       @include_partial_paid CHAR(1) = 'Y'
AS 
       BEGIN

             SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

    /* Make sure the dates passed to the procedure include the entire day (from midnight on the start date to 11:59 PM on the end date)  */

             SELECT @report_start_date = CONVERT(CHAR(10), @report_start_date, 111) + ' 00:00:00',
                    @report_end_date = CONVERT(CHAR(10), @report_end_date, 111) + ' 23:59:59'

             SELECT @include_partial_paid = ISNULL(@include_partial_paid, 'Y')
             IF @include_partial_paid <> 'N' 
                SELECT  @include_partial_paid = 'Y'

    /*  Create temp tables needed for this report  */

             IF OBJECT_ID('tempdb..#sli_temp_table') IS NOT NULL 
                DROP TABLE [#sli_temp_table]
                    
             CREATE TABLE [#sli_temp_table] ([order_no] INT,
                                             [sli_no] INT,
                                             [perf_no] INT,
                                             [zone_no] INT,
                                             [due_amt] DECIMAL(18, 2),
                                             [paid_amt] DECIMAL(18, 2),
                                             [sli_status] INT,
                                             [comp_code] INT,
                                             [price_type] INT,
                                             [ticket_no] INT,
                                             [li_seq_no] INT)

             CREATE CLUSTERED INDEX [ix_sli_perf_no] ON [#sli_temp_table] ([perf_no] ASC) ON [PRIMARY]
             CREATE NONCLUSTERED INDEX [ix_sli_zone_no] ON [#sli_temp_table] ([zone_no] ASC) ON [PRIMARY]
             CREATE NONCLUSTERED INDEX [ix_sli_order_no] ON [#sli_temp_table] ([order_no] ASC) ON [PRIMARY]
             CREATE NONCLUSTERED INDEX [ix_sli_comp_code] ON [#sli_temp_table] ([comp_code] ASC) ON [PRIMARY]
             CREATE NONCLUSTERED INDEX [ix_sli_price_type] ON [#sli_temp_table] ([price_type] ASC) ON [PRIMARY]
             CREATE NONCLUSTERED INDEX [ix_sli_ticket_no] ON [#sli_temp_table] ([ticket_no] ASC) ON [PRIMARY]

             IF OBJECT_ID('tempdb..#ord_temp_table') IS NOT NULL 
                DROP TABLE [#ord_temp_table]
        
             CREATE TABLE [#ord_temp_table] ([order_no] INT,
                                             [total_due_amt] DECIMAL(18, 2),
                                             [total_paid_amt] DECIMAL(18, 2))

             CREATE CLUSTERED INDEX [ix_ord_order_no] ON [#ord_temp_table] ([order_no] ASC) ON [PRIMARY]

             IF OBJECT_ID('tempdb..#att_temp_table') IS NOT NULL 
                DROP TABLE [#att_temp_table]

             CREATE TABLE [#att_temp_table] ([attendance_type] VARCHAR(50),
                                             [performance_type] VARCHAR(50),
                                             [order_no] INT,
                                             [sli_no] INT,
                                             [li_seq_no] INT,
                                             [title_name] VARCHAR(30),
                                             [perf_no] INT,
                                             [perf_date] CHAR(10),
                                             [perf_time] CHAR(8),
                                             [zone_no] INT,
                                             [production_name] VARCHAR(50),
                                             [production_name_short] VARCHAR(50),
                                             [production_name_long] VARCHAR(150),
                                             [comp_code] INT,
                                             [comp_code_name] VARCHAR(50),
                                             [order_payment_status] VARCHAR(50),
                                             [price_type] INT,
                                             [price_type_name] VARCHAR(50),
                                             [due_amt] DECIMAL(18, 2),
                                             [paid_amt] DECIMAL(18, 2),
                                             [sale_total] INT,
                                             [performance_date] CHAR(10),
                                             [performance_time] CHAR(8),
                                             [scan_admission_date] CHAR(10),
                                             [scan_admission_time] CHAR(8),
                                             [scan_device] VARCHAR(30),
                                             [scan_admission_adult] INT,
                                             [scan_admission_child] INT,
                                             [scan_admission_other] INT,
                                             [scan_admission_auto] INT,
                                             [scan_admission_total] INT,
                                             [sli_status] INT,
                                             [report_start_date] DATETIME,
                                             [report_end_date] DATETIME,
                                             [include_partial_paid] CHAR(1))


    /*  Get Products Sold For date range  -  This table will keep a list of all subline items we will be working with
        Looks specifically for subline items where the status is 3 (Seated, Paid) or 12 (Ticketed, Paid)   */

             INSERT INTO [#sli_temp_table]
                    SELECT  [order_no],
                            [sli_no],
                            [perf_no],
                            [zone_no],
                            [due_amt],
                            [paid_amt],
                            [sli_status],
                            [comp_code],
                            [price_type],
                            [ticket_no],
                            [li_seq_no]
                    FROM    [dbo].[T_SUB_LINEITEM] AS sli (NOLOCK)
                    WHERE   sli.[perf_no] IN (SELECT    [perf_no]
                                              FROM      [dbo].[T_PERF]
                                              WHERE     [perf_dt] BETWEEN @report_start_date
                                                                  AND     @report_end_date)
                            AND [sli_status] IN (3, 12)

    /*  Get Order Information the products sold - This table will keep a list of all orders we will be working with   */

             INSERT INTO [#ord_temp_table]
                    SELECT  [order_no],
                            [tot_due_amt],
                            [tot_paid_amt]
                    FROM    [dbo].[T_ORDER] (NOLOCK)
                    WHERE   [order_no] IN (SELECT   [order_no]
                                           FROM     [#sli_temp_table])

    /*  Remove records that are not needed  */

        --Delete Unpaid Transactions from the order table
             DELETE FROM [#ord_temp_table]
             WHERE  [total_paid_amt] = 0.00

             IF @include_partial_paid <> 'Y' 
                DELETE  FROM [#ord_temp_table]
                WHERE   [total_paid_amt] < [total_due_amt]
        
        --Delete from subline items where the order is not included in the order table    
             DELETE FROM [#sli_temp_table]
             WHERE  [order_no] NOT IN (SELECT   [order_no]
                                       FROM     [#ord_temp_table])

    /*  Retrieve the TICKETED Attendance date from the database  */

             INSERT INTO [#att_temp_table]
                    SELECT  CASE WHEN prf.[production_gate_attendance] = 'N' THEN 'Non-Gate'
                                 ELSE 'Gate A (Ticketed)'
                            END,
                            prf.[performance_type_name],
                            sli.[order_no],
                            sli.[sli_no],
                            sli.[li_seq_no],
                            prf.[title_name],
                            sli.[perf_no],
                            prf.[performance_date],
                            prf.[performance_time],
                            sli.[zone_no],
                            prf.[production_name],
                            prf.[production_name_short],
                            prf.[production_name_long],
                            ISNULL(sli.[comp_code], 0),
                            ISNULL(cmp.[description], ''),
                            'paid',
                            ISNULL(sli.[price_type], 0),
                            ISNULL(typ.[description], 'Unknown'),
                            sli.[due_amt],
                            sli.[paid_amt],
                            1,
                            prf.[performance_date],
                            prf.[performance_time],
                            CASE WHEN prf.[production_auto_scan] = 'Y' THEN prf.[performance_date]
                                 ELSE CONVERT(CHAR(10), att.[attend_dt], 111)
                            END,
                            CASE WHEN prf.[production_auto_scan] = 'Y' THEN prf.[performance_time]
                                 ELSE CONVERT(CHAR(8), att.[attend_dt], 108)
                            END,
                            ISNULL(att.[device], ''),
                            ISNULL(att.[admission_adult], 0),
                            ISNULL(att.[admission_child], 0),
                            ISNULL(att.[admission_other], 0),
                            CASE WHEN prf.[production_auto_scan] = 'Y' THEN 1
                                 ELSE 0
                            END,
                            CASE WHEN prf.[production_auto_scan] = 'Y' THEN 1
                                 ELSE (ISNULL(att.[admission_adult], 0) + ISNULL(att.[admission_child], 0) + ISNULL(att.[admission_other], 0))
                            END,
                            sli.[sli_status],
                            @report_start_date,
                            @report_end_date,
                            @include_partial_paid
                    FROM    [T_SUB_LINEITEM] AS sli (NOLOCK)
                    LEFT OUTER JOIN [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] AS prf (NOLOCK) ON prf.[performance_no] = sli.[perf_no]
                                                                                                  AND prf.[performance_zone] = sli.[zone_no]
                    LEFT OUTER JOIN [dbo].[TR_COMP_CODE] AS cmp (NOLOCK) ON cmp.[id] = sli.[comp_code]
                    LEFT OUTER JOIN [dbo].[TR_PRICE_TYPE] AS typ (NOLOCK) ON typ.[id] = sli.[price_type]
                    LEFT OUTER JOIN [dbo].[T_ATTENDANCE] AS att (NOLOCK) ON att.[ticket_no] = sli.[ticket_no]
                    WHERE   prf.[performance_no] IN (SELECT [perf_no]
                                                     FROM   [#sli_temp_table])

    /*  Delete returned/refunded products  */
             DELETE FROM [#att_temp_table]
             WHERE  [sli_status] NOT IN (3, 12)

    /*  Set Traveling Programs Mileage Fee to its own product to separate it out in the report  */
             UPDATE [#att_temp_table]
             SET    production_name = 'Trav Prog Mileage Fee',
                    production_name_short = 'Mileage Fee',
                    production_name_long = 'Traveling Programs Mileage Fee'
             WHERE  [price_type_name] LIKE 'Trav Prog Mileage%'

    /*  Retrieve the NON-TICKETED Attendance data (minus Show and Go) from the database  */

             INSERT INTO [#att_temp_table]
                    SELECT  'Gate B (Non-Ticketed)',
                            'Public',
                            0,
                            0,
                            0,
                            'Gate Scan',
                            att.[perf_no],
                            prf.[performance_date],
                            prf.[performance_time],
                            0,
                            lev.[description],
                            lev.[description],
                            lev.[description],
                            0,
                            'No Discount',
                            'paid',
                            0,
                            '',
                            0.00,
                            0.00,
                            0,
                            prf.[performance_date],
                            prf.[performance_time],
                            CONVERT(CHAR(10), att.[attend_dt], 111),
                            CONVERT(CHAR(8), att.[attend_dt], 108),
                            ISNULL(att.[device], ''),
                            ISNULL(att.[admission_adult], 0) AS 'scan_admission_adult',
                            ISNULL(att.[admission_child], 0) AS 'scan_admission_child',
                            ISNULL(att.[admission_other], 0) AS 'scan_addmission_other',
                            0,
                            (ISNULL(att.[admission_adult], 0) + ISNULL(att.[admission_child], 0) + ISNULL(att.[admission_other], 0)),
                            0,
                            @report_start_date,
                            @report_end_date,
                            @include_partial_paid
                    FROM    [dbo].[T_ATTENDANCE] AS att (NOLOCK)
                    LEFT OUTER JOIN [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] AS prf (NOLOCK) ON prf.[performance_no] = att.[perf_no]
                                                                                                  AND prf.[performance_zone] = att.[perf_no]
                    LEFT OUTER JOIN [dbo].[T_MEMB_LEVEL] AS lev (NOLOCK) ON lev.[memb_level_no] = att.[memb_level_no]
                    WHERE   (att.[ticket_no] IS NULL
                            OR att.[ticket_no] = 0
                            )
                            AND att.[attend_dt] BETWEEN @report_start_date
                                                AND     @report_end_date
                            AND att.[area_no] IS NULL

    /*  Retrieve the SHOW AND GO Attendance data (minus Show and Go) from the database  */

             INSERT INTO [#att_temp_table]
                    SELECT  'Gate B (Non-Ticketed)',
                            'Public',
                            0,
                            0,
                            0,
                            'Show and Go',
                            0,
                            '',
                            '',
                            0,
                            [show_and_go_name],
                            [show_and_go_name],
                            [show_and_go_name],
                            0,
                            'Free Admission',
                            'Free',
                            0,
                            '',
                            0.00,
                            0.00,
                            0,
                            [scan_date],
                            [scan_time],
                            [scan_date],
                            [scan_time],
                            [device_name],
                            0,
                            0,
                            [scan_admission],
                            0,
                            [scan_admission],
                            0,
                            @report_start_date,
                            @report_end_date,
                            @include_partial_paid
                    FROM    [dbo].[LV_RPT_SHOW_AND_GO]
                    WHERE   [scan_dt] BETWEEN @report_start_date
                                      AND     @report_end_date

             DELETE FROM [#att_temp_table]
             WHERE  [title_name] = 'Show and Go'
                    AND [production_name] = 'Stamped Hands at Discovery Center'

             DONE:
    
        /* Select aggregated date into the report  */
  
		-- 2016/05/15, H. Sheridan - Add value to field to indicate no rows returned
             IF (SELECT COUNT([attendance_type])
                 FROM   [#att_temp_table]) = 0 
                SELECT  'No rows found' AS [attendance_type],
                        '' AS [performance_type],
                        '' AS [title_name],
                        '' AS [production_name],
                        '' AS [production_name_short],
                        '' AS [production_name_long],
                        '' AS [comp_code_name],
                        '' AS [order_payment_status],
                        0 AS 'sale_total',
                        0 AS 'scan_admission_total',
                        0 AS 'due_amt',
                        0 AS 'paid_amt',
                        @report_start_date AS 'p_report_start_date',
                        @report_end_date AS 'p_report_end_date',
                        @include_partial_paid AS 'p_include_partial_paid'
             ELSE 
                SELECT  [attendance_type],
                        [performance_type],
                        [title_name],
                        [production_name],
                        [production_name_short],
                        [production_name_long],
                        [comp_code_name],
                        [order_payment_status],
                        SUM([sale_total]) AS 'sale_total',
                        SUM([scan_admission_total]) AS 'scan_admission_total',
                        SUM([due_amt]) AS 'due_amt',
                        SUM([paid_amt]) AS 'paid_amt',
                        @report_start_date AS 'p_report_start_date',
                        @report_end_date AS 'p_report_end_date',
                        @include_partial_paid AS 'p_include_partial_paid'
                FROM    [#att_temp_table]
                WHERE   [title_name] = @title_name
                GROUP BY [attendance_type],
                        [performance_type],
                        [title_name],
                        [production_name],
                        [production_name_short],
                        [production_name_long],
                        [comp_code_name],
                        [order_payment_status]

             CLEAN_UP:

        /*  Not really necessary, but the programmer in me wants to clean up  */

             IF OBJECT_ID('tempdb..#sli_temp_table') IS NOT NULL 
                DROP TABLE [#att_temp_table]
             IF OBJECT_ID('tempdb..#sli_temp_table') IS NOT NULL 
                DROP TABLE [#ord_temp_table]
             IF OBJECT_ID('tempdb..#sli_temp_table') IS NOT NULL 
                DROP TABLE [#sli_temp_table]
        
       END

GO


