USE [impresario]
GO

USE impresario;
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LP_UPDATE_HISTORY_VISIT_COUNT]') AND type in (N'P', N'PC')) BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LP_UPDATE_HISTORY_VISIT_COUNT] AS';
    EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LP_UPDATE_HISTORY_VISIT_COUNT] TO impusers';
END;
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[LP_UPDATE_HISTORY_VISIT_COUNT]
        @report_start_date DATETIME = NULL,
        @return_or_write CHAR(1) = 'R'              --W = Write to data mart table / R = Return data from procedure (to a report) / B = Both
AS BEGIN

    /*  Procedure Parameters and temporary tables  */

        DECLARE @double_count TABLE ([attendance_date] DATE NULL, 
                                     [attendance_month] VARCHAR(50) NOT NULL DEFAULT(''),
                                     [attendance_month_sort] VARCHAR(10) NOT NULL DEFAULT(''),
                                     [customer_no] INT NOT NULL DEFAULT(0),
                                     [member_scan] INT NOT NULL DEFAULT(0),
                                     [venue_tickets] INT NOT NULL DEFAULT(0),
                                     [double_counted] INT NOT NULL DEFAULT(0))
        
        IF OBJECT_ID('tempdb..#vc_final') is not null DROP TABLE [#vc_final]

        CREATE TABLE [#vc_final] ([attendance_type] VARCHAR(30) NOT NULL DEFAULT (''), 
                                  [order_no] INT NOT NULL DEFAULT (0), 
                                  [sale_total] INT NOT NULL DEFAULT (0), 
                                  [scan_admission_total] INT NOT NULL DEFAULT (0));

    /*  Check Parameters  */

        SELECT @report_start_date = CAST(ISNULL(@report_start_date, DATEADD(DAY,-1,GETDATE())) AS DATE)

    /*  Retrieve the Visit Count data for ticketed events from the database  */
        
        INSERT INTO [#vc_final] ([attendance_type], [order_no], [sale_total], [scan_admission_total])
        EXECUTE [dbo].[LP_DAILY_VISIT_COUNT] @report_start_date, @report_start_date, 'Y', 'N';    


    /*  Retrieve negative visit counts from Members being double counted in Exhibit Halls and Special Exhibitions
        Done as a separate step because it also uses a procedure and INSERT/EXECUTE statements cannot be nested.  */

        INSERT INTO @double_count ([attendance_date], [attendance_month], [attendance_month_sort], [customer_no], 
                                   [member_scan], [venue_tickets], [double_counted])
        EXECUTE [dbo].[LRP_MEMBER_DOUBLE_VISIT_COUNT] @report_start_date, @report_start_date, 37;  --37 = Title Number for Special Exhibitions

        IF EXISTS (SELECT * FROM @double_count)
            INSERT INTO [#vc_final] ([attendance_type],[order_no],[sale_total],[scan_admission_total])
            SELECT 'Special Exhibit Double Count',
                   SUM(double_counted),
                   (SUM(double_counted) * -1),
                   (SUM(double_counted) * -1)
            FROM @double_count;
    
    DONE:

       /*  Replace the data in the table  */

       IF @return_or_write IN ('W','B') BEGIN

            DELETE FROM [dbo].[LT_HISTORY_VISIT_COUNT]
            WHERE [history_dt] = @report_start_date

            INSERT INTO [dbo].[LT_HISTORY_VISIT_COUNT] ([run_dt], [history_dt], [history_date], [attendance_type], [transaction_count], [visit_count], [visit_count_scan])
            SELECT GETDATE(),
                   @report_start_date,
                   CONVERT(CHAR(10),@report_start_date,111),
                   [attendance_type], 
                   CASE WHEN SUM([sale_total]) = 0 THEN 0
                        ELSE COUNT([order_no]) END AS [transaction_count], 
                   SUM([sale_total]) AS [visit_count], 
                   SUM([scan_admission_total]) AS [visit_count_scan]
            FROM [#vc_final]
            GROUP BY [attendance_type] 
            HAVING SUM([sale_total]) <> 0 OR SUM([scan_admission_total]) <> 0 
            ORDER BY [attendance_type]

        END

        IF @return_or_write IN ('R','B')
            SELECT GETDATE(),
                   @report_start_date,
                   CONVERT(CHAR(10),@report_start_date,111),
                   [attendance_type], 
                   CASE WHEN SUM([sale_total]) = 0 THEN 0
                        ELSE COUNT([order_no]) END AS [transaction_count], 
                   SUM([sale_total]) AS [visit_count], 
                   SUM([scan_admission_total]) AS [visit_count_scan]
            FROM [#vc_final]
            GROUP BY [attendance_type]
            HAVING SUM([sale_total]) <> 0 OR SUM([scan_admission_total]) <> 0 
            ORDER BY [attendance_type]

    CLEAN_UP:

        /*  Not really necessary, but the programmer in me wants to clean up  */

        --IF OBJECT_ID('tempdb..#visit_count_raw_data') is not null DROP TABLE [#visit_count_raw_data]
        IF OBJECT_ID('tempdb..#vc_final') is not null DROP TABLE [#vc_final]

END
GO

--EXECUTE [dbo].[LP_UPDATE_HISTORY_VISIT_COUNT] @report_start_date = '10-1-2019', @return_or_write = 'R'
