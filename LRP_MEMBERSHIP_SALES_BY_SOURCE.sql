USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_MEMBERSHIP_SALES_BY_SOURCE]') AND type in (N'P', N'PC')) BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_MEMBERSHIP_SALES_BY_SOURCE] AS' 
END
GO

ALTER PROCEDURE [dbo].[LRP_MEMBERSHIP_SALES_BY_SOURCE]
        @report_start_dt DATETIME = NULL,
        @report_end_dt DATETIME = NULL 
AS BEGIN

    /*  Procedure Variables  */
        
        DECLARE @mem_products TABLE ([perf_no] INT, [perf_date] CHAR(10), [production_name] VARCHAR(30))
        DECLARE @link_table TABLE ([cust_memb_no] INT, [sli_no] INT)

    /*  Check Parameters
            -If Nulls passed, run for yesterday  */

        SELECT @report_start_dt = ISNULL(@report_start_dt,DATEADD(DAY,-1,GETDATE()))
        SELECT @report_end_dt = ISNULL(@report_end_dt,DATEADD(DAY,-1,GETDATE()))

        SELECT @report_start_dt = CONVERT(CHAR(10), @report_start_dt, 111) + ' 00:00:00',
               @report_end_dt = CONVERT(CHAR(10), @report_end_dt, 111) + ' 23:59:59.957'

    /*  Create the Temp Table to hold raw data from the T_SUB_LINEITEM TABLE  */

        IF OBJECT_ID('tempdb..#membership_sales_information') IS NOT NULL DROP TABLE [#membership_sales_information]

        CREATE TABLE [#membership_sales_information] ([cust_memb_no] INT NOT NULL, [sli_no] INT, [order_no] INT, [customer_no] INT, [recipient_no] INT, [mode_of_sale] INT, [perf_no] int, [perf_zone_no] INT, 
                                                      [perf_zone_text] VARCHAR(50), [perf_date] CHAR(10), [perf_time] VARCHAR(8), [perf_type] INT, [due_amt] decimal (18,2), [paid_amt] decimal(18,2), 
                                                      [current_status] INT, [current_status_name] VARCHAR(30), [sli_status] int, [price_type] INT, [source_no] INT, [source_name] VARCHAR(50), 
                                                      [created_by] varchar(50), [create_dt] DATETIME, [create_dt_sort] CHAR(7), [create_dt_display] VARCHAR(50), create_loc VARCHAR(50), 
                                                      [NRR_status] VARCHAR(15), [rpt_message] VARCHAR(100))
                                                
        --ALTER TABLE [#membership_sales_information] ADD CONSTRAINT [PK_SCH_ATT_SUB] PRIMARY KEY NONCLUSTERED ([cust_memb_no] ASC) ON [PRIMARY]
        CREATE NONCLUSTERED INDEX [mem_sales_info_cust_memb_no] ON [#membership_sales_information] ([cust_memb_no] ASC) ON [PRIMARY]

        CREATE CLUSTERED INDEX [mem_sales_info_customer_no] ON [#membership_sales_information] ([customer_no] ASC) ON [PRIMARY]
        CREATE NONCLUSTERED INDEX [mem_sales_info_order_no] ON [#membership_sales_information] ([order_no] ASC) ON [PRIMARY]

   
    /*  Creating a link tavle variable rather than just joining the table because I have found records in the table that don't correspond to any record in T_SUBLINEITEM 
        Rather than dealing with that in the query, it's easier to just take a temporary copy of the table and delete those records ahead of time so they are not a factor.  */
        INSERT INTO @link_table 
        SELECT [cust_memb_no], [sli_no] FROM [dbo].[LX_SLI_MEMB] (NOLOCK) 
        WHERE [cust_memb_no] IN (SELECT [cust_memb_no] FROM [dbo].[TX_CUST_MEMBERSHIP] (NOLOCK) WHERE [create_dt] BETWEEN @report_start_dt AND @report_end_dt)
        
        DELETE FROM @link_table WHERE [sli_no] NOT IN (SELECT sli_no FROM [dbo].[T_SUB_LINEITEM] (NOLOCK))

           
    /*  Get all membership sales information for the date range passed to the procedure  
            --uses create_dt in the T_SUB_LINEITEM table
            --Excludes List Card Replacement, Temporary Membership Card and all Upgrades  */


        INSERT INTO [#membership_sales_information] ([cust_memb_no], [sli_no], [order_no], [customer_no], [recipient_no], [mode_of_sale], [perf_no], [perf_zone_no], [perf_zone_text], 
                                                     [perf_date], [perf_time], [perf_type], [due_amt], [paid_amt], [current_status], [current_status_name], [sli_status], [price_type], 
                                                     [source_no], [source_name], [created_by], [create_dt], [create_dt_sort], [create_dt_display], [create_loc], [NRR_status], [rpt_message])
        SELECT mem.[cust_memb_no], ISNULL(sli.[sli_no],0), ISNULL(sli.[order_no],0), mem.[customer_no], ISNULL(sli.[recipient_no],0), ISNULL(ord.[MOS],0), ISNULL(sli.[perf_no],0), ISNULL(sli.[zone_no],0), 
               REPLACE(lev.[description],' Membership',''), ISNULL(prf.[performance_date],''), ISNULL(prf.[performance_time],''), ISNULL(prf.[performance_type],''), 
               ISNULL(sli.[due_amt],mem.[memb_amt]), ISNULL(sli.[paid_amt],mem.[memb_amt]), mem.[current_status], sta.[description], ISNULL(sli.[sli_status],0), ISNULL(sli.[price_type],0), 
               ISNULL(ord.[source_no],0), ISNULL(src.[source_name],'Unknown Source'), mem.[created_by], mem.[create_dt], '', '', mem.[create_loc], ISNULL(mem.[NRR_status],'UN'), ''
        FROM [dbo].[TX_CUST_MEMBERSHIP] AS mem (NOLOCK)
             LEFT OUTER JOIN @link_table AS lxm ON lxm.[cust_memb_no] = mem.[cust_memb_no]
             LEFT OUTER JOIN [dbo].[T_SUB_LINEITEM] AS sli (NOLOCK) ON sli.[sli_no] = lxm.[sli_no]
             LEFT OUTER JOIN [dbo].[T_ORDER] AS ord (NOLOCK) ON ord.[order_no] = sli.[order_no]
             LEFT OUTER JOIN [dbo].[TX_APPEAL_MEDIA_TYPE] AS src (NOLOCK) ON src.[source_no] = ord.[source_no]
             LEFT OUTER JOIN [dbo].[T_MEMB_LEVEL] AS lev (NOLOCK) ON lev.[memb_level] = mem.[memb_level]
             LEFT OUTER JOIN [dbo].[TR_CURRENT_STATUS] AS sta (NOLOCK) ON sta.[id] = mem.[current_status]
             LEFT OUTER JOIN [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] AS prf (NOLOCK) ON prf.[performance_no] = sli.[perf_no] AND prf.[performance_zone] = sli.[zone_no]
        WHERE convert(DATE,mem.[create_dt]) BETWEEN @report_start_dt AND @report_end_dt
          AND mem.[memb_org_no] = 4 --(4=Household)
          AND ISNULL(prf.[performance_zone_text],'') NOT IN ('Lost Card Replacement','Temporary Membership Card')
          AND ISNULL(prf.[performance_zone_text],'') NOT LIKE '%Upgrade%'
          AND src.[source_name] NOT IN ('conversion renewals','Staff membership')
          AND sli.[sli_status] IN (3,12)

    /*  If no records found, jump to the end  */

        IF NOT EXISTS (SELECT * FROM [#membership_sales_information]) GOTO FINISHED

    /*  If recipient number is null (not a gift membership) make the recipient number the customer number  */

        UPDATE [#membership_sales_information] SET [recipient_no] = [customer_no] WHERE [recipient_no] IS NULL 
       
    /* Set sort and display dates  */

        --UPDATE [#membership_sales_information]
        --SET [create_dt_sort] = LEFT([perf_date],7),
        --    [create_dt_display] = DATENAME(MONTH,CONVERT(DATE,[perf_date])) + ' ' + DATENAME(YEAR,CONVERT(DATE,[perf_date]))

        UPDATE [#membership_sales_information]
        SET [create_dt_sort] = LEFT(CONVERT(CHAR(10),[create_dt],111),7),
            [create_dt_display] = DATENAME(MONTH,[create_dt]) + ' ' + DATENAME(YEAR,[create_dt])

    /*  Set Full Text of NRR Status  */

        UPDATE [#membership_sales_information] 
        SET [NRR_status] = CASE WHEN [NRR_status] = 'NE' THEN 'New'
                                WHEN [NRR_status] = 'RN' THEN 'Renew'
                                WHEN [NRR_status] = 'RI' THEN 'Reinstate'
                                ELSE 'Unknown' END

    FINISHED:

        /*  If no records found, insert single record with a message  */

            IF NOT EXISTS (SELECT * FROM [#membership_sales_information])
                INSERT INTO [#membership_sales_information] ([cust_memb_no], [sli_no], [order_no], [customer_no], [recipient_no], [mode_of_sale], [perf_no], [perf_zone_no], [perf_zone_text], 
                                                             [perf_date], [perf_time], [perf_type], [due_amt], [paid_amt], [current_status], [current_status_name], [sli_status], [price_type], 
                                                             [source_no], [source_name], [created_by], [create_dt], [create_dt_sort], [create_dt_display], [create_loc], [NRR_status], [rpt_message])
                VALUES (0, 0, 0, 0, 0, 0, 0, 0, '', '', '', 0, 0.0, 0.0, 0, '', 0, 0, 0, '', '', GETDATE(), '', '', '', '', 'No records found for the criteria entered.')                    

        /*  Select final data set to be passed to the report  */

            SELECT [sli_no], [order_no], [customer_no], [recipient_no], [mode_of_sale], [perf_no], [perf_zone_no], [perf_zone_text], [perf_date], [perf_time], [perf_type], [due_amt], [paid_amt], 
                   [sli_status], [price_type], [source_no], [source_name], [created_by], [create_dt], [create_dt_sort], [create_dt_display], [create_loc], [cust_memb_no], [NRR_status], [rpt_message]
            FROM [#membership_sales_information]
            --ORDER BY [create_dt_sort], [source_name], [NRR_status]
                        
    DONE:

END
GO

GRANT EXECUTE ON [dbo].[LRP_MEMBERSHIP_SALES_BY_SOURCE] TO ImpUsers
GO

--EXECUTE [dbo].[LRP_MEMBERSHIP_SALES_BY_SOURCE] '7-1-2017', '8-31-2017 23:59:59'
EXECUTE [dbo].[LRP_MEMBERSHIP_SALES_BY_SOURCE] '11-1-2017', '11-30-2017 23:59:59'
--SELECT * FROM dbo.TX_CUST_MEMBERSHIP WHERE cust_memb_no = 1146855
--SELECT * FROM dbo.LX_SLI_MEMB WHERE cust_memb_no = 1146855
--SELECT * FROM dbo.T_SUB_LINEITEM WHERE sli_no = 3862052
--SELECT * FROM dbo.T_ORDER WHERE order_no = 964374
--SELECT * FROM dbo.TX_APPEAL_MEDIA_TYPE WHERE source_no = 9317

--SELECT * FROM dbo.TX_CUST_MEMBERSHIP WHERE cust_memb_no = 1148471
--SELECT * FROM dbo.LX_SLI_MEMB WHERE cust_memb_no = 1148471
--SELECT * FROM T_SUB_LINEITEM WHERE sli_no =  3932169
--SELECT * FROM dbo.T_ORDER WHERE order_no = 982735
--SELECT * FROM dbo.TX_APPEAL_MEDIA_TYPE WHERE source_no = 9667

    --SELECT * FROM [#membership_sublineitems] WHERE cust_memb_no = 0
    --    SELECT * FROM [#membership_sublineitems] WHERE cust_memb_no <> 0


    --SELECT production_name, performance_zone_text FROM dbo.LV_PRODUCTION_ELEMENTS_PERFORMANCE WHERE performance_no = 1142 AND performance_zone = 10
    --SELECT order_no, sli_no, customer_no, recipient_no [perf_no], perf_zone_no, perf_zone_text, perf_date, source_name, created_by, create_dt, cust_memb_no, NRR_status FROM [#membership_sublineitems] WHERE cust_memb_no = 0


--SELECT DATEDIFF(DAY,'7-15-2017','7-16-2017')
   

    --SELECT sli.[sli_no], sli.[order_no], sli.[customer_no], sli.[mode_of_sale], sli.[perf_no], sli.[perf_zone_no], sli.[perf_zone_text], sli.[perf_date], sli.[perf_time], sli.[perf_type],
    --        sli.[due_amt], sli.[paid_amt], sli.[sli_status], sli.[price_type], sli.[source_no], sli.[source_name], sli.[created_by], sli.[create_dt], sli.[create_loc],
    --        sli.[cust_memb_no], sli.[NRR_status], mem.cust_memb_no, mem.NRR_status, DATEDIFF(MINUTE,mem.create_dt,sli.create_dt)
    --FROM [#membership_sublineitems] AS sli
    --      LEFT OUTER JOIN [dbo].[TX_CUST_MEMBERSHIP] AS mem (NOLOCK) ON mem.customer_no = sli.[customer_no]
    --                                                                AND CONVERT(DATE,mem.[create_dt]) = CONVERT(DATE,sli.[create_dt])
    --                                                                --AND DATEDIFF(MINUTE,mem.create_dt,sli.create_dt) BETWEEN -2 AND 2
    --WHERE ISNULL(mem.cust_memb_no,0) = 0
    
  
    --SELECT * FROM T_CUSTOMER WHERE customer_no = 17595
    --SELECT * FROM [#membership_sublineitems] WHERE sli_no = 2905048 
    --SELECT * FROM dbo.TX_CUST_MEMBERSHIP WHERE customer_no = 3499189
    ----SELECT * FROM [dbo].[LT_MOS_MEMBER_CARDS] WHERE sli_no = 2905048
    ----SELECT * FROM dbo.  WHERE sli_no = 2905048

--SELECT DISTINCT performance_zone, [performance_zone_text] FROM dbo.LV_PRODUCTION_ELEMENTS_PERFORMANCE WHERE title_name = 'Membership' AND [production_name] = 'Household' ORDER BY performance_zone_text
--SELECT * FROM dbo.LV_PRODUCTION_ELEMENTS_PERFORMANCE WHERE performance_no IN (6160,6161)
--SELECT * FROM dbo.T_SUB_LINEITEM WHERE sli_no = 1533957
--SELECT * FROM [dbo].[TR_ORIGINAL_SOURCE]
--SELECT * FROM dbo.TX_APPEAL_MEDIA_TYPE
--SELECT * FROM dbo.TX_CUST_MEMBERSHIP
--SELECT * FROM dbo.T_SLI_DETAIL
--SELECT * FROM dbo.LT_MOS_ACK_MEMBERSHIPS_AS_PRODUCTS WHERE sli_no = 1533957
--SELECT order_no, sli_no, COUNT(*) FROM dbo.LT_MOS_ACK_MEMBERSHIPS_AS_PRODUCTS GROUP BY order_no, sli_no HAVING COUNT(*) > 1


----SELECT cust_memb_no FROM [#membership_sales_information] GROUP BY cust_memb_no HAVING COUNT(*) > 1
   
--SELECT cust_memb_no FROM dbo.TX_CUST_MEMBERSHIP WHERE CONVERT(DATE,create_dt) BETWEEN '11-1-2017' AND '11-30-2017' AND memb_org_no = 4 AND NRR_status = 'ne'
--        AND cust_memb_no NOT IN (SELECT cust_memb_no FROM [#membership_sales_information])
