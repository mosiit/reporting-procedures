USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LRP_CORPORATE_MEMBERSHIP_EXECUTIVE_PASSHOLDERS]    Script Date: 8/15/2018 11:09:34 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[LRP_CORPORATE_MEMBERSHIP_EXECUTIVE_PASSHOLDERS]
(
	@group_customer_no INT
)    
AS

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
SET NOCOUNT ON 


SELECT aff.individual_customer_no, cust.display_name, emp.*
FROM dbo.T_AFFILIATION aff
	INNER JOIN dbo.FT_CONSTITUENT_DISPLAY_NAME() AS cust 
		ON aff.individual_customer_no = cust.customer_no
	-- 20180815, H. Sheridan - WO 94151: 
	--		The sub report 'Sub_Corporate_Contacts' in the 'MOS ADV: Corporate Membership Renewal Letter' report failed when calling this function because
	--		the second argument for @Handbook was not included below.  I added an empty string to the call below for that value.
	OUTER APPLY [dbo].[LFT_BOARD_GetEmployerInformation](aff.individual_customer_no, '') emp
WHERE aff.group_customer_no = @group_customer_no
	AND aff.affiliation_type_id = 10018 -- Executive Passholder
ORDER BY cust.sort_name



GO


