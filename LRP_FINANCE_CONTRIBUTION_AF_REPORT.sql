USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_FINANCE_CONTRIBUTION_AF_REPORT]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LRP_FINANCE_CONTRIBUTION_AF_REPORT]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LRP_FINANCE_CONTRIBUTION_AF_REPORT]
        @report_start_dt datetime = Null,
        @report_end_dt datetime = Null
AS BEGIN

    DECLARE @report_date datetime       SELECT @report_date = getdate()

    /*  Check Parameters - Include Government defaults to yes if nothing is passed.  
                           Start date defaults to yesterday if nothing is passed - end date defaults to same as start date if nothing is passed  */

        SELECT @report_start_dt = IsNull(@report_start_dt, dateadd(day,-1,getdate()))
        SELECT @report_end_dt = IsNull(@report_end_dt, @report_start_dt)

     
    /*  Make sure start date starts at 00:00:00 and end date ends at 23:59:59.997  */

        SELECT @report_start_dt = convert(char(10),@report_start_dt,111) + ' 00:00:00',
               @report_end_dt = convert(char(10),@report_end_dt,111) + ' 23:59:59.997'

    /*  Create temporary tables to hold the data for this report  */

        IF OBJECT_ID('tempdb..#contribution_table') IS NOT NULL DROP TABLE [#contribution_table]
   
        CREATE TABLE #contribution_table ([report_date] datetime, [report_message] varchar(100), [record_type] varchar(30), [ref_no] int, [customer_no] int, [customer_type_no] int, 
                                          [customer_type_name] varchar(30), [customer_first_name] varchar(20), [customer_middle_name] varchar(20), [customer_last_name] varchar(55),
                                          [customer_sort_name] varchar(255), [contribution_type] varchar(50), [contribution_dt] datetime, [contribution_date] char(10),  [contribution_time] char(8), 
                                          [contribution_amount] decimal(18,2), [fund_description] varchar(30), [nonrestricted_income_gl_no] varchar(30), [source] varchar(4), [creditee_no] int, 
                                          [creditee_type_no] int, [creditee_type_name] varchar(30), [creditee_first_name] varchar(20), [creditee_middle_name] varchar(20), [creditee_last_name] varchar(55), 
                                          [creditee_sort_name] varchar(100), [amount_sort] varchar(25), [post_no] INT, [payment_method] INT, [payment_method_name] VARCHAR(30))

        CREATE CLUSTERED INDEX [ix_contribution_table_customer_type_no] ON #contribution_table ([customer_type_no] ASC) ON [PRIMARY]

        CREATE NONCLUSTERED INDEX [ix_contribution_table_contribution_amount] ON #contribution_table ([contribution_amount] ASC) ON [PRIMARY]

        CREATE NONCLUSTERED INDEX [ix_contribution_table_customer_sort_name] ON #contribution_table ([customer_sort_name] ASC) ON [PRIMARY]

        IF OBJECT_ID('tempdb..#payment_info') IS NOT NULL DROP TABLE [#payment_info]

        CREATE TABLE [#payment_info] ([sequence_no] INT, [transaction_no] INT, [ref_no] INT, [trn_type] INT, [trn_type_name] VARCHAR(30), [transaction_dt] DATETIME, [transaction_amount] MONEY, 
                                      [batch_no] INT, [payment_no] INT, [payment_dt] DATETIME, [payment_amount] MONEY, [pmt_method] INT, [pmt_method_name] VARCHAR(30), [post_no] INT)
            
        CREATE CLUSTERED INDEX [ix_payment_info_ref_no] ON #payment_info ([ref_no] ASC) ON [PRIMARY]


    /*  The 1st and 2nd Insert Statments pulls standard contributions from the LV_RPT_FIN_CONTRIBUTION views (1 and 2) if not pulling only governnment  */
    
        INSERT INTO #contribution_table
        SELECT @report_date, '', 'contribution 1', [ref_no], [customer_no], [customer_type_no], [customer_type_name], [customer_first_name], [customer_middle_name], [customer_last_name], 
               [customer_sort_name], [contribution_type], [contribution_dt], [contribution_date], [contribution_time], [contribution_amount], [fund_description], [nonrestricted_income_gl_no], 
               [source], [creditee_no], [creditee_type_no], [creditee_type_name], [creditee_first_name], [creditee_middle_name], [creditee_last_name], [creditee_sort_name], '', 0, 0, ''
        FROM [dbo].[LV_RPT_FIN_CONTRIBUTION_1] 
        WHERE [contribution_dt] between @report_start_dt and @report_end_dt AND [fund_description] LIKE '03%'

        INSERT INTO #contribution_table
        SELECT @report_date, '', 'contribution 2', [ref_no], [customer_no], [customer_type_no], [customer_type_name], [customer_first_name], [customer_middle_name], [customer_last_name], 
            [customer_sort_name], [contribution_type], [contribution_dt], [contribution_date], [contribution_time], [contribution_amount], [fund_description], [nonrestricted_income_gl_no], 
            [source], [creditee_no], [creditee_type_no], [creditee_type_name], [creditee_first_name], [creditee_middle_name], [creditee_last_name], [creditee_sort_name], '', 0, 0, ''
        FROM [dbo].[LV_RPT_FIN_CONTRIBUTION_2] 
        WHERE [contribution_dt] between @report_start_dt and @report_end_dt AND [fund_description] LIKE '03%'
        

    /*  The 3rd and 4th Insert Statments pull eligible pledges from the LV_RPT_FIN_CONTRIBUTION_PLEDGE views (1 and 2) if not pulling only governnment  */

        INSERT INTO #contribution_table
        SELECT @report_date, '', 'pledge 1', [ref_no], [customer_no], [customer_type_no], [customer_type_name], [customer_first_name], [customer_middle_name], [customer_last_name], 
               [customer_sort_name], [contribution_type], [contribution_dt], [contribution_date], [contribution_time], [contribution_amount], [fund_description], [nonrestricted_income_gl_no], 
               [source], [creditee_no], [creditee_type_no], [creditee_type_name], [creditee_first_name], [creditee_middle_name], [creditee_last_name], [creditee_sort_name], '', 0, 0, ''
        FROM [dbo].[LV_RPT_FIN_CONTRIBUTION_PLEDGE_1] 
        WHERE [contribution_dt] between @report_start_dt and @report_end_dt AND [fund_description] LIKE '03%'

        INSERT INTO #contribution_table
        SELECT @report_date, '', 'pledge 2', [ref_no], [customer_no], [customer_type_no], [customer_type_name], [customer_first_name], [customer_middle_name], [customer_last_name], 
               [customer_sort_name], [contribution_type], [contribution_dt], [contribution_date], [contribution_time], [contribution_amount], [fund_description], [nonrestricted_income_gl_no], 
               [source], [creditee_no], [creditee_type_no], [creditee_type_name], [creditee_first_name], [creditee_middle_name], [creditee_last_name], [creditee_sort_name], '', 0, 0, ''
        FROM [dbo].[LV_RPT_FIN_CONTRIBUTION_PLEDGE_2] 
        WHERE [contribution_dt] between @report_start_dt and @report_end_dt AND [fund_description] LIKE '03%'


    /*  The 5th, 6th, 7th, and 8th Insert Statmenta pull eligible pledge payments from the LV_RPT_FIN_CONTRIBUTION_PAYMENT views (1, 2, 3, and 4) if not pulling only governnment */

        INSERT INTO #contribution_table
        SELECT @report_date, '', 'payment 1', [ref_no], [customer_no], [customer_type_no], [customer_type_name], [customer_first_name], [customer_middle_name], [customer_last_name], 
               [customer_sort_name], [contribution_type], [contribution_dt], [contribution_date], [contribution_time], [contribution_amount], [fund_description], [nonrestricted_income_gl_no], 
               [source], [creditee_no], [creditee_type_no], [creditee_type_name], [creditee_first_name], [creditee_middle_name], [creditee_last_name], [creditee_sort_name], '', 0, 0, ''
        FROM [dbo].[LV_RPT_FIN_CONTRIBUTION_PAYMENT_1] 
        WHERE [contribution_dt] between @report_start_dt and @report_end_dt AND [fund_description] LIKE '03%'
    
        INSERT INTO #contribution_table
        SELECT @report_date, '', 'payment 2', [ref_no], [customer_no], [customer_type_no], [customer_type_name], [customer_first_name], [customer_middle_name], [customer_last_name], 
               [customer_sort_name], [contribution_type], [contribution_dt], [contribution_date], [contribution_time], [contribution_amount], [fund_description], [nonrestricted_income_gl_no], 
               [source], [creditee_no], [creditee_type_no], [creditee_type_name], [creditee_first_name], [creditee_middle_name], [creditee_last_name], [creditee_sort_name], '', 0, 0, ''
        FROM [dbo].[LV_RPT_FIN_CONTRIBUTION_PAYMENT_2] 
        WHERE [contribution_dt] between @report_start_dt and @report_end_dt AND [fund_description] LIKE '03%'

        INSERT INTO #contribution_table
        SELECT @report_date, '', 'payment 4', [ref_no], [customer_no], [customer_type_no], [customer_type_name], [customer_first_name], [customer_middle_name], [customer_last_name], 
               [customer_sort_name], [contribution_type], [contribution_dt], [contribution_date], [contribution_time], [contribution_amount], [fund_description], [nonrestricted_income_gl_no], 
               [source], [creditee_no], [creditee_type_no], [creditee_type_name], [creditee_first_name], [creditee_middle_name], [creditee_last_name], [creditee_sort_name], '', 0, 0, ''
        FROM [dbo].[LV_RPT_FIN_CONTRIBUTION_PAYMENT_3] 
        WHERE [contribution_dt] between @report_start_dt and @report_end_dt AND [fund_description] LIKE '03%'
    
        INSERT INTO #contribution_table
        SELECT @report_date, '', 'payment 4', [ref_no], [customer_no], [customer_type_no], [customer_type_name], [customer_first_name], [customer_middle_name], [customer_last_name], 
               [customer_sort_name], [contribution_type], [contribution_dt], [contribution_date], [contribution_time], [contribution_amount], [fund_description], [nonrestricted_income_gl_no], 
               [source], [creditee_no], [creditee_type_no], [creditee_type_name], [creditee_first_name], [creditee_middle_name], [creditee_last_name], [creditee_sort_name], '', 0, 0, ''
        FROM [dbo].[LV_RPT_FIN_CONTRIBUTION_PAYMENT_4] 
        WHERE [contribution_dt] between @report_start_dt and @report_end_dt AND [fund_description] LIKE '03%'


    /*  The 9th and 10th Insert Statments pull eligible government transactions from the LV_RPT_FIN_CONTRIBUTION_GOVERNMENT views (1 and 2) if pulling governnment */


        INSERT INTO #contribution_table
        SELECT @report_date, '', 'government 1', [ref_no], [customer_no], [customer_type_no], [customer_type_name], [customer_first_name], [customer_middle_name], [customer_last_name], 
               [customer_sort_name], [contribution_type], [contribution_dt], [contribution_date], [contribution_time], [contribution_amount], [fund_description], [nonrestricted_income_gl_no], 
               [source], [creditee_no], [creditee_type_no], [creditee_type_name], [creditee_first_name], [creditee_middle_name], [creditee_last_name], [creditee_sort_name], '', 0, 0, ''
        FROM [dbo].[LV_RPT_FIN_CONTRIBUTION_GOVERNMENT_1] 
        WHERE [contribution_dt] between @report_start_dt and @report_end_dt AND [fund_description] LIKE '03%'

        INSERT INTO #contribution_table
        SELECT @report_date, '', 'government 2', [ref_no], [customer_no], [customer_type_no], [customer_type_name], [customer_first_name], [customer_middle_name], [customer_last_name], 
               [customer_sort_name], [contribution_type], [contribution_dt], [contribution_date], [contribution_time], [contribution_amount], [fund_description], [nonrestricted_income_gl_no], 
               [source], [creditee_no], [creditee_type_no], [creditee_type_name], [creditee_first_name], [creditee_middle_name], [creditee_last_name], [creditee_sort_name], '', 0, 0, ''
        FROM [dbo].[LV_RPT_FIN_CONTRIBUTION_GOVERNMENT_2] 
        WHERE [contribution_dt] between @report_start_dt and @report_end_dt AND [fund_description] LIKE '03%'


    /*  Delete zero contributions  */

        DELETE FROM [#contribution_table] WHERE [contribution_amount] = 0.00
    
    /*  Clean up customer information on the records where the customer is the annonymous kiosk user  */
            
        UPDATE [#contribution_table]
        SET [customer_first_name] = '', [customer_middle_name] = '', 
            [customer_last_name] = 'Annonymous Kiosk User', [customer_sort_name] = 'Annonymous Kiosk User'
        WHERE [customer_sort_name] = 'Do Not Remove/Kiosk Anon User'

    /*  Clean up customer information on the records where the customer is the annonymous web site user  */

        UPDATE [#contribution_table]
        SET [customer_first_name] = '', [customer_middle_name] = '', 
            [customer_last_name] = 'Annonymous Web Site User', [customer_sort_name] = 'Annonymous Web Site User'
        WHERE [customer_sort_name] = 'User/Guest'

    /*  Create an amount text field using leading zerows to make sure it is the same length across the board (20 characters)  */

        UPDATE [#contribution_table] SET [amount_sort] = convert(varchar(25),[contribution_Amount])
        WHILE exists (SELECT * FROM [#contribution_table] WHERE len([amount_sort]) < 20) BEGIN
            UPDATE [#contribution_table] SET [amount_sort] = '0' + [amount_sort] WHERE len([amount_sort]) < 20 and [contribution_amount] >= 0
            UPDATE [#contribution_table] SET [amount_sort] = '_0' + right([amount_sort],len([amount_sort]) - 1) WHERE len([amount_sort]) < 20 and [contribution_amount] < 0
        END

    /* Create the sort field based on what was passed to that parameter  */

        UPDATE [#contribution_table] SET [customer_sort_name] = [contribution_date] + ' ' + [customer_sort_name] + ' ' + [amount_sort]

        UPDATE [#contribution_table] SET [contribution_type] = 'P' WHERE [contribution_type] = 'Pledge Payment Received'
        UPDATE [#contribution_table] SET [contribution_type] = 'A' WHERE [contribution_type] = 'Adjustment to Payment'


    /*  Get All payment info */
    
        INSERT INTO #payment_info
        SELECT trx.[sequence_no], trx.[transaction_no], trx.[ref_no], trx.[trn_type], typ.[description], trx.[trn_dt], trx.[trn_amt], trx.[batch_no],
               ISNULL(MAX(pay.[payment_no]),0), pay.pmt_dt, ISNULL(SUM(pay.[pmt_amt]),0.00), ISNULL(pay.[pmt_method],0), mth.[description], ISNULL(bat.[post_no],0)
        FROM [dbo].[T_TRANSACTION] AS trx (NOLOCK)
             INNER JOIN [dbo].[T_PAYMENT] AS pay (NOLOCK) ON pay.[transaction_no] = trx.[transaction_no] AND pay.[sequence_no] = trx.[sequence_no]
            INNER JOIN [dbo].[TR_TRANSACTION_TYPE] AS typ (NOLOCK) ON typ.[id] = trx.[trn_type]
            INNER JOIN [dbo].[TR_PAYMENT_METHOD] AS mth (NOLOCK) ON mth.[id] = pay.[pmt_method]
            INNER JOIN [dbo].[T_BATCH] AS bat (NOLOCK) ON bat.[batch_no] = pay.[batch_no]
        WHERE trx.[ref_no] IN (SELECT [ref_no] FROM [#contribution_table])
        GROUP BY trx.[sequence_no], trx.[transaction_no], trx.[ref_no], trx.[trn_type], typ.[description], trx.[trn_dt], trx.[trn_amt], trx.[batch_no],
               pay.pmt_dt, pay.[pmt_method], mth.[description], bat.[post_no]

    /*  Update the contribution table with the payment information  */

        UPDATE #contribution_table SET [post_no] = (SELECT ISNULL(MAX([post_no]),0) FROM #payment_info AS pay WHERE pay.[ref_no] = [#contribution_table].[ref_no])

        UPDATE #contribution_table SET [payment_method] = (SELECT MAX([pmt_method]) FROM #payment_info AS pay WHERE pay.[ref_no] = [#contribution_table].[ref_no])

        UPDATE #contribution_table SET [payment_method_name] = (SELECT [description] FROM [dbo].[TR_PAYMENT_METHOD] WHERE [id] = [#contribution_table].[payment_method])

    DONE:

        /*  Insert a single record with a no records found message if nothing was found  */

            IF not exists (SELECT * FROM [#contribution_table])
                INSERT INTO #contribution_table VALUES  (@report_date,'No records found for the criteria entered.','',0,0,0,'','','','','','',Null,'','',0.00,'','','',0,0,'','','','','','', 0, 0, '')

        /*  SELECT final data set from #contribution_table with parameter values on the end  */

            SELECT [report_date], [report_message], [record_type], [ref_no], [customer_no], [customer_type_no], [customer_type_name], [customer_first_name], [customer_middle_name], 
                   [customer_last_name], [customer_sort_name], [contribution_type], [contribution_dt], [contribution_date], [contribution_time], [contribution_amount], [fund_description],
                   [nonrestricted_income_gl_no], [source], [creditee_no], [creditee_type_no], [creditee_type_name], [creditee_first_name], [creditee_middle_name], [creditee_last_name], 
                   [creditee_sort_name], [post_no], [payment_method_name]
            FROM #contribution_table
            ORDER BY [customer_sort_name]
                                       
        /*  Destroy the temporary table  */

            IF OBJECT_ID('tempdb..#contribution_table') IS NOT NULL DROP TABLE [#contribution_table]

            IF OBJECT_ID('tempdb..#payment_info') IS NOT NULL DROP TABLE [#payment_info]

END
GO

GRANT EXECUTE ON [dbo].[LRP_FINANCE_CONTRIBUTION_AF_REPORT] TO impusers
GO

--EXECUTE [dbo].[LRP_FINANCE_CONTRIBUTION_AF_REPORT] '12-18-2016', '12-31-2016'

