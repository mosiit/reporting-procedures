USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_TESSITURA_CASHOUT_ONE_STEP]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LRP_TESSITURA_CASHOUT_ONE_STEP]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LRP_TESSITURA_CASHOUT_ONE_STEP]
        @report_dt DATETIME = null,
        @report_operator VARCHAR(10) = '',
        @machine_locations VARCHAR(4000) = ''
AS BEGIN

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

    /*  Procedure Variables  */

        DECLARE @one_step_table table ([one_step_count] INT, [membership_create_location] varchar(30))
        DECLARE @location_list TABLE ([location_no_str] VARCHAR(20), [location_no] INT)
        DECLARE @machine_list TABLE ([machine_name] VARCHAR(50))

    /*  Check Parameters  - Null Date = Today, Null Operator = All  */

        SELECT @report_dt = ISNULL(@report_dt, GETDATE())
        SELECT @report_operator = ISNULL(@report_operator,'')
    
       SELECT @machine_locations = ISNULL(@machine_locations,'')
    
    /*  Parse machine_locations parameter  */

        INSERT INTO @location_list ([location_no_str]) 
        SELECT [item] FROM [dbo].[LFT_SPLIT_DILEMETED_STRING] (@machine_locations, ',')

        DELETE FROM @location_list WHERE [dbo].[FS_isReallyNumeric] ([location_no_str]) = 0

        UPDATE @location_list SET [location_no] = CONVERT(INT,[location_no_str])

        INSERT INTO @machine_list ([machine_name])
        SELECT [machine_name] FROM [dbo].[TX_MACHINE_LOCATION] WHERE [location] IN (SELECT [location_no] FROM @location_list)

    /*  GET All membership Records for this operator on this day  */
        
        INSERT INTO @one_step_table
        SELECT [one_step_count], [membership_create_location]
        FROM [dbo].[LV_RPT_CASHOUT_MEMBERSHIPS]
        WHERE [membership_date] = convert(char(10),@report_dt,111) AND [membership_operator] = @report_operator
              AND Membership_type NOT LIKE '%Upgrade%'
    
    /*  Delete all Records where one step was not added to the membership sales  */

        DELETE FROM @one_step_table WHERE [one_step_count] = 0
    
    /*  IF specific machine locations passed to procedure, delete all records from other machines  */

        DELETE FROM @one_step_table WHERE [membership_create_location] NOT IN (SELECT [machine_name] FROM @machine_list)

    FINISHED:  

        SELECT [one_step_count] FROM @one_step_table

END
GO

GRANT EXECUTE ON [dbo].[LRP_TESSITURA_CASHOUT_ONE_STEP] TO ImpUsers
GO

 EXECUTE [dbo].[LRP_TESSITURA_CASHOUT_ONE_STEP] @report_dt = '9-30-2018', @report_operator = 'ahile00', @machine_locations = '15,16'



         