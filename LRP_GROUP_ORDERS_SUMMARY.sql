USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_GROUP_ORDERS_SUMMARY]') AND type in (N'P', N'PC')) BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_GROUP_ORDERS_SUMMARY] AS' 
    EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LRP_GROUP_ORDERS_SUMMARY] TO impusers' 
END
GO

ALTER PROCEDURE [dbo].[LRP_GROUP_ORDERS_SUMMARY]
        @report_start_dt DATETIME = NULL,
        @report_end_dt DATETIME = NULL,
        @date_type VARCHAR(25) = 'Visit Date',
        @mos_str VARCHAR(4000) = '',
        @order_categories VARCHAR(4000) = '',
        @exclude_unpaid_past_orders CHAR(1) = 'Y'
WITH RECOMPILE AS BEGIN

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    SET NOCOUNT ON;


     /*  Report Variables and tables  */

        DECLARE @fiscal_year INT = [dbo].[LF_GetFiscalYear](GETDATE());

        DECLARE @order_category_list TABLE  ([order_category] VARCHAR(50));

        IF OBJECT_ID('tempdb..#order_summary') IS NOT NULL DROP TABLE [#order_summary];
            
        CREATE TABLE [#order_summary] ([customer_no] INT NOT NULL DEFAULT (0), 
                                       [customer_name] VARCHAR(100) NOT NULL DEFAULT (''),
                                       [customer_id] VARCHAR(150) NOT NULL DEFAULT (''),
                                       [city_state] VARCHAR(100) NOT NULL DEFAULT (''),
                                       [postal_code] VARCHAR(25) NOT NULL DEFAULT (''),
                                       [order_category_name] VARCHAR(30) NOT NULL DEFAULT (''),
                                       [mode_of_sale_name] VARCHAR(30) NOT NULL DEFAULT (''),
                                       [order_no] INT NOT NULL DEFAULT (0),
                                       [order_dt] DATETIME NULL,
                                       [perf_dt] DATETIME NULL,
                                       [advance_days] INT NOT NULL DEFAULT (0),
                                       [exhibits] INT NOT NULL DEFAULT (0),
                                       [omni] INT NOT NULL DEFAULT (0),
                                       [planetarium] INT NOT NULL DEFAULT (0),
                                       [Theater4d] INT NOT NULL DEFAULT (0),
                                       [butterfly] INT NOT NULL DEFAULT (0),
                                       [special] INT NOT NULL DEFAULT (0),
                                       [amount_due] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                       [amount_paid] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                       [balance_due] DECIMAL(18,2) NOT NULL DEFAULT (0.0));


    /*  Check Parameters - If null is passed to either date, use start and/or end of current fiscal year  */

        IF @report_start_dt IS NULL or @report_end_dt IS NULL BEGIN

            SELECT @report_start_dt = ISNULL(@report_start_dt,[dbo].[LF_GetFiscalYearStartDt](@fiscal_year));

            SELECT @report_end_dt = ISNULL(@report_end_dt,[dbo].[LF_GetFiscalYearEndDt](@fiscal_year));

        END;

        --SET START DATE TO 0:00 and end date to 23:59
        SELECT @report_start_dt = CAST(@report_start_dt AS DATE),
               @report_end_dt = CONVERT(CHAR(10),@report_end_dt,111) + ' 23:59:59.957';

        IF ISNULL(@order_categories,'') = ''
            INSERT INTO @order_category_list ([order_category]) 
            SELECT [description] 
            FROM [dbo].[TR_GOOESOFT_DROPDOWN] 
            WHERE [code] = '1494' AND [inactive] = 'N'  --code 1494 is the code that gets categories       
        ELSE
            INSERT INTO @order_category_list ([order_category]) 
            SELECT [item] 
            FROM [dbo].[LFT_SPLIT_DILEMETED_STRING] (REPLACE(@order_categories,'"',''), ',');


    /*  Use the LRP_GROUP_ORDER_DETAILS procedure (passing the parameters from this procedure) to populate the temp table with each order and it's info.  */
    
        INSERT INTO [#order_summary] ([customer_no], [customer_name], [customer_id], [city_state], [postal_code], [order_category_name], [mode_of_sale_name],
                                      [order_no], [order_dt], [perf_dt], [advance_days], [exhibits], [omni], [planetarium], [Theater4d], [butterfly],
                                      [special], [amount_due], [amount_paid], [balance_due])
        EXECUTE [dbo].[LRP_GROUP_ORDERS_DETAILS] @report_start_dt = @report_start_dt, 
                                                 @report_end_dt = @report_end_dt, 
                                                 @date_type = @date_type, 
                                                 @mos_str = @mos_str,
                                                 @order_categories = @order_categories,
                                                 @exclude_unpaid_past_orders = @exclude_unpaid_past_orders;

    FINISHED:        

        /*   Get final data set for the report  */    

            WITH CTE_VISIT_COUNTS ([order_no], [perf_dt], [visit_count])
            AS  (
                 SELECT [order_no], 
                        [perf_dt], 
                        MAX([visit_count])
                 FROM [#order_summary]
                 UNPIVOT ([visit_count] FOR [venue] IN ([exhibits], [omni], [planetarium], [special])) AS unpvt
                 GROUP BY [order_no], [perf_dt]
                )
            SELECT os.[customer_no],
                   os.[customer_name],
                   os.[customer_id],
                   os.[city_state],
                   os.[postal_code],
                   os.[order_category_name],
                   os.[mode_of_sale_name],
                   os.[order_no],
                   os.[order_dt],
                   os.[perf_dt],
                   CASE WHEN @date_type = 'Order Date' THEN os.[order_dt]
                        ELSE os.[perf_dt] END AS [date_display],
                   CASE WHEN @date_type = 'Order Date' THEN LEFT(CONVERT(VARCHAR(10),os.[order_dt],111),7)
                        ELSE LEFT(CONVERT(VARCHAR(10),os.[perf_dt],111),7) END AS [month_sort],
                   CASE WHEN @date_type = 'Order Date' THEN LEFT(DATENAME(MONTH,os.[order_dt]),3) + ' ' + RIGHT(DATENAME(YEAR,os.[order_dt]),2)
                        ELSE LEFT(DATENAME(MONTH,os.[perf_dt]),3) + ' ' + RIGHT(DATENAME(YEAR,os.[perf_dt]),2) END AS [month_sort_display],
                   os.[advance_days],
                   ISNULL(vc.[visit_count],0) AS [order_visit_count],
                   (os.exhibits + os.omni + os.planetarium + os.Theater4d + os.butterfly + os.special) AS [order_tickets_sold],
                   1 AS [order_counter],
                   os.[amount_due],
                   os.[amount_paid],
                   os.[balance_due]
            FROM [#order_summary] AS os
                 LEFT OUTER JOIN CTE_VISIT_COUNTS AS vc ON vc.[order_no] = os.[order_no] AND vc.[perf_dt] = os.[perf_dt]

 END 
 GO
 

--EXECUTE [dbo].[LRP_GROUP_ORDERS_SUMMARY] @report_start_dt = '10-1-2018', @report_end_dt = '10-31-2018', @date_type = 'Visit Date', @mos_str = '', @order_categories = '', @exclude_unpaid_past_orders = 'N'
