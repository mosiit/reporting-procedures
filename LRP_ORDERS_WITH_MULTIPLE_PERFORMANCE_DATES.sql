USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_ORDERS_WITH_MULTIPLE_PERFORMANCE_DATES]') AND type in (N'P', N'PC'))
BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_ORDERS_WITH_MULTIPLE_PERFORMANCE_DATES] AS' 
    EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LRP_ORDERS_WITH_MULTIPLE_PERFORMANCE_DATES] TO ImpUsers' 
END
GO


ALTER PROCEDURE [dbo].[LRP_ORDERS_WITH_MULTIPLE_PERFORMANCE_DATES]
        @report_start_dt DATETIME = NULL,
        @report_end_dt DATETIME = NULL,
        @date_type VARCHAR(30) = 'Order Date',
        @location_str VARCHAR(4000) = ''
AS BEGIN
    
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    SET NOCOUNT ON;

    /*  Procedure Variables  */

        DECLARE @location_list TABLE ([location_name] VARCHAR(30))

        DECLARE @order_data TABLE ([order_no] INT, [customer_no] INT, [perf_dt] DATETIME, [title_name] VARCHAR(30), [total_tickets] INT)

        DECLARE @final_table TABLE ([order_no] INT, [customer_no] INT DEFAULT (0), [perf_date] varchar(20) DEFAULT (''), [exhibit_halls] INT DEFAULT (0), 
                                    [omni_theater] INT DEFAULT (0), [planetarium] INT DEFAULT (0), [4d_theater] INT DEFAULT (0), [butterfly_garden] INT DEFAULT (0), 
                                    [school_lunch] INT DEFAULT (0), [other] INT DEFAULT (0), [total] INT DEFAULT (0))
    
    /*  Create temp table to deal with large number of rows  */
    
        IF OBJECT_ID('tempdb..#all_orders') IS NOT NULL DROP TABLE [#all_orders]

        CREATE TABLE [#all_orders] ([order_no] INT, [customer_no] INT)

        IF OBJECT_ID('tempdb..#order_table') IS NOT NULL DROP TABLE [#order_table]
        
        CREATE TABLE [#order_table] ([order_no] INT, [customer_no] INT, [perf_date_count] int)

    /*  Check Parameters  */
    
        SELECT @report_start_dt = ISNULL(@report_start_dt,DATEADD(DAY,-1,GETDATE()))
        SELECT @report_start_dt = CONVERT(DATE,@report_start_dt)

        SELECT @report_end_dt = ISNULL(@report_end_dt,DATEADD(DAY,-1,GETDATE()))
        SELECT @report_end_dt = CONVERT(CHAR(10),@report_end_dt,111) + ' 23:59:59.957'

        SELECT @date_type = ISNULL(@date_type,'')
        IF @date_type <> 'Performance Date' SELECT @date_type = 'Order Date'

        IF ISNULL(@location_str,'') <> ''
            INSERT INTO @location_list ([location_name])
            SELECT Element
            FROM [dbo].[FT_SPLIT_LIST](REPLACE(@location_str,'"',''),',');
        ELSE
            INSERT INTO @location_list ([location_name])
            SELECT DISTINCT [location] FROM [dbo].[T_METUSER]
        
    /*  Get a list of all orders first.  If narrowed down to specific location(s), this gives all orders created or touched by users within that date range by
        users from those locations.  It's better to get this list first because the next step is to pull any orders with multiple performance dates.  If we
        didn't have the initial list of all orders first, and the report was run for Science Central, orders that had mupltiple performance dates, one date
        added by Science Central and one date added by the Box Office would not show up on the report.  */
    
        IF @date_type = 'Performance Date'
            INSERT INTO [#all_orders] ([order_no], [customer_no])
            SELECT DISTINCT sli.[order_no], 
                            ord.[customer_no]
            FROM [dbo].[T_SUB_LINEITEM] AS sli
                 LEFT OUTER JOIN [dbo].[T_ORDER] AS ord ON ord.[order_no] = sli.[order_no]
                 LEFT OUTER JOIN [dbo].[T_METUSER] AS usr ON usr.[userid] = sli.[created_by]
                 LEFT OUTER JOIN  [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] AS prf ON prf.[performance_no] = sli.[perf_no] AND prf.[performance_zone] = sli.[zone_no]
                 INNER JOIN @location_list AS loc ON loc.[location_name] = usr.[location]
            WHERE prf.[performance_dt] BETWEEN @report_start_dt AND @report_end_dt
        ELSE
            INSERT INTO [#all_orders] ([order_no], [customer_no])
            SELECT ord.[order_no], 
                   ord.[customer_no]
            FROM [dbo].[T_ORDER] AS ord
                 LEFT OUTER JOIN [dbo].[T_METUSER] AS usr ON usr.[userid] = ord.[created_by]
                 INNER JOIN @location_list AS loc ON loc.[location_name] = usr.[location]
            WHERE ord.[create_dt] BETWEEN @report_start_dt AND @report_end_dt
          
        /*  Based on the list of orders just generated, get orders that have products from more than one performance date on them.
            Memberships, Vouchers, and Summer Courses are excluded because it's normal for those products to have a different 
            performance date than other products on the same order  */
        
            INSERT INTO [#order_table] ([order_no], [customer_no], [perf_date_count])
            SELECT sli.[order_no], 
                   ord.[customer_no],
                   COUNT (DISTINCT prf.[performance_date])
            FROM [dbo].[T_SUB_LINEITEM] AS sli
                 INNER JOIN [#all_orders] AS ord ON ord.[order_no] = sli.[order_no] 
                 LEFT OUTER JOIN [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] AS prf ON prf.[performance_no] = sli.[perf_no] AND prf.[performance_zone] = sli.[zone_no]
            WHERE prf.[title_name] NOT IN ('Membership','Vouchers','Summer Courses')
              AND sli.[sli_status] IN (2, 3, 6, 12)
            GROUP BY sli.[order_no],
                     ord.[customer_no]
            HAVING COUNT (DISTINCT prf.[performance_date]) > 1

    /*  Get the specific data for each order  */

        INSERT INTO @order_data([order_no],[customer_no], [perf_dt], [title_name], [total_tickets])
        SELECT sli.[order_no],
               ord.[customer_no],
               CONVERT(DATE,prf.[performance_dt]), 
               prf.[title_name], 
               COUNT(sli.[sli_no])
        FROM [dbo].[T_SUB_LINEITEM] AS sli
             INNER JOIN [#order_table] AS ord ON ord.[order_no] = sli.[order_no]
             LEFT OUTER JOIN [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] AS prf ON prf.[performance_no] = sli.[perf_no] AND prf.[performance_zone] = sli.[zone_no]
        WHERE prf.[title_name] NOT IN ('Membership','Vouchers','Summer Courses')
          AND sli.[sli_status] IN (2, 3, 12)
        GROUP BY sli.[order_no], ord.[customer_no], CONVERT(DATE,prf.[performance_dt]), prf.[title_name]

    /*  Double-check to make sure any order with only a single performance date is deleted  */

        DELETE FROM @order_data 
        WHERE [order_no] IN (SELECT [order_no] 
                             FROM @order_data 
                             GROUP BY [order_no] 
                             HAVING COUNT(*) = 1)
    
    /*  Get the data for each venue.  I tried to do this with a pivot table but was having trouble making it work so for the sake of speed, 
        I processed one title at a time, the totaled everything up at the end.  */

        --Exhibit Halls
        INSERT INTO @final_table ([order_no],[customer_no],[perf_date],[exhibit_halls])
        SELECT [order_no], 
               [customer_no],
               CONVERT(VARCHAR(20),[perf_dt],101), 
               SUM(total_tickets)
        FROM @order_data 
        WHERE [title_name] = 'Exhibit Halls' 
        GROUP BY [order_no], [customer_no], CONVERT(VARCHAR(20),[perf_dt],101)

        --Omni Theater
        INSERT INTO @final_table ([order_no],[customer_no],[perf_date],[omni_theater])
        SELECT [order_no], 
               [customer_no],
               CONVERT(VARCHAR(20),[perf_dt],101), 
               SUM(total_tickets)
        FROM @order_data 
        WHERE [title_name] = 'Mugar Omni Theater' 
        GROUP BY [order_no], [customer_no], CONVERT(VARCHAR(20),[perf_dt],101)

        --Planetarium
        INSERT INTO @final_table ([order_no],[customer_no],[perf_date],[planetarium])
        SELECT [order_no], 
               [customer_no],
               CONVERT(VARCHAR(20),[perf_dt],101), 
               SUM(total_tickets)
        FROM @order_data 
        WHERE [title_name] = 'Hayden Planetarium' 
        GROUP BY [order_no], [customer_no], CONVERT(VARCHAR(20),[perf_dt],101)

        --4D Theater
        INSERT INTO @final_table ([order_no],[customer_no],[perf_date],[4d_theater])
        SELECT [order_no], 
               [customer_no],
               CONVERT(VARCHAR(20),[perf_dt],101), 
               SUM(total_tickets)
        FROM @order_data 
        WHERE [title_name] = '4-D Theater' 
        GROUP BY [order_no], [customer_no], CONVERT(VARCHAR(20),[perf_dt],101)

        --Butterfly Garden
        INSERT INTO @final_table ([order_no],[customer_no],[perf_date],[butterfly_garden]) 
        SELECT [order_no],
               [customer_no],
               CONVERT(VARCHAR(20),[perf_dt],101), 
               SUM(total_tickets)
        FROM @order_data 
        WHERE [title_name] = 'Butterfly Garden' 
        GROUP BY [order_no], [customer_no], CONVERT(VARCHAR(20),[perf_dt],101)

        --School Lunch
        INSERT INTO @final_table ([order_no],[customer_no],[perf_date],[school_lunch]) 
        SELECT [order_no],
               [customer_no],
               CONVERT(VARCHAR(20),[perf_dt],101),
               SUM(total_tickets)
        FROM @order_data 
        WHERE [title_name] = 'School Lunch' 
        GROUP BY [order_no], [customer_no], CONVERT(VARCHAR(20),[perf_dt],101)

        --Other
        INSERT INTO @final_table ([order_no],[customer_no],[perf_date],[other]) 
        SELECT [order_no],
               [customer_no],
               CONVERT(VARCHAR(20),[perf_dt],101),
               SUM(total_tickets)
        FROM @order_data 
        WHERE [title_name] NOT IN ('Exhibit Halls', 'Mugar Omni Theater', 'Hayden Planetarium', '4-D Theater', 'Butterfly Garden', 'School Lunch')
        GROUP BY [order_no], [customer_no], CONVERT(VARCHAR(20),[perf_dt],101)
         
        /*  Do the math to get the total (which for each individual record should be the same as whaever venue is on that record)  */
    
        UPDATE @final_table 
        SET [total] = ([exhibit_halls] + [omni_theater] + [planetarium] + [4d_theater] + [butterfly_garden] + [school_lunch] + [other])
            
        /*  Pull the final data  */

        SELECT [order_no],
               [customer_no],
               [perf_date],
               SUM([exhibit_halls]) AS [exhibit_halls],
               SUM([omni_theater]) AS [omni_theater],
               SUM([planetarium]) AS [planetarium],
               SUM([4d_theater]) AS [4d_theater],
               SUM([butterfly_garden]) AS [butterfly_garden],
               SUM([school_lunch]) AS [school_lunch],
               SUM([other]) AS [other],
               SUM([total]) AS [total]
        FROM @final_table 
        GROUP BY [order_no], [customer_no], [perf_date]
        ORDER BY order_no, perf_date

    /*  Drop the Temp Tables  */

        IF OBJECT_ID('tempdb..#all_orders') IS NOT NULL DROP TABLE [#all_orders]

        IF OBJECT_ID('tempdb..#order_table') IS NOT NULL DROP TABLE [#order_table]

END
GO

--EXECUTE [dbo].[LRP_ORDERS_WITH_MULTIPLE_PERFORMANCE_DATES] @report_start_dt = '6-1-2018', @report_end_dt = '6-30-2018', @date_type = 'Performance Date', @location_str = ''
    





