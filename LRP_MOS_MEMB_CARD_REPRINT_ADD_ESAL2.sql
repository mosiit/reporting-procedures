USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LRP_MOS_MEMB_CARD_REPRINT_ADD_ESAL2]    Script Date: 11/18/2019 7:34:28 AM ******/
DROP PROCEDURE [dbo].[LRP_MOS_MEMB_CARD_REPRINT_ADD_ESAL2]
GO

/****** Object:  StoredProcedure [dbo].[LRP_MOS_MEMB_CARD_REPRINT_ADD_ESAL2]    Script Date: 11/18/2019 7:34:28 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



/*
-- =============================================
-- Author:		Alex Harris for Tessitura Network
-- Create date: April 22, 2016
-- Description:	Member Card Reprint Utility
-- =============================================

@perf_no = 8 --Membership Perf
@price_type = 204 --Member Card Reprint


MODIFIED 6/20/2017 BG(Tess)
added optional @start_dt and @end_dt parameters 

MODIFIED 6/23/2017 BG(Tess)
fixed issue with leading zeros in the PIN by converting int to varchar before appending

MODIFIED 7/6/2017 BG(Tess)
Now inserting sli_no into LT_MOS_MEMBER_CARDS so cards will only be reprinted once for each reprint-ticket request

MODIFIED 10/18/2019 CCastilla/TN
For TeamSupport ticket 71547
Added esal2_desc to the output. Calculation similar to membership fulfillment report, so a new Affiliation_type
parameter has also been added.

MODIFIED 11/8/2019 CCastilla/TN
For TeamSupport ticket 71547
Re-ordered the parameters in report setup so the "Preview or Update" parameter would appear on the first page -
it was spilling onto the second page after the addition of the "Affiliation Type" parameter. Also moved the 
parameter position of @Preview here to the last place.

 Sample Exec:

Exec LRP_MOS_MEMB_CARD_REPRINT @customer_no = 2653105

*/

CREATE PROCEDURE [dbo].[LRP_MOS_MEMB_CARD_REPRINT_ADD_ESAL2] 
	@customer_no INT = NULL,
	@affiliation_type INT = 0,		--added 10/18/2019 CCastilla/TN
	@start_dt DATETIME = NULL,
	@end_dt DATETIME = NULL,
	@perf_type INT = NULL,
	@price_type_str VARCHAR(4000) = NULL,
	@activity_category INT = NULL,
	@activity_type_str VARCHAR(4000) = NULL,
	--@resolved_action int = null,
	@memb_org_str VARCHAR(4000) = NULL,
	@memb_level_str VARCHAR(4000) = NULL,
	@signor INT = NULL,
	@Preview CHAR(1) = 'R', --'U' = Update, 'R' = Review
	@resolved_action INT = NULL


AS
BEGIN

if OBJECT_ID('tempdb..#memb_cards') is not null begin drop table #memb_cards end 

	SET NOCOUNT ON;

	Declare	@memb_card_prefix varchar(10),
			@memb_card_customer_length int,
			@card_name_override_kw int

Set		@card_name_override_kw = 482

Select @memb_card_prefix = Coalesce(dbo.FS_GET_DEFAULT_VALUE(dbo.FS_GET_PARAM_FROM_APPNAME('UG'), 'IMPRESARIO', 'NSCAN_MEMBER_CUSTOMER_PREFIX'),'')
Select @memb_card_customer_length = Coalesce(dbo.FS_GET_DEFAULT_VALUE(dbo.FS_GET_PARAM_FROM_APPNAME('UG'), 'IMPRESARIO', 'NSCAN_MEMBER_CUSTOMER_LENGTH'),0)


Create table #memb_cards (
		customer_no int null,
		cust_memb_no int null,
		esal1_desc varchar(55) null,
		esal2_desc varchar(55) null,		--added 10/18/2019 CCastilla/TN
		street1 varchar(64) null,
		street2 varchar(64) null,
		street3 varchar(64) null,
		city varchar(30) null,
		state varchar(20) null,
		postal_code varchar(10) null,
		country varchar(30) null,
		memb_desc varchar(30) null,
		expr_dt datetime null,
		a1_name varchar(55) null,
		a2_name varchar(55) null,
		household_customer_no int null,
		card_number varchar(30) null,
		pin char(4) null,
		mag_stripe1 varchar(255) null,
		mag_stripe2 varchar(255) null,
		sli_no int null,
		activity_no int null,
		activity_notes varchar(4000) null
		)

	
	-- If a customer number is provided, populate the temp table for that customer number	
	If Isnull(@customer_no,0) > 0
	  Begin
		Insert into #memb_cards (customer_no, cust_memb_no, memb_desc, expr_dt)
		Select	cm.customer_no,
				cm.customer_memb_no,
				cm.memb_level_description,
				Convert(datetime,Convert(date,cm.expiration_date))
		From		dbo.LV_CURRENT_MEMBERSHIP_INFO As cm With (Nolock)
				Join dbo.T_MEMB_ORG As mo On cm.memb_org_description = mo.description
				Join	 dbo.T_MEMB_LEVEL As ml With (Nolock) On mo.memb_org_no = ml.memb_org_no And cm.memb_level_code = ml.memb_level
		Where	cm.customer_no = @customer_no
				And	(Isnull(@memb_org_str,'') = '' Or Charindex(','+Convert(varchar,mo.memb_org_no)+',',','+@memb_org_str+',') > 0)
				And	(Isnull(@memb_level_str,'') = '' Or Charindex(','+Convert(varchar,ml.memb_level_no)+',',','+@memb_level_str+',') > 0)	
	  End

	ELSE
	  BEGIN
		-- First gather up anyone who requested a reprint via a reprint ticket order	
		Insert into #memb_cards (customer_no, cust_memb_no, memb_desc, expr_dt, sli_no)
		Select	cm.customer_no,
				cm.customer_memb_no,
				cm.memb_level_description,
				Convert(datetime,Convert(date,cm.expiration_date)),
				sli.sli_no
		From		dbo.T_SUB_LINEITEM As sli With (Nolock)
				Join	 dbo.T_ORDER As o With (Nolock) On sli.order_no = o.order_no
				Join	 dbo.LV_CURRENT_MEMBERSHIP_INFO As cm With (Nolock) On o.customer_no = cm.customer_no
				Join	 dbo.T_MEMB_ORG As mo On cm.memb_org_description = mo.description
				Join	 dbo.T_MEMB_LEVEL As ml On mo.memb_org_no = ml.memb_org_no And cm.memb_level_code = ml.memb_level
				Join	 dbo.T_PERF As p With (Nolock) On sli.perf_no = p.perf_no
		Where	p.perf_type = @perf_type
				And	sli.create_dt between ISNULL(@start_dt,'1/1/1901') and ISNULL(@end_dt,getdate())		
				And	Charindex(','+convert(varchar,sli.price_type)+',',','+@price_type_str+',') > 0
				And	(Isnull(@memb_org_str,'') = '' Or Charindex(','+Convert(varchar,mo.memb_org_no)+',',','+@memb_org_str+',') > 0)
				And	(Isnull(@memb_level_str,'') = '' Or Charindex(','+Convert(varchar,ml.memb_level_no)+',',','+@memb_level_str+',') > 0)
				And Not Exists (Select 1 From dbo.LT_MOS_MEMBER_CARDS As mc Where sli.sli_no = mc.sli_no) -- Only include cards that haven't been printed yet
	
		-- Now gather anyone who requested a reprint via a CSI
		INSERT INTO #memb_cards (customer_no, cust_memb_no, memb_desc, expr_dt, activity_no, activity_notes)
		SELECT	ca.customer_no,			
				cm.customer_memb_no,
				cm.memb_level_description,
				CONVERT(DATETIME,CONVERT(DATE,cm.expiration_date)),
				ca.activity_no,
				ca.notes
		FROM		dbo.T_CUST_ACTIVITY AS ca
				JOIN	 dbo.LV_CURRENT_MEMBERSHIP_INFO AS cm ON ca.customer_no = cm.customer_no
				JOIN	 dbo.T_MEMB_ORG AS mo ON cm.memb_org_description = mo.description
				JOIN	 dbo.T_MEMB_LEVEL AS ml ON mo.memb_org_no = ml.memb_org_no AND cm.memb_level_code = ml.memb_level
		WHERE	ca.create_dt BETWEEN ISNULL(@start_dt,'1/1/1901') AND ISNULL(@end_dt,GETDATE())		
				AND	CHARINDEX(','+CONVERT(VARCHAR,ca.activity_type)+',',','+@activity_type_str+',') > 0
				AND	(ISNULL(@memb_org_str,'') = '' OR CHARINDEX(','+CONVERT(VARCHAR,mo.memb_org_no)+',',','+@memb_org_str+',') > 0)
				AND	(ISNULL(@memb_level_str,'') = '' OR CHARINDEX(','+CONVERT(VARCHAR,ml.memb_level_no)+',',','+@memb_level_str+',') > 0)
				AND NOT EXISTS (SELECT 1 FROM dbo.T_ISSUE_ACTION AS ia WHERE ca.activity_no = ia.activity_no AND ia.res_ind = 'Y') -- Only unresolved CSIs
	  END

	-- Update esal1_desc
	Update	m
	Set		m.esal1_desc = Coalesce(s1.esal1_desc,s2.esal1_desc)
	From		#memb_cards As m
			Left Join dbo.TX_CUST_SAL As s1 On m.customer_no = s1.customer_no And s1.signor = @signor
			Join dbo.TX_CUST_SAL As s2 On m.customer_no = s2.customer_no And s2.default_ind = 'Y'

	-- Update esal2_desc			--added 10/18/2019 CCastilla/TN based on LP_MOS_MEMBERSHIP_FULFILLMENT esal2 logic
	Update	m
	Set		m.esal2_desc =	Case
								--When l.gift_memb_role_id = @send_to_giver Then Coalesce(cs4.esal2_desc,cs3.esal2_desc)
								--not using the above - see comments directly below 10/18/2019 CCastilla/TN
								When @affiliation_type > 0 Then cs5.esal1_desc  -- ASH 09/20/2016
								Else Coalesce(cs2.esal2_desc, cs1.esal2_desc) 
							End	
	From		#memb_cards As m
			Join dbo.TX_CUST_SAL As cs1 WITH (NOLOCK) On m.customer_no = cs1.customer_no And cs1.default_ind = 'Y'
			Left Join dbo.TX_CUST_SAL As cs2 WITH (NOLOCK) On m.customer_no = cs2.customer_no And cs2.signor = @signor
			Left Join dbo.T_AFFILIATION As af WITH (NOLOCK) On m.customer_no = af.individual_customer_no And af.affiliation_type_id = @affiliation_type AND (Isnull(af.end_dt,'2999-12-31') > Getdate() And Isnull(af.inactive,'N') = 'N')  -- ASH 09/20/2016 w/ updates by MP 10/2/18 to add Affilitation Type
			Left Join dbo.TX_CUST_SAL As cs5 WITH (NOLOCK) On af.group_customer_no	= cs5.customer_no And cs5.default_ind = 'Y'  -- ASH 09/20/2016
	-- from LP_MOS_MEMBERSHIP_FULFILLMENT esal2 logic - uses these when special request indicates to send to giver (see above)
	-- but since the rest of this reprint process doesn't take gifted memberships into account, leave out for now
	--		Left Join dbo.TX_CUST_SAL As cs3 WITH (NOLOCK) On cm.ben_provider = cs3.customer_no And cs3.default_ind = 'Y'
	--		Left Join dbo.TX_CUST_SAL As cs4 WITH (NOLOCK) On cm.ben_provider = cs4.customer_no And cs4.signor = @signor
		



	-- Update address info
	Update	m
	Set		m.street1 = a.street1,
			m.street2 = a.street2,
			m.street3 = a.street3,
			m.city = a.city,
			m.state = a.state,
			m.postal_code = CASE
					WHEN LEN(a.postal_code)>5 AND a.postal_code NOT LIKE '%[A-Z]%' THEN SUBSTRING(a.postal_code,1,5) +'-' +SUBSTRING(a.postal_code,6,5)
					ELSE a.postal_code
							END,
			m.country = c.short_desc
	From		#memb_cards As m
			Join dbo.T_ADDRESS As a On m.customer_no = a.customer_no And a.primary_ind = 'Y'
			Join dbo.TR_COUNTRY As c On a.country = c.id

	--Update A1/A2 names
	Update m
	Set		m.a1_name = Coalesce(kw1.key_value,cs2.esal1_desc,cs.esal1_desc),
			m.a2_name = Coalesce(kw2.key_value,cs3.esal1_desc)
	From		#memb_cards As m
			Join dbo.TX_CUST_SAL AS cs WITH (NOLOCK) On m.customer_no = cs.customer_no And cs.default_ind = 'Y'
			Left Join dbo.T_AFFILIATION As a1 WITH (NOLOCK) On m.customer_no = a1.group_customer_no And a1.primary_ind = 'Y' And a1.name_ind = -1
			Left Join dbo.T_AFFILIATION As a2 WITH (NOLOCK) On m.customer_no = a2.group_customer_no And a2.primary_ind = 'Y' And a2.name_ind = -2
			Left Join dbo.TX_CUST_SAL As cs2 WITH (NOLOCK) On a1.individual_customer_no = cs2.customer_no And cs2.default_ind = 'Y'
			Left Join dbo.TX_CUST_SAL As cs3 WITH (NOLOCK) On a2.individual_customer_no = cs3.customer_no And cs3.default_ind = 'Y'
			Left Join dbo.TX_CUST_KEYWORD As kw1 WITH (NOLOCK) On a1.individual_customer_no = kw1.customer_no And kw1.keyword_no in (@card_name_override_kw)
			Left Join dbo.TX_CUST_KEYWORD As kw2 WITH (NOLOCK) On a2.individual_customer_no = kw2.customer_no And kw2.keyword_no in (@card_name_override_kw)

	--Update Household Customer No
	Update	m
	Set		m.household_customer_no = pg.expanded_customer_no
	From		#memb_cards As m
			Join dbo.V_CUSTOMER_WITH_PRIMARY_GROUP As pg WITH (NOLOCK) On m.customer_no = pg.customer_no

if @Preview = 'U'
BEGIN
	--Create and record pins, then update #memb_cards with card numbers.
	Update	mc
	Set		mc.inactive = 'Y'
	From		dbo.LT_MOS_MEMBER_CARDS As mc
			Join #memb_cards As m On mc.cust_memb_no = m.cust_memb_no
END
	
	Declare @current_cust_memb_no int
	Declare @pins Table (cust_memb_no int not null, pin varchar(4) null)
	
	Insert into @pins (cust_memb_no)
	Select Distinct cust_memb_no
	From		#memb_cards
	
	Select @current_cust_memb_no = min(cust_memb_no) From @pins

	While Isnull(@current_cust_memb_no,0) <> 0
	  Begin
	    Update	@pins
		Set		pin = case when @preview = 'U' then Right('0000' + Convert(varchar,Convert(Decimal(4,0),Rand()*10000)),4) else 'NEW*' end
		Where	cust_memb_no = @current_cust_memb_no 

		if @Preview = 'U'
		BEGIN
		Insert into dbo.LT_MOS_MEMBER_CARDS (customer_no, cust_memb_no, pin, printed_dt, sli_no, inactive)
		Select	m.customer_no,
				m.cust_memb_no,
				p.pin,
				getdate(),
				m.sli_no, --Added 7/6/2017 BG(Tess)
				'N'
		From		#memb_cards As m
				Join	@pins As p On m.cust_memb_no = p.cust_memb_no And p.cust_memb_no = @current_cust_memb_no
		END

		--Delete From @pins Where cust_memb_no = @current_cust_memb_no

		SELECT @current_cust_memb_no = MIN(cust_memb_no) FROM @pins WHERE cust_memb_no > @current_cust_memb_no
	  END
	
	IF @Preview = 'U'
	BEGIN
	UPDATE m
	SET		m.card_number = @memb_card_prefix + RIGHT('0000000000'+CONVERT(VARCHAR,mc.customer_no),@memb_card_customer_length-LEN(@memb_card_prefix))+'-'+CONVERT(VARCHAR,mc.pin),
			m.pin = mc.pin,
			m.mag_stripe1 = CASE WHEN ISNULL(c2.lname,'') <> '' THEN 'B639647' + RIGHT('00000000'+CONVERT(VARCHAR,m.customer_no),8)
								+ mc.pin + '0^' + UPPER(ISNULL(c2.lname,'')) + '/' + UPPER(ISNULL(c2.fname,'')) +'^'
								+ SUBSTRING(REPLACE(CONVERT(VARCHAR,m.expr_dt,101),'/',''),1,4) + '101'
								+ CONVERT(VARCHAR,GETDATE(),112)
							WHEN ISNULL(c3.lname,'') <> '' THEN 'B639647' + RIGHT('00000000'+CONVERT(VARCHAR,m.customer_no),8)
								+ mc.pin + '0^' + UPPER(ISNULL(c3.lname,'')) + '/' + UPPER(ISNULL(c3.fname,'')) +'^'
								+ SUBSTRING(REPLACE(CONVERT(VARCHAR,m.expr_dt,101),'/',''),1,4) + '101'
								+ CONVERT(VARCHAR,GETDATE(),112)
							ELSE 'B639647' + RIGHT('00000000'+CONVERT(VARCHAR,m.customer_no),8)
								+ mc.pin + '0^' + UPPER(ISNULL(c.lname,'')) + '/' + UPPER(ISNULL(c.fname,'')) +'^'
								+ SUBSTRING(REPLACE(CONVERT(VARCHAR,m.expr_dt,101),'/',''),1,4) + '101'
								+ CONVERT(VARCHAR,GETDATE(),112)
							END,
			m.mag_stripe2 = '639647' + RIGHT('00000000'+CONVERT(VARCHAR,m.customer_no),8) + mc.pin + '0' + '=' 
								+ SUBSTRING(REPLACE(CONVERT(VARCHAR,m.expr_dt,101),'/',''),1,4) + '101'
								+ CONVERT(VARCHAR,GETDATE(),112) + '='
						
	FROM		#memb_cards AS m
			JOIN dbo.LT_MOS_MEMBER_CARDS AS mc ON m.customer_no = mc.customer_no AND m.cust_memb_no = mc.cust_memb_no
			JOIN dbo.T_CUSTOMER AS c ON m.customer_no = c.customer_no
			LEFT JOIN dbo.T_AFFILIATION AS af ON mc.customer_no = af.group_customer_no AND af.primary_ind = 'Y' AND af.name_ind = -1
			LEFT JOIN dbo.T_CUSTOMER AS c2 ON af.individual_customer_no = c2.customer_no
			LEFT JOIN dbo.T_AFFILIATION AS af2 ON mc.customer_no = af2.group_customer_no AND af2.primary_ind = 'Y' AND af2.name_ind = -2
			LEFT JOIN dbo.T_CUSTOMER AS c3 ON af2.individual_customer_no = c3.customer_no
	WHERE	mc.inactive = 'N'
	END

	IF @Preview = 'R' 
	BEGIN
	UPDATE	m
			SET m.pin = p.pin 
	FROM		#memb_cards m 
			JOIN @pins p ON m.cust_memb_no = p.cust_memb_no
	END

	--Insert a duplicate row into #memb_cards if there is an A2 name.
	INSERT INTO #memb_cards
	SELECT * FROM #memb_cards WHERE ISNULL(a2_name,'') <> ''

	
	-- Output results
	SELECT * FROM #memb_cards
	ORDER BY customer_no, cust_memb_no

	--select * from @pins

END
GO

GRANT EXECUTE ON [dbo].[LRP_MOS_MEMB_CARD_REPRINT_ADD_ESAL2] TO [ImpUsers]
GO
GRANT EXECUTE ON [dbo].[LRP_MOS_MEMB_CARD_REPRINT_ADD_ESAL2] TO [tessitura_app]
GO


