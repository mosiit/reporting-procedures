USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_ADV_CAMPAIGN_ACTIVITY_25K_PLUS]') AND type IN (N'P', N'PC'))
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_ADV_CAMPAIGN_ACTIVITY_25K_PLUS] AS' 
GO

EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LRP_ADV_CAMPAIGN_ACTIVITY_25K_PLUS] TO [impusers], [tessitura_app]'
GO

/*      LRP_ADV_CAMPAIGN_ACTIVITY_25K_PLUS
        Pulls a list from the LV_CAMPAIGN_ACTIVITY_CONTRIBUTIONS view of all customers who have given a total gift on a single day of $25,000 or more within the last 90 days.
        It could be comprised of multiple contributions with the same contribution date and long as the campaign category is not �skip�.
        
        NOTE: Uses LV_CAMPAIGN_ACTIVITY_CONTRIBUTIONS view which is a view that was created using the
              same logic in the MOS Advancement Contributions report (LRP_MOS_ADVANCEMENT_CONTRIBUTION_DETAIL).
*/
ALTER PROCEDURE [dbo].[LRP_ADV_CAMPAIGN_ACTIVITY_25K_PLUS]
        @fiscal_year INT = NULL,
        @workers_str VARCHAR(4000) = NULL
WITH RECOMPILE AS BEGIN

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    SET ANSI_WARNINGS OFF
	SET NOCOUNT ON;

    /*  Procedure Variables  */

        DECLARE @fiscal_start_dt DATETIME = NULL, @fiscal_end_dt DATETIME = NULL
        DECLARE @current_dt DATETIME = GETDATE()

        DECLARE @worker_list TABLE ([worker_no] INT NOT NULL DEFAULT (0))

        DECLARE @major_gifts TABLE ([worker_no] INT NOT NULL DEFAULT (0),
                                    [worker_name] VARCHAR(80) NOT NULL DEFAULT (''),
                                    [worker_initials] VARCHAR(25) NOT NULL DEFAULT (''),
                                    [worker_inactive] char(1) NOT NULL DEFAULT ('N'),
                                    [customer_no] INT NOT NULL DEFAULT(0),
                                    [constituent_name] VARCHAR(100) NOT NULL DEFAULT (''),
                                    [sort_name] VARCHAR(100) NOT NULL DEFAULT (''),
                                    [contribution_date] DATETIME NULL,
                                    [contribution_amount] DECIMAL (18,2) NOT NULL DEFAULT (0),
                                    [is_anonymous] INT NOT NULL DEFAULT (0),
                                    [designation_count] INT NOT NULL DEFAULT (0),
                                    [designation] VARCHAR(50) NOT NULL DEFAULT (''))

    /*  Check Parameters  */

        IF @fiscal_year IS NULL SELECT @fiscal_year = dbo.LF_GetFiscalYear(GETDATE())
        
        SELECT @fiscal_start_dt = [dbo].[LF_GetFiscalYearStartDt](@fiscal_year)
        SELECT @fiscal_end_dt = [dbo].[LF_GetFiscalYearEndDt](@fiscal_year)

        IF @current_dt > @fiscal_end_dt SELECT @current_dt = @fiscal_end_dt

        IF ISNULL(@workers_str,'') <> ''
            INSERT INTO @worker_list ([worker_no])
            SELECT ISNULL(TRY_CAST([Element] AS INTEGER),0) 
            FROM [dbo].[FT_SPLIT_LIST](REPLACE(@workers_str,'"',''),',')
            WHERE ISNULL(TRY_CAST([Element] AS INTEGER),0) > 0

    /*  Get list of 25K + Gifts in the last 90 daya*/

        INSERT INTO @major_gifts ([worker_no], [worker_name], [worker_initials], [worker_inactive], [customer_no], 
                                  [constituent_name], [sort_name], [contribution_date], [contribution_amount], 
                                  [is_anonymous], [designation_count], [designation])
        SELECT ISNULL(con.[solicitor_number],0),
               ISNULL(wrk.[worker_name],'Unknown'),
               ISNULL(wrk.[worker_initials],''),
               ISNULL(wrk.[inactive],'N'),
               ISNULL(con.[customer_no],0),
               ISNULL(con.[constituent_name],''),
               ISNULL(con.[sort_name],''),
               CAST(con.[contribution_date] AS DATE), 
               SUM(con.[contribution_amount]),
               SUM(CASE WHEN ISNULL(con.[is_anonymous],'N') = 'Y' THEN 1 ELSE 0 END),
               COUNT(DISTINCT con.[designation]),
               ISNULL(MIN(con.[designation]) + CASE WHEN COUNT(DISTINCT con.[designation]) > 1 
                                             THEN ' (' + CAST(COUNT(DISTINCT con.[designation]) AS VARCHAR(5)) + ' des)' 
                                             ELSE '' END,'')
        FROM [dbo].[LV_CAMPAIGN_ACTIVITY_CONTRIBUTIONS] AS con
             LEFT OUTER JOIN [dbo].[LV_CAMPAIGN_ACTIVITY_WORKERS] AS wrk ON wrk.[worker_customer_no] = con.[solicitor_number]
        WHERE con.[contribution_date] between @fiscal_start_dt AND @current_dt
          AND con.[campaign_category] <> 'Skip'
        GROUP BY ISNULL(con.[solicitor_number],0), ISNULL(wrk.[worker_name],'Unknown'), ISNULL(wrk.[worker_initials],''),
                 ISNULL(wrk.[inactive],'N'), con.[customer_no], con.[constituent_name], con.[sort_name], CAST(con.[contribution_date] AS DATE)
        HAVING SUM(con.[contribution_amount]) >= 25000

        IF EXISTS (SELECT [worker_no] FROM @worker_list)
            DELETE FROM @major_gifts
            WHERE [worker_no] NOT IN (SELECT [worker_no] FROM @worker_list)

    FINISHED:
    
        /*  Select final data set to return to the report  */

            SELECT [worker_no],
                   [worker_name],
                   [worker_initials],
                   [worker_inactive],
                   [customer_no],
                   [constituent_name],
                   [sort_name],
                   [contribution_date],
                   [contribution_amount],
                   CASE WHEN [is_anonymous] > 0 THEN 'Y' ELSE 'N' END AS [is_anonymous],
                   [designation_count],
                   [designation]
            FROM @major_gifts
            ORDER BY [contribution_date] DESC, [sort_name]
             
END 
GO

EXECUTE [dbo].[LRP_ADV_CAMPAIGN_ACTIVITY_25K_PLUS] @fiscal_year = NULL, @workers_str = NULL





--SELECT * FROM T_CUSTOMER WHERE [customer_no] = 2671183
--SELECT * FROM LV_CAMPAIGN_ACTIVITY_WORKERS WHERE [worker_customer_no] = 2671183
--SELECT solicitor_number, is_anonymous,* FROM [dbo].[LV_CAMPAIGN_ACTIVITY_CONTRIBUTIONS] WHERE [contribution_date] = '2020-08-30' AND  [customer_no]= 681098
--SELECT solicitor_number,* FROM [dbo].[LV_CAMPAIGN_ACTIVITY_CONTRIBUTIONS] WHERE [contribution_date] = '2020-08-04' AND  [customer_no]= 12759
--SELECT DISTINCT is_anonymous FROM  [dbo].[LV_CAMPAIGN_ACTIVITY_CONTRIBUTIONS] WHERE [contribution_date] > = '7-1-2019'
