USE impresario
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_ADV_SOLICITATION_ACTIVITY_SUMMARY]') AND type IN (N'P', N'PC'))
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_ADV_SOLICITATION_ACTIVITY_SUMMARY] AS' 
GO

EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LRP_ADV_SOLICITATION_ACTIVITY_SUMMARY] TO [impusers], [tessitura_app]'
GO

/*      LRP_ADV_CAMPAIGN_ACTIVITY_STAFF_PIPELINE
        Pulls information for goals, numbers, and YTD percentages for all step activity, vists, ask/yield amounts, and actual contributions taken in.

        NOTE: Contribution amounts are based on what's in The Plan record under cont_amount

*/
ALTER PROCEDURE [dbo].[LRP_ADV_SOLICITATION_ACTIVITY_SUMMARY]
        @fiscal_year INT = NULL,
        @workers_str VARCHAR(4000) = NULL
WITH RECOMPILE AS BEGIN

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

    /* Procedure Variables  */

        --Determine current fiscal year, fiscal start date, fiscal end date, and current date
        DECLARE @fiscal_start_dt DATETIME = NULL, @fiscal_end_dt DATETIME = NULL
        DECLARE @current_dt DATETIME = GETDATE()

        DECLARE @worker_list TABLE ([worker_no] INT NOT NULL DEFAULT (0))

        DECLARE @ask_ranges TABLE ([range_order] INT NOT NULL DEFAULT (0),
                                   [range_minimum] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                   [range_maximum] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                   [range_display] VARCHAR(50) NOT NULL DEFAULT (''))

    /*  Temporary Tables  */
                                    
        IF OBJECT_ID('tempdb..#plan_data') IS NOT NULL DROP TABLE #plan_data

        CREATE TABLE [#plan_data] ([plan_no] INT NOT NULL DEFAULT (0),
                                   [worker_customer_no] INT NOT NULL DEFAULT (0),
                                   [worker_name] VARCHAR(75) NOT NULL DEFAULT (0),
                                   [worker_initials] VARCHAR(75) NOT NULL DEFAULT (0),
                                   [worker_inactive] CHAR(1) NOT NULL DEFAULT ('N'),
                                   [start_dt] DATETIME NULL,
                                   [complete_by_dt] DATETIME NULL,
                                   [campaign_no] INT NOT NULL DEFAULT (0),
                                   [campaign_description] VARCHAR(50) NOT NULL DEFAULT (''),
                                   [ask_amount] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                   [goal_amount] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                   [plan_amount] DECIMAL(18,2) NOT NULL DEFAULT (0.0),
                                   [contribution_amount] DECIMAL (18,2) NOT NULL DEFAULT (0.0),
                                   [plan_status_id] INT NOT NULL DEFAULT (0),
                                   [plan_status] VARCHAR(50) NOT NULL DEFAULT (''))

        IF OBJECT_ID('tempdb..#final_table') IS NOT NULL DROP TABLE #final_table

        CREATE TABLE #final_table ([worker_customer_no] INT NOT NULL DEFAULT (0),
                                   [worker_name] VARCHAR(80) NOT NULL DEFAULT (''),
                                   [worker_initials] VARCHAR(75) NOT NULL DEFAULT (''),
                                   [worker_inactive] CHAR(1) NOT NULL DEFAULT ('N'),
                                   [plan_no] INT NOT NULL DEFAULT (0),
                                   [campaign_no] INT NOT NULL DEFAULT (0),
                                   [campaign_description] VARCHAR(50) NOT NULL DEFAULT(''),
                                   [ask_amount] DECIMAL (18,2) NOT NULL DEFAULT (0.0),
                                   [contribution_amount] DECIMAL (18,2) NOT NULL DEFAULT (0.0),
                                   [range_order] INT NOT NULL DEFAULT (0),
                                   [range_display] VARCHAR(50) NOT NULL DEFAULT (''),
                                   [range_minimum] DECIMAL (18,2) NOT NULL DEFAULT (0.0),
                                   [range_maximum] DECIMAL (18,2) NOT NULL DEFAULT (0.0))
    /*  Check Parameters  */

        IF @fiscal_year IS NULL SELECT @fiscal_year = dbo.LF_GetFiscalYear(GETDATE())
        
        SELECT @fiscal_start_dt = [dbo].[LF_GetFiscalYearStartDt](@fiscal_year)

        SELECT @fiscal_end_dt = [dbo].[LF_GetFiscalYearEndDt](@fiscal_year)
        
        IF @current_dt > @fiscal_end_dt SELECT @current_dt = @fiscal_end_dt

        IF ISNULL(@workers_str,'') = ''
            INSERT INTO @worker_list ([worker_no])
            SELECT ISNULL([worker_customer_no], 0)
            FROM [dbo].[LV_CAMPAIGN_ACTIVITY_WORKERS] 
            WHERE [inactive] = 'N'
              AND ISNULL([worker_customer_no], 0) > 0
        ELSE
            INSERT INTO @worker_list ([worker_no])
            SELECT ISNULL(TRY_CAST([Element] AS INTEGER),0) 
            FROM [dbo].[FT_SPLIT_LIST](REPLACE(@workers_str,'"',''),',')
            WHERE ISNULL(TRY_CAST([Element] AS INTEGER),0) > 0

    /*  Create Ask Ranges  */

        INSERT INTO @ask_ranges ([range_order], [range_minimum], [range_maximum], [range_display])
        VALUES (1, 10000000.00, 1000000000.00, '$10M+'),
               (2, 5000000.00, 9999999.99, '$5M - $9.9M'),
               (3, 1000000.00, 4999999.99, '$1M - $4.9M'),
               (4, 500000.00, 999999.99, '$500K - $999K'),
               (5, 250000.00, 499999.99, '$250K - $499K'),
               (6, 100000.00, 249999.00, '$100K - $249K'),
               (7, 25000.00, 99999.99, '$25K - $99K'),
               (8, 10000.00, 24999.99, '$10K - $24.9K'),
               (9, 2500.00, 9999.99, '$2.5K - $9.9K'),
               (10, 300.00, 2499.99, '$300 - 2.49K'),
               (11, 1.00, 299.00, 'Under $300')

    /*  Pull the plan data to be used  */

        INSERT INTO [#plan_data] ([plan_no], [worker_customer_no], [worker_name], [worker_initials], [worker_inactive], [start_dt], [complete_by_dt], 
                                  [campaign_no], [campaign_description], [ask_amount], [goal_amount], [contribution_amount], [plan_status_id], [plan_status])
        SELECT pln.[plan_no],
               xpp.[customer_no],
               wrk.[worker_name],
               wrk.[worker_initials],
               wrk.[inactive],
               pln.[start_dt],
               pln.[complete_by_dt],
               pln.[campaign_no],
               cmp.[description],
               ISNULL(pln.[ask_amt],0.0),
               ISNULL(pln.[goal_amt],0.0),
               ISNULL(pln.[cont_amt],0.0),
               pln.[status],
               sta.[description]
        FROM [dbo].[T_PLAN] AS pln
             LEFT OUTER JOIN [dbo].[TX_CUST_PLAN] AS xpp ON xpp.[plan_no] = pln.[plan_no] AND xpp.[role_no] = 1
             INNER JOIN @worker_list AS lis ON lis.[worker_no] = xpp.[customer_no]
             INNER JOIN [dbo].[LV_CAMPAIGN_ACTIVITY_WORKERS] AS wrk ON wrk.[worker_customer_no] = xpp.[customer_no]
             LEFT OUTER JOIN [dbo].[TR_PLAN_STATUS] AS sta ON sta.[id] = pln.[status]
             LEFT OUTER JOIN [dbo].[T_CAMPAIGN] AS cmp ON cmp.[campaign_no] = pln.[campaign_no]
        WHERE pln.[start_dt] BETWEEN @fiscal_start_dt AND @fiscal_end_dt
          AND pln.[status] IN (23, 26, 35, 38)
          AND ISNULL(pln.[ask_amt], 0) > 0;

     /*  ***Solicitations***  */
     
        INSERT INTO [#final_table] ([worker_customer_no],[worker_name],[worker_initials],[worker_inactive],[plan_no], [campaign_no], [campaign_description], 
                                    [ask_amount], [contribution_amount],[range_order],[range_display],[range_minimum],[range_maximum])
        SELECT pln.[worker_customer_no],
               pln.[worker_name],
               pln.[worker_initials],
               pln.[worker_inactive],
               pln.[plan_no],
               pln.[campaign_no],
               pln.[campaign_description],
               pln.[ask_amount],
               pln.[contribution_amount],
               rng.[range_order],
               rng.[range_display],
               rng.[range_minimum],
               rng.[range_maximum]
        FROM [#plan_data] AS pln
             LEFT OUTER JOIN @ask_ranges AS rng ON pln.[ask_amount] BETWEEN rng.[range_minimum] AND rng.[range_maximum]     
        WHERE pln.[start_dt] BETWEEN @fiscal_start_dt AND @current_dt 
          AND pln.[worker_inactive] = 'N'
        ORDER BY rng.[range_order]
        

    FINISHED:       

        SELECT [worker_customer_no],
            [worker_name],
            [worker_initials],
            [worker_inactive],
            [plan_no],
            [campaign_no],
            [campaign_description],
            [ask_amount],
            [contribution_amount],
            [range_order],
            [range_display],
            [range_minimum],
            [range_maximum],
            1 AS [plan_counter]
        FROM [#final_table]

    DONE:

        IF OBJECT_ID('tempdb..#final_table') IS NOT NULL DROP TABLE [#final_table]
        IF OBJECT_ID('tempdb..#plan_data') IS NOT NULL DROP TABLE #plan_data

END
GO

EXECUTE [dbo].[LRP_ADV_SOLICITATION_ACTIVITY_SUMMARY] @fiscal_year = 2021, @workers_str = '672463'
--EXECUTE [dbo].[LRP_ADV_CAMPAIGN_ACTIVITY_STAFF_PIPELINE] @fiscal_year = 2021, @workers_str = '3504561,3657886,672463,817730'

/*
--FOR PARAMETER
SELECT [worker_customer_no],                --DATA
       [worker_name]                        --DISPLAY
FROM  [dbo].[LV_CAMPAIGN_ACTIVITY_WORKERS]  --TABLE
WHERE [inactive] = 'N'                      --WHERE CLAUSE
ORDER BY [worker_sort]                      --SORT
--"3504561","3657886","672463","817730"
*/


