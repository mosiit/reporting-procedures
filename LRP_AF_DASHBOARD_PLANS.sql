USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_AF_DASHBOARD_PLANS]') AND type in (N'P', N'PC'))
BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_AF_DASHBOARD_PLANS] AS' 
    EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LRP_AF_DASHBOARD_PLANS] TO ImpUsers' 
END
GO

ALTER PROCEDURE [dbo].[LRP_AF_DASHBOARD_PLANS]
        @report_start_dt DATETIME = NULL,               --Report start and end dates filter on complete_by_dt field in T_PLAN
        @report_end_dt DATETIME = NULL,                 --Report start and end dates must be within same fiscal year
        @campaign_category_str VARCHAR(4000) = NULL,    --optional multi choice of 7 (Innovator) and/or 28 (Annual Giving) and/or 45 (Annual Fund_L)
        @worker_role INT = NULL,                        --1 = primary / 11 = secondary
        @plan_source_str VARCHAR(4000) = NULL,          --optional multi choice of any source in TR_PLAN_SOURCE where inactive = 'N'
        @worker_str VARCHAR(4000) = NULL,               --optional multi choice of all staff people who have worker id numbers
        @hide_close_date CHAR(1) = 'Y'                  --Yes/No - Determines how output will look
AS BEGIN

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    SET NOCOUNT ON;

    /*  Procedure Variables  */

        DECLARE @campaign_category_list TABLE ([campaign_category_no] INT)

        DECLARE @plan_source_list TABLE ([plan_source_no] INT)

        DECLARE @worker_list TABLE ([customer_no] INT)

        DECLARE @report_message VARCHAR(100) = ''

        DECLARE @seed_plan_source VARCHAR(30) = '', @seed_month CHAR(7) = '', @seed_month_display VARCHAR(30) = '', @seed_solicitor_role VARCHAR(30) = ''
    
    /*  Temporary Tables  */

        IF OBJECT_ID('tempdb..#summary_info_cache') IS NOT NULL DROP TABLE [#summary_info_cache]

        CREATE TABLE [#summary_info_cache] ([customer_no] INT NOT NULL, 
                                            [which_year] VARCHAR(15) NOT NULL, 
                                            [summary_amount] DECIMAL (18,2) NOT NULL)

        IF OBJECT_ID('tempdb..#dashboard_plans_data') IS NOT NULL DROP TABLE [#dashboard_plans_data]

        CREATE TABLE [#dashboard_plans_data] ([plan_no] INT NOT NULL DEFAULT (0),
                                              [customer_no] INT NOT NULL DEFAULT (0),
                                              [customer_type_no] INT NOT NULL DEFAULT (0),
                                              [customer_type] VARCHAR(30) NOT NULL DEFAULT(''),
                                              [customer_name] VARCHAR(50) NOT NULL DEFAULT(''),
                                              [customer_sort_name] VARCHAR(75) NOT NULL DEFAULT(''),
                                              [plan_status_no] INT NOT NULL DEFAULT (0),
                                              [plan_status] VARCHAR(30) NOT NULL DEFAULT(''),
                                              [plan_type_no] INT NOT NULL DEFAULT (0),
                                              [plan_type] VARCHAR(30) NOT NULL DEFAULT(''),
                                              [plan_status_group] VARCHAR(30) NOT NULL DEFAULT(''),
                                              [plan_status_group_secondary] VARCHAR(30) NOT NULL DEFAULT('Secondary'),
                                              [plan_source_no] INT NOT NULL DEFAULT (0),
                                              [plan_source] VARCHAR(30) NOT NULL DEFAULT(''),
                                              [plan_start_dt] DATETIME NULL,
                                              [complete_by_dt] DATETIME NULL,
                                              [plan_complete_date] CHAR(10) NULL DEFAULT(''),
                                              [plan_complete_month] CHAR(7) NULL DEFAULT(''),
                                              [plan_complete_month_display] VARCHAR(30) NULL DEFAULT(''),
                                              [plan_goal_amt] DECIMAL(18,2) NOT NULL DEFAULT (0),
                                              [plan_ask_amt] DECIMAL(18,2) NOT NULL DEFAULT (0),
                                              [plan_cont_amt] DECIMAL(18,2) NOT NULL DEFAULT (0),
                                              [plan_recorded_amt] DECIMAL(18,2) NOT NULL DEFAULT (0),
                                              [plan_report_amt] DECIMAL(18,2) NOT NULL DEFAULT (0),
                                              [plan_report_amt_soc] DECIMAL(18,2) NOT NULL DEFAULT (0),
                                              [plan_soc_count] DECIMAL(18,2) NOT NULL DEFAULT (0),
                                              [plan_ask_group] VARCHAR(30) NOT NULL DEFAULT(''),
                                              [plan_ask_group_sort] CHAR(1) NOT NULL DEFAULT('A'),
                                              [campaign_no] INT NOT NULL DEFAULT (0),
                                              [campaign_name] VARCHAR(30) NOT NULL DEFAULT(''),
                                              [campaign_type] CHAR(1) NOT NULL DEFAULT(''),
                                              [campaign_category_no] INT NOT NULL DEFAULT (0),
                                              [campaign_category] VARCHAR(30) NOT NULL DEFAULT(''),
                                              [fyear] INT NOT NULL DEFAULT (0),
                                              [cont_designation_no] INT NOT NULL DEFAULT (0), 
                                              [cont_designation] VARCHAR(30) NOT NULL DEFAULT(''),
                                              [plan_solicitor_no] INT NOT NULL DEFAULT (0),
                                              [plan_solicitor] VARCHAR(50) NOT NULL DEFAULT(''),
                                              [plan_solicitor_role_no] INT NOT NULL DEFAULT (0),
                                              [plan_solicitor_role] VARCHAR(30) NOT NULL DEFAULT(''),
                                              [plan_solicitor_display] VARCHAR(50) NOT NULL DEFAULT (''),
                                              [selected_year_summary] DECIMAL(18,2) NOT NULL DEFAULT (0),
                                              [previous_year_summary] DECIMAL(18,2) NOT NULL DEFAULT (0),
                                              [upgrade_count] INT NOT NULL DEFAULT (0),
                                              [downgrade_count] INT NOT NULL DEFAULT (0),
                                              [same_count] INT NOT NULL DEFAULT (0),
                                              [create_loc] VARCHAR(50) NULL DEFAULT(''),
                                              [create_dt] DATETIME NULL,
                                              [created_by] VARCHAR(10) NULL DEFAULT(''),
                                              [last_update_dt] datetime NULL,
                                              [last_updated_by] VARCHAR(10) NULL DEFAULT(''),
                                              [report_message] VARCHAR(100) NULL)

    /*  Check Parameters  */

        IF @report_start_dt IS NULL OR @report_end_dt IS NULL
        OR [dbo].[LF_GetFiscalYear](@report_start_dt) <> [dbo].[LF_GetFiscalYear](@report_end_dt) BEGIN
       
            SELECT @report_message = 'invalid dates...you must choose two dates that are both within the same fiscal year'
            GOTO FINISHED

        END

        SELECT @worker_role = ISNULL(@worker_role,0)
        IF @worker_role NOT IN (1, 11, 12) SELECT @worker_role = 0

        SELECT @hide_close_date = ISNULL(@hide_close_date,'Y')
        IF @hide_close_date <> 'N' SELECT @hide_close_date = 'Y'

        IF ISNULL(@campaign_category_str,'') <> ''
            INSERT INTO @campaign_category_list ([campaign_category_no])
            SELECT CONVERT(INT,[Element])
            FROM [dbo].[FT_SPLIT_LIST](REPLACE(@campaign_category_str,'"',''),',')
        ELSE
            INSERT INTO @campaign_category_list ([campaign_category_no])
            VALUES (7), (28), (45);

        IF ISNULL(@plan_source_str,'') <> ''
            INSERT INTO @plan_source_list ([plan_source_no])
            SELECT CONVERT(INT,[Element])
            FROM [dbo].[FT_SPLIT_LIST](REPLACE(@plan_source_str,'"',''),',')
        ELSE
            INSERT INTO @plan_source_list ([plan_source_no])
            SELECT [id] FROM [dbo].[TR_PLAN_SOURCE] 
            WHERE [inactive] = 'N';

        IF ISNULL(@worker_str,'') <> ''
            INSERT INTO @worker_list ([customer_no])
            SELECT CONVERT(INT,[Element])
            FROM [dbo].[FT_SPLIT_LIST](REPLACE(@worker_str,'"',''),',')
        ELSE
            INSERT INTO @worker_list ([customer_no])
            SELECT [worker_customer_no] 
            FROM [dbo].[LV_CAMPAIGN_ACTIVITY_WORKERS]
            WHERE [inactive] = 'N'
            UNION SELECT 0;
            --SELECT [worker_customer_no] 
            --FROM [dbo].[T_METUSER]
            --WHERE ISNULL([worker_customer_no],0) > 0
            --UNION SELECT 0;


    /*  Cache the summary information  */

        --current year
        INSERT INTO [#summary_info_cache] ([customer_no], [which_year], [summary_amount])
        SELECT [customer_no],
               'selected',
               [value] 
        FROM [dbo].[LT_NIGHTLY_SUMMARY]
        WHERE SUBSTRING ([section], 21,6) = 'Annual' 
          AND SUBSTRING ([section], 3, 4) IN (SELECT cmp.[fyear]
                                              FROM [dbo].[T_PLAN] AS pln					
		                                           INNER JOIN [dbo].[T_CAMPAIGN] AS cmp ON cmp.[campaign_no] = pln.[campaign_no]
		                                      WHERE CAST(@report_start_dt AS DATE) >= cmp.[start_dt]
		                                        AND CAST (@report_end_dt AS DATE) <= cmp.[end_dt])
        --previous year
        INSERT INTO [#summary_info_cache] ([customer_no], [which_year], [summary_amount])
        SELECT [customer_no],
               'previous',
               [value]
        FROM [dbo].[LT_NIGHTLY_SUMMARY]
        WHERE SUBSTRING ([section], 21,6) = 'Annual' 
          AND SUBSTRING ([section], 3, 4) + 1 IN (SELECT cmp.[fyear]
                                                  FROM [dbo].[T_PLAN] AS pln					
                                                       INNER JOIN [dbo].[T_CAMPAIGN] AS cmp ON cmp.[campaign_no] = pln.[campaign_no]
                                                  WHERE CAST(@report_start_dt AS DATE) >= cmp.[start_dt]
                                                    AND CAST (@report_end_dt AS DATE) <= cmp.[end_dt])
        
    /*  Customer Number should not appear more than once in a given year  */


        IF EXISTS (SELECT [customer_no], [which_year] FROM [#summary_info_cache] GROUP BY [customer_no], [which_year] HAVING COUNT(*) > 1) BEGIN
            SELECT @report_message = 'Bad Data Found...customer number appearing more than once in a year within LT_NIGHTLY_SUMMARY'
            GOTO FINISHED
        END


    /*  Pull Plan data  */
        
        INSERT INTO [#dashboard_plans_data] ([plan_no], [customer_no], [customer_type_no], [customer_type], [customer_name], [customer_sort_name], [plan_status_no], [plan_status], 
                                             [plan_type_no], [plan_type], [plan_status_group], [plan_source_no], [plan_source], [plan_start_dt], [complete_by_dt], [plan_complete_date], 
                                             [plan_complete_month], [plan_complete_month_display], [plan_goal_amt], [plan_ask_amt], [plan_cont_amt], [plan_recorded_amt], [campaign_no], 
                                             [campaign_name], [campaign_type], [campaign_category_no], [campaign_category], [fyear], [cont_designation_no], [cont_designation], 
                                             [plan_solicitor_no], [plan_solicitor], [plan_solicitor_role_no], [plan_solicitor_role], [selected_year_summary], [previous_year_summary], 
                                             [create_loc], [create_dt], [created_by], [last_update_dt], [last_updated_by])                                           
        SELECT pln.[plan_no],
               pln.[customer_no],
               cus.[cust_type] AS [customer_type_no],
               ctp.[description] AS [customer_type],
               nam.[best_name],
               na2.[sort_name],
               pln.[status] AS [plan_status_no],
               pst.[description] AS [plan_status],
               pln.[type] AS [plan_type_no],
               ptp.[description] AS [plan_type],
               CASE WHEN pln.[status] = 1 THEN 'Identification'
                    WHEN pln.[status] IN (2, 4, 22) THEN 'Cultivation'
                    WHEN pln.[status] IN (6, 9, 23, 24, 26, 38) THEN 'Solicitation'
                    WHEN pln.[status] = 35 THEN 'Stewardship'
                    ELSE '_Disqualify' END AS [plan_status_group],
               pln.[original_source] AS [plan_source_no],
               src.[description] AS [plan_source],
               pln.[start_dt] AS [plan_start_dt],
               pln.[complete_by_dt] AS [plan_complete_dt],
               CONVERT(CHAR(10),pln.[complete_by_dt],111) AS [plan_complete_date],
               LEFT(CONVERT(CHAR(10),pln.[complete_by_dt],111),7) AS [plan_complete_month],
               LEFT(DATENAME(MONTH,pln.[complete_by_dt]),3) + ' ' + RIGHT(DATENAME(YEAR,pln.[complete_by_dt]),2) AS [plan_complete_month_display],
               ISNULL(pln.[goal_amt],0.00) AS [plan_goal_amt],
               ISNULL(pln.[ask_amt],0.00) AS [plan_ask_amt],
               ISNULL(pln.[cont_amt],0.00) AS [plan_cont_amt],
               ISNULL(pln.[recorded_amt],0.00) AS [plan_recorded_amt],
               pln.[campaign_no],
               cmp.[description] AS [campaign_name],
               cmp.[camp_type] AS [campaign_type],
               cmp.[category] AS [campaign_category_no],
               cct.[description] AS [campaign_category],
               cmp.[fyear] AS [campaign_fiscal_year],
               pln.[cont_designation] AS [cont_designation_no], 
               cds.[description] AS [cont_designation],
               ISNULL(slp.[customer_no],0) AS [plan_solicitor_no],
               ISNULL(slp.[sort_name],'XXX') AS [plan_solicitor],
               ISNULL(xpp.[role_no],0) AS [plan_solicitor_role_no],
               ISNULL(rol.[description],'No Solicitor') AS [plan_solicitor_role],
               ISNULL(ics.[summary_amount],0.00),
               ISNULL(icp.[summary_amount],0.00),
               pln.[create_loc],
               pln.[create_dt],
               pln.[created_by],
               pln.[last_update_dt],
               pln.[last_updated_by]
        FROM [dbo].[T_PLAN] AS pln
             INNER JOIN [dbo].[T_CAMPAIGN] AS cmp ON cmp.[campaign_no] = pln.[campaign_no]
             INNER JOIN @campaign_category_list clist ON clist.campaign_category_no = cmp.[category]
             INNER JOIN @plan_source_list AS slist ON slist.[plan_source_no] = pln.[original_source]
             INNER JOIN [dbo].[TR_CAMPAIGN_CATEGORY] AS cct ON cct.[id] = cmp.[category]
             INNER JOIN [dbo].[LV_ADV_BEST_NAME] AS nam ON nam.[customer_no] = pln.[customer_no]
             INNER JOIN [dbo].[T_CUSTOMER] AS na2 ON na2.[customer_no] = pln.[customer_no]
             INNER JOIN [dbo].[T_CUSTOMER] AS cus ON cus.[customer_no] = pln.[customer_no]
             INNER JOIN [dbo].[TR_CUST_TYPE] AS ctp ON ctp.[id] = cus.[cust_type]
             INNER JOIN [dbo].[TR_PLAN_SOURCE] AS src ON src.[id] = pln.[original_source]
             INNER JOIN [dbo].[TR_CONT_DESIGNATION] AS cds ON cds.[id] = pln.[cont_designation]
             INNER JOIN [dbo].[TR_PLAN_STATUS] AS pst ON pst.[id] = pln.[status]
             INNER JOIN [dbo].[TR_PLAN_TYPE] AS ptp ON ptp.[id] = pln.[type]
             LEFT OUTER JOIN [dbo].[TX_CUST_PLAN] AS xpp ON xpp.[plan_no] = pln.[plan_no] AND xpp.[role_no] IN (1, 11, 12)
             LEFT OUTER JOIN [dbo].[T_CUSTOMER] AS slp ON slp.[customer_no] = xpp.[customer_no]
             LEFT OUTER JOIN [dbo].[TR_WORKER_ROLE] AS rol ON rol.[id] = xpp.[role_no]
             LEFT OUTER JOIN [#summary_info_cache] AS ics ON ics.customer_no = pln.[customer_no] AND ics.[which_year] = 'selected'
             LEFT OUTER JOIN [#summary_info_cache] AS icp ON icp.customer_no = pln.[customer_no] AND icp.[which_year] = 'previous'
        WHERE pln.[complete_by_dt] BETWEEN @report_start_dt AND @report_end_dt
              AND pln.[cont_designation] = 2  -- 2 = Annual Fund
              AND cus.[cust_type] IN (1, 7)   -- 1 = Individual / 7 = Household
              AND (@worker_role = 0 OR ISNULL(xpp.[role_no],0) IN (@worker_role,0))

    /*  This needed to be a separate statement so that the exceptions would still show up on the report  */

        DELETE FROM [#dashboard_plans_data] WHERE [plan_solicitor_no] > 0 AND [plan_solicitor_no] NOT IN (SELECT [customer_no] FROM @worker_list)
             
    /*  IF no records selected, jump to end  */

        IF NOT EXISTS (SELECT * FROM [#dashboard_plans_data]) GOTO FINISHED

    /*  Plan/Solicitor combo should never appear in table more than once  */

        IF EXISTS (SELECT [plan_no], [plan_solicitor_no] FROM [#dashboard_plans_data] GROUP BY [plan_no], [plan_solicitor_no] HAVING COUNT(*) > 1) BEGIN
            SELECT @report_message = 'Bad Data Found...One or more plans is appearing multiple times in the data with the same solicitor.'
            GOTO FINISHED
        END

    /*  Update the Plan Ask Groups  */

        UPDATE [#dashboard_plans_data]
        SET [plan_ask_group] = CASE WHEN [plan_ask_amt] <= 0 THEN '$0.00'
                                    WHEN [plan_ask_amt] < 300.00 THEN '< $300'
                                    WHEN [plan_ask_amt] < 2500.00 THEN '$300 - $2499'
                                    ELSE '$2500 +' END,
            [plan_ask_group_sort] = CASE WHEN [plan_ask_amt] <= 0 THEN 'A'
                                         WHEN [plan_ask_amt] < 300.00 THEN 'B'
                                         WHEN [plan_ask_amt] < 2500.00 THEN 'C'
                                         ELSE 'D' END
        WHERE [plan_status_group] <> '_Disqualify'

        UPDATE [#dashboard_plans_data]
        SET [plan_ask_group] = 'ALL',
            [plan_ask_group_sort] = 'A'
        WHERE [plan_status_group] = '_Disqualify'


    /* Manual Seeding - This makes shure all columns show up in the report even if no data exists for that column */
    
        IF @worker_role = 12 SELECT @seed_solicitor_role = 'Former' 
        ELSE IF @worker_role = 11 SELECT @seed_solicitor_role = 'Secondary' 
        ELSE SELECT @seed_solicitor_role = 'Primary'
        
        SELECT @seed_plan_source = MIN([plan_source]) FROM [#dashboard_plans_data]
        SELECT @seed_plan_source = ISNULL(@seed_plan_source,'')
        
        SELECT @seed_month = LEFT(CONVERT(CHAR(10),@report_end_dt,111),7)
        SELECT @seed_month_display = LEFT(DATENAME(MONTH,@report_end_dt),3) + ' ' + RIGHT(DATENAME(YEAR,@report_end_dt),2)
        
        INSERT INTO [#dashboard_plans_data] ([plan_status_group], [plan_source], [plan_start_dt], [complete_by_dt], [plan_complete_date], [plan_complete_month], 
                                             [plan_complete_month_display], [plan_ask_group], [plan_ask_group_sort], [plan_solicitor], [plan_solicitor_role], 
                                             [plan_solicitor_display], [create_dt], [last_update_dt])
        VALUES ('_Disqualify', @seed_plan_source, @report_start_dt, @report_end_dt, CONVERT(CHAR(10),@report_end_dt,111), @seed_month, @seed_month_display, 
                'All', 'A', '', @seed_solicitor_role + ' Solicitor', @seed_solicitor_role, GETDATE(), GETDATE()),

               ('Identification', @seed_plan_source, @report_start_dt, @report_end_dt, CONVERT(CHAR(10),@report_end_dt,111), @seed_month, @seed_month_display, 
                '$0.00', 'A', '', @seed_solicitor_role + ' Solicitor', @seed_solicitor_role, GETDATE(), GETDATE()),

               ('Identification', @seed_plan_source, @report_start_dt, @report_end_dt, CONVERT(CHAR(10),@report_end_dt,111), @seed_month, @seed_month_display, 
                '< $300', 'B', '', @seed_solicitor_role + ' Solicitor', @seed_solicitor_role, GETDATE(), GETDATE()),

               ('Identification', @seed_plan_source, @report_start_dt, @report_end_dt, CONVERT(CHAR(10),@report_end_dt,111), @seed_month, @seed_month_display, 
                '$300 - $2499', 'C', '', @seed_solicitor_role + ' Solicitor', @seed_solicitor_role, GETDATE(), GETDATE()),

               ('Identification', @seed_plan_source, @report_start_dt, @report_end_dt, CONVERT(CHAR(10),@report_end_dt,111), @seed_month, @seed_month_display, 
                '$2500 +', 'D', '', @seed_solicitor_role + ' Solicitor', @seed_solicitor_role, GETDATE(), GETDATE()),

               ('Cultivation', @seed_plan_source, @report_start_dt, @report_end_dt, CONVERT(CHAR(10),@report_end_dt,111), @seed_month, @seed_month_display, 
                '$0.00', 'A', '', @seed_solicitor_role + ' Solicitor', @seed_solicitor_role, GETDATE(), GETDATE()),

               ('Cultivation', @seed_plan_source, @report_start_dt, @report_end_dt, CONVERT(CHAR(10),@report_end_dt,111), @seed_month, @seed_month_display, 
                '< $300', 'B', '', @seed_solicitor_role + ' Solicitor', @seed_solicitor_role, GETDATE(), GETDATE()),

               ('Cultivation', @seed_plan_source, @report_start_dt, @report_end_dt, CONVERT(CHAR(10),@report_end_dt,111), @seed_month, @seed_month_display, 
                '$300 - $2499', 'C', '', @seed_solicitor_role + ' Solicitor', @seed_solicitor_role, GETDATE(), GETDATE()),

               ('Cultivation', @seed_plan_source, @report_start_dt, @report_end_dt, CONVERT(CHAR(10),@report_end_dt,111), @seed_month, @seed_month_display, 
                '$2500 +', 'D', '', @seed_solicitor_role + ' Solicitor', @seed_solicitor_role, GETDATE(), GETDATE()),

               ('Solicitation', @seed_plan_source, @report_start_dt, @report_end_dt, CONVERT(CHAR(10),@report_end_dt,111), @seed_month, @seed_month_display, 
                '$0.00', 'A', '', @seed_solicitor_role + ' Solicitor', @seed_solicitor_role, GETDATE(), GETDATE()),

               ('Solicitation', @seed_plan_source, @report_start_dt, @report_end_dt, CONVERT(CHAR(10),@report_end_dt,111), @seed_month, @seed_month_display, 
                '< $300', 'B', '', @seed_solicitor_role + ' Solicitor', @seed_solicitor_role, GETDATE(), GETDATE()),

               ('Solicitation', @seed_plan_source, @report_start_dt, @report_end_dt, CONVERT(CHAR(10),@report_end_dt,111), @seed_month, @seed_month_display, 
                '$300 - $2499', 'C', '', @seed_solicitor_role + ' Solicitor', @seed_solicitor_role, GETDATE(), GETDATE()),

               ('Solicitation', @seed_plan_source, @report_start_dt, @report_end_dt, CONVERT(CHAR(10),@report_end_dt,111), @seed_month, @seed_month_display, 
                '$2500 +', 'D', '', @seed_solicitor_role + ' Solicitor', @seed_solicitor_role, GETDATE(), GETDATE()),

               ('Stewardship', @seed_plan_source, @report_start_dt, @report_end_dt, CONVERT(CHAR(10),@report_end_dt,111), @seed_month, @seed_month_display, 
                '$0.00', 'A', '', @seed_solicitor_role + ' Solicitor', @seed_solicitor_role, GETDATE(), GETDATE()),

               ('Stewardship', @seed_plan_source, @report_start_dt, @report_end_dt, CONVERT(CHAR(10),@report_end_dt,111), @seed_month, @seed_month_display, 
                '< $300', 'B', '', @seed_solicitor_role + ' Solicitor', @seed_solicitor_role, GETDATE(), GETDATE()),

               ('Stewardship', @seed_plan_source, @report_start_dt, @report_end_dt, CONVERT(CHAR(10),@report_end_dt,111), @seed_month, @seed_month_display, 
                '$300 - $2499', 'C', '', @seed_solicitor_role + ' Solicitor', @seed_solicitor_role, GETDATE(), GETDATE()),

               ('Stewardship', @seed_plan_source, @report_start_dt, @report_end_dt, CONVERT(CHAR(10),@report_end_dt,111), @seed_month, @seed_month_display, 
                '$2500 +', 'D', '', @seed_solicitor_role + ' Solicitor', @seed_solicitor_role, GETDATE(), GETDATE())

    /*  Set Amount Reported based on plan status group  */

        UPDATE [#dashboard_plans_data]
        SET [plan_report_amt] = CASE WHEN [plan_status_group] = 'Stewardship' THEN [plan_cont_amt]
                                     ELSE [plan_ask_amt] END

    /*  Update total for Society Realized Percent  */

        UPDATE [#dashboard_plans_data]
        SET [plan_report_amt_soc] = [plan_report_amt], [plan_soc_count] = 1
        WHERE [plan_status_no] = 35         --35 = 07-Stewardship
          AND [plan_ask_group] IN ('$300 - $2499','$2500 +') 

    /*  Set Solicitor Display Text  */

        UPDATE [#dashboard_plans_data]
        SET [plan_solicitor_display] = CASE WHEN [plan_solicitor_role] LIKE '%fmr%' THEN 'Former'
                                            WHEN [plan_solicitor] = 'XXX' THEN 'NONE' 
                                            ELSE LTRIM(RTRIM(REPLACE([plan_solicitor_role],'Solicitor',''))) END 
                                     + ' ' + LTRIM(RTRIM([plan_solicitor]))


    /*  Set the upgrade/downgrade flags  */

        UPDATE [#dashboard_plans_data]
        SET [upgrade_count] = 1
        WHERE [selected_year_summary] > [previous_year_summary]

        UPDATE [#dashboard_plans_data]
        SET [downgrade_count] = 1
        WHERE [selected_year_summary] < [previous_year_summary]

        UPDATE [#dashboard_plans_data]
        SET [same_count] = 1
        WHERE [selected_year_summary] = [previous_year_summary]

    /*  Update Close Date Display if hiding close date  */

        IF @hide_close_date = 'Y'
            UPDATE [#dashboard_plans_data]
            SET [plan_complete_month] = 'All',
                [plan_complete_month_display] = 'All Months'

    FINISHED:

        /*  If nothing in the [#dashboard_plans_data] temp table and the report_message variable is blank, 
            add a message saying nothing was found  */

            IF NOT EXISTS (SELECT * FROM [#dashboard_plans_data]) AND ISNULL(@report_message,'') = '' 
                SELECT @report_message = 'No data was found for the criteria you entered.'

        /*  If the report message variable is not blank, that means the process did not complete successfully.  
            Delete everything from the table and add a single record with the message  */

            IF @report_message <> '' BEGIN

                DELETE FROM [#dashboard_plans_data]

                INSERT INTO [#dashboard_plans_data] ([report_message]) VALUES (@report_message)
                
            END

        /*  Pull final data set to be passed to the report*/

            SELECT [plan_no], 
                   [customer_no], 
                   [customer_type_no], 
                   [customer_type], 
                   [customer_name], 
                   [customer_sort_name],
                   [plan_status_no], 
                   [plan_status], 
                   [plan_type_no], 
                   [plan_type],
                   [plan_status_group],
                   [plan_status_group_secondary], 
                   [plan_source_no], 
                   [plan_source], 
                   [plan_start_dt], 
                   [complete_by_dt], 
                   [plan_complete_date], 
                   [plan_complete_month],
                   [plan_complete_month_display], 
                   [plan_goal_amt], 
                   [plan_ask_amt], 
                   [plan_cont_amt], 
                   [plan_recorded_amt], 
                   [plan_report_amt],
                   [plan_report_amt_soc],
                   [plan_soc_count],
                   [plan_ask_group],
                   [plan_ask_group_sort],
                   [campaign_no], 
                   [campaign_name],
                   [campaign_type], 
                   [campaign_category_no], 
                   [campaign_category], 
                   [fyear], 
                   [cont_designation_no], 
                   [cont_designation], 
                   [plan_solicitor_no],
                   [plan_solicitor], 
                   [plan_solicitor_role_no], 
                   [plan_solicitor_role],
                   [plan_solicitor_display],
                   [selected_year_summary],
                   [previous_year_summary],
                   [upgrade_count],
                   [downgrade_count],
                   [same_count],
                   [create_loc], 
                   [create_dt], 
                   [created_by], 
                   [last_update_dt], 
                   [last_updated_by],
                   ISNULL(report_message,'') AS [report_message]
            FROM [#dashboard_plans_data]
            
            
    DONE:

END
GO

EXECUTE  [dbo].[LRP_AF_DASHBOARD_PLANS] @report_start_dt = '7-1-2018', @report_end_dt = '6-30-2019', @campaign_category_str = '', 
         @worker_role = 1, @plan_source_str = '', @worker_str = '', @hide_close_date = 'N';

--SELECT * FROM dbo.T_PLAN WHERE plan_no IN (SELECT plan_no FROM [dbo].[TX_CUST_PLAN] WHERE role_no = 12) and [cont_designation] = 2 
--SELECT * FROM T_PLAN WHERE plan_no = 128976
--SELECT * FROM TX_CUST_PLAN WHERE plan_no = 128976
--SELECT category FROM dbo.T_CAMPAIGN WHERE campaign_no = 211
--SELECT * FROM T_PLAN WHERE plan_no IN (SELECT plan_no FROM dbo.TX_CUST_PLAN WHERE role_no = 12)
--SELECT * FROM dbo.TR_PLAN_STATUS

--(128218,128616,128204,128222,128322,128554,128922,128207,128257,128258,128325,129847,128163,128220,128320,128327,128595,129706,129849,128169,128178
--128205,128544,128612,128614,128737,128151,128164,128251,128566,128615,128262,128321,128206,128209,128223,128242,128259,128323)