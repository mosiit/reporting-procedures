USE [impresario]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[LRP_AF_CALL_SHEET_CALCULATIONS]
(
	@customer_no INT
)    
AS

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
SET NOCOUNT ON 

--DECLARE @customer_no INT = 10080
DECLARE @currentFY INT 

SELECT @currentFY = fyear
FROM dbo.TR_BATCH_Period
WHERE GETDATE() BETWEEN start_dt AND end_dt

-- Final results will be here 
DECLARE @resultsTbl TABLE 
(
	customer_no INT,
	[Current FY AF Des Plg Balance] MONEY,
	[Current FY Non AF Des GP Total] MONEY,
	[Last GP Amt] MONEY,
	[Last GP Date] DATE,
	[Last AF Des GP Amt] MONEY,
	[Last AF Des GP Date] DATE,
	[Num FYs gave large AF Designation] INT 
)

INSERT INTO @resultsTbl
	(customer_no,
	[Current FY AF Des Plg Balance],
	[Current FY Non AF Des GP Total],
	[Last GP Amt],
	[Last GP Date],
	[Last AF Des GP Amt],
	[Last AF Des GP Date],
	[Num FYs gave large AF Designation])
VALUES  ( 
	@customer_no,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL 
)

DECLARE @RawData TABLE
(
	[reference_no] [INT] NOT NULL,
	[customer_number] [INT] NOT NULL,
	[name] [VARCHAR](55) NULL,
	[type] [CHAR](1) NULL,
	[creditee_type] [VARCHAR](20) NOT NULL,
	[date] [DATE] NULL,
	[fiscal_year] [INT] NOT NULL,
	[fiscal_quarter] [INT] NULL,
	[fiscal_period] [INT] NOT NULL,
	[contribution_amount] [MONEY] NULL,
	[received_amount] [MONEY] NULL,
	[anonymous] [VARCHAR](30) NULL,
	[soft_credit_type] [VARCHAR](255) NULL,
	[fund_description] [VARCHAR](30) NULL,
	[printable_fund] [VARCHAR](80) NULL,
	[designation] [VARCHAR](30) NULL,
	[overall_campaign] [VARCHAR](30) NULL,
	[campaign] [VARCHAR](30) NULL,
	[campaign_category] [VARCHAR](30) NULL,
	[account_goal] [VARCHAR](30) NULL,
	[account_group] [VARCHAR](30) NULL,
	[appeal] [VARCHAR](30) NOT NULL,
	[media] [VARCHAR](30) NULL,
	[source] [VARCHAR](50) NULL,
	[notes] [VARCHAR](MAX) NULL,
	[cancel] [CHAR](1) NULL,
	[pledge_status_desc] [VARCHAR](30) NULL,
	[billing_type] [VARCHAR](30) NULL,
	[batch] [INT] NULL,
	[KG_xfer_dt] [DATETIME] NULL,
	[KGift_desc] [VARCHAR](50) NULL,
	[stock_ticker] [VARCHAR](255) NULL,
	[contribution_detail1] [VARCHAR](30) NULL,
	[contribution_detail2] [VARCHAR](30) NULL,
	[agreement_date] [VARCHAR](255) NULL,
	[challenge_earned] [VARCHAR](255) NULL,
	[pg_instrument] [VARCHAR](30) NULL,
	[solicitation] [VARCHAR](255) NULL,
	[solicitor_number] [INT] NULL,
	[solicitor_name] [VARCHAR](55) NULL,
	[cpf_flag] [CHAR](1) NULL,
	[exh_prog_cat] [VARCHAR](30) NULL,
	[full_fund_name] [VARCHAR](80) NULL,
	[cm_category1] [VARCHAR](80) NULL,
	[cm_intermediate1] [VARCHAR](80) NULL,
	[cm_pillar1] [VARCHAR](80) NULL,
	[cm_category2] [VARCHAR](80) NULL,
	[cm_intermediate2] [VARCHAR](80) NULL,
	[cm_category3] [VARCHAR](80) NULL,
	[cm_intermediate3] [VARCHAR](80) NULL,
	[custom_2] [VARCHAR](255) NULL,
	[custom_9] [VARCHAR](255) NULL,
	[main_customer_type] [VARCHAR](30) NULL,
	[original_customer_type] [VARCHAR](30) NULL,
	[sort_name] [VARCHAR](55) NULL
)

-- This code was taken from LRP_MOS_ADVANCEMENT_CONTRIBUTION_DETAIL.
INSERT INTO @RawData
        (reference_no,
         customer_number,
         name,
         type,
         creditee_type,
         date,
         fiscal_year,
         fiscal_quarter,
         fiscal_period,
         contribution_amount,
         received_amount,
         anonymous,
         soft_credit_type,
         fund_description,
         printable_fund,
         designation,
         overall_campaign,
         campaign,
         campaign_category,
         account_goal,
         account_group,
         appeal,
         media,
         source,
         notes,
         cancel,
         pledge_status_desc,
         billing_type,
         batch,
         KG_xfer_dt,
         KGift_desc,
         stock_ticker,
         contribution_detail1,
         contribution_detail2,
         agreement_date,
         challenge_earned,
         pg_instrument,
         solicitation,
         solicitor_number,
         solicitor_name,
         cpf_flag,
         exh_prog_cat,
         full_fund_name,
         cm_category1,
         cm_intermediate1,
         cm_pillar1,
         cm_category2,
         cm_intermediate2,
         cm_category3,
         cm_intermediate3,
         custom_2,
         custom_9,
         main_customer_type,
         original_customer_type,
         sort_name)
SELECT	DISTINCT reference_no = c.ref_no,
			customer_number = c.customer_no,
			name = CASE WHEN cust.cust_type = 7 THEN cust.lname ELSE cs.esal1_desc END,
			type = c.cont_type,
			creditee_type = '',
			date = CONVERT(DATE,c.cont_dt),
			fiscal_year = bp.fyear,
			fiscal_quarter = bp.quarter,
			fiscal_period = bp.period,
			contribution_amount = c.cont_amt,
			received_amount = c.recd_amt,
			anonymous = kv.key_value,
			soft_credit_type = c.custom_1,
			fund_description = coa.fund_description,
			printable_fund = coa.printable_fund,
			designation = coa.designation,
			overall_campaign = coa.overall_cm,
			campaign = cm.description,
			campaign_category = coa.cm_category,
			account_goal = coa.acct_goal,
			account_group = coa.acct_grp,
			appeal = a.description,
			media = mt.description,
			source = amt.source_name,
			notes = REPLACE(REPLACE(c.notes,CHAR(13),'/'),CHAR(10),'/'),
			cancel = c.cancel, --they've requested we leave this out of the report, but I'm leaving it here for now in case they change their minds soon
			pledge_status_desc = ps.description,
			billing_type = bt.description,
			batch = c.batch_no,
			KG_xfer_dt = c.KG_xfer_dt,
			KGift_desc = c.KGift_desc,
			stock_ticker = c.custom_2,
			contribution_detail1 = kv3.key_value,
			contribution_detail2 = kv4.key_value,
			agreement_date = c.custom_6,
			challenge_earned = c.custom_7,
			pg_instrument = kv5.key_value,
			solicitation = c.custom_0,
			solicitor_number = c.worker_customer_no,
			solicitor_name = WCS.esal1_desc,
			cpf_flag = coa.cpf_flag,
			exh_prog_cat = coa.exh_prog_cat,
			full_fund_name = coa.full_fund,
			cm_category1 = coa.cm_category1,
			cm_intermediate1 = coa.cm1_intermediatelevel,
			cm_pillar1 = coa.cm1_main_level,
			cm_category2 = coa.cm_category2,
			cm_intermediate2 = coa.cm2_intermediate_level,
			cm_category3 = coa.cm_category3,
			cm_intermediate3 = coa.cm3_intermediate_level,
			custom_2 = c.custom_2,
			custom_9 = c.custom_9,
			main_customer_type = ct.description,
			original_customer_type = ct.description,
			sort_name = cust.sort_name
	FROM dbo.T_CONTRIBUTION AS c
	JOIN dbo.T_CUSTOMER AS cust ON c.customer_no = cust.customer_no
	JOIN dbo.TR_CUST_TYPE AS ct ON cust.cust_type = ct.id
	JOIN dbo.TX_CUST_SAL AS cs ON c.customer_no = cs.customer_no AND cs.default_ind = 'Y'
	LEFT JOIN TX_CUST_SAL WCS ON C.worker_customer_no = WCS.customer_no AND WCS.default_ind = 'Y' 
	LEFT JOIN dbo.T_KWCODED_VALUES AS kv ON c.custom_5 = kv.key_value AND kv.keyword_no IN (460)
	LEFT JOIN dbo.T_KWCODED_VALUES AS kv2 ON c.custom_1 = kv2.key_value AND kv2.keyword_no IN (456)
	JOIN dbo.TR_Batch_Period AS bp ON c.cont_dt BETWEEN bp.start_dt AND bp.end_dt
	JOIN dbo.T_CAMPAIGN AS cm ON c.campaign_no = cm.campaign_no
	JOIN dbo.LV_MOS_CHART_OF_ACCOUNTS AS coa ON c.fund_no = coa.fund_no AND cm.campaign_no = coa.campaign_no 
	JOIN dbo.T_APPEAL AS a ON c.appeal_no = a.appeal_no
	JOIN dbo.TR_MEDIA_TYPE AS mt ON c.media_type = mt.id
	JOIN dbo.TX_APPEAL_MEDIA_TYPE AS amt ON c.source_no = amt.source_no
	LEFT JOIN dbo.TR_BILLING_TYPE AS bt ON c.billing_type = bt.id
	LEFT JOIN dbo.T_KWCODED_VALUES AS kv3 ON c.custom_3 = kv3.key_value AND kv3.keyword_no IN (458)
	LEFT JOIN dbo.T_KWCODED_VALUES AS kv4 ON c.custom_4 = kv4.key_value AND kv4.keyword_no IN (459)
	LEFT JOIN dbo.T_KWCODED_VALUES AS kv5 ON c.custom_8 = kv5.key_value AND kv5.keyword_no IN (463)
	LEFT JOIN dbo.TX_CUST_SAL AS cs2 ON c.worker_customer_no = cs2.customer_no AND cs2.default_ind = 'Y'
	LEFT JOIN TR_PLEDGE_STATUS PS ON C.pledge_status = PS.id 
	WHERE ISNULL(c.custom_1,'(none)') IN ('(none)','Matching Gift Credit')
	AND c.cont_amt > 0 
	AND c.customer_no = @customer_no

	UNION ALL

	SELECT	DISTINCT reference_no = c.ref_no,
			customer_number = cr.creditee_no,
			name = CASE WHEN cust2.cust_type = 7 THEN cust2.lname ELSE cs.esal1_desc END,
			type = c.cont_type,
			creditee_type = crt.descriptiuon,
			date = CONVERT(DATE,c.cont_dt),
			fiscal_year = bp.fyear,
			fiscal_quarter = bp.quarter,
			fiscal_period = bp.period,
			contribution_amount = c.cont_amt,
			received_amount = c.recd_amt,
			anonymous = kv.key_value,
			soft_credit_type = c.custom_1,
			fund_description = coa.fund_description,
			printable_fund = coa.printable_fund,
			designation = coa.designation,
			overall_campaign = coa.overall_cm,
			campaign = cm.description,
			campaign_category = coa.cm_category,
			account_goal = coa.acct_goal,
			account_group = coa.acct_grp,
			appeal = a.description,
			media = mt.description,
			source = amt.source_name,
			notes = REPLACE(REPLACE(c.notes,CHAR(13),'/'),CHAR(10),'/'),
			cancel = c.cancel, --they've requested we leave this out of the report, but I'm leaving it here for now in case they change their minds soon
			pledge_status_desc = ps.description,
			billing_type = bt.description,
			batch = c.batch_no,
			KG_xfer_dt = c.KG_xfer_dt,
			KGift_desc = c.KGift_desc,
			stock_ticker = c.custom_2,
			contribution_detail1 = kv3.key_value,
			contribution_detail2 = kv4.key_value,
			agreement_date = c.custom_6,
			challenge_earned = c.custom_7,
			pg_instrument = kv5.key_value,
			solicitation = c.custom_0,
			solicitor_number = c.worker_customer_no,
			solicitor_name = WCS.esal1_desc,
			cpf_flag = coa.cpf_flag,
			exh_prog_cat = coa.exh_prog_cat,
			full_fund_name = coa.full_fund,
			cm_category1 = coa.cm_category1,
			cm_intermediate1 = coa.cm1_intermediatelevel,
			cm_pillar1 = coa.cm1_main_level,
			cm_category2 = coa.cm_category2,
			cm_intermediate2 = coa.cm2_intermediate_level,
			cm_category3 = coa.cm_category3,
			cm_intermediate3 = coa.cm3_intermediate_level,
			custom_2 = c.custom_2,
			custom_8 = c.custom_9,
			main_customer_type = ct2.description,
			original_customer_type = ct.description,
			sort_name = cust2.sort_name
	FROM dbo.T_CONTRIBUTION AS c
	JOIN dbo.T_CUSTOMER AS cust ON c.customer_no = cust.customer_no
	JOIN dbo.TR_CUST_TYPE AS ct ON cust.cust_type = ct.id
	LEFT JOIN TX_CUST_SAL WCS ON C.worker_customer_no = WCS.customer_no AND WCS.default_ind = 'Y' 
	JOIN dbo.T_CREDITEE AS cr ON c.ref_no = cr.ref_no
	JOIN dbo.TR_CREDITEE_TYPE AS crt ON cr.creditee_type = crt.id
	JOIN dbo.T_CUSTOMER AS cust2 ON cr.creditee_no = cust2.customer_no
	JOIN dbo.TR_CUST_TYPE AS ct2 ON cust2.cust_type = ct2.id
	JOIN dbo.TX_CUST_SAL AS cs ON cr.creditee_no = cs.customer_no AND cs.default_ind = 'Y'
	LEFT JOIN dbo.T_KWCODED_VALUES AS kv ON c.custom_5 = kv.key_value AND kv.keyword_no IN (460)
	LEFT JOIN dbo.T_KWCODED_VALUES AS kv2 ON c.custom_1 = kv2.key_value AND kv2.keyword_no IN (456)
	JOIN dbo.TR_Batch_Period AS bp ON c.cont_dt BETWEEN bp.start_dt AND bp.end_dt
	JOIN dbo.T_CAMPAIGN AS cm ON c.campaign_no = cm.campaign_no
	JOIN dbo.LV_MOS_CHART_OF_ACCOUNTS AS coa ON c.fund_no = coa.fund_no AND cm.campaign_no = coa.campaign_no 
	JOIN dbo.T_APPEAL AS a ON c.appeal_no = a.appeal_no
	JOIN dbo.TR_MEDIA_TYPE AS mt ON c.media_type = mt.id
	JOIN dbo.TX_APPEAL_MEDIA_TYPE AS amt ON c.source_no = amt.source_no
	LEFT JOIN dbo.TR_BILLING_TYPE AS bt ON c.billing_type = bt.id
	LEFT JOIN dbo.T_KWCODED_VALUES AS kv3 ON c.custom_3 = kv3.key_value AND kv3.keyword_no IN (458)
	LEFT JOIN dbo.T_KWCODED_VALUES AS kv4 ON c.custom_4 = kv4.key_value AND kv4.keyword_no IN (459)
	LEFT JOIN dbo.T_KWCODED_VALUES AS kv5 ON c.custom_8 = kv5.key_value AND kv5.keyword_no IN (463)
	LEFT JOIN dbo.TX_CUST_SAL AS cs2 ON c.worker_customer_no = cs2.customer_no AND cs2.default_ind = 'Y'
	LEFT JOIN TR_PLEDGE_STATUS PS ON C.pledge_status = PS.id 
	WHERE c.custom_1 IN ('Primary Soft Credit')
	AND cr.creditee_type IN (5,12,15)
	AND c.cont_amt > 0 
	AND cr.creditee_no = @customer_no

-- Current FY AF Des Plg Balance
UPDATE @resultsTbl
SET [Current FY AF Des Plg Balance] = total
FROM 
(
	SELECT customer_number, SUM(contribution_amount) AS total 
	FROM @RawData
	WHERE type = 'P'
	AND designation = 'Annual Fund'
	AND contribution_amount - received_amount > 0 
	AND fiscal_year = @currentFY
	GROUP BY customer_number
) X

--Current FY Non AF Des GP Total
UPDATE @resultsTbl
SET [Current FY Non AF Des GP Total] = total
FROM 
(
	SELECT customer_number, SUM(contribution_amount) AS total
	FROM @RawData
	WHERE fiscal_year = @currentFY
	AND designation <> 'Annual Fund'
	GROUP BY customer_number, fiscal_year, campaign_category
) X 

--Last GP Amt/Dt
UPDATE @resultsTbl
SET [Last GP Amt] = total, [Last GP Date] = date
FROM 
(
	SELECT customer_number, date, SUM(contribution_amount) AS total
	FROM @RawData
	WHERE date = (SELECT MAX(date) FROM @RawData WHERE date < GETDATE())
	GROUP BY customer_number, date 
) X

--Last AF Des GP Amt/Dt
UPDATE @resultsTbl
SET [Last AF Des GP Amt] = total , [Last AF Des GP Date] = date
FROM 
( 
	SELECT customer_number, date, SUM(contribution_amount) AS total
	FROM @RawData
	WHERE date = (SELECT MAX(date) FROM @RawData WHERE designation = 'Annual Fund' AND date < GETDATE())
		AND designation = 'Annual Fund'
	GROUP BY customer_number, date 
) X 

-- # FYs gave 2500+ AF Designation
UPDATE @resultsTbl
SET [Num FYs gave large AF Designation] = [# FYs gave 2500+ AF Designation] 
FROM 
( 
	SELECT COUNT(*) AS [# FYs gave 2500+ AF Designation] 
	FROM 
	(
		SELECT customer_number, fiscal_year, SUM(contribution_amount) AS total
		FROM @RawData
		GROUP BY customer_number, fiscal_year
		HAVING SUM(contribution_amount) >= 2500 
	) X 
) X1

SELECT * FROM @resultsTbl