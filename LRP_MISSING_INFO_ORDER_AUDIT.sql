USE [impresario];
GO

SET ANSI_NULLS ON;
GO

SET QUOTED_IDENTIFIER ON;

--GO
--DROP PROCEDURE [dbo].LRP_MISSING_INFO_ORDER_AUDIT;
GO

CREATE PROCEDURE [dbo].LRP_MISSING_INFO_ORDER_AUDIT
(
    @order_start_dt DATETIME,
    @order_end_dt DATETIME,
    @mode_of_sale VARCHAR(4000),
    @created_by CHAR(4000)
)
AS
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

-- =============================================
-- Author: Aileen Duffy-Brown
-- Create date: 5/29/2018
-- Description:	Used to audit missing info on 
-- ticket orders.  Specifically geared to Schools and Groups.
-- Included Potential CSI issue Numbers.  
-- TrackIt! Work Order # 90786
-- =============================================

BEGIN
    ----Check Parameters
    SELECT @order_start_dt = ISNULL(@order_start_dt, '1/1/1900');
    SELECT @order_end_dt = ISNULL(@order_end_dt, '12/31/2999');
    SELECT @mode_of_sale = ISNULL(@mode_of_sale, '');
    SELECT @created_by = ISNULL(@created_by, '');

 
    DECLARE @mos TABLE
    (
        mode_of_sale VARCHAR(30)
    );
    IF ISNULL(@mode_of_sale, '') <> ''
        INSERT INTO @mos
        SELECT CONVERT(VARCHAR(30), Element)
        FROM dbo.FT_SPLIT_LIST(REPLACE(@mode_of_sale, '"', ''), ',');

    DECLARE @user TABLE
    (
        userid CHAR(8)
    );
    IF ISNULL(@created_by, '') <> ''
        INSERT INTO @user
        SELECT CONVERT(CHAR(8), Element)
        FROM dbo.FT_SPLIT_LIST(REPLACE(@created_by, '"', ''), ',');

    SELECT ord.customer_no,
           cus.display_name AS constituentname,
           ord.order_no,
           ord.initiator_no,
           ISNULL(ini.display_name, '') AS initiatorname,
           ord.order_dt,
           ord.created_by,
           ord.notes,
           ord.custom_1 AS 'Field_1',
           ord.custom_2 AS 'Billing_Info',
           ord.custom_3 AS 'Arrival_Time',
           ord.custom_4 AS 'Departure_Time',
           ord.custom_5 AS 'Grade_Audience',
           ord.custom_6 AS 'Transportation',
           ord.custom_7 AS 'Lunch',
           ord.custom_8 AS 'PO#',
           ord.custom_9 AS 'Field_9',
           ord.custom_0 AS 'Lowest_Grade',
           ord.MOS,
           mos.description AS mode_of_sale,
           ord.notes,
           (usr.lname + ', ' + usr.fname) AS operator_name,
           usr.lname,
           usr.fname,
           usr.userid,
           csi.activity_no,
           csi.issue_dt
    FROM T_ORDER AS ord
        INNER JOIN dbo.TR_MOS AS mos
            ON ord.MOS = mos.id
        LEFT OUTER JOIN dbo.FT_CONSTITUENT_DISPLAY_NAME() cus
            ON ord.customer_no = cus.customer_no
        LEFT OUTER JOIN dbo.FT_CONSTITUENT_DISPLAY_NAME() ini
            ON ord.initiator_no = ini.customer_no
        LEFT OUTER JOIN dbo.T_METUSER AS usr
            ON usr.userid = ord.created_by
        LEFT OUTER JOIN T_CUST_ACTIVITY AS csi
            ON csi.customer_no = ord.customer_no
               OR csi.customer_no = ini.customer_no
                  AND csi.issue_dt >= ord.order_dt
    WHERE ord.order_dt
          --> '2017/10/01'
          --AND mos.description = 'Schools'
          BETWEEN @order_start_dt AND @order_end_dt
          AND
          (
              @mode_of_sale = 'ALL'
              OR mos.description IN
                 (
                     SELECT mode_of_sale FROM @mos
                 )
          )
          AND
          (
              @created_by = 'ALL'
              OR usr.userid IN
                 (
                     SELECT userid FROM @user
                 )
          );

END;

GO
GRANT EXECUTE ON [dbo].[LRP_MISSING_INFO_ORDER_AUDIT] TO ImpUsers;

GO

