USE [impresario]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[LRP_EventResearchNotesByList]
(
	@list_no INT = NULL
)
AS

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET NOCOUNT ON 

-- List code stolen from LP_SEATING_BOOK_RPT
-- DSJ 11/17/16

If @list_no > 0 AND NOT EXISTS (SELECT * FROM [dbo].VS_LIST WHERE list_no = @list_no)
BEGIN
	RAISERROR('Invalid List (list_no).', 11, 2) WITH SETERROR
	RETURN
END

SELECT c.customer_no, c.sort_name,
CASE c.cust_type 
	WHEN 7 THEN c.lname -- cust_type = Household
	ELSE sal.esal1_desc
END AS name, namStat.description AS NameStatus,
[dbo].[LFS_BOARD_GetResearchNotes](cust_notes_no) AS notes, cn.last_update_dt, cn.last_updated_by 
-- select * 
FROM T_LIST_CONTENTS lst
INNER JOIN dbo.V_CUSTOMER_WITH_PRIMARY_AFFILIATES hou
	ON hou.customer_no = lst.customer_no
	AND lst.list_no = @list_no
INNER JOIN dbo.T_CUSTOMER c
	ON c.customer_no = hou.expanded_customer_no
INNER JOIN dbo.TX_CUST_NOTES cn
	ON hou.expanded_customer_no = cn.customer_no
	AND cn.note_type = 3 -- Event Research Note
INNER JOIN dbo.TX_CUST_SAL sal
	ON c.customer_no = sal.customer_no
	AND sal.default_ind = 'Y'
INNER JOIN TR_NAMESTATUS AS namStat
ON c.name_status = namStat.id
ORDER BY c.sort_name



