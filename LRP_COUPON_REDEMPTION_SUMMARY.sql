USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_COUPON_REDEMPTION_SUMMARY]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LRP_COUPON_REDEMPTION_SUMMARY]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LRP_COUPON_REDEMPTION_SUMMARY]
        @report_start_dt datetime = Null, 
        @report_end_dt datetime = Null,
        @group_by varchar(20) = 'Title'       --Title, Production, or Performance
AS BEGIN

        /*  If null passed to either date parameter, set to yesterday.
            IF null passed to group by parameter, set to title  */

            SELECT @report_start_dt = IsNull(@report_start_dt, dateadd(day, -1, getdate()))
            SELECT @report_end_dt = IsNUll(@report_end_dt, dateadd(day, -1, getdate()))
            SELECT @group_by = IsNull(@group_by, 'Title')


    /*  Will be pulling performance data based on a text date field in the yyyy/MM/dd format.  
        Convert @report_start_date and @report_end_date now to make it easier. */

        DECLARE @start_date char(10), @end_date char(10)

        SELECT @start_date = convert(char(10), @report_start_dt, 111),
               @end_date = convert(char(10), @report_end_dt, 111)

       
    /*  The #final_table temporary table contains the combined information from #performance_table and #coupon_redemption_table.
        The data is aggregated so that there should be a single row for each performance/coupon code combination.  */

            IF OBJECT_ID('tempdb..#final_table') IS NOT NULL DROP TABLE [#final_table]

            CREATE TABLE #final_table ([report_message] varchar(100), [perf_no] int, [zone_no] int, [title_name] varchar(30), [performance_date] char(10), 
                                       [performance_time] varchar(8), [production_name_long] varchar(255), [price_type] int, [price_type_name] varchar(30), 
                                       [comp_code] int, [comp_code_name] varchar(30), [sale_admission] int, [scan_admission] int, [due_amt] decimal(18,2),
                                       [paid_amt] decimal(18,2))

            CREATE NONCLUSTERED INDEX [ix_final_table_title_name] ON #final_table ([title_name] ASC) ON [PRIMARY]

            CREATE NONCLUSTERED INDEX [ix_final_table_production_name_long] ON #final_table ([production_name_long] ASC) ON [PRIMARY]

        INSERT INTO #final_table
        SELECT '', [perf_no], [zone_no], [title_name], [performance_date], [performance_time], [production_name_long],
               [price_type], [price_type_name], [comp_code], [comp_code_name], sum([sale_total]), sum([scan_admission_total]), 
               sum([due_amt]), sum([paid_amt])
        FROM LT_HISTORY_TICKET
        WHERE performance_date between @start_date and @end_date and comp_code <> 0
        GROUP BY [order_no], [perf_no], [zone_no], [title_name], [performance_date], [performance_time], [production_name_long],
                 [price_type], [price_type_name], [comp_code], [comp_code_name]



        INSERT INTO #final_table
        SELECT '', [perf_no], [zone_no], [title_name], [performance_date], [performance_time], [production_name_long],
               [price_type], [price_type_name], 0, 'Electronic Member Pass', sum([sale_total]), sum([scan_admission_total]), 
               sum([due_amt]), sum([paid_amt])
        FROM LT_HISTORY_TICKET
        WHERE performance_date between @start_date and @end_date and price_type IN (SELECT [id] FROM [dbo].[TR_PRICE_TYPE] WHERE [price_type_group] = 2)
        GROUP BY [order_no], [perf_no], [zone_no], [title_name], [performance_date], [performance_time], [production_name_long],
                 [price_type], [price_type_name], [comp_code], [comp_code_name]


    DONE:

    /*  If not records found, add single record with message saying no records were found.  */
        
            IF not exists (SELECT * FROM #final_table)
                INSERT INTO #final_table VALUES ('No records found for criteria entered.', 0, 0, '', null, '', '', 0, '', 0, '', 0, 0, 0, 0)

    /*  Select final aggregated record set
        If grouping by performance, include performance date/time and title to produce a single record for each performance/discount code combination.
        If grouping by production, leave out date/time but leave in title to produce a single record for each production/discount code combination.
        If grouping by title (venue), leave out date/time and title to produce a single record for each title/discount code combination (default if group_by is not known).  */

        IF @group_by = 'Performance'

            SELECT [report_message], [perf_no], [zone_no], [title_name], convert(datetime,[performance_date]) as 'performance_dt', [performance_time], [production_name_long], 
                   [price_type], [price_type_name], [comp_code], [comp_code_name], sum([sale_admission]) as 'sale_total', sum([scan_admission]) as 'scan_admission_total', 
                   sum([paid_amt]) as 'paid_amount', @report_start_dt as 'p_report_start_dt', @report_end_dt as 'p_report_end_dt', @group_by as 'p_group_by'
            FROM #final_table
            GROUP BY [report_message], [perf_no], [zone_no], [title_name], [performance_date], [performance_time], [production_name_long], 
                     [price_type], [price_type_name], [comp_code], [comp_code_name]
            ORDER BY [title_name], [performance_date], [performance_time], [production_name_long], [comp_code_name]

        ELSE IF @group_by = 'Production'

            SELECT [report_message], 0 as 'perf_no', 0 as 'zone_no', [title_name], null as 'performance_dt', '' as 'performance_time', [production_name_long], 
                   [price_type], [price_type_name], [comp_code], [comp_code_name], sum([sale_admission]) as 'sale_total', sum([scan_admission]) as 'scan_admission_total', 
                   sum([paid_amt]) as 'paid_amount', @report_start_dt as 'p_report_start_dt', @report_end_dt as 'p_report_end_dt', @group_by as 'p_group_by'
            FROM #final_table
            GROUP BY [report_message], [title_name], [production_name_long], 
                     [price_type], [price_type_name], [comp_code], [comp_code_name]
            ORDER BY [title_name], [production_name_long], [comp_code_name]

        ELSE

            SELECT [report_message], 0 as 'perf_no', 0 as 'zone_no', [title_name], null as 'performance_dt', '' as 'performance_time', convert(varchar(255),'') as 'production_name_long', 
                   [price_type], [price_type_name], [comp_code], [comp_code_name], sum([sale_admission]) as 'sale_total', sum([scan_admission]) as 'scan_admission_total', 
                   sum([paid_amt]) as 'paid_amount', @report_start_dt as 'p_report_start_dt', @report_end_dt as 'p_report_end_dt', @group_by as 'p_group_by'
            FROM #final_table
            GROUP BY [report_message], [title_name], [price_type], [price_type_name], [comp_code], [comp_code_name]
            ORDER BY [title_name], [comp_code_name]
    

    /*  Destroy temporary tables  */

    IF OBJECT_ID('tempdb..#final_table') IS NOT NULL DROP TABLE [#final_table]

END
GO

GRANT EXECUTE ON [dbo].[LRP_COUPON_REDEMPTION_SUMMARY] TO impusers
GO

--EXECUTE [dbo].[LRP_COUPON_REDEMPTION_SUMMARY] '6-1-2016', '6-30-2016', 'Title'
