USE impresario;
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_ATTEND_BUDGET_VS_ACTUAL_WITH_INCREMENTAL]') AND type in (N'P', N'PC')) BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_ATTEND_BUDGET_VS_ACTUAL_WITH_INCREMENTAL] AS';
    EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LRP_ATTEND_BUDGET_VS_ACTUAL_WITH_INCREMENTAL] TO impusers';
END;
GO

ALTER PROCEDURE [dbo].[LRP_ATTEND_BUDGET_VS_ACTUAL_WITH_INCREMENTAL]
        @week_ending DATETIME = NULL
AS BEGIN

   SET NOCOUNT ON;
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

    /*  Procedure Variables  */

        DECLARE @fiscal_year INT = 0;
        DECLARE @week_start_dt DATETIME; 
        DECLARE @week_end_dt DATETIME;
        DECLARE @year_start_dt DATETIME;
        DECLARE @year_end_dt DATETIME;

        DECLARE @prev_fiscal_year INT = 0;
        DECLARE @prev_week_start_dt DATETIME;
        DECLARE @prev_week_end_dt DATETIME; 
        DECLARE @prev_year_start_dt DATETIME;
        DECLARE @prev_year_end_dt DATETIME;

        DECLARE @month_start_dt DATETIME,
                @month_end_dt DATETIME,
                @prev_month_start_dt DATETIME,
                @prev_month_end_dt DATETIME;
   
        DECLARE @gross_attend_no INT = 0, 
                @incremental_attend_no INT = -1;

        DECLARE @num_of_days INT = 0,
                @total_days INT = 0;

        DECLARE @full_month TINYINT = 0

        DECLARE @month_data TABLE ([month_id] CHAR(7) NOT NULL DEFAULT (''), 
                                   [month_base] INT NOT NULL DEFAULT (0), 
                                   [month_incr] INT NOT NULL DEFAULT (0),
                                   [month_total] INT NOT NULL DEFAULT (0),
                                   [month_actual_base] INT NOT NULL DEFAULT (0),
                                   [month_actual_incr] INT NOT NULL DEFAULT (0),
                                   [month_actual_total] INT NOT NULL DEFAULT (0),
                                   [prev_month_base] INT NOT NULL DEFAULT (0), 
                                   [prev_month_incr] INT NOT NULL DEFAULT (0),
                                   [prev_month_total] INT NOT NULL DEFAULT (0),
                                   [prev_month_actual_base] INT NOT NULL DEFAULT (0),
                                   [prev_month_actual_incr] INT NOT NULL DEFAULT (0),
                                   [prev_month_actual_total] INT NOT NULL DEFAULT (0))

        DECLARE @mtd_final_table TABLE ([attendance_category] VARCHAR(30) NOT NULL DEFAULT (''),
                                        [attendance_category_sort] CHAR(3) NOT NULL DEFAULT ('ZZZ'),
                                        [fiscal_year] INT NOT NULL DEFAULT (0),
                                        [mtd_budget] INT NOT NULL DEFAULT (0),
                                        [mtd_actual] INT NOT NULL DEFAULT (0),
                                        [mtd_variance] INT NOT NULL DEFAULT (0),
                                        [mtd_percentage] DECIMAL(18,4) NOT NULL DEFAULT (0.0),
                                        [prev_fiscal_year] INT NOT NULL DEFAULT (0),
                                        [prev_mtd_budget] INT NOT NULL DEFAULT (0),
                                        [prev_mtd_actual] INT NOT NULL DEFAULT (0),
                                        [prev_mtd_variance] INT NOT NULL DEFAULT (0),
                                        [prev_mtd_percentage] DECIMAL(18,4) NOT NULL DEFAULT (0.0),
                                        [number_of_days] INT NOT NULL DEFAULT(0),
                                        [total_days] INT NOT NULL DEFAULT (0))

    /*  Check Parameters  */

        SELECT @week_ending = CAST(ISNULL(@week_ending,GETDATE()) AS DATE);

    /*  Get date values from LFT_ATTENDANCE_WEEK_DATES function  */
        
        SELECT @fiscal_year = [cur_fiscal_year], 
               @week_start_dt = [cur_week_start_dt], 
               @week_end_dt = [cur_week_end_dt],
               @month_start_dt = [cur_month_start_dt],
               @month_end_dt = [cur_month_end_dt],
               @year_start_dt = [cur_fiscal_start_dt], 
               @year_end_dt = [cur_fiscal_end_dt], 
               @prev_fiscal_year = [prv_fiscal], 
               @prev_week_start_dt = [prv_week_start_dt], 
               @prev_week_end_dt = [prv_week_end_dt], 
               @prev_year_start_dt = [prv_fiscal_start_dt],
               @prev_year_end_dt = [prv_fiscal_end_dt]
        FROM [dbo].[LFT_ATTENDANCE_WEEK_DATES] (@week_ending, 0, 'N');
        

    /*  Set the month start and date to be the first and last day of the week ending date  */

        SELECT @month_start_dt = FORMAT(@week_end_dt,'yyyy/MM/01')
        SELECT @month_end_dt = EOMONTH(@month_start_dt)

        SELECT @prev_month_start_dt = FORMAT(@prev_week_end_dt,'yyyy/MM/01')
        SELECT @prev_month_end_dt = EOMONTH(@prev_month_start_dt)

    /*  Get number of days between start of fiscal year and run date of the report  */

        SELECT @num_of_days = DATEPART(DAY, @week_end_dt);
        SELECT @total_days = DATEPART(DAY, EOMONTH(@week_end_dt));

        IF CAST(@week_end_dt AS DATE) = CAST(@month_end_dt AS DATE) SELECT @full_month = 1;

    /*  Get Month Data  */
    
        WITH CTE_VISIT_COUNT ([month_id], [visit_count])
        AS (SELECT FORMAT(CAST([history_date] AS date),'yyyy/MM'),
                   SUM(ISNULL([visit_count],0))
            FROM [dbo].[LT_HISTORY_VISIT_COUNT]
            WHERE CAST([history_date] AS DATE) BETWEEN @month_start_dt AND @week_end_dt
            GROUP BY FORMAT(CAST([history_date] AS date),'yyyy/MM'))
        INSERT INTO @month_data ([month_id], [month_base], [month_incr], [month_total], [month_actual_base], [month_actual_incr], [month_actual_total])
        SELECT FORMAT(att.[perf_dt],'yyyy/MM'),
               SUM((att.[budget] - att.[incremental])),
               SUM(att.[incremental]),
               SUM(att.[budget]),
               vis.[visit_count],
               0,
               vis.[visit_count]
        FROM [dbo].[LTR_ATTENDANCE_BUDGET_DATA] AS att
             INNER JOIN [CTE_VISIT_COUNT] AS vis ON vis.[month_id] = FORMAT(att.[perf_dt],'yyyy/MM')
        WHERE att.[perf_dt] BETWEEN @month_start_dt AND @week_end_dt
          AND att.[title_no] = @gross_attend_no
        GROUP BY FORMAT(att.[perf_dt],'yyyy/MM'), vis.[visit_count]

        UPDATE @month_data
        SET [month_actual_base] = [month_base],
            [month_actual_incr] = ([month_actual_base] - [month_base])
        WHERE [month_actual_base] > [month_base] AND [month_incr] > 0;

        WITH CTE_VISIT_COUNT ([month_id], [visit_count])
        AS (SELECT FORMAT(CAST([history_date] AS date),'yyyy/MM'),
                   SUM(ISNULL([visit_count],0))
            FROM [dbo].[LT_HISTORY_VISIT_COUNT]
            WHERE CAST([history_date] AS DATE) BETWEEN @prev_month_start_dt AND @prev_week_end_dt
            GROUP BY FORMAT(CAST([history_date] AS date),'yyyy/MM'))
        INSERT INTO @month_data ([month_id], [prev_month_base], [prev_month_incr], [prev_month_total], [prev_month_actual_base], [prev_month_actual_incr], [prev_month_actual_total])
        SELECT FORMAT(att.[perf_dt],'yyyy/MM'),
               SUM((att.[budget] - att.[incremental])),
               SUM(att.[incremental]),
               SUM(att.[budget]),
               vis.[visit_count],
               0,
               vis.[visit_count]
        FROM [dbo].[LTR_ATTENDANCE_BUDGET_DATA] AS att
             INNER JOIN [CTE_VISIT_COUNT] AS vis ON vis.[month_id] = FORMAT(att.[perf_dt],'yyyy/MM')
        WHERE att.[perf_dt] BETWEEN @prev_month_start_dt AND @prev_week_end_dt
          AND att.[title_no] = @gross_attend_no
        GROUP BY FORMAT(att.[perf_dt],'yyyy/MM'), vis.[visit_count]

        UPDATE @month_data
        SET [prev_month_actual_base] = [prev_month_base],
            [prev_month_actual_incr] = ([prev_month_actual_base] - [prev_month_base])
        WHERE [prev_month_actual_base] > [prev_month_base] AND [prev_month_incr] > 0

    /*  Create Final Aggregated Data  */

        --Base Attendance
        INSERT INTO @mtd_final_table ([attendance_category], [attendance_category_sort], [fiscal_year],[mtd_budget],[mtd_actual],[mtd_variance],[mtd_percentage],
                                      [prev_fiscal_year], [prev_mtd_budget],[prev_mtd_actual],[prev_mtd_variance],[prev_mtd_percentage], [number_of_days], [total_days])
        SELECT  'Museum Base', 
                'AAA',
                @fiscal_year,
                SUM(month_base), 
                SUM(month_actual_base), 
                (SUM(month_actual_base) - SUM(month_base)), 
                0.0000,
                @prev_fiscal_year,
                SUM(prev_month_base), 
                SUM(prev_month_actual_base), 
                (SUM(month_actual_base) - SUM(prev_month_actual_base)),
                0.0000,
                @num_of_days,
                @total_days
        FROM @month_data
        
        --Incremental Attendance
        INSERT INTO @mtd_final_table ([attendance_category], [attendance_category_sort], [fiscal_year],[mtd_budget],[mtd_actual],[mtd_variance],[mtd_percentage],
                                      [prev_fiscal_year], [prev_mtd_budget],[prev_mtd_actual],[prev_mtd_variance],[prev_mtd_percentage])
        SELECT  'Museum Incremental', 
                'BBB',
                @fiscal_year,
                SUM([month_incr]), 
                SUM([month_actual_incr]), 
                (SUM(month_actual_incr) - SUM(month_incr)), 
                CASE WHEN SUM([month_incr]) = 0 THEN 0.0000
                     ELSE ((CAST(SUM(month_actual_incr) AS decimal(18,2)) - CAST(SUM(month_incr) AS DECIMAL(18,4))) / CAST(SUM(month_incr) AS DECIMAL(18,4))) END,
                @prev_fiscal_year,
                SUM(prev_month_incr), 
                SUM(prev_month_actual_incr), 
                (SUM(prev_month_actual_incr) - SUM(prev_month_incr)),
                CASE WHEN SUM([prev_month_incr]) = 0 THEN 0.0000
                     ELSE ((CAST(SUM(prev_month_actual_incr) AS decimal(18,2)) - CAST(SUM(prev_month_incr) AS DECIMAL(18,4))) / CAST(SUM(prev_month_incr) AS DECIMAL(18,4))) END
        FROM @month_data

        --Total Attendance
        INSERT INTO @mtd_final_table ([attendance_category], [attendance_category_sort], [fiscal_year],[mtd_budget],[mtd_actual],[mtd_variance],[mtd_percentage],
                                      [prev_fiscal_year], [prev_mtd_budget],[prev_mtd_actual],[prev_mtd_variance],[prev_mtd_percentage])
        SELECT  'Total Gross Gate Attendance', 
                'CCC',
                @fiscal_year,
                SUM([month_total]), 
                SUM(month_actual_total), 
                (SUM(month_actual_total) - SUM(month_total)), 
                CASE WHEN SUM([month_total]) = 0 THEN 0.0000
                     ELSE ((CAST(SUM([month_actual_total]) AS decimal(18,4)) - CAST(SUM([month_total]) AS DECIMAL(18,4))) / CAST(SUM([month_total]) AS DECIMAL(18,4))) END,
                @prev_fiscal_year,
                SUM(prev_month_total), 
                SUM(prev_month_actual_total), 
                (SUM(month_actual_total) - SUM(prev_month_actual_total)),
                CASE WHEN SUM([prev_month_total]) = 0 THEN 0.0000
                     ELSE ((CAST(SUM(prev_month_actual_total) AS decimal(18,2)) - CAST(SUM([prev_month_total]) AS DECIMAL(18,4))) / CAST(SUM([prev_month_total]) AS DECIMAL(18,4))) END
        FROM @month_data

        UPDATE @mtd_final_table
        SET [mtd_percentage] = CASE WHEN [mtd_budget] = 0 AND [mtd_actual] = 0 THEN 0.000
                                    WHEN [mtd_budget] > [mtd_actual] THEN CAST([mtd_variance] AS DECIMAL(18,4)) / CAST([mtd_budget] AS DECIMAL(18,4))
                                    ELSE CAST([mtd_variance] AS DECIMAL(18,4)) / CAST([mtd_actual] AS DECIMAL(18,4)) END,
            [prev_mtd_percentage] = CASE WHEN [mtd_actual] = 0 AND [prev_mtd_actual] = 0 THEN 0.0000
                                         WHEN [mtd_actual] > [prev_mtd_actual] THEN CAST([prev_mtd_variance] AS DECIMAL(18,4)) / CAST([mtd_actual] AS DECIMAL(18,4))
                                         ELSE CAST([prev_mtd_variance] AS DECIMAL(18,4)) / CAST([prev_mtd_actual] AS DECIMAL(18,4)) END

    FINISHED:

        /*  Final data set is a single row per category  */

            SELECT  [attendance_category_sort],
                    [attendance_category],
                    [fiscal_year],
                    [mtd_budget],
                    [mtd_actual],
                    [mtd_variance],
                    [mtd_percentage],
                    [prev_fiscal_year],
                    [prev_mtd_budget],
                    [prev_mtd_actual],
                    [prev_mtd_variance],
                    [prev_mtd_percentage],
                    [number_of_days],
                    [total_days],
                    @full_month AS [full_month],
                    @month_start_dt AS [start_dt],
                    @week_end_dt AS [end_dt],
                    @prev_month_start_dt AS [prev_start_dt],
                    @prev_week_end_dt AS [prev_end_dt]
            FROM @mtd_final_table
            ORDER BY [attendance_category_sort]

            
    DONE:

        IF OBJECT_ID('tempdb..#attendance_history_data') IS NOT NULL DROP TABLE [#attendance_history_data];
        IF OBJECT_ID('tempdb..#budget_table') IS NOT NULL DROP TABLE [#budget_table];

END
GO

--EXECUTE [dbo].[LRP_ATTEND_BUDGET_VS_ACTUAL_WITH_INCREMENTAL] @week_ending = '10-31-2019'
--EXECUTE [dbo].[LRP_ATTEND_BUDGET_VS_ACTUAL_WITH_INCREMENTAL] @week_ending = '11-3-2019'

--DISTINCT title_no, title_group
--SELECT * FROM [dbo].[LTR_ATTENDANCE_BUDGET_DATA] WHERE perf_dt BETWEEN '9-3-2019' AND '9-8-2019' OR perf_dt BETWEEN '9-4-2018' AND '9-9-2018'

--BEGIN TRAN
--    DELETE FROM [dbo].[LTR_ATTENDANCE_BUDGET_DATA] WHERE perf_dt BETWEEN '9-3-2019' AND '9-8-2019'
--    PRINT @@ROWCOUNT
--COMMIT TRAN
  
  --INSERT INTO [dbo].[LTR_ATTENDANCE_BUDGET_DATA] ([perf_dt],[perf_day],[title_group],[title_no],[budget],[inactive])
  --VALUES ('9-3-2019', 'Tuesday', 'Exhibits', 37, 0, 'N')


--  SELECT * FROM dbo.T_INVENTORY WHERE type = 'T'

--SELECT SUM(budget) FROM [dbo].[LTR_ATTENDANCE_BUDGET_DATA] WHERE perf_dt BETWEEN '9-1-2019' AND '9-30-2019' AND title_no = 0
--SELECT SUM(budget) FROM [dbo].[LTR_ATTENDANCE_BUDGET_DATA] WHERE perf_dt BETWEEN '9-1-2019' AND '9-8-2019' AND title_no = 0
