USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LRP_MEMB_SCREEN_ACCT_KEYWORD]    Script Date: 10/3/2017 11:02:24 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Jonathan Smillie, Tessitura Technical Services 
-- Create date: 2017-04-20
-- Description:	Selects value of keyword for account number designated on web as one-step card 
-- 04/20/2017 - Added code to select keyword values from keyword 
-- =============================================
CREATE PROCEDURE [dbo].[LRP_MEMB_SCREEN_ACCT_KEYWORD]
					@keyword_no INT 
					,@customer_no INT 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET @keyword_no = 417 -- value for keyword for account token as of April 2017 
DECLARE @get_keywords TABLE (kw_customer_no INT, account_kw INT NULL) 
	INSERT @get_keywords 
		SELECT a.customer_no,a.key_value
			FROM tx_cust_keyword a WHERE keyword_no = @keyword_no 
			ORDER BY a.customer_no
   SELECT kw_customer_no, account_kw FROM @get_keywords 
   WHERE kw_customer_no = @customer_no 
   END

GO


