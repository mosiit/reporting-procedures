USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_TESSITURA_CASHOUT_PAYMENT_REVIEW]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LRP_TESSITURA_CASHOUT_PAYMENT_REVIEW]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LRP_TESSITURA_CASHOUT_PAYMENT_REVIEW]
        @report_dt datetime,
        @report_operator varchar(10),
        @machine_locations VARCHAR(4000) = ''
AS BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

    /*  Procedure Variables  */

        DECLARE @report_end_dt datetime
        DECLARE @one_step_table table ([one_step_count] INT, [membership_create_location] varchar(30))
        DECLARE @location_list TABLE ([location_no_str] VARCHAR(20), [location_no] INT)
        DECLARE @machine_list TABLE ([machine_name] VARCHAR(50))
        DECLARE @pay_table table ([payment_no] int, [sequence_no] int, [trx_no] int, [customer_no] int, [batch_no] int, [order_no] int, [payment_dt] datetime, 
                                  [payment_date] char(10), [payment_time] varchar(8), [payment_operator] varchar(10), [payment_operator_location] varchar(30),
                                  [payment_method_name] varchar(30), [payment_type_name] varchar(30), [payment_amount] decimal(18,2), [trx_machine] VARCHAR(50))



    /*  Check Parameters  - Null Date = Today, Null Operator = All  */

        SELECT @report_dt = ISNULL(@report_dt, GETDATE())
        SELECT @report_operator = ISNULL(@report_operator,'')
        SELECT @machine_locations = ISNULL(@machine_locations,'')

        SELECT @report_dt = convert(char(10),@report_dt,111) + ' 00:00:00',
               @report_end_dt = convert(char(10),@report_dt,111) + ' 23:59:59.957'

          
    /*  Parse machine_locations parameter  */

        INSERT INTO @location_list ([location_no_str]) 
        SELECT [item] FROM [dbo].[LFT_SPLIT_DILEMETED_STRING] (@machine_locations, ',')

        DELETE FROM @location_list WHERE [dbo].[FS_isReallyNumeric] ([location_no_str]) = 0

        UPDATE @location_list SET [location_no] = CONVERT(INT,[location_no_str])

        INSERT INTO @machine_list ([machine_name])
        SELECT [machine_name] FROM [dbo].[TX_MACHINE_LOCATION] WHERE [location] IN (SELECT [location_no] FROM @location_list)
        
    /*  Get Payment Data */

        INSERT INTO @pay_table
        SELECT [payment_no], [sequence_no], [transaction_no], [customer_no], [batch_no], 0, [payment_dt], [payment_date], [payment_time], 
               [payment_operator], [payment_operator_location], [payment_method_name], [payment_type_name], [payment_amount], [transaction_machine]
        FROM [dbo].[LV_RPT_CASHOUT_PAYMENTS] 
        WHERE [payment_dt] between @report_dt and @report_end_dt and [payment_type_name] = 'On Account'

        INSERT INTO @pay_table
        SELECT [payment_no], [sequence_no], [transaction_no], [customer_no], [batch_no], 0, [payment_dt], [payment_date], [payment_time], 
               [payment_operator], [payment_operator_location], [payment_method_name], [payment_type_name], [payment_amount], [transaction_machine]
        FROM [dbo].[LV_RPT_CASHOUT_PAYMENTS] 
        WHERE [payment_dt] between @report_dt and @report_end_dt and [payment_type_name] = 'Invoice'

        INSERT INTO @pay_table
        SELECT [payment_no], [sequence_no], [transaction_no], [customer_no], [batch_no], 0, [payment_dt], [payment_date], [payment_time], 
               [payment_operator], [payment_operator_location], [payment_method_name], [payment_type_name], [payment_amount], [transaction_machine]
        FROM [dbo].[LV_RPT_CASHOUT_PAYMENTS] 
        WHERE [payment_dt] between @report_dt and @report_end_dt and [payment_method_name] = 'Interdepartmental Transfer'
        
    /*  Delete unneeded records  */    

        IF @report_operator <> '' DELETE FROM @pay_table WHERE [payment_operator] <> @report_operator

        IF EXISTS (SELECT * FROM @machine_list)
            DELETE FROM @pay_table WHERE [trx_machine] NOT IN (SELECT [machine_name] FROM @machine_list)

    /*  Update order number  */

        UPDATE @pay_table 
        SET [order_no] = (SELECT IsNull(min(trx.[order_no]),0) FROM [dbo].[T_TRANSACTION] as trx WHERE trx.transaction_no = trx_no)

        FINISHED:

            SELECT [payment_no], [sequence_no], [trx_no], [customer_no], [batch_no], [order_no], [payment_dt], [payment_date], [payment_time], 
                   [payment_operator], [payment_operator_location], [payment_method_name], [payment_type_name], [payment_amount]
            FROM @pay_table

END
GO

GRANT EXECUTE ON [dbo].[LRP_TESSITURA_CASHOUT_PAYMENT_REVIEW] TO impusers
GO

    --EXECUTE [dbo].[LRP_TESSITURA_CASHOUT_PAYMENT_REVIEW]  '9-17-2016', 'cmonag00'
    EXECUTE [dbo].[LRP_TESSITURA_CASHOUT_PAYMENT_REVIEW]  '10-11-2016', 'jlobat00', '15'

    --SELECT * FROM T_TRANSACTION WHERE order_no = 481467
    --SELECT DISTINCT payment_type_name, payment_method_name FROM LV_RPT_CASHOUT_PAYMENTS WHERE payment_date >= '2016/09/01' ORDER BY payment_type_name, payment_method_name -- and payment_type_name = 'On Account'
    
     --SELECT * FROM LV_RPT_CASHOUT_PAYMENTS WHERE payment_date >= '2016/09/01' and payment_operator in (SELECT userid from T_METUSER WHERE location = 'Box Office') and payment_method_name = 'Interdepartmental Transfer'

     --SELECT @report_dt = '9-17-2016', @report_operator = 'cmonag00'
     --SELECT * FROM TR_LOCATION


