--USE [impresario]
--GO

--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON

--DROP PROCEDURE [dbo].[LRP_TP_ORDER_AUDIT];

GO
CREATE PROCEDURE [dbo].[LRP_TP_ORDER_AUDIT]
(
    @perf_start_dt DATETIME,
    @perf_end_dt DATETIME,
    @mos_desc VARCHAR(30)
)
AS
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

-- =============================================
-- Author: Aileen Duffy-Brown
-- Create date: 3/27/2018
-- Description:	Used to audit missing info on 
-- ticket orders for Traveling Programs.  Must be 
-- exportable to Excel or CSV  TrackIt! Work Order # 89913
-- 4/4/2018 changing date parameters from order date 
-- to performance date per Matt Pacewicz.
-- =============================================

BEGIN
    DECLARE @order_details_prog TABLE
    (
        customer_no INT,
        order_no INT,
        sli_no INT,
        sli_status SMALLINT,
        perf_code VARCHAR(10),
        perf_date DATETIME,
        price_type SMALLINT,
        price_type_desc VARCHAR(30),
        zone_no INT,
        zone VARCHAR(30),
        seat_no INT
    );

    INSERT INTO @order_details_prog
    (
        customer_no,
        order_no,
        sli_no,
        sli_status,
        perf_code,
        perf_date,
        price_type,
        price_type_desc,
        zone_no,
        zone,
        seat_no
    )
    SELECT ord.customer_no,
           ord.order_no,
           sli.sli_no,
           sli.sli_status,
           per.perf_code,
           per.perf_dt,
           sli.price_type,
           pri.description AS price_type_desc,
           sli.zone_no,
           zon.description AS zone,
           sli.seat_no
    FROM T_ORDER AS ord
        INNER JOIN dbo.T_SUB_LINEITEM AS sli
            ON ord.order_no = sli.order_no
        LEFT OUTER JOIN dbo.T_PERF AS per
            ON sli.perf_no = per.perf_no
        LEFT OUTER JOIN dbo.TR_PRICE_TYPE AS pri
            ON sli.price_type = pri.id
        LEFT OUTER JOIN dbo.T_ZONE AS zon
            ON sli.zone_no = zon.zone_no
    WHERE per.perf_dt
          BETWEEN @perf_start_dt AND @perf_end_dt
          AND sli.sli_status IN ( 3, 12, 2, 6 ) -- Seated, Paid, Ticketed, Unpaid, unseated_paid
          AND sli.zone_no = 92;

    --SELECT * FROM @order_details_prog;

    DECLARE @order TABLE
    (
        customer_no INT,
        customer_name VARCHAR(100),
        order_no INT,
        notes VARCHAR(255),
        grade_audience VARCHAR(255),
        tot_due_amt MONEY,
        tot_fee_amt MONEY,
        initiator_no INT,
        initiator_name VARCHAR(100),
        mos INT,
        mode_of_sale VARCHAR(30)
    );

    INSERT INTO @order
    (
        customer_no,
        customer_name,
        order_no,
        notes,
        grade_audience,
        tot_due_amt,
        tot_fee_amt,
        initiator_no,
        initiator_name,
        mos,
        mode_of_sale
    )
    SELECT ord.customer_no,
           cus.display_name AS constituentname,
           ord.order_no,
           ord.notes,
           ord.custom_5,
           ord.tot_due_amt,
           ord.tot_fee_amt,
           ord.initiator_no,
           ISNULL(ini.display_name, '') AS initiatorname,
           ord.MOS,
           mos.description AS mode_of_sale
    FROM dbo.T_ORDER AS ord
        INNER JOIN dbo.TR_MOS AS mos
            ON ord.MOS = mos.id
        LEFT OUTER JOIN dbo.FT_CONSTITUENT_DISPLAY_NAME() cus
            ON ord.customer_no = cus.customer_no
        LEFT OUTER JOIN dbo.FT_CONSTITUENT_DISPLAY_NAME() ini
            ON ord.initiator_no = ini.customer_no
    WHERE ord.order_no IN
          (
              SELECT order_no FROM @order_details_prog
          )
          AND mos.description = @mos_desc;

    --SELECT * FROM @order;

    DECLARE @order_details_mileage TABLE
    (
        customer_no INT,
        order_no INT,
        sli_no INT,
        sli_status SMALLINT,
        perf_code VARCHAR(10),
        perf_dt DATETIME,
        price_type SMALLINT,
        price_type_desc VARCHAR(30),
        zone_no INT,
        zone VARCHAR(30),
        seat_no INT
    );

    INSERT INTO @order_details_mileage
    (
        customer_no,
        order_no,
        sli_no,
        sli_status,
        perf_code,
        perf_dt,
        price_type,
        price_type_desc,
        zone_no,
        zone,
        seat_no
    )
    SELECT ord.customer_no,
           ord.order_no,
           sli.sli_no,
           sli.sli_status,
           per.perf_code,
           per.perf_dt,
           sli.price_type,
           pri.description AS price_type_desc,
           sli.zone_no,
           zon.description AS zone,
           sli.seat_no
    FROM T_ORDER AS ord
        INNER JOIN dbo.T_SUB_LINEITEM AS sli
            ON ord.order_no = sli.order_no
        LEFT OUTER JOIN dbo.T_PERF AS per
            ON sli.perf_no = per.perf_no
        LEFT OUTER JOIN dbo.TR_PRICE_TYPE AS pri
            ON sli.price_type = pri.id
        LEFT OUTER JOIN dbo.T_ZONE AS zon
            ON sli.zone_no = zon.zone_no
    WHERE sli.order_no IN
          (
              SELECT order_no FROM @order
          )
          AND sli.sli_status IN ( 3, 12, 2, 6 ) -- Seated, Paid, Ticketed, Unpaid, unseated_paid
          AND sli.zone_no = 103;

    --SELECT * FROM @order_details_mileage;

    DECLARE @scholarship_payment TABLE
    (
        order_no INT,
        pay_amt MONEY,
        funder_no INT,
        funding_account VARCHAR(30)
    );

    INSERT INTO @scholarship_payment
    (
        order_no,
        pay_amt,
        funder_no,
        funding_account
    )
    SELECT ord.order_no,
           SUM(pay.pmt_amt),
           pay.check_no,
           met.description AS funding_account
    FROM dbo.T_PAYMENT AS pay
        JOIN T_ORDER AS ord
            ON pay.transaction_no = ord.transaction_no
        JOIN dbo.TR_PAYMENT_METHOD AS met
            ON pay.pmt_method = met.id
    WHERE ord.order_no IN
          (
              SELECT order_no FROM @order
          )
          AND met.description LIKE 'Scholarships%'
          AND pay.pmt_amt > 0
    GROUP BY ord.order_no,
             pay.check_no,
             met.description;


    --SELECT * FROM @scholarship_payment;

    SELECT trx.order_no AS 'Order no',
           trx.notes AS 'Order Notes',
           prg.perf_code AS 'Performance Code',
           FORMAT(prg.perf_date, 'd', 'en-us') AS 'Performance Date',
           prg.seat_no AS 'Program Seat no',
           mil.seat_no AS 'Mileage Seat no',
           trx.grade_audience AS 'Grade/Audience',
           prg.price_type_desc AS 'Program Price Type',
           mil.price_type_desc AS 'Mileage Price Type',
           trx.tot_fee_amt AS 'Fees',
           trx.tot_due_amt AS 'Order Total Amount',
           pay.pay_amt AS 'Scholarship Payment Amount',
           pay.funder_no AS 'Scholarship Payment Constituent no',
           trx.customer_no AS 'Owner Constituent_no',
           trx.customer_name AS 'Owner LastName',
           trx.initiator_name AS 'Initiator Name'
    FROM @order AS trx
        INNER JOIN @order_details_prog AS prg
            ON prg.order_no = trx.order_no
        INNER JOIN @order_details_mileage AS mil
            ON mil.order_no = trx.order_no
        LEFT OUTER JOIN @scholarship_payment AS pay
            ON pay.order_no = trx.order_no;

END;

GO
GRANT EXECUTE ON [dbo].[LRP_TP_ORDER_AUDIT] TO ImpUsers;





