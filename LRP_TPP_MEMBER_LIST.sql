USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_TPP_MEMBER_LIST]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LRP_TPP_MEMBER_LIST] AS' 
END
GO

ALTER PROCEDURE [dbo].[LRP_TPP_MEMBER_LIST]
        @active_only CHAR(1) = 'N'
AS BEGIN

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    SET NOCOUNT ON;

    /*  Procedure Variables  */

        CREATE TABLE [#teacher_affiliations] ([data_pass] INT, [row_num] INT, [individual_customer_no] INT, [individual_inactive] INT, [teacher_first_name] VARCHAR(50), 
                                              [teacher_last_name] VARCHAR(50), [group_customer_no] INT, [group_inactive] INT, [school_name] VARCHAR(100), [street1] VARCHAR(100), 
                                              [street2] VARCHAR(100), [city] VARCHAR(50), [state] VARCHAR(50), [postal_code] VARCHAR(50), 
                                              [affiliation_inactive] CHAR(1), [affiliation_start_dt] DATETIME, [affiliation_end_dt] DATETIME, 
                                              [affiliation_create_dt] DATETIME, [cust_memb_no] INT, [memb_create_dt] DATETIME,
                                              [grades_taught] VARCHAR(100), [subjects_taught] VARCHAR(500), [email_address] VARCHAR(100));

        CREATE TABLE [#tpp_memberships] ([teacher_no] INT, [teacher_first_name] VARCHAR(50), [teacher_last_name] VARCHAR(50), [teacher_email] VARCHAR(100), [school_no] INT,  [school_name] VARCHAR(100), 
                                         [school_address_1] VARCHAR(100), [school_address2] VARCHAR(100), [school_city] VARCHAR(50), [school_state] VARCHAR(25),
                                         [school_postal_code] VARCHAR(25), [grades_taught] VARCHAR(1000), [subjects_taught] VARCHAR(1000),
                                         [memb_no] INT, [memb_pin] VARCHAR(4), [memb_status] VARCHAR(25), [memb_expire_dt] VARCHAR(25), [memb_level] VARCHAR(25), [memb_card_barcode] VARCHAR(20));

        CREATE TABLE [#pin_table] ([row_num] INT, [cust_memb_no] INT, [PIN] CHAR(4), [inactive] CHAR(1));
                      
    /*  Check Parameters  */              

        SELECT @active_only = ISNULL(@active_only,'N');

    /*  Get first set of teacher affiliations (with first dupe check)
        Since we are trying to get the list down to one record per teacher, if any teacher has multiple affiliations, the most recent will be used.
        The most recent affiliation for each teacher will have a row number of 1 so that anything higher than 1 can be deleted.  */
                                            
        INSERT INTO [#teacher_affiliations]
        SELECT 1, 
               ROW_NUMBER() OVER (PARTITION BY afl.[individual_customer_no], afl.[group_customer_no] ORDER BY afl.[individual_customer_no], afl.[create_dt] DESC),
               afl.[individual_customer_no],
               cus.[inactive],
               cus.fname,
               cus.lname,
               afl.[group_customer_no],
               sch.[inactive],
               ISNULL(sch.[lname],''),
               ISNULL(adr.[street1],''),
               ISNULL(adr.[street2],''),
               ISNULL(adr.[city],''),
               ISNULL(adr.[state],''),
               ISNULL(adr.[postal_code],''),
               afl.[inactive],
               afl.start_dt,
               afl.end_dt,
               afl.create_dt,
               0,
               NULL,
               '',
               '',
               ISNULL(ema.[address],'')
        FROM [dbo].[T_AFFILIATION] AS afl
             LEFT OUTER JOIN [dbo].[T_CUSTOMER] AS cus ON cus.[customer_no] = afl.individual_customer_no
             LEFT OUTER JOIN [dbo].[T_CUSTOMER] AS sch ON sch.[customer_no] = afl.[group_customer_no]
             LEFT OUTER JOIN [dbo].[T_ADDRESS] AS adr ON adr.[customer_no] = afl.[group_customer_no] AND adr.primary_ind = 'Y'
             LEFT OUTER JOIN [dbo].[T_EADDRESS] AS ema ON ema.[customer_no] = afl.[individual_customer_no] AND ema.primary_ind ='Y'
        WHERE afl.[affiliation_type_id] = 10197  --Affiliation Type 10197 = Teacher
          AND afl.[inactive] = 'N';
         
        --Delete Duplicate Records
        DELETE FROM [#teacher_affiliations] WHERE [data_pass] = 1 AND [row_num] > 1;

        --Retrieve grades taught and subjects taught from the attributes on the teacher's record.
        UPDATE [#teacher_affiliations]
        SET [grades_taught] = ISNULL(STUFF((SELECT ', ' + kwd.[key_value],''
                              FROM [dbo].[TX_CUST_KEYWORD] AS kwd (NOLOCK)
                              WHERE kwd.[customer_no] = [#teacher_affiliations].[individual_customer_no] and kwd.[keyword_no] = 452  --452 = TPP Grades
                              FOR XML PATH('')) ,1,1,''),''),
            [subjects_taught] = ISNULL(STUFF((SELECT ', ' + kwd.[key_value],''
                                FROM [dbo].[TX_CUST_KEYWORD] AS kwd (NOLOCK)
                                WHERE kwd.[customer_no] = [#teacher_affiliations].[individual_customer_no] and kwd.[keyword_no] = 453  --453 = TPP Subjects
                                FOR XML PATH('')) ,1,1,''),'');

        --Get the date the TPP membership was created
        UPDATE [#teacher_affiliations]
        SET [memb_create_dt] = (SELECT [create_dt] 
                                FROM [dbo].[TX_CUST_MEMBERSHIP] AS mem (NOLOCK)
                                WHERE [cust_memb_no] = [#teacher_affiliations].[cust_memb_no]);


    /*  Get second set of teacher affiliations (with second dupe check)
        Since we are trying to get the list down to one record per teacher, if any teacher has multiple affiliations, the most recent will be used.
        The most recent affiliation for each teacher will have a row number of 1 so that anything higher than 1 can be deleted.  */

        INSERT INTO [#teacher_affiliations]
        SELECT 2,
               ROW_NUMBER() OVER (PARTITION BY [individual_customer_no] ORDER BY [affiliation_create_dt] DESC),
               [individual_customer_no],
               [individual_inactive],
               [teacher_first_name],
               [teacher_last_name],
               [group_customer_no],
               [group_inactive],
               [school_name],
               [street1],
               [street2],
               [city],
               [state],
               [postal_code],
               [affiliation_inactive],
               [affiliation_start_dt],
               [affiliation_end_dt],
               [affiliation_create_dt],
               [cust_memb_no],
               [memb_create_dt],
               [grades_taught],
               [subjects_taught],
               [email_address]
        FROM [#teacher_affiliations]
        WHERE [data_pass] = 1;

        --Delete the records from the first data pass
        DELETE FROM [#teacher_affiliations] WHERE [data_pass] = 1;

        --Delete Duplicate Records
        DELETE FROM [#teacher_affiliations] WHERE [data_pass] = 2 AND row_num > 1;

        --Get most recent membership number for each teacher
        UPDATE [#teacher_affiliations]
        SET [cust_memb_no] = [dbo].[LFS_GetMostRecentMembershipNumber] ([individual_customer_no], 13, 'N');

        /*  Cache all the most recent membership card PINs for all the TPP Memberships in the teacher affiliations table  */

        INSERT INTO [#pin_table]
        SELECT ROW_NUMBER() OVER (PARTITION BY [cust_memb_no] ORDER BY [inactive], [id] DESC),
               [cust_memb_no],
               [PIN],
               [inactive]
        FROM [dbo].[LT_MOS_MEMBER_CARDS]
        WHERE [cust_memb_no] IN (SELECT [cust_memb_no] FROM [#teacher_affiliations] WHERE [cust_memb_no] > 0)
        
    /*  Delete duplicates so that there's one per membership (the most recently created active PIN) */

        DELETE FROM [#pin_table] WHERE row_num > 1
       
    /*  Get final data set combining information from the affiliation temp table with
        information from the TX_CUST_MEMBERSHIP table.  */

        INSERT INTO [#tpp_memberships]
        SELECT tpp.[individual_customer_no],
               tpp.[teacher_first_name],
               tpp.[teacher_last_name],
               tpp.[email_address],
               tpp.[group_customer_no],
               tpp.[school_name],
               tpp.[street1],
               tpp.[street2],
               tpp.[city],
               tpp.[state],
               tpp.[postal_code],
               tpp.[grades_taught],
               tpp.[subjects_taught],
               tpp.[cust_memb_no],
               pin.[PIN],
               sta.[description],
               ISNULL(CONVERT(varchar(25),mem.[expr_dt],101),''),
               ISNULL(mem.[memb_level],''),
               CONVERT(VARCHAR(20),[individual_customer_no])
        FROM [#teacher_affiliations] AS tpp
             LEFT OUTER JOIN [dbo].[TX_CUST_MEMBERSHIP] AS mem ON mem.[cust_memb_no] = tpp.[cust_memb_no] AND mem.[memb_org_no] = 13  --13 = TPP
             LEFT OUTER JOIN [dbo].[TR_CURRENT_STATUS] AS sta ON sta.[id] = mem.[current_status]
             LEFT OUTER JOIN [#pin_table] AS pin ON pin.[cust_memb_no] = tpp.[cust_memb_no]
        WHERE tpp.[data_pass] = 2 AND tpp.[cust_memb_no] > 0
        
   /*  Create Barcode Text for Each Membership  */

        --Add leading zeros to make each number nine-digits
        WHILE EXISTS (SELECT * FROM [#tpp_memberships] WHERE LEN([memb_card_barcode]) < 9)
            UPDATE [#tpp_memberships]
            SET [memb_card_barcode] = '0' + [memb_card_barcode] 
            WHERE LEN([memb_card_barcode]) < 9;

        --Add leading letter C and the PIN at the end
        UPDATE [#tpp_memberships]
        SET [memb_card_barcode] = ISNULL('C' + [memb_card_barcode] + '-' + [memb_pin],'')
      
    FINISHED:
    
        /*  Pull final list of TPP members
            Check for and remove any extra double-quotes in any of the string data elements
            Remove any leading spaces from the grades/subjects taught fields  */

            SELECT GETDATE() AS 'run_dt',
                   [school_no],
                   REPLACE([school_name],'"','') AS 'school_name',
                   [teacher_no],
                   REPLACE([teacher_first_name],'"','') AS 'teacher_first_name' ,
                   REPLACE([teacher_last_name],'"','') AS 'teacher_last_name',
                   REPLACE([teacher_email],'"','') AS 'teacher_email_address',
                   REPLACE([school_address_1],'"','') AS 'school_address_1' ,
                   REPLACE([school_address2],'"','') AS 'school_address_2', 
                   REPLACE([school_city],'"','') AS 'school_city',
                   REPLACE([school_state],'"','') AS 'school_state',
                   REPLACE(LEFT([school_postal_code],5),'"','') AS 'school_postal_code',
                   LTRIM(REPLACE([grades_taught],'"','')) AS 'grades_taught',
                   LTRIM(REPLACE([subjects_taught],'"','')) AS 'subjects_taught',
                   [memb_no],
                   ISNULL([memb_pin],'') AS 'memb_pin',
                   [memb_status],
                   [memb_expire_dt],
                   [memb_level],
                   [memb_card_barcode]
            FROM [#tpp_memberships]
            WHERE (@active_only <> 'Y' OR [memb_status] = 'Active')
            ORDER BY [school_name], [teacher_last_name], [teacher_first_name]

END
GO

GRANT EXECUTE ON [dbo].[LRP_TPP_MEMBER_LIST] TO ImpUsers
GO


EXECUTE [dbo].[LRP_TPP_MEMBER_LIST] @active_only = 'N'




