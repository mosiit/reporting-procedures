USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LRP_FINANCE_CONTRIBUTION_909_REPORT]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LRP_FINANCE_CONTRIBUTION_909_REPORT]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LRP_FINANCE_CONTRIBUTION_909_REPORT]
        @report_start_dt DATETIME,
        @report_end_dt DATETIME,
        @contribution_threshold DECIMAL(18,2)
AS BEGIN

        DECLARE @government CHAR(1)             SELECT @government = 'Y'
        DECLARE @rpt_message VARCHAR(255)       SELECT @rpt_message = ''
        DECLARE @customer_no INT                SELECT @customer_no = 0
        DECLARE @report_dt DATETIME             SELECT @report_dt = GETDATE()


    /*  Threshold defaults to 100,000 if null is passed to the procedure  */

        SELECT @contribution_threshold = 100000

    /*  Create temp table to contain the raw data about the contributions  */
        
        IF OBJECT_ID('tempdb..#contribution_raw_data') IS NOT NULL DROP TABLE [#contribution_raw_data]
   
        CREATE TABLE [#contribution_raw_data] ([report_date] datetime, [report_message] varchar(100), [record_type] varchar(30), [ref_no] int, [customer_no] int, [customer_type_no] int, 
                                               [customer_type_name] varchar(30), [customer_first_name] varchar(20), [customer_middle_name] varchar(20), [customer_last_name] varchar(55),
                                               [customer_sort_name] varchar(255), [contribution_type] varchar(50), [contribution_dt] datetime, [contribution_date] char(10),  [contribution_time] char(8), 
                                               [contribution_amount] decimal(18,2), [fund_description] varchar(30), [nonrestricted_income_gl_no] varchar(30), [source] varchar(4), [creditee_no] int, 
                                               [creditee_type_no] int, [creditee_type_name] varchar(30), [creditee_first_name] varchar(20), [creditee_middle_name] varchar(20), [creditee_last_name] varchar(55), 
                                               [creditee_sort_name] varchar(100))

        CREATE CLUSTERED INDEX [ix_contribution_table_customer_no] ON [#contribution_raw_data] ([customer_no] ASC) ON [PRIMARY]

    /*  Create temp table to compile the list of customers who meet the threshold criteria  */

        IF OBJECT_ID('tempdb..#contribution_909_customers') IS NOT NULL DROP TABLE [#contribution_909_customers]

        CREATE TABLE [#contribution_909_customers] ([customer_no] INT, [total_contributions] DECIMAL(18,2))

        CREATE CLUSTERED INDEX [ix_contribution_909_customers_customer_no] ON [#contribution_909_customers] ([customer_no] ASC) ON [PRIMARY]

        CREATE NONCLUSTERED INDEX [ix_contribution_909_customers_total_contributions] ON [#contribution_909_customers] ([total_contributions] ASC) ON [PRIMARY]

    /*  Create a temp table to hold the final data about the customer and their contributions to be pulled into the report  */

        IF OBJECT_ID('tempdb..#contribution_final_data') IS NOT NULL DROP TABLE [#contribution_final_data]

        CREATE TABLE [#contribution_final_data] ([report_dt] DATETIME, [rpt_message] VARCHAR(255), [customer_no] int, [customer_type_name] VARCHAR(30), [customer_first_name] VARCHAR(30), [customer_middle_name] VARCHAR(30), 
                                                 [customer_last_name] VARCHAR(60), [customer_sort_name] VARCHAR(125), [address_type] VARCHAR(30), [address_street_1] VARCHAR(100), [address_street_2] VARCHAR(100), 
                                                 [address_street_3] VARCHAR(100), [address_city] VARCHAR(30), [address_state] VARCHAR(20), [address_postal_code] VARCHAR(10), [address_country] VARCHAR(30), [name_address_display] VARCHAR(255),
                                                 [contribution_type] VARCHAR(30), [contribution_dt] DATETIME, [contribution_date] CHAR(10), [contribution_time] VARCHAR(8), [contribution_amount] DECIMAL(18,2), [fund_description] VARCHAR(30), 
                                                 [nonrestricted_income_gl_no] VARCHAR(50), [creditee_no] INT, [creditee_type_name] VARCHAR(30), [creditee_first_name] VARCHAR(30), [creditee_middle_name] VARCHAR(30),
                                                 [creditee_last_name] VARCHAR(60), [creditee_sort_name] VARCHAR(125), [gift_counter] INT)

    /*  Raw data pulled un using the same procedure used by the standard Finance Contribution Report  */

        INSERT INTO [#contribution_raw_data]
        EXECUTE [dbo].[LRP_FINANCE_CONTRIBUTION_REPORT] @report_start_dt, @report_end_dt, @government;


    /*  Generate a list of all customers and their total giving for the supplied date range  */        

        INSERT INTO #contribution_909_customers
        SELECT customer_no, SUM(contribution_amount) FROM [#contribution_raw_data] WHERE [customer_no] > 0 GROUP BY [customer_no]

    /*  Delete all customers who do not meet the threshold passed to the procedure.  If threshold is 0, all customers are returned  */

        DELETE FROM [#contribution_909_customers] WHERE [total_contributions] < @contribution_threshold

    /*  Check the customer list to make sure each one has only one primary address  (everyone should only have one address flagged as primary)
        Multiple primary addresses can cause contributions to be counted multiple times so if any are found, the report stops here and provides
        a list of customer numbers that need to be fixed before the report can be run  */

        IF EXISTS (SELECT [customer_no] FROM [dbo].T_ADDRESS WHERE [customer_no] IN (SELECT [customer_no] FROM [#contribution_909_customers]) AND [primary_ind] = 'Y' GROUP BY [customer_no] HAVING COUNT(*) > 1) BEGIN
            
            SELECT @rpt_message = 'BAD DATA:  The following customers have *multiple* primary addresses that must be fixed before the report can be run.' + CHAR(10)

            DECLARE multiple_address_cursor INSENSITIVE CURSOR FOR 
            SELECT [customer_no] FROM [dbo].[T_ADDRESS] WHERE [customer_no] IN (SELECT [customer_no] FROM [#contribution_909_customers]) AND [primary_ind] = 'Y' GROUP BY [customer_no] HAVING COUNT(*) > 1
            OPEN multiple_address_cursor
            BEGIN_MULTIPLE_ADDRESS_LOOP:

                FETCH NEXT FROM multiple_address_cursor INTO @customer_no
                IF @@FETCH_STATUS = -1 GOTO END_MULTIPLE_ADDRESS_LOOP

                SELECT @rpt_message = @rpt_message + LTRIM(rtrim(CONVERT(VARCHAR(25),@customer_no))) + '  -  '
                
                GOTO BEGIN_MULTIPLE_ADDRESS_LOOP

            END_MULTIPLE_ADDRESS_LOOP:
            CLOSE multiple_address_cursor
            DEALLOCATE multiple_address_cursor

            GOTO FINISHED
            
        END

        /*  Get the final data including the contribution information from the raw data table as well as the customer's address information from the T_ADDRESS table  */
        
            INSERT INTO [#contribution_final_data]
            SELECT @report_dt, '', con.[customer_no], con.[customer_type_name], con.[customer_first_name], con.[customer_middle_name], con.[customer_last_name], '' AS 'customer_sort_name', 
                   ISNULL(aty.[description],''), ISNULL(adr.[street1],''), ISNULL(adr.[street2],''), ISNULL(adr.[street3],''), ISNULL(adr.[city],''), ISNULL(adr.[state],''), ISNULL(adr.[postal_code],''),
                   ISNULL(cou.[description],''), '', con.[contribution_type], con.[contribution_dt], con.[contribution_date], con.contribution_time, con.[contribution_amount], con.[fund_description], 
                   con.[nonrestricted_income_gl_no], con.[creditee_no], con.[creditee_type_name], con.[creditee_first_name], con.[creditee_middle_name], con.[creditee_last_name], '' AS 'creditee_sort_name', 1
            FROM [#contribution_raw_data]  AS con
                 LEFT OUTER JOIN [dbo].[T_ADDRESS] AS adr (NOLOCK) ON adr.[customer_no] = con.[customer_no] AND adr.[primary_ind] = 'Y'
                 LEFT OUTER JOIN [dbo].[TR_ADDRESS_TYPE] AS aty (NOLOCK) ON aty.[id] = adr.[address_type]
                 LEFT OUTER JOIN [dbo].[TR_COUNTRY] AS cou (NOLOCK) ON cou.[id] = adr.[country]
            WHERE con.[customer_no] IN (SELECT [customer_no] FROM [#contribution_909_customers])

        /*  Add the dash into the zip code if the plus four is present  */

            UPDATE [#contribution_final_data] SET [address_postal_code] = LEFT(RTRIM([address_postal_code]),5) + '-' + RIGHT(RTRIM([address_postal_code]),4) WHERE LEN(RTRIM([address_postal_code])) = 9

        /*  Create the customer sort name (last, first, middle  */

            UPDATE [#contribution_final_data] SET [customer_sort_name] = LTRIM(RTRIM([customer_last_name]))
                                                                       + CASE WHEN [customer_first_name] = '' THEN '' ELSE ' ' + [customer_first_name] END
                                                                       + CASE WHEN [customer_middle_name] = '' THEN '' ELSE ' ' + [customer_middle_name] END
                                                                       + ' ' + CONVERT(VARCHAR(50),[customer_no])

        /*  Create the creditee sort name (last, first, middle  */

            UPDATE [#contribution_final_data] SET [creditee_sort_name] = LTRIM(RTRIM([creditee_last_name]))
                                                                       + CASE WHEN [creditee_first_name] = '' THEN '' ELSE ' ' + [creditee_first_name] END
                                                                       + CASE WHEN [creditee_middle_name] = '' THEN '' ELSE ' ' + [creditee_middle_name] END
                                                                       + ' ' + CONVERT(VARCHAR(50),[creditee_no])

        /*  Change contribution type to a more user-friendly description (G = Gift, P = Pledge, and A = Pledge Payment 
            If it's none of these, it simply leaves it as it is  */
    
            UPDATE [#contribution_final_data] SET [contribution_type] = CASE WHEN [contribution_type] = 'G' THEN 'Gift'
                                                                             WHEN [contribution_type] = 'P' THEN 'Pledge'
                                                                             WHEN [contribution_type] = 'A' THEN 'Payment'
                                                                             ELSE [contribution_type] END

        /*  Create the name and address display field which includes full name and address information with carriage returns  */

            UPDATE [#contribution_final_data] SET [name_address_display] = LTRIM([customer_first_name] + ' ' + [customer_middle_name] + ' ' + [customer_last_name]) + CHAR(13) + CHAR(10)
                                                                         + CASE WHEN [address_street_1] = '' THEN '' ELSE [address_street_1] + CHAR(13) + CHAR(10) END
                                                                         + CASE WHEN [address_street_2] = '' THEN '' ELSE [address_street_2] + CHAR(13) + CHAR(10) END
                                                                         + CASE WHEN [address_street_3] = '' THEN '' ELSE [address_street_3] + CHAR(13) + CHAR(10) END
                                                                         + [address_city] + ', ' + [address_state] + '  ' + [address_postal_code]

        FINISHED:

            /*  If no records exist in the final table, add a single record with a message to be passed back to the report  */

                IF NOT EXISTS(SELECT * FROM [#contribution_final_data]) BEGIN

                    IF @rpt_message = '' SELECT @rpt_message = 'No data found for the criteria you entered.'

                    INSERT INTO #contribution_final_data
                    VALUES  (@report_dt, @rpt_message, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', Null, '', '', 0.00, '', '', 0, '', '', '', '', '', 0)

                END

        /*  Select final data set from the final table to be passed back to the report  */

            SELECT [report_dt], [rpt_message], [customer_no], [customer_type_name], [customer_first_name], [customer_middle_name], [customer_last_name], [customer_sort_name], 
                   [address_type], [address_street_1], [address_street_2], [address_street_3], [address_city], [address_state], [address_postal_code], [address_country], [name_address_display],
                   [contribution_type], [contribution_dt], [contribution_date], [contribution_time], [contribution_amount], [fund_description], [nonrestricted_income_gl_no], 
                   [creditee_no], [creditee_type_name], [creditee_first_name], [creditee_middle_name], [creditee_last_name], [creditee_sort_name], [gift_counter]
            FROM [#contribution_final_data]

        /*  Though not stricly necessary, clean up the TempDB by deleting the temporary tables  */

           IF OBJECT_ID('tempdb..#contribution_final_data') IS NOT NULL DROP TABLE [#contribution_final_data]

            IF OBJECT_ID('tempdb..#contribution_909_table') IS NOT NULL DROP TABLE [#contribution_909_table]

            IF OBJECT_ID('tempdb..#contribution_raw_data') IS NOT NULL DROP TABLE [#contribution_raw_data]

END
GO

GRANT EXECUTE ON [dbo].[LRP_FINANCE_CONTRIBUTION_909_REPORT] TO ImpUsers
GO


--EXECUTE [dbo].[LRP_FINANCE_CONTRIBUTION_909_REPORT] @report_start_dt = '1-1-2016', @report_end_dt = '11-30-2016', @contribution_threshold = 100000
--REPLACE(REPLACE([name_address_display],CHAR(10), ' '), CHAR(13), ' ')


