USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LP_AUTO_UPDATE_HISTORY]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LP_AUTO_UPDATE_HISTORY]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LP_AUTO_UPDATE_HISTORY]
AS BEGIN

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

    /*  Procedure Variables  */

        DECLARE @last_run_date datetime, @create_dt datetime, @work_dt DATETIME, @trx_stat_dt DATETIME, @sales_met_dt DATETIME
        DECLARE @dates_to_update table (history_dt datetime)
        DECLARE @archive_date CHAR(10)

    /*  Determine the last date when records were added to the LT_HISTORY_TICKET table  */

        SELECT @last_run_date = max(create_dt) FROM [dbo].[LT_HISTORY_TICKET]
    
    /*  Insert Yesterday's date to be processed
        NOTE: Physical table used so that if procedure stops in the middle, those dates remain in the table
              and the next time the procedure is run, the dates will be processed  */

        INSERT INTO [dbo].[LTR_HISTORY_DATES] ([history_type], [history_dt]) VALUES ('attendance', dateadd(day, -1, getdate()))

    /*  Insert dates that have changed since the last time the procedure was run.  */

        INSERT INTO [dbo].[LTR_HISTORY_DATES] ([history_type], [history_dt])
        SELECT DISTINCT 'attendance', [perf_dt] FROM [dbo].[T_Perf]
        WHERE [perf_no] in (SELECT [perf_no] FROM [dbo].[T_SUB_LINEITEM] (NOLOCK) WHERE [create_dt] > @last_run_date or [last_update_dt] > @last_run_date) and [perf_dt] < getdate()

    /*  Redo the last 30 days regardless of change  */

        INSERT INTO [dbo].[LTR_HISTORY_DATES] ([history_type], [history_dt])
        SELECT 'attendance', [return_dt] 
        FROM [dbo].[LFT_INDIVIDUAL_DATES](DATEADD(DAY, -30, GETDATE()),DATEADD(DAY, -1, GETDATE()))
        
    /*  Once data has been archived, any changes are ignored.  Also any dates before go live should be ignored.  */

        SELECT @archive_date = MAX([perf_date]) FROM [dbo].[LT_HISTORY_TICKET_ARCHIVE]
        SELECT @archive_date = ISNULL(@archive_date,'2016/05/17')

        DELETE FROM [dbo].[LTR_HISTORY_DATES] WHERE CONVERT(CHAR(10),[history_dt],111) <= @archive_date
        
    /*  If there are any dates to update, do the update one date at a time  */

        IF exists (SELECT * FROM [dbo].[LTR_HISTORY_DATES]) BEGIN

            DECLARE UpdateCursor INSENSITIVE CURSOR FOR
            SELECT DISTINCT convert(char(10),[history_dt],111) FROM [dbo].[LTR_HISTORY_DATES] 
            WHERE [history_type] = 'attendance' ORDER BY convert(char(10),[history_dt],111) DESC
            OPEN UpdateCursor
            BEGIN_UPDATE_LOOP:

                FETCH NEXT FROM UpdateCursor INTO @work_dt
                IF @@FETCH_STATUS = -1 GOTO END_UPDATE_LOOP

                SELECT @create_dt = IsNull(@create_dt, getdate())

                /*  If this date has been archived, skip it  */
                IF EXISTS (SELECT * FROM [dbo].[LT_HISTORY_TICKET_ARCHIVE] WHERE [performance_date] = @work_dt) GOTO BEGIN_UPDATE_LOOP
        
                EXECUTE [dbo].[LP_UPDATE_HISTORY_TICKET] @history_dt = @work_dt, @create_dt = @create_dt, @include_partial_paid = 'Y'

                EXECUTE [dbo].[LP_UPDATE_HISTORY_VISIT_COUNT] @report_start_date = @work_dt, @return_or_write = 'W'

                EXECUTE [dbo].[LP_UPDATE_HISTORY_TICKET_ANALYTICS] @history_dt = @work_dt

                PRINT convert(char(10),@work_dt,111) + ' - Processed'

                DELETE FROM [dbo].[LTR_HISTORY_DATES] WHERE convert(char(10),history_dt,111) = convert(char(10),@work_dt,111)

                GOTO BEGIN_UPDATE_LOOP

            END_UPDATE_LOOP:
            CLOSE UpdateCursor
            DEALLOCATE UpdateCursor

        END

    /*  Update transaction statistics data mart - Add one date to the maximum date in the table then process from that date through yesterday  */
    
        SELECT @trx_stat_dt = DATEADD(DAY,1,MAX(history_dt)) FROM [dbo].[LT_HISTORY_TRX_STATS]

        WHILE @trx_stat_dt < DATEADD(DAY,-1,GETDATE())  BEGIN

            EXECUTE [dbo].[LP_UPDATE_HISTORY_TRX_STATS] @history_dt= @trx_stat_dt, @create_dt = @create_dt, @operator = '', @return_or_write = 'W'

            SELECT @trx_stat_dt = DATEADD(DAY,1,@trx_stat_dt)

        END

    /*  Update Sales Metrics - Add one date to the maximum date in the table then process from that date through yesterday  
            NOTE:  THIS IS THE NEW UPDATE TRANSACTION STATISTICS.  BOTH ARE RUNNING FOR NOW BUT EVENTUALLY THE CODE SECTION ABOVE WILL BE REMOVED  */

        SELECT @sales_met_dt = DATEADD(DAY,1,MAX(history_dt)) FROM [dbo].[LT_HISTORY_SALES_METRICS]

        WHILE @sales_met_dt < DATEADD(DAY,-1,GETDATE())  BEGIN

            EXECUTE [dbo].[LP_UPDATE_HISTORY_SALES_METRICS] @history_dt= @sales_met_dt, @run_dt = @create_dt, @operator = '', @return_or_write = 'W'

            SELECT @sales_met_dt = DATEADD(DAY,1,@sales_met_dt)

        END

    DONE:



END
GO

GRANT EXECUTE ON [dbo].[LP_AUTO_UPDATE_HISTORY] TO impusers
GO


